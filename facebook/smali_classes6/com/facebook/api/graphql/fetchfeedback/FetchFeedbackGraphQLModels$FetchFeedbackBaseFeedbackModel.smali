.class public final Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5AI;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x68880daa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864126
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864127
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 864128
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 864129
    return-void
.end method

.method private A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864130
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 864131
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864132
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 864133
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private C()I
    .locals 2

    .prologue
    .line 864134
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864135
    iget v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->D:I

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 864136
    iput p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->D:I

    .line 864137
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864138
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864139
    if-eqz v0, :cond_0

    .line 864140
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 864141
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 864142
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 864143
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864144
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864145
    if-eqz v0, :cond_0

    .line 864146
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 864147
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 864148
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 864149
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864150
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864151
    if-eqz v0, :cond_0

    .line 864152
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 864153
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 864154
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 864155
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864156
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864157
    if-eqz v0, :cond_0

    .line 864158
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 864159
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 864067
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 864068
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864069
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864070
    if-eqz v0, :cond_0

    .line 864071
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 864072
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 864160
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->g:Z

    .line 864161
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864162
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864163
    if-eqz v0, :cond_0

    .line 864164
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 864165
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 864166
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->k:Z

    .line 864167
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864168
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864169
    if-eqz v0, :cond_0

    .line 864170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 864171
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 864172
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->o:Z

    .line 864173
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864174
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864175
    if-eqz v0, :cond_0

    .line 864176
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 864177
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 864178
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->r:Z

    .line 864179
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864180
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864181
    if-eqz v0, :cond_0

    .line 864182
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 864183
    :cond_0
    return-void
.end method

.method private s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864184
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 864185
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864186
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 864187
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    return-object v0
.end method

.method private u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863913
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 863914
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864188
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 864189
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    return-object v0
.end method

.method private w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864190
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 864191
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private x()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 864192
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y:Ljava/util/List;

    .line 864193
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864194
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 864195
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method

.method private z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864196
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 864197
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 864198
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864199
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 864200
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->n()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 864201
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 864202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 864203
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 864204
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 864205
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 864206
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->q()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 864207
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 864208
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 864209
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 864210
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 864211
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 864212
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 864213
    const/16 v17, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 864214
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->e:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864215
    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->f:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864216
    const/16 v17, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864217
    const/16 v17, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->h:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864218
    const/16 v17, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->i:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864219
    const/16 v17, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->j:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864220
    const/16 v17, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->k:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864221
    const/16 v17, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->l:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864222
    const/16 v17, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->m:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 864223
    const/16 v17, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 864224
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->o:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 864225
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 864226
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 864227
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 864228
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 864229
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 864230
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 864231
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 864232
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 864233
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 864234
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 864235
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 864236
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 864237
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 864238
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864239
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->D:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 864240
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864241
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 864073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864074
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 864075
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 864076
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 864077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864078
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 864079
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 864080
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 864081
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 864082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864083
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 864084
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 864085
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 864086
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 864087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864088
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 864089
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 864090
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 864091
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 864092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864093
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 864094
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 864095
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 864096
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 864097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864098
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 864099
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 864100
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 864101
    if-eqz v2, :cond_5

    .line 864102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864103
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y:Ljava/util/List;

    move-object v1, v0

    .line 864104
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 864105
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 864106
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 864107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864108
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 864109
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 864110
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 864111
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 864112
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864113
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 864114
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 864115
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 864116
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 864117
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864118
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 864119
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 864120
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 864121
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 864122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    .line 864123
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 864124
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864125
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 863905
    new-instance v0, LX/5BT;

    invoke-direct {v0, p1}, LX/5BT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864043
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 864029
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 864030
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->e:Z

    .line 864031
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->f:Z

    .line 864032
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->g:Z

    .line 864033
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->h:Z

    .line 864034
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->i:Z

    .line 864035
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->j:Z

    .line 864036
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->k:Z

    .line 864037
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->l:Z

    .line 864038
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->m:Z

    .line 864039
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->o:Z

    .line 864040
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->r:Z

    .line 864041
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->D:I

    .line 864042
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 863977
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863978
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 863979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 863980
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 863981
    :goto_0
    return-void

    .line 863982
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863983
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 863984
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 863985
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 863986
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 863987
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 863988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 863989
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 863990
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 863991
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 863992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 863993
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 863994
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 863995
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 863996
    if-eqz v0, :cond_9

    .line 863997
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 863998
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 863999
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 864000
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 864001
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 864002
    if-eqz v0, :cond_9

    .line 864003
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864004
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864005
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 864006
    :cond_5
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 864007
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 864008
    if-eqz v0, :cond_9

    .line 864009
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864010
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864011
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 864012
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 864013
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 864014
    if-eqz v0, :cond_9

    .line 864015
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864016
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864017
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 864018
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 864019
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 864020
    if-eqz v0, :cond_9

    .line 864021
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864022
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864023
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 864024
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 864025
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864027
    const/16 v0, 0x19

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 864028
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 863968
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863969
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V

    .line 863970
    :cond_0
    :goto_0
    return-void

    .line 863971
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 863972
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 863973
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 863974
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0

    .line 863975
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863976
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 863917
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863918
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(Z)V

    .line 863919
    :cond_0
    :goto_0
    return-void

    .line 863920
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 863921
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->b(Z)V

    goto :goto_0

    .line 863922
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 863923
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->c(Z)V

    goto :goto_0

    .line 863924
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 863925
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->d(Z)V

    goto :goto_0

    .line 863926
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 863927
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 863928
    if-eqz v0, :cond_0

    .line 863929
    if-eqz p3, :cond_5

    .line 863930
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 863931
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    .line 863932
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    goto :goto_0

    .line 863933
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 863934
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 863935
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 863936
    if-eqz v0, :cond_0

    .line 863937
    if-eqz p3, :cond_7

    .line 863938
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 863939
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 863940
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 863941
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 863942
    :cond_8
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 863943
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 863944
    if-eqz v0, :cond_0

    .line 863945
    if-eqz p3, :cond_9

    .line 863946
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 863947
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    .line 863948
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 863949
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 863950
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 863951
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 863952
    if-eqz v0, :cond_0

    .line 863953
    if-eqz p3, :cond_b

    .line 863954
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 863955
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 863956
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 863957
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto/16 :goto_0

    .line 863958
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 863959
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 863960
    if-eqz v0, :cond_0

    .line 863961
    if-eqz p3, :cond_d

    .line 863962
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 863963
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 863964
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 863965
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0

    .line 863966
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863967
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 863915
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863916
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 863911
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863912
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 863908
    new-instance v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;-><init>()V

    .line 863909
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 863910
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 863906
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863907
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 863903
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863904
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 864044
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864045
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 864046
    const v0, -0x6c97d8d2

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 864047
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864048
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 864049
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 864050
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864051
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 864052
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864053
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864054
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->n:Ljava/lang/String;

    .line 864055
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 864056
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864057
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->o:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864058
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->p:Ljava/lang/String;

    .line 864059
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 864060
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864061
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->r:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864062
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s:Ljava/lang/String;

    .line 864063
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864064
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w:Ljava/lang/String;

    .line 864065
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864066
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method
