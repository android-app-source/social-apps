.class public final Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/3cp;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x62eb5da4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864727
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864726
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 864724
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 864725
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864702
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864703
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    return-object v0
.end method

.method private a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 864718
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864719
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 864720
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 864721
    if-eqz v0, :cond_0

    .line 864722
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 864723
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 864712
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864713
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 864714
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 864715
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 864716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864717
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 864704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864705
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 864706
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864707
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 864708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;

    .line 864709
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864710
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864711
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 864728
    new-instance v0, LX/5Bb;

    invoke-direct {v0, p1}, LX/5Bb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 864689
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 864690
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    .line 864691
    if-eqz v0, :cond_0

    .line 864692
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 864693
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 864694
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 864695
    :goto_0
    return-void

    .line 864696
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 864677
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 864678
    check-cast p2, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;)V

    .line 864679
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 864680
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 864681
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->a()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    .line 864682
    if-eqz v0, :cond_0

    .line 864683
    if-eqz p3, :cond_1

    .line 864684
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864685
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->a(I)V

    .line 864686
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;->e:Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    .line 864687
    :cond_0
    :goto_0
    return-void

    .line 864688
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 864697
    new-instance v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel;-><init>()V

    .line 864698
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 864699
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 864700
    const v0, -0x5740a4df

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 864701
    const v0, -0x78fb05b

    return v0
.end method
