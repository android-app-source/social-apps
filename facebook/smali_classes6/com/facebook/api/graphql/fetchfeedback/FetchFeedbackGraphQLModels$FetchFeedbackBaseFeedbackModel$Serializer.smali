.class public final Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 863790
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 863791
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 863792
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 863793
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 863794
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 863795
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 863796
    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result v2

    .line 863797
    if-eqz v2, :cond_0

    .line 863798
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863799
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863800
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863801
    if-eqz v2, :cond_1

    .line 863802
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863803
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863804
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863805
    if-eqz v2, :cond_2

    .line 863806
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863807
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863808
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863809
    if-eqz v2, :cond_3

    .line 863810
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863811
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863812
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863813
    if-eqz v2, :cond_4

    .line 863814
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863815
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863816
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863817
    if-eqz v2, :cond_5

    .line 863818
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863819
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863820
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863821
    if-eqz v2, :cond_6

    .line 863822
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863823
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863824
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863825
    if-eqz v2, :cond_7

    .line 863826
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863827
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863828
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863829
    if-eqz v2, :cond_8

    .line 863830
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863831
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863832
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863833
    if-eqz v2, :cond_9

    .line 863834
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863835
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863836
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863837
    if-eqz v2, :cond_a

    .line 863838
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863839
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863840
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863841
    if-eqz v2, :cond_b

    .line 863842
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863843
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863844
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863845
    if-eqz v2, :cond_c

    .line 863846
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863847
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863848
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863849
    if-eqz v2, :cond_d

    .line 863850
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863851
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863852
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863853
    if-eqz v2, :cond_e

    .line 863854
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863855
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863856
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863857
    if-eqz v2, :cond_f

    .line 863858
    const-string v3, "likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863859
    invoke-static {v1, v2, p1}, LX/5Ac;->a(LX/15i;ILX/0nX;)V

    .line 863860
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863861
    if-eqz v2, :cond_10

    .line 863862
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863863
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 863864
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863865
    if-eqz v2, :cond_11

    .line 863866
    const-string v3, "real_time_activity_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863867
    invoke-static {v1, v2, p1, p2}, LX/5Ab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863868
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863869
    if-eqz v2, :cond_12

    .line 863870
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863871
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863872
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863873
    if-eqz v2, :cond_13

    .line 863874
    const-string v3, "reshares"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863875
    invoke-static {v1, v2, p1}, LX/5Ad;->a(LX/15i;ILX/0nX;)V

    .line 863876
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863877
    if-eqz v2, :cond_14

    .line 863878
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863879
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863880
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863881
    if-eqz v2, :cond_15

    .line 863882
    const-string v3, "top_level_comments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863883
    invoke-static {v1, v2, p1}, LX/5Ae;->a(LX/15i;ILX/0nX;)V

    .line 863884
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863885
    if-eqz v2, :cond_16

    .line 863886
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863887
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863888
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863889
    if-eqz v2, :cond_17

    .line 863890
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863891
    invoke-static {v1, v2, p1, p2}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863892
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863893
    if-eqz v2, :cond_18

    .line 863894
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863895
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 863896
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 863897
    if-eqz v2, :cond_19

    .line 863898
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863899
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 863900
    :cond_19
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 863901
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 863902
    check-cast p1, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel$Serializer;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchFeedbackGraphQLModels$FetchFeedbackBaseFeedbackModel;LX/0nX;LX/0my;)V

    return-void
.end method
