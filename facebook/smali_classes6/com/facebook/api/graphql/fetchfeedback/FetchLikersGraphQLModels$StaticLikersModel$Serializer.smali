.class public final Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 864786
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 864787
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 864788
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 864789
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 864790
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x0

    .line 864791
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 864792
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 864793
    if-eqz v2, :cond_0

    .line 864794
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864795
    invoke-static {v1, v0, v4, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 864796
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864797
    if-eqz v2, :cond_1

    .line 864798
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864799
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864800
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864801
    if-eqz v2, :cond_2

    .line 864802
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864803
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864804
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864805
    if-eqz v2, :cond_3

    .line 864806
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864807
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864808
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864809
    if-eqz v2, :cond_4

    .line 864810
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864811
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864812
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864813
    if-eqz v2, :cond_5

    .line 864814
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864815
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864816
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864817
    if-eqz v2, :cond_6

    .line 864818
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864819
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864820
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864821
    if-eqz v2, :cond_7

    .line 864822
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864823
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864824
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864825
    if-eqz v2, :cond_8

    .line 864826
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864827
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864828
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864829
    if-eqz v2, :cond_9

    .line 864830
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864831
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864832
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864833
    if-eqz v2, :cond_a

    .line 864834
    const-string v3, "comments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864835
    invoke-static {v1, v2, p1}, LX/5Bg;->a(LX/15i;ILX/0nX;)V

    .line 864836
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 864837
    if-eqz v2, :cond_b

    .line 864838
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864839
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864840
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864841
    if-eqz v2, :cond_c

    .line 864842
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864843
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864844
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 864845
    if-eqz v2, :cond_d

    .line 864846
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864847
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864848
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864849
    if-eqz v2, :cond_e

    .line 864850
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864851
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864852
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 864853
    if-eqz v2, :cond_f

    .line 864854
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864855
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 864856
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 864857
    if-eqz v2, :cond_10

    .line 864858
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864859
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864860
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864861
    if-eqz v2, :cond_11

    .line 864862
    const-string v3, "like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864863
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864864
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864865
    if-eqz v2, :cond_12

    .line 864866
    const-string v3, "likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864867
    invoke-static {v1, v2, p1, p2}, LX/5Bf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864868
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864869
    if-eqz v2, :cond_13

    .line 864870
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864871
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 864872
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 864873
    if-eqz v2, :cond_14

    .line 864874
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864875
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864876
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864877
    if-eqz v2, :cond_16

    .line 864878
    const-string v3, "seen_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864879
    const/4 v3, 0x0

    .line 864880
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 864881
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 864882
    if-eqz v3, :cond_15

    .line 864883
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864884
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 864885
    :cond_15
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 864886
    :cond_16
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864887
    if-eqz v2, :cond_17

    .line 864888
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864889
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864890
    :cond_17
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864891
    if-eqz v2, :cond_18

    .line 864892
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864893
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864894
    :cond_18
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864895
    if-eqz v2, :cond_19

    .line 864896
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864897
    invoke-static {v1, v2, p1, p2}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864898
    :cond_19
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864899
    if-eqz v2, :cond_1a

    .line 864900
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864901
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 864902
    :cond_1a
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864903
    if-eqz v2, :cond_1b

    .line 864904
    const-string v3, "viewer_does_not_like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864905
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864906
    :cond_1b
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 864907
    if-eqz v2, :cond_1c

    .line 864908
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864909
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 864910
    :cond_1c
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 864911
    if-eqz v2, :cond_1d

    .line 864912
    const-string v3, "viewer_likes_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864913
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 864914
    :cond_1d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 864915
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 864916
    check-cast p1, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$Serializer;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;LX/0nX;LX/0my;)V

    return-void
.end method
