.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3b1ac866
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865934
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865958
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865956
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865957
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865954
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865955
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 865948
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865949
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 865950
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 865951
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 865952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865953
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865941
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865942
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865943
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 865944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;

    .line 865945
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865946
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865947
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865937
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageUnlikeModel;-><init>()V

    .line 865938
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865939
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865936
    const v0, 0x184b4dac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865935
    const v0, 0x40fb6ece

    return v0
.end method
