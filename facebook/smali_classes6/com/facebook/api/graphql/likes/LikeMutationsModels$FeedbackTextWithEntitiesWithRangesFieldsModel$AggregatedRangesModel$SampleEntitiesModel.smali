.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c970a70
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865699
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865698
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865696
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865697
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865693
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 865694
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 865695
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865691
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->f:Ljava/lang/String;

    .line 865692
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865666
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    .line 865667
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865689
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->h:Ljava/lang/String;

    .line 865690
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 865677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865678
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 865679
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 865680
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 865681
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 865682
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 865683
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 865684
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 865685
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 865686
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 865687
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865688
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 865674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865676
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865673
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865670
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;-><init>()V

    .line 865671
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865672
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865669
    const v0, -0x2aee5f44    # -1.00074709E13f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865668
    const v0, 0x7c02d003

    return v0
.end method
