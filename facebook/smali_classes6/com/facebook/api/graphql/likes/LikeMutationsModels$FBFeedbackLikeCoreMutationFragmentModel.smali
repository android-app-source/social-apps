.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x51e822da
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865397
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865396
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865394
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865395
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedback"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865392
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865393
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 865386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865387
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 865388
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 865389
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 865390
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865391
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865378
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865379
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865380
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865381
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 865382
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;

    .line 865383
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865384
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865385
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865375
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeCoreMutationFragmentModel;-><init>()V

    .line 865376
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865377
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865373
    const v0, -0x7cfe5136

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865374
    const v0, 0x2afb9591

    return v0
.end method
