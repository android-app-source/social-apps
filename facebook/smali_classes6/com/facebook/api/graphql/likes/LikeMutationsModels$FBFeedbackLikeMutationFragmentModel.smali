.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x90f836f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865553
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865554
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865555
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865556
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 865557
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865558
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865559
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 865560
    iput-object p1, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 865561
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 865562
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 865563
    if-eqz v0, :cond_0

    .line 865564
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 865565
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 865566
    iput-boolean p1, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->e:Z

    .line 865567
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 865568
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 865569
    if-eqz v0, :cond_0

    .line 865570
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 865571
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 865572
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 865573
    iget-boolean v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->e:Z

    return v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865507
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->f:Ljava/lang/String;

    .line 865508
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865574
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->g:Ljava/lang/String;

    .line 865575
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLikeSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865576
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->h:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->h:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865577
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->h:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865578
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 865579
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerDoesNotLikeSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865551
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->j:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->j:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865552
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->j:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerLikesSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865580
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->k:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->k:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865581
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->k:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 865534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865535
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 865536
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 865537
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->m()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 865538
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 865539
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->o()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 865540
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->p()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 865541
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 865542
    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->e:Z

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 865543
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 865544
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 865545
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 865546
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 865547
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 865548
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 865549
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865550
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865512
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->m()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865513
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->m()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865514
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->m()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 865515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865516
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->h:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865517
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 865518
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 865519
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 865520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865521
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 865522
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->o()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 865523
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->o()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865524
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->o()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 865525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865526
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->j:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865527
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->p()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 865528
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->p()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865529
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->p()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 865530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865531
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->k:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865532
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865533
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 865510
    new-instance v0, LX/5Br;

    invoke-direct {v0, p1}, LX/5Br;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865509
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 865504
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 865505
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->e:Z

    .line 865506
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 865492
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865493
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 865494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 865495
    iput v2, p2, LX/18L;->c:I

    .line 865496
    :goto_0
    return-void

    .line 865497
    :cond_0
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 865498
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v0

    .line 865499
    if-eqz v0, :cond_1

    .line 865500
    invoke-virtual {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 865501
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 865502
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 865503
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 865489
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865490
    check-cast p2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->a(Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;)V

    .line 865491
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 865478
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 865479
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->a(Z)V

    .line 865480
    :cond_0
    :goto_0
    return-void

    .line 865481
    :cond_1
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865482
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->n()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    move-result-object v0

    .line 865483
    if-eqz v0, :cond_0

    .line 865484
    if-eqz p3, :cond_2

    .line 865485
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 865486
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;->a(I)V

    .line 865487
    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;->i:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    goto :goto_0

    .line 865488
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865475
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;-><init>()V

    .line 865476
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865477
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865474
    const v0, 0x2b70a078

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865473
    const v0, -0x78fb05b

    return v0
.end method
