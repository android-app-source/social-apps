.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/175;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d8be840
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865720
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865721
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865722
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865723
    return-void
.end method

.method private j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAggregatedRanges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 865724
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x13539a61

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->e:LX/3Sb;

    .line 865725
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 865726
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865727
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->j()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 865728
    invoke-virtual {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 865729
    invoke-virtual {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 865730
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 865731
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 865732
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 865733
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 865734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865735
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 865736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865737
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->j()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 865738
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 865739
    if-eqz v1, :cond_0

    .line 865740
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865741
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->e:LX/3Sb;

    .line 865742
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 865743
    invoke-virtual {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 865744
    if-eqz v1, :cond_1

    .line 865745
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    .line 865746
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->f:Ljava/util/List;

    .line 865747
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865748
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865749
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->g:Ljava/lang/String;

    .line 865750
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 865751
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->f:Ljava/util/List;

    .line 865752
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865753
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;-><init>()V

    .line 865754
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865755
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865756
    const v0, 0x2c0cede2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865757
    const v0, -0x726d476c

    return v0
.end method
