.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3b1ac866
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865823
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865822
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865820
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865821
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 865817
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865818
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865819
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865824
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865825
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 865811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865812
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 865813
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 865814
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 865815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865816
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865803
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865804
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865805
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865806
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 865807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;

    .line 865808
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 865809
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865810
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865800
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;-><init>()V

    .line 865801
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865802
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865799
    const v0, 0x36fbb171

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865798
    const v0, -0x467af1b9

    return v0
.end method
