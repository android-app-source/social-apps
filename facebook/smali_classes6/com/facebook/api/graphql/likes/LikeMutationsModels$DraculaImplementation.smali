.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 865255
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 865256
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 865332
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 865333
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 865317
    if-nez p1, :cond_0

    .line 865318
    :goto_0
    return v0

    .line 865319
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 865320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 865321
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 865322
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v2

    .line 865323
    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(III)I

    move-result v3

    .line 865324
    const-class v4, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;

    invoke-virtual {p0, p1, v8, v4}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 865325
    invoke-static {p3, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 865326
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 865327
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 865328
    invoke-virtual {p3, v6, v2, v0}, LX/186;->a(III)V

    .line 865329
    invoke-virtual {p3, v7, v3, v0}, LX/186;->a(III)V

    .line 865330
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 865331
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13539a61
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 865308
    if-nez p0, :cond_0

    move v0, v1

    .line 865309
    :goto_0
    return v0

    .line 865310
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 865311
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 865312
    :goto_1
    if-ge v1, v2, :cond_2

    .line 865313
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 865314
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 865315
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 865316
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 865301
    const/4 v7, 0x0

    .line 865302
    const/4 v1, 0x0

    .line 865303
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 865304
    invoke-static {v2, v3, v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 865305
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 865306
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 865307
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 865300
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 865296
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 865297
    if-eqz v0, :cond_0

    .line 865298
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 865299
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 865291
    packed-switch p2, :pswitch_data_0

    .line 865292
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 865293
    :pswitch_0
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel$AggregatedRangesModel$SampleEntitiesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 865294
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 865295
    return-void

    :pswitch_data_0
    .packed-switch 0x13539a61
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 865290
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 865288
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 865289
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 865283
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 865284
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 865285
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 865286
    iput p2, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->b:I

    .line 865287
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 865282
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 865281
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865278
    iget v0, p0, LX/1vt;->c:I

    .line 865279
    move v0, v0

    .line 865280
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865275
    iget v0, p0, LX/1vt;->c:I

    .line 865276
    move v0, v0

    .line 865277
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 865272
    iget v0, p0, LX/1vt;->b:I

    .line 865273
    move v0, v0

    .line 865274
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865269
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 865270
    move-object v0, v0

    .line 865271
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 865260
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 865261
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 865262
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 865263
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 865264
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 865265
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 865266
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 865267
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/likes/LikeMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 865268
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 865257
    iget v0, p0, LX/1vt;->c:I

    .line 865258
    move v0, v0

    .line 865259
    return v0
.end method
