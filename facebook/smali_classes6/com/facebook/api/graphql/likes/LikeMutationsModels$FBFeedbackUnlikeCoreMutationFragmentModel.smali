.class public final Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x51e822da
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865642
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 865645
    const-class v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865643
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 865644
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedback"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865634
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865635
    iget-object v0, p0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 865636
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865637
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 865638
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 865639
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 865640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865641
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 865625
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865626
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865627
    invoke-direct {p0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->a()Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 865628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;

    .line 865629
    iput-object v0, v1, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    .line 865630
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 865631
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 865621
    new-instance v0, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackUnlikeCoreMutationFragmentModel;-><init>()V

    .line 865622
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 865623
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 865633
    const v0, -0x38dd137a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 865632
    const v0, 0x2eed4b98

    return v0
.end method
