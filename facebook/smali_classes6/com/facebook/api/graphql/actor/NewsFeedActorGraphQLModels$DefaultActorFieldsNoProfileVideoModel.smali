.class public final Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe150991
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 842182
    const-class v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 842183
    const-class v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 842152
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 842153
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842184
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 842185
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 842186
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842191
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->f:Ljava/lang/String;

    .line 842192
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842187
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->g:Ljava/lang/String;

    .line 842188
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842189
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 842190
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 842162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 842163
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 842164
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 842165
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 842166
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 842167
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 842168
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 842169
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 842170
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 842171
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 842172
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 842173
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 842174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 842175
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 842176
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 842177
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 842178
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    .line 842179
    iput-object v0, v1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 842180
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 842181
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 842161
    new-instance v0, LX/57k;

    invoke-direct {v0, p1}, LX/57k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842160
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 842158
    invoke-virtual {p2}, LX/18L;->a()V

    .line 842159
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 842157
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 842154
    new-instance v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;-><init>()V

    .line 842155
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 842156
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 842151
    const v0, -0xca86ac1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 842150
    const v0, 0x3c2b9d5

    return v0
.end method
