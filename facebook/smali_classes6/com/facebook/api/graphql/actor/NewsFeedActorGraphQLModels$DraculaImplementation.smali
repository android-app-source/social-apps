.class public Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 842571
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 842572
    return-void
.end method

.method public constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 842569
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 842570
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 842548
    if-nez p1, :cond_0

    .line 842549
    :goto_0
    return v0

    .line 842550
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 842551
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 842552
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 842553
    const v2, 0x3256388b

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 842554
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 842555
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 842556
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 842557
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 842558
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 842559
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 842560
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 842561
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 842562
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 842563
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 842564
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 842565
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 842566
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 842567
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 842568
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3db5843e -> :sswitch_0
        0x3256388b -> :sswitch_1
        0x355aa7f7 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;
    .locals 3

    .prologue
    .line 842544
    sparse-switch p2, :sswitch_data_0

    .line 842545
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized typeTag:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 842546
    :sswitch_0
    new-instance v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    .line 842547
    :goto_0
    return-object v0

    :sswitch_1
    new-instance v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaPersistableImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaPersistableImplementation;-><init>(LX/15i;II)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3db5843e -> :sswitch_0
        0x3256388b -> :sswitch_1
        0x355aa7f7 -> :sswitch_0
    .end sparse-switch
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 842539
    sparse-switch p2, :sswitch_data_0

    .line 842540
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 842541
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 842542
    const v1, 0x3256388b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 842543
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3db5843e -> :sswitch_0
        0x3256388b -> :sswitch_1
        0x355aa7f7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static b(LX/15i;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 842536
    packed-switch p2, :pswitch_data_0

    .line 842537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized typeTag:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 842538
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x3256388b
        :pswitch_0
    .end packed-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 842520
    if-eqz p1, :cond_0

    .line 842521
    invoke-static {p0, p1, p2}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 842522
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;

    .line 842523
    if-eq v0, v1, :cond_0

    .line 842524
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 842525
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 842535
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 842533
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 842534
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 842528
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 842529
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 842530
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 842531
    iput p2, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->b:I

    .line 842532
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 842527
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 842526
    new-instance v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 842517
    iget v0, p0, LX/1vt;->c:I

    .line 842518
    move v0, v0

    .line 842519
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 842514
    iget v0, p0, LX/1vt;->c:I

    .line 842515
    move v0, v0

    .line 842516
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 842511
    iget v0, p0, LX/1vt;->b:I

    .line 842512
    move v0, v0

    .line 842513
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842508
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 842509
    move-object v0, v0

    .line 842510
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 842499
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 842500
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 842501
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 842502
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 842503
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 842504
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 842505
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 842506
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 842507
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 842496
    iget v0, p0, LX/1vt;->c:I

    .line 842497
    move v0, v0

    .line 842498
    return v0
.end method
