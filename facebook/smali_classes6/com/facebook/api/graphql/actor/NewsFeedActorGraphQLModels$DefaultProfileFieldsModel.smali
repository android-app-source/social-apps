.class public final Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1y7;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x32360437
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 842463
    const-class v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 842464
    const-class v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 842465
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 842466
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842467
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 842468
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 842469
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842470
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 842471
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 842472
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 842473
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 842474
    invoke-virtual {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 842475
    invoke-virtual {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 842476
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 842477
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 842478
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 842479
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 842480
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 842481
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 842482
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 842483
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 842484
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 842485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 842486
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 842487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 842488
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 842489
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 842490
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 842491
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;

    .line 842492
    iput-object v0, v1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 842493
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 842494
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 842495
    new-instance v0, LX/57n;

    invoke-direct {v0, p1}, LX/57n;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842457
    invoke-virtual {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 842458
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 842459
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->g:Z

    .line 842460
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->h:Z

    .line 842461
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->i:Z

    .line 842462
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 842455
    invoke-virtual {p2}, LX/18L;->a()V

    .line 842456
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 842454
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 842451
    new-instance v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;-><init>()V

    .line 842452
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 842453
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842450
    invoke-direct {p0}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 842449
    const v0, 0x7d694c74

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842447
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->f:Ljava/lang/String;

    .line 842448
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 842446
    const v0, 0x50c72189

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 842444
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 842445
    iget-boolean v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->h:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 842442
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 842443
    iget-boolean v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->i:Z

    return v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 842440
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->j:Ljava/lang/String;

    .line 842441
    iget-object v0, p0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultProfileFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method
