.class public final Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 842342
    const-class v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 842343
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 842311
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 842313
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 842314
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 842315
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 842316
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 842317
    if-eqz v2, :cond_0

    .line 842318
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842319
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 842320
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 842321
    if-eqz v2, :cond_1

    .line 842322
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842323
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842324
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 842325
    if-eqz v2, :cond_2

    .line 842326
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842327
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842328
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842329
    if-eqz v2, :cond_3

    .line 842330
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842331
    invoke-static {v1, v2, p1}, LX/57s;->a(LX/15i;ILX/0nX;)V

    .line 842332
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 842333
    if-eqz v2, :cond_4

    .line 842334
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842335
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842336
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842337
    if-eqz v2, :cond_5

    .line 842338
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842339
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 842340
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 842341
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 842312
    check-cast p1, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultLikersProfileFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
