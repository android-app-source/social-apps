.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7385611c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875307
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875306
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 875304
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 875305
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875302
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    .line 875303
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 875294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875295
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 875296
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 875297
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 875298
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 875299
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 875300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875301
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 875291
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875293
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 875308
    new-instance v0, LX/5Dl;

    invoke-direct {v0, p1}, LX/5Dl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875290
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 875286
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 875287
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->f:Z

    .line 875288
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;->g:Z

    .line 875289
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 875284
    invoke-virtual {p2}, LX/18L;->a()V

    .line 875285
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 875283
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 875278
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel$CharityInterfaceModel$PageModel;-><init>()V

    .line 875279
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 875280
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 875282
    const v0, 0x44978bc2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 875281
    const v0, 0x25d6af

    return v0
.end method
