.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 875465
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 875466
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 875467
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 875468
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 875469
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 875470
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 875471
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875472
    if-eqz v2, :cond_0

    .line 875473
    const-string p0, "campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875474
    invoke-static {v1, v2, p1, p2}, LX/5FC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875475
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 875476
    if-eqz v2, :cond_1

    .line 875477
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875478
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 875479
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 875480
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 875481
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
