.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6cb8b861
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881341
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881342
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 881343
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 881344
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881345
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    .line 881346
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideoBroadcastSchedule"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881347
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881348
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 881349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881350
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 881351
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 881352
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 881353
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 881354
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 881355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881356
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 881357
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881358
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881359
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    .line 881360
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 881361
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;

    .line 881362
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    .line 881363
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 881364
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881365
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 881366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;

    .line 881367
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881368
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881369
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 881370
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel;-><init>()V

    .line 881371
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 881372
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 881373
    const v0, -0x1b5665aa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 881374
    const v0, 0x602953b6

    return v0
.end method
