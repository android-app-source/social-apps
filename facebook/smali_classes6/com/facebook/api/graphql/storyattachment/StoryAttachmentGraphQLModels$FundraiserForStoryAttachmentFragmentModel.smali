.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/57w;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7bb3dfab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875240
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875239
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 875237
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 875238
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875235
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 875236
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875233
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    .line 875234
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875231
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    .line 875232
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    return-object v0
.end method

.method private n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875229
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->i:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->i:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 875230
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->i:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    return-object v0
.end method


# virtual methods
.method public final synthetic A()LX/585;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875228
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875227
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875241
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k:Ljava/lang/String;

    .line 875242
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 875209
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875210
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 875211
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 875212
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 875213
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 875214
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 875215
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 875216
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 875217
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 875218
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 875219
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 875220
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 875221
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 875222
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 875223
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 875224
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 875225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875226
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 875186
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875187
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 875188
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 875189
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 875190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 875191
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 875192
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 875193
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    .line 875194
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 875195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 875196
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    .line 875197
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 875198
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    .line 875199
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 875200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 875201
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$DonorsSocialContextTextModel;

    .line 875202
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 875203
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 875204
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->n()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 875205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 875206
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->i:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 875207
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875208
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875185
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 875182
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;-><init>()V

    .line 875183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 875184
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 875181
    const v0, 0x278cca89

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 875180
    const v0, 0x291507f7

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875178
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j:Ljava/lang/String;

    .line 875179
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic x()LX/176;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875177
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()LX/584;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875176
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$CharityInterfaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875174
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->g:Ljava/lang/String;

    .line 875175
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method
