.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 882296
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 882297
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 882316
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 882299
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 882300
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 882301
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 882302
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 882303
    if-eqz v2, :cond_0

    .line 882304
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882305
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882306
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882307
    if-eqz v2, :cond_1

    .line 882308
    const-string p0, "rating"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882309
    invoke-static {v1, v2, p1}, LX/5Ge;->a(LX/15i;ILX/0nX;)V

    .line 882310
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 882311
    if-eqz v2, :cond_2

    .line 882312
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882313
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882314
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 882315
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 882298
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$StoryAttachmentAppAdLinkTargetModel;LX/0nX;LX/0my;)V

    return-void
.end method
