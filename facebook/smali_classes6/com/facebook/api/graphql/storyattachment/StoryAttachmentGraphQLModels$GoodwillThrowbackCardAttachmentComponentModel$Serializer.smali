.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 875591
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 875592
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 875590
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 875557
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 875558
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 875559
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 875560
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875561
    if-eqz v2, :cond_0

    .line 875562
    const-string p0, "accent_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875563
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 875564
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875565
    if-eqz v2, :cond_1

    .line 875566
    const-string p0, "action_links"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875567
    invoke-static {v1, v2, p1, p2}, LX/5FD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875568
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875569
    if-eqz v2, :cond_2

    .line 875570
    const-string p0, "additional_accent_images"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875571
    invoke-static {v1, v2, p1, p2}, LX/5FE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875572
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875573
    if-eqz v2, :cond_3

    .line 875574
    const-string p0, "data_points"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875575
    invoke-static {v1, v2, p1, p2}, LX/5FG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875576
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875577
    if-eqz v2, :cond_4

    .line 875578
    const-string p0, "throwback_media"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875579
    invoke-static {v1, v2, p1, p2}, LX/5lp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875580
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875581
    if-eqz v2, :cond_5

    .line 875582
    const-string p0, "throwback_media_attachments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875583
    invoke-static {v1, v2, p1, p2}, LX/5Gg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 875584
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 875585
    if-eqz v2, :cond_6

    .line 875586
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 875587
    invoke-static {v1, v2, p1}, LX/5FH;->a(LX/15i;ILX/0nX;)V

    .line 875588
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 875589
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 875556
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;LX/0nX;LX/0my;)V

    return-void
.end method
