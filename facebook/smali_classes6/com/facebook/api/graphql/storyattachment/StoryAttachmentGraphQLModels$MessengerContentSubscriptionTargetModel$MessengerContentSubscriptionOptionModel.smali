.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5d78c3db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877509
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877508
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 877506
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 877507
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877504
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->e:Ljava/lang/String;

    .line 877505
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877502
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    .line 877503
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877510
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->g:Ljava/lang/String;

    .line 877511
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 877492
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877493
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 877494
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 877495
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 877496
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 877497
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 877498
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 877499
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 877500
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877501
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 877484
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877485
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 877486
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    .line 877487
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 877488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 877489
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel$PageModel;

    .line 877490
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877491
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 877479
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;-><init>()V

    .line 877480
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 877481
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 877483
    const v0, -0x38ced82e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 877482
    const v0, 0x57029498

    return v0
.end method
