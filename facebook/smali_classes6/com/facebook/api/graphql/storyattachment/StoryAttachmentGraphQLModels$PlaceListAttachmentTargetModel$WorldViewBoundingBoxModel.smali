.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x40239636
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:D

.field private h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 878919
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 878918
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 878916
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 878917
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 878914
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 878915
    iget-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 878906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 878907
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 878908
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 878909
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 878910
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->g:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 878911
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 878912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 878913
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 878903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 878904
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 878905
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 878920
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 878921
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->e:D

    .line 878922
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->f:D

    .line 878923
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->g:D

    .line 878924
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->h:D

    .line 878925
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 878901
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 878902
    iget-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 878898
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;-><init>()V

    .line 878899
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 878900
    return-object v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 878896
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 878897
    iget-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->g:D

    return-wide v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 878894
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 878895
    iget-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->h:D

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 878893
    const v0, -0x2c4b6662

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 878892
    const v0, -0x7960c2c2

    return v0
.end method
