.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4444fcba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881940
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881939
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 881945
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 881946
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881941
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 881942
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881943
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    .line 881944
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881926
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 881927
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 881928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881929
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 881930
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 881931
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 881932
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 881933
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 881934
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 881935
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 881936
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 881937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881938
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 881909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881910
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881911
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    .line 881912
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 881913
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    .line 881914
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel$SouvenirMediaModel;

    .line 881915
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881916
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881917
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 881918
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 881919
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;->f:Z

    .line 881920
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 881921
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;-><init>()V

    .line 881922
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 881923
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 881925
    const v0, -0x79f5e0ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 881924
    const v0, 0x684f0b47

    return v0
.end method
