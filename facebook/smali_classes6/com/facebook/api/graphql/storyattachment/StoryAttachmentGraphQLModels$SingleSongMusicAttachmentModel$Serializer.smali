.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 881625
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 881626
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 881628
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 881629
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 881630
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 881631
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 881632
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 881633
    if-eqz v2, :cond_0

    .line 881634
    const-string v3, "application_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881635
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 881636
    :cond_0
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 881637
    if-eqz v2, :cond_1

    .line 881638
    const-string v2, "artist_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881639
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 881640
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 881641
    if-eqz v2, :cond_2

    .line 881642
    const-string v3, "audio_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881643
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 881644
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 881645
    if-eqz v2, :cond_3

    .line 881646
    const-string v2, "collection_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881647
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 881648
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 881649
    if-eqz v2, :cond_4

    .line 881650
    const-string v3, "cover_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881651
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 881652
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 881653
    if-eqz v2, :cond_5

    .line 881654
    const-string v3, "duration_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881655
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 881656
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 881657
    if-eqz v2, :cond_6

    .line 881658
    const-string v3, "global_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881659
    invoke-static {v1, v2, p1, p2}, LX/5GW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 881660
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 881661
    if-eqz v2, :cond_7

    .line 881662
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881663
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 881664
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 881665
    if-eqz v2, :cond_8

    .line 881666
    const-string v3, "music_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881667
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 881668
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 881669
    if-eqz v2, :cond_9

    .line 881670
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 881671
    invoke-static {v1, v2, p1}, LX/5GX;->a(LX/15i;ILX/0nX;)V

    .line 881672
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 881673
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 881627
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
