.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 880979
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 880980
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 880985
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 880982
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 880983
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/5GQ;->a(LX/15i;ILX/0nX;)V

    .line 880984
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 880981
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$BroadcasterModel;LX/0nX;LX/0my;)V

    return-void
.end method
