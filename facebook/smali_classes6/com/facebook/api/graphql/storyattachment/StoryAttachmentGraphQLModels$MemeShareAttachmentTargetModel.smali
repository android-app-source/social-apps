.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd1d48e1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877379
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877378
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 877376
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 877377
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877374
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 877375
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877380
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    .line 877381
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 877348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877349
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 877350
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 877351
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 877352
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 877353
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 877354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877355
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 877361
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877362
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 877363
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 877364
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 877365
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;

    .line 877366
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 877367
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 877368
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    .line 877369
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 877370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;

    .line 877371
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$OwnerModel;

    .line 877372
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877373
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 877358
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel;-><init>()V

    .line 877359
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 877360
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 877357
    const v0, -0x63b77a7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 877356
    const v0, 0x4984e12

    return v0
.end method
