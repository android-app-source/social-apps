.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32413840
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 876333
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 876336
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 876334
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 876335
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 876330
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 876331
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 876332
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 876328
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->f:Ljava/lang/String;

    .line 876329
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 876337
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    .line 876338
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 876318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 876319
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 876320
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 876321
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 876322
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 876323
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 876324
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 876325
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 876326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 876327
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 876310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 876311
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 876312
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    .line 876313
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 876314
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;

    .line 876315
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->g:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel$MediaModel;

    .line 876316
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 876317
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 876309
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 876304
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GroupMemberAddedAttachmentStyleInfoFieldsModel$GroupModel$GroupMediasetModel;-><init>()V

    .line 876305
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 876306
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 876308
    const v0, -0x3c0274d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 876307
    const v0, -0x31d68202

    return v0
.end method
