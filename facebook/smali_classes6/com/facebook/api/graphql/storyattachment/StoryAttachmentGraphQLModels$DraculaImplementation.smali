.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 873551
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 873552
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 873252
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 873253
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 873254
    if-nez p1, :cond_0

    .line 873255
    const/4 v0, 0x0

    .line 873256
    :goto_0
    return v0

    .line 873257
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 873258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 873259
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873260
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873261
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873262
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873263
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 873264
    :sswitch_1
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873265
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 873266
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 873267
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 873268
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 873269
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873270
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873271
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873272
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873273
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 873274
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873275
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873276
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873277
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873278
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 873279
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873280
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873281
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873282
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873283
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873284
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873285
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873286
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873287
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873288
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873289
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873290
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873291
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873292
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873293
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873294
    :sswitch_6
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 873295
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873296
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873297
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 873298
    const/4 v1, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v1, v4}, LX/15i;->a(III)I

    move-result v7

    .line 873299
    const/4 v1, 0x4

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873300
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v1, v0, v4}, LX/186;->a(III)V

    .line 873301
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873302
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 873303
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 873304
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873305
    :sswitch_7
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 873306
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873307
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873308
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 873309
    const/4 v1, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v1, v4}, LX/15i;->a(III)I

    move-result v7

    .line 873310
    const/4 v1, 0x4

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873311
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v1, v0, v4}, LX/186;->a(III)V

    .line 873312
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873313
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 873314
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 873315
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873316
    :sswitch_8
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 873317
    const v1, 0x41dc040c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 873318
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873319
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873320
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873321
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873322
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873323
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873324
    :sswitch_9
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 873325
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 873326
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873327
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873328
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873329
    :sswitch_a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873330
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873331
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873332
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873333
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873334
    :sswitch_b
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873335
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873336
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873337
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873338
    const/4 v2, 0x3

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873339
    const/4 v2, 0x1

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873340
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873341
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873342
    :sswitch_c
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 873343
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873344
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873345
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 873346
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 873347
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v0, v4}, LX/186;->a(III)V

    .line 873348
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873349
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 873350
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873351
    :sswitch_d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 873352
    const v1, -0x32349f31

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 873353
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873354
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873355
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873356
    :sswitch_e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 873357
    const v1, -0x58aef037

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 873358
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 873359
    const v2, 0x4cab74cd    # 8.9892456E7f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 873360
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873361
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873362
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873363
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873364
    :sswitch_f
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873365
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873366
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873367
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873368
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873369
    :sswitch_10
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873370
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873371
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873372
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873373
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873374
    :sswitch_11
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873375
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873376
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873377
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873378
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873379
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873380
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873381
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873382
    :sswitch_12
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 873383
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 873384
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873385
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873386
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873387
    :sswitch_13
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873388
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873389
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873390
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873391
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873392
    :sswitch_14
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873393
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873394
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873395
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873396
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873397
    :sswitch_15
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873398
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873399
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 873400
    const v2, -0x6624280a

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 873401
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873402
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873403
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873404
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873405
    :sswitch_16
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873406
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873407
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 873408
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873409
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873410
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 873411
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 873412
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873413
    :sswitch_17
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873414
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 873415
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 873416
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873417
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873418
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873419
    :sswitch_18
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873420
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873421
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873422
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873423
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873424
    :sswitch_19
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873425
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873426
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873427
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873428
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873429
    :sswitch_1a
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873430
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 873431
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873432
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873433
    :sswitch_1b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873434
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873435
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873436
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873437
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873438
    :sswitch_1c
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873439
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 873440
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 873441
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 873442
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 873443
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873444
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873445
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873446
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873447
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873448
    :sswitch_1d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 873449
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 873450
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 873451
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->a(IZ)V

    .line 873452
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 873453
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873454
    :sswitch_1e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873455
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873456
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873457
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873458
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873459
    :sswitch_1f
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873460
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873461
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873462
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873463
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873464
    :sswitch_20
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 873465
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873466
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 873467
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873468
    :sswitch_21
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873469
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873470
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873471
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873472
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873473
    :sswitch_22
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873474
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873475
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873476
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873477
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873478
    :sswitch_23
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873479
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 873480
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873481
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873482
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873483
    :sswitch_24
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 873484
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873485
    const/4 v1, 0x2

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 873486
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v1, v0, v4}, LX/186;->a(III)V

    .line 873487
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873488
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873489
    :sswitch_25
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873490
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 873491
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873492
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 873493
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 873494
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 873495
    const/4 v0, 0x3

    const-class v4, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 873496
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 873497
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 873498
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v1}, LX/186;->b(II)V

    .line 873499
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 873500
    const/4 v1, 0x2

    invoke-virtual {p3, v1, v3}, LX/186;->b(II)V

    .line 873501
    const/4 v1, 0x3

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 873502
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 873503
    :sswitch_26
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 873504
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 873505
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 873506
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 873507
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 873508
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873509
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873510
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873511
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 873512
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x75f4fdeb -> :sswitch_4
        -0x6ff1a2f0 -> :sswitch_a
        -0x67a9e046 -> :sswitch_8
        -0x6624280a -> :sswitch_16
        -0x5cda772c -> :sswitch_b
        -0x5cb8b36c -> :sswitch_24
        -0x58aef037 -> :sswitch_f
        -0x5717891a -> :sswitch_19
        -0x40d35855 -> :sswitch_1d
        -0x3b404242 -> :sswitch_5
        -0x32349f31 -> :sswitch_e
        -0x1fe53beb -> :sswitch_1c
        -0x13519e79 -> :sswitch_c
        -0xa562a08 -> :sswitch_23
        -0x6e52f25 -> :sswitch_18
        -0x6b3064f -> :sswitch_1b
        -0x6a839f8 -> :sswitch_17
        -0x6523493 -> :sswitch_12
        -0x62371dc -> :sswitch_25
        -0x481f8bd -> :sswitch_d
        0x2a452b9 -> :sswitch_11
        0x7601011 -> :sswitch_2
        0xb8c0a2e -> :sswitch_14
        0xe7b5c41 -> :sswitch_7
        0x1384d012 -> :sswitch_1f
        0x152cb227 -> :sswitch_20
        0x180f02e8 -> :sswitch_26
        0x233590b5 -> :sswitch_21
        0x2a7a7ee7 -> :sswitch_3
        0x3ec8d68c -> :sswitch_0
        0x40fcdb3e -> :sswitch_22
        0x4c6172a8 -> :sswitch_15
        0x4cab74cd -> :sswitch_10
        0x58061f2a -> :sswitch_1a
        0x5c4eae25 -> :sswitch_13
        0x5c96732c -> :sswitch_1
        0x749916b7 -> :sswitch_6
        0x78150a2d -> :sswitch_1e
        0x7deccd33 -> :sswitch_9
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 873513
    if-nez p0, :cond_0

    move v0, v1

    .line 873514
    :goto_0
    return v0

    .line 873515
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 873516
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 873517
    :goto_1
    if-ge v1, v2, :cond_2

    .line 873518
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 873519
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 873520
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 873521
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 873239
    const/4 v7, 0x0

    .line 873240
    const/4 v1, 0x0

    .line 873241
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 873242
    invoke-static {v2, v3, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 873243
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 873244
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 873245
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 873522
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 873523
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 873524
    if-eqz v0, :cond_0

    .line 873525
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 873526
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 873527
    if-eqz p0, :cond_0

    .line 873528
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 873529
    if-eq v0, p0, :cond_0

    .line 873530
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 873531
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 873532
    sparse-switch p2, :sswitch_data_0

    .line 873533
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 873534
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 873535
    const v1, 0x41dc040c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 873536
    :goto_0
    :sswitch_1
    return-void

    .line 873537
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 873538
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 873539
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 873540
    const v1, -0x32349f31

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 873541
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 873542
    const v1, -0x58aef037

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 873543
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 873544
    const v1, 0x4cab74cd    # 8.9892456E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 873545
    :sswitch_5
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 873546
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 873547
    :sswitch_6
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 873548
    const v1, -0x6624280a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 873549
    :sswitch_7
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 873550
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x75f4fdeb -> :sswitch_1
        -0x6ff1a2f0 -> :sswitch_1
        -0x67a9e046 -> :sswitch_0
        -0x6624280a -> :sswitch_1
        -0x5cda772c -> :sswitch_1
        -0x5cb8b36c -> :sswitch_1
        -0x58aef037 -> :sswitch_1
        -0x5717891a -> :sswitch_1
        -0x40d35855 -> :sswitch_1
        -0x3b404242 -> :sswitch_1
        -0x32349f31 -> :sswitch_4
        -0x1fe53beb -> :sswitch_1
        -0x13519e79 -> :sswitch_1
        -0xa562a08 -> :sswitch_1
        -0x6e52f25 -> :sswitch_1
        -0x6b3064f -> :sswitch_1
        -0x6a839f8 -> :sswitch_1
        -0x6523493 -> :sswitch_5
        -0x62371dc -> :sswitch_7
        -0x481f8bd -> :sswitch_3
        0x2a452b9 -> :sswitch_1
        0x7601011 -> :sswitch_1
        0xb8c0a2e -> :sswitch_1
        0xe7b5c41 -> :sswitch_1
        0x1384d012 -> :sswitch_1
        0x152cb227 -> :sswitch_1
        0x180f02e8 -> :sswitch_1
        0x233590b5 -> :sswitch_1
        0x2a7a7ee7 -> :sswitch_1
        0x3ec8d68c -> :sswitch_1
        0x40fcdb3e -> :sswitch_1
        0x4c6172a8 -> :sswitch_6
        0x4cab74cd -> :sswitch_1
        0x58061f2a -> :sswitch_1
        0x5c4eae25 -> :sswitch_1
        0x5c96732c -> :sswitch_1
        0x749916b7 -> :sswitch_1
        0x78150a2d -> :sswitch_1
        0x7deccd33 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 873553
    if-nez p1, :cond_0

    move v0, v1

    .line 873554
    :goto_0
    return v0

    .line 873555
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 873556
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 873557
    :goto_1
    if-ge v1, v2, :cond_2

    .line 873558
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 873559
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 873560
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 873561
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 873562
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 873563
    if-eqz p1, :cond_0

    .line 873564
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 873565
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 873566
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 873567
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 873568
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 873569
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 873246
    if-eqz p1, :cond_0

    .line 873247
    invoke-static {p0, p1, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 873248
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    .line 873249
    if-eq v0, v1, :cond_0

    .line 873250
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 873251
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 873205
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 873206
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 873207
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 873234
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 873235
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 873236
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 873237
    iput p2, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->b:I

    .line 873238
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 873208
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 873209
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 873210
    iget v0, p0, LX/1vt;->c:I

    .line 873211
    move v0, v0

    .line 873212
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 873213
    iget v0, p0, LX/1vt;->c:I

    .line 873214
    move v0, v0

    .line 873215
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 873216
    iget v0, p0, LX/1vt;->b:I

    .line 873217
    move v0, v0

    .line 873218
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 873219
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 873220
    move-object v0, v0

    .line 873221
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 873222
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 873223
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 873224
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 873225
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 873226
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 873227
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 873228
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 873229
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 873230
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 873231
    iget v0, p0, LX/1vt;->c:I

    .line 873232
    move v0, v0

    .line 873233
    return v0
.end method
