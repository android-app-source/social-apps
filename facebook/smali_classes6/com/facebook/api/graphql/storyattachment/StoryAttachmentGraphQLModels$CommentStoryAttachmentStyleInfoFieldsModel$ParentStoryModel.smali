.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xebe3954
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872567
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872566
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 872564
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 872565
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872562
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    .line 872563
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872560
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->f:Ljava/lang/String;

    .line 872561
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 872552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872553
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 872554
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 872555
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 872556
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 872557
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 872558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872559
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 872534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872535
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 872536
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    .line 872537
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 872538
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;

    .line 872539
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel$FeedbackModel;

    .line 872540
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872541
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 872551
    new-instance v0, LX/5Db;

    invoke-direct {v0, p1}, LX/5Db;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872550
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 872548
    invoke-virtual {p2}, LX/18L;->a()V

    .line 872549
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 872547
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 872544
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentStyleInfoFieldsModel$ParentStoryModel;-><init>()V

    .line 872545
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 872546
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 872543
    const v0, 0x2818d5e3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 872542
    const v0, 0x4c808d5

    return v0
.end method
