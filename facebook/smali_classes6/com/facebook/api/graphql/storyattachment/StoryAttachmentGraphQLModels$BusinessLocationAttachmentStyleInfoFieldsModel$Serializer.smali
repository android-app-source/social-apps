.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 872052
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 872053
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 872051
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 872025
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 872026
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 872027
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 872028
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 872029
    if-eqz v2, :cond_0

    .line 872030
    const-string p0, "bounding_box"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 872031
    invoke-static {v1, v2, p1}, LX/5EZ;->a(LX/15i;ILX/0nX;)V

    .line 872032
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 872033
    if-eqz v2, :cond_1

    .line 872034
    const-string p0, "label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 872035
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 872036
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 872037
    if-eqz v2, :cond_2

    .line 872038
    const-string p0, "location"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 872039
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 872040
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 872041
    if-eqz v2, :cond_3

    .line 872042
    const-string p0, "logo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 872043
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 872044
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 872045
    if-eqz v2, :cond_4

    .line 872046
    const-string p0, "nearby_locations"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 872047
    invoke-static {v1, v2, p1, p2}, LX/4aX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 872048
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 872049
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 872050
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$BusinessLocationAttachmentStyleInfoFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
