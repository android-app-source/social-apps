.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5dc65d13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875714
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 875713
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 875711
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 875712
    return-void
.end method

.method private a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875709
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 875710
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLinks"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 875707
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, -0x5cda772c

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->f:LX/3Sb;

    .line 875708
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdditionalAccentImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 875705
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, -0x13519e79

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->g:LX/3Sb;

    .line 875706
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDataPoints"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875703
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 875704
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 875715
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->i:Ljava/util/List;

    .line 875716
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThrowbackMediaAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 875633
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j:Ljava/util/List;

    .line 875634
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 875701
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    .line 875702
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 875635
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875636
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 875637
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 875638
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 875639
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x481f8bd

    invoke-static {v4, v3, v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 875640
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 875641
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->n()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 875642
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->o()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 875643
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 875644
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 875645
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 875646
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 875647
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 875648
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 875649
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 875650
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 875651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 875653
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 875654
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 875655
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 875656
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 875657
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875658
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 875659
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 875660
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 875661
    if-eqz v2, :cond_1

    .line 875662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875663
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->f:LX/3Sb;

    move-object v1, v0

    .line 875664
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 875665
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 875666
    if-eqz v2, :cond_2

    .line 875667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875668
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->g:LX/3Sb;

    move-object v1, v0

    .line 875669
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 875670
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x481f8bd

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 875671
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 875672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875673
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->h:I

    move-object v1, v0

    .line 875674
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 875675
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 875676
    if-eqz v2, :cond_4

    .line 875677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875678
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->i:Ljava/util/List;

    move-object v1, v0

    .line 875679
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 875680
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 875681
    if-eqz v2, :cond_5

    .line 875682
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875683
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 875684
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->o()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 875685
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->o()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    .line 875686
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->o()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 875687
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    .line 875688
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$TitleModel;

    .line 875689
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 875690
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 875691
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move-object p0, v1

    .line 875692
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 875693
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 875694
    const/4 v0, 0x3

    const v1, -0x481f8bd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;->h:I

    .line 875695
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 875696
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;-><init>()V

    .line 875697
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 875698
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 875699
    const v0, -0x6606c437

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 875700
    const v0, 0x206c8f8e

    return v0
.end method
