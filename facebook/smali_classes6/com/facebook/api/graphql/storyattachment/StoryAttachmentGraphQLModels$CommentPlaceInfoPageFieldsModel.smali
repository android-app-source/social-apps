.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5CI;
.implements LX/5CH;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2d0f6374
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Z

.field private v:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872368
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872318
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 872319
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 872320
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 872321
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 872322
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 872323
    return-void
.end method

.method private A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872324
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 872325
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872326
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 872327
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    return-object v0
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 872328
    if-nez p0, :cond_0

    .line 872329
    const/4 p0, 0x0

    .line 872330
    :goto_0
    return-object p0

    .line 872331
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    if-eqz v0, :cond_1

    .line 872332
    check-cast p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    goto :goto_0

    .line 872333
    :cond_1
    new-instance v1, LX/5DY;

    invoke-direct {v1}, LX/5DY;-><init>()V

    .line 872334
    invoke-interface {p0}, LX/5CH;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 872335
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v1, LX/5DY;->b:LX/15i;

    iput v0, v1, LX/5DY;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872336
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->d:Ljava/lang/String;

    .line 872337
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 872338
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p0}, LX/5CH;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 872339
    invoke-interface {p0}, LX/5CH;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 872340
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 872341
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 872342
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->e:LX/0Px;

    .line 872343
    invoke-interface {p0}, LX/5CH;->d()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->f:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 872344
    invoke-interface {p0}, LX/5CH;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->g:Ljava/lang/String;

    .line 872345
    invoke-interface {p0}, LX/5CH;->ae_()Z

    move-result v0

    iput-boolean v0, v1, LX/5DY;->h:Z

    .line 872346
    invoke-interface {p0}, LX/5CH;->af_()LX/1k1;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a(LX/1k1;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 872347
    invoke-interface {p0}, LX/5CH;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->j:Ljava/lang/String;

    .line 872348
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v1, LX/5DY;->k:LX/15i;

    iput v0, v1, LX/5DY;->l:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 872349
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iput-object v2, v1, LX/5DY;->m:LX/15i;

    iput v0, v1, LX/5DY;->n:I

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 872350
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->o:Ljava/lang/String;

    .line 872351
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->p:Ljava/lang/String;

    .line 872352
    invoke-interface {p0}, LX/5CH;->k()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 872353
    invoke-interface {p0}, LX/5CH;->l()Z

    move-result v0

    iput-boolean v0, v1, LX/5DY;->r:Z

    .line 872354
    invoke-interface {p0}, LX/5CH;->m()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->a(Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;)Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->s:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 872355
    invoke-interface {p0}, LX/5CH;->n()Z

    move-result v0

    iput-boolean v0, v1, LX/5DY;->t:Z

    .line 872356
    invoke-interface {p0}, LX/5CH;->o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->u:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 872357
    invoke-interface {p0}, LX/5CH;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->v:Ljava/lang/String;

    .line 872358
    invoke-interface {p0}, LX/5CH;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, v1, LX/5DY;->w:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 872359
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iput-object v2, v1, LX/5DY;->x:LX/15i;

    iput v0, v1, LX/5DY;->y:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 872360
    invoke-virtual {v1}, LX/5DY;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    move-result-object p0

    goto/16 :goto_0

    .line 872361
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 872362
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 872363
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method private y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872364
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->i:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->i:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 872365
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->i:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872366
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 872367
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 23

    .prologue
    .line 872270
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872271
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 872272
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x7601011

    invoke-static {v5, v4, v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 872273
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 872274
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->c()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 872275
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 872276
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 872277
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 872278
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->j()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 872279
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    const v13, 0x34c84503

    invoke-static {v12, v11, v13}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 872280
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    const v14, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v13, v12, v14}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 872281
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 872282
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 872283
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 872284
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 872285
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 872286
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->p()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 872287
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 872288
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, LX/1vs;->b:I

    move/from16 v20, v0

    const v22, 0x471b9538

    move-object/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 872289
    const/16 v21, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 872290
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 872291
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 872292
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 872293
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 872294
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 872295
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 872296
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 872297
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 872298
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 872299
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 872300
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 872301
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 872302
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 872303
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 872304
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 872305
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 872306
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 872307
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 872308
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 872309
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 872310
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 872311
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872312
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 872369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872370
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 872371
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7601011

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872372
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 872373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872374
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->f:I

    move-object v1, v0

    .line 872375
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 872376
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 872377
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 872378
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872379
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->i:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 872380
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 872381
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 872382
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 872383
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872384
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 872385
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 872386
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x34c84503

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 872387
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 872388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872389
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->n:I

    move-object v1, v0

    .line 872390
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 872391
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 872392
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 872393
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872394
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->o:I

    move-object v1, v0

    .line 872395
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 872396
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 872397
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 872398
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872399
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 872400
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 872401
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 872402
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 872403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872404
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 872405
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 872406
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x471b9538

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 872407
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 872408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 872409
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y:I

    move-object v1, v0

    .line 872410
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872411
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 872412
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 872413
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 872414
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 872415
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :cond_8
    move-object p0, v1

    .line 872416
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 872417
    new-instance v0, LX/5DZ;

    invoke-direct {v0, p1}, LX/5DZ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872418
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 872419
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 872420
    const/4 v0, 0x1

    const v1, 0x7601011

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->f:I

    .line 872421
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->k:Z

    .line 872422
    const/16 v0, 0x9

    const v1, 0x34c84503

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->n:I

    .line 872423
    const/16 v0, 0xa

    const v1, -0x33df0b5b    # -4.2193556E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->o:I

    .line 872424
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s:Z

    .line 872425
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u:Z

    .line 872426
    const/16 v0, 0x14

    const v1, 0x471b9538

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y:I

    .line 872427
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 872428
    invoke-virtual {p2}, LX/18L;->a()V

    .line 872429
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 872430
    return-void
.end method

.method public final ae_()Z
    .locals 2

    .prologue
    .line 872431
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872432
    iget-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->k:Z

    return v0
.end method

.method public final synthetic af_()LX/1k1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872433
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872434
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 872435
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 872436
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 872313
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;-><init>()V

    .line 872314
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 872315
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 872316
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->h:Ljava/util/List;

    .line 872317
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872237
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 872238
    const v0, 0x2f420b8f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872239
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->j:Ljava/lang/String;

    .line 872240
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 872241
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872242
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->m:Ljava/lang/String;

    .line 872243
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872244
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 872245
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872246
    iget-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s:Z

    return v0
.end method

.method public final synthetic m()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872247
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->B()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 872248
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872249
    iget-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u:Z

    return v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872250
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 872251
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872252
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w:Ljava/lang/String;

    .line 872253
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872254
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 872255
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOverallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 872256
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872257
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872258
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872259
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872260
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872261
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872262
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->g:Ljava/lang/String;

    .line 872263
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872264
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->p:Ljava/lang/String;

    .line 872265
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872266
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->q:Ljava/lang/String;

    .line 872267
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 872268
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 872269
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
