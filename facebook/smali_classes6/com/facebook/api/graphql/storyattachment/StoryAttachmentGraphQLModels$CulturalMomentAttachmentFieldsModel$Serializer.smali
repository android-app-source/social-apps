.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 873056
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 873057
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 873009
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 873010
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 873011
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 873012
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 873013
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 873014
    if-eqz v2, :cond_0

    .line 873015
    const-string p0, "action_button_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873016
    invoke-static {v1, v2, p1}, LX/5Ei;->a(LX/15i;ILX/0nX;)V

    .line 873017
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873018
    if-eqz v2, :cond_1

    .line 873019
    const-string p0, "action_button_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873020
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873021
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873022
    if-eqz v2, :cond_2

    .line 873023
    const-string p0, "argb_background_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873024
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873025
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873026
    if-eqz v2, :cond_3

    .line 873027
    const-string p0, "argb_text_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873028
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873029
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873030
    if-eqz v2, :cond_4

    .line 873031
    const-string p0, "content_block_bottom_margin"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873032
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873033
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 873034
    if-eqz v2, :cond_5

    .line 873035
    const-string p0, "cultural_moment_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873036
    invoke-static {v1, v2, p1}, LX/5Ej;->a(LX/15i;ILX/0nX;)V

    .line 873037
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 873038
    if-eqz v2, :cond_6

    .line 873039
    const-string p0, "cultural_moment_video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873040
    invoke-static {v1, v2, p1, p2}, LX/5El;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 873041
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 873042
    if-eqz v2, :cond_7

    .line 873043
    const-string p0, "favicon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873044
    invoke-static {v1, v2, p1}, LX/5Ek;->a(LX/15i;ILX/0nX;)V

    .line 873045
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873046
    if-eqz v2, :cond_8

    .line 873047
    const-string p0, "favicon_color_style"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873048
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873049
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 873050
    if-eqz v2, :cond_9

    .line 873051
    const-string p0, "image_margin"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 873052
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 873053
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 873054
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 873055
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
