.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 878348
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 878349
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 878350
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 878351
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 878352
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 878353
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 878354
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 878355
    if-eqz v2, :cond_0

    .line 878356
    const-string v3, "is_music_item"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 878357
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 878358
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 878359
    if-eqz v2, :cond_1

    .line 878360
    const-string v2, "music_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 878361
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 878362
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 878363
    if-eqz v2, :cond_2

    .line 878364
    const-string v3, "musicians"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 878365
    invoke-static {v1, v2, p1, p2}, LX/5Fq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 878366
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 878367
    if-eqz v2, :cond_3

    .line 878368
    const-string v3, "preview_urls"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 878369
    invoke-static {v1, v2, p1, p2}, LX/5Fr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 878370
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 878371
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 878372
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel;LX/0nX;LX/0my;)V

    return-void
.end method
