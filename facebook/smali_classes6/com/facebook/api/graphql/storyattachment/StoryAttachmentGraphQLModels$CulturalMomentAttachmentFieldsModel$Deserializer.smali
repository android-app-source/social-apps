.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 873007
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 873008
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 873006
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 872956
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 872957
    const/4 v2, 0x0

    .line 872958
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_d

    .line 872959
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 872960
    :goto_0
    move v1, v2

    .line 872961
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 872962
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 872963
    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentAttachmentFieldsModel;-><init>()V

    .line 872964
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 872965
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 872966
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 872967
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 872968
    :cond_0
    return-object v1

    .line 872969
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 872970
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_c

    .line 872971
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 872972
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 872973
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 872974
    const-string p0, "action_button_title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 872975
    invoke-static {p1, v0}, LX/5Ei;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 872976
    :cond_3
    const-string p0, "action_button_url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 872977
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 872978
    :cond_4
    const-string p0, "argb_background_color"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 872979
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 872980
    :cond_5
    const-string p0, "argb_text_color"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 872981
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 872982
    :cond_6
    const-string p0, "content_block_bottom_margin"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 872983
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 872984
    :cond_7
    const-string p0, "cultural_moment_image"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 872985
    invoke-static {p1, v0}, LX/5Ej;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 872986
    :cond_8
    const-string p0, "cultural_moment_video"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 872987
    invoke-static {p1, v0}, LX/5El;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 872988
    :cond_9
    const-string p0, "favicon"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 872989
    invoke-static {p1, v0}, LX/5Ek;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 872990
    :cond_a
    const-string p0, "favicon_color_style"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 872991
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 872992
    :cond_b
    const-string p0, "image_margin"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 872993
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 872994
    :cond_c
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 872995
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 872996
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 872997
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 872998
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 872999
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 873000
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 873001
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 873002
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 873003
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 873004
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 873005
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
