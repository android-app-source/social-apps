.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x204d8a17
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881978
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881977
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 881975
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 881976
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881973
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    .line 881974
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 881967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881968
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 881969
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 881970
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 881971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 881954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881955
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881956
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    .line 881957
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 881958
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;

    .line 881959
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel$NodeModel;

    .line 881960
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881961
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 881964
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel$EdgesModel;-><init>()V

    .line 881965
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 881966
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 881963
    const v0, -0x66c62d2f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 881962
    const v0, 0x5c5cdd52

    return v0
.end method
