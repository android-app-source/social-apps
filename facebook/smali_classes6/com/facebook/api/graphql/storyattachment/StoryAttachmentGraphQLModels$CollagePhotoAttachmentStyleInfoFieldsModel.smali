.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38dea6b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872160
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 872156
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 872154
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 872155
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 872146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872147
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 872148
    iget v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 872149
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 872150
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 872151
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->h:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 872152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872153
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 872157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 872158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 872159
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 872140
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 872141
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->e:I

    .line 872142
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->f:I

    .line 872143
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->g:I

    .line 872144
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;->h:I

    .line 872145
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 872137
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;-><init>()V

    .line 872138
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 872139
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 872136
    const v0, -0x7d24d9ee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 872135
    const v0, -0x44fe9de3

    return v0
.end method
