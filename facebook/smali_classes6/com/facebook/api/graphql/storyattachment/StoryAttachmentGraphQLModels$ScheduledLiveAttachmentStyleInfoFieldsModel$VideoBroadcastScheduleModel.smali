.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2f988acb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$Serializer;
.end annotation


# instance fields
.field private A:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:J

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:J

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:J

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:J

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881279
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881280
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 881275
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 881276
    return-void
.end method

.method private A()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getScheduleProfileImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881281
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 881282
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private B()J
    .locals 2

    .prologue
    .line 881283
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 881284
    iget-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->B:J

    return-wide v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 881285
    iput-wide p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->B:J

    .line 881286
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881287
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881288
    if-eqz v0, :cond_0

    .line 881289
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 881290
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 881291
    iput-object p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->m:Ljava/lang/String;

    .line 881292
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881293
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881294
    if-eqz v0, :cond_0

    .line 881295
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 881296
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 881297
    iput-boolean p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->o:Z

    .line 881298
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881299
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881300
    if-eqz v0, :cond_0

    .line 881301
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 881302
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 881303
    iput-object p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->s:Ljava/lang/String;

    .line 881304
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881305
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881306
    if-eqz v0, :cond_0

    .line 881307
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 881308
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 881325
    iput-boolean p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q:Z

    .line 881326
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881327
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881328
    if-eqz v0, :cond_0

    .line 881329
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 881330
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 881309
    iput-object p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t:Ljava/lang/String;

    .line 881310
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881311
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881312
    if-eqz v0, :cond_0

    .line 881313
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 881314
    :cond_0
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 881331
    iput-object p1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u:Ljava/lang/String;

    .line 881332
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 881333
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 881334
    if-eqz v0, :cond_0

    .line 881335
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 881336
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881337
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    .line 881338
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881323
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    .line 881324
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881339
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->g:Ljava/lang/String;

    .line 881340
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881321
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    .line 881322
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881319
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    .line 881320
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881317
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->l:Ljava/lang/String;

    .line 881318
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881315
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->m:Ljava/lang/String;

    .line 881316
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881277
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->n:Ljava/lang/String;

    .line 881278
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 881133
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 881134
    iget-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->o:Z

    return v0
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 881131
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 881132
    iget-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q:Z

    return v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881135
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->s:Ljava/lang/String;

    .line 881136
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881137
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t:Ljava/lang/String;

    .line 881138
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881139
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u:Ljava/lang/String;

    .line 881140
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881141
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->v:Ljava/lang/String;

    .line 881142
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881143
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->x:Ljava/lang/String;

    .line 881144
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method private y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getScheduleBackgroundImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881145
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 881146
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private z()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881147
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    .line 881148
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 881149
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881150
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 881151
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 881152
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 881153
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->m()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 881154
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 881155
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 881156
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->p()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 881157
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 881158
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 881159
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 881160
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->v()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 881161
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->w()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 881162
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->x()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 881163
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x233590b5

    invoke-static {v6, v5, v7}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 881164
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 881165
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x40fcdb3e

    invoke-static {v6, v5, v7}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 881166
    const/16 v5, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 881167
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 881168
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 881169
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 881170
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->h:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 881171
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 881172
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 881173
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->k:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 881174
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 881175
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 881176
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 881177
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 881178
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 881179
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 881180
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->r:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 881181
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 881182
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 881183
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 881184
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 881185
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->w:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 881186
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 881187
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 881188
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 881189
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 881190
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->B:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 881191
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881192
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 881193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881194
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 881195
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x233590b5

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 881196
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 881197
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881198
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y:I

    move-object v1, v0

    .line 881199
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 881200
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    .line 881201
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 881202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881203
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->z:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel$ScheduleCustomImageModel;

    .line 881204
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 881205
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x40fcdb3e

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 881206
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 881207
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    .line 881208
    iput v3, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A:I

    move-object v1, v0

    .line 881209
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881210
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 881211
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 881212
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 881213
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 881214
    new-instance v0, LX/5EO;

    invoke-direct {v0, p1}, LX/5EO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881215
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 881216
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 881217
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->h:J

    .line 881218
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->k:J

    .line 881219
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->o:Z

    .line 881220
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->p:Z

    .line 881221
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->q:Z

    .line 881222
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->r:J

    .line 881223
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->w:J

    .line 881224
    const/16 v0, 0x14

    const v1, 0x233590b5

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->y:I

    .line 881225
    const/16 v0, 0x16

    const v1, 0x40fcdb3e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->A:I

    .line 881226
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->B:J

    .line 881227
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 881228
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881229
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881231
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 881232
    :goto_0
    return-void

    .line 881233
    :cond_0
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881234
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881236
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 881237
    :cond_1
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 881238
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->s()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881240
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 881241
    :cond_2
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 881242
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881244
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 881245
    :cond_3
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 881246
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881248
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 881249
    :cond_4
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 881250
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881251
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881252
    const/16 v0, 0x10

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 881253
    :cond_5
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 881254
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->B()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 881255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 881256
    const/16 v0, 0x17

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 881257
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 881258
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881259
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->a(Ljava/lang/String;)V

    .line 881260
    :cond_0
    :goto_0
    return-void

    .line 881261
    :cond_1
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 881262
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->a(Z)V

    goto :goto_0

    .line 881263
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 881264
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->b(Z)V

    goto :goto_0

    .line 881265
    :cond_3
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 881266
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 881267
    :cond_4
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 881268
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 881269
    :cond_5
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 881270
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 881271
    :cond_6
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881272
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;->a(J)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 881128
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ScheduledLiveAttachmentStyleInfoFieldsModel$VideoBroadcastScheduleModel;-><init>()V

    .line 881129
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 881130
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 881273
    const v0, 0x33359572

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 881274
    const v0, 0x56118b5d    # 4.0006937E13f

    return v0
.end method
