.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 877756
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 877757
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 877758
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 877759
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 877760
    const/4 v2, 0x0

    .line 877761
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 877762
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 877763
    :goto_0
    move v1, v2

    .line 877764
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 877765
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 877766
    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel;-><init>()V

    .line 877767
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 877768
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 877769
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 877770
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 877771
    :cond_0
    return-object v1

    .line 877772
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 877773
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_9

    .line 877774
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 877775
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 877776
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 877777
    const-string p0, "__type__"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 877778
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 877779
    :cond_4
    const-string p0, "employer"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 877780
    invoke-static {p1, v0}, LX/5Fl;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 877781
    :cond_5
    const-string p0, "image"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 877782
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 877783
    :cond_6
    const-string p0, "school"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 877784
    invoke-static {p1, v0}, LX/5Fn;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 877785
    :cond_7
    const-string p0, "school_class"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 877786
    invoke-static {p1, v0}, LX/5Fm;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 877787
    :cond_8
    const-string p0, "work_project"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 877788
    invoke-static {p1, v0}, LX/5Fo;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 877789
    :cond_9
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 877790
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 877791
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 877792
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 877793
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 877794
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 877795
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 877796
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
