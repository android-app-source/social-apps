.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 881375
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 881376
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 881377
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 881378
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 881379
    const/4 v11, 0x0

    .line 881380
    const/4 v10, 0x0

    .line 881381
    const/4 v9, 0x0

    .line 881382
    const/4 v8, 0x0

    .line 881383
    const/4 v7, 0x0

    .line 881384
    const/4 v6, 0x0

    .line 881385
    const/4 v5, 0x0

    .line 881386
    const/4 v4, 0x0

    .line 881387
    const/4 v3, 0x0

    .line 881388
    const/4 v2, 0x0

    .line 881389
    const/4 v1, 0x0

    .line 881390
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 881391
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 881392
    const/4 v1, 0x0

    .line 881393
    :goto_0
    move v1, v1

    .line 881394
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 881395
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 881396
    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel;-><init>()V

    .line 881397
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 881398
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 881399
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 881400
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 881401
    :cond_0
    return-object v1

    .line 881402
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 881403
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_c

    .line 881404
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 881405
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 881406
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 881407
    const-string p0, "application_name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 881408
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 881409
    :cond_3
    const-string p0, "artist_names"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 881410
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 881411
    :cond_4
    const-string p0, "audio_url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 881412
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 881413
    :cond_5
    const-string p0, "collection_names"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 881414
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 881415
    :cond_6
    const-string p0, "cover_url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 881416
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 881417
    :cond_7
    const-string p0, "duration_ms"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 881418
    const/4 v1, 0x1

    .line 881419
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 881420
    :cond_8
    const-string p0, "global_share"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 881421
    invoke-static {p1, v0}, LX/5GW;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 881422
    :cond_9
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 881423
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 881424
    :cond_a
    const-string p0, "music_title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 881425
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 881426
    :cond_b
    const-string p0, "owner"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 881427
    invoke-static {p1, v0}, LX/5GX;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 881428
    :cond_c
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 881429
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 881430
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 881431
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 881432
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 881433
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 881434
    if-eqz v1, :cond_d

    .line 881435
    const/4 v1, 0x5

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v6, v7}, LX/186;->a(III)V

    .line 881436
    :cond_d
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 881437
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 881438
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 881439
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 881440
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
