.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x57bba27e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877543
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877542
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 877540
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 877541
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877538
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 877539
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 877544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877545
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 877546
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 877547
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 877548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877549
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 877525
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877526
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 877527
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 877528
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 877529
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;

    .line 877530
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 877531
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877532
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 877535
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel;-><init>()V

    .line 877536
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 877537
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 877534
    const v0, -0x1dd7e6a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 877533
    const v0, 0x1eaef984

    return v0
.end method
