.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43135c95
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881506
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 881505
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 881503
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 881504
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881500
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 881501
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 881502
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 881498
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->f:Ljava/util/List;

    .line 881499
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881496
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->g:Ljava/lang/String;

    .line 881497
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 881486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881487
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 881488
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 881489
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 881490
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 881491
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 881492
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 881493
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 881494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881495
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 881507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 881508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 881509
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 881485
    new-instance v0, LX/5EP;

    invoke-direct {v0, p1}, LX/5EP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 881484
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 881482
    invoke-virtual {p2}, LX/18L;->a()V

    .line 881483
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 881481
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 881478
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SingleSongMusicAttachmentModel$GlobalShareModel$OpenGraphNodeModel;-><init>()V

    .line 881479
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 881480
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 881477
    const v0, -0x41010f6d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 881476
    const v0, 0x252222

    return v0
.end method
