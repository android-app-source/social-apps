.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3f11a788
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874207
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874208
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 874209
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 874210
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874232
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    .line 874233
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 874211
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874212
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 874213
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 874214
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 874215
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 874216
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 874217
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 874218
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 874219
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 874220
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 874221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874222
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 874223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874224
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 874225
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    .line 874226
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 874227
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;

    .line 874228
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    .line 874229
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874230
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 874231
    new-instance v0, LX/5De;

    invoke-direct {v0, p1}, LX/5De;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874204
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 874205
    invoke-virtual {p2}, LX/18L;->a()V

    .line 874206
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 874190
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874191
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 874192
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 874193
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 874194
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;-><init>()V

    .line 874195
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 874196
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874197
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->f:Ljava/lang/String;

    .line 874198
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874199
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->g:Ljava/lang/String;

    .line 874200
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 874201
    const v0, 0x2dc47a1e

    return v0
.end method

.method public final synthetic e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874202
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 874203
    const v0, 0x3c2b9d5

    return v0
.end method
