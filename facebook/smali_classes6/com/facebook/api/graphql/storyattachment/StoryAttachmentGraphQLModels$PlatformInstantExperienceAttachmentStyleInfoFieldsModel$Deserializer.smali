.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 879891
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 879892
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 879893
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 879894
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 879895
    const/4 v2, 0x0

    .line 879896
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_c

    .line 879897
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 879898
    :goto_0
    move v1, v2

    .line 879899
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 879900
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 879901
    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;-><init>()V

    .line 879902
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 879903
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 879904
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 879905
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 879906
    :cond_0
    return-object v1

    .line 879907
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 879908
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 879909
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 879910
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 879911
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 879912
    const-string p0, "instant_experience_ad_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 879913
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 879914
    :cond_3
    const-string p0, "instant_experience_app"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 879915
    invoke-static {p1, v0}, LX/5GG;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 879916
    :cond_4
    const-string p0, "instant_experience_app_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 879917
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 879918
    :cond_5
    const-string p0, "instant_experience_domain_whitelist"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 879919
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 879920
    :cond_6
    const-string p0, "instant_experience_feature_enabled_list"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 879921
    invoke-static {p1, v0}, LX/5GH;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 879922
    :cond_7
    const-string p0, "instant_experience_link_uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 879923
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 879924
    :cond_8
    const-string p0, "instant_experience_page"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 879925
    invoke-static {p1, v0}, LX/5GI;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 879926
    :cond_9
    const-string p0, "instant_experience_user_app_scoped_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 879927
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 879928
    :cond_a
    const-string p0, "instant_experience_user_page_scoped_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 879929
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 879930
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 879931
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 879932
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 879933
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 879934
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 879935
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 879936
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 879937
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 879938
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 879939
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 879940
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
