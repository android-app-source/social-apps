.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x569c9a13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874797
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874796
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 874794
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 874795
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874792
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    .line 874793
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 874786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874787
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 874788
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 874789
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 874790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 874778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874779
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 874780
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    .line 874781
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 874782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;

    .line 874783
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    .line 874784
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874785
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 874773
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel$EdgesModel;-><init>()V

    .line 874774
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 874775
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 874777
    const v0, 0x589ef884

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 874776
    const v0, 0x20f1351

    return v0
.end method
