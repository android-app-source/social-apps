.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6f30072f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 878832
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 878782
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 878830
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 878831
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceRecommendationPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878828
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    .line 878829
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878826
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 878827
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878824
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 878825
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 878812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 878813
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 878814
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 878815
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 878816
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 878817
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 878818
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 878819
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 878820
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 878821
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 878822
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 878823
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 878794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 878795
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 878796
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    .line 878797
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 878798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;

    .line 878799
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    .line 878800
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 878801
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 878802
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 878803
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;

    .line 878804
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 878805
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 878806
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 878807
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 878808
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;

    .line 878809
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->h:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 878810
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 878811
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878793
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceRecommendationPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878792
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 878789
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;-><init>()V

    .line 878790
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 878791
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878787
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    .line 878788
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()LX/176;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878786
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 878785
    const v0, -0x3a8a86c1

    return v0
.end method

.method public final synthetic e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 878784
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 878783
    const v0, 0x7dfc96d8

    return v0
.end method
