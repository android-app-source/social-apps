.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x458bc456
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874615
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 874591
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 874613
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 874614
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOpenGraphNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 874611
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 874612
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 874605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874606
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 874607
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 874608
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 874609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 874597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 874598
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 874599
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 874600
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 874601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;

    .line 874602
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 874603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 874604
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 874594
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;-><init>()V

    .line 874595
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 874596
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 874593
    const v0, -0x7532a4ef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 874592
    const v0, 0x1eaef984

    return v0
.end method
