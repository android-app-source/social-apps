.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 882737
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 882738
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 882658
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 882660
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 882661
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x10

    const/16 v5, 0xe

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 882662
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 882663
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 882664
    if-eqz v2, :cond_0

    .line 882665
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882666
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 882667
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882668
    if-eqz v2, :cond_1

    .line 882669
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882670
    invoke-static {v1, v2, p1}, LX/5Gh;->a(LX/15i;ILX/0nX;)V

    .line 882671
    :cond_1
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 882672
    if-eqz v2, :cond_2

    .line 882673
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882674
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 882675
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882676
    if-eqz v2, :cond_3

    .line 882677
    const-string v3, "city"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882678
    invoke-static {v1, v2, p1}, LX/5CP;->a(LX/15i;ILX/0nX;)V

    .line 882679
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 882680
    if-eqz v2, :cond_4

    .line 882681
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882682
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882683
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 882684
    if-eqz v2, :cond_5

    .line 882685
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882686
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 882687
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882688
    if-eqz v2, :cond_6

    .line 882689
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882690
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 882691
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882692
    if-eqz v2, :cond_7

    .line 882693
    const-string v3, "map_bounding_box"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882694
    invoke-static {v1, v2, p1}, LX/5Gi;->a(LX/15i;ILX/0nX;)V

    .line 882695
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 882696
    if-eqz v2, :cond_8

    .line 882697
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882698
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882699
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882700
    if-eqz v2, :cond_9

    .line 882701
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882702
    invoke-static {v1, v2, p1}, LX/5CQ;->a(LX/15i;ILX/0nX;)V

    .line 882703
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882704
    if-eqz v2, :cond_a

    .line 882705
    const-string v3, "page_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882706
    invoke-static {v1, v2, p1}, LX/5CR;->a(LX/15i;ILX/0nX;)V

    .line 882707
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 882708
    if-eqz v2, :cond_b

    .line 882709
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882710
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 882711
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882712
    if-eqz v2, :cond_c

    .line 882713
    const-string v3, "saved_collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882714
    invoke-static {v1, v2, p1, p2}, LX/40i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 882715
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 882716
    if-eqz v2, :cond_d

    .line 882717
    const-string v3, "should_show_reviews_on_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882718
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 882719
    :cond_d
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 882720
    if-eqz v2, :cond_e

    .line 882721
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882722
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882723
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 882724
    if-eqz v2, :cond_f

    .line 882725
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882726
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882727
    :cond_f
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 882728
    if-eqz v2, :cond_10

    .line 882729
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882730
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 882731
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 882732
    if-eqz v2, :cond_11

    .line 882733
    const-string v3, "viewer_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 882734
    invoke-static {v1, v2, p1}, LX/5CS;->a(LX/15i;ILX/0nX;)V

    .line 882735
    :cond_11
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 882736
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 882659
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
