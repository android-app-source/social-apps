.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x56e0d8a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 882590
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 882589
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 882587
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 882588
    return-void
.end method

.method private a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachmentProperties"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 882585
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x62371dc

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->e:LX/3Sb;

    .line 882586
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 882583
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    .line 882584
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 882591
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CollagePhotoAttachmentStyleInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->g:Ljava/util/List;

    .line 882592
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 882581
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->h:Ljava/util/List;

    .line 882582
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 882569
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 882570
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 882571
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 882572
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 882573
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->c(Ljava/util/List;)I

    move-result v3

    .line 882574
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 882575
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 882576
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 882577
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 882578
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 882579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 882580
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 882551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 882552
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 882553
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 882554
    if-eqz v1, :cond_3

    .line 882555
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    .line 882556
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->e:LX/3Sb;

    move-object v1, v0

    .line 882557
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 882558
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    .line 882559
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 882560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    .line 882561
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel$MediaModel;

    .line 882562
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 882563
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 882564
    if-eqz v2, :cond_1

    .line 882565
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    .line 882566
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 882567
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 882568
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 882548
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;-><init>()V

    .line 882549
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 882550
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 882547
    const v0, 0x5213ffd2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 882546
    const v0, -0x4b900828

    return v0
.end method
