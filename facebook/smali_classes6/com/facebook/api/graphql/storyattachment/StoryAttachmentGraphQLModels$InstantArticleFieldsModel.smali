.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x69b1da1c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877203
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 877202
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 877200
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 877201
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInstantArticle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 877198
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 877199
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 877179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877180
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 877181
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 877182
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 877183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877184
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 877190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 877191
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 877192
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 877193
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 877194
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;

    .line 877195
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 877196
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 877197
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 877187
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel;-><init>()V

    .line 877188
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 877189
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 877186
    const v0, 0x374e2848

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 877185
    const v0, 0x1eaef984

    return v0
.end method
