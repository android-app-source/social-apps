.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 875512
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 875513
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 875514
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 875515
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 875516
    const/4 v2, 0x0

    .line 875517
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 875518
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 875519
    :goto_0
    move v1, v2

    .line 875520
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 875521
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 875522
    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GoodwillThrowbackCardAttachmentComponentModel;-><init>()V

    .line 875523
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 875524
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 875525
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 875526
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 875527
    :cond_0
    return-object v1

    .line 875528
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 875529
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_9

    .line 875530
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 875531
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 875532
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 875533
    const-string p0, "accent_image"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 875534
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 875535
    :cond_3
    const-string p0, "action_links"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 875536
    invoke-static {p1, v0}, LX/5FD;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 875537
    :cond_4
    const-string p0, "additional_accent_images"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 875538
    invoke-static {p1, v0}, LX/5FE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 875539
    :cond_5
    const-string p0, "data_points"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 875540
    invoke-static {p1, v0}, LX/5FG;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 875541
    :cond_6
    const-string p0, "throwback_media"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 875542
    invoke-static {p1, v0}, LX/5lp;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 875543
    :cond_7
    const-string p0, "throwback_media_attachments"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 875544
    invoke-static {p1, v0}, LX/5Gg;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 875545
    :cond_8
    const-string p0, "title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 875546
    invoke-static {p1, v0}, LX/5FH;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 875547
    :cond_9
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 875548
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 875549
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 875550
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 875551
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 875552
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 875553
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 875554
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 875555
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
