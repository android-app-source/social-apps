.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 874578
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 874579
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 874580
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 874581
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 874582
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 874583
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 874584
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 874585
    if-eqz v2, :cond_0

    .line 874586
    const-string p0, "open_graph_node"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874587
    invoke-static {v1, v2, p1, p2}, LX/5Ey;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 874588
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 874589
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 874590
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
