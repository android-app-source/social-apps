.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 874830
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 874831
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 874832
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 874833
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 874834
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 874835
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 874836
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 874837
    if-eqz v2, :cond_0

    .line 874838
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874839
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 874840
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 874841
    if-eqz v2, :cond_1

    .line 874842
    const-string p0, "media_question_option_order"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874843
    invoke-static {v1, v2, p1, p2}, LX/5F1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 874844
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 874845
    if-eqz v2, :cond_2

    .line 874846
    const-string p0, "media_question_photos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874847
    invoke-static {v1, v2, p1, p2}, LX/5F2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 874848
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 874849
    if-eqz v2, :cond_3

    .line 874850
    const-string p0, "media_question_type"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874851
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 874852
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 874853
    if-eqz v2, :cond_4

    .line 874854
    const-string p0, "viewer_has_voted"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 874855
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 874856
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 874857
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 874858
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
