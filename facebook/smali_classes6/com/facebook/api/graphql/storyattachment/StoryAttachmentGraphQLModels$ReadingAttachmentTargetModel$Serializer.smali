.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 880915
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 880916
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 880896
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 880897
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 880898
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 880899
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 880900
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880901
    if-eqz v2, :cond_0

    .line 880902
    const-string p0, "author_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880903
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880904
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 880905
    if-eqz v2, :cond_1

    .line 880906
    const-string p0, "page_likers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880907
    invoke-static {v1, v2, p1}, LX/5GP;->a(LX/15i;ILX/0nX;)V

    .line 880908
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 880909
    if-eqz v2, :cond_2

    .line 880910
    const-string p0, "rectangular_profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880911
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 880912
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 880913
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 880914
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ReadingAttachmentTargetModel;LX/0nX;LX/0my;)V

    return-void
.end method
