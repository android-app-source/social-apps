.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd53c0e3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 879521
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 879478
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 879479
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 879480
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 879481
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 879482
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 879483
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;
    .locals 13

    .prologue
    .line 879484
    if-nez p0, :cond_0

    .line 879485
    const/4 p0, 0x0

    .line 879486
    :goto_0
    return-object p0

    .line 879487
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    if-eqz v0, :cond_1

    .line 879488
    check-cast p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    goto :goto_0

    .line 879489
    :cond_1
    new-instance v0, LX/5EA;

    invoke-direct {v0}, LX/5EA;-><init>()V

    .line 879490
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->a:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    .line 879491
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->c()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->b:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    .line 879492
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->c:Ljava/lang/String;

    .line 879493
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->d:Ljava/lang/String;

    .line 879494
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->aP_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 879495
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->aO_()LX/176;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;->a(LX/176;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5EA;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 879496
    const/4 v6, 0x1

    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 879497
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 879498
    iget-object v3, v0, LX/5EA;->a:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 879499
    iget-object v5, v0, LX/5EA;->b:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 879500
    iget-object v7, v0, LX/5EA;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 879501
    iget-object v8, v0, LX/5EA;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 879502
    iget-object v9, v0, LX/5EA;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 879503
    iget-object v10, v0, LX/5EA;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 879504
    const/4 v11, 0x6

    invoke-virtual {v2, v11}, LX/186;->c(I)V

    .line 879505
    invoke-virtual {v2, v12, v3}, LX/186;->b(II)V

    .line 879506
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 879507
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 879508
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 879509
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 879510
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 879511
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 879512
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 879513
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 879514
    invoke-virtual {v3, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 879515
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 879516
    new-instance v3, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;-><init>(LX/15i;)V

    .line 879517
    move-object p0, v3

    .line 879518
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879519
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    .line 879520
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879522
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    .line 879523
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879524
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 879525
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879526
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 879527
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 879528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 879529
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 879530
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 879531
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 879532
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 879533
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 879534
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 879535
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 879536
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 879537
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 879538
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 879539
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 879540
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 879541
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 879542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 879543
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 879454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 879455
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 879456
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    .line 879457
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 879458
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    .line 879459
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    .line 879460
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 879461
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    .line 879462
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 879463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    .line 879464
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    .line 879465
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 879466
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 879467
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 879468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    .line 879469
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 879470
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 879471
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 879472
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 879473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    .line 879474
    iput-object v0, v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 879475
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 879476
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 879477
    new-instance v0, LX/5ED;

    invoke-direct {v0, p1}, LX/5ED;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879437
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 879441
    invoke-virtual {p2}, LX/18L;->a()V

    .line 879442
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 879443
    return-void
.end method

.method public final synthetic aO_()LX/176;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879444
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aP_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879445
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879446
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 879438
    new-instance v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;-><init>()V

    .line 879439
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 879440
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879447
    invoke-direct {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879448
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->g:Ljava/lang/String;

    .line 879449
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 879450
    const v0, 0x3cff9ca0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 879451
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->h:Ljava/lang/String;

    .line 879452
    iget-object v0, p0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 879453
    const v0, 0x285feb

    return v0
.end method
