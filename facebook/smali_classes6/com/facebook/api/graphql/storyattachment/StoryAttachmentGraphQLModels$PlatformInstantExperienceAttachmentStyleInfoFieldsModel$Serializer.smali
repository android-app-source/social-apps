.class public final Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 880150
    const-class v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 880151
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 880149
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 880108
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 880109
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    .line 880110
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 880111
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880112
    if-eqz v2, :cond_0

    .line 880113
    const-string v3, "instant_experience_ad_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880114
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880115
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 880116
    if-eqz v2, :cond_1

    .line 880117
    const-string v3, "instant_experience_app"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880118
    invoke-static {v1, v2, p1, p2}, LX/5GG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 880119
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880120
    if-eqz v2, :cond_2

    .line 880121
    const-string v3, "instant_experience_app_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880122
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880123
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 880124
    if-eqz v2, :cond_3

    .line 880125
    const-string v2, "instant_experience_domain_whitelist"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880126
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 880127
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 880128
    if-eqz v2, :cond_4

    .line 880129
    const-string v3, "instant_experience_feature_enabled_list"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880130
    invoke-static {v1, v2, p1}, LX/5GH;->a(LX/15i;ILX/0nX;)V

    .line 880131
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880132
    if-eqz v2, :cond_5

    .line 880133
    const-string v3, "instant_experience_link_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880134
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880135
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 880136
    if-eqz v2, :cond_6

    .line 880137
    const-string v3, "instant_experience_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880138
    invoke-static {v1, v2, p1}, LX/5GI;->a(LX/15i;ILX/0nX;)V

    .line 880139
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880140
    if-eqz v2, :cond_7

    .line 880141
    const-string v3, "instant_experience_user_app_scoped_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880142
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880143
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 880144
    if-eqz v2, :cond_8

    .line 880145
    const-string v3, "instant_experience_user_page_scoped_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 880146
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 880147
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 880148
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 880107
    check-cast p1, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlatformInstantExperienceAttachmentStyleInfoFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
