.class public Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field private a:LX/1Ml;

.field private b:Lcom/facebook/content/SecureContextHelper;

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6Pt;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0kL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1086506
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1086504
    iget-object v0, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a:LX/1Ml;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1Ml;->a(Z)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x539

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1086505
    return-void
.end method

.method private a(LX/1Ml;Lcom/facebook/content/SecureContextHelper;Ljava/util/Set;LX/0kL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ml;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/util/Set",
            "<",
            "LX/6Pt;",
            ">;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086499
    iput-object p1, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a:LX/1Ml;

    .line 1086500
    iput-object p2, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1086501
    iput-object p3, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->c:Ljava/util/Set;

    .line 1086502
    iput-object p4, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->d:LX/0kL;

    .line 1086503
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;LX/6Pr;)V
    .locals 2

    .prologue
    .line 1086492
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1086493
    iget-object v1, p2, LX/6Pr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1086494
    iget-object v1, p2, LX/6Pr;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1086495
    invoke-static {p2}, LX/6Ps;->a(LX/6Pr;)LX/0Tn;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1086496
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1086497
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1086498
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;

    invoke-static {v2}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    new-instance v3, LX/0U8;

    invoke-interface {v2}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v4

    new-instance p1, LX/6Pu;

    invoke-direct {p1, v2}, LX/6Pu;-><init>(LX/0QB;)V

    invoke-direct {v3, v4, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v3, v3

    invoke-static {v2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a(LX/1Ml;Lcom/facebook/content/SecureContextHelper;Ljava/util/Set;LX/0kL;)V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1086481
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1086482
    invoke-static {p0, p0}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1086483
    invoke-virtual {p0}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 1086484
    iget-object v0, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Pt;

    .line 1086485
    invoke-interface {v0}, LX/6Pt;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Pr;

    .line 1086486
    invoke-direct {p0, v1, v0}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/6Pr;)V

    goto :goto_0

    .line 1086487
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1086488
    iget-object v0, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a:LX/1Ml;

    invoke-virtual {v0}, LX/1Ml;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1086489
    iget-object v0, p0, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->d:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Need to give permission to draw overlay first"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1086490
    invoke-direct {p0}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->a()V

    .line 1086491
    :cond_2
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1086478
    const/16 v0, 0x539

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1086479
    invoke-virtual {p0}, Lcom/facebook/debug/debugoverlay/DebugOverlaySettingsActivity;->finish()V

    .line 1086480
    :cond_0
    return-void
.end method
