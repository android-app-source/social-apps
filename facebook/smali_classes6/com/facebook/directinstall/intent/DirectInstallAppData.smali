.class public Lcom/facebook/directinstall/intent/DirectInstallAppData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6Qo;

.field public final b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

.field public final c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088052
    new-instance v0, LX/6Qm;

    invoke-direct {v0}, LX/6Qm;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1088044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088045
    const-class v0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    .line 1088046
    const-class v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    .line 1088047
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/6Qo;->fromValue(I)LX/6Qo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->a:LX/6Qo;

    .line 1088048
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->d:Ljava/lang/String;

    .line 1088049
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->e:Landroid/os/Bundle;

    .line 1088050
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->f:Ljava/lang/String;

    .line 1088051
    return-void
.end method

.method private constructor <init>(Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;Lcom/facebook/directinstall/intent/DirectInstallAppDetails;LX/6Qo;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1088035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088036
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1088037
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    .line 1088038
    iput-object p2, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    .line 1088039
    iput-object p3, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->a:LX/6Qo;

    .line 1088040
    iput-object p4, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->d:Ljava/lang/String;

    .line 1088041
    iput-object p5, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->e:Landroid/os/Bundle;

    .line 1088042
    iput-object p6, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->f:Ljava/lang/String;

    .line 1088043
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;Lcom/facebook/directinstall/intent/DirectInstallAppDetails;LX/6Qo;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1088034
    invoke-direct/range {p0 .. p6}, Lcom/facebook/directinstall/intent/DirectInstallAppData;-><init>(Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;Lcom/facebook/directinstall/intent/DirectInstallAppDetails;LX/6Qo;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1088033
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1088053
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1088032
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/directinstall/intent/DirectInstallAppDetails;
    .locals 1

    .prologue
    .line 1088031
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088030
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;
    .locals 1

    .prologue
    .line 1088029
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1088022
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1088023
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1088024
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->a:LX/6Qo;

    invoke-virtual {v0}, LX/6Qo;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088025
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088026
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->e:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 1088027
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088028
    return-void
.end method
