.class public final Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088139
    new-instance v0, LX/6Qt;

    invoke-direct {v0}, LX/6Qt;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1088140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->a:Ljava/lang/String;

    .line 1088142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->b:Ljava/util/ArrayList;

    .line 1088143
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->b:Ljava/util/ArrayList;

    const-class v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1088144
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1088145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088146
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->a:Ljava/lang/String;

    .line 1088147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->b:Ljava/util/ArrayList;

    .line 1088148
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088149
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1088150
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088151
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1088152
    return-void
.end method
