.class public final Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088138
    new-instance v0, LX/6Qu;

    invoke-direct {v0}, LX/6Qu;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1088134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->a:Ljava/lang/String;

    .line 1088136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->b:Ljava/lang/String;

    .line 1088137
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1088130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088131
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->a:Ljava/lang/String;

    .line 1088132
    iput-object p2, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->b:Ljava/lang/String;

    .line 1088133
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088129
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1088126
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088127
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities$Entity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088128
    return-void
.end method
