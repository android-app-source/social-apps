.class public Lcom/facebook/directinstall/intent/DirectInstallAppDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:I

.field public final f:I

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088213
    new-instance v0, LX/6Qq;

    invoke-direct {v0}, LX/6Qq;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1088176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    .line 1088178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b:Ljava/lang/String;

    .line 1088179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c:Ljava/lang/String;

    .line 1088180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    .line 1088181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->e:I

    .line 1088182
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->f:I

    .line 1088183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->g:Ljava/lang/String;

    .line 1088184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h:Ljava/lang/String;

    .line 1088185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    .line 1088186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    .line 1088187
    const-class v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1088188
    const-class v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1088189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1088190
    const-class v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1088191
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->m:LX/0Px;

    .line 1088192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1088193
    const-class v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1088194
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->n:LX/0Px;

    .line 1088195
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;LX/0Px;LX/0Px;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;",
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1088196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088197
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    .line 1088198
    iput-object p2, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b:Ljava/lang/String;

    .line 1088199
    iput-object p3, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c:Ljava/lang/String;

    .line 1088200
    iput-object p4, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    .line 1088201
    iput p5, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->e:I

    .line 1088202
    iput p6, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->f:I

    .line 1088203
    iput-object p7, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->g:Ljava/lang/String;

    .line 1088204
    iput-object p8, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h:Ljava/lang/String;

    .line 1088205
    iput-object p9, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    .line 1088206
    iput-object p10, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    .line 1088207
    iput-object p11, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1088208
    iput-object p12, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1088209
    iput-object p13, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->m:LX/0Px;

    .line 1088210
    iput-object p14, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->n:LX/0Px;

    .line 1088211
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;LX/0Px;LX/0Px;B)V
    .locals 0

    .prologue
    .line 1088212
    invoke-direct/range {p0 .. p14}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;LX/0Px;LX/0Px;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088214
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088174
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088175
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088173
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088172
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1088171
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1088153
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088170
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088169
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1088154
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088155
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088156
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088157
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088158
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088159
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088160
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088161
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088162
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088163
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088164
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1088165
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1088166
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1088167
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->n:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1088168
    return-void
.end method
