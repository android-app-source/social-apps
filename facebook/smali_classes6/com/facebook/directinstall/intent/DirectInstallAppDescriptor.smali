.class public Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088085
    new-instance v0, LX/6Qp;

    invoke-direct {v0}, LX/6Qp;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1088076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088077
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    .line 1088078
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->b:I

    .line 1088079
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->c:Ljava/lang/String;

    .line 1088080
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1088081
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1088082
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->d:LX/0Px;

    .line 1088083
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    .line 1088084
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1088069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088070
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    .line 1088071
    iput p2, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->b:I

    .line 1088072
    iput-object p3, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->c:Ljava/lang/String;

    .line 1088073
    iput-object p4, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->d:LX/0Px;

    .line 1088074
    iput-object p5, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    .line 1088075
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088068
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1088067
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->b:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088057
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1088066
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->d:LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088065
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088064
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1088058
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088059
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088060
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088061
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1088062
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088063
    return-void
.end method
