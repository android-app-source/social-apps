.class public final Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088109
    new-instance v0, LX/6Qs;

    invoke-direct {v0}, LX/6Qs;-><init>()V

    sput-object v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1088110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->a:Ljava/lang/String;

    .line 1088112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->b:I

    .line 1088113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->c:I

    .line 1088114
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1088115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088116
    iput-object p1, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->a:Ljava/lang/String;

    .line 1088117
    iput p2, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->b:I

    .line 1088118
    iput p3, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->c:I

    .line 1088119
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1088104
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1088105
    iget-object v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1088106
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088107
    iget v0, p0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1088108
    return-void
.end method
