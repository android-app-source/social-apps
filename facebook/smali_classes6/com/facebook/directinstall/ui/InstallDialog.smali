.class public Lcom/facebook/directinstall/ui/InstallDialog;
.super Landroid/app/Dialog;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Landroid/widget/FrameLayout;

.field public f:Z

.field public g:Lcom/facebook/resources/ui/FbButton;

.field private h:Z

.field public i:Lcom/facebook/resources/ui/FbButton;

.field public j:Z

.field public k:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088317
    const-class v0, Lcom/facebook/directinstall/ui/InstallDialog;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/directinstall/ui/InstallDialog;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1088318
    const v0, 0x7f0e0813

    invoke-direct {p0, p1, v0}, Lcom/facebook/directinstall/ui/InstallDialog;-><init>(Landroid/content/Context;I)V

    .line 1088319
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1088320
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 1088321
    iput-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->f:Z

    .line 1088322
    iput-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->h:Z

    .line 1088323
    iput-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->j:Z

    .line 1088324
    invoke-virtual {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030424

    const/4 p1, 0x0

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1088325
    invoke-super {p0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1088326
    const v0, 0x7f0d0343

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1088327
    const v0, 0x7f0d02c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1088328
    const v0, 0x7f0d02c3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1088329
    const v0, 0x7f0d0cbd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->g:Lcom/facebook/resources/ui/FbButton;

    .line 1088330
    const v0, 0x7f0d0cbc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->e:Landroid/widget/FrameLayout;

    .line 1088331
    const v0, 0x7f0d0525

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    .line 1088332
    const v0, 0x7f0d0526

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    .line 1088333
    const v0, 0x7f0a0113

    .line 1088334
    iget-object v1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1088335
    return-void
.end method

.method public static a(ZLandroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 1088336
    if-eqz p0, :cond_0

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1088337
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1088338
    :goto_0
    return-void

    .line 1088339
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 1088352
    invoke-virtual {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/directinstall/ui/InstallDialog;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 1088340
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1088341
    iget-object v3, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbButton;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_0

    iget-object v3, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbButton;->getVisibility()I

    move-result v3

    if-eq v3, v2, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 1088342
    :goto_0
    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1088343
    return-void

    :cond_1
    move v3, v1

    .line 1088344
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1088345
    goto :goto_1
.end method

.method public static e(Lcom/facebook/directinstall/ui/InstallDialog;)V
    .locals 2

    .prologue
    .line 1088298
    iget-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->h:Z

    iget-object v1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-static {v0, v1}, Lcom/facebook/directinstall/ui/InstallDialog;->a(ZLandroid/widget/TextView;)V

    .line 1088299
    invoke-static {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Lcom/facebook/directinstall/ui/InstallDialog;)V

    .line 1088300
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1088346
    invoke-direct {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1088347
    iget-object p1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1088348
    invoke-static {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->e(Lcom/facebook/directinstall/ui/InstallDialog;)V

    .line 1088349
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1088350
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/directinstall/ui/InstallDialog;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1088351
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1088313
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1088314
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1088315
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->e:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1088316
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1088310
    iput-boolean p1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->h:Z

    .line 1088311
    invoke-static {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->e(Lcom/facebook/directinstall/ui/InstallDialog;)V

    .line 1088312
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1088303
    invoke-direct {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1088304
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1088305
    iget-object v1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1088306
    iget-object v1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1088307
    :goto_0
    invoke-static {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Lcom/facebook/directinstall/ui/InstallDialog;)V

    .line 1088308
    return-void

    .line 1088309
    :cond_0
    iget-object v1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1088301
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1088302
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1088295
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1088296
    iget-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->f:Z

    iget-object p1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-static {v0, p1}, Lcom/facebook/directinstall/ui/InstallDialog;->a(ZLandroid/widget/TextView;)V

    .line 1088297
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1088293
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1088294
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1088289
    iput-boolean p1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->j:Z

    .line 1088290
    iget-boolean v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->j:Z

    iget-object p1, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-static {v0, p1}, Lcom/facebook/directinstall/ui/InstallDialog;->a(ZLandroid/widget/TextView;)V

    .line 1088291
    invoke-static {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Lcom/facebook/directinstall/ui/InstallDialog;)V

    .line 1088292
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 1088287
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1088288
    return-void
.end method

.method public final setTitle(I)V
    .locals 1

    .prologue
    .line 1088285
    invoke-direct {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/directinstall/ui/InstallDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1088286
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1088283
    iget-object v0, p0, Lcom/facebook/directinstall/ui/InstallDialog;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1088284
    return-void
.end method
