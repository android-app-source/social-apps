.class public Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/6QG;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/content/Context;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6QF;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/directinstall/intent/DirectInstallAppData;

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/6QI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1087239
    const-class v0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    const-string v1, "network_image"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Ljava/util/List;Lcom/facebook/content/SecureContextHelper;LX/6QI;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/directinstall/intent/DirectInstallAppData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/directinstall/intent/DirectInstallAppData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/6QF;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6QI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087231
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1087232
    iput-object p1, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->b:Landroid/content/Context;

    .line 1087233
    iput-object p5, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1087234
    iput-object p4, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->d:Ljava/util/List;

    .line 1087235
    iput-object p2, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->f:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1087236
    iput-object p3, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->g:Ljava/util/Map;

    .line 1087237
    iput-object p6, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->h:LX/6QI;

    .line 1087238
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1087240
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03126d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1087241
    new-instance v1, LX/6QG;

    invoke-direct {v1, p0, p0, v0}, LX/6QG;-><init>(Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 1087215
    check-cast p1, LX/6QG;

    .line 1087216
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6QF;

    .line 1087217
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1087218
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1087219
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1087220
    iget v3, v0, LX/6QF;->b:I

    move v3, v3

    .line 1087221
    int-to-double v4, v3

    .line 1087222
    iget v3, v0, LX/6QF;->c:I

    move v3, v3

    .line 1087223
    int-to-double v6, v3

    div-double/2addr v4, v6

    .line 1087224
    int-to-double v6, v2

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    .line 1087225
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1087226
    invoke-virtual {v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1087227
    iget-object v2, v0, LX/6QF;->a:Landroid/net/Uri;

    move-object v0, v2

    .line 1087228
    sget-object v2, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1087229
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1087230
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
