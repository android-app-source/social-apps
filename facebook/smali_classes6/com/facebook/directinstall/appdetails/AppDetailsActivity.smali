.class public Lcom/facebook/directinstall/appdetails/AppDetailsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;


# instance fields
.field public p:LX/6QI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/directinstall/intent/DirectInstallAppData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1086733
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    invoke-static {v0}, LX/6QI;->a(LX/0QB;)LX/6QI;

    move-result-object v0

    check-cast v0, LX/6QI;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->p:LX/6QI;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086777
    const-string v0, "neko_di_app_details"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086743
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1086744
    invoke-static {p0, p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1086745
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->setContentView(I)V

    .line 1086746
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1086747
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1086748
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a068a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1086749
    check-cast v0, LX/0h5;

    .line 1086750
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasFbLogo(Z)V

    .line 1086751
    new-instance v1, LX/6Q3;

    invoke-direct {v1, p0}, LX/6Q3;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1086752
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, LX/6Qv;->a(Landroid/content/Intent;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086753
    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086754
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v1, v2

    .line 1086755
    if-eqz v1, :cond_0

    .line 1086756
    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086757
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v1, v2

    .line 1086758
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1086759
    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1086760
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1086761
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1086762
    const-string v2, "app_data"

    .line 1086763
    const-string v3, "analytics"

    .line 1086764
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1086765
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1086766
    new-instance v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-direct {v1}, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;-><init>()V

    .line 1086767
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1086768
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d03ae

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1086769
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->p:LX/6QI;

    const-string v1, "neko_di_app_details_loaded"

    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086770
    iget-object v3, v2, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v2, v3

    .line 1086771
    iget-object v3, v2, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1086772
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086773
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v3, v4

    .line 1086774
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1086775
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v4}, LX/6Qv;->b(Landroid/os/Bundle;)LX/0P1;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/6QI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1086776
    return-void
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1086734
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1086735
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->p:LX/6QI;

    const-string v1, "neko_di_app_details_back_pressed"

    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086736
    iget-object v3, v2, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v2, v3

    .line 1086737
    iget-object v3, v2, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1086738
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086739
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v3, v4

    .line 1086740
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1086741
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v4}, LX/6Qv;->b(Landroid/os/Bundle;)LX/0P1;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/6QI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1086742
    return-void
.end method
