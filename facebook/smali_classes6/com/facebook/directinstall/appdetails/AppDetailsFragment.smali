.class public Lcom/facebook/directinstall/appdetails/AppDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/support/v7/widget/RecyclerView;

.field private B:Lcom/facebook/resources/ui/FbTextView;

.field public C:Lcom/facebook/resources/ui/FbTextView;

.field private D:Lcom/facebook/resources/ui/FbTextView;

.field private E:Lcom/facebook/resources/ui/FbTextView;

.field public F:Landroid/widget/LinearLayout;

.field public final G:LX/6Q6;

.field public final H:LX/6Q6;

.field private final I:LX/6Q5;

.field private J:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

.field public c:LX/0s6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6Q9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6QH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/6QD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/6QI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/content/pm/PackageManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/6VP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

.field public o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Lcom/facebook/directinstall/feed/progressservice/ProgressService;

.field public t:Landroid/content/ServiceConnection;

.field public u:I

.field public v:Landroid/view/View;

.field public w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

.field public y:Landroid/widget/Button;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1086861
    const-class v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->a:Ljava/lang/String;

    .line 1086862
    const-class v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1086863
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1086864
    new-instance v0, LX/6Q6;

    invoke-direct {v0, p0}, LX/6Q6;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->G:LX/6Q6;

    .line 1086865
    new-instance v0, LX/6Q6;

    invoke-direct {v0, p0}, LX/6Q6;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->H:LX/6Q6;

    .line 1086866
    new-instance v0, LX/6Q5;

    invoke-direct {v0, p0}, LX/6Q5;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->I:LX/6Q5;

    .line 1086867
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 1086868
    const v0, 0x7f0d11f7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->z:Landroid/widget/LinearLayout;

    .line 1086869
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086870
    iget-object v1, v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v0, v1

    .line 1086871
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1086872
    iget-object v1, v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->n:LX/0Px;

    move-object v4, v1

    .line 1086873
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;

    .line 1086874
    if-eqz v1, :cond_0

    .line 1086875
    iget-object v7, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1086876
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1086877
    iget v8, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->b:I

    move v8, v8

    .line 1086878
    iget v9, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->c:I

    move v1, v9

    .line 1086879
    if-eqz v7, :cond_0

    if-lez v8, :cond_0

    if-lez v1, :cond_0

    .line 1086880
    new-instance v9, LX/6QF;

    invoke-direct {v9, v7, v8, v1}, LX/6QF;-><init>(Landroid/net/Uri;II)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1086881
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1086882
    :cond_1
    move-object v1, v3

    .line 1086883
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->h:LX/6QH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    iget-object v4, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->o:Ljava/util/Map;

    .line 1086884
    new-instance v6, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/6QI;->a(LX/0QB;)LX/6QI;

    move-result-object v12

    check-cast v12, LX/6QI;

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    move-object v10, v1

    invoke-direct/range {v6 .. v12}, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;-><init>(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Ljava/util/List;Lcom/facebook/content/SecureContextHelper;LX/6QI;)V

    .line 1086885
    move-object v0, v6

    .line 1086886
    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->J:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1086887
    const v0, 0x7f0d11f8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->A:Landroid/support/v7/widget/RecyclerView;

    .line 1086888
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->A:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->J:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1086889
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->A:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->I:LX/6Q5;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1086890
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 1086891
    invoke-virtual {v0, v5}, LX/1P1;->b(I)V

    .line 1086892
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->A:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1086893
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1086894
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->z:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1086895
    :goto_1
    return-void

    .line 1086896
    :cond_2
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    .line 1086897
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086898
    iget-object v1, v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v1, v1

    .line 1086899
    const/16 v2, 0x8

    .line 1086900
    iget-boolean v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->r:Z

    if-eqz v0, :cond_6

    .line 1086901
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1086902
    :goto_0
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    .line 1086903
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1086904
    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setTitleText(Ljava/lang/String;)V

    .line 1086905
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    .line 1086906
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1086907
    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setSubtitleText(Ljava/lang/String;)V

    .line 1086908
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->E:Lcom/facebook/resources/ui/FbTextView;

    .line 1086909
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1086910
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1086911
    iget-object v0, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1086912
    if-eqz v0, :cond_4

    .line 1086913
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    .line 1086914
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1086915
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setImageUri(Landroid/net/Uri;)V

    .line 1086916
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->B:Lcom/facebook/resources/ui/FbTextView;

    .line 1086917
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1086918
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1086919
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->G:LX/6Q6;

    .line 1086920
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1086921
    iput-object v2, v0, LX/6Q6;->b:Ljava/lang/String;

    .line 1086922
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->H:LX/6Q6;

    .line 1086923
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1086924
    iput-object v2, v0, LX/6Q6;->b:Ljava/lang/String;

    .line 1086925
    const-string v0, ""

    .line 1086926
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object v2, v2

    .line 1086927
    if-eqz v2, :cond_1

    .line 1086928
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1086929
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object v2, v2

    .line 1086930
    iget-object v3, v2, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1086931
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1086932
    :cond_1
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object v2, v2

    .line 1086933
    if-eqz v2, :cond_3

    .line 1086934
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1086935
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1086936
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1086937
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object v1, v2

    .line 1086938
    iget-object v2, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1086939
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1086940
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1086941
    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1086942
    :goto_2
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    const v1, 0x7f0d11f6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 1086943
    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    const v2, 0x7f0d09a9

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 1086944
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    const v3, 0x7f0d11f5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 1086945
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    const v4, 0x7f0d10d8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 1086946
    iget-object v4, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    .line 1086947
    iput-object v0, v4, LX/6QD;->d:Landroid/widget/ProgressBar;

    .line 1086948
    iput-object v1, v4, LX/6QD;->g:Landroid/widget/ImageButton;

    .line 1086949
    iput-object v2, v4, LX/6QD;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1086950
    iput-object v3, v4, LX/6QD;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1086951
    iget-object v5, v4, LX/6QD;->g:Landroid/widget/ImageButton;

    new-instance p0, LX/6QB;

    invoke-direct {p0, v4}, LX/6QB;-><init>(LX/6QD;)V

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1086952
    invoke-virtual {v4}, LX/6QD;->a()V

    .line 1086953
    return-void

    .line 1086954
    :cond_4
    iget v0, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->e:I

    move v0, v0

    .line 1086955
    if-eqz v0, :cond_0

    .line 1086956
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setImageResource(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1086957
    :cond_5
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2

    .line 1086958
    :cond_6
    iget-object v0, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->m:LX/0Px;

    move-object v0, v0

    .line 1086959
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1086960
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1086961
    iget-object v0, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->m:LX/0Px;

    move-object v0, v0

    .line 1086962
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;

    .line 1086963
    iget-object v3, v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1086964
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 1086965
    :cond_7
    iget v0, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->f:I

    move v0, v0

    .line 1086966
    if-nez v0, :cond_8

    .line 1086967
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1086968
    :cond_8
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1086969
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1086970
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1086971
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-static {v0}, LX/6Qa;->a(LX/0QB;)LX/6Qa;

    move-result-object v5

    check-cast v5, LX/6Q9;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const-class v9, LX/6QH;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/6QH;

    new-instance p1, LX/6QD;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p1, v10, v11, v12}, LX/6QD;-><init>(Landroid/content/Context;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object v10, p1

    check-cast v10, LX/6QD;

    invoke-static {v0}, LX/6QI;->a(LX/0QB;)LX/6QI;

    move-result-object v11

    check-cast v11, LX/6QI;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object p1

    check-cast p1, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/6VP;->b(LX/0QB;)LX/6VP;

    move-result-object v0

    check-cast v0, LX/6VP;

    iput-object v4, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->c:LX/0s6;

    iput-object v5, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->d:LX/6Q9;

    iput-object v6, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->e:LX/03V;

    iput-object v7, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->f:LX/0W3;

    iput-object v8, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->h:LX/6QH;

    iput-object v10, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    iput-object v11, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->j:LX/6QI;

    iput-object v12, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->k:LX/0Uh;

    iput-object p1, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->l:Landroid/content/pm/PackageManager;

    iput-object v0, v3, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->m:LX/6VP;

    .line 1086972
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1086973
    invoke-static {v0}, LX/6Qv;->a(Landroid/os/Bundle;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086974
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1086975
    new-instance v1, Ljava/util/HashMap;

    invoke-static {v0}, LX/6Qv;->b(Landroid/os/Bundle;)LX/0P1;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1086976
    const-string v3, "app_details"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086977
    move-object v0, v1

    .line 1086978
    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->o:Ljava/util/Map;

    .line 1086979
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1086980
    const-string v1, "IS_WATCH_AND_DIRECT_INSTALL"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->r:Z

    .line 1086981
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1086982
    const-string v1, "WATCH_AND_DIRECT_INSTALL_DUMMY_VIDEO_VIEW_HEIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->u:I

    .line 1086983
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086984
    iget-object v1, v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v0, v1

    .line 1086985
    if-nez v0, :cond_1

    .line 1086986
    :cond_0
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->e:LX/03V;

    sget-object v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->a:Ljava/lang/String;

    const-string v2, "Missing app data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086987
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2a4f13dc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1086988
    const v0, 0x7f03068e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    .line 1086989
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f1

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->x:Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    .line 1086990
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f0

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1086991
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f3

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    .line 1086992
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f2

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->D:Lcom/facebook/resources/ui/FbTextView;

    .line 1086993
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f4

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    .line 1086994
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11fa

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->E:Lcom/facebook/resources/ui/FbTextView;

    .line 1086995
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    new-instance v2, LX/6Q4;

    invoke-direct {v2, p0}, LX/6Q4;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1086996
    const/4 v3, 0x0

    .line 1086997
    iget-boolean v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->r:Z

    if-nez v0, :cond_3

    .line 1086998
    :goto_0
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->a(Landroid/view/View;)V

    .line 1086999
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d0cbe

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->B:Lcom/facebook/resources/ui/FbTextView;

    .line 1087000
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const v2, 0x7f0d11f9

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    .line 1087001
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1087002
    iget-object v2, v0, Lcom/facebook/directinstall/intent/DirectInstallAppData;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object v0, v2

    .line 1087003
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1087004
    new-instance p1, LX/3DI;

    const-string v2, " "

    invoke-direct {p1, v2}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 1087005
    iget-object v2, v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1087006
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    .line 1087007
    :goto_1
    iget-object p2, v0, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;->j:Ljava/lang/String;

    move-object p2, p2

    .line 1087008
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_5

    .line 1087009
    :goto_2
    if-eqz v2, :cond_0

    .line 1087010
    const p2, 0x7f081ea4

    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->G:LX/6Q6;

    invoke-virtual {p1, p2, p3, v5}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 1087011
    :cond_0
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 1087012
    const v2, 0x7f081ea5

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 1087013
    :cond_1
    if-eqz v3, :cond_2

    .line 1087014
    const v2, 0x7f081ea3

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->H:LX/6Q6;

    invoke-virtual {p1, v2, v3, v5}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 1087015
    :cond_2
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, p1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1087016
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1087017
    invoke-direct {p0}, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->e()V

    .line 1087018
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x2b4b5031

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    .line 1087019
    :cond_3
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    iget v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->u:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 1087020
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_4
    move v2, v5

    .line 1087021
    goto :goto_1

    :cond_5
    move v3, v5

    .line 1087022
    goto :goto_2
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x265eb52e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1087023
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1087024
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1087025
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->t:Landroid/content/ServiceConnection;

    if-eqz v2, :cond_0

    .line 1087026
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->t:Landroid/content/ServiceConnection;

    const v3, -0x1a093ffd

    invoke-static {v1, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 1087027
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->t:Landroid/content/ServiceConnection;

    .line 1087028
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6b90b271

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x32bc3e3a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1087029
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1087030
    iget-boolean v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->p:Z

    if-nez v1, :cond_0

    .line 1087031
    iget-object v1, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->f:LX/0W3;

    sget-wide v2, LX/0X5;->dR:J

    invoke-interface {v1, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v1

    .line 1087032
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 1087033
    iget-boolean v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->r:Z

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 1087034
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->m:LX/6VP;

    .line 1087035
    iget-object v4, v3, LX/6VP;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gt;

    .line 1087036
    iput-object v1, v4, LX/0gt;->a:Ljava/lang/String;

    .line 1087037
    move-object v4, v4

    .line 1087038
    sget-object p0, LX/1x2;->LCAU:LX/1x2;

    invoke-virtual {v4, p0}, LX/0gt;->a(LX/1x2;)LX/0gt;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0gt;->a(Landroid/content/Context;)V

    .line 1087039
    :cond_0
    :goto_0
    const v1, -0xa9919d2

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1087040
    :cond_1
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->m:LX/6VP;

    invoke-virtual {v2, v1}, LX/6VP;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
