.class public Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public p:LX/6QI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1087145
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;

    invoke-static {v0}, LX/6QI;->a(LX/0QB;)LX/6QI;

    move-result-object v0

    check-cast v0, LX/6QI;

    iput-object v0, p0, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->p:LX/6QI;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1087146
    const-string v0, "neko_di_app_details_screenshots"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1087147
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1087148
    invoke-static {p0, p0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1087149
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1087150
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LX/6Qv;->a(Landroid/os/Bundle;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->q:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1087151
    invoke-virtual {p0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LX/6Qv;->b(Landroid/os/Bundle;)LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->r:Ljava/util/Map;

    .line 1087152
    const-string v1, "screenshot_url_list"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1087153
    const-string v1, "screenshot_current_position"

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1087154
    if-eqz v3, :cond_0

    if-gez v4, :cond_1

    .line 1087155
    :cond_0
    :goto_0
    return-void

    .line 1087156
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 1087157
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1087158
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087159
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1087160
    :cond_2
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->setContentView(I)V

    .line 1087161
    const v0, 0x7f0d03c6

    invoke-virtual {p0, v0}, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 1087162
    new-instance v1, LX/70i;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    invoke-direct {v1, v3, p0}, LX/70i;-><init>(LX/0gc;Landroid/content/Context;)V

    .line 1087163
    iget-object v3, v1, LX/70i;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1087164
    iget-object v3, v1, LX/70i;->b:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1087165
    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 1087166
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1087167
    invoke-virtual {v0, v4, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1087168
    new-instance v1, LX/6QE;

    invoke-direct {v1, p0}, LX/6QE;-><init>(Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    goto :goto_0
.end method
