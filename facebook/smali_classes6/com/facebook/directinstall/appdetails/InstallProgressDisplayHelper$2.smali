.class public final Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6QD;


# direct methods
.method public constructor <init>(LX/6QD;)V
    .locals 0

    .prologue
    .line 1087050
    iput-object p1, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1087051
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget-object v1, v0, LX/6QD;->d:Landroid/widget/ProgressBar;

    .line 1087052
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    .line 1087053
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget v2, v2, LX/6QD;->m:I

    if-le v0, v2, :cond_0

    .line 1087054
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget v0, v0, LX/6QD;->m:I

    .line 1087055
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget-object v2, v2, LX/6QD;->d:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget v3, v3, LX/6QD;->m:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1087056
    :cond_0
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget v2, v2, LX/6QD;->m:I

    if-ge v0, v2, :cond_1

    .line 1087057
    iget-object v2, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget-object v2, v2, LX/6QD;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1087058
    iget-object v4, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget-wide v4, v4, LX/6QD;->l:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const/high16 v3, 0x43480000    # 200.0f

    div-float/2addr v2, v3

    .line 1087059
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    .line 1087060
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget-object v3, v3, LX/6QD;->k:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    .line 1087061
    iget-object v3, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    iget v3, v3, LX/6QD;->m:I

    sub-int/2addr v3, v0

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1087062
    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1087063
    iget-object v0, p0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;->a:LX/6QD;

    const/16 v1, 0xa

    invoke-static {v0, v1}, LX/6QD;->d(LX/6QD;I)V

    .line 1087064
    :cond_1
    return-void
.end method
