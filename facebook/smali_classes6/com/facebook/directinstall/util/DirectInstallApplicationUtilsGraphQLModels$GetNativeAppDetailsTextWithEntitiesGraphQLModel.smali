.class public final Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6df63439
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1089359
    const-class v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1089358
    const-class v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1089356
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1089357
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1089353
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1089354
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1089355
    return-void
.end method

.method public static a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;)Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;
    .locals 11

    .prologue
    .line 1089326
    if-nez p0, :cond_0

    .line 1089327
    const/4 p0, 0x0

    .line 1089328
    :goto_0
    return-object p0

    .line 1089329
    :cond_0
    instance-of v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    if-eqz v0, :cond_1

    .line 1089330
    check-cast p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    goto :goto_0

    .line 1089331
    :cond_1
    new-instance v2, LX/6RC;

    invoke-direct {v2}, LX/6RC;-><init>()V

    .line 1089332
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1089333
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1089334
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;

    invoke-static {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;->a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;)Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1089335
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1089336
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/6RC;->a:LX/0Px;

    .line 1089337
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/6RC;->b:Ljava/lang/String;

    .line 1089338
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1089339
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1089340
    iget-object v5, v2, LX/6RC;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1089341
    iget-object v7, v2, LX/6RC;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1089342
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 1089343
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 1089344
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1089345
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1089346
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1089347
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1089348
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1089349
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1089350
    new-instance v5, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    invoke-direct {v5, v4}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;-><init>(LX/15i;)V

    .line 1089351
    move-object p0, v5

    .line 1089352
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1089360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1089361
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1089362
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1089363
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1089364
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1089365
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1089366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1089367
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1089324
    iget-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel$RangesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->e:Ljava/util/List;

    .line 1089325
    iget-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1089316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1089317
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1089318
    invoke-virtual {p0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1089319
    if-eqz v1, :cond_0

    .line 1089320
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    .line 1089321
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->e:Ljava/util/List;

    .line 1089322
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1089323
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1089313
    new-instance v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;-><init>()V

    .line 1089314
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1089315
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1089311
    iget-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->f:Ljava/lang/String;

    .line 1089312
    iget-object v0, p0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1089309
    const v0, 0x3b434528

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1089310
    const v0, -0x726d476c

    return v0
.end method
