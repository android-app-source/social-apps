.class public Lcom/facebook/directinstall/feed/InstallDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/13v;


# instance fields
.field public p:LX/6Qa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1087811
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    invoke-static {v0}, LX/6Qa;->a(LX/0QB;)LX/6Qa;

    move-result-object v0

    check-cast v0, LX/6Qa;

    iput-object v0, p0, Lcom/facebook/directinstall/feed/InstallDialogActivity;->p:LX/6Qa;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1087812
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1087813
    invoke-static {p0, p0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1087814
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->setContentView(I)V

    .line 1087815
    invoke-virtual {p0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1087816
    iget-object v1, p0, Lcom/facebook/directinstall/feed/InstallDialogActivity;->p:LX/6Qa;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, LX/6Qv;->a(Landroid/os/Bundle;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v2

    .line 1087817
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 1087818
    const-string v3, "analytics"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 1087819
    if-eqz v5, :cond_0

    .line 1087820
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1087821
    invoke-virtual {v5, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v4, v3, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1087822
    :cond_0
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v3, v3

    .line 1087823
    const-string v4, "can_skip_permissions"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move v0, v4

    .line 1087824
    invoke-virtual {v1, p0, v2, v3, v0}, LX/6Qa;->a(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Z)Z

    .line 1087825
    return-void
.end method
