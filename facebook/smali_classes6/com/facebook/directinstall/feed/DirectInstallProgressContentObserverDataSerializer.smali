.class public Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1087792
    const-class v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    new-instance v1, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;

    invoke-direct {v1}, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1087793
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1087804
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1087805
    if-nez p0, :cond_0

    .line 1087806
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1087807
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1087808
    invoke-static {p0, p1, p2}, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;->b(Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;LX/0nX;LX/0my;)V

    .line 1087809
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1087810
    return-void
.end method

.method private static b(Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1087795
    const-string v0, "storyCacheId"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087796
    const-string v0, "packageName"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->packageName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087797
    const-string v0, "appName"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087798
    const-string v0, "appLaunchUrl"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appLaunchUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087799
    const-string v0, "iconUrl"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->iconUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087800
    const-string v0, "appId"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087801
    const-string v0, "trackingCodes"

    iget-object v1, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->trackingCodes:LX/162;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;LX/0gT;)V

    .line 1087802
    const-string v0, "updateId"

    iget-wide v2, p0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->updateId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1087803
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1087794
    check-cast p1, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    invoke-static {p1, p2, p3}, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;->a(Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;LX/0nX;LX/0my;)V

    return-void
.end method
