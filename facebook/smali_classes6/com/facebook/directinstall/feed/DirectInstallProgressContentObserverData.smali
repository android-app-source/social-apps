.class public Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;
.end annotation


# instance fields
.field public appId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "appId"
    .end annotation
.end field

.field public appLaunchUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "appLaunchUrl"
    .end annotation
.end field

.field public appName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "appName"
    .end annotation
.end field

.field public iconUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "iconUrl"
    .end annotation
.end field

.field public packageName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "packageName"
    .end annotation
.end field

.field public storyCacheId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "storyCacheId"
    .end annotation
.end field

.field public trackingCodes:LX/162;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "trackingCodes"
    .end annotation
.end field

.field public updateId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "updateId"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1087761
    const-class v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1087762
    const-class v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverDataSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1087763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
