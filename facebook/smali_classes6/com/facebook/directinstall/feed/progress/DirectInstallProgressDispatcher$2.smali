.class public final Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6VZ;

.field public final synthetic e:LX/2v6;


# direct methods
.method public constructor <init>(LX/2v6;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V
    .locals 0

    .prologue
    .line 1087868
    iput-object p1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->e:LX/2v6;

    iput-object p2, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->a:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    iput-object p3, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->d:LX/6VZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1087869
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->e:LX/2v6;

    iget-object v2, v0, LX/2v6;->l:Ljava/util/Set;

    monitor-enter v2

    .line 1087870
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->e:LX/2v6;

    iget-object v0, v0, LX/2v6;->l:Ljava/util/Set;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1087871
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qd;

    .line 1087872
    iget-object v5, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;->a:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-interface {v0, v5}, LX/6Qd;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)V

    .line 1087873
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1087874
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
