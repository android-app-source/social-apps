.class public final Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6Qc;

.field public final synthetic e:LX/2v6;


# direct methods
.method public constructor <init>(LX/2v6;JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V
    .locals 0

    .prologue
    .line 1087826
    iput-object p1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iput-wide p2, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->a:J

    iput-object p4, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->c:Ljava/lang/String;

    iput-object p6, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->d:LX/6Qc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1087827
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-wide v2, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->a:J

    .line 1087828
    invoke-static {v0, v2, v3}, LX/2v6;->a(LX/2v6;J)LX/6Qf;

    move-result-object v5

    move-object v2, v5

    .line 1087829
    if-eqz v2, :cond_0

    iget-wide v0, v2, LX/6Qf;->a:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget v0, v2, LX/6Qf;->b:I

    if-nez v0, :cond_0

    .line 1087830
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2v6;->b(Ljava/lang/String;)V

    .line 1087831
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->d:LX/6Qc;

    invoke-static {v0, v1}, LX/2v6;->b(LX/2v6;LX/6Qc;)V

    .line 1087832
    :goto_0
    return-void

    .line 1087833
    :cond_0
    if-eqz v2, :cond_2

    iget-wide v0, v2, LX/6Qf;->a:J

    iget-wide v4, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->a:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1087834
    iget v0, v2, LX/6Qf;->b:I

    .line 1087835
    packed-switch v0, :pswitch_data_0

    .line 1087836
    :pswitch_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    :goto_1
    move-object v1, v1

    .line 1087837
    iget-boolean v0, v2, LX/6Qf;->c:Z

    .line 1087838
    iget-object v3, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->d:LX/6Qc;

    iget-boolean v2, v2, LX/6Qf;->d:Z

    .line 1087839
    iput-boolean v2, v3, LX/6Qc;->j:Z

    .line 1087840
    move v7, v0

    .line 1087841
    :goto_2
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v8, v0, LX/2v6;->l:Ljava/util/Set;

    monitor-enter v8

    .line 1087842
    :try_start_0
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v0, v0, LX/2v6;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1087843
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->d:LX/6Qc;

    .line 1087844
    new-instance v2, LX/6VZ;

    invoke-direct {v2}, LX/6VZ;-><init>()V

    .line 1087845
    iget-object v3, v0, LX/6Qc;->d:Ljava/lang/String;

    iput-object v3, v2, LX/6VZ;->b:Ljava/lang/String;

    .line 1087846
    iget-object v3, v0, LX/6Qc;->e:Ljava/lang/String;

    iput-object v3, v2, LX/6VZ;->d:Ljava/lang/String;

    .line 1087847
    iget-object v3, v0, LX/6Qc;->f:Ljava/lang/String;

    iput-object v3, v2, LX/6VZ;->c:Ljava/lang/String;

    .line 1087848
    iget-object v3, v0, LX/6Qc;->c:Ljava/lang/String;

    iput-object v3, v2, LX/6VZ;->a:Ljava/lang/String;

    .line 1087849
    iget-object v3, v0, LX/6Qc;->g:Ljava/lang/String;

    iput-object v3, v2, LX/6VZ;->f:Ljava/lang/String;

    .line 1087850
    iget-object v3, v0, LX/6Qc;->i:LX/162;

    iput-object v3, v2, LX/6VZ;->e:LX/162;

    .line 1087851
    move-object v6, v2

    .line 1087852
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-wide v2, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->b:Ljava/lang/String;

    iget-object v4, v6, LX/6VZ;->a:Ljava/lang/String;

    iget-object v5, v6, LX/6VZ;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/2v6;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V

    .line 1087853
    if-nez v7, :cond_1

    .line 1087854
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2v6;->b(Ljava/lang/String;)V

    .line 1087855
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->d:LX/6Qc;

    invoke-static {v0, v1}, LX/2v6;->b(LX/2v6;LX/6Qc;)V

    .line 1087856
    :cond_1
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1087857
    :cond_2
    const/4 v0, 0x0

    .line 1087858
    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, v1, LX/2v6;->b:LX/0s6;

    iget-object v2, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/01H;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1087859
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move v7, v0

    goto :goto_2

    .line 1087860
    :cond_3
    iget-object v1, p0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;->e:LX/2v6;

    iget-object v1, v1, LX/2v6;->c:LX/03V;

    sget-object v2, LX/2v6;->a:Ljava/lang/String;

    const-string v3, "install failed"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087861
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move v7, v0

    goto :goto_2

    .line 1087862
    :pswitch_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    .line 1087863
    :pswitch_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    .line 1087864
    :pswitch_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    .line 1087865
    :pswitch_4
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    .line 1087866
    :pswitch_5
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    .line 1087867
    :pswitch_6
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
    .end packed-switch
.end method
