.class public Lcom/facebook/directinstall/feed/progressservice/ProgressService;
.super LX/0te;
.source ""


# instance fields
.field public a:Landroid/content/ContentResolver;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/os/IBinder;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Qi;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1087934
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 1087935
    new-instance v0, LX/6Qh;

    invoke-direct {v0, p0}, LX/6Qh;-><init>(Lcom/facebook/directinstall/feed/progressservice/ProgressService;)V

    iput-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->d:Landroid/os/IBinder;

    .line 1087936
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->e:Ljava/util/List;

    .line 1087937
    return-void
.end method

.method public static a(Lcom/facebook/directinstall/feed/progressservice/ProgressService;)V
    .locals 4

    .prologue
    .line 1087927
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a:Landroid/content/ContentResolver;

    invoke-static {v0}, LX/6Ql;->a(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v0

    .line 1087928
    new-instance v1, LX/6Qg;

    invoke-direct {v1, p0}, LX/6Qg;-><init>(Lcom/facebook/directinstall/feed/progressservice/ProgressService;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1087929
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1087930
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qk;

    .line 1087931
    iget-object v3, v0, LX/6Qk;->b:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1087932
    :cond_0
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;

    invoke-direct {v2, p0, v1}, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;-><init>(Lcom/facebook/directinstall/feed/progressservice/ProgressService;Ljava/util/Map;)V

    const v1, 0x1ed3c369

    invoke-static {v0, v2, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1087933
    return-void
.end method

.method private static a(Lcom/facebook/directinstall/feed/progressservice/ProgressService;Landroid/content/ContentResolver;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 1087945
    iput-object p1, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->b:Landroid/os/Handler;

    iput-object p3, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;

    invoke-static {v2}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    invoke-static {v2}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a(Lcom/facebook/directinstall/feed/progressservice/ProgressService;Landroid/content/ContentResolver;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6Q7;)V
    .locals 2

    .prologue
    .line 1087946
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->e:Ljava/util/List;

    new-instance v1, LX/6Qi;

    invoke-direct {v1, p1, p2}, LX/6Qi;-><init>(Ljava/lang/String;LX/6Q7;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087947
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1087944
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->d:Landroid/os/IBinder;

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x24

    const v1, 0x2748c0b5

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1087938
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 1087939
    invoke-static {p0, p0}, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1087940
    new-instance v1, LX/6Qj;

    iget-object v2, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->b:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, LX/6Qj;-><init>(Lcom/facebook/directinstall/feed/progressservice/ProgressService;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->f:Landroid/database/ContentObserver;

    .line 1087941
    iget-object v1, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a:Landroid/content/ContentResolver;

    invoke-static {}, LX/6na;->a()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->f:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1087942
    invoke-static {p0}, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->a(Lcom/facebook/directinstall/feed/progressservice/ProgressService;)V

    .line 1087943
    const/16 v1, 0x25

    const v2, -0x4b9212f

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
