.class public final Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:Lcom/facebook/directinstall/feed/progressservice/ProgressService;


# direct methods
.method public constructor <init>(Lcom/facebook/directinstall/feed/progressservice/ProgressService;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1087887
    iput-object p1, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;->b:Lcom/facebook/directinstall/feed/progressservice/ProgressService;

    iput-object p2, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1087888
    iget-object v0, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;->b:Lcom/facebook/directinstall/feed/progressservice/ProgressService;

    iget-object v0, v0, Lcom/facebook/directinstall/feed/progressservice/ProgressService;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qi;

    .line 1087889
    iget-object v1, p0, Lcom/facebook/directinstall/feed/progressservice/ProgressService$2;->a:Ljava/util/Map;

    iget-object v3, v0, LX/6Qi;->b:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Qk;

    .line 1087890
    if-eqz v1, :cond_0

    .line 1087891
    iget-object v0, v0, LX/6Qi;->a:LX/6Q7;

    .line 1087892
    iget v4, v1, LX/6Qk;->d:I

    packed-switch v4, :pswitch_data_0

    .line 1087893
    :goto_1
    :pswitch_0
    goto :goto_0

    .line 1087894
    :cond_1
    return-void

    .line 1087895
    :pswitch_1
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    const v5, 0x7f081e84

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 1087896
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    sget-object v5, LX/6QC;->DOWNLOADING:LX/6QC;

    invoke-virtual {v4, v5}, LX/6QD;->a(LX/6QC;)V

    .line 1087897
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    iget-wide v6, v1, LX/6Qk;->i:J

    .line 1087898
    iget-wide v8, v4, LX/6QD;->i:J

    cmp-long v8, v8, v6

    if-eqz v8, :cond_2

    .line 1087899
    iput-wide v6, v4, LX/6QD;->i:J

    .line 1087900
    invoke-static {v4}, LX/6QD;->b(LX/6QD;)V

    .line 1087901
    :cond_2
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    iget-wide v6, v1, LX/6Qk;->j:J

    .line 1087902
    iget-wide v8, v4, LX/6QD;->i:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_4

    const/4 v8, 0x0

    .line 1087903
    :goto_2
    iget v9, v4, LX/6QD;->m:I

    if-eq v9, v8, :cond_3

    .line 1087904
    iput v8, v4, LX/6QD;->m:I

    .line 1087905
    invoke-static {v4}, LX/6QD;->b(LX/6QD;)V

    .line 1087906
    :cond_3
    goto :goto_1

    .line 1087907
    :pswitch_2
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    const v5, 0x7f081e86

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 1087908
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    sget-object v5, LX/6QC;->INSTALLING:LX/6QC;

    invoke-virtual {v4, v5}, LX/6QD;->a(LX/6QC;)V

    goto :goto_1

    .line 1087909
    :pswitch_3
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    const v5, 0x7f081e89

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 1087910
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    sget-object v5, LX/6QC;->COMPLETED:LX/6QC;

    invoke-virtual {v4, v5}, LX/6QD;->a(LX/6QC;)V

    goto :goto_1

    .line 1087911
    :pswitch_4
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->y:Landroid/widget/Button;

    const v5, 0x7f081e9b

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 1087912
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->i:LX/6QD;

    invoke-virtual {v4}, LX/6QD;->a()V

    .line 1087913
    iget-object v4, v0, LX/6Q7;->a:LX/6Q8;

    iget-object v4, v4, LX/6Q8;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1087914
    :cond_4
    const-wide/16 v8, 0x64

    mul-long/2addr v8, v6

    iget-wide v10, v4, LX/6QD;->i:J

    div-long/2addr v8, v10

    long-to-int v8, v8

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
