.class public final Lcom/facebook/optic/CameraDevice$9$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/5fK;


# direct methods
.method public constructor <init>(LX/5fK;)V
    .locals 0

    .prologue
    .line 971175
    iput-object p1, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 971176
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 971177
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->r:LX/5fY;

    if-eqz v0, :cond_0

    .line 971178
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->r:LX/5fY;

    sget-object v1, LX/5fd;->CANCELLED:LX/5fd;

    invoke-interface {v0, v1, v3}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    .line 971179
    :cond_0
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 971180
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    .line 971181
    iput-boolean v2, v0, LX/5fQ;->u:Z

    .line 971182
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->c:LX/5fS;

    invoke-virtual {v0, v3}, LX/5fS;->a(Ljava/util/List;)V

    .line 971183
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->c:LX/5fS;

    iget-object v1, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v1, v1, LX/5fK;->d:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5fS;->b(Ljava/lang/String;)V

    .line 971184
    iget-object v0, p0, Lcom/facebook/optic/CameraDevice$9$1;->a:LX/5fK;

    iget-object v0, v0, LX/5fK;->d:LX/5fQ;

    invoke-static {v0, v2}, LX/5fQ;->c(LX/5fQ;Z)V

    .line 971185
    :cond_1
    return-void
.end method
