.class public final Lcom/facebook/optic/util/ThreadUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/FutureTask;

.field public final synthetic b:LX/5f5;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/FutureTask;LX/5f5;)V
    .locals 0

    .prologue
    .line 972135
    iput-object p1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    iput-object p2, p0, Lcom/facebook/optic/util/ThreadUtil$1;->b:LX/5f5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 972136
    :try_start_0
    iget-object v0, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    const v1, 0x55ce0d94

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    .line 972137
    iget-object v1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->b:LX/5f5;

    .line 972138
    if-eqz v1, :cond_0

    .line 972139
    new-instance v2, Lcom/facebook/optic/util/ThreadUtil$3;

    invoke-direct {v2, v1, v0}, Lcom/facebook/optic/util/ThreadUtil$3;-><init>(LX/5f5;Ljava/lang/Object;)V

    invoke-static {v2}, LX/5fo;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 972140
    :cond_0
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 972141
    sget-object v0, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 972142
    :goto_0
    return-void

    .line 972143
    :catch_0
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 972144
    sget-object v0, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 972145
    :catch_1
    move-exception v0

    .line 972146
    :try_start_1
    iget-object v1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->b:LX/5f5;

    .line 972147
    if-eqz v1, :cond_1

    .line 972148
    new-instance v2, Lcom/facebook/optic/util/ThreadUtil$2;

    invoke-direct {v2, v1, v0}, Lcom/facebook/optic/util/ThreadUtil$2;-><init>(LX/5f5;Ljava/lang/Exception;)V

    invoke-static {v2}, LX/5fo;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 972149
    :cond_1
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 972150
    sget-object v0, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 972151
    :catchall_0
    move-exception v0

    sget-object v1, LX/5fo;->c:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/facebook/optic/util/ThreadUtil$1;->a:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 972152
    sget-object v1, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v1, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    throw v0
.end method
