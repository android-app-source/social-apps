.class public Lcom/facebook/optic/CameraPreviewView;
.super Landroid/view/TextureView;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:I

.field public c:I

.field private d:LX/5fO;

.field private e:LX/5fO;

.field public f:Z

.field private g:Landroid/view/OrientationEventListener;

.field public h:I

.field private i:I

.field private j:Z

.field private k:LX/5fe;

.field public l:LX/5fb;

.field private m:Landroid/view/GestureDetector;

.field private n:Landroid/view/ScaleGestureDetector;

.field public o:LX/5ff;

.field public p:LX/5fM;

.field public q:Landroid/graphics/Matrix;

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:LX/5fi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 971971
    const-class v0, Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/optic/CameraPreviewView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 971878
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/optic/CameraPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 971879
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 971880
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/optic/CameraPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 971881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 971882
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 971883
    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    .line 971884
    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    .line 971885
    sget-object v2, LX/5fM;->BACK:LX/5fM;

    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 971886
    iput-boolean v1, p0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    .line 971887
    new-instance v2, LX/5fj;

    invoke-direct {v2}, LX/5fj;-><init>()V

    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    .line 971888
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, LX/JuJ;->CameraPreviewView:[I

    invoke-virtual {v2, p2, v3, v1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 971889
    :try_start_0
    const/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-static {v2}, LX/5fO;->fromId(I)LX/5fO;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->d:LX/5fO;

    .line 971890
    const/16 v2, 0x2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-static {v2}, LX/5fO;->fromId(I)LX/5fO;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->e:LX/5fO;

    .line 971891
    const/16 v2, 0x3

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/optic/CameraPreviewView;->f:Z

    .line 971892
    const/16 v2, 0x0

    sget-object v4, LX/5fM;->BACK:LX/5fM;

    invoke-virtual {v4}, LX/5fM;->getInfoId()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 971893
    invoke-static {v2}, LX/5fM;->fromId(I)LX/5fM;

    move-result-object v2

    .line 971894
    iput-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 971895
    const/16 v2, 0x4

    const/4 v4, 0x3

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 971896
    and-int/lit8 v2, v4, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lcom/facebook/optic/CameraPreviewView;->r:Z

    .line 971897
    and-int/lit8 v2, v4, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/optic/CameraPreviewView;->s:Z

    .line 971898
    const/16 v0, 0x5

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/optic/CameraPreviewView;->setMediaOrientationLocked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 971899
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 971900
    invoke-virtual {p0, p0}, Lcom/facebook/optic/CameraPreviewView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 971901
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v2, LX/5fa;

    invoke-direct {v2, p0}, LX/5fa;-><init>(Lcom/facebook/optic/CameraPreviewView;)V

    invoke-direct {v0, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->m:Landroid/view/GestureDetector;

    .line 971902
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v2, LX/5fc;

    invoke-direct {v2, p0}, LX/5fc;-><init>(Lcom/facebook/optic/CameraPreviewView;)V

    invoke-direct {v0, p1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->n:Landroid/view/ScaleGestureDetector;

    .line 971903
    return-void

    :cond_0
    move v2, v1

    .line 971904
    goto :goto_0

    :cond_1
    move v0, v1

    .line 971905
    goto :goto_1

    .line 971906
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 971907
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971908
    iget-boolean v1, v0, LX/5fQ;->m:Z

    if-nez v1, :cond_0

    .line 971909
    invoke-virtual {v0}, LX/5fQ;->j()V

    .line 971910
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/5fQ;->m:Z

    .line 971911
    :cond_0
    return-void
.end method

.method private a(LX/5f5;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fl;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 971912
    invoke-direct {p0}, Lcom/facebook/optic/CameraPreviewView;->j()V

    .line 971913
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971914
    invoke-virtual {v0, p1, p2}, LX/5fQ;->a(LX/5f5;Ljava/lang/String;)V

    .line 971915
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 971916
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 971917
    new-instance v0, LX/5fT;

    invoke-direct {v0, p0, p1}, LX/5fT;-><init>(Lcom/facebook/optic/CameraPreviewView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    .line 971918
    :cond_0
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 971919
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 971920
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/Matrix;)V
    .locals 7

    .prologue
    const/high16 v6, 0x447a0000    # 1000.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/high16 v5, -0x3b860000    # -1000.0f

    .line 971921
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v4, v4, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 971922
    invoke-virtual {p1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 971923
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 971924
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971925
    iget-object v4, v0, LX/5fQ;->i:LX/5fM;

    move-object v0, v4

    .line 971926
    sget-object v4, LX/5fM;->FRONT:LX/5fM;

    if-ne v0, v4, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 971927
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971928
    invoke-virtual {v0}, LX/5fQ;->f()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 971929
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 971930
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v5, v5, v6, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 971931
    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 971932
    invoke-virtual {v3, v0, v3}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 971933
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->q:Landroid/graphics/Matrix;

    .line 971934
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->q:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 971935
    return-void

    :cond_0
    move v0, v1

    .line 971936
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/optic/CameraPreviewView;I)V
    .locals 2

    .prologue
    .line 971937
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971938
    new-instance v1, LX/5fV;

    invoke-direct {v1, p0, p1}, LX/5fV;-><init>(Lcom/facebook/optic/CameraPreviewView;I)V

    invoke-virtual {v0, p1, v1}, LX/5fQ;->a(ILX/5f5;)V

    .line 971939
    return-void
.end method

.method public static a$redex0(Lcom/facebook/optic/CameraPreviewView;IIII)V
    .locals 6

    .prologue
    .line 971940
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/optic/CameraPreviewView;->getTransform(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 971941
    int-to-float v0, p1

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 971942
    sget-object v2, LX/5fQ;->b:LX/5fQ;

    move-object v2, v2

    .line 971943
    invoke-virtual {v2}, LX/5fQ;->f()I

    move-result v2

    .line 971944
    const/16 v3, 0x5a

    if-eq v2, v3, :cond_0

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_1

    :cond_0
    move v5, p3

    move p3, p4

    move p4, v5

    .line 971945
    :cond_1
    int-to-float v2, p3

    int-to-float v3, p4

    div-float/2addr v2, v3

    .line 971946
    cmpl-float v0, v2, v0

    if-lez v0, :cond_2

    .line 971947
    int-to-float v0, p2

    int-to-float v2, p4

    div-float/2addr v0, v2

    .line 971948
    :goto_0
    int-to-float v2, p3

    int-to-float v3, p1

    div-float/2addr v2, v3

    mul-float/2addr v2, v0

    .line 971949
    int-to-float v3, p4

    int-to-float v4, p2

    div-float/2addr v3, v4

    mul-float/2addr v0, v3

    .line 971950
    div-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p2, 0x2

    int-to-float v4, v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 971951
    invoke-virtual {p0, v1}, Lcom/facebook/optic/CameraPreviewView;->setTransform(Landroid/graphics/Matrix;)V

    .line 971952
    invoke-direct {p0, v1}, Lcom/facebook/optic/CameraPreviewView;->a(Landroid/graphics/Matrix;)V

    .line 971953
    return-void

    .line 971954
    :cond_2
    int-to-float v0, p1

    int-to-float v2, p3

    div-float/2addr v0, v2

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/optic/CameraPreviewView;[F)V
    .locals 2

    .prologue
    .line 971955
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 971956
    iget-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->q:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 971957
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 971958
    return-void
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 971959
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971960
    iget-boolean v1, v0, LX/5fQ;->m:Z

    move v0, v1

    .line 971961
    return v0
.end method

.method public static d()Z
    .locals 2

    .prologue
    .line 971869
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971870
    iget-boolean v1, v0, LX/5fQ;->A:Z

    move v0, v1

    .line 971871
    return v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 971962
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971963
    invoke-virtual {v0}, LX/5fQ;->r()Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 971964
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971965
    invoke-virtual {v0}, LX/5fQ;->s()Z

    move-result v0

    return v0
.end method

.method public static getDisplayRotation(Lcom/facebook/optic/CameraPreviewView;)I
    .locals 2

    .prologue
    .line 971966
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 971967
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method private h()V
    .locals 10

    .prologue
    .line 971968
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971969
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    invoke-static {p0}, Lcom/facebook/optic/CameraPreviewView;->getDisplayRotation(Lcom/facebook/optic/CameraPreviewView;)I

    move-result v3

    iget v4, p0, Lcom/facebook/optic/CameraPreviewView;->b:I

    iget v5, p0, Lcom/facebook/optic/CameraPreviewView;->c:I

    iget-object v6, p0, Lcom/facebook/optic/CameraPreviewView;->e:LX/5fO;

    iget-object v7, p0, Lcom/facebook/optic/CameraPreviewView;->d:LX/5fO;

    iget-object v8, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    new-instance v9, LX/5fU;

    invoke-direct {v9, p0}, LX/5fU;-><init>(Lcom/facebook/optic/CameraPreviewView;)V

    invoke-virtual/range {v0 .. v9}, LX/5fQ;->a(Landroid/graphics/SurfaceTexture;LX/5fM;IIILX/5fO;LX/5fO;LX/5fi;LX/5f5;)V

    .line 971970
    return-void
.end method

.method private static i()V
    .locals 2

    .prologue
    .line 971972
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971973
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5fQ;->a(LX/5f5;)V

    .line 971974
    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 971975
    iget-boolean v0, p0, Lcom/facebook/optic/CameraPreviewView;->j:Z

    if-eqz v0, :cond_1

    .line 971976
    :cond_0
    :goto_0
    return-void

    .line 971977
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 971978
    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 971979
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 971980
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/optic/CameraPreviewView;->i:I

    .line 971981
    invoke-static {p0}, Lcom/facebook/optic/CameraPreviewView;->getDisplayRotation(Lcom/facebook/optic/CameraPreviewView;)I

    move-result v1

    .line 971982
    if-nez v1, :cond_3

    .line 971983
    invoke-virtual {v0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 971984
    :cond_2
    :goto_1
    iput-boolean v3, p0, Lcom/facebook/optic/CameraPreviewView;->j:Z

    goto :goto_0

    .line 971985
    :cond_3
    if-ne v1, v3, :cond_4

    .line 971986
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 971987
    :cond_4
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 971988
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1
.end method

.method private k()V
    .locals 2

    .prologue
    .line 971989
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 971990
    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 971991
    :cond_0
    :goto_0
    return-void

    .line 971992
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/optic/CameraPreviewView;->j:Z

    if-eqz v0, :cond_0

    .line 971993
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 971994
    iget v1, p0, Lcom/facebook/optic/CameraPreviewView;->i:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 971995
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/optic/CameraPreviewView;->j:Z

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 971996
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/optic/CameraPreviewView;->setFocusCallbackListener(LX/5fY;)V

    .line 971997
    return-void
.end method


# virtual methods
.method public final a(LX/5f4;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f4",
            "<[B",
            "LX/5fk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 971998
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971999
    new-instance v1, LX/5fX;

    invoke-direct {v1, p0, p1}, LX/5fX;-><init>(Lcom/facebook/optic/CameraPreviewView;LX/5f4;)V

    .line 972000
    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 972001
    iget-boolean v2, v0, LX/5fQ;->A:Z

    move v2, v2

    .line 972002
    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/5fQ;->n:Z

    if-nez v2, :cond_0

    .line 972003
    :goto_0
    return-void

    .line 972004
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/5fQ;->l:Z

    .line 972005
    new-instance v2, Ljava/util/concurrent/FutureTask;

    new-instance v3, LX/5f7;

    invoke-direct {v3, v0}, LX/5f7;-><init>(LX/5fQ;)V

    invoke-direct {v2, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 972006
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 972007
    new-instance v3, LX/5f9;

    invoke-direct {v3, v0, v4, v5, v1}, LX/5f9;-><init>(LX/5fQ;JLX/5f4;)V

    invoke-static {v2, v3}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    goto :goto_0

    .line 972008
    :cond_1
    new-instance v2, LX/5fL;

    const-string v3, "Busy taking photo"

    invoke-direct {v2, v0, v3}, LX/5fL;-><init>(LX/5fQ;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/5f4;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(LX/5f5;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 972009
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 972010
    new-instance v1, LX/5fW;

    invoke-direct {v1, p0, p1}, LX/5fW;-><init>(Lcom/facebook/optic/CameraPreviewView;LX/5f5;)V

    iget-object v2, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    .line 972011
    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 972012
    iget-object v3, v0, LX/5fQ;->i:LX/5fM;

    sget-object v4, LX/5fM;->BACK:LX/5fM;

    invoke-virtual {v3, v4}, LX/5fM;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v5, LX/5fM;->FRONT:LX/5fM;

    .line 972013
    :goto_0
    iget-object v4, v0, LX/5fQ;->e:Landroid/graphics/SurfaceTexture;

    iget v6, v0, LX/5fQ;->f:I

    iget v7, v0, LX/5fQ;->g:I

    iget v8, v0, LX/5fQ;->h:I

    iget-object v9, v0, LX/5fQ;->k:LX/5fO;

    iget-object v10, v0, LX/5fQ;->j:LX/5fO;

    move-object v3, v0

    move-object v11, v2

    move-object v12, v1

    invoke-virtual/range {v3 .. v12}, LX/5fQ;->a(Landroid/graphics/SurfaceTexture;LX/5fM;IIILX/5fO;LX/5fO;LX/5fi;LX/5f5;)V

    .line 972014
    :goto_1
    return-void

    .line 972015
    :cond_0
    sget-object v5, LX/5fM;->BACK:LX/5fM;

    goto :goto_0

    .line 972016
    :cond_1
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to switch camera. Camera not initialised."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, LX/5f5;->a(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final a(LX/5f5;LX/5f5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fl;",
            ">;",
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 972017
    invoke-direct {p0}, Lcom/facebook/optic/CameraPreviewView;->k()V

    .line 972018
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 972019
    iget-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    invoke-virtual {v0, p1, p2, v1}, LX/5fQ;->a(LX/5f5;LX/5f5;LX/5fi;)V

    .line 972020
    return-void
.end method

.method public final a(LX/5f5;Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fl;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 972021
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;Ljava/lang/String;)V

    .line 972022
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 971872
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971873
    iget-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    .line 971874
    iget-boolean p0, v0, LX/5fQ;->m:Z

    if-eqz p0, :cond_0

    .line 971875
    const/4 p0, 0x0

    invoke-virtual {v0, p0, v1}, LX/5fQ;->b(LX/5f5;LX/5fi;)V

    .line 971876
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/5fQ;->m:Z

    .line 971877
    :cond_0
    return-void
.end method

.method public getCameraFacing()LX/5fM;
    .locals 1

    .prologue
    .line 971786
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971787
    iget-object p0, v0, LX/5fQ;->i:LX/5fM;

    move-object v0, p0

    .line 971788
    return-object v0
.end method

.method public getFlashMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 971789
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971790
    invoke-virtual {v0}, LX/5fQ;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInitialCameraFacing()LX/5fM;
    .locals 1

    .prologue
    .line 971791
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    return-object v0
.end method

.method public getPreviewFrame()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 971792
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971793
    invoke-virtual {v0}, LX/5fQ;->n()Landroid/graphics/Rect;

    move-result-object v0

    .line 971794
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/optic/CameraPreviewView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedFlashModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 971795
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971796
    invoke-virtual {v0}, LX/5fQ;->p()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6174ad39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 971797
    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    .line 971798
    invoke-virtual {p0}, Lcom/facebook/optic/CameraPreviewView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/optic/CameraPreviewView;->a(Landroid/content/Context;)V

    .line 971799
    const/16 v1, 0x2d

    const v2, -0x6e9ad910

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1f9462d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 971800
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    .line 971801
    iget-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    if-eqz v1, :cond_0

    .line 971802
    iget-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->g:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->disable()V

    .line 971803
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/optic/CameraPreviewView;->setCameraInitialisedCallback(LX/5fb;)V

    .line 971804
    invoke-direct {p0}, Lcom/facebook/optic/CameraPreviewView;->l()V

    .line 971805
    const/16 v1, 0x2d

    const v2, 0x10ef181d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 971806
    iput p2, p0, Lcom/facebook/optic/CameraPreviewView;->b:I

    .line 971807
    iput p3, p0, Lcom/facebook/optic/CameraPreviewView;->c:I

    .line 971808
    invoke-direct {p0}, Lcom/facebook/optic/CameraPreviewView;->h()V

    .line 971809
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 971867
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->i()V

    .line 971868
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 971810
    iput p2, p0, Lcom/facebook/optic/CameraPreviewView;->b:I

    .line 971811
    iput p3, p0, Lcom/facebook/optic/CameraPreviewView;->c:I

    .line 971812
    invoke-static {p0}, Lcom/facebook/optic/CameraPreviewView;->getDisplayRotation(Lcom/facebook/optic/CameraPreviewView;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/optic/CameraPreviewView;->a$redex0(Lcom/facebook/optic/CameraPreviewView;I)V

    .line 971813
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 971814
    iget-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->k:LX/5fe;

    if-eqz v0, :cond_0

    .line 971815
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/optic/CameraPreviewView;->k:LX/5fe;

    .line 971816
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    const v2, 0x730b0b7f

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 971817
    iget-boolean v3, p0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    if-nez v3, :cond_0

    .line 971818
    const v1, -0x19804f36

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 971819
    :goto_0
    return v0

    .line 971820
    :cond_0
    iget-object v3, p0, Lcom/facebook/optic/CameraPreviewView;->m:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 971821
    iget-object v4, p0, Lcom/facebook/optic/CameraPreviewView;->n:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 971822
    if-nez v3, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    const v1, -0x554fb03d

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setCameraInitialisedCallback(LX/5fb;)V
    .locals 1

    .prologue
    .line 971823
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971824
    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 971825
    invoke-interface {p1}, LX/5fb;->a()V

    .line 971826
    :cond_0
    monitor-enter p0

    .line 971827
    :try_start_0
    iput-object p1, p0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    .line 971828
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFlashMode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 971829
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971830
    invoke-virtual {v0, p1}, LX/5fQ;->a(Ljava/lang/String;)V

    .line 971831
    return-void
.end method

.method public setFocusCallbackListener(LX/5fY;)V
    .locals 2

    .prologue
    .line 971832
    if-nez p1, :cond_0

    .line 971833
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971834
    const/4 v1, 0x0

    .line 971835
    iput-object v1, v0, LX/5fQ;->r:LX/5fY;

    .line 971836
    :goto_0
    return-void

    .line 971837
    :cond_0
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971838
    new-instance v1, LX/5fZ;

    invoke-direct {v1, p0, p1}, LX/5fZ;-><init>(Lcom/facebook/optic/CameraPreviewView;LX/5fY;)V

    .line 971839
    iput-object v1, v0, LX/5fQ;->r:LX/5fY;

    .line 971840
    goto :goto_0
.end method

.method public setHdr(Z)V
    .locals 1

    .prologue
    .line 971841
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971842
    invoke-virtual {v0, p1}, LX/5fQ;->b(Z)V

    .line 971843
    return-void
.end method

.method public setInitialCameraFacing(LX/5fM;)V
    .locals 0

    .prologue
    .line 971844
    iput-object p1, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 971845
    return-void
.end method

.method public setMediaOrientationLocked(Z)V
    .locals 1

    .prologue
    .line 971846
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971847
    const/4 p0, 0x0

    iput p0, v0, LX/5fQ;->c:I

    .line 971848
    iput-boolean p1, v0, LX/5fQ;->o:Z

    .line 971849
    return-void
.end method

.method public setOnPreviewStartedListener(LX/ALQ;)V
    .locals 1

    .prologue
    .line 971850
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971851
    iput-object p1, v0, LX/5fQ;->p:LX/ALQ;

    .line 971852
    return-void
.end method

.method public setOnPreviewStoppedListener(LX/ALR;)V
    .locals 1

    .prologue
    .line 971853
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971854
    iput-object p1, v0, LX/5fQ;->q:LX/ALR;

    .line 971855
    return-void
.end method

.method public setOnSurfaceTextureUpdatedListener(LX/5fe;)V
    .locals 0

    .prologue
    .line 971856
    iput-object p1, p0, Lcom/facebook/optic/CameraPreviewView;->k:LX/5fe;

    .line 971857
    return-void
.end method

.method public setPinchZoomListener(LX/5ff;)V
    .locals 0

    .prologue
    .line 971858
    iput-object p1, p0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    .line 971859
    return-void
.end method

.method public setSizeSetter(LX/5fi;)V
    .locals 0

    .prologue
    .line 971860
    iput-object p1, p0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    .line 971861
    return-void
.end method

.method public setTouchEnabled(Z)V
    .locals 0

    .prologue
    .line 971862
    iput-boolean p1, p0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    .line 971863
    return-void
.end method

.method public setZoomChangeListener(LX/ALT;)V
    .locals 1

    .prologue
    .line 971864
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971865
    iput-object p1, v0, LX/5fQ;->t:LX/ALT;

    .line 971866
    return-void
.end method
