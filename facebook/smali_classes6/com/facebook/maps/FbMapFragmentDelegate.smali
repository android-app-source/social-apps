.class public Lcom/facebook/maps/FbMapFragmentDelegate;
.super Lcom/facebook/maps/delegate/MapFragmentDelegate;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public d:LX/6Zk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3BS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1111560
    invoke-direct {p0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;-><init>()V

    .line 1111561
    return-void
.end method

.method public static a(LX/681;)Lcom/facebook/maps/FbMapFragmentDelegate;
    .locals 1

    .prologue
    .line 1111559
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/maps/FbMapFragmentDelegate;->a(LX/681;Ljava/lang/Integer;)Lcom/facebook/maps/FbMapFragmentDelegate;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/681;Ljava/lang/Integer;)Lcom/facebook/maps/FbMapFragmentDelegate;
    .locals 2

    .prologue
    .line 1111544
    new-instance v0, Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-direct {v0}, Lcom/facebook/maps/FbMapFragmentDelegate;-><init>()V

    .line 1111545
    if-eqz p1, :cond_0

    .line 1111546
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/facebook/maps/FbMapFragmentDelegate;->c:I

    .line 1111547
    :cond_0
    iput-object p0, v0, Lcom/facebook/maps/FbMapFragmentDelegate;->a:LX/681;

    .line 1111548
    return-object v0
.end method

.method private static a(Lcom/facebook/maps/FbMapFragmentDelegate;LX/6Zk;LX/3BS;)V
    .locals 0

    .prologue
    .line 1111558
    iput-object p1, p0, Lcom/facebook/maps/FbMapFragmentDelegate;->d:LX/6Zk;

    iput-object p2, p0, Lcom/facebook/maps/FbMapFragmentDelegate;->e:LX/3BS;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/maps/FbMapFragmentDelegate;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-static {v1}, LX/6Zk;->b(LX/0QB;)LX/6Zk;

    move-result-object v0

    check-cast v0, LX/6Zk;

    invoke-static {v1}, LX/3BS;->a(LX/0QB;)LX/3BS;

    move-result-object v1

    check-cast v1, LX/3BS;

    invoke-static {p0, v0, v1}, Lcom/facebook/maps/FbMapFragmentDelegate;->a(Lcom/facebook/maps/FbMapFragmentDelegate;LX/6Zk;LX/3BS;)V

    return-void
.end method

.method private b(LX/680;)LX/68M;
    .locals 5

    .prologue
    .line 1111555
    new-instance v0, LX/68M;

    iget v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    iget-object v2, p0, Lcom/facebook/maps/FbMapFragmentDelegate;->e:LX/3BS;

    .line 1111556
    iget-object v3, p1, LX/680;->z:Landroid/content/Context;

    move-object v3, v3

    .line 1111557
    const v4, 0x7f0801c7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, LX/68M;-><init>(LX/680;ILX/3BT;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/680;)LX/67m;
    .locals 1

    .prologue
    .line 1111554
    invoke-direct {p0, p1}, Lcom/facebook/maps/FbMapFragmentDelegate;->b(LX/680;)LX/68M;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3d038667

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111549
    invoke-super {p0, p1}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->onCreate(Landroid/os/Bundle;)V

    .line 1111550
    const-class v1, Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-static {v1, p0}, Lcom/facebook/maps/FbMapFragmentDelegate;->a(Ljava/lang/Class;LX/02k;)V

    .line 1111551
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/maps/FbMapFragmentDelegate;->b:Ljava/lang/Boolean;

    .line 1111552
    iget-object v1, p0, Lcom/facebook/maps/FbMapFragmentDelegate;->d:LX/6Zk;

    invoke-virtual {v1}, LX/6Zk;->a()V

    .line 1111553
    const/16 v1, 0x2b

    const v2, -0x77b5bb29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
