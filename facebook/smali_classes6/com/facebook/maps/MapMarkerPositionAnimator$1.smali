.class public final Lcom/facebook/maps/MapMarkerPositionAnimator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:I

.field public final synthetic c:LX/6ax;

.field public final synthetic d:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic e:LX/IJO;

.field public final synthetic f:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic g:LX/6aE;


# direct methods
.method public constructor <init>(LX/6aE;JILX/6ax;Lcom/facebook/android/maps/model/LatLng;LX/IJO;Lcom/facebook/android/maps/model/LatLng;)V
    .locals 0

    .prologue
    .line 1112196
    iput-object p1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->g:LX/6aE;

    iput-wide p2, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->a:J

    iput p4, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->b:I

    iput-object p5, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->c:LX/6ax;

    iput-object p6, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->d:Lcom/facebook/android/maps/model/LatLng;

    iput-object p7, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->e:LX/IJO;

    iput-object p8, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->f:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    .line 1112197
    iget-object v0, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->g:LX/6aE;

    iget-object v0, v0, LX/6aE;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->a:J

    sub-long/2addr v0, v2

    .line 1112198
    iget-object v2, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->g:LX/6aE;

    iget-object v2, v2, LX/6aE;->c:Landroid/view/animation/Interpolator;

    long-to-float v0, v0

    iget v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 1112199
    cmpl-float v1, v0, v10

    if-ltz v1, :cond_1

    .line 1112200
    iget-object v0, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->c:LX/6ax;

    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->d:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, LX/6ax;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1112201
    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->e:LX/IJO;

    .line 1112202
    invoke-static {v1}, LX/6aE;->a(LX/IJO;)V

    .line 1112203
    :cond_0
    :goto_0
    return-void

    .line 1112204
    :cond_1
    float-to-double v2, v0

    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->d:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v1, Lcom/facebook/android/maps/model/LatLng;->b:D

    mul-double/2addr v2, v4

    sub-float v1, v10, v0

    float-to-double v4, v1

    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->f:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v1, Lcom/facebook/android/maps/model/LatLng;->b:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 1112205
    float-to-double v4, v0

    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->d:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v1, Lcom/facebook/android/maps/model/LatLng;->a:D

    mul-double/2addr v4, v6

    sub-float v1, v10, v0

    float-to-double v6, v1

    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->f:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v8, v1, Lcom/facebook/android/maps/model/LatLng;->a:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 1112206
    iget-object v1, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->c:LX/6ax;

    new-instance v6, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v6, v4, v5, v2, v3}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v6}, LX/6ax;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1112207
    cmpg-float v0, v0, v10

    if-gez v0, :cond_0

    .line 1112208
    iget-object v0, p0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;->g:LX/6aE;

    iget-object v0, v0, LX/6aE;->b:Landroid/os/Handler;

    const-wide/16 v2, 0x28

    const v1, 0x4ba4839c    # 2.1563192E7f

    invoke-static {v0, p0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
