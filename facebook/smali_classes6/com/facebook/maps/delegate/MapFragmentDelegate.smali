.class public Lcom/facebook/maps/delegate/MapFragmentDelegate;
.super Landroid/support/v4/app/Fragment;
.source ""


# instance fields
.field public a:LX/681;

.field public b:Ljava/lang/Boolean;

.field public c:I

.field private d:Lcom/facebook/android/maps/MapView;

.field private e:LX/7au;

.field public f:LX/6al;

.field private g:Lcom/google/android/gms/maps/GoogleMapOptions;

.field public h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/6Zz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1111533
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 1111534
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    return-void
.end method


# virtual methods
.method public a(LX/680;)LX/67m;
    .locals 3

    .prologue
    .line 1111535
    new-instance v0, LX/68M;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    invoke-direct {v0, v1, p1, v2}, LX/68M;-><init>(Landroid/content/Context;LX/680;I)V

    return-object v0
.end method

.method public final a()LX/6al;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1111525
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->f:LX/6al;

    if-nez v0, :cond_0

    .line 1111526
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    .line 1111527
    iget-object v1, v0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    move-object v0, v1

    .line 1111528
    if-eqz v0, :cond_0

    .line 1111529
    new-instance v0, LX/6al;

    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    .line 1111530
    iget-object v2, v1, Lcom/facebook/android/maps/MapView;->v:LX/680;

    move-object v1, v2

    .line 1111531
    invoke-direct {v0, v1}, LX/6al;-><init>(LX/680;)V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->f:LX/6al;

    .line 1111532
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->f:LX/6al;

    return-object v0
.end method

.method public final a(LX/6Zz;)V
    .locals 2

    .prologue
    .line 1111517
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1111518
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    new-instance v1, LX/6am;

    invoke-direct {v1, p0, p1}, LX/6am;-><init>(Lcom/facebook/maps/delegate/MapFragmentDelegate;LX/6Zz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1111519
    :goto_0
    return-void

    .line 1111520
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    if-eqz v0, :cond_1

    .line 1111521
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    new-instance v1, LX/6ao;

    invoke-direct {v1, p0, p1}, LX/6ao;-><init>(Lcom/facebook/maps/delegate/MapFragmentDelegate;LX/6Zz;)V

    invoke-virtual {v0, v1}, LX/7au;->a(LX/6an;)V

    goto :goto_0

    .line 1111522
    :cond_1
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->h:Ljava/util/Queue;

    if-nez v0, :cond_2

    .line 1111523
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->h:Ljava/util/Queue;

    .line 1111524
    :cond_2
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->h:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c8c9793

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1111493
    if-eqz p3, :cond_0

    .line 1111494
    const-string v0, "isOxygenEnabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    .line 1111495
    const-string v0, "state_report_button_position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    .line 1111496
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 1111497
    new-instance v0, LX/6aw;

    const-string v2, "You MUST set a MapLibrarySelector on the MapFragmentDelegate before the MapView is initialized."

    invoke-direct {v0, v2}, LX/6aw;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2b

    const v3, -0x739a9994

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0

    .line 1111498
    :cond_1
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1111499
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a:LX/681;

    if-eqz v0, :cond_3

    .line 1111500
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a:LX/681;

    invoke-direct {v0, v2, v3}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;LX/681;)V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    .line 1111501
    :goto_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->h:Ljava/util/Queue;

    if-eqz v0, :cond_2

    .line 1111502
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    new-instance v2, LX/6ap;

    invoke-direct {v2, p0}, LX/6ap;-><init>(Lcom/facebook/maps/delegate/MapFragmentDelegate;)V

    invoke-virtual {v0, v2}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1111503
    :cond_2
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    new-instance v2, LX/6aq;

    invoke-direct {v2, p0}, LX/6aq;-><init>(Lcom/facebook/maps/delegate/MapFragmentDelegate;)V

    invoke-virtual {v0, v2}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1111504
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p3}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 1111505
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    const v2, -0x254034ac

    invoke-static {v2, v1}, LX/02F;->f(II)V

    .line 1111506
    :goto_1
    return-object v0

    .line 1111507
    :cond_3
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    goto :goto_0

    .line 1111508
    :cond_4
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a:LX/681;

    if-eqz v0, :cond_6

    .line 1111509
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a:LX/681;

    invoke-static {v0}, LX/6as;->a(LX/681;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    .line 1111510
    new-instance v0, LX/7au;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0, v2, v3}, LX/7au;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    .line 1111511
    :goto_2
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->h:Ljava/util/Queue;

    if-eqz v0, :cond_5

    .line 1111512
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    new-instance v2, LX/6ar;

    invoke-direct {v2, p0}, LX/6ar;-><init>(Lcom/facebook/maps/delegate/MapFragmentDelegate;)V

    invoke-virtual {v0, v2}, LX/7au;->a(LX/6an;)V

    .line 1111513
    :cond_5
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v0, p3}, LX/7au;->a(Landroid/os/Bundle;)V

    .line 1111514
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/7av;->a(Landroid/content/Context;)I

    .line 1111515
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    const v2, -0x6d940c1b

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_1

    .line 1111516
    :cond_6
    new-instance v0, LX/7au;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/7au;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    goto :goto_2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0xa184edf

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111536
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1111537
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    if-eqz v1, :cond_1

    .line 1111538
    iput-object v3, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    .line 1111539
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->f:LX/6al;

    .line 1111540
    const v1, 0x233ef8bf

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1111541
    :cond_1
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    if-eqz v1, :cond_0

    .line 1111542
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v1}, LX/7au;->f()V

    .line 1111543
    iput-object v3, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    goto :goto_0
.end method

.method public final onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111483
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 1111484
    invoke-static {p2}, LX/6as;->a(Landroid/util/AttributeSet;)I

    move-result v0

    .line 1111485
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1111486
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    .line 1111487
    :cond_0
    invoke-static {p2}, LX/6as;->b(Landroid/util/AttributeSet;)Ljava/lang/Integer;

    move-result-object v0

    .line 1111488
    if-eqz v0, :cond_1

    .line 1111489
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    .line 1111490
    :cond_1
    invoke-static {p1, p2}, LX/681;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a:LX/681;

    .line 1111491
    return-void

    .line 1111492
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 1111478
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 1111479
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111480
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->b()V

    .line 1111481
    :goto_0
    return-void

    .line 1111482
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v0}, LX/7au;->g()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x227d074e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111474
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1111475
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1111476
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v1}, LX/7au;->d()V

    .line 1111477
    :cond_0
    const v1, -0x36e0ba7a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5233231c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111463
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1111464
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1111465
    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v1}, LX/7au;->b()V

    .line 1111466
    :cond_0
    const v1, 0x4348cdfb

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111467
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1111468
    const-string v0, "isOxygenEnabled"

    iget-object v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1111469
    const-string v0, "state_report_button_position"

    iget v1, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1111470
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111471
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->d:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/android/maps/MapView;->b(Landroid/os/Bundle;)V

    .line 1111472
    :goto_0
    return-void

    .line 1111473
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/delegate/MapFragmentDelegate;->e:LX/7au;

    invoke-virtual {v0, p1}, LX/7au;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method
