.class public Lcom/facebook/maps/GoogleEmbeddableMapActivity;
.super Lcom/google/android/maps/MapActivity;
.source ""


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field private b:LX/6Zj;

.field private c:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1111945
    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 1111944
    const/4 v0, 0x0

    return v0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1111942
    iget-object v0, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 1111943
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1111932
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 1111933
    invoke-virtual {p0}, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->c:Landroid/app/Activity;

    .line 1111934
    invoke-virtual {p0}, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "API_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1111935
    const v0, 0x7f030470

    invoke-virtual {p0, v0}, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->setContentView(I)V

    .line 1111936
    const v0, 0x7f0d0d5b

    invoke-virtual {p0, v0}, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->a:Landroid/widget/FrameLayout;

    .line 1111937
    new-instance v0, LX/6Zj;

    invoke-direct {v0, p0, v1}, LX/6Zj;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->b:LX/6Zj;

    .line 1111938
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1111939
    iget-object v1, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->b:LX/6Zj;

    invoke-virtual {v1, v0}, LX/6Zj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1111940
    iget-object v0, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/maps/GoogleEmbeddableMapActivity;->b:LX/6Zj;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1111941
    return-void
.end method
