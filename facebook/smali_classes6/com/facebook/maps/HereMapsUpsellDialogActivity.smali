.class public Lcom/facebook/maps/HereMapsUpsellDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6Zi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1111954
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111955
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/maps/HereMapsUpsellDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "external_map_intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "dest_latitude"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "dset_longitude"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "dest_name"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1111956
    if-eqz p7, :cond_0

    .line 1111957
    const-string v1, "dest_address"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1111958
    :cond_0
    return-object v0
.end method

.method private static a(Lcom/facebook/maps/HereMapsUpsellDialogActivity;LX/6Zi;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1111959
    iput-object p1, p0, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->p:LX/6Zi;

    iput-object p2, p0, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/maps/HereMapsUpsellDialogActivity;

    invoke-static {v1}, LX/6Zi;->a(LX/0QB;)LX/6Zi;

    move-result-object v0

    check-cast v0, LX/6Zi;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->a(Lcom/facebook/maps/HereMapsUpsellDialogActivity;LX/6Zi;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x0

    .line 1111960
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1111961
    invoke-static {p0, p0}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1111962
    invoke-virtual {p0}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1111963
    const-string v1, "external_map_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 1111964
    const-string v1, "dest_latitude"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "dset_longitude"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->p:LX/6Zi;

    const-string v2, "dest_latitude"

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v4

    const-string v2, "dset_longitude"

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v6

    const-string v2, "dest_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "dest_address"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, LX/6a7;

    invoke-direct {v10, p0}, LX/6a7;-><init>(Lcom/facebook/maps/HereMapsUpsellDialogActivity;)V

    move-object v2, p0

    invoke-virtual/range {v1 .. v10}, LX/6Zi;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnDismissListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1111965
    :goto_0
    if-nez v0, :cond_0

    .line 1111966
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v3, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1111967
    invoke-virtual {p0}, Lcom/facebook/maps/HereMapsUpsellDialogActivity;->finish()V

    .line 1111968
    :cond_0
    return-void

    .line 1111969
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
