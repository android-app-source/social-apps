.class public Lcom/facebook/maps/FbMapViewDelegate;
.super LX/6Zn;
.source ""

# interfaces
.implements LX/31N;


# instance fields
.field public c:LX/6Zk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3BS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/121;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/6Zz;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/os/Bundle;

.field public h:LX/0yY;

.field public i:LX/0gc;

.field private j:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1111714
    invoke-direct {p0, p1}, LX/6Zn;-><init>(Landroid/content/Context;)V

    .line 1111715
    invoke-direct {p0}, Lcom/facebook/maps/FbMapViewDelegate;->e()V

    .line 1111716
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/681;)V
    .locals 0

    .prologue
    .line 1111708
    invoke-direct {p0, p1, p2}, LX/6Zn;-><init>(Landroid/content/Context;LX/681;)V

    .line 1111709
    invoke-direct {p0}, Lcom/facebook/maps/FbMapViewDelegate;->e()V

    .line 1111710
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1111711
    invoke-direct {p0, p1, p2}, LX/6Zn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1111712
    invoke-direct {p0}, Lcom/facebook/maps/FbMapViewDelegate;->e()V

    .line 1111713
    return-void
.end method

.method private static a(Lcom/facebook/maps/FbMapViewDelegate;LX/6Zk;LX/3BS;LX/121;)V
    .locals 0

    .prologue
    .line 1111717
    iput-object p1, p0, Lcom/facebook/maps/FbMapViewDelegate;->c:LX/6Zk;

    iput-object p2, p0, Lcom/facebook/maps/FbMapViewDelegate;->d:LX/3BS;

    iput-object p3, p0, Lcom/facebook/maps/FbMapViewDelegate;->e:LX/121;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/maps/FbMapViewDelegate;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/maps/FbMapViewDelegate;

    invoke-static {v2}, LX/6Zk;->b(LX/0QB;)LX/6Zk;

    move-result-object v0

    check-cast v0, LX/6Zk;

    invoke-static {v2}, LX/3BS;->a(LX/0QB;)LX/3BS;

    move-result-object v1

    check-cast v1, LX/3BS;

    invoke-static {v2}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v2

    check-cast v2, LX/121;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/maps/FbMapViewDelegate;->a(Lcom/facebook/maps/FbMapViewDelegate;LX/6Zk;LX/3BS;LX/121;)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1111703
    const-class v0, Lcom/facebook/maps/FbMapViewDelegate;

    invoke-static {v0, p0}, Lcom/facebook/maps/FbMapViewDelegate;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1111704
    iget v0, p0, LX/6Zn;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1111705
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->a:I

    .line 1111706
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->c:LX/6Zk;

    invoke-virtual {v0}, LX/6Zk;->a()V

    .line 1111707
    return-void
.end method

.method private f()Lcom/facebook/resources/ui/FbTextView;
    .locals 4

    .prologue
    .line 1111669
    new-instance v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/maps/FbMapViewDelegate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1111670
    invoke-virtual {p0}, Lcom/facebook/maps/FbMapViewDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1111671
    const v2, 0x7f0801c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1111672
    const v2, -0x958e80

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1111673
    const/4 v2, 0x0

    const v3, 0x7f0b0056

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1111674
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1111675
    const v1, 0x7f02182d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 1111676
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 1111677
    return-object v0
.end method

.method public static setZeroRatingEnabled(Lcom/facebook/maps/FbMapViewDelegate;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1111678
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->h:LX/0yY;

    if-nez v0, :cond_3

    .line 1111679
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->g:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 1111680
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/maps/FbMapViewDelegate;->setEnabled(Z)V

    .line 1111681
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 1111682
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1111683
    :cond_1
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    if-eqz v0, :cond_2

    .line 1111684
    :goto_0
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Zz;

    if-eqz v0, :cond_2

    .line 1111685
    invoke-virtual {p0, v0}, LX/6Zn;->a(LX/6Zz;)V

    goto :goto_0

    .line 1111686
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    .line 1111687
    :goto_1
    return-void

    .line 1111688
    :cond_3
    invoke-virtual {p0, v3}, Lcom/facebook/maps/FbMapViewDelegate;->setEnabled(Z)V

    .line 1111689
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_4

    .line 1111690
    invoke-direct {p0}, Lcom/facebook/maps/FbMapViewDelegate;->f()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 1111691
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/6Zm;

    invoke-direct {v1, p0}, LX/6Zm;-><init>(Lcom/facebook/maps/FbMapViewDelegate;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1111692
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0, v2, v2}, Lcom/facebook/maps/FbMapViewDelegate;->addView(Landroid/view/View;II)V

    .line 1111693
    :cond_4
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/680;)LX/68M;
    .locals 5

    .prologue
    .line 1111694
    new-instance v0, LX/68M;

    iget v1, p0, LX/6Zn;->b:I

    iget-object v2, p0, Lcom/facebook/maps/FbMapViewDelegate;->d:LX/3BS;

    invoke-virtual {p0}, Lcom/facebook/maps/FbMapViewDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0801c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, LX/68M;-><init>(LX/680;ILX/3BT;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(LX/6Zz;)V
    .locals 1

    .prologue
    .line 1111695
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->h:LX/0yY;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/maps/FbMapViewDelegate;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1111696
    :cond_0
    invoke-super {p0, p1}, LX/6Zn;->a(LX/6Zz;)V

    .line 1111697
    :goto_0
    return-void

    .line 1111698
    :cond_1
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    if-nez v0, :cond_2

    .line 1111699
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    .line 1111700
    :cond_2
    iget-object v0, p0, Lcom/facebook/maps/FbMapViewDelegate;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setMapSource(I)V
    .locals 0

    .prologue
    .line 1111701
    iput p1, p0, LX/6Zn;->a:I

    .line 1111702
    return-void
.end method
