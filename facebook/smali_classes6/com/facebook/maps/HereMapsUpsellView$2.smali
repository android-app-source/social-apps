.class public final Lcom/facebook/maps/HereMapsUpsellView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6a9;


# direct methods
.method public constructor <init>(LX/6a9;)V
    .locals 0

    .prologue
    .line 1111979
    iput-object p1, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1111980
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v0, v0, LX/6a9;->i:Ljava/lang/Double;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v0, v0, LX/6a9;->j:Ljava/lang/Double;

    if-nez v0, :cond_5

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v0, v0, LX/6a9;->h:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v0, v0, LX/6a9;->h:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 1111981
    :cond_1
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    invoke-virtual {v0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 1111982
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v1

    .line 1111983
    const/4 v2, 0x0

    .line 1111984
    if-eqz v1, :cond_4

    .line 1111985
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1111986
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 1111987
    if-eqz v2, :cond_3

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1111988
    if-nez v1, :cond_7

    .line 1111989
    :cond_2
    :goto_1
    move v4, v9

    .line 1111990
    if-eqz v4, :cond_6

    :cond_3
    :goto_2
    move-object v2, v1

    .line 1111991
    goto :goto_0

    .line 1111992
    :cond_4
    if-eqz v2, :cond_5

    .line 1111993
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 1111994
    iput-object v1, v0, LX/6a9;->i:Ljava/lang/Double;

    .line 1111995
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 1111996
    iput-object v1, v0, LX/6a9;->j:Ljava/lang/Double;

    .line 1111997
    :cond_5
    iget-object v0, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v1, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v1, v1, LX/6a9;->i:Ljava/lang/Double;

    iget-object v2, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v2, v2, LX/6a9;->j:Ljava/lang/Double;

    iget-object v3, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v3, v3, LX/6a9;->k:Ljava/lang/Double;

    iget-object v4, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v4, v4, LX/6a9;->l:Ljava/lang/Double;

    iget-object v5, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v5, v5, LX/6a9;->f:LX/6a8;

    .line 1111998
    iget-object v6, v5, LX/6a8;->f:Landroid/view/View;

    const v7, 0x7f0d1618

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    move v5, v6

    .line 1111999
    iget-object v6, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v6, v6, LX/6a9;->f:LX/6a8;

    .line 1112000
    iget-object v7, v6, LX/6a8;->f:Landroid/view/View;

    const v8, 0x7f0d1618

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    move v6, v7

    .line 1112001
    invoke-static/range {v0 .. v6}, LX/6a9;->a$redex0(LX/6a9;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;II)Landroid/graphics/Bitmap;

    move-result-object v7

    move-object v0, v7

    .line 1112002
    iget-object v1, p0, Lcom/facebook/maps/HereMapsUpsellView$2;->a:LX/6a9;

    iget-object v1, v1, LX/6a9;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/maps/HereMapsUpsellView$2$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/maps/HereMapsUpsellView$2$1;-><init>(Lcom/facebook/maps/HereMapsUpsellView$2;Landroid/graphics/Bitmap;)V

    const v0, -0x6b134eb1

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1112003
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_2

    .line 1112004
    :cond_7
    if-nez v2, :cond_8

    move v9, v8

    .line 1112005
    goto :goto_1

    .line 1112006
    :cond_8
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v11

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v13

    sub-long/2addr v11, v13

    .line 1112007
    const-wide/32 v13, 0x1d4c0

    cmp-long v7, v11, v13

    if-lez v7, :cond_9

    move v9, v8

    .line 1112008
    goto/16 :goto_1

    .line 1112009
    :cond_9
    const-wide/32 v13, -0x1d4c0

    cmp-long v7, v11, v13

    if-ltz v7, :cond_2

    .line 1112010
    const-wide/16 v13, 0x0

    cmp-long v7, v11, v13

    if-lez v7, :cond_b

    move v7, v8

    .line 1112011
    :goto_3
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v10

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v11

    sub-float/2addr v10, v11

    float-to-int v10, v10

    .line 1112012
    if-gtz v10, :cond_c

    move v12, v8

    .line 1112013
    :goto_4
    const/16 v11, 0xc8

    if-le v10, v11, :cond_d

    move v10, v8

    .line 1112014
    :goto_5
    invoke-virtual {v1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v11

    .line 1112015
    invoke-virtual {v2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v13

    .line 1112016
    if-nez v11, :cond_f

    if-nez v13, :cond_e

    move v11, v8

    .line 1112017
    :goto_6
    if-nez v12, :cond_a

    if-eqz v7, :cond_2

    if-nez v10, :cond_2

    if-eqz v11, :cond_2

    :cond_a
    move v9, v8

    .line 1112018
    goto/16 :goto_1

    :cond_b
    move v7, v9

    .line 1112019
    goto :goto_3

    :cond_c
    move v12, v9

    .line 1112020
    goto :goto_4

    :cond_d
    move v10, v9

    .line 1112021
    goto :goto_5

    :cond_e
    move v11, v9

    .line 1112022
    goto :goto_6

    :cond_f
    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    goto :goto_6
.end method
