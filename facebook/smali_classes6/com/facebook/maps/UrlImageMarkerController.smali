.class public Lcom/facebook/maps/UrlImageMarkerController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:LX/1HI;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1112345
    const-class v0, Lcom/facebook/maps/UrlImageMarkerController;

    const-string v1, "nearby_map"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/maps/UrlImageMarkerController;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1112346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112347
    iput-object p1, p0, Lcom/facebook/maps/UrlImageMarkerController;->b:LX/1HI;

    .line 1112348
    iput-object p2, p0, Lcom/facebook/maps/UrlImageMarkerController;->c:Ljava/util/concurrent/ExecutorService;

    .line 1112349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/UrlImageMarkerController;->d:Ljava/util/List;

    .line 1112350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    .line 1112351
    return-void
.end method


# virtual methods
.method public final a(LX/6ax;Landroid/net/Uri;LX/IJY;)V
    .locals 3

    .prologue
    .line 1112352
    invoke-static {p2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1112353
    iget-object v1, p0, Lcom/facebook/maps/UrlImageMarkerController;->b:LX/1HI;

    sget-object v2, Lcom/facebook/maps/UrlImageMarkerController;->a:Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1112354
    new-instance v1, LX/6aL;

    invoke-direct {v1, p0, v0, p3, p1}, LX/6aL;-><init>(Lcom/facebook/maps/UrlImageMarkerController;LX/1ca;LX/IJY;LX/6ax;)V

    .line 1112355
    iget-object v2, p0, Lcom/facebook/maps/UrlImageMarkerController;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1112356
    iget-object v2, p0, Lcom/facebook/maps/UrlImageMarkerController;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1112357
    return-void
.end method
