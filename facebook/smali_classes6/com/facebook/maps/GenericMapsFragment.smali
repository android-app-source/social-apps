.class public Lcom/facebook/maps/GenericMapsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/6ZZ;
.implements LX/6a3;
.implements LX/6Zz;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final g:[Ljava/lang/String;


# instance fields
.field public a:LX/6Zi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6a5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:D

.field private k:D

.field private l:Lcom/facebook/android/maps/model/LatLng;

.field public m:Lcom/facebook/android/maps/model/LatLng;

.field private n:F

.field private o:Ljava/lang/String;

.field private p:Z

.field public q:Ljava/lang/String;

.field public r:Lcom/facebook/maps/delegate/MapFragmentDelegate;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1111872
    const-class v0, Lcom/facebook/maps/GenericMapsFragment;

    sput-object v0, Lcom/facebook/maps/GenericMapsFragment;->f:Ljava/lang/Class;

    .line 1111873
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/maps/GenericMapsFragment;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1111874
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1111875
    const-string v0, "mechanism_unknown"

    iput-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/maps/GenericMapsFragment;

    invoke-static {v6}, LX/6Zi;->a(LX/0QB;)LX/6Zi;

    move-result-object v2

    check-cast v2, LX/6Zi;

    invoke-static {v6}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v3

    check-cast v3, LX/6Zb;

    invoke-static {v6}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v4

    check-cast v4, LX/0y3;

    new-instance p1, LX/6a5;

    invoke-static {v6}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v5

    check-cast v5, LX/0Zm;

    invoke-static {v6}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    invoke-direct {p1, v5, p0}, LX/6a5;-><init>(LX/0Zm;LX/0Zb;)V

    move-object v5, p1

    check-cast v5, LX/6a5;

    const-class p0, LX/0i4;

    invoke-interface {v6, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0i4;

    iput-object v2, v1, Lcom/facebook/maps/GenericMapsFragment;->a:LX/6Zi;

    iput-object v3, v1, Lcom/facebook/maps/GenericMapsFragment;->b:LX/6Zb;

    iput-object v4, v1, Lcom/facebook/maps/GenericMapsFragment;->c:LX/0y3;

    iput-object v5, v1, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    iput-object v6, v1, Lcom/facebook/maps/GenericMapsFragment;->e:LX/0i4;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/maps/GenericMapsFragment;)V
    .locals 2

    .prologue
    .line 1111881
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->r:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    new-instance v1, LX/6a0;

    invoke-direct {v1, p0}, LX/6a0;-><init>(Lcom/facebook/maps/GenericMapsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a(LX/6Zz;)V

    .line 1111882
    return-void
.end method

.method public static b$redex0(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1111876
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6az;->c(Z)V

    .line 1111877
    invoke-virtual {p1, v2}, LX/6al;->a(Z)V

    .line 1111878
    iput-boolean v2, p0, Lcom/facebook/maps/GenericMapsFragment;->p:Z

    .line 1111879
    invoke-virtual {p1, p0}, LX/6al;->a(LX/6a3;)V

    .line 1111880
    return-void
.end method

.method public static c(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V
    .locals 3

    .prologue
    .line 1111885
    new-instance v0, LX/696;

    invoke-direct {v0}, LX/696;-><init>()V

    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->l:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    .line 1111886
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1111887
    invoke-static {v0, v1}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    const/16 v1, 0x5dc

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/6al;->a(LX/6aM;ILX/6aj;)V

    .line 1111888
    return-void
.end method

.method public static d$redex0(Lcom/facebook/maps/GenericMapsFragment;)V
    .locals 10

    .prologue
    .line 1111883
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->a:LX/6Zi;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/maps/GenericMapsFragment;->o:Ljava/lang/String;

    iget-wide v4, p0, Lcom/facebook/maps/GenericMapsFragment;->j:D

    iget-wide v6, p0, Lcom/facebook/maps/GenericMapsFragment;->k:D

    iget-object v8, p0, Lcom/facebook/maps/GenericMapsFragment;->h:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/maps/GenericMapsFragment;->i:Ljava/lang/String;

    invoke-virtual/range {v1 .. v9}, LX/6Zi;->b(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 1111884
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1111857
    const-string v0, "full_screen_map"

    return-object v0
.end method

.method public final a(LX/6ZY;)V
    .locals 3

    .prologue
    .line 1111858
    sget-object v0, LX/6a2;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1111859
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    const-string v1, "result_failed"

    invoke-virtual {v0, v1}, LX/6a5;->b(Ljava/lang/String;)V

    .line 1111860
    :goto_0
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_1

    .line 1111861
    sget-object v0, Lcom/facebook/maps/GenericMapsFragment;->f:Ljava/lang/Class;

    const-string v1, "Unrecognized mechanism"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1111862
    :cond_1
    :goto_2
    return-void

    .line 1111863
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    const-string v1, "result_succeeded"

    invoke-virtual {v0, v1}, LX/6a5;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1111864
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    const-string v1, "result_canceled"

    invoke-virtual {v0, v1}, LX/6a5;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1111865
    :sswitch_0
    const-string v2, "mechanism_my_location_button"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "mechanism_get_direction_button"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 1111866
    :pswitch_2
    sget-object v0, LX/6ZY;->DIALOG_SUCCESS:LX/6ZY;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/6ZY;->DIALOG_NOT_NEEDED:LX/6ZY;

    if-ne p1, v0, :cond_1

    .line 1111867
    :cond_2
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->c:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_3

    .line 1111868
    invoke-static {p0}, Lcom/facebook/maps/GenericMapsFragment;->b$redex0(Lcom/facebook/maps/GenericMapsFragment;)V

    goto :goto_2

    .line 1111869
    :cond_3
    new-instance v0, LX/6Zy;

    invoke-direct {v0, p0}, LX/6Zy;-><init>(Lcom/facebook/maps/GenericMapsFragment;)V

    .line 1111870
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->e:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    sget-object v2, Lcom/facebook/maps/GenericMapsFragment;->g:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_2

    .line 1111871
    :pswitch_3
    invoke-static {p0}, Lcom/facebook/maps/GenericMapsFragment;->d$redex0(Lcom/facebook/maps/GenericMapsFragment;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1a2a2e1 -> :sswitch_1
        0x21fe750f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/6al;)V
    .locals 2

    .prologue
    .line 1111831
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->l:Lcom/facebook/android/maps/model/LatLng;

    iget v1, p0, Lcom/facebook/maps/GenericMapsFragment;->n:F

    invoke-static {v0, v1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 1111832
    new-instance v0, LX/699;

    invoke-direct {v0}, LX/699;-><init>()V

    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->l:Lcom/facebook/android/maps/model/LatLng;

    .line 1111833
    iput-object v1, v0, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1111834
    move-object v0, v0

    .line 1111835
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->h:Ljava/lang/String;

    .line 1111836
    iput-object v1, v0, LX/699;->i:Ljava/lang/String;

    .line 1111837
    move-object v0, v0

    .line 1111838
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->i:Ljava/lang/String;

    .line 1111839
    iput-object v1, v0, LX/699;->h:Ljava/lang/String;

    .line 1111840
    move-object v0, v0

    .line 1111841
    const v1, 0x7f020e8f

    invoke-static {v1}, LX/690;->a(I)LX/68w;

    move-result-object v1

    .line 1111842
    iput-object v1, v0, LX/699;->c:LX/68w;

    .line 1111843
    move-object v0, v0

    .line 1111844
    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    move-result-object v0

    .line 1111845
    if-eqz v0, :cond_0

    .line 1111846
    invoke-virtual {v0}, LX/6ax;->d()V

    .line 1111847
    new-instance v1, LX/6Zv;

    invoke-direct {v1, p0, v0}, LX/6Zv;-><init>(Lcom/facebook/maps/GenericMapsFragment;LX/6ax;)V

    invoke-virtual {p1, v1}, LX/6al;->a(LX/6Zu;)V

    .line 1111848
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    .line 1111849
    const-string v1, "generic_map_my_location_button_impression"

    invoke-static {v0, v1}, LX/6a5;->d(LX/6a5;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    .line 1111850
    if-nez v1, :cond_1

    .line 1111851
    :goto_0
    const v0, 0x7f0d144a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1111852
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1111853
    new-instance v1, LX/6Zw;

    invoke-direct {v1, p0, p1}, LX/6Zw;-><init>(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1111854
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1111855
    return-void

    .line 1111856
    :cond_1
    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 1111826
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    .line 1111827
    iget-boolean v0, p0, Lcom/facebook/maps/GenericMapsFragment;->p:Z

    if-eqz v0, :cond_0

    .line 1111828
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/maps/GenericMapsFragment;->p:Z

    .line 1111829
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->r:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    new-instance v1, LX/6a1;

    invoke-direct {v1, p0}, LX/6a1;-><init>(Lcom/facebook/maps/GenericMapsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a(LX/6Zz;)V

    .line 1111830
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1111822
    const-class v0, Lcom/facebook/maps/GenericMapsFragment;

    invoke-static {v0, p0}, Lcom/facebook/maps/GenericMapsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1111823
    iget-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->b:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 1111824
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1111825
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1111817
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 1111818
    instance-of v0, p1, Lcom/facebook/maps/FbMapFragmentDelegate;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1111819
    check-cast v0, Lcom/facebook/maps/delegate/MapFragmentDelegate;

    iput-object v0, p0, Lcom/facebook/maps/GenericMapsFragment;->r:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    .line 1111820
    check-cast p1, Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-virtual {p1, p0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a(LX/6Zz;)V

    .line 1111821
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x9388c05

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111804
    const v1, 0x7f030799

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1111805
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1111806
    const-string v3, "place_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/maps/GenericMapsFragment;->h:Ljava/lang/String;

    .line 1111807
    const-string v3, "address"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/maps/GenericMapsFragment;->i:Ljava/lang/String;

    .line 1111808
    const-string v3, "latitude"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/maps/GenericMapsFragment;->j:D

    .line 1111809
    const-string v3, "longitude"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/maps/GenericMapsFragment;->k:D

    .line 1111810
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, p0, Lcom/facebook/maps/GenericMapsFragment;->j:D

    iget-wide v6, p0, Lcom/facebook/maps/GenericMapsFragment;->k:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iput-object v3, p0, Lcom/facebook/maps/GenericMapsFragment;->l:Lcom/facebook/android/maps/model/LatLng;

    .line 1111811
    const-string v3, "zoom"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lcom/facebook/maps/GenericMapsFragment;->n:F

    .line 1111812
    const-string v3, "curation_surface"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/maps/GenericMapsFragment;->o:Ljava/lang/String;

    .line 1111813
    if-eqz p3, :cond_0

    .line 1111814
    const-string v2, "mechanism"

    const-string v3, "mechanism_unknown"

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    .line 1111815
    :cond_0
    const v2, 0x7f0d1449

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/6Zt;

    invoke-direct {v3, p0}, LX/6Zt;-><init>(Lcom/facebook/maps/GenericMapsFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1111816
    const/16 v2, 0x2b

    const v3, 0x69696137

    invoke-static {v8, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x11cb5705

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111800
    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->b:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 1111801
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->r:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    .line 1111802
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1111803
    const/16 v1, 0x2b

    const v2, -0x5619c3f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111797
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1111798
    const-string v0, "mechanism"

    iget-object v1, p0, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111799
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x32693216

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1111791
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1111792
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1111793
    if-eqz v0, :cond_0

    .line 1111794
    iget-object v2, p0, Lcom/facebook/maps/GenericMapsFragment;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 1111795
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 1111796
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x86941d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
