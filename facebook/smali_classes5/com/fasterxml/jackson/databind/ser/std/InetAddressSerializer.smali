.class public Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer",
        "<",
        "Ljava/net/InetAddress;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 816324
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 816323
    const-class v0, Ljava/net/InetAddress;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private static a(Ljava/net/InetAddress;LX/0nX;)V
    .locals 3

    .prologue
    .line 816309
    invoke-virtual {p0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 816310
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 816311
    if-ltz v1, :cond_0

    .line 816312
    if-nez v1, :cond_1

    .line 816313
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 816314
    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 816315
    return-void

    .line 816316
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/net/InetAddress;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 816319
    const-class v0, Ljava/net/InetAddress;

    invoke-virtual {p3, p0, p1, v0}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;Ljava/lang/Class;)V

    .line 816320
    invoke-static {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;->a(Ljava/net/InetAddress;LX/0nX;)V

    .line 816321
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 816322
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816318
    check-cast p1, Ljava/net/InetAddress;

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;->a(Ljava/net/InetAddress;LX/0nX;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 816317
    check-cast p1, Ljava/net/InetAddress;

    invoke-static {p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;->a(Ljava/net/InetAddress;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method
