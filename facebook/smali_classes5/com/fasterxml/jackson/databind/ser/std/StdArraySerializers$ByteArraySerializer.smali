.class public final Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$ByteArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<[B>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 816584
    const-class v0, [B

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 816585
    return-void
.end method

.method private static a([BLX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 816581
    iget-object v0, p2, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 816582
    invoke-virtual {v0}, LX/0m4;->r()LX/0ln;

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, p0

    invoke-virtual {p1, v0, p0, v1, v2}, LX/0nX;->a(LX/0ln;[BII)V

    .line 816583
    return-void
.end method

.method private static a([BLX/0nX;LX/0my;LX/4qz;)V
    .locals 3

    .prologue
    .line 816586
    invoke-virtual {p3, p0, p1}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;)V

    .line 816587
    iget-object v0, p2, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 816588
    invoke-virtual {v0}, LX/0m4;->r()LX/0ln;

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, p0

    invoke-virtual {p1, v0, p0, v1, v2}, LX/0nX;->a(LX/0ln;[BII)V

    .line 816589
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 816590
    return-void
.end method

.method private static a([B)Z
    .locals 1

    .prologue
    .line 816580
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816579
    check-cast p1, [B

    invoke-static {p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$ByteArraySerializer;->a([BLX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 816578
    check-cast p1, [B

    invoke-static {p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$ByteArraySerializer;->a([BLX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816577
    check-cast p1, [B

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$ByteArraySerializer;->a([B)Z

    move-result v0

    return v0
.end method
