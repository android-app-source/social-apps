.class public Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573586
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 573587
    const-class v0, Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 573588
    instance-of v0, p1, Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 573589
    check-cast p1, Ljava/util/Date;

    invoke-virtual {p3, p1, p2}, LX/0my;->b(Ljava/util/Date;LX/0nX;)V

    .line 573590
    :goto_0
    return-void

    .line 573591
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
