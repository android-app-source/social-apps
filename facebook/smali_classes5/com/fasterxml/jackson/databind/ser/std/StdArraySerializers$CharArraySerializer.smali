.class public final Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<[C>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 816613
    const-class v0, [C

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private static a(LX/0nX;[C)V
    .locals 3

    .prologue
    .line 816609
    const/4 v0, 0x0

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 816610
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/0nX;->a([CII)V

    .line 816611
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 816612
    :cond_0
    return-void
.end method

.method private static a([CLX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 816603
    sget-object v0, LX/0mt;->WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS:LX/0mt;

    invoke-virtual {p2, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816604
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 816605
    invoke-static {p1, p0}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;->a(LX/0nX;[C)V

    .line 816606
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 816607
    :goto_0
    return-void

    .line 816608
    :cond_0
    const/4 v0, 0x0

    array-length v1, p0

    invoke-virtual {p1, p0, v0, v1}, LX/0nX;->a([CII)V

    goto :goto_0
.end method

.method private static a([CLX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 816595
    sget-object v0, LX/0mt;->WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS:LX/0mt;

    invoke-virtual {p2, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816596
    invoke-virtual {p3, p0, p1}, LX/4qz;->c(Ljava/lang/Object;LX/0nX;)V

    .line 816597
    invoke-static {p1, p0}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;->a(LX/0nX;[C)V

    .line 816598
    invoke-virtual {p3, p0, p1}, LX/4qz;->f(Ljava/lang/Object;LX/0nX;)V

    .line 816599
    :goto_0
    return-void

    .line 816600
    :cond_0
    invoke-virtual {p3, p0, p1}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;)V

    .line 816601
    const/4 v0, 0x0

    array-length v1, p0

    invoke-virtual {p1, p0, v0, v1}, LX/0nX;->a([CII)V

    .line 816602
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    goto :goto_0
.end method

.method private static a([C)Z
    .locals 1

    .prologue
    .line 816594
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816593
    check-cast p1, [C

    invoke-static {p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;->a([CLX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 816592
    check-cast p1, [C

    invoke-static {p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;->a([CLX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816591
    check-cast p1, [C

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$CharArraySerializer;->a([C)Z

    move-result v0

    return v0
.end method
