.class public Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase",
        "<[",
        "Ljava/lang/Object;",
        ">;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:LX/0lJ;

.field public final d:LX/4qz;

.field public e:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/2B6;


# direct methods
.method public constructor <init>(LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Z",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 816493
    const-class v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;-><init>(Ljava/lang/Class;LX/2Ay;)V

    .line 816494
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    .line 816495
    iput-boolean p2, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a:Z

    .line 816496
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    .line 816497
    sget-object v0, LX/2B7;->a:LX/2B7;

    move-object v0, v0

    .line 816498
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816499
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816500
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 816501
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;LX/2Ay;)V

    .line 816502
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    .line 816503
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    .line 816504
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a:Z

    .line 816505
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816506
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816507
    return-void
.end method

.method private a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "LX/0lJ;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 816508
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(LX/0lJ;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 816509
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 816510
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816511
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method private a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 816514
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(Ljava/lang/Class;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 816515
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 816516
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816517
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method private a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;"
        }
    .end annotation

    .prologue
    .line 816512
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    if-ne v0, p2, :cond_0

    .line 816513
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a([Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 816518
    array-length v3, p1

    .line 816519
    if-nez v3, :cond_1

    .line 816520
    :cond_0
    :goto_0
    return-void

    .line 816521
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_2

    .line 816522
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a([Ljava/lang/Object;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    .line 816523
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    if-eqz v0, :cond_3

    .line 816524
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b([Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 816525
    :cond_3
    const/4 v2, 0x0

    .line 816526
    const/4 v1, 0x0

    .line 816527
    :try_start_0
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816528
    :goto_1
    if-ge v2, v3, :cond_0

    .line 816529
    aget-object v1, p1, v2

    .line 816530
    if-nez v1, :cond_4

    .line 816531
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 816532
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 816533
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 816534
    invoke-virtual {v4, v5}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816535
    if-nez v0, :cond_5

    .line 816536
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 816537
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    invoke-virtual {p3, v0, v5}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-direct {p0, v4, v0, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816538
    :cond_5
    :goto_3
    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 816539
    :catch_0
    move-exception v0

    .line 816540
    throw v0

    .line 816541
    :cond_6
    :try_start_1
    invoke-direct {p0, v4, v5, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_3

    .line 816542
    :goto_4
    instance-of v3, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 816543
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_4

    .line 816544
    :cond_7
    instance-of v3, v0, Ljava/lang/Error;

    if-eqz v3, :cond_8

    .line 816545
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 816546
    :cond_8
    invoke-static {v0, v1, v2}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;I)LX/28E;

    move-result-object v0

    throw v0

    .line 816547
    :catch_1
    move-exception v0

    goto :goto_4
.end method

.method private a([Ljava/lang/Object;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "LX/0nX;",
            "LX/0my;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 816472
    array-length v3, p1

    .line 816473
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    .line 816474
    const/4 v0, 0x0

    .line 816475
    const/4 v1, 0x0

    move v2, v0

    .line 816476
    :goto_0
    if-ge v2, v3, :cond_4

    .line 816477
    :try_start_0
    aget-object v1, p1, v2

    .line 816478
    if-nez v1, :cond_0

    .line 816479
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 816480
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 816481
    :cond_0
    if-nez v4, :cond_1

    .line 816482
    invoke-virtual {p4, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 816483
    :catch_0
    move-exception v0

    .line 816484
    throw v0

    .line 816485
    :cond_1
    :try_start_1
    invoke-virtual {p4, v1, p2, p3, v4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 816486
    :catch_1
    move-exception v0

    .line 816487
    :goto_2
    instance-of v3, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 816488
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_2

    .line 816489
    :cond_2
    instance-of v3, v0, Ljava/lang/Error;

    if-eqz v3, :cond_3

    .line 816490
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 816491
    :cond_3
    invoke-static {v0, v1, v2}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;I)LX/28E;

    move-result-object v0

    throw v0

    .line 816492
    :cond_4
    return-void
.end method

.method private static a([Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816422
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b([Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 816448
    array-length v3, p1

    .line 816449
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    .line 816450
    const/4 v2, 0x0

    .line 816451
    const/4 v1, 0x0

    .line 816452
    :try_start_0
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->f:LX/2B6;

    .line 816453
    :goto_0
    if-ge v2, v3, :cond_4

    .line 816454
    aget-object v1, p1, v2

    .line 816455
    if-nez v1, :cond_0

    .line 816456
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 816457
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 816458
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    .line 816459
    invoke-virtual {v5, v6}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816460
    if-nez v0, :cond_1

    .line 816461
    invoke-direct {p0, v5, v6, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816462
    :cond_1
    invoke-virtual {v0, v1, p2, p3, v4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 816463
    :catch_0
    move-exception v0

    .line 816464
    throw v0

    .line 816465
    :goto_2
    instance-of v3, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 816466
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_2

    .line 816467
    :cond_2
    instance-of v3, v0, Ljava/lang/Error;

    if-eqz v3, :cond_3

    .line 816468
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 816469
    :cond_3
    invoke-static {v0, v1, v2}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;I)LX/28E;

    move-result-object v0

    throw v0

    .line 816470
    :cond_4
    return-void

    .line 816471
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static b([Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 816447
    array-length v1, p0

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816427
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->d:LX/4qz;

    .line 816428
    if-eqz v0, :cond_5

    .line 816429
    invoke-virtual {v0, p2}, LX/4qz;->a(LX/2Ay;)LX/4qz;

    move-result-object v0

    move-object v1, v0

    .line 816430
    :goto_0
    const/4 v0, 0x0

    .line 816431
    if-eqz p2, :cond_0

    .line 816432
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v2

    .line 816433
    if-eqz v2, :cond_0

    .line 816434
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v3

    .line 816435
    if-eqz v3, :cond_0

    .line 816436
    invoke-virtual {p1, v2, v3}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816437
    :cond_0
    if-nez v0, :cond_1

    .line 816438
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816439
    :cond_1
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816440
    if-nez v0, :cond_4

    .line 816441
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    if-eqz v2, :cond_3

    .line 816442
    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a:Z

    if-nez v2, :cond_2

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->a_(LX/0my;LX/2Ay;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 816443
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816444
    :cond_3
    :goto_1
    invoke-direct {p0, p2, v1, v0}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;

    move-result-object v0

    return-object v0

    .line 816445
    :cond_4
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_3

    .line 816446
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816426
    check-cast p1, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a([Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816425
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b:LX/0lJ;

    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a:Z

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;-><init>(LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816424
    check-cast p1, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->a([Ljava/lang/Object;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816423
    check-cast p1, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;->b([Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
