.class public Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;
.super Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
        "<",
        "Ljava/util/Map",
        "<**>;>;",
        "LX/0nU;"
    }
.end annotation


# static fields
.field public static final a:LX/0lJ;


# instance fields
.field public final b:LX/2Ay;

.field public final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:LX/0lJ;

.field public final f:LX/0lJ;

.field public g:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/4qz;

.field public j:LX/2B6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573571
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a:LX/0lJ;

    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/util/HashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573560
    const-class v0, Ljava/util/Map;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 573561
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573562
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    .line 573563
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    .line 573564
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    .line 573565
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    .line 573566
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573567
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573568
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573569
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    .line 573570
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;LX/4qz;)V
    .locals 2

    .prologue
    .line 573549
    const-class v0, Ljava/util/Map;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 573550
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573551
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    .line 573552
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    .line 573553
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    .line 573554
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    .line 573555
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573556
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573557
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573558
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    .line 573559
    return-void
.end method

.method private constructor <init>(Ljava/util/HashSet;LX/0lJ;LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0lJ;",
            "LX/0lJ;",
            "Z",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 573537
    const-class v0, Ljava/util/Map;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 573538
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573539
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    .line 573540
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    .line 573541
    iput-boolean p4, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    .line 573542
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    .line 573543
    iput-object p6, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573544
    iput-object p7, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573545
    sget-object v0, LX/2B7;->a:LX/2B7;

    move-object v0, v0

    .line 573546
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573547
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    .line 573548
    return-void
.end method

.method private a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "LX/0lJ;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573533
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(LX/0lJ;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 573534
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 573535
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573536
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method private a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573529
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(Ljava/lang/Class;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 573530
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 573531
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573532
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method private a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;"
        }
    .end annotation

    .prologue
    .line 573528
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/util/HashSet;)V

    return-object v0
.end method

.method public static a([Ljava/lang/String;LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "LX/0lJ;",
            "Z",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 573517
    invoke-static {p0}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a([Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v1

    .line 573518
    if-nez p1, :cond_1

    .line 573519
    sget-object v2, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a:LX/0lJ;

    move-object v3, v2

    .line 573520
    :goto_0
    if-nez p2, :cond_2

    .line 573521
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0lJ;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v4, v0

    .line 573522
    :goto_1
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;-><init>(Ljava/util/HashSet;LX/0lJ;LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0

    .line 573523
    :cond_1
    invoke-virtual {p1}, LX/0lJ;->q()LX/0lJ;

    move-result-object v2

    .line 573524
    invoke-virtual {p1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v3

    goto :goto_0

    .line 573525
    :cond_2
    iget-object v4, v3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v4, v4

    .line 573526
    const-class v5, Ljava/lang/Object;

    if-ne v4, v5, :cond_3

    move v4, v0

    .line 573527
    goto :goto_1

    :cond_3
    move v4, p2

    goto :goto_1
.end method

.method private static a([Ljava/lang/String;)Ljava/util/HashSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573366
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    .line 573367
    :cond_0
    const/4 v0, 0x0

    .line 573368
    :cond_1
    return-object v0

    .line 573369
    :cond_2
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 573370
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p0, v1

    .line 573371
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 573372
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/Map;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 573508
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 573509
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 573510
    sget-object v0, LX/0mt;->ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573511
    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 573512
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_2

    .line 573513
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 573514
    :cond_1
    :goto_0
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    .line 573515
    return-void

    .line 573516
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;)V

    goto :goto_0
.end method

.method private a(Ljava/util/Map;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573486
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573487
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573488
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    .line 573489
    sget-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 573490
    :goto_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573491
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 573492
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    .line 573493
    if-nez v7, :cond_2

    .line 573494
    invoke-virtual {p3}, LX/0my;->k()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    const/4 v8, 0x0

    invoke-virtual {v0, v8, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 573495
    :goto_2
    if-nez v6, :cond_5

    .line 573496
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    goto :goto_1

    .line 573497
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 573498
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v6, :cond_0

    .line 573499
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573500
    :cond_4
    invoke-virtual {v2, v7, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_2

    .line 573501
    :cond_5
    if-nez v4, :cond_6

    .line 573502
    :try_start_0
    invoke-virtual {p4, v6, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 573503
    :catch_0
    move-exception v0

    .line 573504
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 573505
    invoke-static {p3, v0, p1, v6}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 573506
    :cond_6
    :try_start_1
    invoke-virtual {p4, v6, p2, p3, v4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 573507
    :cond_7
    return-void
.end method

.method private static a(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)Z"
        }
    .end annotation

    .prologue
    .line 573485
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/Map;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 573476
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 573477
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 573478
    sget-object v0, LX/0mt;->ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573479
    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 573480
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_2

    .line 573481
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 573482
    :cond_1
    :goto_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 573483
    return-void

    .line 573484
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;)V

    goto :goto_0
.end method

.method private static b(Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 573365
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;
    .locals 1

    .prologue
    .line 573373
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;LX/4qz;)V

    return-object v0
.end method

.method private static c(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)",
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 573374
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    .line 573375
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p0}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private c(Ljava/util/Map;LX/0nX;LX/0my;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 573376
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573377
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573378
    sget-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 573379
    :goto_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v2, v3

    move-object v4, v3

    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573380
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    .line 573381
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    .line 573382
    if-nez v10, :cond_2

    .line 573383
    invoke-virtual {p3}, LX/0my;->k()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    invoke-virtual {v0, v3, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 573384
    :goto_2
    if-nez v9, :cond_5

    .line 573385
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    goto :goto_1

    .line 573386
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 573387
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v9, :cond_0

    .line 573388
    :cond_3
    if-eqz v7, :cond_4

    invoke-virtual {v7, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573389
    :cond_4
    invoke-virtual {v6, v10, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_2

    .line 573390
    :cond_5
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 573391
    if-ne v5, v2, :cond_6

    move-object v0, v2

    move-object v2, v4

    .line 573392
    :goto_3
    :try_start_0
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    invoke-virtual {v4, v9, p2, p3, v5}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v2

    move-object v2, v0

    .line 573393
    goto :goto_1

    .line 573394
    :cond_6
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 573395
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    invoke-virtual {p3, v0, v5}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v0, v2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    :goto_4
    move-object v4, v0

    move-object v2, v0

    move-object v0, v5

    .line 573396
    goto :goto_3

    .line 573397
    :cond_7
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v5, v0}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_4

    .line 573398
    :catch_0
    move-exception v4

    .line 573399
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 573400
    invoke-static {p3, v4, p1, v5}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v2

    move-object v2, v0

    .line 573401
    goto :goto_1

    .line 573402
    :cond_8
    return-void
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 573403
    if-eqz p2, :cond_d

    .line 573404
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v2

    .line 573405
    if-eqz v2, :cond_d

    .line 573406
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v3

    .line 573407
    invoke-virtual {v3, v2}, LX/0lU;->g(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 573408
    if-eqz v0, :cond_c

    .line 573409
    invoke-virtual {p1, v2, v0}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 573410
    :goto_0
    invoke-virtual {v3, v2}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v3

    .line 573411
    if-eqz v3, :cond_b

    .line 573412
    invoke-virtual {p1, v2, v3}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 573413
    :goto_1
    if-nez v0, :cond_0

    .line 573414
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573415
    :cond_0
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 573416
    if-nez v0, :cond_4

    .line 573417
    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->d:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    .line 573418
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 573419
    const-class v3, Ljava/lang/Object;

    if-ne v2, v3, :cond_2

    :cond_1
    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->a_(LX/0my;LX/2Ay;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 573420
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    move-object v3, v0

    .line 573421
    :goto_2
    if-nez v1, :cond_9

    .line 573422
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573423
    :goto_3
    if-nez v0, :cond_5

    .line 573424
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->e:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0my;->b(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 573425
    :cond_3
    :goto_4
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573426
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v1

    .line 573427
    if-eqz v1, :cond_7

    if-eqz p2, :cond_7

    .line 573428
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v4

    .line 573429
    if-eqz v4, :cond_7

    .line 573430
    if-nez v2, :cond_6

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 573431
    :goto_5
    array-length v5, v4

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 573432
    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 573433
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 573434
    :cond_4
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_a

    .line 573435
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    .line 573436
    :cond_5
    instance-of v1, v0, LX/0nU;

    if-eqz v1, :cond_3

    .line 573437
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_4

    .line 573438
    :cond_6
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_5

    :cond_7
    move-object v1, v2

    .line 573439
    :cond_8
    invoke-direct {p0, p2, v0, v3, v1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-result-object v0

    return-object v0

    :cond_9
    move-object v0, v1

    goto :goto_3

    :cond_a
    move-object v3, v0

    goto :goto_2

    :cond_b
    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto/16 :goto_1

    :cond_c
    move-object v0, v1

    goto/16 :goto_0

    :cond_d
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 573440
    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b(Ljava/util/Map;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 573441
    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method

.method public final a(Ljava/util/Map;LX/0nX;LX/0my;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 573442
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->i:LX/4qz;

    if-eqz v0, :cond_1

    .line 573443
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c(Ljava/util/Map;LX/0nX;LX/0my;)V

    .line 573444
    :cond_0
    return-void

    .line 573445
    :cond_1
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->g:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 573446
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c:Ljava/util/HashSet;

    .line 573447
    sget-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 573448
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    .line 573449
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v0

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573450
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 573451
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    .line 573452
    if-nez v7, :cond_4

    .line 573453
    invoke-virtual {p3}, LX/0my;->k()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    const/4 v8, 0x0

    invoke-virtual {v0, v8, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 573454
    :goto_2
    if-nez v6, :cond_7

    .line 573455
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    goto :goto_1

    .line 573456
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 573457
    :cond_4
    if-eqz v1, :cond_5

    if-eqz v6, :cond_2

    .line 573458
    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 573459
    :cond_6
    invoke-virtual {v3, v7, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_2

    .line 573460
    :cond_7
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    .line 573461
    invoke-virtual {v2, v8}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 573462
    if-nez v0, :cond_9

    .line 573463
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 573464
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->f:LX/0lJ;

    invoke-virtual {p3, v0, v8}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-direct {p0, v2, v0, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 573465
    :goto_3
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->j:LX/2B6;

    move-object v9, v0

    move-object v0, v2

    move-object v2, v9

    .line 573466
    :goto_4
    :try_start_0
    invoke-virtual {v2, v6, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 573467
    goto :goto_1

    .line 573468
    :cond_8
    invoke-direct {p0, v2, v8, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_3

    .line 573469
    :catch_0
    move-exception v2

    .line 573470
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 573471
    invoke-static {p3, v2, p1, v6}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    .line 573472
    goto :goto_1

    :cond_9
    move-object v9, v0

    move-object v0, v2

    move-object v2, v9

    goto :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 573473
    check-cast p1, Ljava/util/Map;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 1

    .prologue
    .line 573474
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->c(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 573475
    check-cast p1, Ljava/util/Map;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->b(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method
