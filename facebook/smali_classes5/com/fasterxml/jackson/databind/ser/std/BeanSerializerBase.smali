.class public abstract Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""

# interfaces
.implements LX/0nU;
.implements LX/2B9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/0nU;",
        "LX/2B9;"
    }
.end annotation


# static fields
.field public static final b:[LX/2Ax;


# instance fields
.field public final c:[LX/2Ax;

.field public final d:[LX/2Ax;

.field public final e:LX/4rL;

.field public final f:Ljava/lang/Object;

.field public final g:LX/2An;

.field public final h:LX/4rR;

.field public final i:LX/2BA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 570517
    const/4 v0, 0x0

    new-array v0, v0, [LX/2Ax;

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b:[LX/2Ax;

    return-void
.end method

.method public constructor <init>(LX/0lJ;LX/2Aw;[LX/2Ax;[LX/2Ax;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 570518
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(LX/0lJ;)V

    .line 570519
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    .line 570520
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570521
    if-nez p2, :cond_1

    .line 570522
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    .line 570523
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    .line 570524
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570525
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570526
    :cond_0
    :goto_0
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    .line 570527
    return-void

    .line 570528
    :cond_1
    iget-object v1, p2, LX/2Aw;->g:LX/2An;

    move-object v1, v1

    .line 570529
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    .line 570530
    iget-object v1, p2, LX/2Aw;->e:LX/4rL;

    move-object v1, v1

    .line 570531
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    .line 570532
    iget-object v1, p2, LX/2Aw;->f:Ljava/lang/Object;

    move-object v1, v1

    .line 570533
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570534
    iget-object v1, p2, LX/2Aw;->h:LX/4rR;

    move-object v1, v1

    .line 570535
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570536
    iget-object v1, p2, LX/2Aw;->a:LX/0lS;

    move-object v1, v1

    .line 570537
    invoke-virtual {v1, v0}, LX/0lS;->a(LX/4pN;)LX/4pN;

    move-result-object v1

    .line 570538
    if-eqz v1, :cond_0

    .line 570539
    iget-object v0, v1, LX/4pN;->b:LX/2BA;

    move-object v0, v0

    .line 570540
    goto :goto_0
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;LX/4rR;)V
    .locals 1

    .prologue
    .line 570541
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 570542
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    .line 570543
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570544
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    .line 570545
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    .line 570546
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570547
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570548
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    .line 570549
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;LX/4ro;)V
    .locals 2

    .prologue
    .line 570550
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    invoke-static {v0, p2}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a([LX/2Ax;LX/4ro;)[LX/2Ax;

    move-result-object v0

    iget-object v1, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    invoke-static {v1, p2}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a([LX/2Ax;LX/4ro;)[LX/2Ax;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[LX/2Ax;[LX/2Ax;)V

    .line 570551
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[LX/2Ax;[LX/2Ax;)V
    .locals 1

    .prologue
    .line 570576
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 570577
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    .line 570578
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570579
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    .line 570580
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    .line 570581
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570582
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570583
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    .line 570584
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 570552
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 570553
    invoke-static {p2}, LX/0nj;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    .line 570554
    iget-object v4, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    .line 570555
    iget-object v5, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570556
    array-length v6, v4

    .line 570557
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 570558
    if-nez v5, :cond_1

    move-object v1, v2

    .line 570559
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v6, :cond_2

    .line 570560
    aget-object v8, v4, v0

    .line 570561
    invoke-virtual {v8}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 570562
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570563
    if-eqz v5, :cond_0

    .line 570564
    aget-object v8, v5, v0

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570565
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 570566
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object v1, v0

    goto :goto_0

    .line 570567
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/2Ax;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ax;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    .line 570568
    if-nez v1, :cond_3

    :goto_2
    iput-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570569
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    .line 570570
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    .line 570571
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570572
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570573
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    .line 570574
    return-void

    .line 570575
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/2Ax;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ax;

    move-object v2, v0

    goto :goto_2
.end method

.method private static a(LX/0my;LX/2Ax;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ax;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 570595
    invoke-virtual {p0}, LX/0my;->e()LX/0lU;

    move-result-object v0

    .line 570596
    if-eqz v0, :cond_0

    .line 570597
    invoke-virtual {p1}, LX/2Ax;->b()LX/2An;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->m(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 570598
    if-eqz v0, :cond_0

    .line 570599
    invoke-virtual {p1}, LX/2Ax;->b()LX/2An;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/0mz;->a(LX/0lO;Ljava/lang/Object;)LX/1Xr;

    move-result-object v1

    .line 570600
    invoke-virtual {p0}, LX/0mz;->c()LX/0li;

    invoke-interface {v1}, LX/1Xr;->c()LX/0lJ;

    move-result-object v2

    .line 570601
    invoke-virtual {p0, v2, p1}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v3

    .line 570602
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    invoke-direct {v0, v1, v2, v3}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 570603
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final a([LX/2Ax;LX/4ro;)[LX/2Ax;
    .locals 4

    .prologue
    .line 570585
    if-eqz p0, :cond_0

    array-length v0, p0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, LX/4ro;->a:LX/4ro;

    if-ne p1, v0, :cond_1

    .line 570586
    :cond_0
    :goto_0
    return-object p0

    .line 570587
    :cond_1
    array-length v2, p0

    .line 570588
    new-array v0, v2, [LX/2Ax;

    .line 570589
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 570590
    aget-object v3, p0, v1

    .line 570591
    if-eqz v3, :cond_2

    .line 570592
    invoke-virtual {v3, p1}, LX/2Ax;->a(LX/4ro;)LX/2Ax;

    move-result-object v3

    aput-object v3, v0, v1

    .line 570593
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object p0, v0

    .line 570594
    goto :goto_0
.end method

.method private b(LX/0my;)LX/4rM;
    .locals 4

    .prologue
    .line 570507
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    .line 570508
    invoke-virtual {p1}, LX/0my;->g()LX/4rN;

    move-result-object v1

    .line 570509
    if-nez v1, :cond_0

    .line 570510
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not resolve BeanPropertyFilter with id \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; no FilterProvider configured"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v1

    .line 570511
    :cond_0
    invoke-virtual {v1}, LX/4rN;->a()LX/4rM;

    move-result-object v0

    .line 570512
    return-object v0
.end method

.method private final b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 570513
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    invoke-virtual {v0, p1}, LX/2An;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 570514
    if-nez v0, :cond_0

    .line 570515
    const-string v0, ""

    .line 570516
    :goto_0
    return-object v0

    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 4

    .prologue
    .line 570317
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570318
    iget-object v0, v1, LX/4rR;->c:LX/4pS;

    invoke-virtual {p3, p1, v0}, LX/0my;->a(Ljava/lang/Object;LX/4pS;)LX/4rX;

    move-result-object v2

    .line 570319
    invoke-virtual {v2, p2, p3, v1}, LX/4rX;->a(LX/0nX;LX/0my;LX/4rR;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570320
    :goto_0
    return-void

    .line 570321
    :cond_0
    invoke-virtual {v2, p1}, LX/4rX;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 570322
    iget-boolean v3, v1, LX/4rR;->e:Z

    if-eqz v3, :cond_1

    .line 570323
    iget-object v1, v1, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 570324
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 570325
    :goto_1
    if-nez v0, :cond_3

    .line 570326
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 570327
    :goto_2
    invoke-virtual {v2, p2, p3, v1}, LX/4rX;->b(LX/0nX;LX/0my;LX/4rR;)V

    .line 570328
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    if-eqz v1, :cond_4

    .line 570329
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 570330
    :goto_3
    if-nez v0, :cond_5

    .line 570331
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    goto :goto_0

    .line 570332
    :cond_2
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 570333
    :cond_3
    invoke-virtual {p4, p1, p2, v0}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V

    goto :goto_2

    .line 570334
    :cond_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_3

    .line 570335
    :cond_5
    invoke-virtual {p4, p1, p2, v0}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 570336
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570337
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v5

    .line 570338
    if-eqz p2, :cond_0

    if-nez v5, :cond_4

    :cond_0
    move-object v4, v0

    .line 570339
    :goto_0
    if-eqz v4, :cond_d

    .line 570340
    invoke-virtual {v5, v4}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v1

    .line 570341
    invoke-virtual {v5, v4}, LX/0lU;->a(LX/0lO;)LX/4qt;

    move-result-object v6

    .line 570342
    if-nez v6, :cond_5

    .line 570343
    if-eqz v2, :cond_c

    .line 570344
    new-instance v2, LX/4qt;

    const-string v3, ""

    invoke-direct {v2, v3, v0, v0}, LX/4qt;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-virtual {v5, v4, v2}, LX/0lU;->a(LX/0lO;LX/4qt;)LX/4qt;

    move-result-object v2

    .line 570345
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570346
    iget-boolean v6, v2, LX/4qt;->d:Z

    move v2, v6

    .line 570347
    invoke-virtual {v3, v2}, LX/4rR;->a(Z)LX/4rR;

    move-result-object v2

    move-object v12, v1

    move-object v1, v2

    move-object v2, v12

    .line 570348
    :goto_1
    if-eqz v1, :cond_b

    .line 570349
    iget-object v3, v1, LX/4rR;->a:LX/0lJ;

    invoke-virtual {p1, v3, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v3

    .line 570350
    invoke-virtual {v1, v3}, LX/4rR;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/4rR;

    move-result-object v1

    .line 570351
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    if-eq v1, v3, :cond_b

    .line 570352
    invoke-virtual {p0, v1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a(LX/4rR;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    move-result-object v1

    .line 570353
    :goto_2
    if-eqz v2, :cond_1

    array-length v3, v2

    if-eqz v3, :cond_1

    .line 570354
    invoke-virtual {v1, v2}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a([Ljava/lang/String;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    move-result-object v1

    .line 570355
    :cond_1
    if-eqz v4, :cond_2

    .line 570356
    invoke-virtual {v5, v4}, LX/0lU;->e(LX/0lO;)LX/4pN;

    move-result-object v2

    .line 570357
    if-eqz v2, :cond_2

    .line 570358
    iget-object v0, v2, LX/4pN;->b:LX/2BA;

    move-object v0, v0

    .line 570359
    :cond_2
    if-nez v0, :cond_3

    .line 570360
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->i:LX/2BA;

    .line 570361
    :cond_3
    sget-object v2, LX/2BA;->ARRAY:LX/2BA;

    if-ne v0, v2, :cond_a

    .line 570362
    invoke-virtual {v1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d()Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    move-result-object v0

    .line 570363
    :goto_3
    return-object v0

    .line 570364
    :cond_4
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    move-object v4, v1

    goto :goto_0

    .line 570365
    :cond_5
    invoke-virtual {v5, v4, v6}, LX/0lU;->a(LX/0lO;LX/4qt;)LX/4qt;

    move-result-object v6

    .line 570366
    iget-object v2, v6, LX/4qt;->b:Ljava/lang/Class;

    move-object v2, v2

    .line 570367
    invoke-virtual {p1, v2}, LX/0mz;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v7

    .line 570368
    invoke-virtual {p1}, LX/0mz;->c()LX/0li;

    move-result-object v8

    const-class v9, LX/4pS;

    invoke-virtual {v8, v7, v9}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v7

    aget-object v7, v7, v3

    .line 570369
    const-class v8, LX/4pV;

    if-ne v2, v8, :cond_9

    .line 570370
    iget-object v2, v6, LX/4qt;->a:Ljava/lang/String;

    move-object v7, v2

    .line 570371
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    array-length v8, v2

    move v2, v3

    .line 570372
    :goto_4
    if-ne v2, v8, :cond_6

    .line 570373
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Object Id definition for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": can not find property with name \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 570374
    :cond_6
    iget-object v9, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    aget-object v9, v9, v2

    .line 570375
    invoke-virtual {v9}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 570376
    if-lez v2, :cond_7

    .line 570377
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    iget-object v8, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    invoke-static {v7, v3, v8, v11, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 570378
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    aput-object v9, v7, v3

    .line 570379
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-eqz v7, :cond_7

    .line 570380
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    aget-object v7, v7, v2

    .line 570381
    iget-object v8, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    iget-object v10, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    invoke-static {v8, v3, v10, v11, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 570382
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    aput-object v7, v2, v3

    .line 570383
    :cond_7
    invoke-virtual {v9}, LX/2Ax;->a()LX/0lJ;

    move-result-object v2

    .line 570384
    new-instance v3, LX/4rS;

    invoke-direct {v3, v6, v9}, LX/4rS;-><init>(LX/4qt;LX/2Ax;)V

    .line 570385
    iget-boolean v7, v6, LX/4qt;->d:Z

    move v6, v7

    .line 570386
    invoke-static {v2, v0, v3, v6}, LX/4rR;->a(LX/0lJ;Ljava/lang/String;LX/4pS;Z)LX/4rR;

    move-result-object v2

    move-object v12, v1

    move-object v1, v2

    move-object v2, v12

    .line 570387
    goto/16 :goto_1

    .line 570388
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 570389
    :cond_9
    invoke-virtual {p1, v4, v6}, LX/0mz;->a(LX/0lO;LX/4qt;)LX/4pS;

    move-result-object v2

    .line 570390
    iget-object v3, v6, LX/4qt;->a:Ljava/lang/String;

    move-object v3, v3

    .line 570391
    iget-boolean v8, v6, LX/4qt;->d:Z

    move v6, v8

    .line 570392
    invoke-static {v7, v3, v2, v6}, LX/4rR;->a(LX/0lJ;Ljava/lang/String;LX/4pS;Z)LX/4rR;

    move-result-object v2

    move-object v12, v1

    move-object v1, v2

    move-object v2, v12

    goto/16 :goto_1

    :cond_a
    move-object v0, v1

    goto/16 :goto_3

    :cond_b
    move-object v1, p0

    goto/16 :goto_2

    :cond_c
    move-object v12, v1

    move-object v1, v2

    move-object v2, v12

    goto/16 :goto_1

    :cond_d
    move-object v1, v2

    move-object v2, v0

    goto/16 :goto_1
.end method

.method public abstract a(LX/4rR;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
.end method

.method public abstract a([Ljava/lang/String;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
.end method

.method public final a(LX/0my;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 570393
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-nez v1, :cond_3

    move v2, v0

    .line 570394
    :goto_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    array-length v4, v1

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_6

    .line 570395
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    aget-object v5, v0, v3

    .line 570396
    iget-boolean v0, v5, LX/2Ax;->n:Z

    move v0, v0

    .line 570397
    if-nez v0, :cond_0

    invoke-virtual {v5}, LX/2Ax;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 570398
    invoke-virtual {p1}, LX/0my;->l()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 570399
    if-eqz v0, :cond_0

    .line 570400
    invoke-virtual {v5, v0}, LX/2Ax;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 570401
    if-ge v3, v2, :cond_0

    .line 570402
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    aget-object v1, v1, v3

    .line 570403
    if-eqz v1, :cond_0

    .line 570404
    invoke-virtual {v1, v0}, LX/2Ax;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 570405
    :cond_0
    invoke-virtual {v5}, LX/2Ax;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 570406
    invoke-static {p1, v5}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a(LX/0my;LX/2Ax;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 570407
    if-nez v1, :cond_5

    .line 570408
    iget-object v0, v5, LX/2Ax;->j:LX/0lJ;

    move-object v0, v0

    .line 570409
    if-nez v0, :cond_4

    .line 570410
    invoke-virtual {v5}, LX/2Ax;->i()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0mz;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    .line 570411
    invoke-virtual {v0}, LX/0lJ;->k()Z

    move-result v1

    if-nez v1, :cond_4

    .line 570412
    invoke-virtual {v0}, LX/0lJ;->l()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/0lJ;->s()I

    move-result v1

    if-lez v1, :cond_2

    .line 570413
    :cond_1
    iput-object v0, v5, LX/2Ax;->r:LX/0lJ;

    .line 570414
    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 570415
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    array-length v1, v1

    move v2, v1

    goto :goto_0

    .line 570416
    :cond_4
    invoke-virtual {p1, v0, v5}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 570417
    invoke-virtual {v0}, LX/0lJ;->l()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 570418
    invoke-virtual {v0}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-virtual {v0}, LX/0lJ;->u()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qz;

    .line 570419
    if-eqz v0, :cond_5

    .line 570420
    instance-of v6, v1, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    if-eqz v6, :cond_5

    .line 570421
    check-cast v1, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->a(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    move-result-object v1

    .line 570422
    :cond_5
    invoke-virtual {v5, v1}, LX/2Ax;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 570423
    if-ge v3, v2, :cond_2

    .line 570424
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    aget-object v0, v0, v3

    .line 570425
    if-eqz v0, :cond_2

    .line 570426
    invoke-virtual {v0, v1}, LX/2Ax;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_2

    .line 570427
    :cond_6
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    if-eqz v0, :cond_7

    .line 570428
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    invoke-virtual {v0, p1}, LX/4rL;->a(LX/0my;)V

    .line 570429
    :cond_7
    return-void
.end method

.method public a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 570430
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    if-eqz v0, :cond_0

    .line 570431
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    .line 570432
    :goto_0
    return-void

    .line 570433
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->g:LX/2An;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 570434
    :goto_1
    if-nez v0, :cond_2

    .line 570435
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 570436
    :goto_2
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    if-eqz v1, :cond_3

    .line 570437
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 570438
    :goto_3
    if-nez v0, :cond_4

    .line 570439
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    goto :goto_0

    .line 570440
    :cond_1
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 570441
    :cond_2
    invoke-virtual {p4, p1, p2, v0}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V

    goto :goto_2

    .line 570442
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_3

    .line 570443
    :cond_4
    invoke-virtual {p4, p1, p2, v0}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;Z)V
    .locals 4

    .prologue
    .line 570444
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    .line 570445
    iget-object v1, v0, LX/4rR;->c:LX/4pS;

    invoke-virtual {p3, p1, v1}, LX/0my;->a(Ljava/lang/Object;LX/4pS;)LX/4rX;

    move-result-object v1

    .line 570446
    invoke-virtual {v1, p2, p3, v0}, LX/4rX;->a(LX/0nX;LX/0my;LX/4rR;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 570447
    :cond_0
    :goto_0
    return-void

    .line 570448
    :cond_1
    invoke-virtual {v1, p1}, LX/4rX;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 570449
    iget-boolean v3, v0, LX/4rR;->e:Z

    if-eqz v3, :cond_2

    .line 570450
    iget-object v0, v0, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v0, v2, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 570451
    :cond_2
    if-eqz p4, :cond_3

    .line 570452
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 570453
    :cond_3
    invoke-virtual {v1, p2, p3, v0}, LX/4rX;->b(LX/0nX;LX/0my;LX/4rR;)V

    .line 570454
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->f:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 570455
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 570456
    :goto_1
    if-eqz p4, :cond_0

    .line 570457
    invoke-virtual {p2}, LX/0nX;->g()V

    goto :goto_0

    .line 570458
    :cond_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 570459
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-eqz v0, :cond_1

    .line 570460
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 570461
    if-eqz v0, :cond_1

    .line 570462
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570463
    :goto_0
    const/4 v2, 0x0

    .line 570464
    :try_start_0
    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_2

    .line 570465
    aget-object v3, v0, v2

    .line 570466
    if-eqz v3, :cond_0

    .line 570467
    invoke-virtual {v3, p1, p2, p3}, LX/2Ax;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    .line 570468
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 570469
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    goto :goto_0

    .line 570470
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    if-eqz v1, :cond_3

    .line 570471
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    invoke-virtual {v1, p1, p2, p3}, LX/4rL;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_1

    .line 570472
    :cond_3
    :goto_2
    return-void

    .line 570473
    :catch_0
    move-exception v1

    .line 570474
    array-length v3, v0

    if-ne v2, v3, :cond_4

    const-string v0, "[anySetter]"

    .line 570475
    :goto_3
    invoke-static {p3, v1, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 570476
    :cond_4
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 570477
    :catch_1
    move-exception v1

    .line 570478
    new-instance v3, LX/28E;

    const-string v4, "Infinite recursion (StackOverflowError)"

    invoke-direct {v3, v4, v1}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 570479
    array-length v1, v0

    if-ne v2, v1, :cond_5

    const-string v0, "[anySetter]"

    .line 570480
    :goto_4
    new-instance v1, LX/4pp;

    invoke-direct {v1, p1, v0}, LX/4pp;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/28E;->a(LX/4pp;)V

    .line 570481
    throw v3

    .line 570482
    :cond_5
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 570483
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->h:LX/4rR;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 570484
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-eqz v0, :cond_1

    .line 570485
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 570486
    if-eqz v0, :cond_1

    .line 570487
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 570488
    :goto_0
    invoke-direct {p0, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(LX/0my;)LX/4rM;

    move-result-object v1

    .line 570489
    if-nez v1, :cond_2

    .line 570490
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 570491
    :cond_0
    :goto_1
    return-void

    .line 570492
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    goto :goto_0

    .line 570493
    :cond_2
    const/4 v2, 0x0

    .line 570494
    :try_start_0
    array-length v1, v0

    :goto_2
    if-ge v2, v1, :cond_3

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 570495
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    if-eqz v1, :cond_0

    .line 570496
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->e:LX/4rL;

    invoke-virtual {v1, p1, p2, p3}, LX/4rL;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 570497
    :catch_0
    move-exception v1

    .line 570498
    array-length v3, v0

    if-ne v2, v3, :cond_4

    const-string v0, "[anySetter]"

    .line 570499
    :goto_3
    invoke-static {p3, v1, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 570500
    :cond_4
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 570501
    :catch_1
    move-exception v1

    .line 570502
    new-instance v3, LX/28E;

    const-string v4, "Infinite recursion (StackOverflowError)"

    invoke-direct {v3, v4, v1}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 570503
    array-length v1, v0

    if-ne v2, v1, :cond_5

    const-string v0, "[anySetter]"

    .line 570504
    :goto_4
    new-instance v1, LX/4pp;

    invoke-direct {v1, p1, v0}, LX/4pp;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/28E;->a(LX/4pp;)V

    .line 570505
    throw v3

    .line 570506
    :cond_5
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method public abstract d()Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
.end method
