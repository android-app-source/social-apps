.class public Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;
.super Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
        "<",
        "Ljava/util/EnumMap",
        "<+",
        "Ljava/lang/Enum",
        "<*>;*>;>;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:LX/2Ay;

.field public final c:LX/2BV;

.field public final d:LX/0lJ;

.field public final e:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/4qz;


# direct methods
.method public constructor <init>(LX/0lJ;ZLX/2BV;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Z",
            "LX/2BV;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 816245
    const-class v1, Ljava/util/EnumMap;

    invoke-direct {p0, v1, v0}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 816246
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    .line 816247
    if-nez p2, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0lJ;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a:Z

    .line 816248
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->d:LX/0lJ;

    .line 816249
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    .line 816250
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->f:LX/4qz;

    .line 816251
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816252
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 816253
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;)V

    .line 816254
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    .line 816255
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a:Z

    .line 816256
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->d:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->d:LX/0lJ;

    .line 816257
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    .line 816258
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->f:LX/4qz;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->f:LX/4qz;

    .line 816259
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816260
    return-void
.end method

.method private a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;"
        }
    .end annotation

    .prologue
    .line 816243
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne p2, v0, :cond_0

    .line 816244
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    invoke-direct {v0, p0, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a(Ljava/util/EnumMap;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 816238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 816239
    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816240
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b(Ljava/util/EnumMap;LX/0nX;LX/0my;)V

    .line 816241
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 816242
    return-void
.end method

.method private a(Ljava/util/EnumMap;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 816233
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 816234
    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816235
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b(Ljava/util/EnumMap;LX/0nX;LX/0my;)V

    .line 816236
    :cond_0
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    .line 816237
    return-void
.end method

.method private a(Ljava/util/EnumMap;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;",
            "LX/0nX;",
            "LX/0my;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 816212
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    .line 816213
    sget-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    move v3, v0

    .line 816214
    :goto_0
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->f:LX/4qz;

    .line 816215
    invoke-virtual {p1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 816216
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 816217
    if-eqz v3, :cond_1

    if-eqz v6, :cond_0

    .line 816218
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    .line 816219
    if-nez v2, :cond_2

    .line 816220
    invoke-virtual {v1}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v7, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v2, v7}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    check-cast v2, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;

    .line 816221
    check-cast v2, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;

    .line 816222
    iget-object v7, v2, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a:LX/2BV;

    move-object v2, v7

    .line 816223
    :cond_2
    invoke-virtual {v2, v1}, LX/2BV;->a(Ljava/lang/Enum;)LX/0lb;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(LX/0lc;)V

    .line 816224
    if-nez v6, :cond_4

    .line 816225
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    goto :goto_1

    .line 816226
    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 816227
    :cond_4
    if-nez v4, :cond_5

    .line 816228
    :try_start_0
    invoke-virtual {p4, v6, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 816229
    :catch_0
    move-exception v1

    .line 816230
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v1, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 816231
    :cond_5
    :try_start_1
    invoke-virtual {p4, v6, p2, p3, v4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 816232
    :cond_6
    return-void
.end method

.method private static a(Ljava/util/EnumMap;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;)Z"
        }
    .end annotation

    .prologue
    .line 816211
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/EnumMap;LX/0nX;LX/0my;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 816261
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_1

    .line 816262
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(Ljava/util/EnumMap;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 816263
    :cond_0
    return-void

    .line 816264
    :cond_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    .line 816265
    sget-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    move v3, v0

    .line 816266
    :goto_0
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->f:LX/4qz;

    .line 816267
    invoke-virtual {p1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v4, v2

    move-object v5, v2

    move-object v2, v1

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 816268
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    .line 816269
    if-eqz v3, :cond_3

    if-eqz v8, :cond_2

    .line 816270
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    .line 816271
    if-nez v2, :cond_4

    .line 816272
    invoke-virtual {v1}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v9, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v2, v9}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    check-cast v2, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;

    .line 816273
    check-cast v2, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;

    .line 816274
    iget-object v9, v2, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a:LX/2BV;

    move-object v2, v9

    .line 816275
    :cond_4
    invoke-virtual {v2, v1}, LX/2BV;->a(Ljava/lang/Enum;)LX/0lb;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(LX/0lc;)V

    .line 816276
    if-nez v8, :cond_6

    .line 816277
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    goto :goto_1

    .line 816278
    :cond_5
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 816279
    :cond_6
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 816280
    if-ne v1, v4, :cond_7

    move-object v1, v4

    move-object v4, v5

    .line 816281
    :goto_2
    if-nez v6, :cond_8

    .line 816282
    :try_start_0
    invoke-virtual {v5, v8, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    move-object v5, v4

    move-object v4, v1

    .line 816283
    goto :goto_1

    .line 816284
    :cond_7
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v1, v4}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v5

    move-object v4, v5

    .line 816285
    goto :goto_2

    .line 816286
    :cond_8
    :try_start_1
    invoke-virtual {v5, v8, p2, p3, v6}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v5, v4

    move-object v4, v1

    .line 816287
    goto :goto_1

    .line 816288
    :catch_0
    move-exception v5

    .line 816289
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v5, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static b(Ljava/util/EnumMap;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<+",
            "Ljava/lang/Enum",
            "<*>;*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 816210
    invoke-virtual {p0}, Ljava/util/EnumMap;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;
    .locals 6

    .prologue
    .line 816209
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->d:LX/0lJ;

    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a:Z

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c:LX/2BV;

    iget-object v5, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;-><init>(LX/0lJ;ZLX/2BV;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816191
    const/4 v0, 0x0

    .line 816192
    if-eqz p2, :cond_0

    .line 816193
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    .line 816194
    if-eqz v1, :cond_0

    .line 816195
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v2

    .line 816196
    if-eqz v2, :cond_0

    .line 816197
    invoke-virtual {p1, v1, v2}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816198
    :cond_0
    if-nez v0, :cond_1

    .line 816199
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816200
    :cond_1
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816201
    if-nez v0, :cond_3

    .line 816202
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a:Z

    if-eqz v1, :cond_4

    .line 816203
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->d:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    move-result-object p0

    .line 816204
    :cond_2
    :goto_0
    return-object p0

    .line 816205
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    instance-of v1, v1, LX/0nU;

    if-eqz v1, :cond_4

    .line 816206
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816207
    :cond_4
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eq v0, v1, :cond_2

    .line 816208
    invoke-direct {p0, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    move-result-object p0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816190
    check-cast p1, Ljava/util/EnumMap;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(Ljava/util/EnumMap;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 816189
    check-cast p1, Ljava/util/EnumMap;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(Ljava/util/EnumMap;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816188
    check-cast p1, Ljava/util/EnumMap;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->a(Ljava/util/EnumMap;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 1

    .prologue
    .line 816186
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->c(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816187
    check-cast p1, Ljava/util/EnumMap;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;->b(Ljava/util/EnumMap;)Z

    move-result v0

    return v0
.end method
