.class public Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""

# interfaces
.implements LX/0nU;
.implements LX/2B9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/0nU;",
        "LX/2B9;"
    }
.end annotation


# instance fields
.field public final a:LX/1Xr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "*>;"
        }
    .end annotation
.end field

.field public final b:LX/0lJ;

.field public final c:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "*>;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 816750
    invoke-direct {p0, p2}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(LX/0lJ;)V

    .line 816751
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a:LX/1Xr;

    .line 816752
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b:LX/0lJ;

    .line 816753
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816754
    return-void
.end method

.method private a(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "*>;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;"
        }
    .end annotation

    .prologue
    .line 816747
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    if-eq v0, v1, :cond_0

    .line 816748
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sub-class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must override \'withDelegate\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816749
    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 816746
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a:LX/1Xr;

    invoke-interface {v0}, LX/1Xr;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816736
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_2

    .line 816737
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    instance-of v0, v0, LX/0nU;

    if-eqz v0, :cond_0

    .line 816738
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816739
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne v0, v1, :cond_1

    .line 816740
    :cond_0
    :goto_0
    return-object p0

    .line 816741
    :cond_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a:LX/1Xr;

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b:LX/0lJ;

    invoke-direct {p0, v1, v2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    move-result-object p0

    goto :goto_0

    .line 816742
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b:LX/0lJ;

    .line 816743
    if-nez v0, :cond_3

    .line 816744
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a:LX/1Xr;

    invoke-virtual {p1}, LX/0mz;->c()LX/0li;

    invoke-interface {v0}, LX/1Xr;->c()LX/0lJ;

    move-result-object v0

    .line 816745
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a:LX/1Xr;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->a(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/0my;)V
    .locals 1

    .prologue
    .line 816733
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    instance-of v0, v0, LX/2B9;

    if-eqz v0, :cond_0

    .line 816734
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    check-cast v0, LX/2B9;

    invoke-interface {v0, p1}, LX/2B9;->a(LX/0my;)V

    .line 816735
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 816728
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 816729
    if-nez v0, :cond_0

    .line 816730
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 816731
    :goto_0
    return-void

    .line 816732
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 816725
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 816726
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, v0, p2, p3, p4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    .line 816727
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 816723
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 816724
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
