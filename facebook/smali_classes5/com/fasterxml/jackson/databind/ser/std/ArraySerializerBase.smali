.class public abstract Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;
.super Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final c:LX/2Ay;


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;LX/2Ay;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase",
            "<*>;",
            "LX/2Ay;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815903
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 815904
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    .line 815905
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 815919
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;)V

    .line 815920
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    .line 815921
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;LX/2Ay;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/2Ay;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815916
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;)V

    .line 815917
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    .line 815918
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815910
    sget-object v0, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815911
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815912
    :goto_0
    return-void

    .line 815913
    :cond_0
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 815914
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815915
    invoke-virtual {p2}, LX/0nX;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815906
    invoke-virtual {p4, p1, p2}, LX/4qz;->c(Ljava/lang/Object;LX/0nX;)V

    .line 815907
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815908
    invoke-virtual {p4, p1, p2}, LX/4qz;->f(Ljava/lang/Object;LX/0nX;)V

    .line 815909
    return-void
.end method

.method public abstract b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation
.end method
