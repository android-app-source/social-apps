.class public final Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$BooleanArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase",
        "<[Z>;"
    }
.end annotation


# static fields
.field private static final a:LX/0lJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 816576
    const-class v0, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0li;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$BooleanArraySerializer;->a:LX/0lJ;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 816575
    const-class v0, [Z

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;-><init>(Ljava/lang/Class;LX/2Ay;)V

    return-void
.end method

.method private static a([ZLX/0nX;)V
    .locals 3

    .prologue
    .line 816565
    const/4 v0, 0x0

    array-length v1, p0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 816566
    aget-boolean v2, p0, v0

    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 816567
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 816568
    :cond_0
    return-void
.end method

.method private static a([Z)Z
    .locals 1

    .prologue
    .line 816574
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b([Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 816573
    array-length v1, p0

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816572
    check-cast p1, [Z

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$BooleanArraySerializer;->a([Z)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816571
    return-object p0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816570
    check-cast p1, [Z

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$BooleanArraySerializer;->a([ZLX/0nX;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816569
    check-cast p1, [Z

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$BooleanArraySerializer;->b([Z)Z

    move-result v0

    return v0
.end method
