.class public Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer",
        "<",
        "Ljava/lang/Enum",
        "<*>;>;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field public final a:LX/2BV;

.field public final b:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(LX/2BV;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 570604
    const-class v0, Ljava/lang/Enum;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;-><init>(Ljava/lang/Class;B)V

    .line 570605
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a:LX/2BV;

    .line 570606
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->b:Ljava/lang/Boolean;

    .line 570607
    return-void
.end method

.method public static a(Ljava/lang/Class;LX/0m2;LX/4pN;)Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Enum",
            "<*>;>;",
            "LX/0m2;",
            "LX/4pN;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;"
        }
    .end annotation

    .prologue
    .line 570608
    invoke-virtual {p1}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    .line 570609
    sget-object v1, LX/0mt;->WRITE_ENUMS_USING_TO_STRING:LX/0mt;

    invoke-virtual {p1, v1}, LX/0m2;->c(LX/0mt;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2BV;->a(Ljava/lang/Class;)LX/2BV;

    move-result-object v0

    .line 570610
    :goto_0
    const/4 v1, 0x1

    invoke-static {p0, p2, v1}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a(Ljava/lang/Class;LX/4pN;Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 570611
    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;

    invoke-direct {v2, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;-><init>(LX/2BV;Ljava/lang/Boolean;)V

    return-object v2

    .line 570612
    :cond_0
    invoke-static {p0, v0}, LX/2BV;->b(Ljava/lang/Class;LX/0lU;)LX/2BV;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/4pN;Z)Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/4pN;",
            "Z)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 570613
    if-nez p1, :cond_1

    move-object v1, v0

    .line 570614
    :goto_0
    if-nez v1, :cond_2

    .line 570615
    :cond_0
    :goto_1
    return-object v0

    .line 570616
    :cond_1
    iget-object v1, p1, LX/4pN;->b:LX/2BA;

    move-object v1, v1

    .line 570617
    goto :goto_0

    .line 570618
    :cond_2
    sget-object v2, LX/2BA;->ANY:LX/2BA;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/2BA;->SCALAR:LX/2BA;

    if-eq v1, v2, :cond_0

    .line 570619
    sget-object v0, LX/2BA;->STRING:LX/2BA;

    if-ne v1, v0, :cond_3

    .line 570620
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1

    .line 570621
    :cond_3
    invoke-virtual {v1}, LX/2BA;->isNumeric()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 570622
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1

    .line 570623
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported serialization shape ("

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") for Enum "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", not supported as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_5

    const-string v0, "class"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " annotation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    const-string v0, "property"

    goto :goto_2
.end method

.method private a(Ljava/lang/Enum;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 570624
    invoke-direct {p0, p3}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a(LX/0my;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570625
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 570626
    :goto_0
    return-void

    .line 570627
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a:LX/2BV;

    invoke-virtual {v0, p1}, LX/2BV;->a(Ljava/lang/Enum;)LX/0lb;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->c(LX/0lc;)V

    goto :goto_0
.end method

.method private a(LX/0my;)Z
    .locals 1

    .prologue
    .line 570628
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 570629
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 570630
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/0mt;->WRITE_ENUMS_USING_INDEX:LX/0mt;

    invoke-virtual {p1, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 570631
    if-eqz p2, :cond_0

    .line 570632
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v0

    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->e(LX/0lO;)LX/4pN;

    move-result-object v0

    .line 570633
    if-eqz v0, :cond_0

    .line 570634
    invoke-interface {p2}, LX/2Ay;->a()LX/0lJ;

    move-result-object v1

    .line 570635
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 570636
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a(Ljava/lang/Class;LX/4pN;Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 570637
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->b:Ljava/lang/Boolean;

    if-eq v1, v0, :cond_0

    .line 570638
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a:LX/2BV;

    invoke-direct {v0, v2, v1}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;-><init>(LX/2BV;Ljava/lang/Boolean;)V

    move-object p0, v0

    .line 570639
    :cond_0
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 570640
    check-cast p1, Ljava/lang/Enum;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a(Ljava/lang/Enum;LX/0nX;LX/0my;)V

    return-void
.end method
