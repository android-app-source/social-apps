.class public final Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer",
        "<[J>;"
    }
.end annotation


# static fields
.field private static final b:LX/0lJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 816683
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, LX/0li;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;->b:LX/0lJ;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 816682
    const-class v0, [J

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;LX/2Ay;LX/4qz;)V
    .locals 0

    .prologue
    .line 816680
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;LX/2Ay;LX/4qz;)V

    .line 816681
    return-void
.end method

.method private a([JLX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 816665
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;->a:LX/4qz;

    if-eqz v1, :cond_0

    .line 816666
    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 816667
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;->a:LX/4qz;

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {v2, v4, p2, v3}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;Ljava/lang/Class;)V

    .line 816668
    aget-wide v2, p1, v0

    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 816669
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$TypedPrimitiveArraySerializer;->a:LX/4qz;

    invoke-virtual {v2, v4, p2}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 816670
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 816671
    :cond_0
    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 816672
    aget-wide v2, p1, v0

    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 816673
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 816674
    :cond_1
    return-void
.end method

.method private static a([J)Z
    .locals 1

    .prologue
    .line 816679
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b([J)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 816684
    array-length v1, p0

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816678
    check-cast p1, [J

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;->a([J)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 816677
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;->c:LX/2Ay;

    invoke-direct {v0, p0, v1, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;LX/2Ay;LX/4qz;)V

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816676
    check-cast p1, [J

    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;->a([JLX/0nX;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 816675
    check-cast p1, [J

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/std/StdArraySerializers$LongArraySerializer;->b([J)Z

    move-result v0

    return v0
.end method
