.class public Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer",
        "<",
        "Ljava/util/TimeZone;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 816765
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 816774
    const-class v0, Ljava/util/TimeZone;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private static a(Ljava/util/TimeZone;LX/0nX;)V
    .locals 1

    .prologue
    .line 816772
    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 816773
    return-void
.end method

.method private static a(Ljava/util/TimeZone;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 816768
    const-class v0, Ljava/util/TimeZone;

    invoke-virtual {p3, p0, p1, v0}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;Ljava/lang/Class;)V

    .line 816769
    invoke-static {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;->a(Ljava/util/TimeZone;LX/0nX;)V

    .line 816770
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 816771
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 816767
    check-cast p1, Ljava/util/TimeZone;

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;->a(Ljava/util/TimeZone;LX/0nX;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 816766
    check-cast p1, Ljava/util/TimeZone;

    invoke-static {p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;->a(Ljava/util/TimeZone;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method
