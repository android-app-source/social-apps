.class public abstract Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
.super Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
        "<TT;>;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:LX/0lJ;

.field public final c:LX/4qz;

.field public final d:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2Ay;

.field public f:LX/2B6;


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase",
            "<*>;",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815610
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;)V

    .line 815611
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    .line 815612
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    .line 815613
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    .line 815614
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    .line 815615
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815616
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815617
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;LX/0lJ;ZLX/4qz;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "Z",
            "LX/4qz;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 815636
    invoke-direct {p0, p1, v0}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;-><init>(Ljava/lang/Class;Z)V

    .line 815637
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    .line 815638
    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, LX/0lJ;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    .line 815639
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    .line 815640
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    .line 815641
    iput-object p6, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815642
    sget-object v0, LX/2B7;->a:LX/2B7;

    move-object v0, v0

    .line 815643
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815644
    return-void
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 815645
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    .line 815646
    if-eqz v0, :cond_7

    .line 815647
    invoke-virtual {v0, p2}, LX/4qz;->a(LX/2Ay;)LX/4qz;

    move-result-object v0

    move-object v1, v0

    .line 815648
    :goto_0
    const/4 v0, 0x0

    .line 815649
    if-eqz p2, :cond_0

    .line 815650
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v2

    .line 815651
    if-eqz v2, :cond_0

    .line 815652
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v3

    .line 815653
    if-eqz v3, :cond_0

    .line 815654
    invoke-virtual {p1, v2, v3}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815655
    :cond_0
    if-nez v0, :cond_1

    .line 815656
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815657
    :cond_1
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815658
    if-nez v0, :cond_6

    .line 815659
    if-nez v0, :cond_3

    .line 815660
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    if-eqz v2, :cond_3

    .line 815661
    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    if-nez v2, :cond_2

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->a_(LX/0my;LX/2Ay;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 815662
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815663
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne v0, v2, :cond_4

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    if-ne p2, v2, :cond_4

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    if-eq v2, v1, :cond_5

    .line 815664
    :cond_4
    invoke-virtual {p0, p2, v1, v0}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;

    move-result-object p0

    .line 815665
    :cond_5
    return-object p0

    .line 815666
    :cond_6
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_3

    .line 815667
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_1

    :cond_7
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "LX/0lJ;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815628
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(LX/0lJ;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 815629
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 815630
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815631
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method public final a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815632
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    invoke-virtual {p1, p2, p3, v0}, LX/2B6;->a(Ljava/lang/Class;LX/0my;LX/2Ay;)LX/2Bd;

    move-result-object v0

    .line 815633
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    if-eq p1, v1, :cond_0

    .line 815634
    iget-object v1, v0, LX/2Bd;->b:LX/2B6;

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815635
    :cond_0
    iget-object v0, v0, LX/2Bd;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-object v0
.end method

.method public abstract a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815622
    sget-object v0, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815623
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815624
    :goto_0
    return-void

    .line 815625
    :cond_0
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 815626
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815627
    invoke-virtual {p2}, LX/0nX;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815618
    invoke-virtual {p4, p1, p2}, LX/4qz;->c(Ljava/lang/Object;LX/0nX;)V

    .line 815619
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815620
    invoke-virtual {p4, p1, p2}, LX/4qz;->f(Ljava/lang/Object;LX/0nX;)V

    .line 815621
    return-void
.end method

.method public abstract b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation
.end method
