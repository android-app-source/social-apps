.class public final Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase",
        "<",
        "Ljava/util/List",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0lJ;ZLX/4qz;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Z",
            "LX/4qz;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815668
    const-class v1, Ljava/util/List;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;-><init>(Ljava/lang/Class;LX/0lJ;ZLX/4qz;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815669
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815737
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815738
    return-void
.end method

.method private a(Ljava/util/List;LX/0nX;LX/0my;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815713
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_1

    .line 815714
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815715
    :cond_0
    :goto_0
    return-void

    .line 815716
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    if-eqz v0, :cond_2

    .line 815717
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->b(Ljava/util/List;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 815718
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 815719
    if-eqz v3, :cond_0

    .line 815720
    const/4 v1, 0x0

    .line 815721
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815722
    :goto_1
    if-ge v1, v3, :cond_0

    .line 815723
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 815724
    if-nez v4, :cond_3

    .line 815725
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 815726
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 815727
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 815728
    invoke-virtual {v0, v5}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    .line 815729
    if-nez v2, :cond_4

    .line 815730
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    invoke-virtual {v2}, LX/0lJ;->p()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 815731
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    invoke-virtual {p3, v2, v5}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    invoke-virtual {p0, v0, v2, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815732
    :goto_3
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    .line 815733
    :cond_4
    invoke-virtual {v2, v4, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 815734
    :catch_0
    move-exception v0

    .line 815735
    invoke-static {p3, v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V

    goto :goto_0

    .line 815736
    :cond_5
    :try_start_1
    invoke-virtual {p0, v0, v5, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_3
.end method

.method private a(Ljava/util/List;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815699
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 815700
    if-nez v2, :cond_1

    .line 815701
    :cond_0
    return-void

    .line 815702
    :cond_1
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    .line 815703
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 815704
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 815705
    if-nez v0, :cond_2

    .line 815706
    :try_start_0
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 815707
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 815708
    :cond_2
    if-nez v3, :cond_3

    .line 815709
    invoke-virtual {p4, v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 815710
    :catch_0
    move-exception v0

    .line 815711
    invoke-static {p3, v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V

    goto :goto_1

    .line 815712
    :cond_3
    :try_start_1
    invoke-virtual {p4, v0, p2, p3, v3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 815698
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;"
        }
    .end annotation

    .prologue
    .line 815697
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method private b(Ljava/util/List;LX/0nX;LX/0my;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815676
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 815677
    if-nez v3, :cond_1

    .line 815678
    :cond_0
    :goto_0
    return-void

    .line 815679
    :cond_1
    const/4 v1, 0x0

    .line 815680
    :try_start_0
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    .line 815681
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    .line 815682
    :goto_1
    if-ge v1, v3, :cond_0

    .line 815683
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 815684
    if-nez v5, :cond_2

    .line 815685
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 815686
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 815687
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    .line 815688
    invoke-virtual {v0, v6}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    .line 815689
    if-nez v2, :cond_3

    .line 815690
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    invoke-virtual {v2}, LX/0lJ;->p()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 815691
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    invoke-virtual {p3, v2, v6}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    invoke-virtual {p0, v0, v2, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a(LX/2B6;LX/0lJ;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815692
    :goto_3
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->f:LX/2B6;

    move-object v7, v0

    move-object v0, v2

    move-object v2, v7

    .line 815693
    :cond_3
    invoke-virtual {v2, v5, p2, p3, v4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 815694
    :catch_0
    move-exception v0

    .line 815695
    invoke-static {p3, v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V

    goto :goto_0

    .line 815696
    :cond_4
    :try_start_1
    invoke-virtual {p0, v0, v6, p3}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 815675
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
    .locals 1

    .prologue
    .line 815674
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->b(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815673
    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 815672
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    iget-object v4, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    iget-object v5, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;-><init>(LX/0lJ;ZLX/4qz;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 815671
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815670
    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedListSerializer;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method
