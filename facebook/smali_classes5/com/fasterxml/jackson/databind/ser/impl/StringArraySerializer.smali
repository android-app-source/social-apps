.class public Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase",
        "<[",
        "Ljava/lang/String;",
        ">;",
        "LX/0nU;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;

.field private static final d:LX/0lJ;


# instance fields
.field public final b:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 815969
    const-class v0, Ljava/lang/String;

    invoke-static {v0}, LX/0li;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->d:LX/0lJ;

    .line 815970
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 815966
    const-class v0, [Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;-><init>(Ljava/lang/Class;LX/2Ay;)V

    .line 815967
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815968
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815963
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/ArraySerializerBase;LX/2Ay;)V

    .line 815964
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815965
    return-void
.end method

.method private a([Ljava/lang/String;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 815952
    array-length v1, p1

    .line 815953
    if-nez v1, :cond_1

    .line 815954
    :cond_0
    :goto_0
    return-void

    .line 815955
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_2

    .line 815956
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-static {p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->a([Ljava/lang/String;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    .line 815957
    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    .line 815958
    aget-object v2, p1, v0

    .line 815959
    if-nez v2, :cond_3

    .line 815960
    invoke-virtual {p2}, LX/0nX;->h()V

    .line 815961
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 815962
    :cond_3
    aget-object v2, p1, v0

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static a([Ljava/lang/String;LX/0nX;LX/0my;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "LX/0nX;",
            "LX/0my;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815945
    const/4 v0, 0x0

    array-length v1, p0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 815946
    aget-object v2, p0, v0

    .line 815947
    if-nez v2, :cond_0

    .line 815948
    invoke-virtual {p2, p1}, LX/0my;->a(LX/0nX;)V

    .line 815949
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815950
    :cond_0
    aget-object v2, p0, v0

    invoke-virtual {p3, v2, p1, p2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_1

    .line 815951
    :cond_1
    return-void
.end method

.method private static a([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 815944
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b([Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 815922
    array-length v1, p0

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 815927
    if-eqz p2, :cond_5

    .line 815928
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v0

    .line 815929
    if-eqz v0, :cond_5

    .line 815930
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v2

    .line 815931
    if-eqz v2, :cond_5

    .line 815932
    invoke-virtual {p1, v0, v2}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815933
    :goto_0
    if-nez v0, :cond_0

    .line 815934
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815935
    :cond_0
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815936
    if-nez v0, :cond_3

    .line 815937
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815938
    :cond_1
    :goto_1
    invoke-static {v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 815939
    :cond_2
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne v0, v1, :cond_4

    .line 815940
    :goto_2
    return-object p0

    .line 815941
    :cond_3
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_1

    .line 815942
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_1

    .line 815943
    :cond_4
    new-instance v1, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;

    invoke-direct {v1, p0, p2, v0}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    move-object p0, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815926
    check-cast p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->a([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 815925
    return-object p0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 815924
    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->a([Ljava/lang/String;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815923
    check-cast p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->b([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
