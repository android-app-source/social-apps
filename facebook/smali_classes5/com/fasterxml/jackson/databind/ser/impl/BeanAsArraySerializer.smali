.class public Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
.source ""


# instance fields
.field public final a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;)V
    .locals 1

    .prologue
    .line 815553
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;LX/4rR;)V

    .line 815554
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    .line 815555
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 815550
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[Ljava/lang/String;)V

    .line 815551
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    .line 815552
    return-void
.end method

.method private b([Ljava/lang/String;)Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;
    .locals 1

    .prologue
    .line 815549
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;[Ljava/lang/String;)V

    return-object v0
.end method

.method private b(LX/0my;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 815542
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-eqz v0, :cond_0

    .line 815543
    iget-object v0, p1, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 815544
    if-eqz v0, :cond_0

    .line 815545
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 815546
    :goto_0
    array-length v0, v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    return v0

    .line 815547
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    goto :goto_0

    .line 815548
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 815519
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    if-eqz v0, :cond_0

    .line 815520
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 815521
    if-eqz v0, :cond_0

    .line 815522
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->d:[LX/2Ax;

    .line 815523
    :goto_0
    const/4 v2, 0x0

    .line 815524
    :try_start_0
    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_2

    .line 815525
    aget-object v3, v0, v2

    .line 815526
    if-nez v3, :cond_1

    .line 815527
    invoke-virtual {p2}, LX/0nX;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    .line 815528
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 815529
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->c:[LX/2Ax;

    goto :goto_0

    .line 815530
    :cond_1
    :try_start_1
    invoke-virtual {v3, p1, p2, p3}, LX/2Ax;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 815531
    :catch_0
    move-exception v1

    .line 815532
    array-length v3, v0

    if-ne v2, v3, :cond_3

    const-string v0, "[anySetter]"

    .line 815533
    :goto_3
    invoke-static {p3, v1, p1, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V

    .line 815534
    :cond_2
    return-void

    .line 815535
    :cond_3
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 815536
    :catch_1
    move-exception v1

    .line 815537
    new-instance v3, LX/28E;

    const-string v4, "Infinite recursion (StackOverflowError)"

    invoke-direct {v3, v4, v1}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 815538
    array-length v1, v0

    if-ne v2, v1, :cond_4

    const-string v0, "[anySetter]"

    .line 815539
    :goto_4
    new-instance v1, LX/4pp;

    invoke-direct {v1, p1, v0}, LX/4pp;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/28E;->a(LX/4pp;)V

    .line 815540
    throw v3

    .line 815541
    :cond_4
    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method


# virtual methods
.method public final a(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815518
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    invoke-virtual {v0, p1}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4rR;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
    .locals 1

    .prologue
    .line 815556
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    invoke-virtual {v0, p1}, Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;->a(LX/4rR;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a([Ljava/lang/String;)Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
    .locals 1

    .prologue
    .line 815517
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->b([Ljava/lang/String;)Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 815511
    sget-object v0, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3}, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->b(LX/0my;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815512
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->d(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815513
    :goto_0
    return-void

    .line 815514
    :cond_0
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 815515
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->d(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815516
    invoke-virtual {p2}, LX/0nX;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 815509
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/BeanAsArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    .line 815510
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 815506
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/fasterxml/jackson/databind/ser/std/BeanSerializerBase;
    .locals 0

    .prologue
    .line 815508
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 815507
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BeanAsArraySerializer for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
