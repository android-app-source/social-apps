.class public final Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StaticListSerializerBase;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StaticListSerializerBase",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;",
        "LX/0nU;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;


# instance fields
.field public final b:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 815807
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a:Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 815805
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;-><init>(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815806
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815802
    const-class v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StaticListSerializerBase;-><init>(Ljava/lang/Class;)V

    .line 815803
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815804
    return-void
.end method

.method private a(Ljava/util/List;LX/0nX;LX/0my;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815793
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 815794
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v1, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    invoke-virtual {p3, v1}, LX/0my;->a(LX/0mt;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 815795
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b(Ljava/util/List;LX/0nX;LX/0my;)V

    .line 815796
    :goto_0
    return-void

    .line 815797
    :cond_0
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 815798
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-nez v1, :cond_1

    .line 815799
    invoke-static {p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;I)V

    .line 815800
    :goto_1
    invoke-virtual {p2}, LX/0nX;->e()V

    goto :goto_0

    .line 815801
    :cond_1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b(Ljava/util/List;LX/0nX;LX/0my;I)V

    goto :goto_1
.end method

.method private static final a(Ljava/util/List;LX/0nX;LX/0my;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            "LX/0my;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 815783
    const/4 v0, 0x0

    move v1, v0

    .line 815784
    :goto_0
    if-ge v1, p3, :cond_1

    .line 815785
    :try_start_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 815786
    if-nez v0, :cond_0

    .line 815787
    invoke-virtual {p2, p1}, LX/0my;->a(LX/0nX;)V

    .line 815788
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 815789
    :cond_0
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 815790
    :catch_0
    move-exception v0

    .line 815791
    invoke-static {p2, v0, p0, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V

    .line 815792
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;LX/0nX;LX/0my;LX/4qz;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815743
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 815744
    invoke-virtual {p4, p1, p2}, LX/4qz;->c(Ljava/lang/Object;LX/0nX;)V

    .line 815745
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-nez v1, :cond_0

    .line 815746
    invoke-static {p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;I)V

    .line 815747
    :goto_0
    invoke-virtual {p4, p1, p2}, LX/4qz;->f(Ljava/lang/Object;LX/0nX;)V

    .line 815748
    return-void

    .line 815749
    :cond_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b(Ljava/util/List;LX/0nX;LX/0my;I)V

    goto :goto_0
.end method

.method private final b(Ljava/util/List;LX/0nX;LX/0my;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 815779
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-nez v0, :cond_0

    .line 815780
    invoke-static {p1, p2, p3, v1}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;I)V

    .line 815781
    :goto_0
    return-void

    .line 815782
    :cond_0
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b(Ljava/util/List;LX/0nX;LX/0my;I)V

    goto :goto_0
.end method

.method private final b(Ljava/util/List;LX/0nX;LX/0my;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0nX;",
            "LX/0my;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 815769
    :try_start_0
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815770
    :goto_0
    if-ge v1, p4, :cond_1

    .line 815771
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 815772
    if-nez v0, :cond_0

    .line 815773
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 815774
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 815775
    :cond_0
    invoke-virtual {v2, v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 815776
    :catch_0
    move-exception v0

    .line 815777
    invoke-static {p3, v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V

    .line 815778
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 815752
    if-eqz p2, :cond_5

    .line 815753
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v0

    .line 815754
    if-eqz v0, :cond_5

    .line 815755
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v2

    .line 815756
    if-eqz v2, :cond_5

    .line 815757
    invoke-virtual {p1, v0, v2}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815758
    :goto_0
    if-nez v0, :cond_0

    .line 815759
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815760
    :cond_0
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815761
    if-nez v0, :cond_3

    .line 815762
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 815763
    :cond_1
    :goto_1
    invoke-static {v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 815764
    :cond_2
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-ne v0, v1, :cond_4

    .line 815765
    :goto_2
    return-object p0

    .line 815766
    :cond_3
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_1

    .line 815767
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_1

    .line 815768
    :cond_4
    new-instance p0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;-><init>(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 815751
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 815750
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a(Ljava/util/List;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method
