.class public Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase",
        "<",
        "Ljava/util/Iterator",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0lJ;ZLX/4qz;LX/2Ay;)V
    .locals 7

    .prologue
    .line 815832
    const-class v1, Ljava/util/Iterator;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;-><init>(Ljava/lang/Class;LX/0lJ;ZLX/4qz;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815833
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815830
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;-><init>(Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815831
    return-void
.end method

.method private a(Ljava/util/Iterator;LX/0nX;LX/0my;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 815816
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 815817
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->c:LX/4qz;

    move-object v1, v0

    .line 815818
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 815819
    if-nez v4, :cond_2

    .line 815820
    invoke-virtual {p3, p2}, LX/0my;->a(LX/0nX;)V

    .line 815821
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 815822
    :cond_1
    return-void

    .line 815823
    :cond_2
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 815824
    if-ne v2, v0, :cond_3

    move-object v2, v1

    .line 815825
    :goto_1
    if-nez v3, :cond_4

    .line 815826
    invoke-virtual {v2, v4, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 815827
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    invoke-virtual {p3, v2, v0}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    move-object v0, v2

    move-object v2, v1

    .line 815828
    goto :goto_1

    .line 815829
    :cond_4
    invoke-virtual {v2, v4, p2, p3, v3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Iterator;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 815815
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;"
        }
    .end annotation

    .prologue
    .line 815814
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;-><init>(Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;
    .locals 1

    .prologue
    .line 815813
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;->b(LX/2Ay;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815808
    check-cast p1, Ljava/util/Iterator;

    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;->a(Ljava/util/Iterator;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qz;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/ContainerSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 815812
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->b:LX/0lJ;

    iget-boolean v2, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->a:Z

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/ser/std/AsArraySerializerBase;->e:LX/2Ay;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;-><init>(LX/0lJ;ZLX/4qz;LX/2Ay;)V

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 815811
    check-cast p1, Ljava/util/Iterator;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/ser/impl/IteratorSerializer;->a(Ljava/util/Iterator;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 815809
    const/4 v0, 0x0

    move v0, v0

    .line 815810
    return v0
.end method
