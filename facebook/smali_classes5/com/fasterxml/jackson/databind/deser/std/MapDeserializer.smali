.class public Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;
.source ""

# interfaces
.implements LX/1Xy;
.implements LX/1Xx;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;>;",
        "LX/1Xy;",
        "LX/1Xx;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x2ee3642d0404d830L


# instance fields
.field public _delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _hasDefaultCreator:Z

.field public _ignorableProperties:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final _keyDeserializer:LX/1Xt;

.field public final _mapType:LX/0lJ;

.field public _propertyBasedCreator:LX/4qF;

.field public _standardStringKey:Z

.field public final _valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _valueInstantiator:LX/320;

.field public final _valueTypeDeserializer:LX/4qw;


# direct methods
.method public constructor <init>(LX/0lJ;LX/320;LX/1Xt;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/320;",
            "LX/1Xt;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qw;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 814070
    const-class v0, Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;-><init>(Ljava/lang/Class;)V

    .line 814071
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    .line 814072
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    .line 814073
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814074
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 814075
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    .line 814076
    invoke-virtual {p2}, LX/320;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_hasDefaultCreator:Z

    .line 814077
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814078
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    .line 814079
    invoke-static {p1, p3}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/0lJ;LX/1Xt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_standardStringKey:Z

    .line 814080
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;LX/1Xt;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;",
            "LX/1Xt;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qw;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 814058
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;-><init>(Ljava/lang/Class;)V

    .line 814059
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    .line 814060
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    .line 814061
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814062
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 814063
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    .line 814064
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    .line 814065
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814066
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_hasDefaultCreator:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_hasDefaultCreator:Z

    .line 814067
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    .line 814068
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    invoke-static {v0, p2}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/0lJ;LX/1Xt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_standardStringKey:Z

    .line 814069
    return-void
.end method

.method private a(LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;"
        }
    .end annotation

    .prologue
    .line 813896
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    if-ne v0, p4, :cond_0

    .line 813897
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;LX/1Xt;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;Ljava/util/HashSet;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a(LX/15w;LX/0n3;Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814051
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814052
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_0

    .line 814053
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814054
    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_standardStringKey:Z

    if-eqz v0, :cond_1

    .line 814055
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->c(LX/15w;LX/0n3;Ljava/util/Map;)V

    .line 814056
    :goto_0
    return-object p3

    .line 814057
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b(LX/15w;LX/0n3;Ljava/util/Map;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Throwable;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 814044
    move-object v0, p0

    :goto_0
    instance-of v1, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 814045
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 814046
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 814047
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 814048
    :cond_1
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_2

    instance-of v1, v0, LX/28E;

    if-nez v1, :cond_2

    .line 814049
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 814050
    :cond_2
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method private static a(LX/0lJ;LX/1Xt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 814038
    if-nez p1, :cond_1

    .line 814039
    :cond_0
    :goto_0
    return v0

    .line 814040
    :cond_1
    invoke-virtual {p0}, LX/0lJ;->q()LX/0lJ;

    move-result-object v1

    .line 814041
    if-eqz v1, :cond_0

    .line 814042
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 814043
    const-class v2, Ljava/lang/String;

    if-eq v1, v2, :cond_2

    const-class v2, Ljava/lang/Object;

    if-ne v1, v2, :cond_3

    :cond_2
    invoke-static {p1}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/1Xt;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814035
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    .line 814036
    iget-object p0, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p0

    .line 814037
    return-object v0
.end method

.method private b(LX/15w;LX/0n3;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 814015
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814016
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 814017
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 814018
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    .line 814019
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814020
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 814021
    :goto_0
    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v4, :cond_4

    .line 814022
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 814023
    invoke-virtual {v1, v0, p2}, LX/1Xt;->a(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;

    move-result-object v4

    .line 814024
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    .line 814025
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814026
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 814027
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 814028
    :cond_1
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v5, v0, :cond_2

    .line 814029
    const/4 v0, 0x0

    .line 814030
    :goto_2
    invoke-interface {p3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 814031
    :cond_2
    if-nez v3, :cond_3

    .line 814032
    invoke-virtual {v2, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 814033
    :cond_3
    invoke-virtual {v2, p1, p2, v3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 814034
    :cond_4
    return-void
.end method

.method private c(LX/15w;LX/0n3;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 813976
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    .line 813977
    invoke-virtual {v2, p1, p2, v1}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v3

    .line 813978
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813979
    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v4, :cond_0

    .line 813980
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 813981
    :cond_0
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813982
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813983
    :goto_0
    sget-object v6, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v6, :cond_6

    .line 813984
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 813985
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    .line 813986
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 813987
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 813988
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 813989
    :cond_2
    invoke-virtual {v2, v0}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 813990
    if-eqz v0, :cond_3

    .line 813991
    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v6

    .line 813992
    invoke-virtual {v0}, LX/32s;->c()I

    move-result v0

    invoke-virtual {v3, v0, v6}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813993
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 813994
    :try_start_0
    invoke-virtual {v2, p2, v3}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 813995
    invoke-direct {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b(LX/15w;LX/0n3;Ljava/util/Map;)V

    .line 813996
    :goto_2
    return-object v0

    .line 813997
    :catch_0
    move-exception v0

    .line 813998
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    .line 813999
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 814000
    invoke-static {v0, v2}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(Ljava/lang/Throwable;Ljava/lang/Object;)V

    move-object v0, v1

    .line 814001
    goto :goto_2

    .line 814002
    :cond_3
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 814003
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    invoke-virtual {v7, v0, p2}, LX/1Xt;->a(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;

    move-result-object v7

    .line 814004
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v6, v0, :cond_4

    move-object v0, v1

    .line 814005
    :goto_3
    invoke-virtual {v3, v7, v0}, LX/4qL;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 814006
    :cond_4
    if-nez v5, :cond_5

    .line 814007
    invoke-virtual {v4, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 814008
    :cond_5
    invoke-virtual {v4, p1, p2, v5}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 814009
    :cond_6
    :try_start_1
    invoke-virtual {v2, p2, v3}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 814010
    :catch_1
    move-exception v0

    .line 814011
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    .line 814012
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 814013
    invoke-static {v0, v2}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(Ljava/lang/Throwable;Ljava/lang/Object;)V

    move-object v0, v1

    .line 814014
    goto :goto_2
.end method

.method private c(LX/15w;LX/0n3;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813958
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813959
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 813960
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 813961
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813962
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813963
    :goto_0
    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v3, :cond_4

    .line 813964
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 813965
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 813966
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 813967
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 813968
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 813969
    :cond_1
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v4, :cond_2

    .line 813970
    const/4 v0, 0x0

    .line 813971
    :goto_2
    invoke-interface {p3, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 813972
    :cond_2
    if-nez v2, :cond_3

    .line 813973
    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 813974
    :cond_3
    invoke-virtual {v1, p1, p2, v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 813975
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813957
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    return-object v0
.end method

.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 813932
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    .line 813933
    if-nez v0, :cond_2

    .line 813934
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->b(LX/0lJ;LX/2Ay;)LX/1Xt;

    move-result-object v0

    move-object v1, v0

    .line 813935
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813936
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 813937
    if-nez v0, :cond_3

    .line 813938
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 813939
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813940
    if-eqz v2, :cond_1

    .line 813941
    invoke-virtual {v2, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v2

    .line 813942
    :cond_1
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    .line 813943
    invoke-virtual {p1}, LX/0n3;->f()LX/0lU;

    move-result-object v3

    .line 813944
    if-eqz v3, :cond_5

    if-eqz p2, :cond_5

    .line 813945
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v5

    .line 813946
    if-eqz v5, :cond_5

    .line 813947
    if-nez v4, :cond_4

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 813948
    :goto_2
    array-length v6, v5

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v6, :cond_6

    aget-object v7, v5, v4

    .line 813949
    invoke-virtual {v3, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 813950
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 813951
    :cond_2
    instance-of v1, v0, LX/2tf;

    if-eqz v1, :cond_7

    .line 813952
    check-cast v0, LX/2tf;

    invoke-interface {v0}, LX/2tf;->a()LX/1Xt;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 813953
    :cond_3
    instance-of v2, v0, LX/1Xy;

    if-eqz v2, :cond_0

    .line 813954
    check-cast v0, LX/1Xy;

    invoke-interface {v0, p1, p2}, LX/1Xy;->a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_1

    .line 813955
    :cond_4
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_2

    :cond_5
    move-object v3, v4

    .line 813956
    :cond_6
    invoke-direct {p0, v1, v2, v0, v3}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;

    move-result-object v0

    return-object v0

    :cond_7
    move-object v1, v0

    goto :goto_0
.end method

.method public a(LX/15w;LX/0n3;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813916
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_0

    .line 813917
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->c(LX/15w;LX/0n3;)Ljava/util/Map;

    move-result-object v0

    .line 813918
    :goto_0
    return-object v0

    .line 813919
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_1

    .line 813920
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    .line 813921
    :cond_1
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_hasDefaultCreator:Z

    if-nez v0, :cond_2

    .line 813922
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "No default constructor found"

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813923
    :cond_2
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813924
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v1, :cond_4

    .line 813925
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_3

    .line 813926
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    .line 813927
    :cond_3
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 813928
    :cond_4
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 813929
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_standardStringKey:Z

    if-eqz v1, :cond_5

    .line 813930
    invoke-direct {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->c(LX/15w;LX/0n3;Ljava/util/Map;)V

    goto :goto_0

    .line 813931
    :cond_5
    invoke-direct {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->b(LX/15w;LX/0n3;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final a(LX/0n3;)V
    .locals 3

    .prologue
    .line 813904
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813905
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->k()LX/0lJ;

    move-result-object v0

    .line 813906
    if-nez v0, :cond_0

    .line 813907
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid delegate-creator definition for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": value instantiator ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") returned true for \'canCreateUsingDelegate()\', but null for \'getDelegateType()\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813908
    :cond_0
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813909
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813910
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    .line 813911
    iget-object v1, p1, LX/0n3;->_config:LX/0mu;

    move-object v1, v1

    .line 813912
    invoke-virtual {v0, v1}, LX/320;->a(LX/0mu;)[LX/32s;

    move-result-object v0

    .line 813913
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_valueInstantiator:LX/320;

    invoke-static {p1, v1, v0}, LX/4qF;->a(LX/0n3;LX/320;[LX/32s;)LX/4qF;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_propertyBasedCreator:LX/4qF;

    .line 813914
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_mapType:LX/0lJ;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_keyDeserializer:LX/1Xt;

    invoke-static {v0, v1}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/0lJ;LX/1Xt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_standardStringKey:Z

    .line 813915
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813901
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->_ignorableProperties:Ljava/util/HashSet;

    .line 813902
    return-void

    .line 813903
    :cond_1
    invoke-static {p1}, LX/0nj;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813900
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/15w;LX/0n3;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813899
    check-cast p3, Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/MapDeserializer;->a(LX/15w;LX/0n3;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813898
    invoke-virtual {p3, p1, p2}, LX/4qw;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
