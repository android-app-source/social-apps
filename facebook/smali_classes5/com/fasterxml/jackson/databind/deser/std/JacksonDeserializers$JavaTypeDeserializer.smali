.class public final Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer",
        "<",
        "LX/0lJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 813724
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 813725
    const-class v0, LX/0lJ;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public a(LX/15w;LX/0n3;)LX/0lJ;
    .locals 2

    .prologue
    .line 813726
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813727
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 813728
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 813729
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 813730
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->getEmptyValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 813731
    :goto_0
    return-object v0

    .line 813732
    :cond_0
    invoke-virtual {p2}, LX/0mz;->c()LX/0li;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0li;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 813733
    :cond_1
    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_2

    .line 813734
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    goto :goto_0

    .line 813735
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813736
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;->a(LX/15w;LX/0n3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method
