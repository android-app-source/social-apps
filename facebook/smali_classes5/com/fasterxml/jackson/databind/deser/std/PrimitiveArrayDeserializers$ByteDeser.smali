.class public final Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$ByteDeser;
.super Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers",
        "<[B>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814318
    const-class v0, [B

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private final c(LX/15w;LX/0n3;)[B
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 814319
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v2, :cond_0

    sget-object v0, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814320
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 814321
    const/4 v0, 0x0

    .line 814322
    :goto_0
    return-object v0

    .line 814323
    :cond_0
    sget-object v0, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 814324
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814325
    :cond_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814326
    sget-object v2, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-eq v0, v2, :cond_2

    sget-object v2, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    if-ne v0, v2, :cond_3

    .line 814327
    :cond_2
    invoke-virtual {p1}, LX/15w;->v()B

    move-result v0

    .line 814328
    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [B

    aput-byte v0, v2, v1

    move-object v0, v2

    goto :goto_0

    .line 814329
    :cond_3
    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v0, v2, :cond_4

    .line 814330
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    :cond_4
    move v0, v1

    .line 814331
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 814295
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814296
    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v2, :cond_0

    .line 814297
    invoke-virtual {p2}, LX/0n3;->h()LX/0ln;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/15w;->a(LX/0ln;)[B

    move-result-object v0

    .line 814298
    :goto_0
    return-object v0

    .line 814299
    :cond_0
    sget-object v2, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v2, :cond_2

    .line 814300
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 814301
    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 814302
    :cond_1
    instance-of v2, v0, [B

    if-eqz v2, :cond_2

    .line 814303
    check-cast v0, [B

    check-cast v0, [B

    goto :goto_0

    .line 814304
    :cond_2
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    if-nez v0, :cond_3

    .line 814305
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$ByteDeser;->c(LX/15w;LX/0n3;)[B

    move-result-object v0

    goto :goto_0

    .line 814306
    :cond_3
    invoke-virtual {p2}, LX/0n3;->m()LX/0nj;

    move-result-object v0

    invoke-virtual {v0}, LX/0nj;->b()LX/4rf;

    move-result-object v5

    .line 814307
    invoke-virtual {v5}, LX/4rd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v2, v0

    move v0, v1

    .line 814308
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_7

    .line 814309
    sget-object v4, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-eq v3, v4, :cond_4

    sget-object v4, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    if-ne v3, v4, :cond_5

    .line 814310
    :cond_4
    invoke-virtual {p1}, LX/15w;->v()B

    move-result v3

    .line 814311
    :goto_2
    array-length v4, v2

    if-lt v0, v4, :cond_8

    .line 814312
    invoke-virtual {v5, v2, v0}, LX/4rd;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move v4, v1

    move-object v2, v0

    .line 814313
    :goto_3
    add-int/lit8 v0, v4, 0x1

    aput-byte v3, v2, v4

    goto :goto_1

    .line 814314
    :cond_5
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_6

    .line 814315
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    :cond_6
    move v3, v1

    .line 814316
    goto :goto_2

    .line 814317
    :cond_7
    invoke-virtual {v5, v2, v0}, LX/4rd;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0

    :cond_8
    move v4, v0

    goto :goto_3
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814294
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$ByteDeser;->a(LX/15w;LX/0n3;)[B

    move-result-object v0

    return-object v0
.end method
