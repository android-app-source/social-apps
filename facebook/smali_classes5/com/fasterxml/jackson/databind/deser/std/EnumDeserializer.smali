.class public Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer",
        "<",
        "Ljava/lang/Enum",
        "<*>;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x51c9140fe45f7b1eL


# instance fields
.field public final _resolver:LX/4rm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4rm",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4rm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4rm",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813606
    const-class v0, Ljava/lang/Enum;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;-><init>(Ljava/lang/Class;)V

    .line 813607
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    .line 813608
    return-void
.end method

.method public static a(LX/0mu;Ljava/lang/Class;LX/2At;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0mu;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2At;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 813609
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/2At;->a(I)Ljava/lang/Class;

    move-result-object v0

    .line 813610
    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_1

    .line 813611
    const/4 v0, 0x0

    .line 813612
    :goto_0
    invoke-virtual {p0}, LX/0m4;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 813613
    iget-object v1, p2, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v1, v1

    .line 813614
    invoke-static {v1}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 813615
    :cond_0
    new-instance v1, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer$FactoryBasedDeserializer;

    invoke-direct {v1, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer$FactoryBasedDeserializer;-><init>(Ljava/lang/Class;LX/2At;Ljava/lang/Class;)V

    return-object v1

    .line 813616
    :cond_1
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq v0, v1, :cond_2

    const-class v1, Ljava/lang/Integer;

    if-ne v0, v1, :cond_3

    .line 813617
    :cond_2
    const-class v0, Ljava/lang/Integer;

    goto :goto_0

    .line 813618
    :cond_3
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-eq v0, v1, :cond_4

    const-class v1, Ljava/lang/Long;

    if-ne v0, v1, :cond_5

    .line 813619
    :cond_4
    const-class v0, Ljava/lang/Long;

    goto :goto_0

    .line 813620
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parameter #0 type for factory method ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") not suitable, must be java.lang.String or int/Integer/long/Long"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(LX/15w;LX/0n3;)Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 813621
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813622
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_4

    .line 813623
    :cond_0
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 813624
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    invoke-virtual {v0, v1}, LX/4rm;->a(Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    .line 813625
    if-nez v0, :cond_2

    .line 813626
    sget-object v2, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p2, v2}, LX/0n3;->a(LX/0mv;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 813627
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 813628
    :cond_1
    const/4 v0, 0x0

    .line 813629
    :cond_2
    return-object v0

    .line 813630
    :cond_3
    sget-object v2, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    invoke-virtual {p2, v2}, LX/0n3;->a(LX/0mv;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 813631
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    .line 813632
    iget-object v2, v0, LX/4rm;->_enumClass:Ljava/lang/Class;

    move-object v0, v2

    .line 813633
    const-string v2, "value not one of declared Enum instance names"

    invoke-virtual {p2, v1, v0, v2}, LX/0n3;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813634
    :cond_4
    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_6

    .line 813635
    sget-object v0, LX/0mv;->FAIL_ON_NUMBERS_FOR_ENUMS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 813636
    const-string v0, "Not allowed to deserialize Enum value out of JSON number (disable DeserializationConfig.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS to allow)"

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813637
    :cond_5
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v0

    .line 813638
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    invoke-virtual {v1, v0}, LX/4rm;->a(I)Ljava/lang/Enum;

    move-result-object v0

    .line 813639
    if-nez v0, :cond_2

    sget-object v1, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 813640
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    .line 813641
    iget-object v1, v0, LX/4rm;->_enumClass:Ljava/lang/Class;

    move-object v0, v1

    .line 813642
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "index value outside legal index range [0.."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    invoke-virtual {v2}, LX/4rm;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0n3;->b(Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813643
    :cond_6
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->_resolver:LX/4rm;

    .line 813644
    iget-object v1, v0, LX/4rm;->_enumClass:Ljava/lang/Class;

    move-object v0, v1

    .line 813645
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813646
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/EnumDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public final isCachable()Z
    .locals 1

    .prologue
    .line 813647
    const/4 v0, 0x1

    return v0
.end method
