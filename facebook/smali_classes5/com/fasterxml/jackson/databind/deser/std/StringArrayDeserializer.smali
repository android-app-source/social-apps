.class public final Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<[",
        "Ljava/lang/String;",
        ">;",
        "LX/1Xy;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;

.field private static final serialVersionUID:J = -0x69535cf715633df5L


# instance fields
.field public _elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 814647
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814644
    const-class v0, [Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    .line 814645
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814646
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 814641
    const-class v0, [Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    .line 814642
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814643
    return-void
.end method

.method private c(LX/15w;LX/0n3;)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 814590
    invoke-virtual {p2}, LX/0n3;->l()LX/4rv;

    move-result-object v5

    .line 814591
    invoke-virtual {v5}, LX/4rv;->a()[Ljava/lang/Object;

    move-result-object v0

    .line 814592
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move v1, v2

    move-object v3, v0

    .line 814593
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v4, :cond_1

    .line 814594
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x0

    move-object v4, v0

    .line 814595
    :goto_1
    array-length v0, v3

    if-lt v1, v0, :cond_2

    .line 814596
    invoke-virtual {v5, v3}, LX/4rv;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move v3, v2

    .line 814597
    :goto_2
    add-int/lit8 v0, v3, 0x1

    aput-object v4, v1, v3

    move-object v3, v1

    move v1, v0

    .line 814598
    goto :goto_0

    .line 814599
    :cond_0
    invoke-virtual {v6, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_1

    .line 814600
    :cond_1
    const-class v0, Ljava/lang/String;

    invoke-virtual {v5, v3, v1, v0}, LX/4rv;->a([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 814601
    invoke-virtual {p2, v5}, LX/0n3;->a(LX/4rv;)V

    .line 814602
    return-object v0

    :cond_2
    move v7, v1

    move-object v1, v3

    move v3, v7

    goto :goto_2
.end method

.method private final d(LX/15w;LX/0n3;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 814634
    sget-object v1, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 814635
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_0

    sget-object v1, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814636
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 814637
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 814638
    :goto_0
    return-object v0

    .line 814639
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814640
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v3, v4, :cond_2

    :goto_1
    aput-object v0, v1, v2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->w(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814623
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814624
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 814625
    if-nez v0, :cond_3

    .line 814626
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0n3;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 814627
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 814628
    const/4 v0, 0x0

    .line 814629
    :cond_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eq v1, v0, :cond_2

    .line 814630
    new-instance p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;-><init>(Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 814631
    :cond_2
    return-object p0

    .line 814632
    :cond_3
    instance-of v1, v0, LX/1Xy;

    if-eqz v1, :cond_0

    .line 814633
    check-cast v0, LX/1Xy;

    invoke-interface {v0, p1, p2}, LX/1Xy;->a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/15w;LX/0n3;)[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 814605
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 814606
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->d(LX/15w;LX/0n3;)[Ljava/lang/String;

    move-result-object v0

    .line 814607
    :goto_0
    return-object v0

    .line 814608
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_1

    .line 814609
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->c(LX/15w;LX/0n3;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 814610
    :cond_1
    invoke-virtual {p2}, LX/0n3;->l()LX/4rv;

    move-result-object v5

    .line 814611
    invoke-virtual {v5}, LX/4rv;->a()[Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 814612
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_4

    .line 814613
    sget-object v4, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v3, v4, :cond_2

    .line 814614
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    .line 814615
    :goto_2
    array-length v4, v2

    if-lt v0, v4, :cond_5

    .line 814616
    invoke-virtual {v5, v2}, LX/4rv;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move v4, v1

    .line 814617
    :goto_3
    add-int/lit8 v0, v4, 0x1

    aput-object v3, v2, v4

    goto :goto_1

    .line 814618
    :cond_2
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v3, v4, :cond_3

    .line 814619
    const/4 v3, 0x0

    goto :goto_2

    .line 814620
    :cond_3
    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->w(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 814621
    :cond_4
    const-class v1, Ljava/lang/String;

    invoke-virtual {v5, v2, v0, v1}, LX/4rv;->a([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 814622
    invoke-virtual {p2, v5}, LX/0n3;->a(LX/4rv;)V

    goto :goto_0

    :cond_5
    move v4, v0

    goto :goto_3
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814604
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringArrayDeserializer;->a(LX/15w;LX/0n3;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814603
    invoke-virtual {p3, p1, p2}, LX/4qw;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
