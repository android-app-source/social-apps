.class public Lcom/fasterxml/jackson/databind/deser/std/ThrowableDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;)V
    .locals 1

    .prologue
    .line 814648
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;)V

    .line 814649
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ThrowableDeserializer;->_vanillaProcessing:Z

    .line 814650
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;LX/4ro;)V
    .locals 0

    .prologue
    .line 814651
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4ro;)V

    .line 814652
    return-void
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 814653
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_1

    .line 814654
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v4

    .line 814655
    :cond_0
    :goto_0
    return-object v4

    .line 814656
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_2

    .line 814657
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_0

    .line 814658
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 814659
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not instantiate abstract type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814660
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->c()Z

    move-result v7

    .line 814661
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->h()Z

    move-result v0

    .line 814662
    if-nez v7, :cond_4

    if-nez v0, :cond_4

    .line 814663
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not deserialize Throwable of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " without having a default contructor, a single-String-arg constructor; or explicit @JsonCreator"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v1, v5

    move-object v2, v3

    move-object v4, v3

    .line 814664
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v6, :cond_d

    .line 814665
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 814666
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v6, v0}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v6

    .line 814667
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 814668
    if-eqz v6, :cond_7

    .line 814669
    if-eqz v4, :cond_5

    .line 814670
    invoke-virtual {v6, p1, p2, v4}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V

    move v0, v1

    move-object v1, v2

    move-object v2, v4

    .line 814671
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-object v4, v2

    move-object v2, v1

    move v1, v0

    goto :goto_1

    .line 814672
    :cond_5
    if-nez v2, :cond_6

    .line 814673
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 814674
    iget v2, v0, LX/32t;->_size:I

    move v0, v2

    .line 814675
    add-int/2addr v0, v0

    new-array v2, v0, [Ljava/lang/Object;

    .line 814676
    :cond_6
    add-int/lit8 v8, v1, 0x1

    aput-object v6, v2, v1

    .line 814677
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v6, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v2, v8

    move-object v1, v2

    move-object v2, v4

    .line 814678
    goto :goto_2

    .line 814679
    :cond_7
    const-string v6, "message"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 814680
    if-eqz v7, :cond_9

    .line 814681
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p2, v4}, LX/320;->a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 814682
    if-eqz v2, :cond_c

    move v6, v5

    .line 814683
    :goto_3
    if-ge v6, v1, :cond_8

    .line 814684
    aget-object v0, v2, v6

    check-cast v0, LX/32s;

    .line 814685
    add-int/lit8 v8, v6, 0x1

    aget-object v8, v2, v8

    invoke-virtual {v0, v4, v8}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 814686
    add-int/lit8 v0, v6, 0x2

    move v6, v0

    goto :goto_3

    :cond_8
    move v0, v1

    move-object v2, v4

    move-object v1, v3

    .line 814687
    goto :goto_2

    .line 814688
    :cond_9
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 814689
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    move v0, v1

    move-object v1, v2

    move-object v2, v4

    .line 814690
    goto :goto_2

    .line 814691
    :cond_a
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v6, :cond_b

    .line 814692
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v6, p1, p2, v4, v0}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v1

    move-object v1, v2

    move-object v2, v4

    .line 814693
    goto :goto_2

    .line 814694
    :cond_b
    invoke-virtual {p0, p1, p2, v4, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    :cond_c
    move v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_2

    .line 814695
    :cond_d
    if-nez v4, :cond_0

    .line 814696
    if-eqz v7, :cond_e

    .line 814697
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0, p2, v3}, LX/320;->a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 814698
    :goto_4
    if-eqz v2, :cond_0

    .line 814699
    :goto_5
    if-ge v5, v1, :cond_0

    .line 814700
    aget-object v0, v2, v5

    check-cast v0, LX/32s;

    .line 814701
    add-int/lit8 v3, v5, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v0, v4, v3}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 814702
    add-int/lit8 v5, v5, 0x2

    goto :goto_5

    .line 814703
    :cond_e
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v4

    goto :goto_4
.end method

.method public final unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814704
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/databind/deser/std/ThrowableDeserializer;

    if-eq v0, v1, :cond_0

    .line 814705
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/ThrowableDeserializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/deser/std/ThrowableDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;LX/4ro;)V

    move-object p0, v0

    goto :goto_0
.end method
