.class public final Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 814147
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814148
    const-class v0, Ljava/lang/Number;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)Ljava/lang/Number;
    .locals 6

    .prologue
    .line 814149
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814150
    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 814151
    sget-object v0, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814152
    invoke-virtual {p1}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    .line 814153
    :goto_0
    return-object v0

    .line 814154
    :cond_0
    invoke-virtual {p1}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    goto :goto_0

    .line 814155
    :cond_1
    sget-object v1, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    if-ne v0, v1, :cond_3

    .line 814156
    sget-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 814157
    invoke-virtual {p1}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 814158
    :cond_2
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 814159
    :cond_3
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_8

    .line 814160
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 814161
    const/16 v0, 0x2e

    :try_start_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_5

    .line 814162
    sget-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 814163
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 814164
    :catch_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    const-string v2, "not a valid number"

    invoke-virtual {p2, v1, v0, v2}, LX/0n3;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814165
    :cond_4
    :try_start_1
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, v1}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 814166
    :cond_5
    sget-object v0, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 814167
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 814168
    :cond_6
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 814169
    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-gtz v0, :cond_7

    const-wide/32 v4, -0x80000000

    cmp-long v0, v2, v4

    if-ltz v0, :cond_7

    .line 814170
    long-to-int v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 814171
    :cond_7
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 814172
    :cond_8
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v1, v0}, LX/0n3;->a(Ljava/lang/Class;LX/15z;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814173
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 814174
    sget-object v0, LX/4qU;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 814175
    invoke-virtual {p3, p1, p2}, LX/4qw;->c(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 814176
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/NumberDeserializers$NumberDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Number;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
