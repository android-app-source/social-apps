.class public final Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;
.super Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers",
        "<[I>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 814403
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;->a:Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814404
    const-class v0, [I

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private final c(LX/15w;LX/0n3;)[I
    .locals 3

    .prologue
    .line 814405
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814406
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 814407
    const/4 v0, 0x0

    .line 814408
    :goto_0
    return-object v0

    .line 814409
    :cond_0
    sget-object v0, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 814410
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814411
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->o(LX/15w;LX/0n3;)I

    move-result v2

    aput v2, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)[I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 814412
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 814413
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;->c(LX/15w;LX/0n3;)[I

    move-result-object v0

    .line 814414
    :goto_0
    return-object v0

    .line 814415
    :cond_0
    invoke-virtual {p2}, LX/0n3;->m()LX/0nj;

    move-result-object v0

    invoke-virtual {v0}, LX/0nj;->d()LX/4ri;

    move-result-object v4

    .line 814416
    invoke-virtual {v4}, LX/4rd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    move-object v2, v0

    move v0, v1

    .line 814417
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v5, :cond_1

    .line 814418
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->o(LX/15w;LX/0n3;)I

    move-result v5

    .line 814419
    array-length v3, v2

    if-lt v0, v3, :cond_2

    .line 814420
    invoke-virtual {v4, v2, v0}, LX/4rd;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    move v3, v1

    move-object v2, v0

    .line 814421
    :goto_2
    add-int/lit8 v0, v3, 0x1

    aput v5, v2, v3

    goto :goto_1

    .line 814422
    :cond_1
    invoke-virtual {v4, v2, v0}, LX/4rd;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    goto :goto_0

    :cond_2
    move v3, v0

    goto :goto_2
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814423
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$IntDeser;->a(LX/15w;LX/0n3;)[I

    move-result-object v0

    return-object v0
.end method
