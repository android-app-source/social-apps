.class public final Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$CharDeser;
.super Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers",
        "<[C>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814332
    const-class v0, [C

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)[C
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 814333
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 814334
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    .line 814335
    invoke-virtual {p1}, LX/15w;->p()[C

    move-result-object v1

    .line 814336
    invoke-virtual {p1}, LX/15w;->r()I

    move-result v2

    .line 814337
    invoke-virtual {p1}, LX/15w;->q()I

    move-result v3

    .line 814338
    new-array v0, v3, [C

    .line 814339
    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 814340
    :goto_0
    return-object v0

    .line 814341
    :cond_0
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 814342
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 814343
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_3

    .line 814344
    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v1, v2, :cond_1

    .line 814345
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814346
    :cond_1
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 814347
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 814348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Can not convert a JSON String of length "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " into a char element of char array"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814349
    :cond_2
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 814350
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    goto :goto_0

    .line 814351
    :cond_4
    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_8

    .line 814352
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 814353
    if-nez v0, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    .line 814354
    :cond_5
    instance-of v1, v0, [C

    if-eqz v1, :cond_6

    .line 814355
    check-cast v0, [C

    check-cast v0, [C

    goto :goto_0

    .line 814356
    :cond_6
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 814357
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    goto :goto_0

    .line 814358
    :cond_7
    instance-of v1, v0, [B

    if-eqz v1, :cond_8

    .line 814359
    sget-object v1, LX/0lm;->b:LX/0ln;

    move-object v1, v1

    .line 814360
    check-cast v0, [B

    check-cast v0, [B

    invoke-virtual {v1, v0, v4}, LX/0ln;->a([BZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    goto/16 :goto_0

    .line 814361
    :cond_8
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814362
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/PrimitiveArrayDeserializers$CharDeser;->a(LX/15w;LX/0n3;)[C

    move-result-object v0

    return-object v0
.end method
