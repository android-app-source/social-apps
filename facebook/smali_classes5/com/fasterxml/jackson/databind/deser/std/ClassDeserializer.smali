.class public Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer",
        "<",
        "Ljava/lang/Class",
        "<*>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 813464
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 813455
    const-class v0, Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public a(LX/15w;LX/0n3;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 813457
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813458
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    .line 813459
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 813460
    :try_start_0
    invoke-static {v0}, LX/1Xw;->a(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 813461
    :catch_0
    move-exception v0

    .line 813462
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-static {v0}, LX/1Xw;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0

    .line 813463
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v1, v0}, LX/0n3;->a(Ljava/lang/Class;LX/15z;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813456
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/ClassDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method
