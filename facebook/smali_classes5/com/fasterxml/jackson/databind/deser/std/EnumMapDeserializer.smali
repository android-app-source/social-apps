.class public Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "Ljava/util/EnumMap",
        "<**>;>;",
        "LX/1Xy;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1513c456622f2ab4L


# instance fields
.field public final _enumClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public _keyDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Enum",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final _mapType:LX/0lJ;

.field public _valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _valueTypeDeserializer:LX/4qw;


# direct methods
.method public constructor <init>(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "LX/4qw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 813666
    const-class v0, Ljava/util/EnumMap;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    .line 813667
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_mapType:LX/0lJ;

    .line 813668
    invoke-virtual {p1}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    .line 813669
    iget-object p1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p1

    .line 813670
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_enumClass:Ljava/lang/Class;

    .line 813671
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_keyDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813672
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813673
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813674
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "LX/4qw;",
            ")",
            "Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;"
        }
    .end annotation

    .prologue
    .line 813664
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_keyDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueTypeDeserializer:LX/4qw;

    if-ne p3, v0, :cond_0

    .line 813665
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_mapType:LX/0lJ;

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueTypeDeserializer:LX/4qw;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;-><init>(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a()Ljava/util/EnumMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 813650
    new-instance v0, Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_enumClass:Ljava/lang/Class;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 813652
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_keyDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813653
    if-nez v0, :cond_3

    .line 813654
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_mapType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    move-object v1, v0

    .line 813655
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813656
    if-nez v0, :cond_2

    .line 813657
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_mapType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 813658
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813659
    if-eqz v2, :cond_1

    .line 813660
    invoke-virtual {v2, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v2

    .line 813661
    :cond_1
    invoke-direct {p0, v1, v0, v2}, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;

    move-result-object v0

    return-object v0

    .line 813662
    :cond_2
    instance-of v2, v0, LX/1Xy;

    if-eqz v2, :cond_0

    .line 813663
    check-cast v0, LX/1Xy;

    invoke-interface {v0, p1, p2}, LX/1Xy;->a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public a(LX/15w;LX/0n3;)Ljava/util/EnumMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/util/EnumMap",
            "<**>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 813675
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_0

    .line 813676
    const-class v0, Ljava/util/EnumMap;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 813677
    :cond_0
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->a()Ljava/util/EnumMap;

    move-result-object v3

    .line 813678
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813679
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_valueTypeDeserializer:LX/4qw;

    .line 813680
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 813681
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_keyDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 813682
    if-nez v0, :cond_3

    .line 813683
    sget-object v0, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 813684
    :try_start_0
    invoke-virtual {p1}, LX/15w;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813685
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 813686
    :goto_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->_enumClass:Ljava/lang/Class;

    const-string v2, "value not one of declared Enum instance names"

    invoke-virtual {p2, v0, v1, v2}, LX/0n3;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    :catch_0
    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 813687
    :cond_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 813688
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 813689
    :cond_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v2

    .line 813690
    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v6, :cond_4

    move-object v2, v1

    .line 813691
    :goto_2
    invoke-virtual {v3, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 813692
    :cond_4
    if-nez v5, :cond_5

    .line 813693
    invoke-virtual {v4, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_2

    .line 813694
    :cond_5
    invoke-virtual {v4, p1, p2, v5}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_2

    .line 813695
    :cond_6
    return-object v3
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813651
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/EnumMapDeserializer;->a(LX/15w;LX/0n3;)Ljava/util/EnumMap;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813649
    invoke-virtual {p3, p1, p2}, LX/4qw;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCachable()Z
    .locals 1

    .prologue
    .line 813648
    const/4 v0, 0x1

    return v0
.end method
