.class public Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase",
        "<[",
        "Ljava/lang/Object;",
        ">;",
        "LX/1Xy;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _arrayType:LX/4ra;

.field public final _elementClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public _elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _elementTypeDeserializer:LX/4qw;

.field public final _untyped:Z


# direct methods
.method public constructor <init>(LX/4ra;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ra;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 814243
    const-class v0, [Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;-><init>(Ljava/lang/Class;)V

    .line 814244
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_arrayType:LX/4ra;

    .line 814245
    invoke-virtual {p1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    .line 814246
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 814247
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementClass:Ljava/lang/Class;

    .line 814248
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementClass:Ljava/lang/Class;

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_untyped:Z

    .line 814249
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814250
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    .line 814251
    return-void

    .line 814252
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;"
        }
    .end annotation

    .prologue
    .line 814241
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    if-ne p1, v0, :cond_0

    .line 814242
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_arrayType:LX/4ra;

    invoke-direct {v0, v1, p2, p1}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;-><init>(LX/4ra;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/4qw;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private static a(LX/15w;LX/0n3;LX/4qw;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814240
    invoke-virtual {p2, p0, p1}, LX/4qw;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method

.method private static c(LX/15w;LX/0n3;)[Ljava/lang/Byte;
    .locals 5

    .prologue
    .line 814234
    invoke-virtual {p1}, LX/0n3;->h()LX/0ln;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0ln;)[B

    move-result-object v1

    .line 814235
    array-length v0, v1

    new-array v2, v0, [Ljava/lang/Byte;

    .line 814236
    const/4 v0, 0x0

    array-length v3, v1

    :goto_0
    if-ge v0, v3, :cond_0

    .line 814237
    aget-byte v4, v1, v0

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v0

    .line 814238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 814239
    :cond_0
    return-object v2
.end method

.method private final d(LX/15w;LX/0n3;)[Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 814215
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_0

    sget-object v1, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814216
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 814217
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 814218
    :goto_0
    return-object v0

    .line 814219
    :cond_0
    sget-object v1, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 814220
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementClass:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    if-ne v0, v1, :cond_1

    .line 814221
    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->c(LX/15w;LX/0n3;)[Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 814222
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_arrayType:LX/4ra;

    .line 814223
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 814224
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 814225
    :cond_2
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    .line 814226
    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_3

    move-object v1, v0

    .line 814227
    :goto_1
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_untyped:Z

    if-eqz v0, :cond_5

    .line 814228
    new-array v0, v3, [Ljava/lang/Object;

    .line 814229
    :goto_2
    const/4 v2, 0x0

    aput-object v1, v0, v2

    goto :goto_0

    .line 814230
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    if-nez v0, :cond_4

    .line 814231
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 814232
    :cond_4
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    invoke-virtual {v0, p1, p2, v1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 814233
    :cond_5
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementClass:Ljava/lang/Class;

    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814214
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    return-object v0
.end method

.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814204
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814205
    invoke-static {p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 814206
    if-nez v0, :cond_2

    .line 814207
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_arrayType:LX/4ra;

    invoke-virtual {v0}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 814208
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    .line 814209
    if-eqz v1, :cond_1

    .line 814210
    invoke-virtual {v1, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v1

    .line 814211
    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->a(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;

    move-result-object v0

    return-object v0

    .line 814212
    :cond_2
    instance-of v1, v0, LX/1Xy;

    if-eqz v1, :cond_0

    .line 814213
    check-cast v0, LX/1Xy;

    invoke-interface {v0, p1, p2}, LX/1Xy;->a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LX/15w;LX/0n3;)[Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 814185
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 814186
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->d(LX/15w;LX/0n3;)[Ljava/lang/Object;

    move-result-object v0

    .line 814187
    :goto_0
    return-object v0

    .line 814188
    :cond_0
    invoke-virtual {p2}, LX/0n3;->l()LX/4rv;

    move-result-object v5

    .line 814189
    invoke-virtual {v5}, LX/4rv;->a()[Ljava/lang/Object;

    move-result-object v0

    .line 814190
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementTypeDeserializer:LX/4qw;

    move-object v2, v0

    move v0, v1

    .line 814191
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 814192
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v3, v4, :cond_1

    .line 814193
    const/4 v3, 0x0

    .line 814194
    :goto_2
    array-length v4, v2

    if-lt v0, v4, :cond_5

    .line 814195
    invoke-virtual {v5, v2}, LX/4rv;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move v4, v1

    .line 814196
    :goto_3
    add-int/lit8 v0, v4, 0x1

    aput-object v3, v2, v4

    goto :goto_1

    .line 814197
    :cond_1
    if-nez v6, :cond_2

    .line 814198
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v3, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_2

    .line 814199
    :cond_2
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v3, p1, p2, v6}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_2

    .line 814200
    :cond_3
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_untyped:Z

    if-eqz v1, :cond_4

    .line 814201
    invoke-virtual {v5, v2, v0}, LX/4rv;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 814202
    :goto_4
    invoke-virtual {p2, v5}, LX/0n3;->a(LX/4rv;)V

    goto :goto_0

    .line 814203
    :cond_4
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->_elementClass:Ljava/lang/Class;

    invoke-virtual {v5, v2, v0, v1}, LX/4rv;->a([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_4

    :cond_5
    move v4, v0

    goto :goto_3
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814183
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->a(LX/15w;LX/0n3;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814184
    invoke-static {p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/ObjectArrayDeserializer;->a(LX/15w;LX/0n3;LX/4qw;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
