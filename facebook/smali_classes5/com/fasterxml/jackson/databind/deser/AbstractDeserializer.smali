.class public Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x29c6e883091839e2L


# instance fields
.field public final _acceptBoolean:Z

.field public final _acceptDouble:Z

.field public final _acceptInt:Z

.field public final _acceptString:Z

.field public final _backRefProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field

.field public final _baseType:LX/0lJ;

.field public final _objectIdReader:LX/4qD;


# direct methods
.method public constructor <init>(LX/329;LX/0lS;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/329;",
            "LX/0lS;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 812196
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 812197
    iget-object v0, p2, LX/0lS;->a:LX/0lJ;

    move-object v0, v0

    .line 812198
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_baseType:LX/0lJ;

    .line 812199
    iget-object v0, p1, LX/329;->h:LX/4qD;

    move-object v0, v0

    .line 812200
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_objectIdReader:LX/4qD;

    .line 812201
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_backRefProperties:Ljava/util/Map;

    .line 812202
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_baseType:LX/0lJ;

    .line 812203
    iget-object v3, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v3, v3

    .line 812204
    const-class v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptString:Z

    .line 812205
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v3, v0, :cond_0

    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptBoolean:Z

    .line 812206
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq v3, v0, :cond_1

    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptInt:Z

    .line 812207
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-eq v3, v0, :cond_2

    const-class v0, Ljava/lang/Double;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    iput-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptDouble:Z

    .line 812208
    return-void

    :cond_4
    move v0, v1

    .line 812209
    goto :goto_0

    :cond_5
    move v0, v1

    .line 812210
    goto :goto_1
.end method

.method private a(LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812184
    sget-object v0, LX/2te;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 812185
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 812186
    :pswitch_0
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptString:Z

    if-eqz v0, :cond_0

    .line 812187
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 812188
    :pswitch_1
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptInt:Z

    if-eqz v0, :cond_0

    .line 812189
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 812190
    :pswitch_2
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptDouble:Z

    if-eqz v0, :cond_0

    .line 812191
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 812192
    :pswitch_3
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptBoolean:Z

    if-eqz v0, :cond_0

    .line 812193
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 812194
    :pswitch_4
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_acceptBoolean:Z

    if-eqz v0, :cond_0

    .line 812195
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 812178
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_objectIdReader:LX/4qD;

    iget-object v0, v0, LX/4qD;->deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812179
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_objectIdReader:LX/4qD;

    iget-object v1, v1, LX/4qD;->generator:LX/4pS;

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Object;LX/4pS;)LX/4qM;

    move-result-object v1

    .line 812180
    iget-object v1, v1, LX/4qM;->b:Ljava/lang/Object;

    .line 812181
    if-nez v1, :cond_0

    .line 812182
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not resolve Object Id ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] -- unresolved forward-reference?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 812183
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 812177
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_backRefProperties:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_backRefProperties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    goto :goto_0
.end method

.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812174
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_baseType:LX/0lJ;

    .line 812175
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 812176
    const-string v1, "abstract types either need to be mapped to concrete types, have custom deserializer, or be instantiated with additional type information"

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 812166
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_1

    .line 812167
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 812168
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/15z;->isScalarValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 812169
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812170
    :cond_0
    :goto_0
    return-object v0

    .line 812171
    :cond_1
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->a(LX/15w;)Ljava/lang/Object;

    move-result-object v0

    .line 812172
    if-nez v0, :cond_0

    .line 812173
    invoke-virtual {p3, p1, p2}, LX/4qw;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getObjectIdReader()LX/4qD;
    .locals 1

    .prologue
    .line 812165
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->_objectIdReader:LX/4qD;

    return-object v0
.end method

.method public final isCachable()Z
    .locals 1

    .prologue
    .line 812164
    const/4 v0, 0x1

    return v0
.end method
