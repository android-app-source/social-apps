.class public Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _buildMethod:LX/2At;

.field public final _delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

.field public final _orderedProperties:[LX/32s;


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;LX/2At;)V
    .locals 0

    .prologue
    .line 812746
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;)V

    .line 812747
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    .line 812748
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    .line 812749
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_buildMethod:LX/2At;

    .line 812750
    return-void
.end method

.method private b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;
    .locals 4

    .prologue
    .line 812745
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_buildMethod:LX/2At;

    invoke-direct {v0, v1, v2, v3}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;LX/2At;)V

    return-object v0
.end method

.method private b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;"
        }
    .end annotation

    .prologue
    .line 812744
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_buildMethod:LX/2At;

    invoke-direct {v0, v1, v2, v3}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;LX/2At;)V

    return-object v0
.end method

.method private b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812737
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_buildMethod:LX/2At;

    .line 812738
    iget-object v1, v0, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v1

    .line 812739
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 812740
    :goto_0
    return-object v0

    .line 812741
    :catch_0
    move-exception v0

    .line 812742
    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    .line 812743
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 812708
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    if-eqz v0, :cond_0

    .line 812709
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->y(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812710
    :goto_0
    return-object v0

    .line 812711
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v2

    .line 812712
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_1

    .line 812713
    invoke-virtual {p0, p2, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 812714
    :cond_1
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v0, :cond_2

    .line 812715
    iget-object v0, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v0, v0

    .line 812716
    :goto_1
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    .line 812717
    const/4 v1, 0x0

    .line 812718
    array-length v4, v3

    .line 812719
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    move-object v0, v2

    .line 812720
    goto :goto_0

    .line 812721
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 812722
    :cond_3
    if-eq v1, v4, :cond_6

    .line 812723
    aget-object v5, v3, v1

    .line 812724
    add-int/lit8 v1, v1, 0x1

    .line 812725
    if-eqz v5, :cond_5

    .line 812726
    if-eqz v0, :cond_4

    invoke-virtual {v5, v0}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 812727
    :cond_4
    :try_start_0
    invoke-virtual {v5, p1, p2, v2}, LX/32s;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 812728
    :catch_0
    move-exception v6

    .line 812729
    iget-object v7, v5, LX/32s;->_propName:Ljava/lang/String;

    move-object v5, v7

    .line 812730
    invoke-static {v6, v2, v5, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812731
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 812732
    :cond_6
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_7

    .line 812733
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812734
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_8

    .line 812735
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    :cond_8
    move-object v0, v2

    .line 812736
    goto :goto_0
.end method

.method private y(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812700
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    .line 812701
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 812702
    :goto_0
    return-object v0

    .line 812703
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_1

    .line 812704
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 812705
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 812706
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not instantiate abstract type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812707
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No suitable constructor found for type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": can not instantiate from JSON object (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method private z(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812696
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not deserialize a POJO (of type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812697
    iget-object p0, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, p0

    .line 812698
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") from non-Array representation (token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): type/property designed to be serialized as JSON Array"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final synthetic a()Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 812751
    move-object v0, p0

    .line 812752
    return-object v0
.end method

.method public final synthetic a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 812699
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 812695
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 812694
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->z(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 812655
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 812656
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    invoke-virtual {v4, p1, p2, v0}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v5

    .line 812657
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    .line 812658
    array-length v7, v6

    .line 812659
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    .line 812660
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v8, :cond_5

    .line 812661
    if-ge v2, v7, :cond_1

    aget-object v3, v6, v2

    .line 812662
    :goto_1
    if-nez v3, :cond_2

    .line 812663
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 812664
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move-object v3, v1

    .line 812665
    goto :goto_1

    .line 812666
    :cond_2
    if-eqz v0, :cond_3

    .line 812667
    :try_start_0
    invoke-virtual {v3, p1, p2, v0}, LX/32s;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_2

    .line 812668
    :catch_0
    move-exception v8

    .line 812669
    iget-object v9, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v9

    .line 812670
    invoke-static {v8, v0, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812671
    :cond_3
    iget-object v8, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v8, v8

    .line 812672
    invoke-virtual {v4, v8}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v9

    .line 812673
    if-eqz v9, :cond_4

    .line 812674
    invoke-virtual {v9, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v3

    .line 812675
    invoke-virtual {v9}, LX/32s;->c()I

    move-result v9

    invoke-virtual {v5, v9, v3}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 812676
    :try_start_1
    invoke-virtual {v4, p2, v5}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 812677
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v8, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812678
    iget-object v9, v8, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v8, v9

    .line 812679
    if-eq v3, v8, :cond_0

    .line 812680
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not support implicit polymorphic deserialization for POJOs-as-Arrays style: nominal type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812681
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 812682
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", actual type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812683
    :catch_1
    move-exception v3

    .line 812684
    iget-object v9, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812685
    iget-object v10, v9, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v9, v10

    .line 812686
    invoke-static {v3, v9, v8, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812687
    :cond_4
    invoke-virtual {v5, v8}, LX/4qL;->a(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 812688
    invoke-virtual {v3, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v5, v3, v8}, LX/4qL;->a(LX/32s;Ljava/lang/Object;)V

    goto :goto_2

    .line 812689
    :cond_5
    if-nez v0, :cond_6

    .line 812690
    :try_start_2
    invoke-virtual {v4, p2, v5}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    :cond_6
    move-object v1, v0

    .line 812691
    :goto_3
    return-object v1

    .line 812692
    :catch_2
    move-exception v0

    .line 812693
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    goto :goto_3
.end method

.method public deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 812629
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    .line 812630
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->z(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 812631
    :goto_0
    return-object v0

    .line 812632
    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    if-nez v0, :cond_1

    .line 812633
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 812634
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v1

    .line 812635
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    .line 812636
    const/4 v0, 0x0

    .line 812637
    array-length v3, v2

    .line 812638
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 812639
    invoke-direct {p0, p2, v1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 812640
    :cond_2
    if-eq v0, v3, :cond_4

    .line 812641
    aget-object v4, v2, v0

    .line 812642
    if-eqz v4, :cond_3

    .line 812643
    :try_start_0
    invoke-virtual {v4, p1, p2, v1}, LX/32s;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 812644
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 812645
    goto :goto_1

    .line 812646
    :catch_0
    move-exception v5

    .line 812647
    iget-object v6, v4, LX/32s;->_propName:Ljava/lang/String;

    move-object v4, v6

    .line 812648
    invoke-static {v5, v1, v4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812649
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 812650
    :cond_4
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_5

    .line 812651
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812652
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v2, :cond_6

    .line 812653
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 812654
    :cond_6
    invoke-direct {p0, p2, v1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 812606
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_0

    .line 812607
    invoke-virtual {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 812608
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_orderedProperties:[LX/32s;

    .line 812609
    const/4 v0, 0x0

    .line 812610
    array-length v2, v1

    .line 812611
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-ne v3, v4, :cond_1

    .line 812612
    invoke-direct {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 812613
    :goto_1
    return-object v0

    .line 812614
    :cond_1
    if-eq v0, v2, :cond_3

    .line 812615
    aget-object v3, v1, v0

    .line 812616
    if-eqz v3, :cond_2

    .line 812617
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, LX/32s;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 812618
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 812619
    goto :goto_0

    .line 812620
    :catch_0
    move-exception v4

    .line 812621
    iget-object v5, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v5

    .line 812622
    invoke-static {v4, p3, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812623
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 812624
    :cond_3
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_4

    .line 812625
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812626
    :cond_4
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_5

    .line 812627
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 812628
    :cond_5
    invoke-direct {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->b(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public final unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 812605
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayBuilderDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    return-object v0
.end method
