.class public Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

.field public final _orderedProperties:[LX/32s;


# direct methods
.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;)V
    .locals 0

    .prologue
    .line 812886
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;)V

    .line 812887
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    .line 812888
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    .line 812889
    return-void
.end method

.method private b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;
    .locals 3

    .prologue
    .line 812885
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    invoke-direct {v0, v1, v2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;)V

    return-object v0
.end method

.method private b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;"
        }
    .end annotation

    .prologue
    .line 812884
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    invoke-direct {v0, v1, v2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;)V

    return-object v0
.end method

.method private x(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 812855
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    if-eqz v0, :cond_0

    .line 812856
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->y(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812857
    :goto_0
    return-object v0

    .line 812858
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v2

    .line 812859
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_1

    .line 812860
    invoke-virtual {p0, p2, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 812861
    :cond_1
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v0, :cond_2

    .line 812862
    iget-object v0, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v0, v0

    .line 812863
    :goto_1
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    .line 812864
    const/4 v1, 0x0

    .line 812865
    array-length v4, v3

    .line 812866
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    move-object v0, v2

    .line 812867
    goto :goto_0

    .line 812868
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 812869
    :cond_3
    if-eq v1, v4, :cond_6

    .line 812870
    aget-object v5, v3, v1

    .line 812871
    add-int/lit8 v1, v1, 0x1

    .line 812872
    if-eqz v5, :cond_5

    .line 812873
    if-eqz v0, :cond_4

    invoke-virtual {v5, v0}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 812874
    :cond_4
    :try_start_0
    invoke-virtual {v5, p1, p2, v2}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 812875
    :catch_0
    move-exception v6

    .line 812876
    iget-object v7, v5, LX/32s;->_propName:Ljava/lang/String;

    move-object v5, v7

    .line 812877
    invoke-static {v6, v2, v5, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812878
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 812879
    :cond_6
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_7

    .line 812880
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812881
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_8

    .line 812882
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    :cond_8
    move-object v0, v2

    .line 812883
    goto :goto_0
.end method

.method private y(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812753
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    .line 812754
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 812755
    :goto_0
    return-object v0

    .line 812756
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_1

    .line 812757
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 812758
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 812759
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not instantiate abstract type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812760
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No suitable constructor found for type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": can not instantiate from JSON object (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method private z(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812852
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not deserialize a POJO (of type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812853
    iget-object p0, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, p0

    .line 812854
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") from non-Array representation (token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): type/property designed to be serialized as JSON Array"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 0

    .prologue
    .line 812851
    return-object p0
.end method

.method public final synthetic a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 812850
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 812849
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 812848
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->z(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 812809
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 812810
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    invoke-virtual {v4, p1, p2, v0}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v5

    .line 812811
    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    .line 812812
    array-length v7, v6

    .line 812813
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    .line 812814
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v8, :cond_5

    .line 812815
    if-ge v2, v7, :cond_1

    aget-object v3, v6, v2

    .line 812816
    :goto_1
    if-nez v3, :cond_2

    .line 812817
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 812818
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move-object v3, v1

    .line 812819
    goto :goto_1

    .line 812820
    :cond_2
    if-eqz v0, :cond_3

    .line 812821
    :try_start_0
    invoke-virtual {v3, p1, p2, v0}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 812822
    :catch_0
    move-exception v8

    .line 812823
    iget-object v9, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v9

    .line 812824
    invoke-static {v8, v0, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812825
    :cond_3
    iget-object v8, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v8, v8

    .line 812826
    invoke-virtual {v4, v8}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v9

    .line 812827
    if-eqz v9, :cond_4

    .line 812828
    invoke-virtual {v9, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v3

    .line 812829
    invoke-virtual {v9}, LX/32s;->c()I

    move-result v9

    invoke-virtual {v5, v9, v3}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 812830
    :try_start_1
    invoke-virtual {v4, p2, v5}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 812831
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v8, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812832
    iget-object v9, v8, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v8, v9

    .line 812833
    if-eq v3, v8, :cond_0

    .line 812834
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not support implicit polymorphic deserialization for POJOs-as-Arrays style: nominal type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812835
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 812836
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", actual type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812837
    :catch_1
    move-exception v3

    .line 812838
    iget-object v9, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 812839
    iget-object v10, v9, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v9, v10

    .line 812840
    invoke-static {v3, v9, v8, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812841
    :cond_4
    invoke-virtual {v5, v8}, LX/4qL;->a(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 812842
    invoke-virtual {v3, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v5, v3, v8}, LX/4qL;->a(LX/32s;Ljava/lang/Object;)V

    goto :goto_2

    .line 812843
    :cond_5
    if-nez v0, :cond_6

    .line 812844
    :try_start_2
    invoke-virtual {v4, p2, v5}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    :cond_6
    move-object v1, v0

    .line 812845
    :goto_3
    return-object v1

    .line 812846
    :catch_2
    move-exception v0

    .line 812847
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    goto :goto_3
.end method

.method public deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 812783
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    .line 812784
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->z(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812785
    :goto_0
    return-object v0

    .line 812786
    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    if-nez v0, :cond_1

    .line 812787
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 812788
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v1

    .line 812789
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    .line 812790
    const/4 v0, 0x0

    .line 812791
    array-length v3, v2

    .line 812792
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    move-object v0, v1

    .line 812793
    goto :goto_0

    .line 812794
    :cond_2
    if-eq v0, v3, :cond_4

    .line 812795
    aget-object v4, v2, v0

    .line 812796
    if-eqz v4, :cond_3

    .line 812797
    :try_start_0
    invoke-virtual {v4, p1, p2, v1}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 812798
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 812799
    goto :goto_1

    .line 812800
    :catch_0
    move-exception v5

    .line 812801
    iget-object v6, v4, LX/32s;->_propName:Ljava/lang/String;

    move-object v4, v6

    .line 812802
    invoke-static {v5, v1, v4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 812803
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 812804
    :cond_4
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_5

    .line 812805
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812806
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v2, :cond_6

    .line 812807
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    :cond_6
    move-object v0, v1

    .line 812808
    goto :goto_0
.end method

.method public final deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 812762
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_0

    .line 812763
    invoke-virtual {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 812764
    :cond_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_orderedProperties:[LX/32s;

    .line 812765
    const/4 v0, 0x0

    .line 812766
    array-length v2, v1

    .line 812767
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 812768
    :cond_1
    return-object p3

    .line 812769
    :cond_2
    if-eq v0, v2, :cond_4

    .line 812770
    aget-object v3, v1, v0

    .line 812771
    if-eqz v3, :cond_3

    .line 812772
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 812773
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 812774
    goto :goto_0

    .line 812775
    :catch_0
    move-exception v4

    .line 812776
    iget-object v5, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v5

    .line 812777
    invoke-static {v4, p3, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_1

    .line 812778
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 812779
    :cond_4
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_5

    .line 812780
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected JSON values; expected at most "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " properties (in JSON Array)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812781
    :cond_5
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_1

    .line 812782
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2
.end method

.method public final unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 812761
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;->_delegate:Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-virtual {v0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    return-object v0
.end method
