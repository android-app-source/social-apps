.class public final Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "Ljavax/xml/datatype/XMLGregorianCalendar;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 814780
    new-instance v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;->a:Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814781
    const-class v0, Ljavax/xml/datatype/XMLGregorianCalendar;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 814782
    return-void
.end method

.method private static a(Ljavax/xml/datatype/XMLGregorianCalendar;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 814783
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;

    invoke-virtual {p0}, Ljavax/xml/datatype/XMLGregorianCalendar;->toGregorianCalendar()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;->a(Ljava/util/Calendar;LX/0nX;LX/0my;)V

    .line 814784
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 814785
    check-cast p1, Ljavax/xml/datatype/XMLGregorianCalendar;

    invoke-static {p1, p2, p3}, Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;->a(Ljavax/xml/datatype/XMLGregorianCalendar;LX/0nX;LX/0my;)V

    return-void
.end method
