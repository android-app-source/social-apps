.class public Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers;
.super LX/0ni;
.source ""


# static fields
.field public static final a:Ljavax/xml/datatype/DatatypeFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 814765
    :try_start_0
    invoke-static {}, Ljavax/xml/datatype/DatatypeFactory;->newInstance()Ljavax/xml/datatype/DatatypeFactory;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers;->a:Ljavax/xml/datatype/DatatypeFactory;
    :try_end_0
    .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 814766
    return-void

    .line 814767
    :catch_0
    move-exception v0

    .line 814768
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 814769
    invoke-direct {p0}, LX/0ni;-><init>()V

    .line 814770
    return-void
.end method


# virtual methods
.method public final a(LX/0lJ;LX/0mu;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/0mu;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814771
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 814772
    const-class v1, Ljavax/xml/namespace/QName;

    if-ne v0, v1, :cond_0

    .line 814773
    sget-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$QNameDeserializer;->a:Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$QNameDeserializer;

    .line 814774
    :goto_0
    return-object v0

    .line 814775
    :cond_0
    const-class v1, Ljavax/xml/datatype/XMLGregorianCalendar;

    if-ne v0, v1, :cond_1

    .line 814776
    sget-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$GregorianCalendarDeserializer;->a:Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$GregorianCalendarDeserializer;

    goto :goto_0

    .line 814777
    :cond_1
    const-class v1, Ljavax/xml/datatype/Duration;

    if-ne v0, v1, :cond_2

    .line 814778
    sget-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$DurationDeserializer;->a:Lcom/fasterxml/jackson/databind/ext/CoreXMLDeserializers$DurationDeserializer;

    goto :goto_0

    .line 814779
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
