.class public Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers;
.super LX/0nl;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 814786
    invoke-direct {p0}, LX/0nl;-><init>()V

    .line 814787
    return-void
.end method


# virtual methods
.method public final a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814788
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 814789
    const-class v1, Ljavax/xml/datatype/Duration;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Ljavax/xml/namespace/QName;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 814790
    :cond_0
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;

    .line 814791
    :goto_0
    return-object v0

    .line 814792
    :cond_1
    const-class v1, Ljavax/xml/datatype/XMLGregorianCalendar;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 814793
    sget-object v0, Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;->a:Lcom/fasterxml/jackson/databind/ext/CoreXMLSerializers$XMLGregorianCalendarSerializer;

    goto :goto_0

    .line 814794
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
