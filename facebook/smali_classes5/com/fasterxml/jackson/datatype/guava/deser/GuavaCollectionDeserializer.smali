.class public abstract Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<TT;>;",
        "LX/1Xy;"
    }
.end annotation


# instance fields
.field public final _containerType:LX/267;

.field public final _typeDeserializerForValue:LX/4qw;

.field public final _valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/267;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 819362
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 819363
    iput-object p1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_containerType:LX/267;

    .line 819364
    iput-object p2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_typeDeserializerForValue:LX/4qw;

    .line 819365
    iput-object p3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 819366
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 819354
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 819355
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_typeDeserializerForValue:LX/4qw;

    .line 819356
    if-nez v1, :cond_0

    .line 819357
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_containerType:LX/267;

    invoke-virtual {v1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 819358
    :cond_0
    if-eqz v0, :cond_1

    .line 819359
    invoke-virtual {v0, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v0

    .line 819360
    :cond_1
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne v1, v2, :cond_2

    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_typeDeserializerForValue:LX/4qw;

    if-ne v0, v2, :cond_2

    .line 819361
    :goto_0
    return-object p0

    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->a(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;

    move-result-object p0

    goto :goto_0
.end method

.method public abstract a(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation
.end method

.method public deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 819349
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    .line 819350
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_containerType:LX/267;

    .line 819351
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 819352
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 819353
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819348
    invoke-virtual {p3, p1, p2}, LX/4qw;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
