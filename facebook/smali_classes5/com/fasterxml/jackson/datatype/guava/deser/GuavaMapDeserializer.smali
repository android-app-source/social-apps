.class public abstract Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<TT;>;",
        "LX/1Xy;"
    }
.end annotation


# instance fields
.field public final a:LX/1Xn;

.field public b:LX/1Xt;

.field public c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation
.end field

.field public final d:LX/4qw;


# direct methods
.method public constructor <init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xn;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 582497
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 582498
    iput-object p1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a:LX/1Xn;

    .line 582499
    iput-object p2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->b:LX/1Xt;

    .line 582500
    iput-object p3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->d:LX/4qw;

    .line 582501
    iput-object p4, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 582502
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 582503
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->b:LX/1Xt;

    .line 582504
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 582505
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->d:LX/4qw;

    .line 582506
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 582507
    :goto_0
    return-object p0

    .line 582508
    :cond_0
    if-nez v2, :cond_1

    .line 582509
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a:LX/1Xn;

    invoke-virtual {v2}, LX/0lJ;->q()LX/0lJ;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, LX/0n3;->b(LX/0lJ;LX/2Ay;)LX/1Xt;

    move-result-object v2

    .line 582510
    :cond_1
    if-nez v1, :cond_2

    .line 582511
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a:LX/1Xn;

    invoke-virtual {v1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 582512
    :cond_2
    if-eqz v0, :cond_3

    .line 582513
    invoke-virtual {v0, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v0

    .line 582514
    :cond_3
    invoke-virtual {p0, v2, v0, v1}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a(LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;

    move-result-object p0

    goto :goto_0
.end method

.method public abstract a(LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation
.end method

.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 582515
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 582516
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 582517
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 582518
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_1

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v1, :cond_1

    .line 582519
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a:LX/1Xn;

    .line 582520
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 582521
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 582522
    :cond_0
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_1

    .line 582523
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a:LX/1Xn;

    .line 582524
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 582525
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 582526
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 582527
    invoke-virtual {p3, p1, p2}, LX/4qw;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
