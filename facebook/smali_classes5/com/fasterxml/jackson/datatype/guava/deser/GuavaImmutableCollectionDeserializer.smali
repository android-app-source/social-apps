.class public abstract Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableCollectionDeserializer;
.super Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0Py",
        "<",
        "Ljava/lang/Object;",
        ">;>",
        "Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/267;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 819367
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 819368
    return-void
.end method

.method private c(LX/15w;LX/0n3;)LX/0Py;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 819369
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 819370
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_typeDeserializerForValue:LX/4qw;

    .line 819371
    invoke-virtual {p0}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableCollectionDeserializer;->a()LX/0P7;

    move-result-object v3

    .line 819372
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v4, :cond_2

    .line 819373
    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v4, :cond_0

    .line 819374
    const/4 v0, 0x0

    .line 819375
    :goto_1
    invoke-virtual {v3, v0}, LX/0P7;->b(Ljava/lang/Object;)LX/0P7;

    goto :goto_0

    .line 819376
    :cond_0
    if-nez v2, :cond_1

    .line 819377
    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 819378
    :cond_1
    invoke-virtual {v1, p1, p2, v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 819379
    :cond_2
    invoke-virtual {v3}, LX/0P7;->a()LX/0Py;

    move-result-object v0

    .line 819380
    return-object v0
.end method


# virtual methods
.method public abstract a()LX/0P7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P7",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public final synthetic a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819381
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableCollectionDeserializer;->c(LX/15w;LX/0n3;)LX/0Py;

    move-result-object v0

    return-object v0
.end method
