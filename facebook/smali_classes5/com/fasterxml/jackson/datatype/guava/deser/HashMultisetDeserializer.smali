.class public Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;
.super Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMultisetDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMultisetDeserializer",
        "<",
        "LX/1Ly",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/267;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 819403
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 819404
    return-void
.end method

.method private b(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;"
        }
    .end annotation

    .prologue
    .line 819405
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;->_containerType:LX/267;

    invoke-direct {v0, v1, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()LX/1M1;
    .locals 1

    .prologue
    .line 819406
    invoke-static {}, LX/1Ly;->g()LX/1Ly;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/GuavaCollectionDeserializer;
    .locals 1

    .prologue
    .line 819407
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;->b(LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;

    move-result-object v0

    return-object v0
.end method
