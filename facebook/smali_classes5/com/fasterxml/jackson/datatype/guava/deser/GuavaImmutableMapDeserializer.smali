.class public abstract Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableMapDeserializer;
.super Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0P1",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;>",
        "Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xn;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 582495
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;-><init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 582496
    return-void
.end method

.method private b(LX/15w;LX/0n3;)LX/0P1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 582477
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->b:LX/1Xt;

    .line 582478
    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 582479
    iget-object v4, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaMapDeserializer;->d:LX/4qw;

    .line 582480
    invoke-virtual {p0}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableMapDeserializer;->a()LX/0P2;

    move-result-object v5

    .line 582481
    :goto_0
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_3

    .line 582482
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 582483
    if-nez v2, :cond_0

    .line 582484
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    .line 582485
    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v6, :cond_1

    .line 582486
    const/4 v1, 0x0

    .line 582487
    :goto_2
    invoke-virtual {v5, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 582488
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 582489
    :cond_0
    invoke-virtual {v2, v0, p2}, LX/1Xt;->a(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 582490
    :cond_1
    if-nez v4, :cond_2

    .line 582491
    invoke-virtual {v3, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_2

    .line 582492
    :cond_2
    invoke-virtual {v3, p1, p2, v4}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_2

    .line 582493
    :cond_3
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 582494
    return-object v0
.end method


# virtual methods
.method public abstract a()LX/0P2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P2",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public final synthetic a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 582476
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaImmutableMapDeserializer;->b(LX/15w;LX/0n3;)LX/0P1;

    move-result-object v0

    return-object v0
.end method
