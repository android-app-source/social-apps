.class public Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""

# interfaces
.implements LX/1Xy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "LX/0Xu",
        "<**>;>;",
        "LX/1Xy;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Xo;

.field private final c:LX/1Xt;

.field private final d:LX/4qw;

.field private final e:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/reflect/Method;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 819446
    const-string v0, "copyOf"

    const-string v1, "create"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xo;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 819447
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 819448
    invoke-static {v0}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;-><init>(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;Ljava/lang/reflect/Method;)V

    .line 819449
    return-void
.end method

.method private constructor <init>(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;Ljava/lang/reflect/Method;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xo;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "Ljava/lang/reflect/Method;",
            ")V"
        }
    .end annotation

    .prologue
    .line 819450
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 819451
    iput-object p1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    .line 819452
    iput-object p2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->c:LX/1Xt;

    .line 819453
    iput-object p3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->d:LX/4qw;

    .line 819454
    iput-object p4, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 819455
    iput-object p5, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->f:Ljava/lang/reflect/Method;

    .line 819456
    return-void
.end method

.method private a(LX/15w;LX/0n3;)LX/0Xu;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "LX/0Xu",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 819457
    new-instance v0, LX/4zD;

    invoke-direct {v0}, LX/4zD;-><init>()V

    move-object v1, v0

    .line 819458
    :cond_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 819459
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->c:LX/1Xt;

    if-eqz v0, :cond_1

    .line 819460
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->c:LX/1Xt;

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LX/1Xt;->a(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 819461
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 819462
    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    invoke-static {p1, v2}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(LX/15w;LX/15z;)V

    .line 819463
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_0

    .line 819464
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->d:LX/4qw;

    if-eqz v2, :cond_2

    .line 819465
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->d:LX/4qw;

    invoke-virtual {v2, p1, p2, v3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0Xt;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 819466
    :cond_1
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 819467
    :cond_2
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v2, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0Xt;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 819468
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->f:Ljava/lang/reflect/Method;

    if-nez v0, :cond_4

    move-object v0, v1

    .line 819469
    :goto_2
    return-object v0

    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->f:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xu;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 819470
    :catch_0
    move-exception v0

    .line 819471
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not map to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 819472
    :catch_1
    move-exception v0

    .line 819473
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not map to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 819474
    :catch_2
    move-exception v0

    .line 819475
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not map to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 819476
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 819477
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 819478
    :cond_0
    return-object p0
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 819479
    const-class v0, LX/4zD;

    if-eq p0, v0, :cond_0

    const-class v0, LX/0Xv;

    if-eq p0, v0, :cond_0

    const-class v0, LX/0Xu;

    if-ne p0, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 819480
    :goto_0
    return-object v0

    .line 819481
    :cond_1
    sget-object v0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 819482
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Xu;

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 819483
    if-eqz v0, :cond_2

    goto :goto_0

    .line 819484
    :cond_3
    sget-object v0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 819485
    const/4 v3, 0x1

    :try_start_1
    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Xu;

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 819486
    if-eqz v0, :cond_4

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 819487
    goto :goto_0

    .line 819488
    :catch_0
    goto :goto_2

    .line 819489
    :catch_1
    goto :goto_1
.end method

.method private static a(LX/15w;LX/15z;)V
    .locals 3

    .prologue
    .line 819490
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 819491
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expecting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 819492
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 819493
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->c:LX/1Xt;

    .line 819494
    if-nez v2, :cond_0

    .line 819495
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    invoke-virtual {v0}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->b(LX/0lJ;LX/2Ay;)LX/1Xt;

    move-result-object v2

    .line 819496
    :cond_0
    iget-object v4, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 819497
    if-nez v4, :cond_1

    .line 819498
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    invoke-virtual {v0}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v4

    .line 819499
    :cond_1
    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->d:LX/4qw;

    .line 819500
    if-eqz v3, :cond_2

    if-eqz p2, :cond_2

    .line 819501
    invoke-virtual {v3, p2}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object v3

    .line 819502
    :cond_2
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;

    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->b:LX/1Xo;

    iget-object v5, p0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->f:Ljava/lang/reflect/Method;

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;-><init>(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;Ljava/lang/reflect/Method;)V

    return-object v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819503
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;->a(LX/15w;LX/0n3;)LX/0Xu;

    move-result-object v0

    return-object v0
.end method
