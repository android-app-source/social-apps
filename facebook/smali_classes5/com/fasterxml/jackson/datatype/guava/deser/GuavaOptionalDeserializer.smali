.class public final Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "LX/0am",
        "<*>;>;"
    }
.end annotation


# instance fields
.field private final _referenceType:LX/0lJ;


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 1

    .prologue
    .line 819400
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 819401
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;->_referenceType:LX/0lJ;

    .line 819402
    return-void
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "LX/0am",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 819398
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;->_referenceType:LX/0lJ;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 819399
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819397
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;->a(LX/15w;LX/0n3;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getNullValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819396
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method
