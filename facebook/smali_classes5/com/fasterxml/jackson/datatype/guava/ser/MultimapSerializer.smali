.class public Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "LX/0Xu",
        "<**>;>;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field private final a:LX/1Xo;

.field private final b:LX/2Ay;

.field private final c:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/4qz;

.field private final e:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Xo;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xo;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 819520
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    .line 819521
    iput-object p1, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a:LX/1Xo;

    .line 819522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->b:LX/2Ay;

    .line 819523
    iput-object p2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819524
    iput-object p3, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->d:LX/4qz;

    .line 819525
    iput-object p4, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819526
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 819566
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    .line 819567
    iget-object v0, p1, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a:LX/1Xo;

    iput-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a:LX/1Xo;

    .line 819568
    iput-object p2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->b:LX/2Ay;

    .line 819569
    iput-object p3, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819570
    iput-object p4, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->d:LX/4qz;

    .line 819571
    iput-object p5, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819572
    return-void
.end method

.method private a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;"
        }
    .end annotation

    .prologue
    .line 819565
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;-><init>(Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method private a(LX/0Xu;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 819560
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 819561
    invoke-interface {p1}, LX/0Xu;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 819562
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->b(LX/0Xu;LX/0nX;LX/0my;)V

    .line 819563
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 819564
    return-void
.end method

.method private a(LX/0Xu;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            "LX/4qz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 819556
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 819557
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->b(LX/0Xu;LX/0nX;LX/0my;)V

    .line 819558
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    .line 819559
    return-void
.end method

.method private final b(LX/0Xu;LX/0nX;LX/0my;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<**>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 819545
    invoke-interface {p1}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 819546
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v2, :cond_0

    .line 819547
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 819548
    :goto_1
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v2, :cond_2

    .line 819549
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 819550
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 819551
    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v3, v2, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_2

    .line 819552
    :cond_0
    const-class v2, Ljava/lang/String;

    invoke-virtual {p3, v2}, LX/0mz;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->b:LX/2Ay;

    invoke-virtual {p3, v2, v3}, LX/0my;->b(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_1

    .line 819553
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    goto :goto_0

    .line 819554
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p3, v0, p2}, LX/0my;->a(Ljava/lang/Object;LX/0nX;)V

    goto :goto_0

    .line 819555
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 819529
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->e:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819530
    if-nez v0, :cond_3

    .line 819531
    iget-object v1, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a:LX/1Xo;

    invoke-virtual {v1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    .line 819532
    invoke-virtual {v1}, LX/0lJ;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 819533
    invoke-virtual {p1, v1, p2}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    :cond_0
    move-object v1, v0

    .line 819534
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 819535
    if-nez v0, :cond_4

    .line 819536
    iget-object v0, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a:LX/1Xo;

    invoke-virtual {v0}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/0my;->b(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 819537
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->d:LX/4qz;

    .line 819538
    if-eqz v2, :cond_2

    .line 819539
    invoke-virtual {v2, p2}, LX/4qz;->a(LX/2Ay;)LX/4qz;

    move-result-object v2

    .line 819540
    :cond_2
    invoke-direct {p0, p2, v0, v2, v1}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a(LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;

    move-result-object v0

    return-object v0

    .line 819541
    :cond_3
    instance-of v1, v0, LX/0nU;

    if-eqz v1, :cond_5

    .line 819542
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 819543
    :cond_4
    instance-of v2, v0, LX/0nU;

    if-eqz v2, :cond_1

    .line 819544
    check-cast v0, LX/0nU;

    invoke-interface {v0, p1, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 819528
    check-cast p1, LX/0Xu;

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a(LX/0Xu;LX/0nX;LX/0my;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 819527
    check-cast p1, LX/0Xu;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;->a(LX/0Xu;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method
