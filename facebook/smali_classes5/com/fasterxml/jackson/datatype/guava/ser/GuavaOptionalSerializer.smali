.class public final Lcom/fasterxml/jackson/datatype/guava/ser/GuavaOptionalSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "LX/0am",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 0

    .prologue
    .line 819517
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(LX/0lJ;)V

    .line 819518
    return-void
.end method

.method private static a(LX/0am;LX/0nX;LX/0my;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<*>;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 819513
    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819514
    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, LX/0my;->a(Ljava/lang/Object;LX/0nX;)V

    .line 819515
    :goto_0
    return-void

    .line 819516
    :cond_0
    invoke-virtual {p2, p1}, LX/0my;->a(LX/0nX;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 819519
    check-cast p1, LX/0am;

    invoke-static {p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/ser/GuavaOptionalSerializer;->a(LX/0am;LX/0nX;LX/0my;)V

    return-void
.end method
