.class public Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/megaphone/model/MegaphoneLayoutResponseDeserializer;
.end annotation


# instance fields
.field public final cacheId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cache_id"
    .end annotation
.end field

.field public final layout:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 583974
    const-class v0, Lcom/facebook/megaphone/model/MegaphoneLayoutResponseDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 583975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583976
    iput-object v0, p0, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;->cacheId:Ljava/lang/String;

    .line 583977
    iput-object v0, p0, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;->layout:Ljava/lang/String;

    .line 583978
    return-void
.end method
