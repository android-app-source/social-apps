.class public Lcom/facebook/megaphone/api/FetchMegaphoneResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/megaphone/api/FetchMegaphoneResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLMegaphone;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583960
    new-instance v0, LX/3TB;

    invoke-direct {v0}, LX/3TB;-><init>()V

    sput-object v0, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 583961
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 583962
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphone;

    iput-object v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583963
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLMegaphone;LX/0ta;J)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLMegaphone;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 583964
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 583965
    iput-object p1, p0, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583966
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 583967
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 583968
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 583969
    iget-object v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 583970
    return-void
.end method
