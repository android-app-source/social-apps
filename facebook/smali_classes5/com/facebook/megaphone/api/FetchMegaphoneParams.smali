.class public Lcom/facebook/megaphone/api/FetchMegaphoneParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/megaphone/api/FetchMegaphoneParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583808
    new-instance v0, LX/3T8;

    invoke-direct {v0}, LX/3T8;-><init>()V

    sput-object v0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 583809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583810
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iput-object v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 583811
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->b:I

    .line 583812
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->c:I

    .line 583813
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;II)V
    .locals 0

    .prologue
    .line 583814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583815
    iput-object p1, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 583816
    iput p2, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->b:I

    .line 583817
    iput p3, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->c:I

    .line 583818
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 583819
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 583820
    iget-object v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 583821
    iget v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 583822
    iget v0, p0, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 583823
    return-void
.end method
