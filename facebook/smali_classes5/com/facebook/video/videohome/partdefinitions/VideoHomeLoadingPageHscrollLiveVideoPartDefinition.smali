.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614357
    new-instance v0, LX/3cS;

    invoke-direct {v0}, LX/3cS;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614358
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614359
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;
    .locals 3

    .prologue
    .line 614360
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    monitor-enter v1

    .line 614361
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614362
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614363
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614364
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 614365
    new-instance v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    invoke-direct {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;-><init>()V

    .line 614366
    move-object v0, v0

    .line 614367
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614368
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614369
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614370
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 614371
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614372
    const/4 v0, 0x1

    return v0
.end method
