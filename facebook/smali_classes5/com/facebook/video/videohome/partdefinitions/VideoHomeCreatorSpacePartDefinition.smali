.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETe;",
        ":",
        "LX/ETi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/ETj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EUp;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final b:Ljava/lang/String;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final e:Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

.field private final h:LX/0xX;

.field private final i:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 613840
    new-instance v0, LX/3cQ;

    invoke-direct {v0}, LX/3cQ;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a:LX/1Cz;

    .line 613841
    const-class v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    const-string v1, "video_home"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;LX/0xX;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613919
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613920
    const-class v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->b:Ljava/lang/String;

    .line 613921
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 613922
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->e:Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;

    .line 613923
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 613924
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    .line 613925
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->h:LX/0xX;

    .line 613926
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->i:LX/03V;

    .line 613927
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;
    .locals 10

    .prologue
    .line 613908
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    monitor-enter v1

    .line 613909
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613910
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613911
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613912
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613913
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v8

    check-cast v8, LX/0xX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;LX/0xX;LX/03V;)V

    .line 613914
    move-object v0, v3

    .line 613915
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613916
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613917
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/EUp;)Z
    .locals 2

    .prologue
    .line 613905
    iget-object v0, p0, LX/EUp;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 613906
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 613907
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/EUp;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-static {v0}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613904
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 613850
    check-cast p2, LX/EUp;

    check-cast p3, LX/ETe;

    .line 613851
    iget-object v0, p2, LX/EUp;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 613852
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 613853
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 613854
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v1

    .line 613855
    invoke-static {v1}, LX/ESx;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 613856
    const v2, 0x7f0d3116

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    .line 613857
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 613858
    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    :goto_1
    move-object v5, v5

    .line 613859
    invoke-virtual {v4, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v4

    sget-object v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 613860
    iput-object v5, v4, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 613861
    move-object v4, v4

    .line 613862
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 613863
    iget-object v6, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 613864
    invoke-interface {v6}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v7

    .line 613865
    iget-object v6, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 613866
    invoke-interface {v6}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v8

    .line 613867
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 613868
    const-string v6, ""

    .line 613869
    sget-object p3, LX/ESz;->LIVE:LX/ESz;

    invoke-virtual {p3}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 613870
    const v6, 0x7f082256

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 613871
    :cond_0
    :goto_2
    const v8, 0x7f082255

    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 p3, 0x0

    .line 613872
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->l()LX/174;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v7}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->l()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    :goto_3
    move-object v7, v5

    .line 613873
    aput-object v7, v9, p3

    const/4 v7, 0x1

    aput-object v6, v9, v7

    invoke-static {v8, v9}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 613874
    iput-object v5, v4, LX/2f8;->d:Ljava/lang/CharSequence;

    .line 613875
    move-object v4, v4

    .line 613876
    const/4 v5, 0x1

    .line 613877
    iput-boolean v5, v4, LX/2f8;->g:Z

    .line 613878
    move-object v4, v4

    .line 613879
    invoke-virtual {v4}, LX/2f8;->a()LX/2f9;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613880
    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->h:LX/0xX;

    sget-object v3, LX/1vy;->CREATOR_SPACE:LX/1vy;

    invoke-virtual {v2, v3}, LX/0xX;->a(LX/1vy;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 613881
    new-instance v2, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;

    invoke-direct {v2, v1}, Lcom/facebook/video/videohome/creatorchannel/CreatorChannelImpl;-><init>(Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 613882
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 613883
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 613884
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613885
    new-instance v4, LX/ESh;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, LX/ESh;-><init>(Ljava/util/ArrayList;I)V

    move-object v2, v4

    .line 613886
    const v3, 0x7f0d3116

    iget-object v4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->e:Lcom/facebook/video/creatorchannel/CreatorChannelLauncherPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613887
    :goto_4
    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpaceStatusPartDefinition;

    new-instance v3, LX/EUq;

    .line 613888
    iget-object v4, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v4

    .line 613889
    invoke-interface {v0}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v0

    .line 613890
    const/4 v4, 0x0

    .line 613891
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 613892
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 613893
    :cond_1
    move-object v1, v4

    .line 613894
    invoke-direct {v3, v0, v1}, LX/EUq;-><init>(LX/ESz;Ljava/lang/String;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613895
    const/4 v0, 0x0

    return-object v0

    .line 613896
    :cond_2
    sget-object v2, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    iget v3, p2, LX/EUp;->c:I

    .line 613897
    new-instance v4, LX/EUn;

    invoke-direct {v4, v0, v2, v3}, LX/EUn;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;I)V

    move-object v2, v4

    .line 613898
    sget-object v3, LX/0JD;->CREATOR_SPACE_LIST:LX/0JD;

    .line 613899
    iput-object v3, v2, LX/EUn;->c:LX/0JD;

    .line 613900
    move-object v2, v2

    .line 613901
    const v3, 0x7f0d3116

    iget-object v4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_4

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 613902
    :cond_5
    sget-object p3, LX/ESz;->UNSEEN:LX/ESz;

    invoke-virtual {p3}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 613903
    const v6, 0x7f082257

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x73a93895

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613843
    check-cast p1, LX/EUp;

    check-cast p3, LX/ETe;

    .line 613844
    iget v2, p1, LX/EUp;->c:I

    .line 613845
    iget-object p0, p1, LX/EUp;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-object v1, p3

    .line 613846
    check-cast v1, LX/ETi;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1, p2}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v1

    .line 613847
    iget-object p2, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object p2, p2

    .line 613848
    invoke-interface {p3, v2, v1, p2, p0}, LX/ETe;->a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 613849
    const/16 v1, 0x1f

    const v2, 0x763ce090

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613842
    check-cast p1, LX/EUp;

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a(LX/EUp;)Z

    move-result v0

    return v0
.end method
