.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/videohome/environment/HasShownVideosContainer;",
        ":",
        "LX/ETh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 614518
    const v0, 0x7f0315d7

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->a:LX/1Cz;

    .line 614519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614520
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614521
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;

    .line 614522
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    .line 614523
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    .line 614524
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;
    .locals 6

    .prologue
    .line 614525
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    monitor-enter v1

    .line 614526
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614527
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614530
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;)V

    .line 614531
    move-object v0, p0

    .line 614532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614536
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 614537
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614538
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614539
    const v1, 0x7f0d177e

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614540
    const v1, 0x7f0d3112

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    invoke-static {p2}, LX/EVD;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/EVC;

    move-result-object v3

    const/4 v4, 0x0

    .line 614541
    iput-boolean v4, v3, LX/EVC;->b:Z

    .line 614542
    move-object v3, v3

    .line 614543
    const/4 v4, 0x1

    .line 614544
    iput-boolean v4, v3, LX/EVC;->c:Z

    .line 614545
    move-object v3, v3

    .line 614546
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 614547
    invoke-interface {v4}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v4

    .line 614548
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v5

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v5, p3, :cond_0

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v4, v5, :cond_2

    .line 614549
    :cond_0
    const v4, 0x7f0a06e5

    .line 614550
    :goto_0
    move v4, v4

    .line 614551
    iput v4, v3, LX/EVC;->d:I

    .line 614552
    move-object v3, v3

    .line 614553
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 614554
    invoke-interface {v4}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v4

    .line 614555
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v5

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v5, p3, :cond_1

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v4, v5, :cond_3

    .line 614556
    :cond_1
    const v4, 0x7f0a008e

    .line 614557
    :goto_1
    move v4, v4

    .line 614558
    iput v4, v3, LX/EVC;->e:I

    .line 614559
    move-object v3, v3

    .line 614560
    invoke-interface {v0}, LX/9uc;->cm()LX/5sd;

    move-result-object v0

    invoke-static {v0}, LX/9tr;->a(LX/5sd;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 614561
    iput-object v0, v3, LX/EVC;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 614562
    move-object v0, v3

    .line 614563
    invoke-virtual {v0}, LX/EVC;->a()LX/EVD;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614564
    const v0, 0x7f0d3128

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614565
    const/4 v0, 0x0

    return-object v0

    :cond_2
    const v4, 0x106000d

    goto :goto_0

    :cond_3
    const v4, 0x106000d

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x14caf447

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614566
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    check-cast p3, LX/ETy;

    .line 614567
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 614568
    invoke-interface {v1}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 614569
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 614570
    iget-object v2, p3, LX/ETy;->t:LX/ETk;

    move-object v2, v2

    .line 614571
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ETk;->a(Ljava/lang/String;)V

    .line 614572
    const/16 v1, 0x1f

    const v2, 0xf32f547

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 614573
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614574
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614575
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->b:LX/0Px;

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
