.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EVN;",
        "LX/EVJ;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

.field private final c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

.field private final g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

.field private final h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0xX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614219
    new-instance v0, LX/3nc;

    invoke-direct {v0}, LX/3nc;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0xX;LX/0Ot;Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;",
            ">;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;",
            "Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614208
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614209
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->j:LX/0xX;

    .line 614210
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->i:LX/0Ot;

    .line 614211
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    .line 614212
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    .line 614213
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    .line 614214
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 614215
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    .line 614216
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    .line 614217
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    .line 614218
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;
    .locals 13

    .prologue
    .line 614148
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;

    monitor-enter v1

    .line 614149
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614150
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614153
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v4

    check-cast v4, LX/0xX;

    const/16 v5, 0x3826

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;-><init>(LX/0xX;LX/0Ot;Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;)V

    .line 614154
    move-object v0, v3

    .line 614155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614207
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 614175
    check-cast p2, LX/EVN;

    check-cast p3, LX/1Pr;

    .line 614176
    iget-object v2, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614177
    iget-object v0, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614178
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 614179
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 614180
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 614181
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 614182
    new-instance v5, LX/Bt6;

    invoke-direct {v5, v0}, LX/Bt6;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v5, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bt7;

    .line 614183
    invoke-static {v3}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 614184
    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v5, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614185
    const v5, 0x7f0d02c4

    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-interface {p1, v5, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614186
    const v5, 0x7f0d0550

    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    invoke-interface {p1, v5, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614187
    const v1, 0x7f0d311e

    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeTextOrHiddenPartDefinition;

    .line 614188
    iget-object v6, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 614189
    invoke-interface {v6}, LX/9uc;->cm()LX/5sd;

    move-result-object v6

    .line 614190
    if-nez v6, :cond_2

    .line 614191
    const/4 v6, 0x0

    .line 614192
    :goto_0
    move-object v6, v6

    .line 614193
    invoke-interface {p1, v1, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614194
    if-eqz v3, :cond_0

    .line 614195
    const v1, 0x7f0d02c4

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    sget-object v5, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    invoke-static {v2, v5}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v5

    sget-object v6, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    .line 614196
    iput-object v6, v5, LX/EUn;->c:LX/0JD;

    .line 614197
    move-object v5, v5

    .line 614198
    invoke-interface {p1, v1, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614199
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->j:LX/0xX;

    sget-object v3, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    invoke-virtual {v1, v3}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 614200
    const v1, 0x7f0d0906

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    new-instance v5, LX/EUx;

    const-string v6, "RECOMMENDED_VIDEO_CHANNEL"

    const/4 v7, 0x1

    invoke-direct {v5, v2, v4, v6, v7}, LX/EUx;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v1, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614201
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->j:LX/0xX;

    sget-object v3, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    invoke-virtual {v1, v3}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 614202
    const v3, 0x7f0d1e70

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    iget-object v4, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-interface {p1, v3, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614203
    :cond_1
    const v1, 0x7f0d14ad

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSocialContextPartDefinition;

    invoke-interface {p1, v1, v3, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614204
    new-instance v1, LX/EVJ;

    .line 614205
    new-instance v2, LX/EVI;

    invoke-direct {v2, p0, v0}, LX/EVI;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;LX/Bt7;)V

    move-object v2, v2

    .line 614206
    invoke-direct {v1, v0, v2}, LX/EVJ;-><init>(LX/Bt7;LX/EVH;)V

    return-object v1

    :cond_2
    invoke-interface {v6}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6557ce6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614165
    check-cast p2, LX/EVJ;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;

    .line 614166
    iget-object v1, p2, LX/EVJ;->a:LX/Bt7;

    .line 614167
    iget-boolean v2, v1, LX/Bt7;->a:Z

    move v1, v2

    .line 614168
    if-eqz v1, :cond_0

    .line 614169
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a(Z)V

    .line 614170
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x3e09cced

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 614171
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a(Z)V

    .line 614172
    iget-object v1, p2, LX/EVJ;->b:LX/EVH;

    .line 614173
    iput-object v1, p4, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->e:LX/EVH;

    .line 614174
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614164
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 614159
    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;

    .line 614160
    const/4 v0, 0x0

    .line 614161
    iput-object v0, p4, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->e:LX/EVH;

    .line 614162
    invoke-virtual {p4}, Lcom/facebook/video/videohome/views/VideoHomeVideoChannelFeedUnitBottomView;->a()V

    .line 614163
    return-void
.end method
