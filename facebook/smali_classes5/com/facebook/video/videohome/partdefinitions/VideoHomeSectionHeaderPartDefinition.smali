.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETi;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/EVG;",
        "TE;",
        "LX/EVp;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:LX/23r;

.field public final c:LX/2xj;

.field public final d:LX/3AW;

.field public final e:Ljava/lang/String;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Landroid/content/Context;

.field public j:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614483
    new-instance v0, LX/3cT;

    invoke-direct {v0}, LX/3cT;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;LX/23r;Landroid/content/res/Resources;LX/2xj;LX/3AW;LX/0Or;LX/0Or;LX/0Or;Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "LX/23r;",
            "Landroid/content/res/Resources;",
            "LX/2xj;",
            "LX/3AW;",
            "LX/0Or",
            "<",
            "LX/121;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Or",
            "<",
            "LX/15W;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614472
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614473
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->j:Landroid/os/Handler;

    .line 614474
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->b:LX/23r;

    .line 614475
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->c:LX/2xj;

    .line 614476
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->d:LX/3AW;

    .line 614477
    const v0, 0x7f080e4a

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->e:Ljava/lang/String;

    .line 614478
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->f:LX/0Or;

    .line 614479
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->g:LX/0Or;

    .line 614480
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->h:LX/0Or;

    .line 614481
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->i:Landroid/content/Context;

    .line 614482
    return-void
.end method

.method public static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/ETi;)LX/3Qx;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "TE;)",
            "LX/3Qx;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 614450
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 614451
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 614452
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 614453
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 614454
    invoke-interface {p3, v0}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v4

    .line 614455
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614456
    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    .line 614457
    new-instance v0, LX/3Qv;

    invoke-direct {v0}, LX/3Qv;-><init>()V

    invoke-virtual {v0, v2}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v0

    const-string v1, "VIDEO_HOME"

    .line 614458
    iput-object v1, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 614459
    move-object v0, v0

    .line 614460
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 614461
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 614462
    move-object v0, v0

    .line 614463
    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    .line 614464
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 614465
    move-object v0, v0

    .line 614466
    iput-object v5, v0, LX/3Qv;->o:Ljava/lang/String;

    .line 614467
    move-object v0, v0

    .line 614468
    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v6

    .line 614469
    iget-object v7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->b:LX/23r;

    .line 614470
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, LX/EVF;

    invoke-direct {v1, p0}, LX/EVF;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object v8, v0

    .line 614471
    new-instance v0, LX/EVE;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/EVE;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILjava/lang/String;)V

    move-object v1, v7

    move-object v2, v6

    move-object v3, v9

    move-object v4, v8

    move-object v5, v9

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;
    .locals 13

    .prologue
    .line 614439
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    monitor-enter v1

    .line 614440
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614441
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614442
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614443
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614444
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    const-class v5, LX/23r;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/23r;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v7

    check-cast v7, LX/2xj;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v8

    check-cast v8, LX/3AW;

    const/16 v9, 0xbd8

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0xbd2

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0xbca

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const-class v12, Landroid/content/Context;

    invoke-interface {v0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;-><init>(Landroid/os/Handler;LX/23r;Landroid/content/res/Resources;LX/2xj;LX/3AW;LX/0Or;LX/0Or;LX/0Or;Landroid/content/Context;)V

    .line 614445
    move-object v0, v3

    .line 614446
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614447
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614448
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVG;LX/ETi;LX/EVp;)V
    .locals 2
    .param p2    # LX/EVG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/EVG;",
            "TE;",
            "LX/EVp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 614375
    const-string v0, "VideoHomeSectionHeaderPartDefinition.bind"

    const v1, -0x3546ecb2    # -6064551.0f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 614376
    :try_start_0
    invoke-static {p0, p1, p2, p4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVG;LX/EVp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614377
    const v0, -0x2584db6c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 614378
    return-void

    .line 614379
    :catchall_0
    move-exception v0

    const v1, 0x4dbd5cd8    # 3.97122304E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVG;LX/EVp;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/EVG;",
            "LX/EVp;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 614412
    iget-object v0, p3, LX/EVp;->b:Lcom/facebook/fig/header/FigHeader;

    move-object v2, v0

    .line 614413
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 614414
    invoke-interface {v3}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sZ;

    .line 614415
    invoke-interface {v0}, LX/5sZ;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/5sZ;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/5sZ;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/5sZ;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 614416
    invoke-interface {v0}, LX/5sZ;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;->b()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 614417
    invoke-virtual {v2, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 614418
    :goto_0
    invoke-interface {v0}, LX/5sZ;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 614419
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 614420
    iget-object v1, p2, LX/EVG;->a:LX/3GE;

    if-eqz v1, :cond_2

    .line 614421
    invoke-virtual {v2, v5}, Lcom/facebook/fig/header/FigHeader;->setActionType(I)V

    .line 614422
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fig/header/FigHeader;->setActionText(Ljava/lang/CharSequence;)V

    .line 614423
    iget-object v0, p2, LX/EVG;->a:LX/3GE;

    .line 614424
    iget-object v1, v0, LX/3GE;->b:LX/3GR;

    move-object v0, v1

    .line 614425
    iput-object v2, v0, LX/3GR;->b:Landroid/view/View;

    .line 614426
    iget-object v0, p2, LX/EVG;->a:LX/3GE;

    invoke-virtual {v2, v0}, Lcom/facebook/fig/header/FigHeader;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 614427
    :goto_1
    iget-object v0, p2, LX/EVG;->b:LX/3lA;

    if-eqz v0, :cond_0

    .line 614428
    iget-object v0, p3, LX/EVp;->a:Landroid/view/View;

    move-object v0, v0

    .line 614429
    iget-object v1, p2, LX/EVG;->b:LX/3lA;

    .line 614430
    iput-object v0, v1, LX/3lA;->c:Landroid/view/View;

    .line 614431
    new-instance v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition$4;

    invoke-direct {v6, p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition$4;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;)V

    .line 614432
    iget-object v7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->j:Landroid/os/Handler;

    const-wide/16 v8, 0x3e8

    const v10, 0x62f1d88e

    invoke-static {v7, v6, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 614433
    :cond_0
    return-void

    .line 614434
    :cond_1
    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_0

    .line 614435
    :cond_2
    invoke-interface {v3}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 614436
    invoke-virtual {v2, v5}, Lcom/facebook/fig/header/FigHeader;->setActionType(I)V

    .line 614437
    invoke-interface {v3}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fig/header/FigHeader;->setActionText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 614438
    :cond_3
    invoke-virtual {v2, v4}, Lcom/facebook/fig/header/FigHeader;->setActionType(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 3

    .prologue
    .line 614409
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614410
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 614411
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 614408
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 614393
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/ETi;

    .line 614394
    const/4 v0, 0x0

    .line 614395
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 614396
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_0

    .line 614397
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-string v1, "4296"

    const-class v2, LX/3lA;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3lA;

    .line 614398
    :cond_0
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 614399
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 614400
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-eq v2, v3, :cond_2

    .line 614401
    :cond_1
    const/4 v1, 0x0

    .line 614402
    :goto_0
    move-object v1, v1

    .line 614403
    new-instance v2, LX/EVG;

    invoke-direct {v2, v1, v0}, LX/EVG;-><init>(LX/3GE;LX/3lA;)V

    return-object v2

    .line 614404
    :cond_2
    new-instance v3, LX/3GR;

    invoke-static {p0, p2, v1, p3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/ETi;)LX/3Qx;

    move-result-object v1

    invoke-direct {v3, v1}, LX/3GR;-><init>(Landroid/view/View$OnClickListener;)V

    .line 614405
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    .line 614406
    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 614407
    new-instance v2, LX/3GE;

    invoke-direct {v2, v1, v3}, LX/3GE;-><init>(LX/121;LX/3GR;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3d451da0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614392
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/EVG;

    check-cast p3, LX/ETi;

    check-cast p4, LX/EVp;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVG;LX/ETi;LX/EVp;)V

    const/16 v1, 0x1f

    const v2, -0x4b1b9742

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614391
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 614380
    check-cast p2, LX/EVG;

    check-cast p4, LX/EVp;

    const/4 v1, 0x0

    .line 614381
    iget-object v0, p2, LX/EVG;->a:LX/3GE;

    if-eqz v0, :cond_0

    .line 614382
    iget-object v0, p2, LX/EVG;->a:LX/3GE;

    .line 614383
    iget-object p0, v0, LX/3GE;->b:LX/3GR;

    move-object v0, p0

    .line 614384
    iput-object v1, v0, LX/3GR;->b:Landroid/view/View;

    .line 614385
    :cond_0
    iget-object v0, p2, LX/EVG;->b:LX/3lA;

    if-eqz v0, :cond_1

    .line 614386
    iget-object v0, p2, LX/EVG;->b:LX/3lA;

    .line 614387
    iput-object v1, v0, LX/3lA;->c:Landroid/view/View;

    .line 614388
    :cond_1
    iget-object v0, p4, LX/EVp;->b:Lcom/facebook/fig/header/FigHeader;

    move-object v0, v0

    .line 614389
    invoke-virtual {v0, v1}, Lcom/facebook/fig/header/FigHeader;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 614390
    return-void
.end method
