.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EVD;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614341
    const v0, 0x7f0315c1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614354
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614355
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    .line 614356
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;
    .locals 4

    .prologue
    .line 614342
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    monitor-enter v1

    .line 614343
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614344
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614347
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;)V

    .line 614348
    move-object v0, p0

    .line 614349
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614350
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614351
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614353
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 614336
    check-cast p2, LX/EVD;

    .line 614337
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614338
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614339
    check-cast p1, LX/EVD;

    .line 614340
    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a(LX/EVD;)Z

    move-result v0

    return v0
.end method
