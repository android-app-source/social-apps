.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETi;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EVD;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614017
    const v0, 0x7f0315d4

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/multirow/parts/ViewColorPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613976
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613977
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->b:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    .line 613978
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    .line 613979
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->d:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    .line 613980
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 613981
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    .line 613982
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;
    .locals 9

    .prologue
    .line 614006
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    monitor-enter v1

    .line 614007
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614008
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614009
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614010
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614011
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewColorPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;-><init>(Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/multirow/parts/ViewColorPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;)V

    .line 614012
    move-object v0, v3

    .line 614013
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614014
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614015
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/EVD;)Z
    .locals 1

    .prologue
    .line 614005
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614004
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 613984
    check-cast p2, LX/EVD;

    .line 613985
    iget-object v0, p2, LX/EVD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 613986
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 613987
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 613988
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 613989
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 613990
    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 613991
    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->b:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    new-instance v4, LX/D6P;

    iget-boolean v5, p2, LX/EVD;->c:Z

    iget-object v6, p2, LX/EVD;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v4, v0, v5, v6}, LX/D6P;-><init>(Lcom/facebook/graphql/model/GraphQLStory;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613992
    const v0, 0x7f0d0952

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v4, p2, LX/EVD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613993
    if-eqz v1, :cond_0

    .line 613994
    const v0, 0x7f0d0906

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    new-instance v4, LX/EUx;

    iget-object v5, p2, LX/EVD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const-string v6, "RECOMMENDED_VIDEO_CHANNEL"

    iget-boolean v7, p2, LX/EVD;->b:Z

    invoke-direct {v4, v5, v2, v6, v7}, LX/EUx;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613995
    :cond_0
    if-eqz v1, :cond_1

    .line 613996
    iget-object v0, p2, LX/EVD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v1, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    invoke-static {v0, v1}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v0

    sget-object v1, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    .line 613997
    iput-object v1, v0, LX/EUn;->c:LX/0JD;

    .line 613998
    move-object v0, v0

    .line 613999
    const v1, 0x7f0d0953

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614000
    const v1, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614001
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->d:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    iget v1, p2, LX/EVD;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614002
    const v0, 0x7f0d3113

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->d:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    iget v2, p2, LX/EVD;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614003
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613983
    check-cast p1, LX/EVD;

    invoke-static {p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a(LX/EVD;)Z

    move-result v0

    return v0
.end method
