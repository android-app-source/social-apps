.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/videohome/environment/HasShownVideosContainer;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

.field private final d:LX/0xX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614306
    new-instance v0, LX/3cR;

    invoke-direct {v0}, LX/3cR;-><init>()V

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0xX;Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614301
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614302
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->d:LX/0xX;

    .line 614303
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    .line 614304
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    .line 614305
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;
    .locals 6

    .prologue
    .line 614290
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;

    monitor-enter v1

    .line 614291
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614292
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614293
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614294
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614295
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v3

    check-cast v3, LX/0xX;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;-><init>(LX/0xX;Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;)V

    .line 614296
    move-object v0, p0

    .line 614297
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614298
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614299
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 614280
    sget-object v2, LX/EVK;->a:[I

    .line 614281
    iget-object v3, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 614282
    invoke-interface {v3}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 614283
    :cond_0
    :goto_0
    return v0

    .line 614284
    :pswitch_0
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 614285
    invoke-interface {v2}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v2

    .line 614286
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v2, v3, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 614287
    :pswitch_1
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 614288
    invoke-interface {v2}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 614289
    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->d:LX/0xX;

    sget-object v4, LX/1vy;->DOWNLOAD_SEEN_STATE:LX/1vy;

    invoke-virtual {v3, v4}, LX/0xX;->a(LX/1vy;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614279
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 614255
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 614256
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614257
    const v1, 0x7f0d177e

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614258
    const v1, 0x7f0d3112

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;

    invoke-static {p2}, LX/EVD;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/EVC;

    move-result-object v3

    const/4 v4, 0x0

    .line 614259
    iput-boolean v4, v3, LX/EVC;->b:Z

    .line 614260
    move-object v3, v3

    .line 614261
    const/4 v4, 0x1

    .line 614262
    iput-boolean v4, v3, LX/EVC;->c:Z

    .line 614263
    move-object v3, v3

    .line 614264
    invoke-static {p0, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->d(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 614265
    const v4, 0x7f0a06e5

    .line 614266
    :goto_0
    move v4, v4

    .line 614267
    iput v4, v3, LX/EVC;->d:I

    .line 614268
    move-object v3, v3

    .line 614269
    invoke-static {p0, p2}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->d(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 614270
    const v4, 0x7f0a008e

    .line 614271
    :goto_1
    move v4, v4

    .line 614272
    iput v4, v3, LX/EVC;->e:I

    .line 614273
    move-object v3, v3

    .line 614274
    invoke-interface {v0}, LX/9uc;->cm()LX/5sd;

    move-result-object v0

    invoke-static {v0}, LX/9tr;->a(LX/5sd;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 614275
    iput-object v0, v3, LX/EVC;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 614276
    move-object v0, v3

    .line 614277
    invoke-virtual {v0}, LX/EVC;->a()LX/EVD;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614278
    const/4 v0, 0x0

    return-object v0

    :cond_0
    const v4, 0x106000d

    goto :goto_0

    :cond_1
    const v4, 0x106000d

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x76d3a03e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614248
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/ETy;

    .line 614249
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 614250
    invoke-interface {v1}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 614251
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 614252
    iget-object v2, p3, LX/ETy;->t:LX/ETk;

    move-object v2, v2

    .line 614253
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ETk;->a(Ljava/lang/String;)V

    .line 614254
    const/16 v1, 0x1f

    const v2, 0x582b9c36

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 614245
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 614246
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614247
    if-eqz v0, :cond_0

    invoke-static {p1}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
