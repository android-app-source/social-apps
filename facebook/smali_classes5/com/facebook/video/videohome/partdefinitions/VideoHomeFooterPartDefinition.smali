.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/String;",
        "TE;",
        "Lcom/facebook/fig/footer/FigFooter;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final c:LX/ETB;

.field private final d:LX/EVb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613930
    const v0, 0x7f0315cd

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/ETB;LX/EVb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613949
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613950
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 613951
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->c:LX/ETB;

    .line 613952
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->d:LX/EVb;

    .line 613953
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;
    .locals 6

    .prologue
    .line 613938
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    monitor-enter v1

    .line 613939
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613940
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613941
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613942
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613943
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v4

    check-cast v4, LX/ETB;

    invoke-static {v0}, LX/EVb;->a(LX/0QB;)LX/EVb;

    move-result-object v5

    check-cast v5, LX/EVb;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/ETB;LX/EVb;)V

    .line 613944
    move-object v0, p0

    .line 613945
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613946
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613947
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613937
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 613954
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 613955
    sget-object v0, LX/EUm;->SEE_MORE:LX/EUm;

    invoke-static {p2, v0}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v0

    sget-object v1, LX/0JD;->SEE_MORE_BUTTON:LX/0JD;

    .line 613956
    iput-object v1, v0, LX/EUn;->c:LX/0JD;

    .line 613957
    move-object v0, v0

    .line 613958
    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613959
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 613960
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x20e304c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613934
    check-cast p2, Ljava/lang/String;

    check-cast p4, Lcom/facebook/fig/footer/FigFooter;

    .line 613935
    invoke-virtual {p4, p2}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(Ljava/lang/CharSequence;)V

    .line 613936
    const/16 v1, 0x1f

    const v2, 0x50ba391

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 3

    .prologue
    .line 613932
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 613933
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->c:LX/ETB;

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->d:LX/EVb;

    invoke-static {p1, v0, v1}, LX/EV8;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETB;LX/EVb;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613931
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {p0, p1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
