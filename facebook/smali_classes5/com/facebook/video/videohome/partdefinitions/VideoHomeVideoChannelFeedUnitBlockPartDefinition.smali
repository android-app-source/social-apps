.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ":",
        "LX/ETi;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EVN;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static q:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

.field private final f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

.field private final g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

.field private final h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

.field private final i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0xX;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EVP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 614018
    const v0, 0x7f0315ca

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->a:LX/1Cz;

    .line 614019
    const-class v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;

    const-string v1, "video_home"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0xX;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/0Ot;Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xX;",
            "Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDownloadButtonPartDefinition;",
            ">;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoLiveIndicatorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EVP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614066
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614067
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->k:LX/0xX;

    .line 614068
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    .line 614069
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 614070
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    .line 614071
    iput-object p9, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    .line 614072
    iput-object p7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    .line 614073
    iput-object p6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    .line 614074
    iput-object p8, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    .line 614075
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->j:LX/0Ot;

    .line 614076
    iput-object p10, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->l:LX/0Ot;

    .line 614077
    iput-object p11, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->m:LX/0Ot;

    .line 614078
    iput-object p12, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->n:LX/0Ot;

    .line 614079
    iput-object p13, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->o:LX/0Ot;

    .line 614080
    iput-object p14, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->p:LX/0Ot;

    .line 614081
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;
    .locals 3

    .prologue
    .line 614056
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;

    monitor-enter v1

    .line 614057
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614058
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614059
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614060
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->b(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614061
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614062
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;
    .locals 15

    .prologue
    .line 614064
    new-instance v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v1

    check-cast v1, LX/0xX;

    invoke-static {p0}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    const/16 v4, 0x3826

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    invoke-static {p0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    const/16 v10, 0xe12

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x649

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1b

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3836

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x3839

    invoke-static {p0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;-><init>(LX/0xX;Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/0Ot;Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 614065
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614055
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 614021
    check-cast p2, LX/EVN;

    .line 614022
    iget-object v1, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614023
    iget-object v0, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 614024
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 614025
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 614026
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614027
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->k:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 614028
    const v4, 0x7f0d0b87

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    .line 614029
    new-instance v5, LX/2f8;

    invoke-direct {v5}, LX/2f8;-><init>()V

    sget-object v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 614030
    iput-object v6, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 614031
    move-object v5, v5

    .line 614032
    const/4 v6, 0x1

    .line 614033
    iput-boolean v6, v5, LX/2f8;->g:Z

    .line 614034
    move-object v6, v5

    .line 614035
    invoke-static {v2}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 614036
    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const v8, 0x7f0b16b9

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 614037
    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1qa;

    sget-object v9, LX/26P;->Video:LX/26P;

    invoke-virtual {v5, v7, v8, v9}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v5

    .line 614038
    iput-object v5, v6, LX/2f8;->a:LX/1bf;

    .line 614039
    invoke-virtual {v6}, LX/2f8;->a()LX/2f9;

    move-result-object v5

    move-object v5, v5

    .line 614040
    invoke-interface {p1, v4, v0, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614041
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614042
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EVP;

    check-cast p3, LX/ETi;

    invoke-virtual {v0, v1, v2, p3}, LX/EVP;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;LX/ETi;)V

    .line 614043
    :goto_0
    const v0, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->c:Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614044
    const v0, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    sget-object v4, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    invoke-static {v1, v4}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v4

    sget-object v5, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    .line 614045
    iput-object v5, v4, LX/EUn;->c:LX/0JD;

    .line 614046
    move-object v4, v4

    .line 614047
    invoke-interface {p1, v0, v2, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614048
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomeDescriptionPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614049
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeMenuPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614050
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->k:LX/0xX;

    sget-object v2, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    invoke-virtual {v0, v2}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614051
    const v2, 0x7f0d1e70

    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614052
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->h:Lcom/facebook/video/videohome/partdefinitions/VideoHomeSocialContextPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614053
    const/4 v0, 0x0

    return-object v0

    .line 614054
    :cond_1
    const v0, 0x7f0d2ca1

    iget-object v2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614020
    const/4 v0, 0x1

    return v0
.end method
