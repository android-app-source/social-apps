.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EVN;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

.field private final d:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

.field private final f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 614082
    const v0, 0x7f0315de

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->a:LX/1Cz;

    .line 614083
    const-class v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;

    const-string v1, "video_home"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614084
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614085
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    .line 614086
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    .line 614087
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->e:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    .line 614088
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    .line 614089
    iput-object p5, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 614090
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;
    .locals 9

    .prologue
    .line 614091
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;

    monitor-enter v1

    .line 614092
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614093
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614096
    new-instance v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V

    .line 614097
    move-object v0, v3

    .line 614098
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614099
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614100
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614102
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 614103
    check-cast p2, LX/EVN;

    .line 614104
    iget-object v0, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614105
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 614106
    invoke-interface {v1}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 614107
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 614108
    invoke-static {v3}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 614109
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    .line 614110
    const v6, 0x7f0d2ca1

    iget-object v7, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    invoke-interface {p1, v6, v7, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614111
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VIDEO_CHANNEL_WITH_INJECTED_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v6, v7, :cond_4

    const/4 v6, 0x1

    :goto_0
    move v1, v6

    .line 614112
    if-eqz v1, :cond_3

    .line 614113
    iget-object v0, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    sget-object v1, LX/EUm;->RELATED_VIDEO_CHANNEL:LX/EUm;

    invoke-static {v0, v1}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v0

    sget-object v1, LX/0JD;->CHANNELFEED_VIDEO:LX/0JD;

    .line 614114
    iput-object v1, v0, LX/EUn;->c:LX/0JD;

    .line 614115
    move-object v0, v0

    .line 614116
    const v1, 0x7f0d2ca1

    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v1, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614117
    :goto_1
    const v0, 0x7f0d177e

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->e:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    .line 614118
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-double v8, v8

    .line 614119
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v10

    invoke-static {v10}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v10

    if-eqz v10, :cond_5

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v5, v10, :cond_5

    .line 614120
    const-wide v10, 0x3fe5555560000000L    # 0.6666666865348816

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    .line 614121
    :cond_0
    :goto_2
    move-wide v6, v8

    .line 614122
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614123
    if-eqz v4, :cond_2

    .line 614124
    iget-object v0, p2, LX/EVN;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    sget-object v1, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    invoke-static {v0, v1}, LX/EUn;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUm;)LX/EUn;

    move-result-object v0

    sget-object v1, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    .line 614125
    iput-object v1, v0, LX/EUn;->c:LX/0JD;

    .line 614126
    move-object v0, v0

    .line 614127
    const v1, 0x7f0d0953

    iget-object v3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    invoke-interface {p1, v1, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614128
    const v0, 0x7f0d0953

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 614129
    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-static {v4, v2}, Lcom/facebook/video/channelfeed/PublisherTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    .line 614130
    iput-object v5, v3, LX/2f8;->d:Ljava/lang/CharSequence;

    .line 614131
    move-object v3, v3

    .line 614132
    sget-object v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 614133
    iput-object v5, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 614134
    move-object v3, v3

    .line 614135
    const/4 v5, 0x1

    .line 614136
    iput-boolean v5, v3, LX/2f8;->g:Z

    .line 614137
    move-object v3, v3

    .line 614138
    invoke-static {v4}, LX/1xl;->f(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v5

    .line 614139
    if-eqz v5, :cond_1

    .line 614140
    invoke-virtual {v3, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    .line 614141
    :cond_1
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    move-object v2, v3

    .line 614142
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614143
    :cond_2
    const/4 v0, 0x0

    return-object v0

    .line 614144
    :cond_3
    const v1, 0x7f0d2ca1

    iget-object v6, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v1, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 614145
    :cond_5
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v10, v8, v10

    if-eqz v10, :cond_0

    .line 614146
    const-wide v10, 0x3ffc71c720000000L    # 1.7777777910232544

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614147
    const/4 v0, 0x1

    return v0
.end method
