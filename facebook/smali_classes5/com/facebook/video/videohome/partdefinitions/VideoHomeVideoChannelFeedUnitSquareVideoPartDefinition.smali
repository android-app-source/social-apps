.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETi;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

.field private final c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

.field private final e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614309
    const v0, 0x7f0315dd

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614310
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614311
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    .line 614312
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    .line 614313
    iput-object p3, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    .line 614314
    iput-object p4, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    .line 614315
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;
    .locals 7

    .prologue
    .line 614316
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    monitor-enter v1

    .line 614317
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614318
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614319
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614320
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614321
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;->a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;)V

    .line 614322
    move-object v0, p0

    .line 614323
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614324
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614325
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614326
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614327
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 614328
    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614329
    const v0, 0x7f0d2ca1

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->b:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoPlayerPartDefinition;

    new-instance v2, LX/EVN;

    invoke-direct {v2, p2}, LX/EVN;-><init>(Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614330
    const v0, 0x7f0d2ca1

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614331
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614332
    const v0, 0x7f0d3128

    iget-object v1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoNotificationContextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 614333
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614334
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 614335
    invoke-static {p1}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
