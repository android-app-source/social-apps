.class public Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/21I;",
        "TE;",
        "Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/20g;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614614
    const v0, 0x7f03159d

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/20g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614615
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614616
    iput-object p1, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->b:LX/0Uh;

    .line 614617
    iput-object p2, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->c:LX/20g;

    .line 614618
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;
    .locals 5

    .prologue
    .line 614619
    const-class v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;

    monitor-enter v1

    .line 614620
    :try_start_0
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614621
    sput-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614622
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614623
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614624
    new-instance p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/20g;->a(LX/0QB;)LX/20g;

    move-result-object v4

    check-cast v4, LX/20g;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;-><init>(LX/0Uh;LX/20g;)V

    .line 614625
    move-object v0, p0

    .line 614626
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614627
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614628
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 614613
    sget-object v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 614599
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 614600
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->c:LX/20g;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Z)LX/21I;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7e7aacd3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614609
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/21I;

    check-cast p3, LX/1Po;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    .line 614610
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 614611
    invoke-virtual {p4, p1, p2, p3}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21I;LX/1Po;)V

    .line 614612
    const/16 v1, 0x1f

    const v2, 0x4cae78dc    # 9.1473632E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 614601
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 614602
    iget-object v0, p0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->b:LX/0Uh;

    const/16 v2, 0x2b9

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614603
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 614604
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 614605
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/21I;

    check-cast p3, LX/1Po;

    check-cast p4, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    .line 614606
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 614607
    invoke-virtual {p4}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->a()V

    .line 614608
    return-void
.end method
