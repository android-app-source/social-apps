.class public Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2ZK;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile l:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;


# instance fields
.field private final c:LX/0tQ;

.field private final d:LX/3gi;

.field public final e:LX/19w;

.field private final f:LX/0TD;

.field public final g:LX/28W;

.field public final h:LX/16I;

.field public i:LX/0SG;

.field public j:J

.field public k:LX/BUA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 624713
    const-class v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->a:Ljava/lang/String;

    .line 624714
    const-class v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/28W;LX/0tQ;LX/19w;LX/3gi;LX/16I;LX/0TD;LX/0SG;)V
    .locals 0
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624716
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->g:LX/28W;

    .line 624717
    iput-object p2, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->c:LX/0tQ;

    .line 624718
    iput-object p4, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->d:LX/3gi;

    .line 624719
    iput-object p6, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->f:LX/0TD;

    .line 624720
    iput-object p5, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->h:LX/16I;

    .line 624721
    iput-object p7, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->i:LX/0SG;

    .line 624722
    iput-object p3, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->e:LX/19w;

    .line 624723
    iget-object p1, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->h:LX/16I;

    sget-object p2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance p3, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$1;

    invoke-direct {p3, p0}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$1;-><init>(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V

    invoke-virtual {p1, p2, p3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 624724
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;
    .locals 11

    .prologue
    .line 624725
    sget-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->l:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    if-nez v0, :cond_1

    .line 624726
    const-class v1, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    monitor-enter v1

    .line 624727
    :try_start_0
    sget-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->l:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 624728
    if-eqz v2, :cond_0

    .line 624729
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 624730
    new-instance v3, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-static {v0}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v4

    check-cast v4, LX/28W;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v5

    check-cast v5, LX/0tQ;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v6

    check-cast v6, LX/19w;

    .line 624731
    new-instance v8, LX/3gi;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v7

    check-cast v7, LX/0sO;

    invoke-direct {v8, v7}, LX/3gi;-><init>(LX/0sO;)V

    .line 624732
    move-object v7, v8

    .line 624733
    check-cast v7, LX/3gi;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v8

    check-cast v8, LX/16I;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;-><init>(LX/28W;LX/0tQ;LX/19w;LX/3gi;LX/16I;LX/0TD;LX/0SG;)V

    .line 624734
    move-object v0, v3

    .line 624735
    sput-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->l:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624736
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 624737
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624738
    :cond_1
    sget-object v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->l:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    return-object v0

    .line 624739
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 624740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 624741
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->j:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->c:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->j()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 624742
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->f:LX/0TD;

    new-instance v1, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;

    invoke-direct {v1, p0}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;-><init>(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V

    const v2, -0xf1b0c3b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 624743
    :cond_0
    return-void
.end method

.method public final b()LX/2ZE;
    .locals 6

    .prologue
    .line 624744
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->k:LX/BUA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->e:LX/19w;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/19w;->a(ZZ)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 624745
    :cond_0
    const/4 v0, 0x0

    .line 624746
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/BUG;

    iget-object v1, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->k:LX/BUA;

    iget-object v2, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->c:LX/0tQ;

    iget-object v3, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->e:LX/19w;

    iget-object v4, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->d:LX/3gi;

    new-instance v5, LX/BUH;

    invoke-direct {v5, p0}, LX/BUH;-><init>(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V

    invoke-direct/range {v0 .. v5}, LX/BUG;-><init>(LX/BUA;LX/0tQ;LX/19w;LX/3gi;LX/BUH;)V

    goto :goto_0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624747
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->c:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->j()J

    move-result-wide v0

    return-wide v0
.end method
