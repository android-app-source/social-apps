.class public Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D4H;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/D66;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613715
    new-instance v0, LX/3cM;

    invoke-direct {v0}, LX/3cM;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613716
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613717
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    .line 613718
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    .line 613719
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;
    .locals 5

    .prologue
    .line 613720
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    monitor-enter v1

    .line 613721
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613722
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613723
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613724
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613725
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V

    .line 613726
    move-object v0, p0

    .line 613727
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613728
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613729
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613731
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 613732
    check-cast p2, LX/D4H;

    .line 613733
    iget-object v0, p2, LX/D4H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613734
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 613735
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 613736
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 613737
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    new-instance v2, LX/Anm;

    iget-object v3, p2, LX/D4H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-direct {v2, v3, v0}, LX/Anm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613738
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedBlingBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    new-instance v1, LX/82O;

    iget-object v2, p2, LX/D4H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/82O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613739
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 613740
    check-cast p1, LX/D4H;

    .line 613741
    iget-boolean v0, p1, LX/D4H;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/D4H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/D4H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613742
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 613743
    iget-object v1, p1, LX/D4H;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
