.class public Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/D5o;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

.field private final c:LX/0sV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613501
    new-instance v0, LX/3cI;

    invoke-direct {v0}, LX/3cI;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;LX/0sV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613521
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613522
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    .line 613523
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->c:LX/0sV;

    .line 613524
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;
    .locals 5

    .prologue
    .line 613510
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    monitor-enter v1

    .line 613511
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613512
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613513
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613514
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613515
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v4

    check-cast v4, LX/0sV;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;LX/0sV;)V

    .line 613516
    move-object v0, p0

    .line 613517
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613518
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613519
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613525
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 613507
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 613508
    const v0, 0x7f0d02c4

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->b:Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderTitlePartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613509
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 613502
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v0, 0x0

    .line 613503
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedSocialContextHeaderPartDefinition;->c:LX/0sV;

    iget-boolean v1, v1, LX/0sV;->k:Z

    if-nez v1, :cond_1

    .line 613504
    :cond_0
    :goto_0
    return v0

    .line 613505
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 613506
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
