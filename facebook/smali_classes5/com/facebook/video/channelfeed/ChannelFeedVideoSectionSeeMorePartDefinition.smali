.class public Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613802
    const v0, 0x7f030276

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613798
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613799
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 613800
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->c:Landroid/content/res/Resources;

    .line 613801
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;
    .locals 5

    .prologue
    .line 613783
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    monitor-enter v1

    .line 613784
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613785
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613786
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613787
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613788
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 613789
    move-object v0, p0

    .line 613790
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613791
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613792
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613793
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613797
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 613803
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/D4U;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 613804
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613805
    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    .line 613806
    iget-boolean v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;->c:Z

    move v1, v1

    .line 613807
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->c:Landroid/content/res/Resources;

    const v2, 0x7f081a66

    new-array v3, v3, [Ljava/lang/Object;

    .line 613808
    iget-object v4, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;->a:Ljava/lang/String;

    move-object v4, v4

    .line 613809
    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 613810
    :goto_0
    const v2, 0x7f0d02c4

    iget-object v3, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613811
    new-instance v1, LX/D6E;

    invoke-direct {v1, p0, p3, v0}, LX/D6E;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;LX/D4U;Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;)V

    return-object v1

    .line 613812
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMorePartDefinition;->c:Landroid/content/res/Resources;

    const v2, 0x7f081a65

    new-array v3, v3, [Ljava/lang/Object;

    .line 613813
    iget-object v4, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;->a:Ljava/lang/String;

    move-object v4, v4

    .line 613814
    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6a87c397

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613794
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 613795
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 613796
    const/16 v1, 0x1f

    const v2, -0x549bea92

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613782
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 613780
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 613781
    return-void
.end method
