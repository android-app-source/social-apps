.class public Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613713
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613709
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613710
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    .line 613711
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    .line 613712
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;
    .locals 5

    .prologue
    .line 613698
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    monitor-enter v1

    .line 613699
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613700
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613703
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;)V

    .line 613704
    move-object v0, p0

    .line 613705
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613706
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613707
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613714
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 613686
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613687
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613688
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPlaysBlingBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    new-instance v1, LX/82O;

    sget-object v2, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/82O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613689
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613690
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 613691
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, v2

    .line 613692
    :cond_0
    :goto_0
    move-object v0, v1

    .line 613693
    invoke-static {v0}, LX/C66;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0

    .line 613694
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v1, 0x0

    move p0, v1

    :goto_1
    if-ge p0, p2, :cond_2

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 613695
    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 613696
    add-int/lit8 v1, p0, 0x1

    move p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 613697
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x122c07f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613680
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 613681
    const/4 v1, 0x1

    .line 613682
    iput-boolean v1, p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->s:Z

    .line 613683
    if-eqz p2, :cond_0

    .line 613684
    invoke-virtual {p4, p2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setViewsCountClickListener(Landroid/view/View$OnClickListener;)V

    .line 613685
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x5e23289a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613677
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613678
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613679
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 613674
    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 613675
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setViewsCountClickListener(Landroid/view/View$OnClickListener;)V

    .line 613676
    return-void
.end method
