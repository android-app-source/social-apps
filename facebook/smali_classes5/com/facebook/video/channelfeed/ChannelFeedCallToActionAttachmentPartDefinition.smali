.class public Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/D4K;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

.field private final c:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

.field private final d:LX/38q;

.field private final e:LX/D4I;

.field private final f:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

.field private final g:LX/0sV;

.field private final h:LX/2ye;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613671
    new-instance v0, LX/3cL;

    invoke-direct {v0}, LX/3cL;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;LX/38q;LX/D4I;Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;LX/0sV;LX/2ye;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613662
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613663
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 613664
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    .line 613665
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->d:LX/38q;

    .line 613666
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->e:LX/D4I;

    .line 613667
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    .line 613668
    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->g:LX/0sV;

    .line 613669
    iput-object p7, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->h:LX/2ye;

    .line 613670
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 613651
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    monitor-enter v1

    .line 613652
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613653
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613656
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    const-class v6, LX/38q;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/38q;

    invoke-static {v0}, LX/D4I;->a(LX/0QB;)LX/D4I;

    move-result-object v7

    check-cast v7, LX/D4I;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v9

    check-cast v9, LX/0sV;

    invoke-static {v0}, LX/2ye;->a(LX/0QB;)LX/2ye;

    move-result-object v10

    check-cast v10, LX/2ye;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;LX/38q;LX/D4I;Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;LX/0sV;LX/2ye;)V

    .line 613657
    move-object v0, v3

    .line 613658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 613650
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 613626
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 613627
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 613628
    if-nez v1, :cond_0

    .line 613629
    :goto_0
    return-object v0

    .line 613630
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->e:LX/D4I;

    invoke-virtual {v1, p2}, LX/D4I;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/D4K;

    move-result-object v1

    .line 613631
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v2, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613632
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    new-instance v3, LX/C8Z;

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->h:LX/2ye;

    sget-object v5, LX/C8b;->ATTACHMENT:LX/C8b;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->d:LX/38q;

    sget-object v7, LX/C8b;->ATTACHMENT:LX/C8b;

    invoke-virtual {v6, v7}, LX/38q;->a(LX/C8b;)LX/C8c;

    move-result-object v6

    invoke-direct {v3, p2, v4, v5, v6}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613633
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    new-instance v3, LX/C8Z;

    new-instance v4, LX/D4J;

    invoke-direct {v4, p0}, LX/D4J;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;)V

    sget-object v5, LX/C8b;->CTA_BUTTON:LX/C8b;

    iget-object v6, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->d:LX/38q;

    sget-object v7, LX/C8b;->CTA_BUTTON:LX/C8b;

    invoke-virtual {v6, v7}, LX/38q;->a(LX/C8b;)LX/C8c;

    move-result-object v6

    invoke-direct {v3, p2, v4, v5, v6}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613634
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedCallToActionButtonPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move-object v0, v1

    .line 613635
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x106acb59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613644
    check-cast p2, LX/D4K;

    .line 613645
    if-nez p2, :cond_0

    .line 613646
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x4187eae4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v1, p4

    .line 613647
    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/D4K;->a:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 613648
    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/D4K;->b:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    .line 613649
    check-cast p4, LX/3Vx;

    iget-object v1, p2, LX/D4K;->c:Landroid/text/Spannable;

    invoke-interface {p4, v1}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 613638
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 613639
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedCallToActionAttachmentPartDefinition;->g:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->s:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 613640
    :goto_0
    return v0

    .line 613641
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613642
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 613643
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2v7;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 613636
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 613637
    return-void
.end method
