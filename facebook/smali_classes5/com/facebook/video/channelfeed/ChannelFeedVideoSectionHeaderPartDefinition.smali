.class public Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/D6D;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/D4i;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613777
    new-instance v0, LX/3cN;

    invoke-direct {v0}, LX/3cN;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/D4i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613746
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613747
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 613748
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->b:LX/D4i;

    .line 613749
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;
    .locals 5

    .prologue
    .line 613766
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    monitor-enter v1

    .line 613767
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613768
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613769
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613770
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613771
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/D4i;->a(LX/0QB;)LX/D4i;

    move-result-object v4

    check-cast v4, LX/D4i;

    invoke-direct {p0, v3, v4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/D4i;)V

    .line 613772
    move-object v0, p0

    .line 613773
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613774
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613775
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613765
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 613758
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613759
    const v1, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 613760
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613761
    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;

    .line 613762
    iget-object p0, v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeader;->a:Ljava/lang/String;

    move-object v0, p0

    .line 613763
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613764
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1859db88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613755
    check-cast p4, LX/D6D;

    .line 613756
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->b:LX/D4i;

    invoke-virtual {v1, p4}, LX/D4i;->a(Landroid/view/View;)V

    .line 613757
    const/16 v1, 0x1f

    const v2, 0x4f39b55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613754
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 613750
    check-cast p4, LX/D6D;

    .line 613751
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionHeaderPartDefinition;->b:LX/D4i;

    .line 613752
    iget-object p0, v0, LX/D4i;->d:Ljava/util/Set;

    invoke-interface {p0, p4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 613753
    return-void
.end method
