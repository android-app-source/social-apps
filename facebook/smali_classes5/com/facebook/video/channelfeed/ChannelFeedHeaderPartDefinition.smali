.class public Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D4x;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:LX/1VE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613469
    const v0, 0x7f030268

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;LX/1VE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613470
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613471
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    .line 613472
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    .line 613473
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    .line 613474
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 613475
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    .line 613476
    iput-object p6, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->g:LX/1VE;

    .line 613477
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;
    .locals 10

    .prologue
    .line 613478
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    monitor-enter v1

    .line 613479
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613480
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613481
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613482
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613483
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v9

    check-cast v9, LX/1VE;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;LX/1VE;)V

    .line 613484
    move-object v0, v3

    .line 613485
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613486
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613487
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613489
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 613490
    check-cast p2, LX/D4x;

    check-cast p3, LX/1Pn;

    .line 613491
    iget-object v0, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613492
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->g:LX/1VE;

    iget-object v2, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/1VE;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    .line 613493
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->g:LX/1VE;

    iget-object v3, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, LX/1VE;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    .line 613494
    const v3, 0x7f0d160d

    iget-object v4, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    new-instance v5, LX/BsB;

    iget-object v6, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v5, v6, v2, v1}, LX/BsB;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613495
    const v1, 0x7f0d1616

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    new-instance v3, LX/D52;

    iget-object v4, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v5, p2, LX/D4x;->b:Z

    invoke-direct {v3, v4, v5}, LX/D52;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613496
    const v1, 0x7f0d1615

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedHeaderSubtitleWithLayoutPartDefinition;

    new-instance v3, LX/D4y;

    iget-object v4, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, LX/D4y;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613497
    const v1, 0x7f0d0904

    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/D4w;

    invoke-direct {v3, p0, p3, v0}, LX/D4w;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613498
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    new-instance v1, LX/82O;

    iget-object v2, p2, LX/D4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/82O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613499
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613500
    const/4 v0, 0x1

    return v0
.end method
