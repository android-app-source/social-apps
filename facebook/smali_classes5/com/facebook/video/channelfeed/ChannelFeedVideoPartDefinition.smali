.class public Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/channelfeed/HasChannelFeedParams;",
        ":",
        "LX/1Pr;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pe;",
        ":",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/D65;",
        "Lcom/facebook/video/engine/VideoPlayerParams;",
        "TE;",
        "LX/D5z;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:LX/1Cz;

.field private static m:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/19J;

.field private final d:LX/19M;

.field private final e:LX/1Kd;

.field private final f:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

.field private final g:Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

.field private final h:LX/2mZ;

.field private final i:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/04J;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Gg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613593
    new-instance v0, LX/3cJ;

    invoke-direct {v0}, LX/3cJ;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;LX/1AM;LX/0bH;LX/04J;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;",
            "Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;",
            "LX/2mZ;",
            "Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;",
            ">;",
            "LX/1AM;",
            "LX/0bH;",
            "LX/04J;",
            "LX/0Ot",
            "<",
            "LX/3Gg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613577
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613578
    new-instance v0, LX/D62;

    invoke-direct {v0, p0}, LX/D62;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->c:LX/19J;

    .line 613579
    new-instance v0, LX/D63;

    invoke-direct {v0, p0}, LX/D63;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->d:LX/19M;

    .line 613580
    new-instance v0, LX/D64;

    invoke-direct {v0, p0}, LX/D64;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->e:LX/1Kd;

    .line 613581
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a:Ljava/util/List;

    .line 613582
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    .line 613583
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->g:Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    .line 613584
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->h:LX/2mZ;

    .line 613585
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->i:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    .line 613586
    iput-object p5, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->j:LX/0Ot;

    .line 613587
    iput-object p8, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->k:LX/04J;

    .line 613588
    iput-object p9, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->l:LX/0Ot;

    .line 613589
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->c:LX/19J;

    invoke-virtual {p6, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 613590
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->d:LX/19M;

    invoke-virtual {p6, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 613591
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->e:LX/1Kd;

    invoke-virtual {p7, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 613592
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;
    .locals 13

    .prologue
    .line 613566
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    monitor-enter v1

    .line 613567
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613568
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613569
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613570
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613571
    new-instance v3, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    const-class v6, LX/2mZ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    const/16 v8, 0x846

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v9

    check-cast v9, LX/1AM;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static {v0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v11

    check-cast v11, LX/04J;

    const/16 v12, 0x12e8

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;LX/1AM;LX/0bH;LX/04J;LX/0Ot;)V

    .line 613572
    move-object v0, v3

    .line 613573
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613574
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613575
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613576
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613565
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 613545
    check-cast p2, LX/D65;

    check-cast p3, LX/D4U;

    .line 613546
    iget-object v0, p2, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613547
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 613548
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 613549
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 613550
    iget-object v0, p2, LX/D65;->b:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 613551
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    .line 613552
    if-eqz v0, :cond_0

    .line 613553
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->k:LX/04J;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/0JS;->CHANNEL_ID:LX/0JS;

    iget-object v3, v3, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 613554
    :cond_0
    iget-object v0, p3, LX/D4U;->w:LX/3Qw;

    move-object v0, v0

    .line 613555
    if-eqz v0, :cond_1

    iget-object v2, v0, LX/3Qw;->o:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 613556
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->k:LX/04J;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v4, v4, LX/0JS;->value:Ljava/lang/String;

    iget-object v0, v0, LX/3Qw;->o:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 613557
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Gg;

    .line 613558
    iget-object v2, v0, LX/3Gg;->a:LX/0ad;

    sget-short v3, LX/0ws;->dG:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 613559
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->h:LX/2mZ;

    iget-object v3, p2, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3, v1}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, LX/3Im;->a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    .line 613560
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->f:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPartDefinition;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613561
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->i:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    new-instance v3, LX/36r;

    iget-object v4, p2, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, LX/36r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613562
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/3J3;

    iget-object v4, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/3J3;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613563
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->g:Lcom/facebook/video/channelfeed/ChannelFeedImpressionLoggerPartDefinition;

    new-instance v3, LX/D57;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p2, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-direct {v3, v1, v4}, LX/D57;-><init>(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613564
    return-object v2
.end method

.method public a(LX/D65;Lcom/facebook/video/engine/VideoPlayerParams;LX/D4U;LX/D5z;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D65;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "TE;",
            "LX/D5z;",
            ")V"
        }
    .end annotation

    .prologue
    .line 613528
    move-object v0, p3

    check-cast v0, LX/7ze;

    iget-object v1, p1, LX/D65;->b:LX/D5r;

    .line 613529
    iget-object v2, v1, LX/D5r;->b:LX/D60;

    move-object v1, v2

    .line 613530
    invoke-interface {v0, p4, v1}, LX/7ze;->a(Landroid/view/View;LX/2oV;)V

    .line 613531
    invoke-virtual {p4}, LX/D5z;->a()V

    .line 613532
    invoke-virtual {p4}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    move-object v0, p3

    .line 613533
    check-cast v0, LX/D4U;

    .line 613534
    iget-object v2, v0, LX/D4U;->q:LX/04D;

    move-object v0, v2

    .line 613535
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 613536
    iget-object v0, p1, LX/D65;->b:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->a()I

    move-result v2

    iget-object v3, p1, LX/D65;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p1, LX/D65;->b:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/D4U;

    move-object v0, p4

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, LX/D5z;->a(Lcom/facebook/video/engine/VideoPlayerParams;ILcom/facebook/feed/rows/core/props/FeedProps;LX/2oO;LX/D4U;)V

    .line 613537
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 613538
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x21ab0a01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613544
    check-cast p1, LX/D65;

    check-cast p2, Lcom/facebook/video/engine/VideoPlayerParams;

    check-cast p3, LX/D4U;

    check-cast p4, LX/D5z;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a(LX/D65;Lcom/facebook/video/engine/VideoPlayerParams;LX/D4U;LX/D5z;)V

    const/16 v1, 0x1f

    const v2, 0x551d4fe3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public a(LX/D65;)Z
    .locals 1

    .prologue
    .line 613543
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613542
    check-cast p1, LX/D65;

    invoke-virtual {p0, p1}, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a(LX/D65;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 613539
    check-cast p4, LX/D5z;

    .line 613540
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoPartDefinition;->a:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 613541
    return-void
.end method
