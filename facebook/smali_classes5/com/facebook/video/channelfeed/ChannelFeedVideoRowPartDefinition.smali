.class public Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        ":",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;:",
        "LX/1Pe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/D5z;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/0sV;

.field private final c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2mq;

.field private final e:LX/2mn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613623
    new-instance v0, LX/3cK;

    invoke-direct {v0}, LX/3cK;-><init>()V

    sput-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0sV;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;LX/2mq;LX/2mn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613617
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613618
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->b:LX/0sV;

    .line 613619
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 613620
    iput-object p3, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->d:LX/2mq;

    .line 613621
    iput-object p4, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->e:LX/2mn;

    .line 613622
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;
    .locals 7

    .prologue
    .line 613606
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    monitor-enter v1

    .line 613607
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613608
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613609
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613610
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613611
    new-instance p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v5

    check-cast v5, LX/2mq;

    invoke-static {v0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v6

    check-cast v6, LX/2mn;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;-><init>(LX/0sV;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;LX/2mq;LX/2mn;)V

    .line 613612
    move-object v0, p0

    .line 613613
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613614
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613615
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613596
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 613598
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 613599
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->d:LX/2mq;

    .line 613600
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 613601
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v0

    .line 613602
    iget-object v1, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->e:LX/2mn;

    invoke-virtual {v1, p2, v0}, LX/2mn;->a(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 613603
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 613604
    iget-object v2, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v3, LX/3EE;

    const/4 v4, -0x1

    new-instance v5, LX/CDf;

    iget v6, v0, LX/3FO;->a:I

    iget v0, v0, LX/3FO;->b:I

    const/4 v7, 0x0

    invoke-direct {v5, v6, v0, v7}, LX/CDf;-><init>(III)V

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-direct {v3, p2, v4, v0, v1}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613605
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613597
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedVideoRowPartDefinition;->b:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->o:Z

    return v0
.end method
