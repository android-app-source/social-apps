.class public Lcom/facebook/device/resourcemonitor/MonitoredProcessDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 674561
    const-class v0, Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    new-instance v1, Lcom/facebook/device/resourcemonitor/MonitoredProcessDeserializer;

    invoke-direct {v1}, Lcom/facebook/device/resourcemonitor/MonitoredProcessDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 674562
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 674560
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 674559
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->fromString(Ljava/lang/String;)Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    move-result-object v0

    return-object v0
.end method
