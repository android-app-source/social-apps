.class public Lcom/facebook/vault/service/VaultConnectivityChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573753
    const-class v0, Lcom/facebook/vault/service/VaultConnectivityChangeReceiver;

    sput-object v0, Lcom/facebook/vault/service/VaultConnectivityChangeReceiver;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 573754
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 573755
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x78df6ae8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 573756
    new-instance v1, LX/ERg;

    invoke-direct {v1, p1}, LX/ERg;-><init>(Landroid/content/Context;)V

    .line 573757
    invoke-static {v1}, LX/ERg;->a(LX/ERg;)LX/0Uq;

    move-result-object v2

    invoke-virtual {v2}, LX/0Uq;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 573758
    invoke-static {v1}, LX/ERg;->b(LX/ERg;)LX/2TK;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/2TK;->c(I)V

    .line 573759
    invoke-static {v1}, LX/ERg;->c(LX/ERg;)LX/ERr;

    move-result-object v1

    invoke-virtual {v1}, LX/ERr;->b()V

    .line 573760
    :cond_0
    const/16 v1, 0x27

    const v2, -0xee589ae

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
