.class public Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BbY;",
        "TE;",
        "LX/Bc1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/Bc1;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/8xw;

.field public final g:LX/8y8;

.field public final h:LX/189;

.field public final i:LX/0bH;

.field private final j:LX/3BL;

.field public final k:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 585962
    new-instance v0, LX/3UQ;

    invoke-direct {v0}, LX/3UQ;-><init>()V

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xw;LX/8y8;LX/189;LX/0bH;LX/3BL;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585963
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 585964
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    .line 585965
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 585966
    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    .line 585967
    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 585968
    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->f:LX/8xw;

    .line 585969
    iput-object p6, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->g:LX/8y8;

    .line 585970
    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->h:LX/189;

    .line 585971
    iput-object p8, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->i:LX/0bH;

    .line 585972
    iput-object p9, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->j:LX/3BL;

    .line 585973
    iput-object p10, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->k:LX/0kL;

    .line 585974
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;
    .locals 14

    .prologue
    .line 585975
    const-class v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    monitor-enter v1

    .line 585976
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 585977
    sput-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 585978
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585979
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 585980
    new-instance v3, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/8xw;->b(LX/0QB;)LX/8xw;

    move-result-object v8

    check-cast v8, LX/8xw;

    .line 585981
    new-instance v11, LX/8y8;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-direct {v11, v9, v10}, LX/8y8;-><init>(LX/0tX;LX/1Ck;)V

    .line 585982
    move-object v9, v11

    .line 585983
    check-cast v9, LX/8y8;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v10

    check-cast v10, LX/189;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v11

    check-cast v11, LX/0bH;

    invoke-static {v0}, LX/3BL;->a(LX/0QB;)LX/3BL;

    move-result-object v12

    check-cast v12, LX/3BL;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v13

    check-cast v13, LX/0kL;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xw;LX/8y8;LX/189;LX/0bH;LX/3BL;LX/0kL;)V

    .line 585984
    move-object v0, v3

    .line 585985
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 585986
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585987
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 585988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Bc1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 585989
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 585990
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 585991
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 585992
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 585993
    const v1, 0x3ff33333    # 1.9f

    invoke-static {v0, v1}, LX/3BL;->a(IF)I

    move-result v1

    .line 585994
    const v2, 0x7f0d1cd0

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    new-instance v4, LX/Bbc;

    invoke-direct {v4, p2, v0, v1}, LX/Bbc;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585995
    const v2, 0x7f0d2d13

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 585996
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 585997
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 585998
    :goto_0
    new-instance p3, LX/BbV;

    invoke-direct {p3, p0, v5, v4, p2}, LX/BbV;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v4, p3

    .line 585999
    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 586000
    const v2, 0x7f0d2d14

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 586001
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 586002
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 586003
    :goto_1
    new-instance p3, LX/BbX;

    invoke-direct {p3, p0, v5, v4}, LX/BbX;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    move-object v4, p3

    .line 586004
    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 586005
    new-instance v2, LX/BbY;

    invoke-direct {v2, v0, v1}, LX/BbY;-><init>(II)V

    return-object v2

    .line 586006
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 586007
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xfc76534

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586008
    check-cast p2, LX/BbY;

    check-cast p4, LX/Bc1;

    .line 586009
    iget v1, p2, LX/BbY;->a:I

    iget v2, p2, LX/BbY;->b:I

    .line 586010
    iget-object p0, p4, LX/Bc1;->a:Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 586011
    if-nez p0, :cond_1

    .line 586012
    new-instance p0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 586013
    iget-object p2, p4, LX/Bc1;->a:Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;

    invoke-virtual {p2, p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 586014
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x1250b1b1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 586015
    :cond_1
    iget p2, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne p2, v1, :cond_2

    iget p2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq p2, v2, :cond_0

    .line 586016
    :cond_2
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 586017
    iput v2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 586018
    iget-object p2, p4, LX/Bc1;->a:Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;

    invoke-virtual {p2, p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586019
    const/4 v0, 0x1

    return v0
.end method
