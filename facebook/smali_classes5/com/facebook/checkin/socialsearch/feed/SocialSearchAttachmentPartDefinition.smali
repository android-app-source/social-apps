.class public Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BbQ;",
        "TE;",
        "LX/Bc0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/Bc0;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field private static u:LX/0Xm;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field public final d:Landroid/content/Context;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

.field private final g:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

.field private final h:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

.field private final i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final j:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field public final k:LX/8y6;

.field private final l:LX/Bba;

.field private final m:LX/3BN;

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/0hy;

.field public final p:LX/189;

.field public final q:LX/0bH;

.field public final r:LX/0kL;

.field public final s:LX/03V;

.field public final t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 585958
    new-instance v0, LX/3UP;

    invoke-direct {v0}, LX/3UP;-><init>()V

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a:LX/1Cz;

    .line 585959
    const-class v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/8y6;LX/Bba;LX/3BN;LX/189;LX/0bH;Lcom/facebook/content/SecureContextHelper;LX/0hy;LX/0kL;LX/03V;Ljava/lang/Boolean;)V
    .locals 2
    .param p18    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585937
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 585938
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->c:Landroid/content/res/Resources;

    .line 585939
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->d:Landroid/content/Context;

    .line 585940
    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 585941
    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->f:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    .line 585942
    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->g:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    .line 585943
    iput-object p6, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->h:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    .line 585944
    iput-object p8, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 585945
    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 585946
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    .line 585947
    iput-object p9, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->k:LX/8y6;

    .line 585948
    iput-object p10, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->l:LX/Bba;

    .line 585949
    iput-object p11, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->m:LX/3BN;

    .line 585950
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->o:LX/0hy;

    .line 585951
    iput-object p12, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->p:LX/189;

    .line 585952
    iput-object p13, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->q:LX/0bH;

    .line 585953
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->r:LX/0kL;

    .line 585954
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->s:LX/03V;

    .line 585955
    if-nez p18, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->t:Z

    .line 585956
    return-void

    .line 585957
    :cond_0
    invoke-virtual/range {p18 .. p18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 585839
    const-class v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    monitor-enter v1

    .line 585840
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->u:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 585841
    sput-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->u:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 585842
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585843
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 585844
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585845
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 585846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;LX/1Ps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 585900
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->l:LX/Bba;

    const/4 v2, 0x0

    .line 585901
    iget-object v1, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 585902
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 585903
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pl()Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pl()Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/Bba;->d:LX/0ad;

    sget-short v3, LX/5HH;->g:S

    invoke-interface {v1, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 585904
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->m:LX/3BN;

    const/4 v3, 0x0

    .line 585905
    iget-object v2, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 585906
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 585907
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->pZ()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, LX/3BN;->d:LX/0ad;

    sget-short v4, LX/5HH;->n:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 585908
    const v3, 0x7f0d2b08

    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-nez v1, :cond_0

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v3, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585909
    if-eqz v1, :cond_3

    .line 585910
    const v0, 0x7f0d2b08

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->h:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    new-instance v2, LX/BbR;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->l:LX/Bba;

    .line 585911
    new-instance v4, LX/BbZ;

    invoke-direct {v4, v3, p3}, LX/BbZ;-><init>(LX/Bba;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v3, v4

    .line 585912
    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->l:LX/Bba;

    .line 585913
    iget-object v5, v4, LX/Bba;->f:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    const v5, 0x7f0829db

    .line 585914
    :goto_3
    iget-object p2, v4, LX/Bba;->b:Landroid/content/Context;

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 585915
    iget-object v5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->l:LX/Bba;

    .line 585916
    invoke-static {p3}, LX/BbT;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v7

    .line 585917
    if-nez v7, :cond_8

    .line 585918
    iget-object v6, v5, LX/Bba;->f:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_7

    const v6, 0x7f0829d7

    .line 585919
    :goto_4
    iget-object v7, v5, LX/Bba;->b:Landroid/content/Context;

    invoke-virtual {v7, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 585920
    :goto_5
    move-object v5, v6

    .line 585921
    invoke-direct {v2, p3, v3, v4, v5}, LX/BbR;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585922
    :cond_1
    :goto_6
    return-void

    .line 585923
    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    .line 585924
    :cond_3
    if-eqz v2, :cond_1

    .line 585925
    const v0, 0x7f0d2b08

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->h:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    new-instance v2, LX/BbR;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->m:LX/3BN;

    check-cast p2, LX/1Po;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    .line 585926
    const-string v5, "rexUpsellOnRexStory"

    invoke-static {v3, v4, v5}, LX/3BN;->a(LX/3BN;LX/1PT;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    move-object v3, v5

    .line 585927
    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->m:LX/3BN;

    .line 585928
    iget-object v5, v4, LX/3BN;->a:Landroid/content/Context;

    const v6, 0x7f0829dd

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 585929
    iget-object v5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->m:LX/3BN;

    .line 585930
    iget-object v6, v5, LX/3BN;->a:Landroid/content/Context;

    const v7, 0x7f0829dc

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 585931
    invoke-direct {v2, p3, v3, v4, v5}, LX/BbR;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_6

    :cond_4
    move v1, v2

    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto/16 :goto_1

    .line 585932
    :cond_6
    const v5, 0x7f0829da

    goto :goto_3

    .line 585933
    :cond_7
    const v6, 0x7f0829d6

    goto :goto_4

    .line 585934
    :cond_8
    iget-object v6, v5, LX/Bba;->f:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_9

    const v6, 0x7f0829d9

    .line 585935
    :goto_7
    iget-object v8, v5, LX/Bba;->b:Landroid/content/Context;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v7, p0, p2

    invoke-virtual {v8, v6, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    goto :goto_5

    .line 585936
    :cond_9
    const v6, 0x7f0829d8

    goto :goto_7
.end method

.method public static a(LX/1Ps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 585898
    check-cast p0, LX/1Po;

    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    .line 585899
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;
    .locals 20

    .prologue
    .line 585896
    new-instance v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static/range {p0 .. p0}, LX/8y6;->a(LX/0QB;)LX/8y6;

    move-result-object v10

    check-cast v10, LX/8y6;

    invoke-static/range {p0 .. p0}, LX/Bba;->a(LX/0QB;)LX/Bba;

    move-result-object v11

    check-cast v11, LX/Bba;

    invoke-static/range {p0 .. p0}, LX/3BN;->a(LX/0QB;)LX/3BN;

    move-result-object v12

    check-cast v12, LX/3BN;

    invoke-static/range {p0 .. p0}, LX/189;->a(LX/0QB;)LX/189;

    move-result-object v13

    check-cast v13, LX/189;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v14

    check-cast v14, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v15

    check-cast v15, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v16

    check-cast v16, LX/0hy;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v17

    check-cast v17, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-direct/range {v1 .. v19}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/8y6;LX/Bba;LX/3BN;LX/189;LX/0bH;Lcom/facebook/content/SecureContextHelper;LX/0hy;LX/0kL;LX/03V;Ljava/lang/Boolean;)V

    .line 585897
    return-object v1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;)Z
    .locals 1

    .prologue
    .line 585895
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Bc0;",
            ">;"
        }
    .end annotation

    .prologue
    .line 585894
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 585863
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 585864
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 585865
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 585866
    int-to-float v0, v2

    const/high16 v1, 0x40200000    # 2.5f

    div-float/2addr v0, v1

    float-to-int v3, v0

    .line 585867
    const v0, 0x7f0d1cd0

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->f:Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    new-instance v4, LX/Bbc;

    invoke-direct {v4, p2, v2, v3}, LX/Bbc;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v0, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585868
    const v0, 0x7f0d2d03

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->g:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585869
    invoke-static {p3}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a(LX/1Ps;)Z

    move-result v0

    .line 585870
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 585871
    if-eqz v0, :cond_1

    .line 585872
    new-instance v0, LX/BbP;

    invoke-direct {v0, p0, p2}, LX/BbP;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v0

    .line 585873
    :goto_0
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 585874
    invoke-static {p2}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 585875
    if-eqz v0, :cond_0

    .line 585876
    const v0, 0x7f0d2d02

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 585877
    new-instance v4, LX/BbN;

    invoke-direct {v4, p0, p2}, LX/BbN;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v4, v4

    .line 585878
    invoke-interface {p1, v0, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 585879
    :cond_0
    invoke-direct {p0, p1, p3, p2}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a(LX/1aD;LX/1Ps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 585880
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 585881
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 585882
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    .line 585883
    :goto_1
    invoke-static {p2}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, LX/Bbz;

    const/high16 v4, 0x55000000

    invoke-direct {v1, v2, v3, v4}, LX/Bbz;-><init>(III)V

    .line 585884
    :goto_2
    new-instance v2, LX/BbQ;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;)Z

    move-result v0

    invoke-direct {v2, v0, v1}, LX/BbQ;-><init>(ZLandroid/graphics/drawable/Drawable;)V

    return-object v2

    .line 585885
    :cond_1
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 585886
    iput-object v4, v0, LX/89k;->b:Ljava/lang/String;

    .line 585887
    move-object v0, v0

    .line 585888
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 585889
    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->o:LX/0hy;

    invoke-interface {v4, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 585890
    new-instance v4, LX/BbO;

    invoke-direct {v4, p0, v0}, LX/BbO;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;Landroid/content/Intent;)V

    move-object v0, v4

    .line 585891
    goto :goto_0

    .line 585892
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 585893
    :cond_3
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a0101

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x60f5042e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 585852
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/BbQ;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/Bc0;

    .line 585853
    iget-boolean v1, p2, LX/BbQ;->a:Z

    if-nez v1, :cond_0

    .line 585854
    iget-object v1, p2, LX/BbQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, LX/Bc0;->setMapForeground(Landroid/graphics/drawable/Drawable;)V

    .line 585855
    :goto_0
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 585856
    iget-object p2, p4, LX/Bc0;->b:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 p1, 0x0

    :goto_1
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 585857
    invoke-static {p3}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a(LX/1Ps;)Z

    move-result v1

    .line 585858
    iget-object p2, p4, LX/Bc0;->c:Landroid/view/View;

    if-eqz v1, :cond_2

    const/4 p1, 0x0

    :goto_2
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 585859
    const/16 v1, 0x1f

    const v2, -0x72c3047c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 585860
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/Bc0;->setMapForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 585861
    :cond_1
    const/16 p1, 0x8

    goto :goto_1

    .line 585862
    :cond_2
    const/16 p1, 0x8

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 585847
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 585848
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 585849
    if-eqz v0, :cond_0

    .line 585850
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 585851
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
