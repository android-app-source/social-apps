.class public Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile r:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;


# instance fields
.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final c:LX/11M;

.field private final d:LX/0aG;

.field public final e:LX/03V;

.field private final f:LX/0SG;

.field private final g:LX/0lC;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/17Q;

.field public l:LX/0Zb;

.field private m:LX/1BA;

.field private final n:LX/0Sh;

.field public o:Ljava/lang/String;

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 638026
    const-class v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/11M;LX/0aG;LX/03V;LX/0SG;LX/0lC;LX/17Q;LX/0Zb;LX/1BA;LX/0Or;LX/0Or;LX/0Or;LX/0Sh;LX/0Ot;LX/0Uh;)V
    .locals 0
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingMobileAnalyticsFallbackEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsMobileAnalyticsImpressionsEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingWaterfallEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/11M;",
            "LX/0aG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0lC;",
            "LX/17Q;",
            "LX/0Zb;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638010
    iput-object p1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 638011
    iput-object p2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->c:LX/11M;

    .line 638012
    iput-object p3, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->d:LX/0aG;

    .line 638013
    iput-object p4, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->e:LX/03V;

    .line 638014
    iput-object p5, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->f:LX/0SG;

    .line 638015
    iput-object p6, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->g:LX/0lC;

    .line 638016
    iput-object p7, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->k:LX/17Q;

    .line 638017
    iput-object p8, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->l:LX/0Zb;

    .line 638018
    iput-object p9, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->m:LX/1BA;

    .line 638019
    iput-object p13, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->n:LX/0Sh;

    .line 638020
    iput-object p10, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->h:LX/0Or;

    .line 638021
    iput-object p11, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->j:LX/0Or;

    .line 638022
    iput-object p12, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->i:LX/0Or;

    .line 638023
    iput-object p14, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->p:LX/0Ot;

    .line 638024
    iput-object p15, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->q:LX/0Uh;

    .line 638025
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;
    .locals 3

    .prologue
    .line 637999
    sget-object v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->r:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    if-nez v0, :cond_1

    .line 638000
    const-class v1, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    monitor-enter v1

    .line 638001
    :try_start_0
    sget-object v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->r:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 638002
    if-eqz v2, :cond_0

    .line 638003
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->b(LX/0QB;)Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->r:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638004
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 638005
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 638006
    :cond_1
    sget-object v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->r:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    return-object v0

    .line 638007
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 638008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 637979
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 637980
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Ljava/net/URL;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 637981
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 637982
    new-instance v0, LX/3Et;

    iget-object v2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->c:LX/11M;

    invoke-direct {v0, v2}, LX/3Et;-><init>(LX/11M;)V

    .line 637983
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v3, "http.protocol.handle-redirects"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/params/BasicHttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 637984
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v2

    const-string v3, "SponsoredImpressionLogger"

    .line 637985
    iput-object v3, v2, LX/15E;->c:Ljava/lang/String;

    .line 637986
    move-object v2, v2

    .line 637987
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 637988
    iput-object v3, v2, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 637989
    move-object v2, v2

    .line 637990
    iput-object v1, v2, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 637991
    move-object v1, v2

    .line 637992
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 637993
    iput-object v2, v1, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 637994
    move-object v1, v1

    .line 637995
    iput-object v0, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 637996
    move-object v0, v1

    .line 637997
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 637998
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/SponsoredImpression;LX/162;LX/3EA;LX/1g1;)Ljava/util/List;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/SponsoredImpression;",
            "LX/162;",
            "LX/3EA;",
            "LX/1g1;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 637952
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->q:LX/0Uh;

    const/16 v2, 0x520

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 637953
    iget-object v2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 637954
    iget v2, p4, LX/1g1;->i:I

    move v6, v2

    .line 637955
    if-eqz v1, :cond_1

    .line 637956
    iget v1, p4, LX/1g1;->q:I

    move v1, v1

    .line 637957
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    :goto_0
    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/graphql/model/SponsoredImpression;->a(LX/3EA;LX/162;JILjava/lang/Integer;)Ljava/util/List;

    move-result-object v2

    .line 637958
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637959
    :cond_0
    :goto_1
    return-object v8

    :cond_1
    move-object v7, v8

    .line 637960
    goto :goto_0

    .line 637961
    :cond_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 637962
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 637963
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 637964
    invoke-static {p0, v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 637965
    invoke-direct {p0, v3, v1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_2

    .line 637966
    :catch_0
    move-exception v1

    .line 637967
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->o:Ljava/lang/String;

    .line 637968
    iget v2, p1, Lcom/facebook/graphql/model/BaseImpression;->l:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/BaseImpression;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_3
    move v2, v2

    .line 637969
    if-eqz v2, :cond_5

    sget-object v2, LX/3EA;->ORIGINAL:LX/3EA;

    if-ne p3, v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 637970
    if-eqz v2, :cond_0

    .line 637971
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 637972
    instance-of v2, v1, Ljava/net/MalformedURLException;

    if-nez v2, :cond_3

    instance-of v2, v1, Ljava/net/URISyntaxException;

    if-eqz v2, :cond_7

    .line 637973
    :cond_3
    const-string v2, "Failed to parse original impression url: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 637974
    :goto_5
    iget-object v3, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->e:LX/03V;

    sget-object v4, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 637975
    goto :goto_1

    .line 637976
    :cond_4
    invoke-direct {p0, v1, p4, v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(Ljava/util/List;LX/1g1;Z)V

    move-object v8, v1

    .line 637977
    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 637978
    :cond_7
    const-string v2, "Failed to request original impression url: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_5
.end method

.method private a(Ljava/util/List;LX/1g1;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1g1;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 637932
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 637933
    :try_start_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;Ljava/lang/String;)Ljava/lang/String;

    .line 637934
    if-eqz p3, :cond_0

    .line 637935
    sget-object v3, LX/3nB;->VIEWPING:LX/3nB;

    const/4 v4, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, p2, v3, v4, v0}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 637936
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 637937
    :catch_0
    move-exception v0

    .line 637938
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 637939
    sget-object v3, LX/3nB;->VIEWPING:LX/3nB;

    .line 637940
    iget-object v5, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->i:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 637941
    iget-object v5, p2, LX/1g1;->e:LX/162;

    move-object v5, v5

    .line 637942
    iget-object v6, p2, LX/1g1;->d:LX/3EA;

    move-object v6, v6

    .line 637943
    iget v7, p2, LX/1g1;->i:I

    move v10, v7

    .line 637944
    iget-object v7, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->p:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-virtual {v7}, LX/0kb;->d()Z

    move-result v11

    move-object v7, v3

    move v8, v2

    move-object v9, v0

    invoke-static/range {v5 .. v11}, LX/17Q;->a(LX/0lF;LX/3EA;LX/3nB;ZLjava/lang/String;IZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 637945
    iget-object v6, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->q:LX/0Uh;

    const/16 v7, 0x520

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 637946
    const-string v6, "image_load_state"

    .line 637947
    iget v7, p2, LX/1g1;->q:I

    move v7, v7

    .line 637948
    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 637949
    :cond_1
    iget-object v6, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->l:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 637950
    :cond_2
    goto :goto_1

    .line 637951
    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 638027
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638028
    const/4 v2, 0x0

    .line 638029
    :try_start_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 638030
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->g:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 638031
    const-string v3, "third_party_impression_logging_urls"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 638032
    const-string v4, "enable_debug_logging"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 638033
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638034
    invoke-virtual {v3}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638035
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 638036
    :catch_0
    move-exception v0

    .line 638037
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->e:LX/03V;

    sget-object v3, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to parse third party impressions :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    move v0, v2

    .line 638038
    :goto_1
    return v0

    .line 638039
    :cond_1
    if-eqz v4, :cond_0

    :try_start_1
    invoke-virtual {v4}, LX/0lF;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638040
    invoke-virtual {v4}, LX/0lF;->C()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 638041
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 638042
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V
    .locals 7
    .param p3    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 637921
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637922
    iget-object v0, p1, LX/1g1;->e:LX/162;

    move-object v0, v0

    .line 637923
    iget-object v1, p1, LX/1g1;->d:LX/3EA;

    move-object v1, v1

    .line 637924
    iget v2, p1, LX/1g1;->i:I

    move v5, v2

    .line 637925
    iget-object v2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v6

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, LX/17Q;->a(LX/0lF;LX/3EA;LX/3nB;ZLjava/lang/String;IZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 637926
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->q:LX/0Uh;

    const/16 v2, 0x520

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 637927
    const-string v1, "image_load_state"

    .line 637928
    iget v2, p1, LX/1g1;->q:I

    move v2, v2

    .line 637929
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 637930
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 637931
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 637905
    iget-object v0, p1, LX/1g1;->d:LX/3EA;

    move-object v3, v0

    .line 637906
    iget-object v0, p1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    move-object v0, v0

    .line 637907
    iget-object v1, p1, LX/1g1;->e:LX/162;

    move-object v4, v1

    .line 637908
    if-eqz v0, :cond_1

    check-cast v0, Lcom/facebook/graphql/model/SponsoredImpression;

    .line 637909
    iget v1, v0, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    move v0, v1

    .line 637910
    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 637911
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "ad_multi_impression"

    invoke-direct {v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_3

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "tracking"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "io"

    sget-object v5, LX/3EA;->ORIGINAL:LX/3EA;

    if-ne v3, v5, :cond_4

    const-string v5, "1"

    :goto_2
    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "isv"

    sget-object v5, LX/3EA;->VIEWABILITY:LX/3EA;

    if-ne v3, v5, :cond_5

    const-string v5, "1"

    :goto_3
    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "csp"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "is_exp"

    if-eqz p2, :cond_6

    const-string v5, "1"

    :goto_4
    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "imp_connection_state"

    if-eqz v0, :cond_7

    const-string v5, "1"

    :goto_5
    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v5

    .line 637912
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->q:LX/0Uh;

    const/16 v3, 0x520

    invoke-virtual {v1, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 637913
    const-string v1, "image_load_state"

    .line 637914
    iget v2, p1, LX/1g1;->q:I

    move v2, v2

    .line 637915
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 637916
    :cond_0
    if-eqz p2, :cond_2

    .line 637917
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 637918
    :goto_6
    const/4 v0, 0x1

    return v0

    :cond_1
    move v1, v2

    .line 637919
    goto :goto_0

    .line 637920
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_6

    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    const-string v5, "0"

    goto :goto_2

    :cond_5
    const-string v5, "0"

    goto :goto_3

    :cond_6
    const-string v5, "0"

    goto :goto_4

    :cond_7
    const-string v5, "0"

    goto :goto_5
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;
    .locals 17

    .prologue
    .line 637903
    new-instance v1, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v3

    check-cast v3, LX/11M;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v10

    check-cast v10, LX/1BA;

    const/16 v11, 0x14aa

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x149e

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x14af

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v14

    check-cast v14, LX/0Sh;

    const/16 v15, 0x2ca

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/11M;LX/0aG;LX/03V;LX/0SG;LX/0lC;LX/17Q;LX/0Zb;LX/1BA;LX/0Or;LX/0Or;LX/0Or;LX/0Sh;LX/0Ot;LX/0Uh;)V

    .line 637904
    return-object v1
.end method


# virtual methods
.method public final a(LX/1g1;)V
    .locals 3

    .prologue
    .line 637899
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->m:LX/1BA;

    const v1, 0x710004

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 637900
    sget-object v0, LX/3nB;->STARTING:LX/3nB;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V

    .line 637901
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->n:LX/0Sh;

    new-instance v1, LX/3nD;

    invoke-direct {v1, p0, p1}, LX/3nD;-><init>(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 637902
    return-void
.end method

.method public final b(LX/1g1;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 637860
    iget-object v0, p1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    move-object v0, v0

    .line 637861
    check-cast v0, Lcom/facebook/graphql/model/SponsoredImpression;

    .line 637862
    iget-object v1, p1, LX/1g1;->d:LX/3EA;

    move-object v3, v1

    .line 637863
    iget-object v1, p1, LX/1g1;->e:LX/162;

    move-object v4, v1

    .line 637864
    if-nez v0, :cond_2

    .line 637865
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 637866
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    .line 637867
    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    move-object v1, v0

    .line 637868
    :goto_0
    invoke-virtual {v1, v3}, Lcom/facebook/graphql/model/BaseImpression;->b(LX/3EA;)V

    .line 637869
    iget-boolean v0, v1, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    move v0, v0

    .line 637870
    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1g1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637871
    const/16 v0, 0x1

    .line 637872
    :goto_1
    iget-object v2, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/facebook/graphql/model/BaseImpression;->a(LX/3EA;ZJ)V

    .line 637873
    return v0

    .line 637874
    :cond_0
    invoke-direct {p0, v1, v4, v3, p1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(Lcom/facebook/graphql/model/SponsoredImpression;LX/162;LX/3EA;LX/1g1;)Ljava/util/List;

    move-result-object v0

    .line 637875
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final c(LX/1g1;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 637876
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 637877
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    .line 637878
    iget v1, p1, LX/1g1;->g:I

    move v1, v1

    .line 637879
    invoke-static {v0}, LX/18M;->d(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v2

    shl-int v3, v4, v1

    or-int/2addr v2, v3

    invoke-static {v0, v2}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;I)V

    .line 637880
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 637881
    :goto_0
    return-void

    .line 637882
    :cond_0
    new-instance v2, LX/2uB;

    invoke-direct {v2}, LX/2uB;-><init>()V

    move-object v2, v2

    .line 637883
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    .line 637884
    iput-object v3, v2, LX/2uB;->a:Ljava/lang/String;

    .line 637885
    move-object v2, v2

    .line 637886
    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 637887
    iput-object v0, v2, LX/2uB;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 637888
    move-object v0, v2

    .line 637889
    iput v1, v0, LX/2uB;->c:I

    .line 637890
    move-object v0, v0

    .line 637891
    iget-object v1, v0, LX/2uB;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 637892
    iget-object v1, v0, LX/2uB;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 637893
    new-instance v1, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;

    iget-object v2, v0, LX/2uB;->a:Ljava/lang/String;

    iget-object v3, v0, LX/2uB;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iget p1, v0, LX/2uB;->c:I

    invoke-direct {v1, v2, v3, p1}, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;I)V

    move-object v0, v1

    .line 637894
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 637895
    const-string v2, "markImpressionLoggedParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 637896
    iget-object v0, p0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->d:LX/0aG;

    const-string v2, "feed_mark_impression_logged"

    const v3, -0x1d65a8ff

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 637897
    invoke-interface {v0, v4}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    .line 637898
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method
