.class public Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1EF;",
        "LX/AkF;",
        "LX/1Ps;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/1Ua;

.field private final d:LX/AkJ;

.field private final e:LX/AkK;

.field public final f:LX/AkN;

.field public final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586633
    new-instance v0, LX/3Ud;

    invoke-direct {v0}, LX/3Ud;-><init>()V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/AkJ;LX/AkK;LX/AkN;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586634
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586635
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 586636
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->c:LX/1Ua;

    .line 586637
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->d:LX/AkJ;

    .line 586638
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->e:LX/AkK;

    .line 586639
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->f:LX/AkN;

    .line 586640
    const v0, 0x7f0b11ca

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->g:I

    .line 586641
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;
    .locals 9

    .prologue
    .line 586618
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;

    monitor-enter v1

    .line 586619
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586620
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586621
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586622
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586623
    new-instance v3, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v5, LX/AkJ;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AkJ;

    .line 586624
    new-instance v7, LX/AkK;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {v7, v6}, LX/AkK;-><init>(Landroid/content/res/Resources;)V

    .line 586625
    move-object v6, v7

    .line 586626
    check-cast v6, LX/AkK;

    invoke-static {v0}, LX/AkN;->b(LX/0QB;)LX/AkN;

    move-result-object v7

    check-cast v7, LX/AkN;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/AkJ;LX/AkK;LX/AkN;Landroid/content/res/Resources;)V

    .line 586627
    move-object v0, v3

    .line 586628
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586629
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586630
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586632
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 586610
    check-cast p2, LX/1EF;

    .line 586611
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->c:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586612
    new-instance v0, LX/AkE;

    invoke-direct {v0, p0}, LX/AkE;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;)V

    .line 586613
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->d:LX/AkJ;

    .line 586614
    iget-object v2, p2, LX/1EF;->b:LX/0Px;

    move-object v2, v2

    .line 586615
    new-instance p2, LX/AkI;

    const-class p1, Landroid/content/Context;

    invoke-interface {v1, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-direct {p2, v2, v0, p1}, LX/AkI;-><init>(LX/0Px;LX/AkE;Landroid/content/Context;)V

    .line 586616
    move-object v0, p2

    .line 586617
    new-instance v1, LX/AkF;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->e:LX/AkK;

    invoke-direct {v1, v0, v2}, LX/AkF;-><init>(LX/1OM;LX/3x6;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x697e156d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586603
    check-cast p1, LX/1EF;

    check-cast p2, LX/AkF;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 586604
    iget-object v1, p2, LX/AkF;->a:LX/1OM;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 586605
    iget-object v1, p2, LX/AkF;->b:LX/3x6;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 586606
    iget v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->g:I

    .line 586607
    iget v2, p1, LX/1EF;->c:I

    move v2, v2

    .line 586608
    invoke-virtual {p4, v1, v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 586609
    const/16 v1, 0x1f

    const v2, -0x5065f54e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586602
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586598
    check-cast p2, LX/AkF;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 586599
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 586600
    iget-object v0, p2, LX/AkF;->b:LX/3x6;

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 586601
    return-void
.end method
