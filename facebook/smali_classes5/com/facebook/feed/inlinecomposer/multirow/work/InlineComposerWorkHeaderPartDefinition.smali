.class public Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1EE;",
        "Landroid/view/View$OnClickListener;",
        "LX/1Ps;",
        "LX/AkO;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/AkO;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/1Ua;

.field public final d:LX/AkN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586595
    new-instance v0, LX/3Uc;

    invoke-direct {v0}, LX/3Uc;-><init>()V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/AkN;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586587
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586588
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 586589
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p2}, LX/1V7;->c()F

    move-result v1

    .line 586590
    iput v1, v0, LX/1UY;->c:F

    .line 586591
    move-object v0, v0

    .line 586592
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->c:LX/1Ua;

    .line 586593
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->d:LX/AkN;

    .line 586594
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;
    .locals 6

    .prologue
    .line 586576
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;

    monitor-enter v1

    .line 586577
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586578
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586579
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586580
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586581
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/AkN;->b(LX/0QB;)LX/AkN;

    move-result-object v5

    check-cast v5, LX/AkN;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/AkN;)V

    .line 586582
    move-object v0, p0

    .line 586583
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586584
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586585
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/AkO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586575
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 586566
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->c:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586567
    new-instance v0, LX/AkG;

    invoke-direct {v0, p0}, LX/AkG;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3deacd76

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586572
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/AkO;

    .line 586573
    invoke-virtual {p4, p2}, LX/AkO;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 586574
    const/16 v1, 0x1f

    const v2, -0x2a5e9a3c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586571
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586568
    check-cast p4, LX/AkO;

    .line 586569
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/AkO;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 586570
    return-void
.end method
