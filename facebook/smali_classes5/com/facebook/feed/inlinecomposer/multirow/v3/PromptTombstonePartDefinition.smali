.class public Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/AkD;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587357
    new-instance v0, LX/3Uv;

    invoke-direct {v0}, LX/3Uv;-><init>()V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587361
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587362
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;
    .locals 3

    .prologue
    .line 587363
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;

    monitor-enter v1

    .line 587364
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587365
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 587368
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;-><init>()V

    .line 587369
    move-object v0, v0

    .line 587370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/AkD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587360
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 587358
    check-cast p1, LX/1Ri;

    .line 587359
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->c:LX/32e;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->c:LX/32e;

    iget-object v0, v0, LX/32e;->a:LX/24P;

    sget-object v1, LX/24P;->DISMISSED:LX/24P;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
