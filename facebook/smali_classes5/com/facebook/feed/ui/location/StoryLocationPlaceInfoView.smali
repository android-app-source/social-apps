.class public Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/view/View;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field public f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPlace;

.field private h:LX/1nA;

.field private i:LX/1Sa;

.field public j:LX/03V;

.field public k:LX/17Y;

.field public l:Lcom/facebook/content/SecureContextHelper;

.field private m:LX/31f;

.field private n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Uh;

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Bi;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 622278
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 622279
    invoke-direct {p0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a()V

    .line 622280
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 622235
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 622236
    invoke-direct {p0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a()V

    .line 622237
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 622238
    const v0, 0x7f03064c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 622239
    const v0, 0x7f0d1173

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    .line 622240
    const v0, 0x7f0d1174

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a:Landroid/widget/ImageView;

    .line 622241
    const v0, 0x7f0d116d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 622242
    const v0, 0x7f0d116c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->d:Landroid/view/View;

    .line 622243
    const-class v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    invoke-static {v0, p0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 622244
    invoke-virtual {p0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->r:Landroid/content/res/Resources;

    .line 622245
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->setOrientation(I)V

    .line 622246
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    sget-object v1, LX/1vY;->SAVE_ACTION:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 622247
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    sget-object v1, LX/1vY;->PLACE_WRITE_REVIEW_ACTION:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 622248
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, LX/1vY;->ACTOR_PHOTO:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 622249
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->d:Landroid/view/View;

    sget-object v1, LX/1vY;->STORY_LOCATION:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 622250
    return-void
.end method

.method private a(ILcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;LX/1Pf;)V
    .locals 4

    .prologue
    .line 622251
    :try_start_0
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 622252
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 622253
    const v0, 0x7f0d1175

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    .line 622254
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622255
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622256
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    new-instance v1, LX/Bto;

    invoke-direct {v1, p0, p1, p3}, LX/Bto;-><init>(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;ILcom/facebook/graphql/model/GraphQLPlace;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622257
    :goto_0
    return-void

    .line 622258
    :catch_0
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->j:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to display review button with invalid page id. Page id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622259
    invoke-direct {p0, p2, p4}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pf;)Z

    goto :goto_0
.end method

.method private a(LX/1Pf;)V
    .locals 2

    .prologue
    .line 622260
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622261
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 622262
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622263
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    new-instance v1, LX/3Bh;

    invoke-direct {v1, p0, p1}, LX/3Bh;-><init>(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;LX/1Pf;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622264
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->g:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-static {p1, v0, v1}, LX/3Bi;->a(LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    .line 622265
    invoke-static {p0, v0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a$redex0(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;Z)V

    .line 622266
    return-void
.end method

.method private a(LX/1nA;LX/03V;LX/1Sa;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/31f;LX/0Ot;LX/0Or;LX/0Uh;LX/0Ot;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Sa;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/31f;",
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/3Bi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622267
    iput-object p1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->h:LX/1nA;

    .line 622268
    iput-object p2, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->j:LX/03V;

    .line 622269
    iput-object p3, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->i:LX/1Sa;

    .line 622270
    iput-object p4, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->k:LX/17Y;

    .line 622271
    iput-object p5, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->l:Lcom/facebook/content/SecureContextHelper;

    .line 622272
    iput-object p6, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->m:LX/31f;

    .line 622273
    iput-object p7, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->n:LX/0Ot;

    .line 622274
    iput-object p8, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->o:LX/0Or;

    .line 622275
    iput-object p9, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->p:LX/0Uh;

    .line 622276
    iput-object p10, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->q:LX/0Ot;

    .line 622277
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 3

    .prologue
    .line 622232
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/Btn;

    invoke-direct {v1, p0, p1}, LX/Btn;-><init>(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;Lcom/facebook/graphql/model/GraphQLPlace;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622233
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->m:LX/31f;

    const-string v1, "android_feed_add_photo_button"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    .line 622234
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    invoke-static {v10}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v1

    check-cast v1, LX/1nA;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v10}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v3

    check-cast v3, LX/1Sa;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v6

    check-cast v6, LX/31f;

    const/16 v7, 0x31d4

    invoke-static {v10, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {v10, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v11, 0x77d

    invoke-static {v10, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(LX/1nA;LX/03V;LX/1Sa;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/31f;LX/0Ot;LX/0Or;LX/0Uh;LX/0Ot;)V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 622225
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Ljava/lang/String;LX/0Px;)Z

    move-result v3

    .line 622226
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    .line 622227
    :goto_0
    iget-object v4, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->p:LX/0Uh;

    const/16 v5, 0x31d

    invoke-virtual {v4, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 622228
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v1, v2

    .line 622229
    :cond_1
    :goto_1
    return v1

    .line 622230
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Ljava/lang/String;LX/0Px;)Z

    move-result v0

    goto :goto_0

    .line 622231
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    goto :goto_1
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pf;)Z
    .locals 1

    .prologue
    .line 622221
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->g:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-static {p1, v0}, LX/1Sa;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622222
    invoke-direct {p0, p2}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(LX/1Pf;)V

    .line 622223
    const/4 v0, 0x1

    .line 622224
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 622174
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    .line 622175
    :goto_0
    return v0

    .line 622176
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 622177
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 622178
    const/4 v0, 0x1

    goto :goto_0

    .line 622179
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 622180
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;ILcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 14

    .prologue
    .line 622209
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 622210
    invoke-virtual {p0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 622211
    const/4 v2, 0x0

    .line 622212
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 622213
    check-cast v0, Landroid/app/Activity;

    move-object v2, v0

    .line 622214
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 622215
    :goto_1
    return-void

    .line 622216
    :cond_1
    instance-of v1, v0, Landroid/view/ContextThemeWrapper;

    if-eqz v1, :cond_0

    .line 622217
    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 622218
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 622219
    check-cast v0, Landroid/app/Activity;

    move-object v2, v0

    goto :goto_0

    .line 622220
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNP;

    const/16 v1, 0x6df

    sget-object v3, LX/21D;->NEWSFEED:LX/21D;

    const-string v4, "checkin_story"

    const-string v5, "native_story"

    const-string v6, "review_button"

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v7, p1

    invoke-virtual/range {v0 .. v13}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;Z)V
    .locals 3

    .prologue
    .line 622202
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622203
    if-eqz p1, :cond_0

    .line 622204
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a:Landroid/widget/ImageView;

    const v1, 0x7f02178c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 622205
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->r:Landroid/content/res/Resources;

    const v2, 0x7f0810f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 622206
    :goto_0
    return-void

    .line 622207
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a:Landroid/widget/ImageView;

    const v1, 0x7f02178b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 622208
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->r:Landroid/content/res/Resources;

    const v2, 0x7f0810f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1Pf;)V
    .locals 10
    .param p3    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "LX/1Pf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/16 v9, 0x8

    .line 622181
    iput-object p1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 622182
    iput-object p2, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->g:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 622183
    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/1y5;

    move-result-object v2

    .line 622184
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 622185
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 622186
    move-object v7, v0

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 622187
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v6

    .line 622188
    iget-object v8, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->h:LX/1nA;

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->d:Landroid/view/View;

    const-string v5, "tap_story_attachment"

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622189
    iget-object v8, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->h:LX/1nA;

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v5, "tap_story_attachment"

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622190
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 622191
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 622192
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, p2, v0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622193
    invoke-direct {p0, p2}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/graphql/model/GraphQLPlace;)V

    .line 622194
    :cond_0
    const v0, 0x76e56aa7

    invoke-static {v7, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 622195
    if-eqz v0, :cond_2

    .line 622196
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD()I

    move-result v0

    invoke-direct {p0, v0, v7, p2, p4}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(ILcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;LX/1Pf;)V

    .line 622197
    :cond_1
    :goto_0
    return-void

    .line 622198
    :cond_2
    invoke-direct {p0, v7, p4}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pf;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 622199
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->b:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 622200
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 622201
    iget-object v0, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->e:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
