.class public Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/AkC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final c:LX/AlV;

.field private final d:LX/24B;

.field private final e:LX/1kG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586752
    new-instance v0, LX/3Ug;

    invoke-direct {v0}, LX/3Ug;-><init>()V

    sput-object v0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/AlV;LX/24B;LX/1kG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586746
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586747
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586748
    iput-object p2, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->c:LX/AlV;

    .line 586749
    iput-object p3, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->d:LX/24B;

    .line 586750
    iput-object p4, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->e:LX/1kG;

    .line 586751
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;
    .locals 7

    .prologue
    .line 586735
    const-class v1, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;

    monitor-enter v1

    .line 586736
    :try_start_0
    sget-object v0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586737
    sput-object v2, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586740
    new-instance p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/AlV;->b(LX/0QB;)LX/AlV;

    move-result-object v4

    check-cast v4, LX/AlV;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v5

    check-cast v5, LX/24B;

    invoke-static {v0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v6

    check-cast v6, LX/1kG;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/AlV;LX/24B;LX/1kG;)V

    .line 586741
    move-object v0, p0

    .line 586742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1Ri;)LX/1lR;
    .locals 1

    .prologue
    .line 586732
    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kJ;

    .line 586733
    iget-object p0, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, p0

    .line 586734
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586713
    sget-object v0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586728
    check-cast p2, LX/1Ri;

    .line 586729
    iget-object v6, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->d:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586730
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->d:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;

    iget-object v5, p0, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->c:LX/AlV;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 586731
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x15c437ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586721
    check-cast p1, LX/1Ri;

    check-cast p4, LX/AkC;

    .line 586722
    invoke-static {p1}, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->b(LX/1Ri;)LX/1lR;

    move-result-object v2

    .line 586723
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    .line 586724
    invoke-virtual {v1, v2}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(LX/1lR;)V

    .line 586725
    iget-object v2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setPromptSession(LX/1RN;)V

    .line 586726
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586727
    const/16 v1, 0x1f

    const v2, 0xcdb986d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586719
    check-cast p1, LX/1Ri;

    .line 586720
    invoke-static {p1}, LX/1kG;->a(LX/1Ri;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586714
    check-cast p4, LX/AkC;

    .line 586715
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    .line 586716
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setPromptSession(LX/1RN;)V

    .line 586717
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586718
    return-void
.end method
