.class public Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Aly;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/AlV;

.field private final c:LX/24B;

.field private final d:LX/1kG;

.field private final e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587262
    new-instance v0, LX/3Us;

    invoke-direct {v0}, LX/3Us;-><init>()V

    sput-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/AlV;LX/24B;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/1kG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587256
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587257
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->b:LX/AlV;

    .line 587258
    iput-object p2, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->c:LX/24B;

    .line 587259
    iput-object p4, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->d:LX/1kG;

    .line 587260
    iput-object p3, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587261
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;
    .locals 7

    .prologue
    .line 587225
    const-class v1, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;

    monitor-enter v1

    .line 587226
    :try_start_0
    sget-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587227
    sput-object v2, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587228
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587229
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587230
    new-instance p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;

    invoke-static {v0}, LX/AlV;->b(LX/0QB;)LX/AlV;

    move-result-object v3

    check-cast v3, LX/AlV;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v4

    check-cast v4, LX/24B;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v6

    check-cast v6, LX/1kG;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;-><init>(LX/AlV;LX/24B;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/1kG;)V

    .line 587231
    move-object v0, p0

    .line 587232
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587233
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587234
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587235
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1Ri;)LX/1lR;
    .locals 1

    .prologue
    .line 587253
    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kJ;

    .line 587254
    iget-object p0, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, p0

    .line 587255
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587252
    sget-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 587248
    check-cast p2, LX/1Ri;

    .line 587249
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->c:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;

    iget-object v5, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->b:LX/AlV;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 587250
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    new-instance v1, LX/Ak0;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, LX/Ak0;-><init>(LX/1Ri;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587251
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xc2f16e4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587242
    check-cast p1, LX/1Ri;

    check-cast p4, LX/Aly;

    .line 587243
    invoke-static {p1}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->b(LX/1Ri;)LX/1lR;

    move-result-object v1

    .line 587244
    invoke-virtual {p4}, LX/Aly;->getV2AttachmentView()Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v2

    .line 587245
    invoke-virtual {v2, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(LX/1lR;)V

    .line 587246
    iget-object v1, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v2, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setPromptSession(LX/1RN;)V

    .line 587247
    const/16 v1, 0x1f

    const v2, -0x10285efc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587240
    check-cast p1, LX/1Ri;

    .line 587241
    invoke-static {p1}, LX/1kG;->a(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/BMT;->a(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 587236
    check-cast p4, LX/Aly;

    .line 587237
    invoke-virtual {p4}, LX/Aly;->getV2AttachmentView()Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    .line 587238
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setPromptSession(LX/1RN;)V

    .line 587239
    return-void
.end method
