.class public Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/Alx;",
        "TE;",
        "LX/BMR;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/BMR;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/B5l;

.field private final c:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

.field private final d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

.field public final e:LX/1kG;

.field private final f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587083
    new-instance v0, LX/3Uo;

    invoke-direct {v0}, LX/3Uo;-><init>()V

    sput-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;LX/1kG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587076
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587077
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->b:LX/B5l;

    .line 587078
    iput-object p3, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587079
    iput-object p2, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    .line 587080
    iput-object p4, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    .line 587081
    iput-object p5, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->e:LX/1kG;

    .line 587082
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;
    .locals 2

    .prologue
    .line 587074
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->a(Landroid/net/Uri;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 587075
    new-instance v1, LX/Alw;

    invoke-direct {v1, p1, v0}, LX/Alw;-><init>(Landroid/net/Uri;LX/1aX;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;
    .locals 9

    .prologue
    .line 587084
    const-class v1, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;

    monitor-enter v1

    .line 587085
    :try_start_0
    sget-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587086
    sput-object v2, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587089
    new-instance v3, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v4

    check-cast v4, LX/B5l;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->b(LX/0QB;)Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-static {v0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v8

    check-cast v8, LX/1kG;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;-><init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;LX/1kG;)V

    .line 587090
    move-object v0, v3

    .line 587091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BMR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587073
    sget-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 587058
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Ps;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 587059
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    new-instance v1, LX/Ak0;

    invoke-direct {v1, p2, v4}, LX/Ak0;-><init>(LX/1Ri;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587060
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/B5l;->a(Ljava/lang/String;)LX/B5p;

    move-result-object v1

    invoke-static {v1, p2}, LX/Ak3;->a(LX/B5p;LX/1Ri;)LX/Ak3;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587061
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kJ;

    .line 587062
    iget-object v1, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, v1

    .line 587063
    invoke-virtual {v0}, LX/1lR;->a()LX/0Px;

    move-result-object v1

    .line 587064
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 587065
    iget-object v2, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v2, v2

    .line 587066
    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;

    move-result-object v2

    .line 587067
    const/4 v0, 0x0

    .line 587068
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 587069
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 587070
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v0, v1

    .line 587071
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;

    move-result-object v0

    .line 587072
    :cond_0
    new-instance v1, LX/Alx;

    invoke-direct {v1, v2, v0}, LX/Alx;-><init>(LX/Alv;LX/Alv;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x54a12a0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587054
    check-cast p2, LX/Alx;

    check-cast p4, LX/BMR;

    .line 587055
    iget-object v1, p2, LX/Alx;->a:LX/Alv;

    iget-object v2, p2, LX/Alx;->b:LX/Alv;

    invoke-virtual {p4, v1, v2}, LX/BMR;->a(LX/Alv;LX/Alv;)V

    .line 587056
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->e:LX/1kG;

    invoke-virtual {v1}, LX/1kG;->c()V

    .line 587057
    const/16 v1, 0x1f

    const v2, -0x6defad28

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587052
    check-cast p1, LX/1Ri;

    .line 587053
    invoke-static {p1}, LX/1kG;->a(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/BMT;->b(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
