.class public Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/io/File;

.field private static volatile j:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;


# instance fields
.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:LX/0aG;

.field public final f:LX/3As;

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 577209
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "feed-inject.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/0aG;LX/3As;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0aG;",
            "LX/3As;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 577200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577201
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    .line 577202
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    .line 577203
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->d:Z

    .line 577204
    iput-object p3, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->e:LX/0aG;

    .line 577205
    iput-object p4, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    .line 577206
    sget-object v0, LX/32M;->DETECT:LX/32M;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(LX/32M;)V

    .line 577207
    return-void

    .line 577208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;
    .locals 7

    .prologue
    .line 577187
    sget-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->j:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    if-nez v0, :cond_1

    .line 577188
    const-class v1, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    monitor-enter v1

    .line 577189
    :try_start_0
    sget-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->j:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 577190
    if-eqz v2, :cond_0

    .line 577191
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 577192
    new-instance v6, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    const/16 v3, 0x1466

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/3As;->b(LX/0QB;)LX/3As;

    move-result-object v5

    check-cast v5, LX/3As;

    invoke-direct {v6, p0, v3, v4, v5}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;-><init>(LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/0aG;LX/3As;)V

    .line 577193
    move-object v0, v6

    .line 577194
    sput-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->j:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577195
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 577196
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 577197
    :cond_1
    sget-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->j:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    return-object v0

    .line 577198
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 577199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;)Z
    .locals 1

    .prologue
    .line 577186
    iget-boolean v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    invoke-virtual {v0}, LX/3As;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 577210
    invoke-static {p0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b(Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 577211
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 577212
    :goto_0
    return-object v0

    .line 577213
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    .line 577214
    iget-object v1, v0, LX/3As;->c:LX/0Px;

    move-object v2, v1

    .line 577215
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 577216
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 577217
    new-instance v5, LX/1u8;

    invoke-direct {v5}, LX/1u8;-><init>()V

    .line 577218
    iput-object v0, v5, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 577219
    move-object v0, v5

    .line 577220
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injected-cacheid-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 577221
    iput-object v5, v0, LX/1u8;->d:Ljava/lang/String;

    .line 577222
    move-object v0, v0

    .line 577223
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0:0000:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 577224
    iput-object v5, v0, LX/1u8;->i:Ljava/lang/String;

    .line 577225
    move-object v0, v0

    .line 577226
    const-string v5, "synthetic_cursor"

    .line 577227
    iput-object v5, v0, LX/1u8;->c:Ljava/lang/String;

    .line 577228
    move-object v0, v0

    .line 577229
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 577230
    invoke-static {v0}, LX/0x0;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v0

    .line 577231
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 577232
    iget v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    .line 577233
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 577234
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 577112
    invoke-static {p0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b(Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 577113
    :goto_0
    return-object p1

    .line 577114
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    .line 577115
    iget-object v2, v1, LX/3As;->c:LX/0Px;

    move-object v3, v2

    .line 577116
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 577117
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577118
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 577119
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 577120
    if-nez v1, :cond_1

    .line 577121
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 577122
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v2

    .line 577123
    if-nez v1, :cond_1

    .line 577124
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 577125
    iget v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v2, v2

    .line 577126
    move v1, v0

    .line 577127
    :goto_1
    if-ge v1, v2, :cond_3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 577128
    iget v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    rem-int/2addr v0, v5

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 577129
    new-instance v5, LX/1u8;

    invoke-direct {v5}, LX/1u8;-><init>()V

    .line 577130
    iput-object v0, v5, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 577131
    move-object v0, v5

    .line 577132
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injected-cacheid-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 577133
    iput-object v5, v0, LX/1u8;->d:Ljava/lang/String;

    .line 577134
    move-object v0, v0

    .line 577135
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injected-dedup-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 577136
    iput-object v5, v0, LX/1u8;->i:Ljava/lang/String;

    .line 577137
    move-object v0, v0

    .line 577138
    const-string v5, "synthetic_cursor"

    .line 577139
    iput-object v5, v0, LX/1u8;->c:Ljava/lang/String;

    .line 577140
    move-object v0, v0

    .line 577141
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 577142
    iget v5, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->i:I

    .line 577143
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 577144
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 577145
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 577146
    iget v1, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->h:I

    add-int/lit8 v7, v1, 0x1

    iput v7, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->h:I

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    rem-int/2addr v1, v7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 577147
    instance-of v7, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v7, :cond_2

    .line 577148
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    iget v7, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->g:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->g:I

    int-to-long v8, v7

    .line 577149
    iput-wide v8, v1, LX/23u;->G:J

    .line 577150
    move-object v1, v1

    .line 577151
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 577152
    :cond_2
    new-instance v7, LX/1u8;

    invoke-direct {v7}, LX/1u8;-><init>()V

    .line 577153
    iput-object v1, v7, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 577154
    move-object v1, v7

    .line 577155
    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v7

    .line 577156
    iput-object v7, v1, LX/1u8;->d:Ljava/lang/String;

    .line 577157
    move-object v1, v1

    .line 577158
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v7

    .line 577159
    iput-object v7, v1, LX/1u8;->i:Ljava/lang/String;

    .line 577160
    move-object v1, v1

    .line 577161
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 577162
    iput-object v0, v1, LX/1u8;->c:Ljava/lang/String;

    .line 577163
    move-object v0, v1

    .line 577164
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 577165
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 577166
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 577167
    :cond_3
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-static {v0}, LX/17L;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;

    move-result-object v0

    const/4 v1, 0x1

    .line 577168
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 577169
    move-object v0, v0

    .line 577170
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 577171
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 577172
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 577173
    new-instance v3, LX/0uq;

    invoke-direct {v3}, LX/0uq;-><init>()V

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 577174
    iput-object v4, v3, LX/0uq;->d:LX/0Px;

    .line 577175
    move-object v3, v3

    .line 577176
    iput-object v2, v3, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 577177
    move-object v2, v3

    .line 577178
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->d()Ljava/lang/String;

    move-result-object v3

    .line 577179
    iput-object v3, v2, LX/0uq;->c:Ljava/lang/String;

    .line 577180
    move-object v2, v2

    .line 577181
    invoke-virtual {v2}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    .line 577182
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 577183
    iget-wide v10, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v10

    .line 577184
    iget-boolean v6, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v6, v6

    .line 577185
    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    move-object p1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/32M;)V
    .locals 4

    .prologue
    .line 577079
    iget-boolean v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->d:Z

    if-nez v0, :cond_1

    .line 577080
    :cond_0
    :goto_0
    return-void

    .line 577081
    :cond_1
    sget-object v0, LX/32N;->a:[I

    invoke-virtual {p1}, LX/32M;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 577082
    :pswitch_0
    sget-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 577083
    sget-object v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 577084
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 577085
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    .line 577086
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 577087
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    .line 577088
    :cond_4
    :goto_1
    :pswitch_1
    const/4 p1, 0x1

    .line 577089
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 577090
    const-string v1, "clearCacheResetFeedLoader"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 577091
    iget-object v1, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->e:LX/0aG;

    const-string v2, "feed_clear_cache"

    const v3, 0x656559da

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    .line 577092
    goto :goto_0

    .line 577093
    :pswitch_2
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 577094
    iget-boolean v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->d:Z

    if-nez v0, :cond_5

    move v0, v1

    .line 577095
    :goto_2
    move v0, v0

    .line 577096
    if-eqz v0, :cond_0

    goto :goto_1

    .line 577097
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    invoke-virtual {v0}, LX/3As;->a()V

    .line 577098
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v3, v0}, LX/3As;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 577099
    goto :goto_2

    .line 577100
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 577101
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_9

    .line 577102
    invoke-virtual {v3}, LX/3As;->a()V

    .line 577103
    const/4 p1, 0x0

    .line 577104
    :goto_3
    move v0, p1

    .line 577105
    if-eqz v0, :cond_7

    move v0, v2

    .line 577106
    goto :goto_2

    .line 577107
    :cond_7
    iget-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->f:LX/3As;

    sget-object v3, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a:Ljava/io/File;

    invoke-virtual {v0, v3}, LX/3As;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 577108
    goto :goto_2

    :cond_8
    move v0, v1

    .line 577109
    goto :goto_2

    .line 577110
    :cond_9
    new-instance p1, LX/2tt;

    invoke-direct {p1, v3, v0}, LX/2tt;-><init>(LX/3As;Ljava/lang/String;)V

    invoke-static {v3, p1}, LX/3As;->a(LX/3As;LX/82W;)V

    .line 577111
    invoke-virtual {v3}, LX/3As;->b()Z

    move-result p1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public installInjectionFile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 577076
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->b:LX/0am;

    .line 577077
    sget-object v0, LX/32M;->DETECT:LX/32M;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(LX/32M;)V

    .line 577078
    return-void
.end method

.method public installInjectionString(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 577073
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->c:LX/0am;

    .line 577074
    sget-object v0, LX/32M;->DETECT:LX/32M;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(LX/32M;)V

    .line 577075
    return-void
.end method
