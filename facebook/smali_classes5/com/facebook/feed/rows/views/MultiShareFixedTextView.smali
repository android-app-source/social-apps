.class public Lcom/facebook/feed/rows/views/MultiShareFixedTextView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/35p;
.implements LX/2yW;
.implements LX/2yX;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

.field private final d:Landroid/view/View;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 635412
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 635413
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 635410
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 635411
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 635401
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 635402
    const v0, 0x7f030ba1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 635403
    const v0, 0x7f0d1ccd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->a:Landroid/widget/TextView;

    .line 635404
    const v0, 0x7f0d1ccc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    .line 635405
    const v0, 0x7f0d1cce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->c:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 635406
    const v0, 0x7f0d1ccf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->d:Landroid/view/View;

    .line 635407
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b09d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->e:I

    .line 635408
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 635409
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 635397
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->c:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    .line 635398
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 635399
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 635400
    return-void
.end method

.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 635396
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->c:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 635394
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->c:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635395
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 635392
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 635393
    return-void
.end method

.method public setDividerVisible(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 635386
    if-eqz p1, :cond_0

    .line 635387
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 635388
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    iget v4, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->e:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 635389
    :goto_0
    return-void

    .line 635390
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 635391
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 635384
    iget-object v0, p0, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 635385
    return-void
.end method
