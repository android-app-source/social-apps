.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Boz;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/24e;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "LX/BpM;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final c:LX/1Ad;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final e:LX/1er;

.field private final f:LX/1ev;

.field public final g:LX/1f2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 589600
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589601
    new-instance v0, LX/3WD;

    invoke-direct {v0}, LX/3WD;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1ev;LX/1er;LX/1f2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589592
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589593
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->c:LX/1Ad;

    .line 589594
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 589595
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->f:LX/1ev;

    .line 589596
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->g:LX/1f2;

    .line 589597
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->e:LX/1er;

    .line 589598
    invoke-virtual {p4}, LX/1er;->a()V

    .line 589599
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;
    .locals 9

    .prologue
    .line 589537
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    monitor-enter v1

    .line 589538
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589539
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589540
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589541
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589542
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v6

    check-cast v6, LX/1ev;

    invoke-static {v0}, LX/1er;->a(LX/0QB;)LX/1er;

    move-result-object v7

    check-cast v7, LX/1er;

    invoke-static {v0}, LX/1f2;->b(LX/0QB;)LX/1f2;

    move-result-object v8

    check-cast v8, LX/1f2;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;-><init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1ev;LX/1er;LX/1f2;)V

    .line 589543
    move-object v0, v3

    .line 589544
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589545
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589546
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/5kD;)Z
    .locals 1

    .prologue
    .line 589591
    invoke-interface {p0}, LX/5kD;->S()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589590
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 589575
    check-cast p2, LX/5kD;

    check-cast p3, LX/Boz;

    .line 589576
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1UY;->b(I)LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 589577
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589578
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 589579
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 589580
    invoke-static {v1}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;

    move-result-object v2

    .line 589581
    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    move-object v0, p3

    .line 589582
    check-cast v0, LX/1Pt;

    sget-object v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v3, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 589583
    new-instance v0, LX/BpL;

    invoke-direct {v0, p2, v3, p3}, LX/BpL;-><init>(LX/5kD;LX/1bf;LX/Boz;)V

    .line 589584
    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->c:LX/1Ad;

    sget-object v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->p()LX/1Ad;

    .line 589585
    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->g:LX/1f2;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    :goto_0
    iget-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->c:LX/1Ad;

    invoke-virtual {v4, v1, v3, p1}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;)LX/1bf;

    .line 589586
    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->c:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    move-object v3, v3

    .line 589587
    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->f:LX/1ev;

    invoke-virtual {v4, v1}, LX/1ev;->c(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v1

    .line 589588
    new-instance v4, LX/BpM;

    invoke-direct {v4, v3, v2, v1, v0}, LX/BpM;-><init>(LX/1aZ;Landroid/graphics/PointF;LX/1f6;LX/BpL;)V

    return-object v4

    .line 589589
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x37116939

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589555
    check-cast p1, LX/5kD;

    check-cast p2, LX/BpM;

    const/4 v5, 0x0

    .line 589556
    move-object v1, p4

    check-cast v1, LX/24e;

    invoke-interface {v1}, LX/24e;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v2

    .line 589557
    const/4 v1, 0x0

    invoke-static {v2, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 589558
    iget-object v1, p2, LX/BpM;->a:LX/1aZ;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 589559
    iget-object v1, p2, LX/BpM;->c:LX/1f6;

    .line 589560
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 589561
    if-eqz v4, :cond_0

    .line 589562
    iget p3, v1, LX/1f6;->g:I

    move p3, p3

    .line 589563
    iput p3, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 589564
    iget p3, v1, LX/1f6;->h:I

    move p3, p3

    .line 589565
    iput p3, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 589566
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 589567
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object v4, p2, LX/BpM;->b:Landroid/graphics/PointF;

    invoke-virtual {v1, v4}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 589568
    invoke-interface {p1}, LX/5kD;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 589569
    check-cast v1, LX/24e;

    invoke-interface {v1, v5, v5}, LX/24e;->a(II)V

    move-object v1, p4

    .line 589570
    check-cast v1, LX/24e;

    iget-object v2, p2, LX/BpM;->d:LX/BpL;

    invoke-interface {v1, v2}, LX/24e;->setOnPhotoClickListener(LX/24m;)V

    .line 589571
    check-cast p4, LX/24e;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->e:LX/1er;

    .line 589572
    iget v4, v2, LX/1er;->c:I

    move v2, v4

    .line 589573
    invoke-interface {p4, v1, v2}, LX/24e;->a(Ljava/lang/String;I)V

    .line 589574
    const/16 v1, 0x1f

    const v2, 0x4cc9f176    # 1.058764E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589554
    check-cast p1, LX/5kD;

    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a(LX/5kD;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 589548
    const/4 v1, 0x0

    .line 589549
    move-object v0, p4

    check-cast v0, LX/24e;

    invoke-interface {v0}, LX/24e;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v0

    .line 589550
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 589551
    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 589552
    check-cast p4, LX/24e;

    invoke-interface {p4, v1}, LX/24e;->setOnPhotoClickListener(LX/24m;)V

    .line 589553
    return-void
.end method
