.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field public final c:LX/1WM;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final f:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589784
    new-instance v0, LX/3WG;

    invoke-direct {v0}, LX/3WG;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->a:LX/1Cz;

    .line 589785
    sget-object v0, LX/1Ua;->d:LX/1Ua;

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/1WM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589754
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589755
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->c:LX/1WM;

    .line 589756
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 589757
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 589758
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->f:Landroid/content/Context;

    .line 589759
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;
    .locals 7

    .prologue
    .line 589773
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    monitor-enter v1

    .line 589774
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589775
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589776
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589777
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589778
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v3

    check-cast v3, LX/1WM;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;-><init>(LX/1WM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;)V

    .line 589779
    move-object v0, p0

    .line 589780
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589781
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589782
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589786
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 589762
    check-cast p2, LX/5kD;

    check-cast p3, LX/1Ps;

    const/4 v3, 0x0

    .line 589763
    new-instance v0, LX/1X6;

    sget-object v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->b:LX/1Ua;

    invoke-direct {v0, v3, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 589764
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589765
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 589766
    new-instance v1, LX/Bq6;

    invoke-direct {v1, p0, p2, p3}, LX/Bq6;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;LX/5kD;LX/1Ps;)V

    .line 589767
    const v2, 0x7f082700

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 589768
    const v4, 0x7f082702

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 589769
    new-instance v5, LX/47x;

    invoke-direct {v5, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v5, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v2, "link_hide_photo"

    const/16 v5, 0x21

    invoke-virtual {v0, v2, v4, v1, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    .line 589770
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    move-object v0, v0

    .line 589771
    const v1, 0x7f0d1e09

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 589772
    return-object v3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 589760
    check-cast p1, LX/5kD;

    .line 589761
    invoke-static {p1}, LX/1WM;->b(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->c:LX/1WM;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1WM;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
