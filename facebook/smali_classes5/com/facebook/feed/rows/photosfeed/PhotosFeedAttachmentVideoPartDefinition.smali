.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Boz;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "LX/BpZ;",
        "TE;",
        "LX/3JH;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final c:LX/1Ad;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:LX/1ev;

.field private final f:LX/1X6;

.field public final g:LX/7za;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7za",
            "<",
            "LX/3JH;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/BqJ;

.field private final i:LX/Bq8;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 589664
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589665
    new-instance v0, LX/3WE;

    invoke-direct {v0}, LX/3WE;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1ev;LX/7za;LX/BqJ;LX/Bq8;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589604
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589605
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->c:LX/1Ad;

    .line 589606
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 589607
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->e:LX/1ev;

    .line 589608
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->g:LX/7za;

    .line 589609
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->h:LX/BqJ;

    .line 589610
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->i:LX/Bq8;

    .line 589611
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1UY;->b(I)LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->f:LX/1X6;

    .line 589612
    return-void
.end method

.method private static a(FF)F
    .locals 1

    .prologue
    .line 589663
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    div-float v0, p0, p1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;LX/1bf;)LX/1aZ;
    .locals 2

    .prologue
    .line 589662
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->c:LX/1Ad;

    sget-object v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;LX/5kD;LX/Boz;)LX/BpZ;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/5kD;",
            "TE;)",
            "LX/BpZ;"
        }
    .end annotation

    .prologue
    .line 589645
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->f:LX/1X6;

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589646
    invoke-static/range {p2 .. p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 589647
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 589648
    invoke-static {v2}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;

    move-result-object v4

    .line 589649
    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    .line 589650
    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v12

    .line 589651
    invoke-static {p0, v12}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;LX/1bf;)LX/1aZ;

    move-result-object v3

    .line 589652
    iget-object v5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->e:LX/1ev;

    invoke-virtual {v5, v2}, LX/1ev;->c(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v2

    .line 589653
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v5

    sget-object v6, LX/097;->FROM_STREAM:LX/097;

    invoke-virtual {v5, v6}, LX/2oE;->a(LX/097;)LX/2oE;

    move-result-object v5

    invoke-interface/range {p2 .. p2}, LX/5kD;->ad()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2oE;->a(Landroid/net/Uri;)LX/2oE;

    move-result-object v5

    invoke-virtual {v5}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v5

    .line 589654
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v5

    invoke-interface/range {p2 .. p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2oH;->a(Ljava/lang/String;)LX/2oH;

    move-result-object v5

    invoke-interface/range {p2 .. p2}, LX/5kD;->ac()I

    move-result v6

    invoke-virtual {v5, v6}, LX/2oH;->a(I)LX/2oH;

    move-result-object v5

    invoke-virtual {v5}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 589655
    invoke-virtual {v2}, LX/1f6;->e()I

    move-result v7

    .line 589656
    invoke-virtual {v2}, LX/1f6;->f()I

    move-result v8

    .line 589657
    int-to-float v2, v7

    int-to-float v6, v8

    invoke-static {v2, v6}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a(FF)F

    move-result v9

    .line 589658
    new-instance v6, LX/BqG;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, LX/BqG;-><init>(LX/5kD;)V

    move-object/from16 v2, p3

    .line 589659
    check-cast v2, LX/1Pr;

    new-instance v10, LX/4Vj;

    invoke-interface/range {p2 .. p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v6, v10}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/BqH;

    .line 589660
    new-instance v2, LX/BpZ;

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v6

    iget-object v11, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->h:LX/BqJ;

    invoke-interface/range {p2 .. p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13, v10}, LX/BqJ;->a(Ljava/lang/String;LX/BqH;)LX/BqI;

    move-result-object v11

    iget-object v13, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->i:LX/Bq8;

    const/4 v14, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v13, v0, v14, v1, v12}, LX/Bq8;->a(LX/5kD;ZLX/Boz;LX/1bf;)LX/Bq7;

    move-result-object v12

    invoke-direct/range {v2 .. v12}, LX/BpZ;-><init>(LX/1aZ;Landroid/graphics/PointF;Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;IIFLX/BqH;LX/2oV;Landroid/view/View$OnClickListener;)V

    .line 589661
    return-object v2
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;
    .locals 10

    .prologue
    .line 589666
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    monitor-enter v1

    .line 589667
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589668
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589669
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589670
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589671
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v6

    check-cast v6, LX/1ev;

    invoke-static {v0}, LX/7za;->a(LX/0QB;)LX/7za;

    move-result-object v7

    check-cast v7, LX/7za;

    const-class v8, LX/BqJ;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/BqJ;

    const-class v9, LX/Bq8;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Bq8;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;-><init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1ev;LX/7za;LX/BqJ;LX/Bq8;)V

    .line 589672
    move-object v0, v3

    .line 589673
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589674
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589675
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589644
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 589643
    check-cast p2, LX/5kD;

    check-cast p3, LX/Boz;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a(LX/1aD;LX/5kD;LX/Boz;)LX/BpZ;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x66b671c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589625
    check-cast p2, LX/BpZ;

    check-cast p4, LX/3JH;

    .line 589626
    iget-object v4, p4, LX/3JH;->b:LX/7gP;

    move-object v4, v4

    .line 589627
    iget-object v5, p2, LX/BpZ;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5, p4}, LX/7gP;->a(Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 589628
    iget-object v4, p4, LX/3JH;->b:LX/7gP;

    move-object v4, v4

    .line 589629
    sget-object v5, LX/04D;->FEED:LX/04D;

    const/4 v6, 0x0

    iget-object v7, p2, LX/BpZ;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v8, p2, LX/BpZ;->g:F

    float-to-double v8, v8

    iget-object v10, p2, LX/BpZ;->d:LX/0P1;

    invoke-virtual/range {v4 .. v10}, LX/7gP;->a(LX/04D;ZLcom/facebook/video/engine/VideoPlayerParams;DLX/0P1;)V

    .line 589630
    iget-object v4, p4, LX/3JH;->b:LX/7gP;

    move-object v4, v4

    .line 589631
    iget-object v5, p2, LX/BpZ;->a:LX/1aZ;

    invoke-virtual {v4, v5}, LX/7gP;->setCoverController(LX/1aZ;)V

    .line 589632
    iget-object v4, p4, LX/3JH;->b:LX/7gP;

    move-object v4, v4

    .line 589633
    iget v5, p2, LX/BpZ;->e:I

    iget v6, p2, LX/BpZ;->f:I

    invoke-virtual {v4, v5, v6}, LX/7gP;->a(II)V

    .line 589634
    iget v4, p2, LX/BpZ;->g:F

    .line 589635
    iput v4, p4, LX/3JH;->c:F

    .line 589636
    iget-object v4, p4, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v5, v4

    .line 589637
    iget-object v4, p2, LX/BpZ;->i:LX/BpY;

    .line 589638
    iput-object v5, v4, LX/BpY;->b:LX/7Kf;

    .line 589639
    invoke-interface {v5}, LX/7Kf;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v4

    check-cast v4, LX/1af;

    iget-object v6, p2, LX/BpZ;->b:Landroid/graphics/PointF;

    invoke-virtual {v4, v6}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 589640
    iget-object v4, p2, LX/BpZ;->i:LX/BpY;

    invoke-interface {v5, v4}, LX/7Kf;->setVideoListener(LX/2pf;)V

    .line 589641
    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->g:LX/7za;

    iget-object v5, p2, LX/BpZ;->k:LX/2oV;

    invoke-virtual {v4, p4, v5}, LX/7za;->a(Landroid/view/View;LX/2oV;)V

    .line 589642
    const/16 v1, 0x1f

    const v2, 0x7e14bccf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589623
    check-cast p1, LX/5kD;

    .line 589624
    invoke-interface {p1}, LX/5kD;->S()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 589613
    check-cast p2, LX/BpZ;

    check-cast p4, LX/3JH;

    const/4 v1, 0x0

    .line 589614
    iget-object v0, p4, LX/3JH;->b:LX/7gP;

    move-object v0, v0

    .line 589615
    const/4 p0, 0x0

    .line 589616
    invoke-virtual {v0, p0, p0}, LX/7gP;->a(Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 589617
    iget-object v0, p4, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v0

    .line 589618
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/InlineVideoPlayer2;->setVideoListener(LX/2pf;)V

    .line 589619
    iget-object v0, p2, LX/BpZ;->i:LX/BpY;

    .line 589620
    const/4 p0, 0x0

    iput-object p0, v0, LX/BpY;->b:LX/7Kf;

    .line 589621
    invoke-virtual {p4, v1}, LX/3JH;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589622
    return-void
.end method
