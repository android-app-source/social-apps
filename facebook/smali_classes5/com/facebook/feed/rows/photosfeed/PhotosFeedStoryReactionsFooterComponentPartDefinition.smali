.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static k:LX/0Xm;


# instance fields
.field private final e:LX/BqF;

.field private final f:LX/39e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39e",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/1We;

.field private final h:LX/1Wm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wm",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

.field private final j:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589899
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BqF;LX/39e;LX/1We;LX/1Wm;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589891
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589892
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->e:LX/BqF;

    .line 589893
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->f:LX/39e;

    .line 589894
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->g:LX/1We;

    .line 589895
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->h:LX/1Wm;

    .line 589896
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 589897
    iput-object p7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->j:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    .line 589898
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 589887
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->g:LX/1We;

    sget-object v1, LX/1Wi;->TOP:LX/1Wi;

    invoke-virtual {v0, v1}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    .line 589888
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;)I

    move-result v1

    .line 589889
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->f:LX/39e;

    invoke-virtual {v2, p1}, LX/39e;->c(LX/1De;)LX/C48;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/C48;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C48;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/C48;->a(LX/1Po;)LX/C48;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/C48;->a(Z)LX/C48;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/C48;->h(I)LX/C48;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 589890
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->h:LX/1Wm;

    invoke-virtual {v2, p1}, LX/1Wm;->c(LX/1De;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/39Z;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/39Z;->a(LX/1Wj;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/39Z;->a(LX/1X1;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;
    .locals 11

    .prologue
    .line 589876
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    monitor-enter v1

    .line 589877
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589878
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589879
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589880
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589881
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/BqF;->a(LX/0QB;)LX/BqF;

    move-result-object v5

    check-cast v5, LX/BqF;

    invoke-static {v0}, LX/39e;->a(LX/0QB;)LX/39e;

    move-result-object v6

    check-cast v6, LX/39e;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v7

    check-cast v7, LX/1We;

    invoke-static {v0}, LX/1Wm;->a(LX/0QB;)LX/1Wm;

    move-result-object v8

    check-cast v8, LX/1Wm;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/BqF;LX/39e;LX/1We;LX/1Wm;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;)V

    .line 589882
    move-object v0, v3

    .line 589883
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589884
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589885
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589900
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 589875
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 589869
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589870
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->j:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->e:LX/BqF;

    .line 589871
    iget-object v1, v0, LX/BqF;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 589872
    iget-object v1, v0, LX/BqF;->a:LX/0ad;

    sget-short p0, LX/BqE;->g:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/BqF;->e:Ljava/lang/Boolean;

    .line 589873
    :cond_0
    iget-object v1, v0, LX/BqF;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 589874
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 589867
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589868
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589866
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
