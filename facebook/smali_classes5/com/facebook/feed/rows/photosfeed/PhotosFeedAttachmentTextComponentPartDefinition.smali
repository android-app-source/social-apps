.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/5kD;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/BqF;

.field private final f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

.field private final g:LX/1V0;

.field private final h:LX/BpX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589933
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BqF;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;LX/1V0;LX/BpX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589927
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589928
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->e:LX/BqF;

    .line 589929
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

    .line 589930
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->g:LX/1V0;

    .line 589931
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->h:LX/BpX;

    .line 589932
    return-void
.end method

.method private a(LX/1De;LX/5kD;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/5kD;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 589921
    invoke-interface {p2}, LX/5kD;->X()LX/175;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 589922
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 589923
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->h:LX/BpX;

    invoke-virtual {v2, v0, v1}, LX/BpX;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;

    move-result-object v0

    .line 589924
    const v1, 0x7f01029a

    const v2, 0x7f0a0158

    invoke-static {p1, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    .line 589925
    invoke-static {p1, v3, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1ne;->m(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b1064

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b1063

    invoke-virtual {v0, v1}, LX/1ne;->s(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 589926
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->g:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;
    .locals 9

    .prologue
    .line 589910
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;

    monitor-enter v1

    .line 589911
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589912
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589915
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/BqF;->a(LX/0QB;)LX/BqF;

    move-result-object v5

    check-cast v5, LX/BqF;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/BpX;->a(LX/0QB;)LX/BpX;

    move-result-object v8

    check-cast v8, LX/BpX;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/BqF;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;LX/1V0;LX/BpX;)V

    .line 589916
    move-object v0, v3

    .line 589917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589901
    check-cast p2, LX/5kD;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->a(LX/1De;LX/5kD;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 589909
    check-cast p2, LX/5kD;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->a(LX/1De;LX/5kD;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 589903
    check-cast p1, LX/5kD;

    .line 589904
    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->e:LX/BqF;

    .line 589905
    iget-object v1, v0, LX/BqF;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 589906
    iget-object v1, v0, LX/BqF;->a:LX/0ad;

    sget-short p0, LX/BqE;->c:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/BqF;->h:Ljava/lang/Boolean;

    .line 589907
    :cond_0
    iget-object v1, v0, LX/BqF;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 589908
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589902
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
