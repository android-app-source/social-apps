.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/BqF;

.field private final f:LX/1WX;

.field private final g:LX/1V0;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589864
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BqF;LX/1WX;LX/1V0;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BqF;",
            "LX/1WX;",
            "LX/1V0;",
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589858
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589859
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->e:LX/BqF;

    .line 589860
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->f:LX/1WX;

    .line 589861
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->g:LX/1V0;

    .line 589862
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->h:LX/0Ot;

    .line 589863
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 589853
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589854
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 589855
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->f:LX/1WX;

    invoke-virtual {v1, p1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 589856
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->g:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v1

    .line 589857
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XK;

    invoke-virtual {v0, p1}, LX/1XK;->c(LX/1De;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XM;->a(LX/1Po;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1XM;->a(LX/1X1;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;
    .locals 9

    .prologue
    .line 589842
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;

    monitor-enter v1

    .line 589843
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589844
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589847
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/BqF;->a(LX/0QB;)LX/BqF;

    move-result-object v5

    check-cast v5, LX/BqF;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v6

    check-cast v6, LX/1WX;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    const/16 v8, 0x853

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;-><init>(Landroid/content/Context;LX/BqF;LX/1WX;LX/1V0;LX/0Ot;)V

    .line 589848
    move-object v0, v3

    .line 589849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589865
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 589841
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 589832
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 589833
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589834
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 589835
    invoke-static {v0, v1, v1}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v0

    .line 589836
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->e:LX/BqF;

    .line 589837
    iget-object v2, v0, LX/BqF;->f:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 589838
    iget-object v2, v0, LX/BqF;->a:LX/0ad;

    sget-short p0, LX/BqE;->d:S

    const/4 p1, 0x0

    invoke-interface {v2, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/BqF;->f:Ljava/lang/Boolean;

    .line 589839
    :cond_0
    iget-object v2, v0, LX/BqF;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v0, v2

    .line 589840
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 589830
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589831
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589829
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
