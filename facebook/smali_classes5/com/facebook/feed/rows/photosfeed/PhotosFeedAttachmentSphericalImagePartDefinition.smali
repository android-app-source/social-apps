.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Boz;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "LX/BpW;",
        "TE;",
        "LX/8xF;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final c:LX/1Ad;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final e:LX/1f2;

.field public final f:LX/7Dh;

.field public final g:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "LX/8xF;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 589750
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589751
    new-instance v0, LX/3WF;

    invoke-direct {v0}, LX/3WF;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1er;LX/1f2;LX/7Dh;LX/1AV;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589741
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589742
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->c:LX/1Ad;

    .line 589743
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 589744
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->e:LX/1f2;

    .line 589745
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->f:LX/7Dh;

    .line 589746
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->g:LX/1AV;

    .line 589747
    iput-object p7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->h:Landroid/content/Context;

    .line 589748
    invoke-virtual {p3}, LX/1er;->a()V

    .line 589749
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;
    .locals 11

    .prologue
    .line 589730
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    monitor-enter v1

    .line 589731
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589732
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589733
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589734
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589735
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1er;->a(LX/0QB;)LX/1er;

    move-result-object v6

    check-cast v6, LX/1er;

    invoke-static {v0}, LX/1f2;->b(LX/0QB;)LX/1f2;

    move-result-object v7

    check-cast v7, LX/1f2;

    invoke-static {v0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v8

    check-cast v8, LX/7Dh;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v9

    check-cast v9, LX/1AV;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;-><init>(LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1er;LX/1f2;LX/7Dh;LX/1AV;Landroid/content/Context;)V

    .line 589736
    move-object v0, v3

    .line 589737
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589738
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589739
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589679
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 589698
    check-cast p2, LX/5kD;

    check-cast p3, LX/Boz;

    .line 589699
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1UY;->b(I)LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 589700
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589701
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 589702
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 589703
    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    move-object v0, p3

    .line 589704
    check-cast v0, LX/1Pt;

    sget-object v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v3}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 589705
    new-instance v0, LX/BpU;

    invoke-direct {v0, p0, p2, p3, v2}, LX/BpU;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;LX/5kD;LX/Boz;LX/1bf;)V

    move-object v3, v0

    .line 589706
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->c:LX/1Ad;

    sget-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    .line 589707
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->e:LX/1f2;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->c:LX/1Ad;

    invoke-virtual {v2, v1, v0, v4}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;)LX/1bf;

    .line 589708
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->c:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    move-object v1, v0

    .line 589709
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v11

    .line 589710
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 589711
    const/4 v6, 0x0

    .line 589712
    :goto_1
    move-object v4, v6

    .line 589713
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    .line 589714
    if-nez v0, :cond_2

    .line 589715
    sget-object v2, LX/7Dj;->UNKNOWN:LX/7Dj;

    .line 589716
    :goto_2
    move-object v5, v2

    .line 589717
    new-instance v0, LX/BpW;

    .line 589718
    if-eqz p2, :cond_3

    .line 589719
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 589720
    new-instance v2, LX/BpT;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, p0, v6}, LX/BpT;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;Ljava/lang/String;)V

    .line 589721
    :goto_3
    move-object v2, v2

    .line 589722
    invoke-direct/range {v0 .. v5}, LX/BpW;-><init>(LX/1aZ;LX/2oV;Landroid/view/View$OnClickListener;Lcom/facebook/spherical/photo/model/SphericalPhotoParams;LX/7Dj;)V

    return-object v0

    .line 589723
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 589724
    :cond_1
    invoke-static {v11}, LX/7Dx;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/List;

    move-result-object v6

    .line 589725
    iget-object v7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->f:LX/7Dh;

    invoke-virtual {v7}, LX/7Dh;->e()Z

    move-result v7

    iget-object v8, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->h:Landroid/content/Context;

    invoke-static {v8}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v8

    const-string v9, ""

    const-string v10, ""

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v11

    invoke-static/range {v6 .. v11}, LX/7E3;->a(Ljava/util/List;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v6

    goto :goto_1

    .line 589726
    :cond_2
    sget-object v2, LX/BpV;->a:[I

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    invoke-virtual {v5}, LX/1Qt;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 589727
    sget-object v2, LX/7Dj;->UNKNOWN:LX/7Dj;

    goto :goto_2

    .line 589728
    :pswitch_0
    sget-object v2, LX/7Dj;->NEWSFEED:LX/7Dj;

    goto :goto_2

    .line 589729
    :pswitch_1
    sget-object v2, LX/7Dj;->TIMELINE:LX/7Dj;

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x47a4a5f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589689
    check-cast p1, LX/5kD;

    check-cast p2, LX/BpW;

    check-cast p4, LX/8xF;

    .line 589690
    invoke-static {p1}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 589691
    iget-object v2, p2, LX/BpW;->a:LX/1aZ;

    invoke-virtual {p4, v2}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 589692
    iget-object v2, p2, LX/BpW;->d:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    sget-object v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    iget-object p3, p2, LX/BpW;->e:LX/7Dj;

    invoke-virtual {p4, v2, v4, v1, p3}, LX/8xF;->b(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 589693
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->g:LX/1AV;

    iget-object v2, p2, LX/BpW;->b:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 589694
    iget-object v1, p2, LX/BpW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/8xF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589695
    const/4 v1, 0x0

    .line 589696
    iput-boolean v1, p4, LX/8wv;->t:Z

    .line 589697
    const/16 v1, 0x1f

    const v2, -0x611919c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 589684
    check-cast p1, LX/5kD;

    .line 589685
    invoke-static {p1}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 589686
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->f:LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589687
    const/4 v0, 0x1

    .line 589688
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 589680
    check-cast p4, LX/8xF;

    .line 589681
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 589682
    invoke-virtual {p4}, LX/8xF;->q()V

    .line 589683
    return-void
.end method
