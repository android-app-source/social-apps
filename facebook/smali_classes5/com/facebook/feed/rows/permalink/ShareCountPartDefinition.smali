.class public Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VF;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590316
    new-instance v0, LX/3WT;

    invoke-direct {v0}, LX/3WT;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590317
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590318
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->c:LX/0ad;

    .line 590319
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590320
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;
    .locals 5

    .prologue
    .line 590321
    const-class v1, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    monitor-enter v1

    .line 590322
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590323
    sput-object v2, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590324
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590325
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590326
    new-instance p0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;-><init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 590327
    move-object v0, p0

    .line 590328
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590329
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590330
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    .line 590332
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590333
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 590334
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3VF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590335
    sget-object v0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 590336
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590337
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590338
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xe0ad5db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590339
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/3VF;

    const/4 v1, 0x0

    .line 590340
    invoke-virtual {p4, v1}, LX/3VF;->setLikes(I)V

    .line 590341
    invoke-virtual {p4, v1}, LX/3VF;->setComments(I)V

    .line 590342
    invoke-static {p1}, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    invoke-virtual {p4, v1}, LX/3VF;->setShares(I)V

    .line 590343
    const/16 v1, 0x1f

    const v2, 0x32c91a7d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 590344
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 590345
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->c:LX/0ad;

    sget-short v2, LX/0wn;->aF:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
