.class public Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/Bov;",
        "LX/1Ps;",
        "LX/Bow;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

.field public final e:LX/749;

.field public f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 589083
    new-instance v0, LX/3W8;

    invoke-direct {v0}, LX/3W8;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->a:LX/1Cz;

    .line 589084
    new-instance v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v1, LX/3WA;->USER_INITIATED:LX/3WA;

    const-class v2, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    sput-object v0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/749;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589085
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589086
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    .line 589087
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 589088
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->e:LX/749;

    .line 589089
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;
    .locals 6

    .prologue
    .line 589090
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    monitor-enter v1

    .line 589091
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589092
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589093
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589094
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589095
    new-instance p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->b(LX/0QB;)Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/749;->a(LX/0QB;)LX/749;

    move-result-object v5

    check-cast v5, LX/749;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/749;)V

    .line 589096
    move-object v0, p0

    .line 589097
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589101
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 589102
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589103
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589104
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 589105
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 589106
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->e:LX/749;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->k()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v3

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    .line 589107
    invoke-static {}, LX/74I;->a()Ljava/lang/String;

    move-result-object v5

    .line 589108
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v7, LX/748;->PROFILE_PHOTO_PROMPT_DISPLAYED:LX/748;

    invoke-virtual {v7}, LX/748;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 589109
    const-string v7, "photo_num_likes"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p3, "photo_num_comments"

    invoke-virtual {v7, p3, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p3, "photo_fbid"

    invoke-virtual {v7, p3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 589110
    invoke-static {v1, v6, v5}, LX/749;->a(LX/749;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 589111
    move-object v1, v5

    .line 589112
    iput-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->f:Ljava/lang/String;

    .line 589113
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589114
    new-instance v1, LX/Bot;

    invoke-direct {v1, p0, v0}, LX/Bot;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 589115
    new-instance v0, LX/Bou;

    invoke-direct {v0, p0}, LX/Bou;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;)V

    .line 589116
    new-instance v2, LX/Bov;

    invoke-direct {v2, v1, v0}, LX/Bov;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x15a11cf3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589117
    check-cast p2, LX/Bov;

    check-cast p4, LX/Bow;

    .line 589118
    iget-object v1, p2, LX/Bov;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/Bow;->a(Landroid/view/View$OnClickListener;)V

    .line 589119
    const/16 v1, 0x1f

    const v2, -0x47609f8e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 589120
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589121
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589122
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 589123
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v1, :cond_1

    :cond_0
    invoke-static {v0}, LX/17E;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v3, v1, :cond_3

    :cond_1
    move v1, v2

    .line 589124
    :cond_2
    :goto_0
    move v0, v1

    .line 589125
    return v0

    .line 589126
    :cond_3
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 589127
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const p1, 0x4984e12

    if-ne p0, p1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->bf()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 589128
    check-cast p4, LX/Bow;

    .line 589129
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/Bow;->a(Landroid/view/View$OnClickListener;)V

    .line 589130
    return-void
.end method
