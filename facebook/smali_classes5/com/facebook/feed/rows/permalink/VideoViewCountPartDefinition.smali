.class public Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "LX/1Ps;",
        "LX/C63;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/23p;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588941
    new-instance v0, LX/3Vz;

    invoke-direct {v0}, LX/3Vz;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/23p;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588936
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588937
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->b:LX/23p;

    .line 588938
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588939
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->d:Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    .line 588940
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;
    .locals 6

    .prologue
    .line 588925
    const-class v1, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    monitor-enter v1

    .line 588926
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588927
    sput-object v2, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588928
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588929
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588930
    new-instance p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    invoke-static {v0}, LX/23p;->b(LX/0QB;)LX/23p;

    move-result-object v3

    check-cast v3, LX/23p;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;-><init>(LX/23p;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;)V

    .line 588931
    move-object v0, p0

    .line 588932
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588933
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588934
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588935
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 588891
    invoke-static {p0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 588892
    :cond_0
    :goto_0
    return-object v0

    .line 588893
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 588894
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 588895
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 588896
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588924
    sget-object v0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 588917
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588918
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588919
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588920
    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 588921
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588922
    const v1, 0x7f0d3062

    iget-object v2, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->d:Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 588923
    invoke-static {v0}, LX/C66;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x814a06

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588910
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/C63;

    .line 588911
    if-eqz p2, :cond_0

    .line 588912
    iget-object v1, p4, LX/C63;->a:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 588913
    if-eqz v1, :cond_0

    .line 588914
    iget-object v1, p4, LX/C63;->a:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 588915
    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588916
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x37252a1f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 588904
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588905
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->b:LX/23p;

    .line 588906
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588907
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 588908
    invoke-static {v0}, LX/23p;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/23p;->a:LX/14w;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 588909
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 588897
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/C63;

    .line 588898
    if-eqz p2, :cond_0

    .line 588899
    iget-object v0, p4, LX/C63;->a:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v0

    .line 588900
    if-eqz v0, :cond_0

    .line 588901
    iget-object v0, p4, LX/C63;->a:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v0

    .line 588902
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588903
    :cond_0
    return-void
.end method
