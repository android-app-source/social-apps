.class public Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588611
    new-instance v0, LX/3Vo;

    invoke-direct {v0}, LX/3Vo;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588618
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 588619
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->a(Z)V

    .line 588620
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 588621
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 588622
    invoke-direct {p0, p2}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->a(Z)V

    .line 588623
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 588613
    if-eqz p1, :cond_0

    const v0, 0x7f030eb8

    .line 588614
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 588615
    const v0, 0x7f0d1803

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->b:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    .line 588616
    return-void

    .line 588617
    :cond_0
    const v0, 0x7f030eb7

    goto :goto_0
.end method


# virtual methods
.method public getPageIndicator()Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;
    .locals 1

    .prologue
    .line 588612
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->b:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    return-object v0
.end method
