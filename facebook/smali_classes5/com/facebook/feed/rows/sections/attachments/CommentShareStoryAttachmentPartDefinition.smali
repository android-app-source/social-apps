.class public Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BrD;",
        "LX/1Pf;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:Landroid/view/View$OnClickListener;

.field private static n:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:LX/1Uf;

.field public final g:LX/8qo;

.field private final h:LX/0hy;

.field private final i:Lcom/facebook/content/SecureContextHelper;

.field private final j:LX/154;

.field public final k:LX/11S;

.field public final l:LX/6Bu;

.field private final m:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591071
    const v0, 0x7f0302e1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a:LX/1Cz;

    .line 591072
    const-class v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 591073
    new-instance v0, LX/3Wl;

    invoke-direct {v0}, LX/3Wl;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->c:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;LX/8qo;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/154;LX/11S;LX/0Zb;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591057
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591058
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    .line 591059
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 591060
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->f:LX/1Uf;

    .line 591061
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->g:LX/8qo;

    .line 591062
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->h:LX/0hy;

    .line 591063
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->i:Lcom/facebook/content/SecureContextHelper;

    .line 591064
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->j:LX/154;

    .line 591065
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->k:LX/11S;

    .line 591066
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->m:LX/0Zb;

    .line 591067
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 591068
    new-instance v1, LX/BrB;

    invoke-direct {v1}, LX/BrB;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 591069
    new-instance v1, LX/6Bu;

    invoke-direct {v1, v0}, LX/6Bu;-><init>(Ljava/util/Set;)V

    iput-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->l:LX/6Bu;

    .line 591070
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Landroid/text/Spannable;
    .locals 3

    .prologue
    .line 591056
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "   "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f080fc7

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;
    .locals 13

    .prologue
    .line 591045
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;

    monitor-enter v1

    .line 591046
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591047
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591048
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591049
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591050
    new-instance v3, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v6

    check-cast v6, LX/1Uf;

    invoke-static {v0}, LX/AjZ;->b(LX/0QB;)LX/AjZ;

    move-result-object v7

    check-cast v7, LX/8qo;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v10

    check-cast v10, LX/154;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v11

    check-cast v11, LX/11S;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;LX/8qo;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/154;LX/11S;LX/0Zb;)V

    .line 591051
    move-object v0, v3

    .line 591052
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591053
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591054
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 591043
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->j:LX/154;

    invoke-virtual {v0, p2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 591044
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p1, p3, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591032
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 591033
    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->k:LX/11S;

    sget-object v3, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->x()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-interface {v2, v3, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591034
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->k()I

    move-result v1

    .line 591035
    if-nez v1, :cond_0

    .line 591036
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->l()I

    move-result v1

    .line 591037
    if-nez v1, :cond_1

    .line 591038
    :goto_1
    return-object v0

    .line 591039
    :cond_0
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(Landroid/content/res/Resources;)Landroid/text/Spannable;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591040
    new-instance v2, Landroid/text/SpannableString;

    const v3, 0x7f0f00ca

    invoke-static {p0, p1, v1, v3}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 591041
    :cond_1
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(Landroid/content/res/Resources;)Landroid/text/Spannable;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591042
    new-instance v2, Landroid/text/SpannableString;

    const v3, 0x7f0f00cb

    invoke-static {p0, p1, v1, v3}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590967
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 591013
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v2, 0x0

    .line 591014
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591015
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/6X3;->c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 591016
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591017
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    sget-object v5, LX/1Ua;->d:LX/1Ua;

    const v6, 0x7f020a5f

    const/4 v7, -0x1

    invoke-direct {v3, v4, v5, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591018
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591019
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 591020
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->f:LX/1Uf;

    invoke-static {v0}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v0

    invoke-virtual {v3, v0, v2, v2, v2}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 591021
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 591022
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->f:LX/1Uf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v3

    invoke-virtual {v0, v3, v2, v2, v2}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 591023
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->f:LX/1Uf;

    sget-object v3, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->c:Landroid/view/View$OnClickListener;

    const/16 v5, 0xc8

    .line 591024
    iget-object v6, v0, LX/1Uf;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v5, v6}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v6

    move-object v0, v6

    .line 591025
    move-object v5, v0

    move-object v8, v2

    .line 591026
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->h:LX/0hy;

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->m:LX/0Zb;

    invoke-static {p2, v0, v2, v3}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 591027
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v7

    .line 591028
    new-instance v0, LX/BrD;

    .line 591029
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {v2}, LX/33N;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 591030
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_2
    move-object v3, v3

    .line 591031
    if-eqz v5, :cond_0

    :goto_3
    invoke-direct/range {v0 .. v7}, LX/BrD;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/util/List;)V

    return-object v0

    :cond_0
    move-object v5, v8

    goto :goto_3

    :cond_1
    move-object v5, v2

    move-object v8, v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x63215bea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590982
    check-cast p2, LX/BrD;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 590983
    const v1, 0x7f0d09ca

    invoke-static {p4, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 590984
    const v2, 0x7f0d0841

    invoke-static {p4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 590985
    const v4, 0x7f0d09cd

    invoke-static {p4, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 590986
    const v5, 0x7f0d09f1

    invoke-static {p4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/attachments/ui/AttachmentsSection;

    .line 590987
    const v6, 0x7f0d09f2

    invoke-static {p4, v6}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/resources/ui/FbTextView;

    .line 590988
    iget-object v7, p2, LX/BrD;->c:Landroid/net/Uri;

    sget-object p1, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v7, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 590989
    iget-object v7, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->g:LX/8qo;

    iget-object p1, p2, LX/BrD;->b:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 p3, 0x0

    invoke-interface {v7, v1, p1, p3}, LX/8qo;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLProfile;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 590990
    iget-object v1, p2, LX/BrD;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590991
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 590992
    iget-object v1, p2, LX/BrD;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590993
    iget-object v1, p2, LX/BrD;->e:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/BrD;->f:Landroid/view/View$OnClickListener;

    .line 590994
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 590995
    invoke-virtual {v4, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590996
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 590997
    invoke-virtual {v4, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590998
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 590999
    :goto_0
    iget-object v1, p2, LX/BrD;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 591000
    invoke-static {v1}, LX/36l;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 591001
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->l:LX/6Bu;

    invoke-virtual {v5, v2, v4}, Lcom/facebook/attachments/ui/AttachmentsSection;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Bu;)V

    .line 591002
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Lcom/facebook/attachments/ui/AttachmentsSection;->setVisibility(I)V

    .line 591003
    :goto_1
    const/4 v1, 0x1

    .line 591004
    invoke-static {p4}, LX/0vv;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v1, :cond_3

    :goto_2
    move v1, v1

    .line 591005
    if-eqz v1, :cond_0

    .line 591006
    iget-object v1, p2, LX/BrD;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 591007
    :cond_0
    iget-object v1, p2, LX/BrD;->g:Ljava/util/List;

    .line 591008
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v1, v2

    .line 591009
    invoke-virtual {v6, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 591010
    const/16 v1, 0x1f

    const v2, -0x513198fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 591011
    :cond_1
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 591012
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Lcom/facebook/attachments/ui/AttachmentsSection;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590978
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590979
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590980
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/6X3;->c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 590981
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 590968
    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v3, 0x0

    .line 590969
    const v0, 0x7f0d0841

    invoke-static {p4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 590970
    const v1, 0x7f0d09cd

    invoke-static {p4, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 590971
    const v2, 0x7f0d09f2

    invoke-static {p4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 590972
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590973
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590974
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590975
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590976
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590977
    return-void
.end method
