.class public Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/Bqo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bqo",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/1VF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588693
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Bqo;LX/1V0;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588688
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 588689
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->e:LX/1V0;

    .line 588690
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->g:LX/1VF;

    .line 588691
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->f:LX/Bqo;

    .line 588692
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 588671
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->f:LX/Bqo;

    const/4 v1, 0x0

    .line 588672
    new-instance v2, LX/Bqn;

    invoke-direct {v2, v0}, LX/Bqn;-><init>(LX/Bqo;)V

    .line 588673
    iget-object v3, v0, LX/Bqo;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bqm;

    .line 588674
    if-nez v3, :cond_0

    .line 588675
    new-instance v3, LX/Bqm;

    invoke-direct {v3, v0}, LX/Bqm;-><init>(LX/Bqo;)V

    .line 588676
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bqm;->a$redex0(LX/Bqm;LX/1De;IILX/Bqn;)V

    .line 588677
    move-object v2, v3

    .line 588678
    move-object v1, v2

    .line 588679
    move-object v0, v1

    .line 588680
    iget-object v1, v0, LX/Bqm;->a:LX/Bqn;

    iput-object p2, v1, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588681
    iget-object v1, v0, LX/Bqm;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 588682
    move-object v0, v0

    .line 588683
    iget-object v1, v0, LX/Bqm;->a:LX/Bqn;

    iput-object p3, v1, LX/Bqn;->b:LX/1Po;

    .line 588684
    iget-object v1, v0, LX/Bqm;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 588685
    move-object v0, v0

    .line 588686
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 588687
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;
    .locals 7

    .prologue
    .line 588694
    const-class v1, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;

    monitor-enter v1

    .line 588695
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588696
    sput-object v2, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588697
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588698
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588699
    new-instance p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Bqo;->a(LX/0QB;)LX/Bqo;

    move-result-object v4

    check-cast v4, LX/Bqo;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v6

    check-cast v6, LX/1VF;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;-><init>(Landroid/content/Context;LX/Bqo;LX/1V0;LX/1VF;)V

    .line 588700
    move-object v0, p0

    .line 588701
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588702
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588703
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 588670
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 588669
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 588665
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588666
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->g:LX/1VF;

    .line 588667
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588668
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1VF;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 588663
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588664
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 588662
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
