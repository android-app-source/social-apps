.class public Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/BqL;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final h:LX/1VF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590348
    new-instance v0, LX/3WU;

    invoke-direct {v0}, LX/3WU;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->a:LX/1Cz;

    .line 590349
    const-class v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/BqL;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590350
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590351
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->c:LX/BqL;

    .line 590352
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590353
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 590354
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 590355
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 590356
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->h:LX/1VF;

    .line 590357
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;
    .locals 10

    .prologue
    .line 590358
    const-class v1, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;

    monitor-enter v1

    .line 590359
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590360
    sput-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590363
    new-instance v3, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;

    invoke-static {v0}, LX/BqL;->a(LX/0QB;)LX/BqL;

    move-result-object v4

    check-cast v4, LX/BqL;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v9

    check-cast v9, LX/1VF;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;-><init>(LX/BqL;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;LX/1VF;)V

    .line 590364
    move-object v0, v3

    .line 590365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590369
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 590370
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590371
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590372
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 590373
    invoke-static {v0}, LX/BqL;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 590374
    invoke-static {v0}, LX/BqL;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 590375
    invoke-static {v0}, LX/BqL;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 590376
    iget-object v4, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v5, LX/1X6;

    sget-object v6, LX/1Ua;->a:LX/1Ua;

    const v7, 0x7f0a00ea

    const v8, 0x7f0a00ea

    invoke-direct {v5, p2, v6, v7, v8}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590377
    iget-object v4, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {p1, v4, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590378
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590379
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590380
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->c:LX/BqL;

    sget-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->b:Ljava/lang/String;

    .line 590381
    const-string v3, "IMPRESSION"

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, LX/BqL;->a(LX/BqL;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 590382
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 590383
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590384
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590385
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 590386
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->h:LX/1VF;

    invoke-virtual {v1, v0}, LX/1VF;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/BqL;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->c:LX/BqL;

    sget-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/BqL;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->c:LX/BqL;

    sget-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/BqL;->b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->c:LX/BqL;

    sget-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/BqL;->c(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
