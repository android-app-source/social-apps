.class public Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/views/MultiShareFixedTextOnImageView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<",
            "Lcom/facebook/feed/rows/views/MultiShareFixedTextOnImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1qb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590781
    new-instance v0, LX/3Wf;

    invoke-direct {v0}, LX/3Wf;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590777
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590778
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 590779
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->c:LX/1qb;

    .line 590780
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;
    .locals 5

    .prologue
    .line 590766
    const-class v1, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    monitor-enter v1

    .line 590767
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590768
    sput-object v2, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590769
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590770
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590771
    new-instance p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;LX/1qb;)V

    .line 590772
    move-object v0, p0

    .line 590773
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590774
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590775
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590757
    sget-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 590759
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590760
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v2, LX/2yZ;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->c:LX/1qb;

    .line 590761
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590762
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3, v0}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v3

    .line 590763
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590764
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590765
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590758
    const/4 v0, 0x1

    return v0
.end method
