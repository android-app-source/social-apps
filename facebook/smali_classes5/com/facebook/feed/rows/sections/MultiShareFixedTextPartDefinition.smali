.class public Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/views/MultiShareFixedTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "Lcom/facebook/feed/rows/views/MultiShareFixedTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;",
            "Lcom/facebook/feed/rows/views/MultiShareFixedTextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590753
    new-instance v0, LX/3We;

    invoke-direct {v0}, LX/3We;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590749
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590750
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 590751
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 590752
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;
    .locals 5

    .prologue
    .line 590738
    const-class v1, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    monitor-enter v1

    .line 590739
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590740
    sput-object v2, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590741
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590742
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590743
    new-instance p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 590744
    move-object v0, p0

    .line 590745
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590746
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590747
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590754
    sget-object v0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 590721
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590722
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590723
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590724
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4e8e3338

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590725
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;

    .line 590726
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 590727
    invoke-static {v1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 590728
    if-eqz v1, :cond_1

    .line 590729
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 590730
    if-eqz v1, :cond_1

    .line 590731
    invoke-static {v1}, LX/16z;->r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    if-gtz v2, :cond_0

    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-lez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->setDividerVisible(Z)V

    .line 590732
    :cond_1
    const/16 v1, 0x1f

    const v2, 0xdcc9b0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 590733
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590734
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 590735
    check-cast p4, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;

    .line 590736
    invoke-virtual {p4}, Lcom/facebook/feed/rows/views/MultiShareFixedTextView;->a()V

    .line 590737
    return-void
.end method
