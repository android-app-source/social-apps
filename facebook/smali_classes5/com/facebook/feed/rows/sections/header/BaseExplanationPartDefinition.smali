.class public Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Brz;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624239
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 624240
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 624241
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 624242
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    .line 624243
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1Ua;"
        }
    .end annotation

    .prologue
    .line 624209
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 624210
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    .line 624211
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1Ua;->d:LX/1Ua;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;
    .locals 6

    .prologue
    .line 624228
    const-class v1, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    monitor-enter v1

    .line 624229
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 624230
    sput-object v2, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 624231
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624232
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 624233
    new-instance p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;)V

    .line 624234
    move-object v0, p0

    .line 624235
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 624236
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624237
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 624238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1dl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1dl;"
        }
    .end annotation

    .prologue
    .line 624218
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 624219
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 624220
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 624221
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 624222
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    .line 624223
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 624224
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 624225
    :goto_0
    if-eqz v0, :cond_2

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    :goto_1
    return-object v0

    .line 624226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 624227
    :cond_2
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 624212
    check-cast p2, LX/Brz;

    .line 624213
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 624214
    const v0, 0x7f0d2ec4

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 624215
    iget-object v0, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1dl;

    move-result-object v0

    .line 624216
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    iget-object v4, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v3, v4, v0}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 624217
    const/4 v0, 0x0

    return-object v0
.end method
