.class public Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Bsl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bsl",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1VE;

.field private final g:LX/1V0;

.field private final h:LX/1VH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Bsl;LX/1VE;LX/1V0;LX/1VH;Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605735
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 605736
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->e:LX/Bsl;

    .line 605737
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->f:LX/1VE;

    .line 605738
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->g:LX/1V0;

    .line 605739
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->h:LX/1VH;

    .line 605740
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    .line 605741
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 605742
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->e:LX/Bsl;

    const/4 v1, 0x0

    .line 605743
    new-instance v2, LX/Bsk;

    invoke-direct {v2, v0}, LX/Bsk;-><init>(LX/Bsl;)V

    .line 605744
    iget-object v3, v0, LX/Bsl;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bsj;

    .line 605745
    if-nez v3, :cond_0

    .line 605746
    new-instance v3, LX/Bsj;

    invoke-direct {v3, v0}, LX/Bsj;-><init>(LX/Bsl;)V

    .line 605747
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bsj;->a$redex0(LX/Bsj;LX/1De;IILX/Bsk;)V

    .line 605748
    move-object v2, v3

    .line 605749
    move-object v1, v2

    .line 605750
    move-object v0, v1

    .line 605751
    iget-object v1, v0, LX/Bsj;->a:LX/Bsk;

    iput-object p2, v1, LX/Bsk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 605752
    iget-object v1, v0, LX/Bsj;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 605753
    move-object v0, v0

    .line 605754
    iget-object v1, v0, LX/Bsj;->a:LX/Bsk;

    iput-object p3, v1, LX/Bsk;->b:LX/1Pb;

    .line 605755
    iget-object v1, v0, LX/Bsj;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 605756
    move-object v0, v0

    .line 605757
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 605758
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 605759
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 605760
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->h:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;
    .locals 10

    .prologue
    .line 605761
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    monitor-enter v1

    .line 605762
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605763
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605766
    new-instance v3, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/Bsl;->a(LX/0QB;)LX/Bsl;

    move-result-object v5

    check-cast v5, LX/Bsl;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v6

    check-cast v6, LX/1VE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v8

    check-cast v8, LX/1VH;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/Bsl;LX/1VE;LX/1V0;LX/1VH;Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;)V

    .line 605767
    move-object v0, v3

    .line 605768
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605769
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605770
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 605772
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 605773
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 605774
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 605775
    invoke-super {p0, p1, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;)V

    .line 605776
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    .line 605777
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 605778
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605779
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 605780
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 605781
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 605782
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 605783
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 605784
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
