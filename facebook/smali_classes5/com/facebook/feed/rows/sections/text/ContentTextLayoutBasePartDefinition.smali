.class public Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/BtI;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Z

.field private final e:LX/1VF;

.field private final f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

.field private final g:LX/1nq;

.field private final h:LX/1z9;

.field private final i:LX/1Ua;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587642
    new-instance v0, LX/3VE;

    invoke-direct {v0}, LX/3VE;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;LX/1nq;LX/1Ua;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;LX/1VF;Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1z9;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Ua;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587632
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587633
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->d:Z

    .line 587634
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 587635
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->b:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

    .line 587636
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->e:LX/1VF;

    .line 587637
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    .line 587638
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->g:LX/1nq;

    .line 587639
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->h:LX/1z9;

    .line 587640
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->i:LX/1Ua;

    .line 587641
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587631
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 587621
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 587622
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587623
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587624
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->i:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587625
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->b:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587626
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f01029a

    const v2, 0x7f0a0158

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 587627
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f01029b

    const v3, 0x7f0a010c

    invoke-static {v1, v2, v3}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    .line 587628
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->g:LX/1nq;

    invoke-virtual {v1, v0}, LX/1nq;->c(I)LX/1nq;

    .line 587629
    iget-object v7, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->h:LX/1z9;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->g:LX/1nq;

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->g:LX/1nq;

    invoke-virtual {v2}, LX/1nq;->b()F

    move-result v2

    float-to-int v3, v2

    iget-boolean v5, p0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->d:Z

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/1z9;->a(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;IIZLX/1PT;)LX/1zA;

    move-result-object v0

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587630
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4415730f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587614
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/BtI;

    .line 587615
    const v1, 0x7f0d0081

    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, LX/BtI;->setTag(ILjava/lang/Object;)V

    .line 587616
    const/16 v1, 0x1f

    const v2, -0x79b1dc6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587617
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 587618
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587619
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587620
    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
