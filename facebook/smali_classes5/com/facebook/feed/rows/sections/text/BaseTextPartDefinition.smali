.class public Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/views/ContentTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Z

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1xc;

.field private final e:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

.field private final f:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587582
    new-instance v0, LX/3VD;

    invoke-direct {v0}, LX/3VD;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;Ljava/lang/Boolean;LX/1xc;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;",
            "Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;",
            "Ljava/lang/Boolean;",
            "LX/1xc;",
            "Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587597
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587598
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->c:LX/0Or;

    .line 587599
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->e:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

    .line 587600
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->b:Z

    .line 587601
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->d:LX/1xc;

    .line 587602
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 587603
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587596
    sget-object v0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 587604
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 587605
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587606
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587607
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->e:Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587608
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v1, LX/Bt0;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-direct {v1, p0, p2, v2}, LX/Bt0;-><init>(Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587609
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x75e83cd1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587588
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/feed/rows/views/ContentTextView;

    .line 587589
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 587590
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587591
    sget-object v2, LX/1vY;->DESCRIPTION:LX/1vY;

    invoke-static {p4, v2}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587592
    const v2, 0x7f0d0081

    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p4, v2, p0}, Lcom/facebook/feed/rows/views/ContentTextView;->setTag(ILjava/lang/Object;)V

    .line 587593
    iput-object v1, p4, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 587594
    invoke-virtual {p4}, Lcom/facebook/feed/rows/views/ContentTextView;->c()V

    .line 587595
    const/16 v1, 0x1f

    const v2, -0x2030752

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587587
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 587583
    check-cast p4, Lcom/facebook/feed/rows/views/ContentTextView;

    .line 587584
    const/4 v0, 0x0

    .line 587585
    iput-object v0, p4, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 587586
    return-void
.end method
