.class public Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/2dX;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/Bt5;",
        "LX/1Pn;",
        "TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590158
    new-instance v0, LX/3WN;

    invoke-direct {v0}, LX/3WN;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590155
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590156
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;->b:LX/0Ot;

    .line 590157
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 590154
    sget-object v0, Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 590152
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p3, LX/1Pn;

    .line 590153
    new-instance v0, LX/Bt5;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;->b:LX/0Ot;

    invoke-direct {v0, v1, v2, p2}, LX/Bt5;-><init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/graphql/model/GraphQLStory;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x32976bc1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590147
    check-cast p2, LX/Bt5;

    .line 590148
    iget-object v1, p2, LX/Bt5;->a:LX/BtK;

    .line 590149
    iput-object p4, v1, LX/BtK;->e:Landroid/view/View;

    .line 590150
    check-cast p4, LX/2dX;

    iget-object v1, p2, LX/Bt5;->a:LX/BtK;

    invoke-interface {p4, v1}, LX/2dX;->setCopyTextGestureListener(LX/BtK;)V

    .line 590151
    const/16 v1, 0x1f

    const v2, 0x167f96eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590146
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 590144
    check-cast p4, LX/2dX;

    invoke-interface {p4}, LX/2dX;->a()V

    .line 590145
    return-void
.end method
