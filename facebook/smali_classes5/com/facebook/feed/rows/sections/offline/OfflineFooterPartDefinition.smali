.class public Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C96;",
        "TE;",
        "LX/Bsz;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/Boq;

.field private final c:Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

.field private final d:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

.field private final e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0qn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587397
    new-instance v0, LX/3Ux;

    invoke-direct {v0}, LX/3Ux;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/Boq;Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0Ot;LX/0qn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Boq;",
            "Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;",
            "Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;",
            ">;",
            "LX/0qn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587398
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587399
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->b:LX/Boq;

    .line 587400
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->c:Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    .line 587401
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->d:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    .line 587402
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 587403
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->f:LX/0Ot;

    .line 587404
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->g:LX/0qn;

    .line 587405
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;
    .locals 10

    .prologue
    .line 587406
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    monitor-enter v1

    .line 587407
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587408
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587411
    new-instance v3, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    invoke-static {v0}, LX/Boq;->a(LX/0QB;)LX/Boq;

    move-result-object v4

    check-cast v4, LX/Boq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    const/16 v8, 0x8f1

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v9

    check-cast v9, LX/0qn;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;-><init>(LX/Boq;Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0Ot;LX/0qn;)V

    .line 587412
    move-object v0, v3

    .line 587413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587417
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 587418
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    .line 587419
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587420
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587421
    const v1, 0x7f0d00fd

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->d:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 587422
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->c:Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587423
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v3, LX/20d;

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-object v2, p3

    check-cast v2, LX/1Pr;

    invoke-virtual {v4, p2, v2}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v2

    invoke-direct {v3, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587424
    check-cast p3, LX/1Pr;

    new-instance v1, LX/C95;

    invoke-direct {v1, v0}, LX/C95;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C96;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x20191e34

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587425
    check-cast p2, LX/C96;

    check-cast p4, LX/Bsz;

    .line 587426
    invoke-virtual {p4}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v1

    .line 587427
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, LX/Bsz;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 587428
    iget-boolean v1, p2, LX/C96;->a:Z

    move v1, v1

    .line 587429
    if-nez v1, :cond_0

    .line 587430
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->b:LX/Boq;

    .line 587431
    iput-object p4, v1, LX/Boq;->d:LX/Bsz;

    .line 587432
    :cond_0
    const/16 v1, 0x1f

    const v2, 0xdea065

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 587433
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 587434
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->g:LX/0qn;

    .line 587435
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587436
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->d:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
