.class public Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589536
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589534
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589535
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;
    .locals 4

    .prologue
    .line 589523
    const-class v1, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    monitor-enter v1

    .line 589524
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589525
    sput-object v2, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589526
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589527
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589528
    new-instance p0, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589529
    move-object v0, p0

    .line 589530
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589531
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589532
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589522
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 589509
    const/4 v0, 0x0

    .line 589510
    new-instance p0, LX/Brg;

    invoke-direct {p0}, LX/Brg;-><init>()V

    .line 589511
    sget-object p2, LX/Brh;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Brf;

    .line 589512
    if-nez p2, :cond_0

    .line 589513
    new-instance p2, LX/Brf;

    invoke-direct {p2}, LX/Brf;-><init>()V

    .line 589514
    :cond_0
    invoke-static {p2, p1, v0, v0, p0}, LX/Brf;->a$redex0(LX/Brf;LX/1De;IILX/Brg;)V

    .line 589515
    move-object p0, p2

    .line 589516
    move-object v0, p0

    .line 589517
    move-object v0, v0

    .line 589518
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589521
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 589520
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589519
    sget-object v0, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
