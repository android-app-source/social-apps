.class public Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/Brp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589495
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Brp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589492
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589493
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->e:LX/Brp;

    .line 589494
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;
    .locals 5

    .prologue
    .line 589480
    const-class v1, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;

    monitor-enter v1

    .line 589481
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589482
    sput-object v2, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589483
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589484
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589485
    new-instance p0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Brp;->a(LX/0QB;)LX/Brp;

    move-result-object v4

    check-cast v4, LX/Brp;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;-><init>(Landroid/content/Context;LX/Brp;)V

    .line 589486
    move-object v0, p0

    .line 589487
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589488
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589489
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589491
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 589496
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->e:LX/Brp;

    const/4 v1, 0x0

    .line 589497
    new-instance p0, LX/Bro;

    invoke-direct {p0, v0}, LX/Bro;-><init>(LX/Brp;)V

    .line 589498
    sget-object p3, LX/Brp;->a:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/Brn;

    .line 589499
    if-nez p3, :cond_0

    .line 589500
    new-instance p3, LX/Brn;

    invoke-direct {p3}, LX/Brn;-><init>()V

    .line 589501
    :cond_0
    invoke-static {p3, p1, v1, v1, p0}, LX/Brn;->a$redex0(LX/Brn;LX/1De;IILX/Bro;)V

    .line 589502
    move-object p0, p3

    .line 589503
    move-object v1, p0

    .line 589504
    move-object v0, v1

    .line 589505
    iget-object v1, v0, LX/Brn;->a:LX/Bro;

    iput-object p2, v1, LX/Bro;->a:Ljava/lang/Object;

    .line 589506
    iget-object v1, v0, LX/Brn;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 589507
    move-object v0, v0

    .line 589508
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589477
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 589478
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589479
    sget-object v0, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
