.class public Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589014
    new-instance v0, LX/3W5;

    invoke-direct {v0}, LX/3W5;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589012
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 589013
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;
    .locals 3

    .prologue
    .line 588999
    const-class v1, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    monitor-enter v1

    .line 589000
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589001
    sput-object v2, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589002
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589003
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 589004
    new-instance v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;-><init>()V

    .line 589005
    move-object v0, v0

    .line 589006
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589007
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589008
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589009
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 589011
    sget-object v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589010
    const/4 v0, 0x1

    return v0
.end method
