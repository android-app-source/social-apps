.class public Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJZ;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:LX/HLT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602499
    new-instance v0, LX/3Zx;

    invoke-direct {v0}, LX/3Zx;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602495
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602496
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->c:LX/HLT;

    .line 602497
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 602498
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 602484
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;

    monitor-enter v1

    .line 602485
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602486
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602489
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;-><init>(LX/HLT;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 602490
    move-object v0, p0

    .line 602491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602483
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 602466
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v2, 0x0

    .line 602467
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v7, v0

    .line 602468
    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 602469
    if-eqz v1, :cond_0

    .line 602470
    iget-object v8, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    .line 602471
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 602472
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 602473
    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 602474
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->c:LX/HLT;

    .line 602475
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 602476
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 602477
    invoke-virtual {v0, v1, p3, v3, v4}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 602478
    invoke-interface {v7}, LX/9uc;->cX()LX/174;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, LX/9uc;->cX()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v7}, LX/9uc;->cX()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 602479
    :goto_0
    invoke-interface {v7}, LX/9uc;->cY()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, LX/9uc;->cY()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v7}, LX/9uc;->cY()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 602480
    :goto_1
    new-instance v0, LX/HJZ;

    invoke-interface {v7}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, LX/HJZ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_1
    move-object v3, v2

    .line 602481
    goto :goto_0

    :cond_2
    move-object v4, v2

    .line 602482
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1c9ec3b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602439
    check-cast p2, LX/HJZ;

    check-cast p4, Landroid/widget/LinearLayout;

    const/4 p1, 0x0

    .line 602440
    invoke-virtual {p4}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0213ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 602441
    const v1, 0x7f0d237a

    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 602442
    iget-object v2, p2, LX/HJZ;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 602443
    iget-object v2, p2, LX/HJZ;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 602444
    invoke-virtual {v1, p1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    .line 602445
    iget-object v2, p2, LX/HJZ;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 602446
    iget-object v2, p2, LX/HJZ;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 602447
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 602448
    :goto_0
    invoke-virtual {v1, p1}, Lcom/facebook/fig/listitem/FigListItem;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/button/FigButton;

    .line 602449
    iget-object p0, p2, LX/HJZ;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602450
    iget-object p0, p2, LX/HJZ;->d:Ljava/lang/String;

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 602451
    invoke-virtual {v2, p1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 602452
    const/16 p0, 0x82

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 602453
    iget-object v2, p2, LX/HJZ;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 602454
    :cond_0
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x71e16516

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 602455
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 602456
    :cond_2
    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 602457
    iget-object v2, p2, LX/HJZ;->e:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    .line 602458
    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602463
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602464
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602465
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 602459
    check-cast p4, Landroid/widget/LinearLayout;

    .line 602460
    const v0, 0x7f0d237a

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 602461
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602462
    return-void
.end method
