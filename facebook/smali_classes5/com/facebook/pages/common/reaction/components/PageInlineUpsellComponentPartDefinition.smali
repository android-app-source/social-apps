.class public Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJi;",
        "TE;",
        "LX/HLw;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field public c:LX/23P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602523
    new-instance v0, LX/3Zz;

    invoke-direct {v0}, LX/3Zz;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602553
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602554
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->b:LX/HLT;

    .line 602555
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->c:LX/23P;

    .line 602556
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;
    .locals 5

    .prologue
    .line 602542
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;

    monitor-enter v1

    .line 602543
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602544
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602547
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;-><init>(LX/HLT;LX/23P;)V

    .line 602548
    move-object v0, p0

    .line 602549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602557
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 602536
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602537
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602538
    new-instance v1, LX/HJi;

    invoke-interface {v0}, LX/9uc;->aL()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9uc;->H()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 602539
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 602540
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 602541
    invoke-virtual {v5, v0, p3, v6, v7}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, LX/HJi;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x9f3034e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602527
    check-cast p2, LX/HJi;

    check-cast p4, LX/HLw;

    .line 602528
    iget-object v1, p2, LX/HJi;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HJi;->b:Ljava/lang/String;

    iget-object p1, p2, LX/HJi;->c:Ljava/lang/String;

    iget-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->c:LX/23P;

    .line 602529
    iget-object p0, p4, LX/HLw;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 602530
    iget-object p0, p4, LX/HLw;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 602531
    iget-object p0, p4, LX/HLw;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, p3}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 602532
    iget-object p0, p4, LX/HLw;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 602533
    iget-object v1, p2, LX/HJi;->d:Landroid/view/View$OnClickListener;

    .line 602534
    iget-object v2, p4, LX/HLw;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602535
    const/16 v1, 0x1f

    const v2, 0x5e2dd633

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602524
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602525
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602526
    invoke-interface {v0}, LX/9uc;->aL()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aL()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
