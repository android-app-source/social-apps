.class public Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJq;",
        "TE;",
        "LX/3Zu;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final g:Lcom/facebook/common/callercontext/CallerContext;

.field public static final h:Lcom/facebook/location/FbLocationOperationParams;

.field private static l:LX/0Xm;


# instance fields
.field public b:LX/0y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1sS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field public j:Lcom/facebook/location/ImmutableLocation;

.field private k:Lcom/facebook/location/ImmutableLocation;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 602357
    const-class v0, LX/3Zu;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 602358
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x1d4c0

    .line 602359
    iput-wide v2, v0, LX/1S7;->b:J

    .line 602360
    move-object v0, v0

    .line 602361
    const/high16 v1, 0x43fa0000    # 500.0f

    .line 602362
    iput v1, v0, LX/1S7;->c:F

    .line 602363
    move-object v0, v0

    .line 602364
    const-wide/16 v2, 0x1388

    .line 602365
    iput-wide v2, v0, LX/1S7;->d:J

    .line 602366
    move-object v0, v0

    .line 602367
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->h:Lcom/facebook/location/FbLocationOperationParams;

    .line 602368
    new-instance v0, LX/3Zv;

    invoke-direct {v0}, LX/3Zv;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602354
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602355
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->i:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 602356
    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 602351
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->a()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4056800000000000L    # 90.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 602352
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->a()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->b()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 602353
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;
    .locals 8

    .prologue
    .line 602338
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;

    monitor-enter v1

    .line 602339
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602340
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602341
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602342
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602343
    new-instance v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {v3, v4}, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 602344
    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v4

    check-cast v4, LX/0y2;

    invoke-static {v0}, LX/1sS;->b(LX/0QB;)LX/1sS;

    move-result-object v5

    check-cast v5, LX/1sS;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v6

    check-cast v6, LX/0y3;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object p0

    check-cast p0, LX/6aG;

    .line 602345
    iput-object v4, v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->b:LX/0y2;

    iput-object v5, v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->c:LX/1sS;

    iput-object v6, v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->d:LX/0y3;

    iput-object v7, v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->e:LX/1Ck;

    iput-object p0, v3, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->f:LX/6aG;

    .line 602346
    move-object v0, v3

    .line 602347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static e(Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 602335
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->j:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    .line 602336
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->b:LX/0y2;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v2, v3}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->j:Lcom/facebook/location/ImmutableLocation;

    .line 602337
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->j:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->k:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->f:LX/6aG;

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->j:Lcom/facebook/location/ImmutableLocation;

    iget-object v3, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->k:Lcom/facebook/location/ImmutableLocation;

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-string v6, ""

    invoke-virtual/range {v1 .. v6}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602334
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 602302
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602303
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 602304
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v8, v0

    .line 602305
    invoke-interface {v8}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 602306
    if-eqz v1, :cond_0

    .line 602307
    iget-object v9, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->i:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    const/4 v2, 0x0

    .line 602308
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 602309
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 602310
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v9, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 602311
    :cond_0
    invoke-interface {v8}, LX/9uc;->bu()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;

    .line 602312
    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 602313
    if-eqz v0, :cond_1

    .line 602314
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602315
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 602316
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 602317
    invoke-interface {v8}, LX/9uc;->bl()Z

    move-result v1

    .line 602318
    if-eqz v1, :cond_3

    .line 602319
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->k:Lcom/facebook/location/ImmutableLocation;

    .line 602320
    :cond_3
    new-instance v0, LX/HJq;

    const/4 v3, 0x0

    invoke-interface {v8}, LX/9uc;->bw()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    move-result-object v4

    invoke-interface {v8}, LX/9uc;->dv()I

    move-result v5

    invoke-interface {v8}, LX/9uc;->ca()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v6

    move-object v2, v7

    invoke-direct/range {v0 .. v6}, LX/HJq;-><init>(ZLjava/util/ArrayList;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;)V

    return-object v0

    .line 602321
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x153d0084

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602327
    check-cast p2, LX/HJq;

    check-cast p4, LX/3Zu;

    .line 602328
    iget-boolean v4, p2, LX/HJq;->e:Z

    if-eqz v4, :cond_0

    .line 602329
    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->d:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->a()LX/0yG;

    move-result-object v4

    sget-object v5, LX/0yG;->OKAY:LX/0yG;

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_0
    move v4, v4

    .line 602330
    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->c:LX/1sS;

    invoke-virtual {v4}, LX/0SQ;->isDone()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->j:Lcom/facebook/location/ImmutableLocation;

    if-eqz v4, :cond_1

    .line 602331
    :cond_0
    :goto_1
    iget-boolean v5, p2, LX/HJq;->e:Z

    iget-object v6, p2, LX/HJq;->a:Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->e(Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p2, LX/HJq;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    iget v9, p2, LX/HJq;->c:I

    iget-object v10, p2, LX/HJq;->d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-object v4, p4

    invoke-virtual/range {v4 .. v10}, LX/3Zu;->a(ZLjava/util/ArrayList;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;)V

    .line 602332
    const/16 v1, 0x1f

    const v2, 0x17d6ee49

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 602333
    :cond_1
    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->e:LX/1Ck;

    const-string v5, "page_about_map_with_distance_get_location_task_key"

    new-instance v6, LX/HJo;

    invoke-direct {v6, p0}, LX/HJo;-><init>(Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;)V

    new-instance v7, LX/HJp;

    invoke-direct {v7, p0, p4}, LX/HJp;-><init>(Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;LX/3Zu;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 602324
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 602325
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602326
    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 602322
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->e:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 602323
    return-void
.end method
