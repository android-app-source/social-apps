.class public Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HIf;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600584
    new-instance v0, LX/3ZY;

    invoke-direct {v0}, LX/3ZY;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600581
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600582
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->b:LX/0Uh;

    .line 600583
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 600585
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;

    monitor-enter v1

    .line 600586
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600587
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600588
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600589
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600590
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;-><init>(LX/0Uh;)V

    .line 600591
    move-object v0, p0

    .line 600592
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600593
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600594
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600580
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 600568
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600569
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 600570
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 600571
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 600572
    invoke-interface {v2}, LX/9uc;->bd()LX/0Px;

    move-result-object v5

    .line 600573
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_2

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;

    .line 600574
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->b()LX/174;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->b()LX/174;

    move-result-object v7

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 600575
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->b()LX/174;

    move-result-object v7

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600576
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->a()LX/174;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->a()LX/174;

    move-result-object v7

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 600577
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageAboutInfoGridComponentModel$InfoRowsModel;->a()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600578
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 600579
    :cond_2
    new-instance v0, LX/HIf;

    invoke-interface {v2}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v4}, LX/HIf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2ea1756f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600550
    check-cast p2, LX/HIf;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;

    .line 600551
    iget-object v1, p2, LX/HIf;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HIf;->b:Ljava/lang/String;

    iget-object v4, p2, LX/HIf;->c:Ljava/util/ArrayList;

    iget-object v5, p2, LX/HIf;->d:Ljava/util/ArrayList;

    const/4 p2, 0x0

    .line 600552
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v6, v7, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 600553
    :cond_0
    const/16 v6, 0x8

    invoke-virtual {p4, v6}, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->setVisibility(I)V

    .line 600554
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x28deb67b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600555
    :cond_1
    iget-object v6, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    sget-object p0, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 600556
    iget-object v6, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600557
    iget-object v6, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->b:Landroid/widget/TableLayout;

    invoke-virtual {v6}, Landroid/widget/TableLayout;->removeAllViews()V

    move p3, p2

    .line 600558
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge p3, v6, :cond_2

    .line 600559
    invoke-virtual {p4}, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 600560
    const v7, 0x7f030e04

    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->b:Landroid/widget/TableLayout;

    invoke-virtual {v6, v7, p0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TableRow;

    .line 600561
    const v7, 0x7f0d223c

    invoke-virtual {v6, v7}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/widget/text/BetterTextView;

    .line 600562
    const p0, 0x7f0d223d

    invoke-virtual {v6, p0}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/text/BetterTextView;

    .line 600563
    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v7, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600564
    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {p0, v7}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600565
    iget-object v7, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->b:Landroid/widget/TableLayout;

    invoke-virtual {v7, v6}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 600566
    add-int/lit8 v6, p3, 0x1

    move p3, v6

    goto :goto_1

    .line 600567
    :cond_2
    invoke-virtual {p4, p2}, Lcom/facebook/pages/common/reaction/ui/PageAboutInfoGridComponentView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 600547
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x0

    .line 600548
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 600549
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->b:LX/0Uh;

    const/16 v3, 0x5ae

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/9uc;->dc()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->bd()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
