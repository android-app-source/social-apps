.class public Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJE;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601188
    new-instance v0, LX/3Zj;

    invoke-direct {v0}, LX/3Zj;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601189
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601190
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    .line 601191
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 601192
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->d:LX/HLT;

    .line 601193
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 601194
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;

    monitor-enter v1

    .line 601195
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601196
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601197
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601198
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601199
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v5

    check-cast v5, LX/HLT;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;-><init>(Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/HLT;)V

    .line 601200
    move-object v0, p0

    .line 601201
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601202
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601203
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601204
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601205
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 601206
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601207
    const v0, 0x7f020090

    .line 601208
    new-instance v1, LX/HIy;

    const/high16 v2, 0x3f800000    # 1.0f

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-direct {v1, v0, v2, v3}, LX/HIy;-><init>(IFLandroid/widget/ImageView$ScaleType;)V

    .line 601209
    new-instance v0, LX/HIx;

    invoke-direct {v0, v1}, LX/HIx;-><init>(LX/HIw;)V

    .line 601210
    const v1, 0x7f0d23c4

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    move-object v0, p3

    .line 601211
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 601212
    const v1, 0x7f0d23c5

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 601213
    const v0, 0x7f0d23c6

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    const-string v2, ""

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 601214
    new-instance v0, LX/HJE;

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->d:LX/HLT;

    .line 601215
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 601216
    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 601217
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 601218
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 601219
    invoke-virtual {v1, v2, p3, v3, v4}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HJE;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x16101cc8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601220
    check-cast p2, LX/HJE;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    .line 601221
    iget-object v1, p2, LX/HJE;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601222
    const/16 v1, 0x1f

    const v2, 0x2477fc15

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601223
    const/4 v0, 0x1

    move v0, v0

    .line 601224
    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 601225
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    .line 601226
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601227
    return-void
.end method
