.class public Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJn;",
        "TE;",
        "LX/HLx;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601890
    new-instance v0, LX/3Zn;

    invoke-direct {v0}, LX/3Zn;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601933
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601934
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;
    .locals 3

    .prologue
    .line 601922
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;

    monitor-enter v1

    .line 601923
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601924
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601925
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601926
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 601927
    new-instance v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;-><init>()V

    .line 601928
    move-object v0, v0

    .line 601929
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601930
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601931
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601921
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 601935
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601936
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601937
    new-instance v1, LX/HJn;

    invoke-interface {v0}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/HJn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1d8f98f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601906
    check-cast p2, LX/HJn;

    check-cast p4, LX/HLx;

    .line 601907
    iget-object v1, p2, LX/HJn;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HJn;->b:Ljava/lang/String;

    iget-object v4, p2, LX/HJn;->c:Ljava/lang/String;

    const/4 p2, 0x0

    const/4 v6, 0x0

    .line 601908
    iget-object v5, p4, LX/HLx;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601909
    iget-object v5, p4, LX/HLx;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601910
    iget-object v5, p4, LX/HLx;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601911
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    .line 601912
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    .line 601913
    invoke-virtual {p4}, LX/HLx;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    if-eqz p0, :cond_1

    const v5, 0x7f0a00d3

    :goto_0
    invoke-virtual {p3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 601914
    iget-object p3, p4, LX/HLx;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p3, v5}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 601915
    if-eqz p0, :cond_2

    iget-object p0, p4, LX/HLx;->a:LX/0wM;

    const p3, 0x7f0219a9

    invoke-virtual {p0, p3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 601916
    :goto_1
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p3

    invoke-virtual {v5, p2, p2, p0, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 601917
    iget-object p0, p4, LX/HLx;->d:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    move-object v5, v6

    :cond_0
    invoke-virtual {p0, v5, v6, v6, v6}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 601918
    const/16 v1, 0x1f

    const v2, 0x1b117406

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 601919
    :cond_1
    const v5, 0x7f0a00d4

    goto :goto_0

    .line 601920
    :cond_2
    iget-object p0, p4, LX/HLx;->a:LX/0wM;

    const p3, 0x7f020a0f

    invoke-virtual {p0, p3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601892
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601893
    if-eqz p1, :cond_0

    .line 601894
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601895
    if-eqz v0, :cond_0

    .line 601896
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601897
    invoke-interface {v0}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601898
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601899
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601900
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601901
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601902
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601903
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601904
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601905
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 601891
    return-void
.end method
