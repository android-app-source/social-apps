.class public Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HIe;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600633
    new-instance v0, LX/3ZZ;

    invoke-direct {v0}, LX/3ZZ;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600630
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600631
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 600632
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 600619
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;

    monitor-enter v1

    .line 600620
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600621
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600622
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600623
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600624
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 600625
    move-object v0, p0

    .line 600626
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600627
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600628
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600618
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 600598
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v2, 0x0

    .line 600599
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v7, v0

    .line 600600
    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 600601
    if-eqz v1, :cond_0

    .line 600602
    iget-object v8, p0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    .line 600603
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 600604
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 600605
    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600606
    :cond_0
    new-instance v0, LX/HIe;

    invoke-interface {v7}, LX/9uc;->cX()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/HIe;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1d610584

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600610
    check-cast p2, LX/HIe;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;

    .line 600611
    iget-object v1, p2, LX/HIe;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HIe;->b:Ljava/lang/String;

    .line 600612
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600613
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setVisibility(I)V

    .line 600614
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    const/4 p1, 0x6

    invoke-virtual {p0, p1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMaxLines(I)V

    .line 600615
    const p0, 0x7f0d2237

    invoke-virtual {p4, p0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 600616
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p2, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 600617
    const/16 v1, 0x1f

    const v2, 0x2434936e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600607
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600608
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600609
    invoke-interface {v0}, LX/9uc;->cX()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cX()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
