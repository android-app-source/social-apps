.class public Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJr;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/HLl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600663
    new-instance v0, LX/3Za;

    invoke-direct {v0}, LX/3Za;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/HLl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600666
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600667
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->b:LX/HLT;

    .line 600668
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->c:LX/HLl;

    .line 600669
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJr;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HJr;"
        }
    .end annotation

    .prologue
    .line 600664
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v12

    .line 600665
    new-instance v1, LX/HJr;

    invoke-interface {v12}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12}, LX/9uc;->aO()LX/174;

    move-result-object v3

    invoke-interface {v12}, LX/9uc;->bv()LX/174;

    move-result-object v4

    invoke-interface {v12}, LX/9uc;->an()LX/0Px;

    move-result-object v5

    invoke-interface {v12}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v6

    invoke-interface {v12}, LX/9uc;->aU()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v12}, LX/9uc;->du()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v12}, LX/9uc;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v9, v10, v0, v11, v13}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-interface {v12}, LX/9uc;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v10

    invoke-interface {v10}, LX/174;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v12}, LX/9uc;->P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v11, v13, v0, v14, v15}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-interface {v12}, LX/9uc;->P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v12

    invoke-interface {v12}, LX/174;->a()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->c:LX/HLl;

    invoke-direct/range {v1 .. v13}, LX/HJr;-><init>(Ljava/lang/String;LX/174;LX/174;LX/0Px;LX/1Fb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;LX/HLl;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 600650
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;

    monitor-enter v1

    .line 600651
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600652
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600653
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600654
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600655
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, LX/HLl;->a(LX/0QB;)LX/HLl;

    move-result-object v4

    check-cast v4, LX/HLl;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;-><init>(LX/HLT;LX/HLl;)V

    .line 600656
    move-object v0, p0

    .line 600657
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600658
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600659
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/HJr;Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HJr;",
            "Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 600661
    iget-object v1, p0, LX/HJr;->f:LX/HLj;

    iget-object v2, p0, LX/HJr;->b:LX/174;

    iget-object v3, p0, LX/HJr;->c:LX/174;

    iget-object v4, p0, LX/HJr;->a:LX/0Px;

    iget-object v5, p0, LX/HJr;->d:LX/1Fb;

    iget-object v6, p0, LX/HJr;->e:Ljava/lang/String;

    iget-object v7, p0, LX/HJr;->g:Ljava/lang/String;

    iget-object v8, p0, LX/HJr;->h:Landroid/view/View$OnClickListener;

    iget-object v9, p0, LX/HJr;->i:Ljava/lang/String;

    iget-object v10, p0, LX/HJr;->j:Landroid/view/View$OnClickListener;

    iget-object v11, p0, LX/HJr;->k:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v11}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a(LX/HLj;LX/174;LX/174;LX/0Px;LX/1Fb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 600662
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600649
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 600648
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3b174428

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600647
    check-cast p2, LX/HJr;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;

    invoke-static {p2, p4}, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->a(LX/HJr;Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;)V

    const/16 v1, 0x1f

    const v2, -0xbf96086

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600644
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600645
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600646
    invoke-interface {v0}, LX/9uc;->aO()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aO()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aU()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bv()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bv()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->du()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 600636
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;

    .line 600637
    const/4 p1, 0x0

    .line 600638
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->r:LX/HLj;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 600639
    iput-object p1, p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->r:LX/HLj;

    .line 600640
    iput-object p1, p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->q:LX/HM0;

    .line 600641
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600642
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->o:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600643
    return-void
.end method
