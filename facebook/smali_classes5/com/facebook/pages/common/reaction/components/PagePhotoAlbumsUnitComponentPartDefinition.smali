.class public Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKN;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/2U1;

.field public final c:LX/8I0;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601340
    new-instance v0, LX/3Zl;

    invoke-direct {v0}, LX/3Zl;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/2U1;LX/8I0;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2U1;",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601461
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601462
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->b:LX/2U1;

    .line 601463
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->c:LX/8I0;

    .line 601464
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->d:LX/0Or;

    .line 601465
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/view/View$OnClickListener;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 601460
    new-instance v0, LX/HKL;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object/from16 v4, p6

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v9}, LX/HKL;-><init>(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;LX/2km;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 601449
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;

    monitor-enter v1

    .line 601450
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601451
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601452
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601453
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601454
    new-instance v5, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;

    invoke-static {v0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v3

    check-cast v3, LX/2U1;

    invoke-static {v0}, LX/8I0;->b(LX/0QB;)LX/8I0;

    move-result-object v4

    check-cast v4, LX/8I0;

    const/16 p0, 0x1a1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;-><init>(LX/2U1;LX/8I0;LX/0Or;)V

    .line 601455
    move-object v0, v5

    .line 601456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 601443
    if-nez p0, :cond_0

    .line 601444
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 601445
    :goto_0
    return-object v0

    .line 601446
    :cond_0
    if-lez p0, :cond_1

    .line 601447
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0085

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 601448
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid album size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601432
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 601433
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->b:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 601434
    if-eqz v0, :cond_0

    .line 601435
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 601436
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 601437
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v1

    .line 601438
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 601439
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 601440
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601441
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 601442
    :cond_0
    return-object v2
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "ITE;)",
            "Ljava/util/List",
            "<",
            "LX/HKM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601410
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v1

    invoke-interface {v1}, LX/9uc;->bL()LX/0Px;

    move-result-object v13

    .line 601411
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 601412
    const/4 v1, 0x0

    move v12, v1

    :goto_0
    move/from16 v0, p2

    if-ge v12, v0, :cond_6

    .line 601413
    invoke-virtual {v13, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 601414
    invoke-virtual {v13, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v3

    .line 601415
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 601416
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 601417
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    .line 601418
    :goto_2
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 601419
    :goto_3
    if-eqz v1, :cond_3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;->a()I

    move-result v1

    move v2, v1

    :goto_4
    move-object/from16 v1, p3

    .line 601420
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 601421
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    .line 601422
    :goto_5
    if-eqz v1, :cond_5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    .line 601423
    :goto_6
    new-instance v16, LX/HKM;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->c(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->b(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v9

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    invoke-static/range {v1 .. v9}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/view/View$OnClickListener;

    move-result-object v9

    move-object/from16 v4, v16

    move-object v6, v11

    move-object v7, v10

    move-object v8, v15

    invoke-direct/range {v4 .. v9}, LX/HKM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601424
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto/16 :goto_0

    .line 601425
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 601426
    :cond_1
    const-string v1, ""

    move-object v10, v1

    goto/16 :goto_2

    .line 601427
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 601428
    :cond_3
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_4

    .line 601429
    :cond_4
    const/4 v1, 0x0

    goto :goto_5

    .line 601430
    :cond_5
    const/4 v1, 0x0

    move-object v11, v1

    goto :goto_6

    .line 601431
    :cond_6
    return-object v14
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 601401
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601402
    invoke-interface {v0}, LX/9uc;->bL()LX/0Px;

    move-result-object v3

    .line 601403
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    move v0, v1

    .line 601404
    :goto_0
    return v0

    .line 601405
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    .line 601406
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 601407
    goto :goto_0

    .line 601408
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 601409
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4

    .prologue
    .line 601383
    new-instance v1, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v0}, LX/89I;-><init>(JLX/2rw;)V

    .line 601384
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->b:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 601385
    if-nez v0, :cond_0

    .line 601386
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 601387
    :goto_0
    return-object v0

    .line 601388
    :cond_0
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 601389
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 601390
    if-eqz v2, :cond_1

    .line 601391
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 601392
    :cond_1
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 601393
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_3

    .line 601394
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v2

    .line 601395
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 601396
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 601397
    :goto_1
    if-eqz v0, :cond_2

    .line 601398
    iput-object v0, v1, LX/89I;->d:Ljava/lang/String;

    .line 601399
    :cond_2
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    goto :goto_0

    .line 601400
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static c(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 4

    .prologue
    .line 601352
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->b:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 601353
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 601354
    if-eqz v0, :cond_0

    .line 601355
    iget-object v2, v0, LX/8Dk;->b:LX/0am;

    move-object v2, v2

    .line 601356
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 601357
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v2, v2

    .line 601358
    if-nez v2, :cond_0

    .line 601359
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v2

    const/4 v3, 0x1

    .line 601360
    iput-boolean v3, v2, LX/0SK;->d:Z

    .line 601361
    move-object v2, v2

    .line 601362
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v3, v3

    .line 601363
    iput-object v3, v2, LX/0SK;->c:Ljava/lang/String;

    .line 601364
    move-object v2, v2

    .line 601365
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v3, v3

    .line 601366
    iput-object v3, v2, LX/0SK;->f:Ljava/lang/String;

    .line 601367
    move-object v2, v2

    .line 601368
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v1, v3

    .line 601369
    iput-object v1, v2, LX/0SK;->e:Ljava/lang/String;

    .line 601370
    move-object v1, v2

    .line 601371
    iput-object p1, v1, LX/0SK;->a:Ljava/lang/String;

    .line 601372
    move-object v2, v1

    .line 601373
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 601374
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 601375
    iput-object v1, v2, LX/0SK;->b:Ljava/lang/String;

    .line 601376
    move-object v1, v2

    .line 601377
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v2

    .line 601378
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 601379
    iput-object v0, v1, LX/0SK;->g:Ljava/lang/String;

    .line 601380
    move-object v0, v1

    .line 601381
    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 601382
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601351
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 601349
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601350
    new-instance v0, LX/HKN;

    const/4 v1, 0x3

    invoke-direct {p0, p2, v1, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HKN;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xb16bfec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601346
    check-cast p2, LX/HKN;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;

    .line 601347
    iget-object v1, p2, LX/HKN;->a:Ljava/util/List;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->a(Ljava/util/List;)V

    .line 601348
    const/16 v1, 0x1f

    const v2, -0x2c9305e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601345
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 601341
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;

    .line 601342
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    .line 601343
    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 601344
    :cond_0
    return-void
.end method
