.class public Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HKT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601611
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HKT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601612
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601613
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->e:LX/HKT;

    .line 601614
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601615
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->e:LX/HKT;

    const/4 v1, 0x0

    .line 601616
    new-instance v2, LX/HKS;

    invoke-direct {v2, v0}, LX/HKS;-><init>(LX/HKT;)V

    .line 601617
    iget-object p0, v0, LX/HKT;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKR;

    .line 601618
    if-nez p0, :cond_0

    .line 601619
    new-instance p0, LX/HKR;

    invoke-direct {p0, v0}, LX/HKR;-><init>(LX/HKT;)V

    .line 601620
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKR;->a$redex0(LX/HKR;LX/1De;IILX/HKS;)V

    .line 601621
    move-object v2, p0

    .line 601622
    move-object v1, v2

    .line 601623
    move-object v0, v1

    .line 601624
    iget-object v1, v0, LX/HKR;->a:LX/HKS;

    iput-object p2, v1, LX/HKS;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601625
    iget-object v1, v0, LX/HKR;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601626
    move-object v0, v0

    .line 601627
    check-cast p3, LX/2km;

    .line 601628
    iget-object v1, v0, LX/HKR;->a:LX/HKS;

    iput-object p3, v1, LX/HKS;->b:LX/2km;

    .line 601629
    iget-object v1, v0, LX/HKR;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601630
    move-object v0, v0

    .line 601631
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 601632
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 601633
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601634
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601637
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKT;->a(LX/0QB;)LX/HKT;

    move-result-object v4

    check-cast v4, LX/HKT;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKT;)V

    .line 601638
    move-object v0, p0

    .line 601639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 601643
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601644
    invoke-interface {v0}, LX/9uc;->bU()LX/0Px;

    move-result-object v3

    .line 601645
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    .line 601646
    :goto_0
    return v0

    .line 601647
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 601648
    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v0}, LX/1U8;->j()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 601649
    goto :goto_0

    .line 601650
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 601651
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601652
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601653
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601654
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601655
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601656
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601657
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601658
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
