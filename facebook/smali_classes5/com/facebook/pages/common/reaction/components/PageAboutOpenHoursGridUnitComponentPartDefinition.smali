.class public Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HIg;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600710
    new-instance v0, LX/3Zc;

    invoke-direct {v0}, LX/3Zc;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600711
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600712
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;
    .locals 3

    .prologue
    .line 600713
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;

    monitor-enter v1

    .line 600714
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600715
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600716
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600717
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 600718
    new-instance v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;-><init>()V

    .line 600719
    move-object v0, v0

    .line 600720
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600721
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600722
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600723
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600724
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 600725
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v4, 0x0

    .line 600726
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v0

    .line 600727
    invoke-interface {v5}, LX/9uc;->ao()LX/0Px;

    move-result-object v3

    .line 600728
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 600729
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 600730
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageOpenHoursGridComponentFragmentModel$DetailsRowsModel;

    .line 600731
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 600732
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageOpenHoursGridComponentFragmentModel$DetailsRowsModel;->a()LX/174;

    move-result-object v8

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600733
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageOpenHoursGridComponentFragmentModel$DetailsRowsModel;->c()LX/174;

    move-result-object v8

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600734
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageOpenHoursGridComponentFragmentModel$DetailsRowsModel;->b()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600735
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 600737
    :cond_0
    invoke-interface {v5}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    .line 600738
    invoke-interface {v5}, LX/9uc;->dc()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, LX/9uc;->dc()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 600739
    :goto_1
    invoke-interface {v5}, LX/9uc;->cX()LX/174;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, LX/9uc;->cX()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 600740
    :cond_1
    new-instance v0, LX/HIg;

    invoke-interface {v5}, LX/9uc;->ap()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HIg;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V

    return-object v0

    :cond_2
    move-object v3, v4

    .line 600741
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x601d093e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600742
    check-cast p2, LX/HIg;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;

    .line 600743
    iget-object v5, p2, LX/HIg;->a:Ljava/lang/String;

    iget-object v6, p2, LX/HIg;->b:Ljava/util/ArrayList;

    iget-object v7, p2, LX/HIg;->c:Ljava/lang/String;

    iget-object v8, p2, LX/HIg;->d:Ljava/lang/String;

    iget-object v9, p2, LX/HIg;->e:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object v4, p4

    invoke-virtual/range {v4 .. v9}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V

    .line 600744
    const/16 v1, 0x1f

    const v2, 0x1a6eabf7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600745
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600746
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600747
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cX()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cX()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
