.class public Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKx;",
        "TE;",
        "LX/HMf;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602298
    new-instance v0, LX/3Zt;

    invoke-direct {v0}, LX/3Zt;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602295
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602296
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->b:LX/HLT;

    .line 602297
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 602284
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;

    monitor-enter v1

    .line 602285
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602286
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602287
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602288
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602289
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 602290
    move-object v0, p0

    .line 602291
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602299
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 602278
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602279
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602280
    new-instance v1, LX/HKx;

    invoke-interface {v0}, LX/9uc;->cJ()LX/9Zu;

    move-result-object v2

    invoke-static {v2}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->cJ()LX/9Zu;

    move-result-object v3

    invoke-interface {v3}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9uc;->cJ()LX/9Zu;

    move-result-object v4

    invoke-interface {v4}, LX/9Zu;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 602281
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 602282
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 602283
    invoke-virtual {v5, v0, p3, v6, v7}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, LX/HKx;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xb4ec9e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602275
    check-cast p2, LX/HKx;

    check-cast p4, LX/HMf;

    .line 602276
    iget-object v1, p2, LX/HKx;->a:Landroid/net/Uri;

    iget-object v2, p2, LX/HKx;->b:Ljava/lang/String;

    iget-object p0, p2, LX/HKx;->c:Ljava/lang/String;

    iget-object p1, p2, LX/HKx;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2, p0, p1}, LX/HMf;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 602277
    const/16 v1, 0x1f

    const v2, 0x8c8c69a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602272
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602273
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602274
    invoke-interface {v0}, LX/9uc;->cJ()LX/9Zu;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cJ()LX/9Zu;

    move-result-object v1

    invoke-interface {v1}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 602269
    check-cast p4, LX/HMf;

    .line 602270
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, LX/HMf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602271
    return-void
.end method
