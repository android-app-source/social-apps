.class public Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HKE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601559
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HKE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601556
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601557
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->e:LX/HKE;

    .line 601558
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601539
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->e:LX/HKE;

    const/4 v1, 0x0

    .line 601540
    new-instance v2, LX/HKD;

    invoke-direct {v2, v0}, LX/HKD;-><init>(LX/HKE;)V

    .line 601541
    iget-object p0, v0, LX/HKE;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKC;

    .line 601542
    if-nez p0, :cond_0

    .line 601543
    new-instance p0, LX/HKC;

    invoke-direct {p0, v0}, LX/HKC;-><init>(LX/HKE;)V

    .line 601544
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKC;->a$redex0(LX/HKC;LX/1De;IILX/HKD;)V

    .line 601545
    move-object v2, p0

    .line 601546
    move-object v1, v2

    .line 601547
    move-object v0, v1

    .line 601548
    iget-object v1, v0, LX/HKC;->a:LX/HKD;

    iput-object p2, v1, LX/HKD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601549
    iget-object v1, v0, LX/HKC;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601550
    move-object v0, v0

    .line 601551
    check-cast p3, LX/2km;

    .line 601552
    iget-object v1, v0, LX/HKC;->a:LX/HKD;

    iput-object p3, v1, LX/HKD;->b:LX/2km;

    .line 601553
    iget-object v1, v0, LX/HKC;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601554
    move-object v0, v0

    .line 601555
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 601528
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 601529
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601530
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601531
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601532
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601533
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKE;->a(LX/0QB;)LX/HKE;

    move-result-object v4

    check-cast v4, LX/HKE;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKE;)V

    .line 601534
    move-object v0, p0

    .line 601535
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601536
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601537
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601538
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 1

    .prologue
    .line 601523
    if-eqz p0, :cond_0

    .line 601524
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601525
    if-eqz v0, :cond_0

    .line 601526
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601527
    invoke-interface {v0}, LX/9uc;->s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601516
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601522
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601521
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601518
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601519
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601520
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601517
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
