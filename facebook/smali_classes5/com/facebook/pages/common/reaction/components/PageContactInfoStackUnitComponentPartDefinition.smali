.class public Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJ1;",
        "TE;",
        "LX/HLq;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0Uh;

.field private final c:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600877
    new-instance v0, LX/3Ze;

    invoke-direct {v0}, LX/3Ze;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/HLT;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600941
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600942
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b:LX/0Uh;

    .line 600943
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->c:LX/HLT;

    .line 600944
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 600945
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJ1;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HJ1;"
        }
    .end annotation

    .prologue
    .line 600927
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v9, v0

    .line 600928
    new-instance v0, LX/HJ1;

    invoke-interface {v9}, LX/9uc;->am()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b(LX/174;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9}, LX/9uc;->ba()LX/174;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b(LX/174;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9}, LX/9uc;->bh()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9}, LX/9uc;->dr()LX/174;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b(LX/174;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v9}, LX/9uc;->bR()LX/174;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b(LX/174;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v9}, LX/9uc;->bb()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v7

    .line 600929
    iget-object v8, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v8, v8

    .line 600930
    iget-object v10, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v10, v10

    .line 600931
    invoke-virtual {v6, v7, p2, v8, v10}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v9}, LX/9uc;->bg()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v8

    .line 600932
    iget-object v10, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v10, v10

    .line 600933
    iget-object v11, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v11, v11

    .line 600934
    invoke-virtual {v7, v8, p2, v10, v11}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v9}, LX/9uc;->ds()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v10

    .line 600935
    iget-object v11, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v11, v11

    .line 600936
    iget-object v12, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v12, v12

    .line 600937
    invoke-virtual {v8, v10, p2, v11, v12}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v8

    iget-object v10, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v9}, LX/9uc;->bQ()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v9

    .line 600938
    iget-object v11, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v11, v11

    .line 600939
    iget-object v12, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v12, v12

    .line 600940
    invoke-virtual {v10, v9, p2, v11, v12}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/HJ1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 600916
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;

    monitor-enter v1

    .line 600917
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600918
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600919
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600920
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600921
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v4

    check-cast v4, LX/HLT;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;-><init>(LX/0Uh;LX/HLT;Lcom/facebook/content/SecureContextHelper;)V

    .line 600922
    move-object v0, p0

    .line 600923
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600924
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600925
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600926
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/174;)Z
    .locals 1

    .prologue
    .line 600915
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/174;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 600914
    invoke-static {p0}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(LX/174;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600913
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 600912
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJ1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5f91039f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600887
    check-cast p2, LX/HJ1;

    check-cast p4, LX/HLq;

    .line 600888
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->b:LX/0Uh;

    .line 600889
    iput-object v1, p4, LX/HLq;->c:LX/0Uh;

    .line 600890
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 600891
    iput-object v1, p4, LX/HLq;->d:Lcom/facebook/content/SecureContextHelper;

    .line 600892
    iget-object v2, p2, LX/HJ1;->e:Ljava/lang/String;

    iget-object v4, p2, LX/HJ1;->f:Ljava/lang/String;

    iget-object v5, p2, LX/HJ1;->a:Landroid/view/View$OnClickListener;

    iget-object v6, p2, LX/HJ1;->i:Ljava/lang/String;

    iget-object v7, p2, LX/HJ1;->d:Landroid/view/View$OnClickListener;

    iget-object v8, p2, LX/HJ1;->g:Ljava/lang/String;

    iget-object v9, p2, LX/HJ1;->b:Landroid/view/View$OnClickListener;

    iget-object p1, p2, LX/HJ1;->h:Ljava/lang/String;

    iget-object p3, p2, LX/HJ1;->c:Landroid/view/View$OnClickListener;

    move-object v1, p4

    .line 600893
    invoke-virtual {v1}, LX/HLq;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 600894
    iget-object p2, v1, LX/HLq;->e:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    const/4 p4, 0x0

    invoke-static {v1, p2, v2, p4}, LX/HLq;->a(LX/HLq;Landroid/widget/TextView;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 600895
    iget-object p2, v1, LX/HLq;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1, p2, v4, v5}, LX/HLq;->a(LX/HLq;Landroid/widget/TextView;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 600896
    iget-object p2, v1, LX/HLq;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1, p2, p1, p3}, LX/HLq;->a(LX/HLq;Landroid/widget/TextView;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 600897
    iget-object p2, v1, LX/HLq;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1, p2, v6, v7}, LX/HLq;->a(LX/HLq;Landroid/widget/TextView;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 600898
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 600899
    const/4 v6, 0x0

    .line 600900
    const p2, 0x7f0836a8

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    aput-object v8, p4, v6

    invoke-virtual {p0, p2, p4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 600901
    new-instance p4, Landroid/text/SpannableString;

    invoke-direct {p4, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 600902
    new-instance v2, LX/HLp;

    invoke-direct {v2, v1, p0, v9, v8}, LX/HLp;-><init>(LX/HLq;Landroid/content/res/Resources;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 600903
    invoke-virtual {p2, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p2

    .line 600904
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, p2

    .line 600905
    add-int/lit8 p2, p2, -0x1

    const/16 v5, 0x21

    invoke-virtual {p4, v2, p2, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 600906
    iget-object p2, v1, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600907
    iget-object p2, v1, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 600908
    iget-object p2, v1, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 600909
    iget-object p2, v1, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    iget p4, v1, LX/HLq;->b:I

    div-int/lit8 p4, p4, 0x2

    iget v2, v1, LX/HLq;->b:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v6, p4, v6, v2}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 600910
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x49bf611c    # 1567779.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600911
    :cond_0
    iget-object p0, v1, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    const/16 p2, 0x8

    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600884
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600885
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600886
    invoke-interface {v0}, LX/9uc;->am()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(LX/174;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ba()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(LX/174;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bR()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(LX/174;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bh()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dr()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a(LX/174;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 600878
    check-cast p4, LX/HLq;

    .line 600879
    const/4 p1, 0x0

    .line 600880
    iget-object p0, p4, LX/HLq;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600881
    iget-object p0, p4, LX/HLq;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600882
    iget-object p0, p4, LX/HLq;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600883
    return-void
.end method
