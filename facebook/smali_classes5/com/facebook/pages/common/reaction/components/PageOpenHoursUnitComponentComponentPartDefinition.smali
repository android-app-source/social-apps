.class public Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HJz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600750
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HJz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600751
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 600752
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->e:LX/HJz;

    .line 600753
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 600754
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->e:LX/HJz;

    const/4 v1, 0x0

    .line 600755
    new-instance v2, LX/HJy;

    invoke-direct {v2, v0}, LX/HJy;-><init>(LX/HJz;)V

    .line 600756
    iget-object p0, v0, LX/HJz;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HJx;

    .line 600757
    if-nez p0, :cond_0

    .line 600758
    new-instance p0, LX/HJx;

    invoke-direct {p0, v0}, LX/HJx;-><init>(LX/HJz;)V

    .line 600759
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HJx;->a$redex0(LX/HJx;LX/1De;IILX/HJy;)V

    .line 600760
    move-object v2, p0

    .line 600761
    move-object v1, v2

    .line 600762
    move-object v0, v1

    .line 600763
    iget-object v1, v0, LX/HJx;->a:LX/HJy;

    iput-object p2, v1, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600764
    iget-object v1, v0, LX/HJx;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 600765
    move-object v0, v0

    .line 600766
    iget-object v1, v0, LX/HJx;->a:LX/HJy;

    iput-object p3, v1, LX/HJy;->b:LX/2km;

    .line 600767
    iget-object v1, v0, LX/HJx;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 600768
    move-object v0, v0

    .line 600769
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 600770
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 600771
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600772
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600775
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HJz;->a(LX/0QB;)LX/HJz;

    move-result-object v4

    check-cast v4, LX/HJz;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HJz;)V

    .line 600776
    move-object v0, p0

    .line 600777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 600781
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600782
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ap()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v0

    .line 600783
    sget-object v1, LX/HK0;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    .line 600784
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 600785
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 600786
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 600787
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 600788
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600789
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 600790
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600791
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 600792
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 600793
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
