.class public Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HJc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601056
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HJc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601068
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601069
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->e:LX/HJc;

    .line 601070
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601035
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->e:LX/HJc;

    const/4 v1, 0x0

    .line 601036
    new-instance v2, LX/HJb;

    invoke-direct {v2, v0}, LX/HJb;-><init>(LX/HJc;)V

    .line 601037
    iget-object p0, v0, LX/HJc;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HJa;

    .line 601038
    if-nez p0, :cond_0

    .line 601039
    new-instance p0, LX/HJa;

    invoke-direct {p0, v0}, LX/HJa;-><init>(LX/HJc;)V

    .line 601040
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HJa;->a$redex0(LX/HJa;LX/1De;IILX/HJb;)V

    .line 601041
    move-object v2, p0

    .line 601042
    move-object v1, v2

    .line 601043
    move-object v0, v1

    .line 601044
    iget-object v1, v0, LX/HJa;->a:LX/HJb;

    iput-object p2, v1, LX/HJb;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601045
    iget-object v1, v0, LX/HJa;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601046
    move-object v0, v0

    .line 601047
    iget-object v1, v0, LX/HJa;->a:LX/HJb;

    iput-object p3, v1, LX/HJb;->b:LX/2km;

    .line 601048
    iget-object v1, v0, LX/HJa;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601049
    move-object v0, v0

    .line 601050
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 601057
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 601058
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601059
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601060
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601061
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601062
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HJc;->a(LX/0QB;)LX/HJc;

    move-result-object v4

    check-cast v4, LX/HJc;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HJc;)V

    .line 601063
    move-object v0, p0

    .line 601064
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601065
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601066
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 601053
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601054
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601055
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601052
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601051
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601032
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601033
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601034
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601031
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
