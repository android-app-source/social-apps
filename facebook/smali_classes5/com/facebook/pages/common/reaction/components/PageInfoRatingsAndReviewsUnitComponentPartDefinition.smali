.class public Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJU;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600981
    new-instance v0, LX/3Zf;

    invoke-direct {v0}, LX/3Zf;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600978
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600979
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->b:LX/HLT;

    .line 600980
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJU;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HJU;"
        }
    .end annotation

    .prologue
    .line 600970
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600971
    new-instance v1, LX/HJU;

    invoke-interface {v0}, LX/9uc;->cp()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->cy()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9uc;->cr()D

    move-result-wide v4

    invoke-interface {v0}, LX/9uc;->cq()D

    move-result-wide v6

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;->b()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;->b()Ljava/lang/String;

    move-result-object v8

    :goto_0
    iget-object v9, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->dt()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v10

    .line 600972
    iget-object v11, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v11, v11

    .line 600973
    iget-object v12, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v12, v12

    .line 600974
    invoke-virtual {v9, v10, p2, v11, v12}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->cH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 600975
    iget-object v11, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v11, v11

    .line 600976
    iget-object v12, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v12, v12

    .line 600977
    invoke-virtual {v10, v0, p2, v11, v12}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-direct/range {v1 .. v10}, LX/HJU;-><init>(Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 600959
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;

    monitor-enter v1

    .line 600960
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600961
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600962
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600963
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600964
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 600965
    move-object v0, p0

    .line 600966
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600967
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600968
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/HJU;Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HJU;",
            "Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 600982
    iget-object v1, p0, LX/HJU;->c:Ljava/lang/String;

    iget-wide v2, p0, LX/HJU;->e:D

    iget-object v4, p0, LX/HJU;->d:Ljava/lang/String;

    iget-object v5, p0, LX/HJU;->b:Landroid/view/View$OnClickListener;

    iget-object v6, p0, LX/HJU;->g:Ljava/lang/String;

    iget-object v7, p0, LX/HJU;->a:Landroid/view/View$OnClickListener;

    move-object v0, p1

    .line 600983
    if-eqz v6, :cond_0

    if-eqz v7, :cond_0

    .line 600984
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v8, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600985
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    sget-object v10, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v9, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 600986
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 600987
    :goto_0
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->e:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v8, v5}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600988
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->f:Lcom/facebook/fig/starrating/FigStarRatingBar;

    double-to-float v9, v2

    invoke-virtual {v8, v9}, Lcom/facebook/fig/starrating/FigStarRatingBar;->setRating(F)V

    .line 600989
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v8, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600990
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v8, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 600991
    return-void

    .line 600992
    :cond_0
    iget-object v8, v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600958
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 600957
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJU;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x75fd2308

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600956
    check-cast p2, LX/HJU;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;

    invoke-static {p2, p4}, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->a(LX/HJU;Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;)V

    const/16 v1, 0x1f

    const v2, -0x38b7c90

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 600953
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const-wide/16 v4, 0x0

    .line 600954
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600955
    invoke-interface {v0}, LX/9uc;->cq()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cr()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cr()D

    move-result-wide v2

    invoke-interface {v0}, LX/9uc;->cq()D

    move-result-wide v4

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cp()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cp()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cy()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cy()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 600948
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;

    .line 600949
    const/4 p1, 0x0

    .line 600950
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600951
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->e:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600952
    return-void
.end method
