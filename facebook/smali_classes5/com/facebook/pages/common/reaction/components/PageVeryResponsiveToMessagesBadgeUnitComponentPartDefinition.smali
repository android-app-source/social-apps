.class public Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKg;",
        "TE;",
        "LX/HLt;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601185
    new-instance v0, LX/3Zi;

    invoke-direct {v0}, LX/3Zi;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601181
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601182
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->b:LX/HLT;

    .line 601183
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->c:LX/0ad;

    .line 601184
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 601170
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    monitor-enter v1

    .line 601171
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601172
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601173
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601174
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601175
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;-><init>(LX/HLT;LX/0ad;)V

    .line 601176
    move-object v0, p0

    .line 601177
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601178
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601179
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601180
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 601168
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 601169
    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-interface {v1}, LX/9uc;->cv()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cv()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;->a()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;->a()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cx()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cx()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601167
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 601155
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601156
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601157
    new-instance v1, LX/HKg;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/9uc;->cx()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0}, LX/9uc;->cw()LX/3Ab;

    move-result-object v3

    .line 601158
    new-instance v5, Landroid/text/SpannableString;

    invoke-interface {v3}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 601159
    invoke-interface {v3}, LX/3Ab;->b()LX/0Px;

    move-result-object v4

    .line 601160
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5Ph;

    .line 601161
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v4}, LX/5Ph;->c()I

    move-result v7

    invoke-interface {v4}, LX/5Ph;->c()I

    move-result p1

    invoke-interface {v4}, LX/5Ph;->b()I

    move-result v4

    add-int/2addr v4, p1

    const/16 p1, 0x21

    invoke-virtual {v5, v6, v7, v4, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 601162
    move-object v2, v5

    .line 601163
    invoke-interface {v0}, LX/9uc;->cv()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;->a()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 601164
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 601165
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 601166
    invoke-virtual {v5, v0, p3, v6, v7}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, LX/HKg;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x753ce85c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601151
    check-cast p2, LX/HKg;

    check-cast p4, LX/HLt;

    .line 601152
    iget-object v1, p2, LX/HKg;->a:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/HKg;->b:Ljava/lang/String;

    iget-object p0, p2, LX/HKg;->c:Ljava/lang/String;

    invoke-virtual {p4, v1, v2, p0}, LX/HLt;->a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 601153
    iget-object v1, p2, LX/HKg;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/HLt;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601154
    const/16 v1, 0x1f

    const v2, -0x1e79624a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601147
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 601148
    check-cast p4, LX/HLt;

    .line 601149
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/HLt;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601150
    return-void
.end method
