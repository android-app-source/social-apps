.class public Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HKI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601514
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HKI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601511
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601512
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->e:LX/HKI;

    .line 601513
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601494
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->e:LX/HKI;

    const/4 v1, 0x0

    .line 601495
    new-instance v2, LX/HKH;

    invoke-direct {v2, v0}, LX/HKH;-><init>(LX/HKI;)V

    .line 601496
    iget-object p0, v0, LX/HKI;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKG;

    .line 601497
    if-nez p0, :cond_0

    .line 601498
    new-instance p0, LX/HKG;

    invoke-direct {p0, v0}, LX/HKG;-><init>(LX/HKI;)V

    .line 601499
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKG;->a$redex0(LX/HKG;LX/1De;IILX/HKH;)V

    .line 601500
    move-object v2, p0

    .line 601501
    move-object v1, v2

    .line 601502
    move-object v0, v1

    .line 601503
    iget-object v1, v0, LX/HKG;->a:LX/HKH;

    iput-object p2, v1, LX/HKH;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601504
    iget-object v1, v0, LX/HKG;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601505
    move-object v0, v0

    .line 601506
    check-cast p3, LX/2km;

    .line 601507
    iget-object v1, v0, LX/HKG;->a:LX/HKH;

    iput-object p3, v1, LX/HKH;->b:LX/2km;

    .line 601508
    iget-object v1, v0, LX/HKG;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601509
    move-object v0, v0

    .line 601510
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 601483
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 601484
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601485
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601486
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601487
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601488
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKI;->a(LX/0QB;)LX/HKI;

    move-result-object v4

    check-cast v4, LX/HKI;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKI;)V

    .line 601489
    move-object v0, p0

    .line 601490
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601491
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601492
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 601474
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601475
    invoke-interface {v0}, LX/9uc;->bL()LX/0Px;

    move-result-object v3

    .line 601476
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    move v0, v1

    .line 601477
    :goto_0
    return v0

    .line 601478
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    .line 601479
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 601480
    goto :goto_0

    .line 601481
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 601482
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601515
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601473
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601472
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601469
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601470
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601471
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601468
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
