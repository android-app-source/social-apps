.class public Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJd;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601028
    new-instance v0, LX/3Zg;

    invoke-direct {v0}, LX/3Zg;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601025
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601026
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->b:LX/HLT;

    .line 601027
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 601014
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    monitor-enter v1

    .line 601015
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601016
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601017
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601018
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601019
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 601020
    move-object v0, p0

    .line 601021
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601022
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601023
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 601012
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601013
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600995
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 601006
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601007
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601008
    new-instance v1, LX/HJd;

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 601009
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 601010
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 601011
    invoke-virtual {v4, v0, p3, v5, v6}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/HJd;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5dfd89e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601000
    check-cast p2, LX/HJd;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;

    .line 601001
    iget-object v1, p2, LX/HJd;->b:Ljava/lang/String;

    iget-object v2, p2, LX/HJd;->c:Ljava/lang/String;

    .line 601002
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601003
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p3, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 601004
    iget-object v1, p2, LX/HJd;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601005
    const/16 v1, 0x1f

    const v2, 0x63273dcf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600999
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 600996
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;

    .line 600997
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600998
    return-void
.end method
