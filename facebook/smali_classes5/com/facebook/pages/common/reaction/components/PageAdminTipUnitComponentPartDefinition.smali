.class public Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HIv;",
        "TE;",
        "LX/HLn;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602433
    new-instance v0, LX/3Zw;

    invoke-direct {v0}, LX/3Zw;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602434
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602435
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->b:LX/HLT;

    .line 602436
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 602421
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;

    monitor-enter v1

    .line 602422
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602423
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602426
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 602427
    move-object v0, p0

    .line 602428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602432
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 602415
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602416
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602417
    new-instance v1, LX/HIv;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->bm()Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 602418
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 602419
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 602420
    invoke-virtual {v4, v0, p3, v5, v6}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/HIv;-><init>(Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x58cdee5d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602402
    check-cast p2, LX/HIv;

    check-cast p4, LX/HLn;

    .line 602403
    iget-object v1, p2, LX/HIv;->b:Ljava/lang/String;

    iget-boolean v2, p2, LX/HIv;->c:Z

    .line 602404
    iget-object p0, p4, LX/HLn;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602405
    invoke-virtual {p4}, LX/HLn;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 602406
    if-eqz v2, :cond_0

    .line 602407
    iget-object p1, p4, LX/HLn;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const p3, 0x7f0a00d4

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 602408
    iget-object p1, p4, LX/HLn;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const p3, 0x7f0207da

    invoke-virtual {p1, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 602409
    iget-object p1, p4, LX/HLn;->a:Lcom/facebook/widget/text/BetterTextView;

    const p3, 0x7f0a00d4

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 602410
    :goto_0
    iget-object v1, p2, LX/HIv;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/HLn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602411
    const/16 v1, 0x1f

    const v2, -0x6dde4852

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 602412
    :cond_0
    iget-object p1, p4, LX/HLn;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const p3, 0x7f0a00d2

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 602413
    iget-object p1, p4, LX/HLn;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const p3, 0x7f020972

    invoke-virtual {p1, p3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 602414
    iget-object p1, p4, LX/HLn;->a:Lcom/facebook/widget/text/BetterTextView;

    const p3, 0x7f0a00d2

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602399
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602400
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602401
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
