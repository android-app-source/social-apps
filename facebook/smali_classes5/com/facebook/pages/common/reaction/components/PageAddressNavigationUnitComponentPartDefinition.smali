.class public Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HIp;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/CK5;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/8Do;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600845
    new-instance v0, LX/3Zd;

    invoke-direct {v0}, LX/3Zd;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/CK5;Lcom/facebook/content/SecureContextHelper;LX/8Do;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HLT;",
            "LX/CK5;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/8Do;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600794
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600795
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->b:LX/HLT;

    .line 600796
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->c:LX/CK5;

    .line 600797
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 600798
    iput-object p4, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->e:LX/8Do;

    .line 600799
    iput-object p5, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->f:LX/0Ot;

    .line 600800
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;
    .locals 9

    .prologue
    .line 600834
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;

    monitor-enter v1

    .line 600835
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600836
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600839
    new-instance v3, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v4

    check-cast v4, LX/HLT;

    invoke-static {v0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v5

    check-cast v5, LX/CK5;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v7

    check-cast v7, LX/8Do;

    const/16 v8, 0x2b68

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;-><init>(LX/HLT;LX/CK5;Lcom/facebook/content/SecureContextHelper;LX/8Do;LX/0Ot;)V

    .line 600840
    move-object v0, v3

    .line 600841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600833
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 600820
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v0, 0x0

    .line 600821
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v1

    .line 600822
    invoke-interface {v4}, LX/9uc;->cX()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, LX/9uc;->cX()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v4}, LX/9uc;->cX()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 600823
    :goto_0
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->c:LX/CK5;

    invoke-virtual {v1}, LX/CK5;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->e:LX/8Do;

    .line 600824
    iget-object v2, v1, LX/8Do;->a:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget-short v6, LX/8Dn;->g:S

    const/4 v7, 0x0

    invoke-interface {v2, v5, v6, v7}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    move v1, v2

    .line 600825
    if-eqz v1, :cond_1

    .line 600826
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v4}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 600827
    new-instance v0, LX/HIo;

    invoke-direct {v0, p0, v1, v4, p3}, LX/HIo;-><init>(Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;Ljava/lang/String;LX/9uc;LX/2km;)V

    move-object v5, v0

    .line 600828
    :goto_1
    new-instance v0, LX/HIp;

    invoke-interface {v4}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    .line 600829
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v7, v7

    .line 600830
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v8, v8

    .line 600831
    invoke-virtual {v6, v4, p3, v7, v8}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/HIp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    move-object v3, v0

    .line 600832
    goto/16 :goto_0

    :cond_1
    move-object v5, v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5cc95106

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600810
    check-cast p2, LX/HIp;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    .line 600811
    iget-object v1, p2, LX/HIp;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HIp;->b:Ljava/lang/String;

    iget-object p0, p2, LX/HIp;->c:Ljava/lang/String;

    invoke-virtual {p4, v1, v2, p0}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 600812
    iget-object v1, p2, LX/HIp;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600813
    iget-object v1, p2, LX/HIp;->e:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    .line 600814
    iget-object v1, p2, LX/HIp;->e:Landroid/view/View$OnClickListener;

    const/16 p2, 0x8

    .line 600815
    iget-object v2, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->e:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 600816
    iget-object v2, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->f:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    .line 600817
    iget-object v2, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 600818
    iget-object v2, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->h:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600819
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x59ed472f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600807
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600808
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600809
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 600801
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    .line 600802
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600803
    iget-object v0, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->e:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v0, p0}, Landroid/view/View;->setVisibility(I)V

    .line 600804
    iget-object v0, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->f:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Landroid/view/View;->setVisibility(I)V

    .line 600805
    iget-object v0, p4, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->h:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600806
    return-void
.end method
