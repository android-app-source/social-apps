.class public Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJh;",
        "TE;",
        "LX/HLv;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601999
    new-instance v0, LX/3Zp;

    invoke-direct {v0}, LX/3Zp;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602000
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602001
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->b:LX/HLT;

    .line 602002
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 602005
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    monitor-enter v1

    .line 602006
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602007
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602008
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602009
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602010
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 602011
    move-object v0, p0

    .line 602012
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602013
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602014
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 602003
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602004
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aW()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aW()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/HLv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601998
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 601982
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601983
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601984
    new-instance v1, LX/HJh;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->aW()LX/5sY;

    move-result-object v3

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 601985
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 601986
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 601987
    invoke-virtual {v4, v0, p3, v5, v6}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/HJh;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7161573d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601992
    check-cast p2, LX/HJh;

    check-cast p4, LX/HLv;

    .line 601993
    iget-object v1, p2, LX/HJh;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HJh;->b:Ljava/lang/String;

    .line 601994
    iget-object p0, p4, LX/HLv;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601995
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 601996
    iget-object v1, p2, LX/HJh;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/HLv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601997
    const/16 v1, 0x1f

    const v2, 0x6e881e87

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601991
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 601988
    check-cast p4, LX/HLv;

    .line 601989
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/HLv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601990
    return-void
.end method
