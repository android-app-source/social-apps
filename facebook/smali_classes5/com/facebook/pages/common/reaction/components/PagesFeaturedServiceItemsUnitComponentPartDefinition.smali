.class public Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKi;",
        "TE;",
        "LX/HMc;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602266
    new-instance v0, LX/3Zs;

    invoke-direct {v0}, LX/3Zs;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602263
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602264
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->b:LX/E1i;

    .line 602265
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 602252
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;

    monitor-enter v1

    .line 602253
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602254
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602257
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;-><init>(LX/E1i;)V

    .line 602258
    move-object v0, p0

    .line 602259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602251
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 602247
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602248
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602249
    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 602250
    new-instance v2, LX/HKi;

    invoke-interface {v0}, LX/9uc;->cK()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    new-instance v3, LX/HKh;

    invoke-direct {v3, p0, p2, p3, v1}, LX/HKh;-><init>(Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;)V

    invoke-direct {v2, v0, v3}, LX/HKi;-><init>(Ljava/util/List;LX/HKh;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x38129b7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602230
    check-cast p2, LX/HKi;

    check-cast p4, LX/HMc;

    .line 602231
    iget-object v4, p2, LX/HKi;->a:Ljava/util/List;

    iget-object v5, p2, LX/HKi;->b:LX/HKh;

    .line 602232
    iget-object v6, p4, LX/HMc;->b:LX/HMe;

    invoke-virtual {v6, v4}, LX/HMe;->a(Ljava/util/List;)V

    .line 602233
    iget-object v6, p4, LX/HMc;->b:LX/HMe;

    .line 602234
    iput-object v5, v6, LX/HMe;->b:LX/HKh;

    .line 602235
    iget-boolean v6, p4, LX/HMc;->f:Z

    if-nez v6, :cond_0

    iget-object v6, p4, LX/HMc;->d:Landroid/os/Handler;

    if-eqz v6, :cond_0

    iget-object v6, p4, LX/HMc;->e:Ljava/lang/Runnable;

    if-eqz v6, :cond_0

    .line 602236
    iget-object v6, p4, LX/HMc;->d:Landroid/os/Handler;

    iget-object v7, p4, LX/HMc;->e:Ljava/lang/Runnable;

    const-wide/16 v8, 0xbb8

    const v10, -0x594845e3

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 602237
    const/4 v6, 0x1

    iput-boolean v6, p4, LX/HMc;->f:Z

    .line 602238
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x2da6ffcf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602246
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 602239
    check-cast p4, LX/HMc;

    .line 602240
    const/4 p1, 0x0

    .line 602241
    iget-object p0, p4, LX/HMc;->b:LX/HMe;

    if-eqz p0, :cond_0

    .line 602242
    iget-object p0, p4, LX/HMc;->b:LX/HMe;

    invoke-virtual {p0, p1}, LX/HMe;->a(Ljava/util/List;)V

    .line 602243
    iget-object p0, p4, LX/HMc;->b:LX/HMe;

    .line 602244
    iput-object p1, p0, LX/HMe;->b:LX/HKh;

    .line 602245
    :cond_0
    return-void
.end method
