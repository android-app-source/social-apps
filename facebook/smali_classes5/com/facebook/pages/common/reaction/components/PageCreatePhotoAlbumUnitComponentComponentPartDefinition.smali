.class public Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HJD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601234
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HJD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601235
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601236
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->e:LX/HJD;

    .line 601237
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601238
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->e:LX/HJD;

    const/4 v1, 0x0

    .line 601239
    new-instance v2, LX/HJC;

    invoke-direct {v2, v0}, LX/HJC;-><init>(LX/HJD;)V

    .line 601240
    iget-object p0, v0, LX/HJD;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HJB;

    .line 601241
    if-nez p0, :cond_0

    .line 601242
    new-instance p0, LX/HJB;

    invoke-direct {p0, v0}, LX/HJB;-><init>(LX/HJD;)V

    .line 601243
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HJB;->a$redex0(LX/HJB;LX/1De;IILX/HJC;)V

    .line 601244
    move-object v2, p0

    .line 601245
    move-object v1, v2

    .line 601246
    move-object v0, v1

    .line 601247
    iget-object v1, v0, LX/HJB;->a:LX/HJC;

    iput-object p2, v1, LX/HJC;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601248
    iget-object v1, v0, LX/HJB;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601249
    move-object v0, v0

    .line 601250
    iget-object v1, v0, LX/HJB;->a:LX/HJC;

    iput-object p3, v1, LX/HJC;->b:LX/2km;

    .line 601251
    iget-object v1, v0, LX/HJB;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 601252
    move-object v0, v0

    .line 601253
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 601254
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 601255
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601256
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601257
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601258
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601259
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HJD;->a(LX/0QB;)LX/HJD;

    move-result-object v4

    check-cast v4, LX/HJD;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HJD;)V

    .line 601260
    move-object v0, p0

    .line 601261
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601262
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601263
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601264
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601265
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601266
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601267
    const/4 v0, 0x1

    move v0, v0

    .line 601268
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601269
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601270
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601271
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601272
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
