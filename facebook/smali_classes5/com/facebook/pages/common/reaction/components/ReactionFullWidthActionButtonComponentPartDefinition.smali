.class public Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKz;",
        "TE;",
        "LX/HLm;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0W9;

.field private final c:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602096
    new-instance v0, LX/3Zq;

    invoke-direct {v0}, LX/3Zq;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602058
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602059
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->c:LX/HLT;

    .line 602060
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->b:LX/0W9;

    .line 602061
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;
    .locals 5

    .prologue
    .line 602085
    const-class v1, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;

    monitor-enter v1

    .line 602086
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602087
    sput-object v2, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602090
    new-instance p0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;-><init>(LX/HLT;LX/0W9;)V

    .line 602091
    move-object v0, p0

    .line 602092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602084
    sget-object v0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 602078
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602079
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v0

    .line 602080
    new-instance v0, LX/HKz;

    invoke-interface {v5}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, LX/9uc;->z()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5}, LX/9uc;->cZ()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5}, LX/9uc;->E()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v5}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v5

    .line 602081
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v7, v7

    .line 602082
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v8, v8

    .line 602083
    invoke-virtual {v6, v5, p3, v7, v8}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HKz;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4bb888e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602068
    check-cast p2, LX/HKz;

    check-cast p4, LX/HLm;

    .line 602069
    iget-object v2, p2, LX/HKz;->a:Ljava/lang/String;

    iget-object v4, p2, LX/HKz;->b:Ljava/lang/String;

    iget-object v5, p2, LX/HKz;->c:Ljava/lang/String;

    iget-object v6, p2, LX/HKz;->d:Ljava/lang/String;

    iget-object p0, p2, LX/HKz;->e:Landroid/view/View$OnClickListener;

    move-object v1, p4

    .line 602070
    iget-object p1, v1, LX/HLm;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602071
    iget-object p1, v1, LX/HLm;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 602072
    iget-object p1, v1, LX/HLm;->a:Lcom/facebook/resources/ui/FbButton;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p2, "#"

    invoke-direct {p3, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 602073
    new-instance p1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 602074
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p2, "#"

    invoke-direct {p3, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 602075
    const/4 p3, 0x3

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p4, "#"

    invoke-direct {p2, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p3, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 602076
    iget-object p3, v1, LX/HLm;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-static {p3, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 602077
    const/16 v1, 0x1f

    const v2, 0x4ca6cd6a    # 8.7452496E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602065
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602066
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602067
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 602062
    check-cast p4, LX/HLm;

    .line 602063
    iget-object p0, p4, LX/HLm;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602064
    return-void
.end method
