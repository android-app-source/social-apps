.class public Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKF;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 601336
    const-class v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 601337
    new-instance v0, LX/3Zk;

    invoke-direct {v0}, LX/3Zk;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601331
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601332
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->c:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    .line 601333
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 601334
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->e:LX/HLT;

    .line 601335
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 601320
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;

    monitor-enter v1

    .line 601321
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601322
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601323
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601324
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601325
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v5

    check-cast v5, LX/HLT;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;-><init>(Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/HLT;)V

    .line 601326
    move-object v0, p0

    .line 601327
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601328
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601329
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601330
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 601314
    if-nez p0, :cond_0

    .line 601315
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 601316
    :goto_0
    return-object v0

    .line 601317
    :cond_0
    if-lez p0, :cond_1

    .line 601318
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0085

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 601319
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid album size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 1

    .prologue
    .line 601309
    if-eqz p0, :cond_0

    .line 601310
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601311
    if-eqz v0, :cond_0

    .line 601312
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601313
    invoke-interface {v0}, LX/9uc;->s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601273
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 601281
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 601282
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601283
    invoke-interface {v0}, LX/9uc;->s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    move-result-object v3

    .line 601284
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 601285
    :goto_0
    if-eqz v0, :cond_2

    .line 601286
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 601287
    new-instance v0, LX/HIz;

    sget-object v5, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-direct {v0, v4, v5, v7, v6}, LX/HIz;-><init>(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;FLandroid/widget/ImageView$ScaleType;)V

    .line 601288
    :goto_1
    new-instance v4, LX/HIx;

    invoke-direct {v4, v0}, LX/HIx;-><init>(LX/HIw;)V

    .line 601289
    const v0, 0x7f0d23c4

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->c:Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    invoke-interface {p1, v0, v5, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 601290
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 601291
    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    .line 601292
    :goto_3
    const v4, 0x7f0d23c5

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v4, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 601293
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 601294
    :goto_4
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;->a()I

    move-result v0

    move v1, v0

    :cond_0
    move-object v0, p3

    .line 601295
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 601296
    const v1, 0x7f0d23c6

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 601297
    new-instance v0, LX/HKF;

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->e:LX/HLT;

    .line 601298
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 601299
    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 601300
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 601301
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 601302
    invoke-virtual {v1, v2, p3, v3, v4}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HKF;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_1
    move v0, v1

    .line 601303
    goto/16 :goto_0

    .line 601304
    :cond_2
    const v4, 0x7f020626

    .line 601305
    new-instance v0, LX/HIy;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-direct {v0, v4, v7, v5}, LX/HIy;-><init>(IFLandroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 601306
    goto :goto_2

    .line 601307
    :cond_4
    const-string v0, ""

    goto :goto_3

    :cond_5
    move v0, v1

    .line 601308
    goto :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7886d4b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601278
    check-cast p2, LX/HKF;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    .line 601279
    iget-object v1, p2, LX/HKF;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601280
    const/16 v1, 0x1f

    const v2, -0x56cf9b3b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601277
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 601274
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    .line 601275
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 601276
    return-void
.end method
