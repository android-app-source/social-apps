.class public Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/HKp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601889
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HKp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601886
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601887
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->e:LX/HKp;

    .line 601888
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 601861
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->e:LX/HKp;

    const/4 v1, 0x0

    .line 601862
    new-instance v2, LX/HKo;

    invoke-direct {v2, v0}, LX/HKo;-><init>(LX/HKp;)V

    .line 601863
    sget-object p0, LX/HKp;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKn;

    .line 601864
    if-nez p0, :cond_0

    .line 601865
    new-instance p0, LX/HKn;

    invoke-direct {p0}, LX/HKn;-><init>()V

    .line 601866
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKn;->a$redex0(LX/HKn;LX/1De;IILX/HKo;)V

    .line 601867
    move-object v2, p0

    .line 601868
    move-object v1, v2

    .line 601869
    move-object v0, v1

    .line 601870
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 601871
    invoke-interface {v1}, LX/9uc;->bF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 601872
    iget-object v2, v0, LX/HKn;->a:LX/HKo;

    iput-object v1, v2, LX/HKo;->a:Ljava/lang/String;

    .line 601873
    iget-object v2, v0, LX/HKn;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 601874
    move-object v0, v0

    .line 601875
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 601876
    invoke-interface {v1}, LX/9uc;->aM()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 601877
    iget-object v2, v0, LX/HKn;->a:LX/HKo;

    iput-object v1, v2, LX/HKo;->b:Ljava/lang/String;

    .line 601878
    iget-object v2, v0, LX/HKn;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 601879
    move-object v0, v0

    .line 601880
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 601881
    invoke-interface {v1}, LX/9uc;->bc()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 601882
    iget-object v2, v0, LX/HKn;->a:LX/HKo;

    iput-object v1, v2, LX/HKo;->c:Ljava/lang/String;

    .line 601883
    iget-object v2, v0, LX/HKn;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 601884
    move-object v0, v0

    .line 601885
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 601850
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;

    monitor-enter v1

    .line 601851
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601852
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601853
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601854
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601855
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKp;->a(LX/0QB;)LX/HKp;

    move-result-object v4

    check-cast v4, LX/HKp;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKp;)V

    .line 601856
    move-object v0, p0

    .line 601857
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601858
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601859
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601824
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601849
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601829
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601830
    if-eqz p1, :cond_0

    .line 601831
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601832
    if-eqz v0, :cond_0

    .line 601833
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601834
    invoke-interface {v0}, LX/9uc;->bF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601835
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601836
    invoke-interface {v0}, LX/9uc;->bF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;->b()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601837
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601838
    invoke-interface {v0}, LX/9uc;->bF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601839
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601840
    invoke-interface {v0}, LX/9uc;->aM()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601841
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601842
    invoke-interface {v0}, LX/9uc;->aM()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601843
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601844
    invoke-interface {v0}, LX/9uc;->bc()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601845
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601846
    invoke-interface {v0}, LX/9uc;->bc()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;->b()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601847
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601848
    invoke-interface {v0}, LX/9uc;->bc()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601826
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601827
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601828
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601825
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
