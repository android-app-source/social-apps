.class public Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/HKl;

.field private final f:LX/H87;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601822
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HKl;LX/H87;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601818
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 601819
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->e:LX/HKl;

    .line 601820
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->f:LX/H87;

    .line 601821
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 601755
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601756
    invoke-interface {v0}, LX/9uc;->cc()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 601757
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 601758
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601759
    invoke-interface {v0}, LX/9uc;->cc()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$PreviewDataModel;

    .line 601760
    new-instance v6, LX/HM9;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$PreviewDataModel;->a()D

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$PreviewDataModel;->b()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, LX/HM9;-><init>(DD)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601761
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 601762
    :goto_1
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 601763
    invoke-interface {v1}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 601764
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->f:LX/H87;

    .line 601765
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601766
    invoke-interface {v3}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 601767
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 601768
    invoke-interface {v4}, LX/9uc;->bf()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    move-result-object v4

    .line 601769
    const-string v5, "view_insights_dashboard"

    const-string v6, "pma_root_chrome"

    .line 601770
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 601771
    iput-object v6, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 601772
    move-object v7, v7

    .line 601773
    const-string v8, "page_id"

    invoke-virtual {v7, v8, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "metric_type"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 601774
    iget-object v8, v1, LX/H87;->a:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 601775
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->e:LX/HKl;

    const/4 v3, 0x0

    .line 601776
    new-instance v4, LX/HKk;

    invoke-direct {v4, v1}, LX/HKk;-><init>(LX/HKl;)V

    .line 601777
    sget-object v5, LX/HKl;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/HKj;

    .line 601778
    if-nez v5, :cond_2

    .line 601779
    new-instance v5, LX/HKj;

    invoke-direct {v5}, LX/HKj;-><init>()V

    .line 601780
    :cond_2
    invoke-static {v5, p1, v3, v3, v4}, LX/HKj;->a$redex0(LX/HKj;LX/1De;IILX/HKk;)V

    .line 601781
    move-object v4, v5

    .line 601782
    move-object v3, v4

    .line 601783
    move-object v1, v3

    .line 601784
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601785
    invoke-interface {v3}, LX/9uc;->bD()Ljava/lang/String;

    move-result-object v3

    .line 601786
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-object v3, v4, LX/HKk;->a:Ljava/lang/String;

    .line 601787
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601788
    move-object v1, v1

    .line 601789
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601790
    invoke-interface {v3}, LX/9uc;->dk()Ljava/lang/String;

    move-result-object v3

    .line 601791
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-object v3, v4, LX/HKk;->b:Ljava/lang/String;

    .line 601792
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601793
    move-object v1, v1

    .line 601794
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601795
    invoke-interface {v3}, LX/9uc;->aH()Z

    move-result v3

    .line 601796
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-boolean v3, v4, LX/HKk;->e:Z

    .line 601797
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601798
    move-object v1, v1

    .line 601799
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601800
    invoke-interface {v3}, LX/9uc;->bj()Z

    move-result v3

    .line 601801
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-boolean v3, v4, LX/HKk;->f:Z

    .line 601802
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601803
    move-object v1, v1

    .line 601804
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601805
    invoke-interface {v3}, LX/9uc;->ak()Ljava/lang/String;

    move-result-object v3

    .line 601806
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-object v3, v4, LX/HKk;->c:Ljava/lang/String;

    .line 601807
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601808
    move-object v1, v1

    .line 601809
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 601810
    invoke-interface {v3}, LX/9uc;->al()Ljava/lang/String;

    move-result-object v3

    .line 601811
    iget-object v4, v1, LX/HKj;->a:LX/HKk;

    iput-object v3, v4, LX/HKk;->d:Ljava/lang/String;

    .line 601812
    iget-object v4, v1, LX/HKj;->d:Ljava/util/BitSet;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 601813
    move-object v1, v1

    .line 601814
    if-nez v0, :cond_3

    .line 601815
    :goto_2
    iget-object v0, v1, LX/HKj;->a:LX/HKk;

    iput-object v2, v0, LX/HKk;->g:LX/0Px;

    .line 601816
    move-object v0, v1

    .line 601817
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_2

    :cond_4
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 601741
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;

    monitor-enter v1

    .line 601742
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601743
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601746
    new-instance v6, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKl;->a(LX/0QB;)LX/HKl;

    move-result-object v4

    check-cast v4, LX/HKl;

    .line 601747
    new-instance p0, LX/H87;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {p0, v5}, LX/H87;-><init>(LX/0Zb;)V

    .line 601748
    move-object v5, p0

    .line 601749
    check-cast v5, LX/H87;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKl;LX/H87;)V

    .line 601750
    move-object v0, v6

    .line 601751
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601752
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601753
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 601823
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 601740
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601728
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601729
    if-eqz p1, :cond_0

    .line 601730
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601731
    if-eqz v0, :cond_0

    .line 601732
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601733
    invoke-interface {v0}, LX/9uc;->bD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601734
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601735
    invoke-interface {v0}, LX/9uc;->dk()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601736
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601737
    invoke-interface {v0}, LX/9uc;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601738
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601739
    invoke-interface {v0}, LX/9uc;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 601725
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 601726
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 601727
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 601724
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
