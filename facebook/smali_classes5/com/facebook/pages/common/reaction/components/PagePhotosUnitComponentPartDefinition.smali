.class public Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKY;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/E1i;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 601580
    new-instance v0, LX/3Zm;

    invoke-direct {v0}, LX/3Zm;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/E1i;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1i;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 601581
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 601582
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->b:LX/E1i;

    .line 601583
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->c:LX/0Or;

    .line 601584
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;Ljava/lang/String;Ljava/lang/String;LX/2km;JLjava/lang/String;[J)Landroid/view/View$OnClickListener;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TE;J",
            "Ljava/lang/String;",
            "[J)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 601585
    new-instance v1, LX/HKW;

    move-object v2, p0

    move-object v3, p3

    move-wide v4, p4

    move-object/from16 v6, p7

    move-object/from16 v7, p6

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v1 .. v9}, LX/HKW;-><init>(Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;LX/2km;J[JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 601586
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;

    monitor-enter v1

    .line 601587
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 601588
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 601589
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601590
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 601591
    new-instance v4, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    const/16 p0, 0x19e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;-><init>(LX/E1i;LX/0Or;)V

    .line 601592
    move-object v0, v4

    .line 601593
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 601594
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601595
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 601596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "ITE;)",
            "Ljava/util/List",
            "<",
            "LX/HKX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601597
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v2

    invoke-interface {v2}, LX/9uc;->bU()LX/0Px;

    move-result-object v11

    .line 601598
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v2

    move/from16 v0, p2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 601599
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 601600
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v2

    new-array v9, v2, [J

    .line 601601
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v12, :cond_0

    .line 601602
    invoke-virtual {v11, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v9, v3

    .line 601603
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 601604
    :cond_0
    const/4 v2, 0x0

    move v10, v2

    :goto_1
    if-ge v10, v12, :cond_1

    .line 601605
    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v14

    .line 601606
    new-instance v15, LX/HKX;

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->j()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    invoke-static/range {v2 .. v9}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a(Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;Ljava/lang/String;Ljava/lang/String;LX/2km;JLjava/lang/String;[J)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v2

    invoke-interface {v2}, LX/9uc;->bU()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotosComponentFragmentModel$PhotosModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotosComponentFragmentModel$PhotosModel;->k()Ljava/lang/String;

    move-result-object v7

    move-object v2, v15

    move-object v3, v14

    move-object/from16 v4, v16

    move-object/from16 v5, v17

    invoke-direct/range {v2 .. v7}, LX/HKX;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    invoke-interface {v13, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601607
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_1

    .line 601608
    :cond_1
    return-object v13
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 601571
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 601572
    invoke-interface {v0}, LX/9uc;->bU()LX/0Px;

    move-result-object v3

    .line 601573
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    .line 601574
    :goto_0
    return v0

    .line 601575
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 601576
    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v0}, LX/1U8;->j()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 601577
    goto :goto_0

    .line 601578
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 601579
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 601560
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 601569
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 601570
    new-instance v0, LX/HKY;

    const/4 v1, 0x3

    invoke-direct {p0, p2, v1, p3}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HKY;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2fce2cba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 601566
    check-cast p2, LX/HKY;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;

    .line 601567
    iget-object v1, p2, LX/HKY;->a:Ljava/util/List;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->a(Ljava/util/List;)V

    .line 601568
    const/16 v1, 0x1f

    const v2, 0x54bcb914

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 601565
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 601561
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;

    .line 601562
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    .line 601563
    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 601564
    :cond_0
    return-void
.end method
