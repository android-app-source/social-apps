.class public Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;


# instance fields
.field private final e:LX/HIm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600860
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/HIm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600849
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 600850
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->e:LX/HIm;

    .line 600851
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 600861
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->e:LX/HIm;

    const/4 v1, 0x0

    .line 600862
    new-instance v2, LX/HIl;

    invoke-direct {v2, v0}, LX/HIl;-><init>(LX/HIm;)V

    .line 600863
    iget-object p0, v0, LX/HIm;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HIk;

    .line 600864
    if-nez p0, :cond_0

    .line 600865
    new-instance p0, LX/HIk;

    invoke-direct {p0, v0}, LX/HIk;-><init>(LX/HIm;)V

    .line 600866
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HIk;->a$redex0(LX/HIk;LX/1De;IILX/HIl;)V

    .line 600867
    move-object v2, p0

    .line 600868
    move-object v1, v2

    .line 600869
    move-object v0, v1

    .line 600870
    iget-object v1, v0, LX/HIk;->a:LX/HIl;

    iput-object p2, v1, LX/HIl;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600871
    iget-object v1, v0, LX/HIk;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 600872
    move-object v0, v0

    .line 600873
    iget-object v1, v0, LX/HIk;->a:LX/HIl;

    iput-object p3, v1, LX/HIl;->b:LX/2km;

    .line 600874
    iget-object v1, v0, LX/HIk;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 600875
    move-object v0, v0

    .line 600876
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 600858
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 600859
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 600855
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600856
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600857
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 600852
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 600853
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 600854
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 600848
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
