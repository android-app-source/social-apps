.class public Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKy;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602191
    new-instance v0, LX/3Zr;

    invoke-direct {v0}, LX/3Zr;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602188
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602189
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->b:LX/HLT;

    .line 602190
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HKy;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HKy;"
        }
    .end annotation

    .prologue
    .line 602186
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v12

    .line 602187
    new-instance v1, LX/HKy;

    invoke-interface {v12}, LX/9uc;->D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v2

    invoke-interface {v12}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v12}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-interface {v12}, LX/9uc;->aL()LX/174;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v12}, LX/9uc;->aL()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-interface {v12}, LX/9uc;->cV()LX/174;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v12}, LX/9uc;->cV()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-interface {v12}, LX/9uc;->cO()LX/174;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v12}, LX/9uc;->cO()LX/174;

    move-result-object v6

    invoke-interface {v6}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    :goto_3
    invoke-interface {v12}, LX/9uc;->ax()LX/174;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v12}, LX/9uc;->ax()LX/174;

    move-result-object v7

    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    :goto_4
    invoke-interface {v12}, LX/9uc;->ay()LX/174;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-interface {v12}, LX/9uc;->ay()LX/174;

    move-result-object v8

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    :goto_5
    invoke-interface {v12}, LX/9uc;->cD()LX/174;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-interface {v12}, LX/9uc;->cD()LX/174;

    move-result-object v9

    invoke-interface {v9}, LX/174;->a()Ljava/lang/String;

    move-result-object v9

    :goto_6
    invoke-interface {v12}, LX/9uc;->cE()LX/174;

    move-result-object v10

    if-eqz v10, :cond_7

    invoke-interface {v12}, LX/9uc;->cE()LX/174;

    move-result-object v10

    invoke-interface {v10}, LX/174;->a()Ljava/lang/String;

    move-result-object v10

    :goto_7
    invoke-interface {v12}, LX/9uc;->C()LX/174;

    move-result-object v11

    if-eqz v11, :cond_8

    invoke-interface {v12}, LX/9uc;->C()LX/174;

    move-result-object v11

    invoke-interface {v11}, LX/174;->a()Ljava/lang/String;

    move-result-object v11

    :goto_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v12}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v13, v12, v0, v14, v15}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v12

    invoke-direct/range {v1 .. v12}, LX/HKy;-><init>(Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    goto :goto_5

    :cond_6
    const/4 v9, 0x0

    goto :goto_6

    :cond_7
    const/4 v10, 0x0

    goto :goto_7

    :cond_8
    const/4 v11, 0x0

    goto :goto_8
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;
    .locals 4

    .prologue
    .line 602175
    const-class v1, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;

    monitor-enter v1

    .line 602176
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602177
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602178
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602179
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602180
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;-><init>(LX/HLT;)V

    .line 602181
    move-object v0, p0

    .line 602182
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602183
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602184
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602185
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/HKy;Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HKy;",
            "Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 602144
    iget-object v1, p0, LX/HKy;->a:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    iget-object v2, p0, LX/HKy;->b:Ljava/lang/String;

    iget-object v3, p0, LX/HKy;->c:Ljava/lang/String;

    iget-object v4, p0, LX/HKy;->d:Ljava/lang/String;

    iget-object v5, p0, LX/HKy;->e:Ljava/lang/String;

    iget-object v6, p0, LX/HKy;->f:Ljava/lang/String;

    iget-object v7, p0, LX/HKy;->g:Ljava/lang/String;

    iget-object v8, p0, LX/HKy;->h:Ljava/lang/String;

    iget-object v9, p0, LX/HKy;->i:Ljava/lang/String;

    iget-object v10, p0, LX/HKy;->j:Ljava/lang/String;

    move-object v0, p1

    .line 602145
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602146
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602147
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    sget-object v13, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v11, v12, v13}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 602148
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602149
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602150
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602151
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v8}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602152
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602153
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 602154
    sget-object v11, LX/HM7;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 602155
    :goto_0
    invoke-static {v10}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 602156
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v11, v10}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602157
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 602158
    :goto_1
    iget-object v0, p0, LX/HKy;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602159
    return-void

    .line 602160
    :pswitch_0
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e1

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602161
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->j:Landroid/view/View;

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    .line 602162
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020c28

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 602163
    :pswitch_1
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e0

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602164
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->j:Landroid/view/View;

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    .line 602165
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f021612

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 602166
    :pswitch_2
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e1

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602167
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020c28

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 602168
    :pswitch_3
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e0

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602169
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e2

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602170
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f021612

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 602171
    :pswitch_4
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a08e3

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 602172
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->j:Landroid/view/View;

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    .line 602173
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020259

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 602174
    :cond_0
    iget-object v11, v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602143
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 602135
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HKy;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x507a2739

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602142
    check-cast p2, LX/HKy;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;

    invoke-static {p2, p4}, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->a(LX/HKy;Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x780e6113

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602139
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602140
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602141
    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aL()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 602136
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;

    .line 602137
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 602138
    return-void
.end method
