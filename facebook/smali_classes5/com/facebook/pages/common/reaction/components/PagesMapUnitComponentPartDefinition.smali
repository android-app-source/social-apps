.class public Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HKw;",
        "TE;",
        "LX/HLz;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600411
    new-instance v0, LX/3ZV;

    invoke-direct {v0}, LX/3ZV;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600425
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600426
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->b:LX/HLT;

    .line 600427
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HKw;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HKw;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 600412
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 600413
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v0

    .line 600414
    invoke-interface {v5}, LX/9uc;->bu()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;

    .line 600415
    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 600416
    if-eqz v0, :cond_0

    .line 600417
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600418
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 600419
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 600420
    new-instance v8, LX/HKw;

    invoke-interface {v5}, LX/9uc;->bl()Z

    move-result v9

    invoke-interface {v5}, LX/9uc;->bw()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    move-result-object v10

    invoke-interface {v5}, LX/9uc;->dv()I

    move-result v11

    invoke-interface {v5}, LX/9uc;->ca()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v12

    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v5}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 600421
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v2

    .line 600422
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v2

    .line 600423
    invoke-interface {v5}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    move-object v0, v8

    move v1, v9

    move-object v2, v7

    move-object v3, v10

    move v4, v11

    move-object v5, v12

    invoke-direct/range {v0 .. v6}, LX/HKw;-><init>(ZLjava/util/ArrayList;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;Landroid/view/View$OnClickListener;)V

    return-object v8

    :cond_2
    move v0, v1

    .line 600424
    goto :goto_1
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 600388
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->a()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4056800000000000L    # 90.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 600389
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->a()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;->b()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 600390
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 600400
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;

    monitor-enter v1

    .line 600401
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600402
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600403
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600404
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600405
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;-><init>(LX/HLT;)V

    .line 600406
    move-object v0, p0

    .line 600407
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600408
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600409
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HKw;LX/2km;LX/HLz;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HKw;",
            "TE;",
            "LX/HLz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 600428
    iget-boolean v1, p1, LX/HKw;->f:Z

    iget-object v2, p1, LX/HKw;->a:Ljava/util/ArrayList;

    iget-object v3, p1, LX/HKw;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    iget v4, p1, LX/HKw;->c:I

    iget-object v5, p1, LX/HKw;->d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 600429
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600430
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v6

    move-object v0, p3

    invoke-virtual/range {v0 .. v6}, LX/HLz;->a(ZLjava/util/ArrayList;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V

    .line 600431
    iget-object v0, p1, LX/HKw;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, LX/HLz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600432
    check-cast p2, LX/1Pn;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08223c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/HLz;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 600433
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600399
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 600398
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HKw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x528cf2cd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600397
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/HKw;

    check-cast p3, LX/2km;

    check-cast p4, LX/HLz;

    invoke-static {p1, p2, p3, p4}, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HKw;LX/2km;LX/HLz;)V

    const/16 v1, 0x1f

    const v2, -0xf43ae3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 600394
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 600395
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 600396
    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, LX/9uc;->bu()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$LocationsModel;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 600391
    check-cast p4, LX/HLz;

    .line 600392
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/HLz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600393
    return-void
.end method
