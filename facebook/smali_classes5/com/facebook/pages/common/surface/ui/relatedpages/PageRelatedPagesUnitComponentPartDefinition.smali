.class public Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HQs;",
        "TE;",
        "LX/Gdq;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HQo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602640
    new-instance v0, LX/3a1;

    invoke-direct {v0}, LX/3a1;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HQo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602637
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602638
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->b:LX/HQo;

    .line 602639
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 602564
    const-class v1, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;

    monitor-enter v1

    .line 602565
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602566
    sput-object v2, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602567
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602568
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602569
    new-instance p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;

    invoke-static {v0}, LX/HQo;->a(LX/0QB;)LX/HQo;

    move-result-object v3

    check-cast v3, LX/HQo;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;-><init>(LX/HQo;)V

    .line 602570
    move-object v0, p0

    .line 602571
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602572
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602573
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/HQs;LX/1Pn;LX/Gdq;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HQs;",
            "TE;",
            "LX/Gdq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 602588
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p2, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 602589
    iget-object v0, p0, LX/HQs;->a:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    move-object v0, v0

    .line 602590
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/16 v11, 0x8

    const/4 v3, 0x0

    .line 602591
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v5

    .line 602592
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 602593
    :cond_0
    invoke-virtual {p2, v11}, LX/Gdq;->setVisibility(I)V

    .line 602594
    :cond_1
    :goto_0
    return-void

    .line 602595
    :cond_2
    if-eqz v1, :cond_5

    .line 602596
    iget-object v6, p2, LX/Gdq;->j:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 602597
    :goto_1
    iget-object v6, p2, LX/Gdq;->e:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getRotation()I

    move-result v6

    .line 602598
    iget-object v7, p2, LX/Gdq;->q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v7, :cond_4

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p2, LX/Gdq;->q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v8}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 602599
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v7

    iget-object v9, p2, LX/Gdq;->q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v9}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_3

    iget v7, p2, LX/Gdq;->s:I

    if-eq v7, v6, :cond_1

    .line 602600
    :cond_3
    iget-object v7, p2, LX/Gdq;->q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v7}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v7

    invoke-static {v0, v5, v7}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 602601
    :cond_4
    iput-object v0, p2, LX/Gdq;->q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 602602
    iput v6, p2, LX/Gdq;->s:I

    .line 602603
    iget-object v6, p2, LX/Gdq;->c:LX/Gdn;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 602604
    const-class v8, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v8, v7}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    const-string v9, "HScrollFeedUnitView can only be bound to FeedUnits which implement the ScrollableItemListFeedUnit interface."

    invoke-static {v8, v9}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 602605
    iget-object v8, v6, LX/Gdn;->a:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Gdm;

    move-object v6, v8

    .line 602606
    iput-object v6, p2, LX/Gdq;->r:LX/Gdm;

    .line 602607
    iget-object v6, p2, LX/Gdq;->r:LX/Gdm;

    if-nez v6, :cond_6

    .line 602608
    invoke-virtual {p2, v11}, LX/Gdq;->setVisibility(I)V

    .line 602609
    iget-object v4, p2, LX/Gdq;->d:LX/03V;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HScrollFeedUnitView no controller for HScrollFeedUnitView for class "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Zombie:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 602610
    :cond_5
    iget-object v6, p2, LX/Gdq;->j:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 602611
    :cond_6
    iget-object v6, p2, LX/Gdq;->r:LX/Gdm;

    iget-object v7, p2, LX/Gdq;->h:Landroid/widget/TextView;

    iget-object v8, p2, LX/Gdq;->k:Landroid/view/View;

    invoke-virtual {v6, v0, v7, v8}, LX/Gdm;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/widget/TextView;Landroid/view/View;)V

    .line 602612
    iget-object v6, p2, LX/Gdq;->i:Landroid/widget/TextView;

    iget-object v7, p2, LX/Gdq;->l:Landroid/view/View;

    iget-object v8, p2, LX/Gdq;->m:Landroid/view/View;

    const/16 v9, 0x8

    .line 602613
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 602614
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 602615
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 602616
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v4, :cond_7

    iget-object v6, p2, LX/Gdq;->r:LX/Gdm;

    invoke-virtual {v6}, LX/Gdm;->d()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 602617
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {p2, v0, v3}, LX/Gdq;->a(LX/Gdq;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/lang/Object;)V

    .line 602618
    :goto_2
    goto/16 :goto_0

    .line 602619
    :cond_7
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 602620
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3, v8}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 602621
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_a

    .line 602622
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    .line 602623
    iput-boolean v8, v3, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 602624
    :goto_3
    iget-object v3, p2, LX/Gdq;->o:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 602625
    iget-object v3, p2, LX/Gdq;->o:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 602626
    :cond_8
    iget-object v3, p2, LX/Gdq;->r:LX/Gdm;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v6

    iget-object v7, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3, v6, v7}, LX/2sw;->a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V

    .line 602627
    iget-object v3, p2, LX/Gdq;->r:LX/Gdm;

    iget-object v6, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {p2}, LX/Gdq;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, LX/2sw;->a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V

    .line 602628
    iget-object v3, p2, LX/Gdq;->n:LX/Gdp;

    invoke-virtual {v3, v5}, LX/99W;->a(Ljava/util/List;)V

    .line 602629
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v3

    iget-object v6, p2, LX/Gdq;->n:LX/Gdp;

    if-eq v3, v6, :cond_9

    .line 602630
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    iget-object v6, p2, LX/Gdq;->n:LX/Gdp;

    invoke-virtual {v3, v6}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 602631
    :cond_9
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    iget-object v6, p2, LX/Gdq;->r:LX/Gdm;

    invoke-virtual {v6}, LX/2sw;->b()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 602632
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v6

    invoke-virtual {v3, v6, v8}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 602633
    goto :goto_2

    .line 602634
    :cond_a
    iget-object v3, p2, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    .line 602635
    iput-boolean v6, v3, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 602636
    goto :goto_3
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602587
    sget-object v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 602583
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602584
    new-instance v0, LX/HQs;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->b:LX/HQo;

    .line 602585
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 602586
    invoke-interface {v2}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HQo;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HQs;-><init>(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x772c7143

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602582
    check-cast p2, LX/HQs;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/Gdq;

    invoke-static {p2, p3, p4}, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->a(LX/HQs;LX/1Pn;LX/Gdq;)V

    const/16 v1, 0x1f

    const v2, -0x391cdc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602575
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602576
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602577
    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 602578
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602579
    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesUnitComponentPartDefinition;->b:LX/HQo;

    .line 602580
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 602581
    invoke-interface {v1}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HQo;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
