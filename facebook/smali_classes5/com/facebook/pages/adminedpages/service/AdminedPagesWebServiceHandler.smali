.class public Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3f9;

.field public final c:LX/3fC;

.field public final d:LX/3fD;

.field public final e:LX/18V;

.field public final f:LX/16I;


# direct methods
.method public constructor <init>(LX/0Or;LX/3f9;LX/3fC;LX/3fD;LX/18V;LX/16I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/3f9;",
            "LX/3fC;",
            "LX/3fD;",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/16I;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621426
    iput-object p1, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->a:LX/0Or;

    .line 621427
    iput-object p2, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->b:LX/3f9;

    .line 621428
    iput-object p3, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->c:LX/3fC;

    .line 621429
    iput-object p4, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->d:LX/3fD;

    .line 621430
    iput-object p5, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->e:LX/18V;

    .line 621431
    iput-object p6, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->f:LX/16I;

    .line 621432
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;
    .locals 7

    .prologue
    .line 621433
    new-instance v0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 621434
    new-instance v4, LX/3f9;

    invoke-static {p0}, LX/3fA;->b(LX/0QB;)LX/3fA;

    move-result-object v2

    check-cast v2, LX/3fA;

    invoke-static {p0}, LX/3fB;->a(LX/0QB;)LX/3fB;

    move-result-object v3

    check-cast v3, LX/3fB;

    invoke-direct {v4, v2, v3}, LX/3f9;-><init>(LX/3fA;LX/3fB;)V

    .line 621435
    move-object v2, v4

    .line 621436
    check-cast v2, LX/3f9;

    .line 621437
    new-instance v5, LX/3fC;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v3, v4}, LX/3fC;-><init>(LX/0sO;LX/0SG;)V

    .line 621438
    move-object v3, v5

    .line 621439
    check-cast v3, LX/3fC;

    .line 621440
    new-instance v5, LX/3fD;

    invoke-static {p0}, LX/3fA;->b(LX/0QB;)LX/3fA;

    move-result-object v4

    check-cast v4, LX/3fA;

    invoke-direct {v5, v4}, LX/3fD;-><init>(LX/3fA;)V

    .line 621441
    move-object v4, v5

    .line 621442
    check-cast v4, LX/3fD;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v5

    check-cast v5, LX/18V;

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v6

    check-cast v6, LX/16I;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;-><init>(LX/0Or;LX/3f9;LX/3fC;LX/3fD;LX/18V;LX/16I;)V

    .line 621443
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 621444
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 621445
    const-string v1, "fetch_all_pages"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 621446
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->b:LX/3f9;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    .line 621447
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 621448
    :goto_0
    return-object v0

    .line 621449
    :cond_0
    const-string v1, "admined_pages_prefetch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 621450
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->f:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 621451
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 621452
    :goto_1
    move-object v0, v0

    .line 621453
    goto :goto_0

    .line 621454
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621455
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->e:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v1

    .line 621456
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 621457
    const-string v2, "adminedPagesPrefetchParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    .line 621458
    iget-object v2, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->c:LX/3fC;

    invoke-static {v2, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v2, "prefetchAdminedPages"

    .line 621459
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 621460
    move-object v0, v0

    .line 621461
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 621462
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 621463
    const-string v2, "pagesAccessTokenPrefetchParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    .line 621464
    iget-object v2, p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->d:LX/3fD;

    invoke-static {v2, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v2, "prefetchAccessToken"

    .line 621465
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 621466
    move-object v0, v0

    .line 621467
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 621468
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 621469
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 621470
    iput-object v2, v0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 621471
    const-string v2, "fetchAdminedPagesInfo"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 621472
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 621473
    const-string v0, "prefetchAdminedPages"

    invoke-interface {v1, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621474
    const-string v0, "prefetchAccessToken"

    invoke-interface {v1, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621475
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method
