.class public Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 621177
    new-instance v0, LX/3ey;

    invoke-direct {v0}, LX/3ey;-><init>()V

    sput-object v0, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0rS;)V
    .locals 0

    .prologue
    .line 621174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621175
    iput-object p1, p0, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;->a:LX/0rS;

    .line 621176
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 621171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;->a:LX/0rS;

    .line 621173
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 621170
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 621168
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 621169
    return-void
.end method
