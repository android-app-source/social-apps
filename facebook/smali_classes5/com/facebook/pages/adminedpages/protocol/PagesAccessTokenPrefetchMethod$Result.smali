.class public final Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622010
    new-instance v0, LX/3fN;

    invoke-direct {v0}, LX/3fN;-><init>()V

    sput-object v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 622007
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 622008
    iput-object p4, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    .line 622009
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 622003
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 622004
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    .line 622005
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 622006
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 621999
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 622000
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 622001
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 622002
    return-void
.end method
