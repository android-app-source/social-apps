.class public final Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574659
    new-instance v0, LX/2Uz;

    invoke-direct {v0}, LX/2Uz;-><init>()V

    sput-object v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 574655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574656
    iput p1, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;->a:I

    .line 574657
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 574660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574661
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;->a:I

    .line 574662
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 574658
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 574653
    iget v0, p0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 574654
    return-void
.end method
