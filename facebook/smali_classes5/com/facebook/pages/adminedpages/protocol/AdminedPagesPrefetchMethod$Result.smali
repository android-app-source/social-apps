.class public final Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 621982
    new-instance v0, LX/3fL;

    invoke-direct {v0}, LX/3fL;-><init>()V

    sput-object v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;)V
    .locals 0
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 621975
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 621976
    iput-object p4, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    .line 621977
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 621979
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 621980
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    .line 621981
    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminedPages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLInterfaces$AdminedPagesPrefetchQuery$$AdminedPages$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 621983
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 621978
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 621972
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 621973
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 621974
    return-void
.end method
