.class public final Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2c3b3413
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 622108
    const-class v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 622048
    const-class v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 622106
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 622107
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 622103
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 622104
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 622105
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 622089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 622090
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x171aa0d7

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 622091
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 622092
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 622093
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x4e30aecf

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 622094
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 622095
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 622096
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 622097
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 622098
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 622099
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 622100
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 622101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 622102
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 622073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 622074
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 622075
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x171aa0d7

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622076
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 622077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    .line 622078
    iput v3, v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e:I

    move-object v1, v0

    .line 622079
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 622080
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4e30aecf

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 622081
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 622082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    .line 622083
    iput v3, v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->h:I

    move-object v1, v0

    .line 622084
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 622085
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 622086
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 622087
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 622088
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 622072
    new-instance v0, LX/8Df;

    invoke-direct {v0, p1}, LX/8Df;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 622071
    invoke-virtual {p0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 622067
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 622068
    const/4 v0, 0x0

    const v1, -0x171aa0d7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e:I

    .line 622069
    const/4 v0, 0x3

    const v1, -0x4e30aecf

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->h:I

    .line 622070
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 622065
    invoke-virtual {p2}, LX/18L;->a()V

    .line 622066
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 622064
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 622062
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 622063
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 622059
    new-instance v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;-><init>()V

    .line 622060
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 622061
    return-object v0
.end method

.method public final c()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 622057
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 622058
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 622055
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->f:Ljava/lang/String;

    .line 622056
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 622054
    const v0, 0x60ed7652

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 622052
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->g:Ljava/lang/String;

    .line 622053
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final eD_()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 622050
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->i:Ljava/util/List;

    .line 622051
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 622049
    const v0, 0x25d6af

    return v0
.end method
