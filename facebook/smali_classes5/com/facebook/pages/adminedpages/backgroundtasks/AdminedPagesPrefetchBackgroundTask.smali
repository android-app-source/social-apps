.class public Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;
.super LX/1Eg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;


# instance fields
.field private final b:LX/0aG;

.field private final c:LX/0SG;

.field private final d:LX/2Ux;

.field public final e:LX/03V;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/2U1;

.field private volatile h:J

.field private final i:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

.field private final j:Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

.field private final k:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574642
    const-class v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    sput-object v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0SG;LX/2Ux;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2U1;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x80

    .line 574628
    const-string v0, "ADMINED_PAGES_PREFETCH_TASK"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 574629
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->h:J

    .line 574630
    new-instance v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    invoke-direct {v0, v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->i:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    .line 574631
    new-instance v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    invoke-direct {v0, v2}, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->j:Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    .line 574632
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k:Landroid/os/Bundle;

    .line 574633
    iput-object p1, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->b:LX/0aG;

    .line 574634
    iput-object p2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->c:LX/0SG;

    .line 574635
    iput-object p3, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->d:LX/2Ux;

    .line 574636
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k:Landroid/os/Bundle;

    const-string v1, "adminedPagesPrefetchParams"

    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->i:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574637
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k:Landroid/os/Bundle;

    const-string v1, "pagesAccessTokenPrefetchParams"

    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->j:Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574638
    iput-object p4, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->e:LX/03V;

    .line 574639
    iput-object p5, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574640
    iput-object p6, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->g:LX/2U1;

    .line 574641
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;
    .locals 10

    .prologue
    .line 574613
    sget-object v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->l:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    if-nez v0, :cond_1

    .line 574614
    const-class v1, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    monitor-enter v1

    .line 574615
    :try_start_0
    sget-object v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->l:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574616
    if-eqz v2, :cond_0

    .line 574617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 574618
    new-instance v3, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    .line 574619
    invoke-static {}, LX/2Uw;->a()LX/2Ux;

    move-result-object v6

    move-object v6, v6

    .line 574620
    check-cast v6, LX/2Ux;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v9

    check-cast v9, LX/2U1;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;-><init>(LX/0aG;LX/0SG;LX/2Ux;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2U1;)V

    .line 574621
    move-object v0, v3

    .line 574622
    sput-object v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->l:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574623
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574624
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574625
    :cond_1
    sget-object v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->l:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    return-object v0

    .line 574626
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 574591
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->d:LX/2Ux;

    iget-boolean v0, v0, LX/2Ux;->a:Z

    return v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 574612
    const-class v0, Lcom/facebook/pages/adminedpages/annotation/AdminedPagesTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 574609
    invoke-direct {p0}, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574610
    const-wide/16 v0, -0x1

    .line 574611
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->h:J

    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->d:LX/2Ux;

    iget-wide v2, v2, LX/2Ux;->b:J

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574608
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 574603
    invoke-direct {p0}, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k()Z

    move-result v2

    if-nez v2, :cond_1

    .line 574604
    :cond_0
    :goto_0
    return v0

    .line 574605
    :cond_1
    iget-wide v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->h:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3ex;->b:LX/0Tn;

    invoke-interface {v2, v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 574606
    goto :goto_0

    .line 574607
    :cond_3
    iget-wide v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->h:J

    iget-object v4, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->d:LX/2Ux;

    iget-wide v4, v4, LX/2Ux;->b:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574592
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->h:J

    .line 574593
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3ex;->b:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 574594
    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 574595
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 574596
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k:Landroid/os/Bundle;

    const-string v2, "loadAdminedPagesParam"

    new-instance v3, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;

    invoke-direct {v3, v0}, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;-><init>(LX/0rS;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574597
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->b:LX/0aG;

    const-string v1, "admined_pages_prefetch"

    iget-object v2, p0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->k:Landroid/os/Bundle;

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x1b2a8c5

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 574598
    new-instance v1, LX/3ez;

    invoke-direct {v1, p0}, LX/3ez;-><init>(Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574599
    new-instance v1, LX/3f0;

    sget-object v2, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->a:Ljava/lang/Class;

    invoke-direct {v1, v2}, LX/3f0;-><init>(Ljava/lang/Class;)V

    .line 574600
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574601
    return-object v1

    .line 574602
    :cond_0
    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    goto :goto_0
.end method
