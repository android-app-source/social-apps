.class public Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HVJ;",
        "TE;",
        "Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602647
    new-instance v0, LX/3a3;

    invoke-direct {v0}, LX/3a3;-><init>()V

    sput-object v0, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602648
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602649
    return-void
.end method

.method private static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/HVJ;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HVJ;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 602650
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v9, v0

    .line 602651
    if-eqz p1, :cond_1

    instance-of v0, p1, LX/3U6;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/3U6;

    invoke-virtual {v0}, LX/3U6;->u()LX/0o8;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/3U6;

    invoke-virtual {v0}, LX/3U6;->u()LX/0o8;

    move-result-object v0

    instance-of v0, v0, LX/HOo;

    if-eqz v0, :cond_1

    .line 602652
    check-cast p1, LX/3U6;

    invoke-virtual {p1}, LX/3U6;->u()LX/0o8;

    move-result-object v0

    check-cast v0, LX/HOo;

    invoke-interface {v0}, LX/HOo;->k()Landroid/os/ParcelUuid;

    move-result-object v0

    .line 602653
    :goto_0
    new-instance v10, LX/HVJ;

    new-instance v1, LX/HBY;

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->j()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->e()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v6

    check-cast v6, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->k()LX/0Px;

    move-result-object v7

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->hs_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    move-result-object v11

    if-eqz v11, :cond_0

    new-instance v8, LX/4YW;

    invoke-direct {v8}, LX/4YW;-><init>()V

    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->hs_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->a()I

    move-result v11

    .line 602654
    iput v11, v8, LX/4YW;->b:I

    .line 602655
    move-object v8, v8

    .line 602656
    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->hs_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->b()D

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, LX/4YW;->a(D)LX/4YW;

    move-result-object v8

    invoke-virtual {v8}, LX/4YW;->a()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v8

    :cond_0
    invoke-interface {v9}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->ht_()LX/1k1;

    move-result-object v9

    check-cast v9, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-direct/range {v1 .. v9}, LX/HBY;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/0Px;Lcom/facebook/graphql/model/GraphQLRating;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;)V

    invoke-direct {v10, v1, v0}, LX/HVJ;-><init>(LX/HBY;Landroid/os/ParcelUuid;)V

    return-object v10

    :cond_1
    move-object v0, v8

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;
    .locals 3

    .prologue
    .line 602657
    const-class v1, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;

    monitor-enter v1

    .line 602658
    :try_start_0
    sget-object v0, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602659
    sput-object v2, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 602662
    new-instance v0, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;

    invoke-direct {v0}, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;-><init>()V

    .line 602663
    move-object v0, v0

    .line 602664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602668
    sget-object v0, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 602669
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-static {p2, p3}, Lcom/facebook/pages/identity/cards/contextitems/PageContextRowsPlaceholderUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/HVJ;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2df3b63d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602670
    check-cast p2, LX/HVJ;

    check-cast p4, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    .line 602671
    iget-object v1, p2, LX/HVJ;->a:LX/HBY;

    move-object v1, v1

    .line 602672
    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a(LX/HBY;)V

    .line 602673
    iget-object v1, p2, LX/HVJ;->b:Landroid/os/ParcelUuid;

    move-object v1, v1

    .line 602674
    iput-object v1, p4, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->k:Landroid/os/ParcelUuid;

    .line 602675
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->setContainerBorderVisibility(Z)V

    .line 602676
    const/16 v1, 0x1f

    const v2, 0x728c05a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 602677
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 602678
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602679
    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/8A4;->a(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->j()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->j()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->e()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->e()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 602680
    check-cast p4, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    .line 602681
    invoke-virtual {p4}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a()V

    .line 602682
    return-void
.end method
