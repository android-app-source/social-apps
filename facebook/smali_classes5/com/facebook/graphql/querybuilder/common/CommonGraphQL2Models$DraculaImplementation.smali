.class public final Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 790743
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 790744
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 790741
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 790742
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 790730
    if-nez p1, :cond_0

    .line 790731
    :goto_0
    return v1

    .line 790732
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 790733
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 790734
    :pswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 790735
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 790736
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 790737
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 790738
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 790739
    invoke-virtual {p3, v7, v6}, LX/186;->b(II)V

    .line 790740
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x41dc040c
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;
    .locals 1

    .prologue
    .line 790729
    new-instance v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 790726
    packed-switch p0, :pswitch_data_0

    .line 790727
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 790728
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x41dc040c
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 790725
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 790723
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->b(I)V

    .line 790724
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 790692
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 790693
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 790694
    :cond_0
    iput-object p1, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a:LX/15i;

    .line 790695
    iput p2, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->b:I

    .line 790696
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 790722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 790721
    new-instance v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 790718
    iget v0, p0, LX/1vt;->c:I

    .line 790719
    move v0, v0

    .line 790720
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 790715
    iget v0, p0, LX/1vt;->c:I

    .line 790716
    move v0, v0

    .line 790717
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 790712
    iget v0, p0, LX/1vt;->b:I

    .line 790713
    move v0, v0

    .line 790714
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 790709
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 790710
    move-object v0, v0

    .line 790711
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 790700
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 790701
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 790702
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 790703
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 790704
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 790705
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 790706
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 790707
    invoke-static {v3, v9, v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 790708
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 790697
    iget v0, p0, LX/1vt;->c:I

    .line 790698
    move v0, v0

    .line 790699
    return v0
.end method
