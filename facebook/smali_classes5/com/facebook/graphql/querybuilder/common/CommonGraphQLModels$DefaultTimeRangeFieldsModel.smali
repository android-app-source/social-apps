.class public final Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4a2dffad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 791783
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 791747
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 791781
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 791782
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 791778
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 791779
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 791780
    return-void
.end method

.method public static a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;
    .locals 4

    .prologue
    .line 791769
    if-nez p0, :cond_0

    .line 791770
    const/4 p0, 0x0

    .line 791771
    :goto_0
    return-object p0

    .line 791772
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    if-eqz v0, :cond_1

    .line 791773
    check-cast p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    goto :goto_0

    .line 791774
    :cond_1
    new-instance v0, LX/4aQ;

    invoke-direct {v0}, LX/4aQ;-><init>()V

    .line 791775
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v2

    iput-wide v2, v0, LX/4aQ;->a:J

    .line 791776
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v2

    iput-wide v2, v0, LX/4aQ;->b:J

    .line 791777
    invoke-virtual {v0}, LX/4aQ;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 791763
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 791764
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 791765
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 791766
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->f:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 791767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 791768
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 791761
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 791762
    iget-wide v0, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 791758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 791759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 791760
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 791754
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 791755
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->e:J

    .line 791756
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->f:J

    .line 791757
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 791752
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 791753
    iget-wide v0, p0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->f:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 791749
    new-instance v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    invoke-direct {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;-><init>()V

    .line 791750
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 791751
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 791748
    const v0, -0x57606650

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 791746
    const v0, 0x78e1ce50

    return v0
.end method
