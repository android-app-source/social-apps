.class public final Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0tA;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0tA;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 679919
    iput-object p1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679920
    iput-object p2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->b:Ljava/lang/String;

    .line 679921
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const v5, 0x950001

    .line 679922
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 679923
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Bq;

    .line 679924
    if-nez v0, :cond_0

    .line 679925
    :goto_0
    return-void

    .line 679926
    :cond_0
    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v2, v1, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v2

    .line 679927
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v1, v1, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 679928
    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v1, v1, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Bq;

    .line 679929
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 679930
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 679931
    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v1

    .line 679932
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 679933
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v0, v5, v2, v1}, LX/0tA;->a$redex0(LX/0tA;IILjava/util/Collection;)V

    .line 679934
    :cond_2
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sk;

    .line 679935
    invoke-interface {v0, v1, v3}, LX/0sk;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    goto :goto_2

    .line 679936
    :cond_3
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v5, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0
.end method
