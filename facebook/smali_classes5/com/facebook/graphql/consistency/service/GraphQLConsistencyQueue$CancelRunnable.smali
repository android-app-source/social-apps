.class public final Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0tA;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tA;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 679915
    iput-object p1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679916
    iput-object p2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->b:Ljava/lang/String;

    .line 679917
    iput-object p3, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->c:Ljava/util/Collection;

    .line 679918
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 679904
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 679905
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v1, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 679906
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 679907
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Bq;

    .line 679908
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 679909
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 679910
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sk;

    .line 679911
    iget-object v3, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->c:Ljava/util/Collection;

    invoke-interface {v0, v3, v2}, LX/0sk;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    goto :goto_1

    .line 679912
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950001

    iget-object v2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 679913
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950002

    iget-object v2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 679914
    return-void
.end method
