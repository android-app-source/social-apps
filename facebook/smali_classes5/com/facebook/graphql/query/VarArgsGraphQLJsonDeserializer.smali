.class public abstract Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/graphql/modelutil/BaseModel;",
        ">",
        "Lcom/facebook/common/json/FbJsonDeserializer;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/16a;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:S

.field public d:Z


# direct methods
.method public constructor <init>(LX/16a;S)V
    .locals 1

    .prologue
    .line 790420
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 790421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->a:Ljava/lang/Class;

    .line 790422
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16a;

    iput-object v0, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->b:LX/16a;

    .line 790423
    iput-short p2, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->c:S

    .line 790424
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 790425
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 790426
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->a:Ljava/lang/Class;

    .line 790427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->b:LX/16a;

    .line 790428
    const/4 v0, -0x1

    iput-short v0, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->c:S

    .line 790429
    return-void
.end method


# virtual methods
.method public abstract a(LX/186;LX/15w;)I
.end method

.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 790430
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 790431
    const/16 v1, 0xa

    invoke-static {v1}, LX/1R7;->a(I)LX/1R7;

    move-result-object v4

    .line 790432
    iget-short v1, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->c:S

    const/4 v6, -0x1

    if-eq v1, v6, :cond_0

    move v1, v5

    .line 790433
    :goto_0
    new-instance v6, LX/4a5;

    invoke-direct {v6, p0, v2, v1, v4}, LX/4a5;-><init>(Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;LX/186;ZLX/1R7;)V

    .line 790434
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v7

    iget-boolean v8, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->d:Z

    invoke-static {p1, v7, v6, v8}, LX/261;->a(LX/15w;LX/0lC;LX/4Zw;Z)V

    .line 790435
    invoke-virtual {v4}, LX/1R7;->d()[I

    move-result-object v4

    invoke-virtual {v2, v4, v0}, LX/186;->a([IZ)I

    move-result v4

    .line 790436
    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 790437
    invoke-virtual {v2, v0, v4}, LX/186;->b(II)V

    .line 790438
    invoke-virtual {v2}, LX/186;->d()I

    move-result v0

    .line 790439
    invoke-virtual {v2, v0}, LX/186;->d(I)V

    .line 790440
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v0

    .line 790441
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 790442
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "VarArgsGraphQLJsonDeserializer["

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 790443
    if-eqz v1, :cond_1

    .line 790444
    iget-object v1, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->b:LX/16a;

    move-object v4, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, LX/4Bz;->a(Ljava/nio/ByteBuffer;LX/16a;Ljava/lang/String;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)Ljava/util/List;

    move-result-object v0

    .line 790445
    :goto_1
    return-object v0

    :cond_0
    move v1, v0

    .line 790446
    goto :goto_0

    .line 790447
    :cond_1
    iget-object v1, p0, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->a:Ljava/lang/Class;

    move-object v4, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, LX/4Bz;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;Ljava/lang/String;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method
