.class public Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 790367
    const-class v0, LX/0w7;

    new-instance v1, Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 790368
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 790369
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 790370
    const/4 v0, 0x0

    .line 790371
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_3

    .line 790372
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v2, :cond_0

    .line 790373
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 790374
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 790375
    const-string v2, "params"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 790376
    new-instance v0, LX/4a2;

    invoke-direct {v0, p0}, LX/4a2;-><init>(Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;)V

    .line 790377
    invoke-virtual {p1, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 790378
    new-instance v1, LX/0w7;

    invoke-direct {v1, v0}, LX/0w7;-><init>(Ljava/util/Map;)V

    move-object v0, v1

    .line 790379
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 790380
    :catch_0
    move-exception v0

    .line 790381
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 790382
    new-instance v1, LX/2aQ;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to deserialize to instance GraphQlQueryParamSet\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getJsonParserText(LX/15w;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v1

    .line 790383
    :cond_2
    :try_start_1
    const-string v2, "input_name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 790384
    new-instance v1, LX/4a3;

    invoke-direct {v1, p0}, LX/4a3;-><init>(Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;)V

    .line 790385
    invoke-virtual {p1, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 790386
    :cond_3
    return-object v0
.end method
