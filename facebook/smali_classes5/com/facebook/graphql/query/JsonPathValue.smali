.class public Lcom/facebook/graphql/query/JsonPathValue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/query/JsonPathValueDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/query/JsonPathValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mValue:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 630737
    const-class v0, Lcom/facebook/graphql/query/JsonPathValueDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 630736
    new-instance v0, LX/4a4;

    invoke-direct {v0}, LX/4a4;-><init>()V

    sput-object v0, Lcom/facebook/graphql/query/JsonPathValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 630738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/graphql/query/JsonPathValue;
    .locals 1

    .prologue
    .line 630733
    new-instance v0, Lcom/facebook/graphql/query/JsonPathValue;

    invoke-direct {v0}, Lcom/facebook/graphql/query/JsonPathValue;-><init>()V

    .line 630734
    iput-object p0, v0, Lcom/facebook/graphql/query/JsonPathValue;->mValue:Ljava/lang/String;

    .line 630735
    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 630732
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630731
    iget-object v0, p0, Lcom/facebook/graphql/query/JsonPathValue;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 630729
    iget-object v0, p0, Lcom/facebook/graphql/query/JsonPathValue;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 630730
    return-void
.end method
