.class public Lcom/facebook/graphql/error/GraphQLErrorSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/error/GraphQLError;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 741598
    const-class v0, Lcom/facebook/graphql/error/GraphQLError;

    new-instance v1, Lcom/facebook/graphql/error/GraphQLErrorSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/error/GraphQLErrorSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 741599
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 741597
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/error/GraphQLError;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 741591
    if-nez p0, :cond_0

    .line 741592
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 741593
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 741594
    invoke-static {p0, p1, p2}, Lcom/facebook/graphql/error/GraphQLErrorSerializer;->b(Lcom/facebook/graphql/error/GraphQLError;LX/0nX;LX/0my;)V

    .line 741595
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 741596
    return-void
.end method

.method private static b(Lcom/facebook/graphql/error/GraphQLError;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 741580
    const-string v0, "code"

    iget v1, p0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 741581
    const-string v0, "api_error_code"

    iget v1, p0, Lcom/facebook/graphql/error/GraphQLError;->apiErrorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 741582
    const-string v0, "summary"

    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 741583
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 741584
    const-string v0, "is_silent"

    iget-boolean v1, p0, Lcom/facebook/graphql/error/GraphQLError;->isSilent:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 741585
    const-string v0, "is_transient"

    iget-boolean v1, p0, Lcom/facebook/graphql/error/GraphQLError;->isTransient:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 741586
    const-string v0, "requires_reauth"

    iget-boolean v1, p0, Lcom/facebook/graphql/error/GraphQLError;->requiresReauth:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 741587
    const-string v0, "debug_info"

    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->debugInfo:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 741588
    const-string v0, "query_path"

    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->queryPath:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 741589
    const-string v0, "severity"

    iget-object v1, p0, Lcom/facebook/graphql/error/GraphQLError;->severity:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 741590
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 741579
    check-cast p1, Lcom/facebook/graphql/error/GraphQLError;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/error/GraphQLErrorSerializer;->a(Lcom/facebook/graphql/error/GraphQLError;LX/0nX;LX/0my;)V

    return-void
.end method
