.class public final Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761031
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761030
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 761028
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 761029
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 761006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761007
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 761008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 761009
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 761010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 761011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 761012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 761013
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 761014
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 761015
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 761016
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 761017
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v0, v8, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 761018
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 761019
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 761020
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 761021
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 761022
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 761023
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 761024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761025
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 761026
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    goto :goto_0

    .line 761027
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 760983
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 760985
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    .line 760986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 760987
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 760988
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    .line 760989
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 760990
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 760991
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 760992
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 760993
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 760994
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 760995
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 760996
    if-eqz v2, :cond_2

    .line 760997
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 760998
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->h:Ljava/util/List;

    move-object v1, v0

    .line 760999
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 761000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761001
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 761002
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 761003
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761004
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761005
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->e:Ljava/lang/String;

    .line 760982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l:Ljava/lang/String;

    .line 760979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 761032
    const v0, 0x407ccd4f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    .line 760976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760971
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760972
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 760973
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 760968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->h:Ljava/util/List;

    .line 760970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760965
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760966
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 760967
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760962
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760963
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j:Ljava/lang/String;

    .line 760964
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760956
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760957
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 760958
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760960
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 760961
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    return-object v0
.end method
