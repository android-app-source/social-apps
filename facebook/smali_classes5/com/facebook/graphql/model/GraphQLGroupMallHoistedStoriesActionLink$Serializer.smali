.class public final Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 759911
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 759912
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 759880
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 759881
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 759882
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 759883
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 759884
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 759885
    if-eqz v2, :cond_0

    .line 759886
    const-string v2, "hoisted_story_ids"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759887
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 759888
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 759889
    if-eqz v2, :cond_1

    .line 759890
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759891
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 759892
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 759893
    if-eqz v2, :cond_2

    .line 759894
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759895
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 759896
    :cond_2
    invoke-virtual {v1, v0, v5, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 759897
    if-eqz v2, :cond_3

    .line 759898
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759899
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 759900
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 759901
    if-eqz v2, :cond_4

    .line 759902
    const-string v3, "group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759903
    invoke-static {v1, v2, p1, p2}, LX/30j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 759904
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 759905
    if-eqz v2, :cond_5

    .line 759906
    const-string v2, "hoisted_comment_ids"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 759907
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 759908
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 759909
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 759910
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
