.class public final Lcom/facebook/graphql/model/GraphQLPageStatusCard;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStatusCard$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStatusCard$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771671
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStatusCard$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771670
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStatusCard$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 771629
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 771630
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771667
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771668
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 771669
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->h:Ljava/lang/String;

    .line 771666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 771652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771653
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 771654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 771655
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 771656
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 771657
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 771658
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 771659
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 771660
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 771661
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 771662
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771663
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 771639
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 771642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 771643
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    .line 771644
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 771645
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 771646
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 771647
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 771648
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    .line 771649
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 771650
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771651
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 771637
    const v0, 0x513ada31

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 771636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->e:Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->f:Ljava/lang/String;

    .line 771633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->f:Ljava/lang/String;

    return-object v0
.end method
