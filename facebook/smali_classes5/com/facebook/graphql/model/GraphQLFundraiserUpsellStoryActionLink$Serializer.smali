.class public final Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 754185
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 754186
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 754217
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 754188
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 754189
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 754190
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 754191
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 754192
    if-eqz v2, :cond_0

    .line 754193
    const-string v3, "charity"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754194
    invoke-static {v1, v2, p1, p2}, LX/4N3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 754195
    :cond_0
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 754196
    if-eqz v2, :cond_1

    .line 754197
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754198
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 754199
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 754200
    if-eqz v2, :cond_2

    .line 754201
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754202
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 754203
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 754204
    if-eqz v2, :cond_3

    .line 754205
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754206
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 754207
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 754208
    if-eqz v2, :cond_4

    .line 754209
    const-string v3, "updating_post_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754210
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 754211
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 754212
    if-eqz v2, :cond_5

    .line 754213
    const-string v3, "promo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754214
    invoke-static {v1, v2, p1, p2}, LX/4N4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 754215
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 754216
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 754187
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
