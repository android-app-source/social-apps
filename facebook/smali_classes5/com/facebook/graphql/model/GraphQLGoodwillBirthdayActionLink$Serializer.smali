.class public final Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 754998
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 754999
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 755000
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 755001
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 755002
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 755003
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 755004
    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 755005
    if-eqz v2, :cond_0

    .line 755006
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755007
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 755008
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 755009
    if-eqz v2, :cond_1

    .line 755010
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755011
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 755012
    :cond_1
    invoke-virtual {v1, v0, p2, p0}, LX/15i;->a(IIS)S

    move-result v2

    .line 755013
    if-eqz v2, :cond_2

    .line 755014
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755015
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p2, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 755016
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 755017
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 755018
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
