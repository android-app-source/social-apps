.class public final Lcom/facebook/graphql/model/GraphQLFrameImageAsset;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameImageAsset$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameImageAsset$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:D

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752009
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752008
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752006
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752007
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752003
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752004
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->e:Ljava/lang/String;

    .line 752005
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->e:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752000
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752001
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 752002
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751997
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751998
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 751999
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n:Ljava/lang/String;

    .line 751996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 751971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751972
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 751973
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 751974
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 751975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 751976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 751977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 751978
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 751979
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 751980
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 751981
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 751982
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 751983
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 751984
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 751985
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 751986
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 751987
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 751988
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 751989
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 751990
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 751991
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 751992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751993
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 751933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 751935
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 751936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 751937
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751938
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 751939
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 751940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751941
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 751942
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751943
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751944
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 751945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 751947
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751948
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751949
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 751950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751951
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 751952
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751953
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751954
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 751955
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 751957
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751958
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751959
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 751960
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 751961
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 751962
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751963
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 751964
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 751965
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 751966
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 751967
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    .line 751968
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 751969
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751970
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751932
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 751910
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 751911
    const/16 v0, 0x8

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m:D

    .line 751912
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 751931
    const v0, -0x558df47e

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 751930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751925
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751926
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751927
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->g:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751922
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751923
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    .line 751924
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->h:Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751916
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751917
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 751918
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    return-object v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751913
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 751914
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 751915
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m:D

    return-wide v0
.end method
