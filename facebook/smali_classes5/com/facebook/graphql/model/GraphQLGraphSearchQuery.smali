.class public final Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQuery$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQuery$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758089
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758088
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758080
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758081
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p:Ljava/lang/String;

    .line 758092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p:Ljava/lang/String;

    return-object v0
.end method

.method private B()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u:Ljava/util/List;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u:Ljava/util/List;

    .line 758095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v:Ljava/lang/String;

    .line 758098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v:Ljava/lang/String;

    return-object v0
.end method

.method private D()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x:Ljava/util/List;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x:Ljava/util/List;

    .line 758101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 758102
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 758103
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 758104
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->e:I

    return v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->g:Ljava/lang/String;

    .line 758107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->g:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758108
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758109
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->h:Ljava/lang/String;

    .line 758110
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->h:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    .line 758113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758114
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758115
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j:Ljava/lang/String;

    .line 758116
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j:Ljava/lang/String;

    return-object v0
.end method

.method private x()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758117
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758118
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k:Ljava/util/List;

    .line 758119
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758082
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758083
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l:Ljava/util/List;

    .line 758084
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 758085
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758086
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    .line 758087
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 26

    .prologue
    .line 757965
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 757966
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 757967
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 757968
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 757969
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v()Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 757970
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->w()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 757971
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 757972
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y()LX/0Px;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 757973
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 757974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 757975
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->A()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 757976
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 757977
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 757978
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v16

    .line 757979
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 757980
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->B()LX/0Px;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v18

    .line 757981
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->C()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 757982
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 757983
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->D()LX/0Px;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v21

    .line 757984
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 757985
    const/16 v23, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 757986
    const/16 v23, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->s()I

    move-result v24

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 757987
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 757988
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 757989
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 757990
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 757991
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 757992
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 757993
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 757994
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 757995
    const/16 v5, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->z()Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    if-ne v4, v6, :cond_0

    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 757996
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 757997
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 757998
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 757999
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 758000
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758001
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758002
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758003
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758004
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758005
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758006
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 758007
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758008
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4

    .line 758009
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->z()Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 758010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758011
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->D()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 758012
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->D()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 758013
    if-eqz v1, :cond_7

    .line 758014
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758015
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->x:Ljava/util/List;

    move-object v1, v0

    .line 758016
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 758019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758021
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v()Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 758022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v()Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    .line 758023
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->v()Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 758024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758025
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->i:Lcom/facebook/graphql/model/GraphQLGraphSearchModulesConnection;

    .line 758026
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 758027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758028
    if-eqz v2, :cond_2

    .line 758029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758030
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l:Ljava/util/List;

    move-object v1, v0

    .line 758031
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 758032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 758033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 758034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758035
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 758036
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 758037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    .line 758038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 758039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758040
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    .line 758041
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->B()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 758042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->B()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758043
    if-eqz v2, :cond_5

    .line 758044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758045
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u:Ljava/util/List;

    move-object v1, v0

    .line 758046
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758047
    if-nez v1, :cond_6

    :goto_1
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_1

    :cond_7
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758048
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 758049
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 758050
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->e:I

    .line 758051
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 758052
    const v0, -0x1bce060e

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758053
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758054
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 758055
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758056
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758057
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m:Ljava/lang/String;

    .line 758058
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758059
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758060
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o:Ljava/lang/String;

    .line 758061
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758062
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758063
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 758064
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758065
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758066
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    .line 758067
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r:Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758068
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758069
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->s:Ljava/util/List;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->s:Ljava/util/List;

    .line 758070
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758071
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758072
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->t:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->t:Ljava/lang/String;

    .line 758073
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758074
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758075
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->w:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->w:Ljava/lang/String;

    .line 758076
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758077
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758078
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y:Ljava/lang/String;

    .line 758079
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->y:Ljava/lang/String;

    return-object v0
.end method
