.class public final Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774018
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774017
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 774013
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774014
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x42f0a46

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 774015
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o:LX/0x2;

    .line 774016
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774010
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774011
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->i:Ljava/lang/String;

    .line 774012
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774008
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    .line 774009
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774004
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774005
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k:Ljava/lang/String;

    .line 774006
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 774003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773998
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773999
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 774000
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n:Ljava/lang/String;

    .line 773997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 773994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774019
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774020
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g:Ljava/lang/String;

    .line 774021
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773991
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773992
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773993
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->h:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 773988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o:LX/0x2;

    if-nez v0, :cond_0

    .line 773989
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o:LX/0x2;

    .line 773990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 773967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 773969
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 773970
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 773971
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 773972
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 773973
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 773974
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 773975
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 773976
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 773977
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 773978
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 773979
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 773980
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 773981
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 773982
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 773983
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 773984
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 773985
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 773986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 773987
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 773949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773950
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 773951
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    .line 773952
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 773953
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;

    .line 773954
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkConnection;

    .line 773955
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 773956
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773957
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 773958
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;

    .line 773959
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773960
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 773961
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773962
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 773963
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;

    .line 773964
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773965
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 773966
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773948
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 773946
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->h:J

    .line 773947
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 773943
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 773944
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->h:J

    .line 773945
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 773938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 773939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 773940
    :goto_0
    return-object v0

    .line 773941
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 773942
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 773934
    const v0, -0x42f0a46

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773935
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773936
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->f:Ljava/lang/String;

    .line 773937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowAtWorkFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method
