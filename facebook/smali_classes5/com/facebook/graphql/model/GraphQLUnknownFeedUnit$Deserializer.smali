.class public final Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 788917
    const-class v0, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 788918
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 788927
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 788919
    const/16 v0, 0x1b4

    invoke-static {p1, v0}, LX/4U8;->a(LX/15w;S)LX/15i;

    move-result-object v2

    .line 788920
    new-instance v1, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit;-><init>()V

    .line 788921
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 788922
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 788923
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 788924
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 788925
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 788926
    :cond_0
    return-object v1
.end method
