.class public final Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 773744
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 773745
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 773746
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 773747
    const/16 v0, 0x159

    .line 773748
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 773749
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 773750
    const/4 v4, 0x0

    .line 773751
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_7

    .line 773752
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 773753
    :goto_0
    move v2, v4

    .line 773754
    if-eqz v1, :cond_0

    .line 773755
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 773756
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 773757
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 773758
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 773759
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 773760
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 773761
    move-object v2, v1

    .line 773762
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;-><init>()V

    .line 773763
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 773764
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 773765
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 773766
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 773767
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 773768
    :cond_1
    return-object v1

    .line 773769
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 773770
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_6

    .line 773771
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 773772
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 773773
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v8, :cond_3

    .line 773774
    const-string p0, "profile"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 773775
    invoke-static {p1, v3}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 773776
    :cond_4
    const-string p0, "social_context"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 773777
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 773778
    :cond_5
    const-string p0, "tracking"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 773779
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 773780
    :cond_6
    const/4 v8, 0x3

    invoke-virtual {v3, v8}, LX/186;->c(I)V

    .line 773781
    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 773782
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 773783
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 773784
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_7
    move v2, v4

    move v6, v4

    move v7, v4

    goto :goto_1
.end method
