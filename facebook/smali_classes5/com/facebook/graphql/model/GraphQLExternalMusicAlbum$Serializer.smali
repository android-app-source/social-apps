.class public final Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 750782
    const-class v0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 750783
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 750830
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 750785
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 750786
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x3

    .line 750787
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 750788
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 750789
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 750790
    const-string v4, "album_release_date"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750791
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 750792
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750793
    if-eqz v2, :cond_1

    .line 750794
    const-string v3, "application_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750795
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750796
    :cond_1
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 750797
    if-eqz v2, :cond_2

    .line 750798
    const-string v2, "artist_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750799
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 750800
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750801
    if-eqz v2, :cond_3

    .line 750802
    const-string v3, "copy_right"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750803
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750804
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750805
    if-eqz v2, :cond_4

    .line 750806
    const-string v3, "cover_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750807
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750808
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 750809
    if-eqz v2, :cond_5

    .line 750810
    const-string v3, "global_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750811
    invoke-static {v1, v2, p1, p2}, LX/4MV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 750812
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750813
    if-eqz v2, :cond_6

    .line 750814
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750815
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750816
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750817
    if-eqz v2, :cond_7

    .line 750818
    const-string v3, "music_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750819
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750820
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 750821
    if-eqz v2, :cond_8

    .line 750822
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750823
    invoke-static {v1, v2, p1, p2}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 750824
    :cond_8
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 750825
    if-eqz v2, :cond_9

    .line 750826
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 750827
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 750828
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 750829
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 750784
    check-cast p1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;->a(Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;LX/0nX;LX/0my;)V

    return-void
.end method
