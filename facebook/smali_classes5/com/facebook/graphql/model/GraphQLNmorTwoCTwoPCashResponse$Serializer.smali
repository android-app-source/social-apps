.class public final Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 768237
    const-class v0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 768238
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 768239
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 768240
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 768241
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 768242
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 768243
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 768244
    if-eqz p0, :cond_0

    .line 768245
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 768246
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 768247
    :cond_0
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 768248
    if-eqz p0, :cond_1

    .line 768249
    const-string p2, "instructions_url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 768250
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 768251
    :cond_1
    const/4 p0, 0x3

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 768252
    if-eqz p0, :cond_2

    .line 768253
    const-string p2, "url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 768254
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 768255
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 768256
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 768236
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;LX/0nX;LX/0my;)V

    return-void
.end method
