.class public final Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventCreateActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventCreateActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748997
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749000
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 748998
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748999
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 748996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->f:Ljava/lang/String;

    .line 749003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748991
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748992
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->g:Ljava/lang/String;

    .line 748993
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 748988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748989
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 748990
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 748967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748968
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->a()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 748969
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 748970
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 748971
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 748972
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 748973
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 748974
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 748975
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 748976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 748978
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 748980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748981
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->a()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748982
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->a()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 748983
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->a()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 748984
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;

    .line 748985
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventCreateActionLink;->e:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 748986
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748987
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 748979
    const v0, -0x55b63fa

    return v0
.end method
