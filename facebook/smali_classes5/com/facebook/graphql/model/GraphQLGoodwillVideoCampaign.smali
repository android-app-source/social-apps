.class public final Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Z

.field public g:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 757625
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 757626
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 757627
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 757628
    return-void
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757629
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757630
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k:Ljava/lang/String;

    .line 757631
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 757632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 757633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 757634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 757635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 757636
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 757637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 757638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 757639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 757640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 757641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 757642
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 757643
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j()Z

    move-result v10

    invoke-virtual {p1, v0, v10}, LX/186;->a(IZ)V

    .line 757644
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k()Z

    move-result v10

    invoke-virtual {p1, v0, v10}, LX/186;->a(IZ)V

    .line 757645
    const/4 v10, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    if-ne v0, v11, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v10, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 757646
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 757647
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 757648
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 757649
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 757650
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 757651
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 757652
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 757653
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 757654
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 757655
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 757656
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 757657
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 757664
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 757665
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 757666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 757667
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 757668
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 757669
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 757670
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 757671
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 757672
    if-eqz v2, :cond_1

    .line 757673
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 757674
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j:Ljava/util/List;

    move-object v1, v0

    .line 757675
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 757676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 757677
    if-eqz v2, :cond_2

    .line 757678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 757679
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l:Ljava/util/List;

    move-object v1, v0

    .line 757680
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 757681
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 757659
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 757660
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->e:Z

    .line 757661
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->f:Z

    .line 757662
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 757663
    const v0, 0x30c0d4bc

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 757619
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 757620
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 757621
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->e:Z

    return v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 757622
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 757623
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 757624
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->f:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 757592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->g:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->g:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->g:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    .line 757594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->g:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 757597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->i:Ljava/lang/String;

    .line 757600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757602
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j:Ljava/util/List;

    .line 757603
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l:Ljava/util/List;

    .line 757606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757607
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757608
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m:Ljava/lang/String;

    .line 757609
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757610
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757611
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n:Ljava/lang/String;

    .line 757612
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o:Ljava/lang/String;

    .line 757615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757616
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757617
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p:Ljava/lang/String;

    .line 757618
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p:Ljava/lang/String;

    return-object v0
.end method
