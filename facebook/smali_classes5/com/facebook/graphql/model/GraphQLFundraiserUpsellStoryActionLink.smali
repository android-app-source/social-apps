.class public final Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754271
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754270
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 754268
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 754269
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754265
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754266
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 754267
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 754262
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754263
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 754264
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754272
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754273
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->g:Ljava/lang/String;

    .line 754274
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754259
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754260
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->h:Ljava/lang/String;

    .line 754261
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754253
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754254
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 754255
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 754237
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754238
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->a()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 754239
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 754240
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 754241
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 754242
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->n()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 754243
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 754244
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 754245
    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 754246
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 754247
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 754248
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 754249
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 754250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754251
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 754252
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 754219
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754220
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->a()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754221
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->a()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 754222
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->a()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 754223
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;

    .line 754224
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->e:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 754225
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->n()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 754226
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->n()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 754227
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->n()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 754228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;

    .line 754229
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->j:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 754230
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 754231
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754232
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 754233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;

    .line 754234
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserUpsellStoryActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754235
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754236
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 754218
    const v0, -0x14a78e73

    return v0
.end method
