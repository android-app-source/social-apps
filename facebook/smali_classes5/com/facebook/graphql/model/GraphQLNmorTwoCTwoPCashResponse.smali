.class public final Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 768284
    const-class v0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 768257
    const-class v0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 768282
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 768283
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 768279
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 768280
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->e:Ljava/lang/String;

    .line 768281
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 768276
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 768277
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->f:Ljava/lang/String;

    .line 768278
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 768273
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 768274
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->g:Ljava/lang/String;

    .line 768275
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 768263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 768264
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 768265
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 768266
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 768267
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 768268
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 768269
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 768270
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 768271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 768272
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 768260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 768261
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 768262
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 768259
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNmorTwoCTwoPCashResponse;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 768258
    const v0, -0x25d1010f

    return v0
.end method
