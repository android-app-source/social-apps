.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756128
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756159
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 756157
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 756158
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 756149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 756151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 756152
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 756153
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 756154
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 756155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756156
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 756136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 756138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 756139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 756140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 756141
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756142
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 756143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 756145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 756146
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756147
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756148
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756133
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756134
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756135
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 756132
    const v0, 0x37f2ba48

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
