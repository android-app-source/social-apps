.class public final Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766391
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 766392
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766390
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 766377
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 766378
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 766379
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 766380
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 766381
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_0

    .line 766382
    const-string v4, "height_ratio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766383
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 766384
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 766385
    if-eqz v2, :cond_1

    .line 766386
    const-string v3, "hide_share_button"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766387
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 766388
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 766389
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 766376
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata$Serializer;->a(Lcom/facebook/graphql/model/GraphQLMessengerPlatformWebviewMetadata;LX/0nX;LX/0my;)V

    return-void
.end method
