.class public final Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:D

.field public l:D

.field public m:D

.field public n:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774552
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774555
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774553
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774554
    return-void
.end method

.method public constructor <init>(LX/4Y5;)V
    .locals 2

    .prologue
    .line 774540
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774541
    iget v0, p1, LX/4Y5;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->e:I

    .line 774542
    iget v0, p1, LX/4Y5;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->f:I

    .line 774543
    iget v0, p1, LX/4Y5;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->g:I

    .line 774544
    iget v0, p1, LX/4Y5;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->h:I

    .line 774545
    iget v0, p1, LX/4Y5;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->i:I

    .line 774546
    iget v0, p1, LX/4Y5;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j:I

    .line 774547
    iget-wide v0, p1, LX/4Y5;->h:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k:D

    .line 774548
    iget-wide v0, p1, LX/4Y5;->i:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l:D

    .line 774549
    iget-wide v0, p1, LX/4Y5;->j:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m:D

    .line 774550
    iget-wide v0, p1, LX/4Y5;->k:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n:D

    .line 774551
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 774537
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774538
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774539
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 774523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774524
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 774525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->a()I

    move-result v0

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 774526
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 774527
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 774528
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 774529
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 774530
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 774531
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->o()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 774532
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->p()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 774533
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->q()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 774534
    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->r()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 774535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774536
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 774520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774521
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774522
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 774508
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 774509
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->e:I

    .line 774510
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->f:I

    .line 774511
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->g:I

    .line 774512
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->h:I

    .line 774513
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->i:I

    .line 774514
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j:I

    .line 774515
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k:D

    .line 774516
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l:D

    .line 774517
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m:D

    .line 774518
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n:D

    .line 774519
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 774556
    const v0, 0x257b70ae

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774505
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774506
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774507
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->f:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774502
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774503
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774504
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->g:I

    return v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774499
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774500
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774501
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->h:I

    return v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774496
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774497
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774498
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->i:I

    return v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774493
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774494
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774495
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j:I

    return v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774490
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774491
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774492
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k:D

    return-wide v0
.end method

.method public final p()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774481
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774482
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774483
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l:D

    return-wide v0
.end method

.method public final q()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774487
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774488
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774489
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m:D

    return-wide v0
.end method

.method public final r()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 774484
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774485
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774486
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n:D

    return-wide v0
.end method
