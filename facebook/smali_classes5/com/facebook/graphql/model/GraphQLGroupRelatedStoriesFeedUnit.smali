.class public final Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760433
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760432
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 760428
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 760429
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x44c718b7

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 760430
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n:LX/0x2;

    .line 760431
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760425
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760426
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->j:Ljava/lang/String;

    .line 760427
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760422
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760423
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760424
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 760421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g:Ljava/lang/String;

    .line 760420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760415
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 760416
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 760417
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->h:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 760412
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n:LX/0x2;

    if-nez v0, :cond_0

    .line 760413
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n:LX/0x2;

    .line 760414
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 760393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 760395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 760396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 760397
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 760398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 760399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 760400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 760401
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 760402
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 760403
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 760404
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 760405
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 760406
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 760407
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 760408
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 760409
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 760410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760411
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 760434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 760436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 760437
    if-eqz v1, :cond_3

    .line 760438
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    .line 760439
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 760440
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 760441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 760443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    .line 760444
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760445
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 760446
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760447
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 760448
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    .line 760449
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760450
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760451
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760368
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 760370
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->h:J

    .line 760371
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 760372
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 760373
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->h:J

    .line 760374
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760369
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760375
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760376
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l:Ljava/lang/String;

    .line 760377
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 760379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 760380
    :goto_0
    return-object v0

    .line 760381
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 760382
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 760383
    const v0, -0x44c718b7

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760384
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760385
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->f:Ljava/lang/String;

    .line 760386
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760387
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760388
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->i:Ljava/util/List;

    .line 760389
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760390
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760391
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760392
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
