.class public final Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781304
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781310
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781308
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 781309
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->i:Ljava/lang/String;

    .line 781307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 781290
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 781292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 781293
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 781294
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 781295
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 781296
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 781297
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 781298
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 781299
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 781300
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 781301
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 781302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781303
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 781311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 781313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781314
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 781315
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    .line 781316
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781317
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 781318
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    .line 781319
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 781320
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    .line 781321
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    .line 781322
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781323
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 781288
    const v0, 0x34f353ec

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781276
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781277
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->e:Ljava/lang/String;

    .line 781278
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781285
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781286
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->f:Ljava/lang/String;

    .line 781287
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781282
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781283
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781284
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781279
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781280
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    .line 781281
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->h:Lcom/facebook/graphql/model/GraphQLResearchPollResponseRespondersConnection;

    return-object v0
.end method
