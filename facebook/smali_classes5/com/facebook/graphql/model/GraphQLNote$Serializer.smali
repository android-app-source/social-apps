.class public final Lcom/facebook/graphql/model/GraphQLNote$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNote;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 769460
    const-class v0, Lcom/facebook/graphql/model/GraphQLNote;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNote$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNote$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 769461
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 769349
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNote;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 769351
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 769352
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x19

    .line 769353
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 769354
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769355
    if-eqz v2, :cond_0

    .line 769356
    const-string v3, "blurredCoverPhoto"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769357
    invoke-static {v1, v2, p1, p2}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 769358
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769359
    if-eqz v2, :cond_1

    .line 769360
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769361
    invoke-static {v1, v2, p1, p2}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 769362
    :cond_1
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769363
    if-eqz v2, :cond_2

    .line 769364
    const-string v3, "feedAwesomizerProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769365
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769366
    :cond_2
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769367
    if-eqz v2, :cond_3

    .line 769368
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769369
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769370
    :cond_3
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769371
    if-eqz v2, :cond_4

    .line 769372
    const-string v3, "from"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769373
    invoke-static {v1, v2, p1, p2}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769374
    :cond_4
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769375
    if-eqz v2, :cond_5

    .line 769376
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769377
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769378
    :cond_5
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769379
    if-eqz v2, :cond_6

    .line 769380
    const-string v3, "imageHighOrig"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769381
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769382
    :cond_6
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769383
    if-eqz v2, :cond_7

    .line 769384
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769385
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769386
    :cond_7
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769387
    if-eqz v2, :cond_8

    .line 769388
    const-string v3, "plain_body"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769389
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769390
    :cond_8
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769391
    if-eqz v2, :cond_9

    .line 769392
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769393
    invoke-static {v1, v2, p1, p2}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 769394
    :cond_9
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769395
    if-eqz v2, :cond_a

    .line 769396
    const-string v3, "profileImageLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769397
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769398
    :cond_a
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769399
    if-eqz v2, :cond_b

    .line 769400
    const-string v3, "profileImageSmall"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769401
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769402
    :cond_b
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769403
    if-eqz v2, :cond_c

    .line 769404
    const-string v3, "profilePicture50"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769405
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769406
    :cond_c
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769407
    if-eqz v2, :cond_d

    .line 769408
    const-string v3, "profilePictureHighRes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769409
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769410
    :cond_d
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769411
    if-eqz v2, :cond_e

    .line 769412
    const-string v3, "profilePictureLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769413
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769414
    :cond_e
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769415
    if-eqz v2, :cond_f

    .line 769416
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769417
    invoke-static {v1, v2, p1, p2}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769418
    :cond_f
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769419
    if-eqz v2, :cond_10

    .line 769420
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769421
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769422
    :cond_10
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 769423
    if-eqz v2, :cond_11

    .line 769424
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769425
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 769426
    :cond_11
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769427
    if-eqz v2, :cond_12

    .line 769428
    const-string v3, "published_document"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769429
    invoke-static {v1, v2, p1, p2}, LX/4LL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 769430
    :cond_12
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769431
    if-eqz v2, :cond_13

    .line 769432
    const-string v3, "streaming_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769433
    invoke-static {v1, v2, p1}, LX/4TN;->a(LX/15i;ILX/0nX;)V

    .line 769434
    :cond_13
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769435
    if-eqz v2, :cond_14

    .line 769436
    const-string v3, "subject"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769437
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769438
    :cond_14
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769439
    if-eqz v2, :cond_15

    .line 769440
    const-string v3, "taggable_object_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769441
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769442
    :cond_15
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769443
    if-eqz v2, :cond_16

    .line 769444
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769445
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769446
    :cond_16
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IIS)S

    move-result v2

    .line 769447
    if-eqz v2, :cond_17

    .line 769448
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769449
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769450
    :cond_17
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769451
    if-eqz v2, :cond_18

    .line 769452
    const-string v3, "profilePicture180"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769453
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769454
    :cond_18
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769455
    if-eqz v2, :cond_19

    .line 769456
    const-string v3, "publisher_profile_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769457
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769458
    :cond_19
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 769459
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 769350
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNote;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNote$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNote;LX/0nX;LX/0my;)V

    return-void
.end method
