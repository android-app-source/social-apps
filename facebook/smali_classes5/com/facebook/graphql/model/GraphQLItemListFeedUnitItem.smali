.class public final Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLJobOpening;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762286
    const-class v0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762285
    const-class v0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 762283
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 762284
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762277
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 762278
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 762279
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 762280
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 762281
    const/4 v0, 0x0

    .line 762282
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762274
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762275
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762276
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 762263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762264
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 762265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 762266
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 762267
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 762268
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 762269
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 762270
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 762271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762272
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 762273
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 762246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 762248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 762250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    .line 762251
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762252
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 762253
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762254
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 762255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    .line 762256
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762257
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762258
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLJobOpening;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLJobOpening;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLJobOpening;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLJobOpening;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLJobOpening;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 762259
    const v0, -0x6890587a

    return v0
.end method
