.class public final Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 775425
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 775426
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775427
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 775428
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 775429
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 775430
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 775431
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775432
    if-eqz v2, :cond_1

    .line 775433
    const-string p0, "edges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775434
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 775435
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 775436
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/4RW;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 775437
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 775438
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 775439
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 775440
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 775441
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;LX/0nX;LX/0my;)V

    return-void
.end method
