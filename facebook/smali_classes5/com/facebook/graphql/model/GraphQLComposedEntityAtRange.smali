.class public final Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public g:I

.field public h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746248
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746249
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 746246
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 746247
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->f:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746244
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->f:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->f:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 746245
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->f:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 746233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 746235
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 746236
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 746237
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->l()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 746238
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 746239
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 746240
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746241
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 746242
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->l()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 746250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 746252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 746253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 746254
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    .line 746255
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 746256
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746257
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 746230
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746231
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntity;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 746232
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 746226
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 746227
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->g:I

    .line 746228
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->h:I

    .line 746229
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 746225
    const v0, 0xbe38f55

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746219
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 746220
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 746221
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->g:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746222
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 746223
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 746224
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->h:I

    return v0
.end method
