.class public final Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize$Serializer;
.end annotation


# instance fields
.field public e:D

.field public f:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752106
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752090
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752104
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752105
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 752101
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752102
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752103
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 752107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752108
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 752109
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->a()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 752110
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->j()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 752111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752112
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 752113
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->j()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 752098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752099
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752100
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 752095
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 752096
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->e:D

    .line 752097
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 752094
    const v0, -0x3c3c315d

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->f:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->f:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->f:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 752093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->f:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    return-object v0
.end method
