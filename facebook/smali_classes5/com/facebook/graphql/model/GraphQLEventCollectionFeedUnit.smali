.class public final Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748786
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748787
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 748788
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748789
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4ed3e2e6

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 748790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    .line 748791
    return-void
.end method

.method public constructor <init>(LX/4WA;)V
    .locals 2

    .prologue
    .line 748792
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748793
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4ed3e2e6

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 748794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    .line 748795
    iget-object v0, p1, LX/4WA;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->f:Ljava/lang/String;

    .line 748796
    iget-object v0, p1, LX/4WA;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g:Ljava/lang/String;

    .line 748797
    iget-object v0, p1, LX/4WA;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748798
    iget-wide v0, p1, LX/4WA;->e:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->i:J

    .line 748799
    iget-object v0, p1, LX/4WA;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->j:Ljava/lang/String;

    .line 748800
    iget-object v0, p1, LX/4WA;->g:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 748801
    iget-object v0, p1, LX/4WA;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748802
    iget-object v0, p1, LX/4WA;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->m:Ljava/lang/String;

    .line 748803
    iget-object v0, p1, LX/4WA;->j:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    .line 748804
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 748805
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748806
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748807
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g:Ljava/lang/String;

    .line 748808
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 748809
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 748810
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 748811
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->i:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 748812
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 748813
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    if-nez v0, :cond_0

    .line 748814
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    .line 748815
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 748816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 748817
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 748818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 748820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 748821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 748822
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 748823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 748824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 748825
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 748826
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 748827
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 748828
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 748829
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 748830
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 748831
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 748832
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 748833
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 748834
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 748835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748836
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 748739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 748743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    .line 748744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748745
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 748746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 748747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 748748
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    .line 748749
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 748750
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 748751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 748753
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    .line 748754
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748755
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748756
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 748837
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->i:J

    .line 748838
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 748736
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 748737
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->i:J

    .line 748738
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748757
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748758
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->m:Ljava/lang/String;

    .line 748760
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 748761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748762
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 748763
    :goto_0
    return-object v0

    .line 748764
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 748765
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 748766
    const v0, -0x4ed3e2e6

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->f:Ljava/lang/String;

    .line 748769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 748770
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->j:Ljava/lang/String;

    .line 748776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 748777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 748778
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 748781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748782
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748783
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748784
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
