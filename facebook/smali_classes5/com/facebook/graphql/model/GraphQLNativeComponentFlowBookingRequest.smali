.class public final Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLProductItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTimeRange;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767361
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767360
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 767358
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 767359
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 767355
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767356
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 767357
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767352
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767353
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 767354
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLProductItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->h:Lcom/facebook/graphql/model/GraphQLProductItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767350
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->h:Lcom/facebook/graphql/model/GraphQLProductItem;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->h:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 767351
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->h:Lcom/facebook/graphql/model/GraphQLProductItem;

    return-object v0
.end method

.method private n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 767265
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 767266
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 767267
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->i:J

    return-wide v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767346
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767347
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j:Ljava/lang/String;

    .line 767348
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767343
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767344
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k:Ljava/lang/String;

    .line 767345
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 767340
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 767341
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 767342
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l:J

    return-wide v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767362
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767363
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m:Ljava/lang/String;

    .line 767364
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLTimeRange;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767337
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n:Lcom/facebook/graphql/model/GraphQLTimeRange;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767338
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n:Lcom/facebook/graphql/model/GraphQLTimeRange;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimeRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimeRange;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n:Lcom/facebook/graphql/model/GraphQLTimeRange;

    .line 767339
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n:Lcom/facebook/graphql/model/GraphQLTimeRange;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767334
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767335
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->o:Ljava/lang/String;

    .line 767336
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767331
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767332
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p:Lcom/facebook/graphql/model/GraphQLUser;

    .line 767333
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767328
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767329
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->q:Ljava/lang/String;

    .line 767330
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 767300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 767302
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 767303
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 767304
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 767305
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 767306
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 767307
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->s()Lcom/facebook/graphql/model/GraphQLTimeRange;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 767308
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 767309
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->u()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 767310
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 767311
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 767312
    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v4, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 767313
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 767314
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 767315
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 767316
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 767317
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 767318
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 767319
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->q()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 767320
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 767321
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 767322
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 767323
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 767324
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 767325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767326
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 767327
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 767277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767278
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 767279
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 767280
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 767281
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 767282
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 767283
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 767284
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 767285
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->m()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 767286
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 767287
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->h:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 767288
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->s()Lcom/facebook/graphql/model/GraphQLTimeRange;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 767289
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->s()Lcom/facebook/graphql/model/GraphQLTimeRange;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimeRange;

    .line 767290
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->s()Lcom/facebook/graphql/model/GraphQLTimeRange;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 767291
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 767292
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->n:Lcom/facebook/graphql/model/GraphQLTimeRange;

    .line 767293
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->u()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 767294
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->u()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 767295
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->u()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 767296
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    .line 767297
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->p:Lcom/facebook/graphql/model/GraphQLUser;

    .line 767298
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767299
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 767272
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 767273
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->i:J

    .line 767274
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->l:J

    .line 767275
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 767271
    const v0, -0x55958a16

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767268
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767269
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->f:Ljava/lang/String;

    .line 767270
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->f:Ljava/lang/String;

    return-object v0
.end method
