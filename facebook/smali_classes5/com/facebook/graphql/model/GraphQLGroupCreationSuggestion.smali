.class public final Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759636
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759568
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759634
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759635
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->e:Ljava/lang/String;

    .line 759633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 759628
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759629
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 759630
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 759625
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759626
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->g:Ljava/util/List;

    .line 759627
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759622
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759623
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 759624
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->i:Ljava/lang/String;

    .line 759639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 759616
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759617
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 759618
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 759615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 759593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759594
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 759595
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 759596
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 759597
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 759598
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 759599
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 759600
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 759601
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 759602
    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v0, v8, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v7, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 759603
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 759604
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 759605
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 759606
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 759607
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 759608
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 759609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 759611
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    goto :goto_0

    .line 759612
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759570
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759571
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759572
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 759573
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759575
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 759576
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 759577
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 759578
    if-eqz v2, :cond_1

    .line 759579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759580
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->g:Ljava/util/List;

    move-object v1, v0

    .line 759581
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 759582
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 759583
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 759584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759585
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 759586
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 759587
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759588
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 759589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759590
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759591
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759592
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759569
    const v0, 0x6c63d302

    return v0
.end method
