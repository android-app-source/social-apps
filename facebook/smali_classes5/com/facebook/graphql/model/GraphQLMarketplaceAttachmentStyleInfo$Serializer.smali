.class public final Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764894
    const-class v0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764895
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764896
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 764881
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 764882
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 764883
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764884
    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 764885
    if-eqz v2, :cond_0

    .line 764886
    const-string v3, "destination_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764887
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764888
    :cond_0
    invoke-virtual {v1, v0, p2, p0}, LX/15i;->a(IIS)S

    move-result v2

    .line 764889
    if-eqz v2, :cond_1

    .line 764890
    const-string v2, "destination_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764891
    const-class v2, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-virtual {v1, v0, p2, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764892
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764893
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764880
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;->a(Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;LX/0nX;LX/0my;)V

    return-void
.end method
