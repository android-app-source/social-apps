.class public final Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744151
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 744152
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744114
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 744115
    const/16 v0, 0x83

    .line 744116
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 744117
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 744118
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 744119
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_7

    .line 744120
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 744121
    :goto_0
    move v2, v4

    .line 744122
    if-eqz v1, :cond_0

    .line 744123
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 744124
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 744125
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 744126
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 744127
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 744128
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 744129
    move-object v2, v1

    .line 744130
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;-><init>()V

    .line 744131
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 744132
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 744133
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 744134
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 744135
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 744136
    :cond_1
    return-object v1

    .line 744137
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_5

    .line 744138
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 744139
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 744140
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 744141
    const-string p0, "inject_status"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 744142
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v2

    move v8, v2

    move v2, v6

    goto :goto_1

    .line 744143
    :cond_3
    const-string p0, "viewer"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 744144
    invoke-static {p1, v3}, LX/262;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 744145
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 744146
    :cond_5
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 744147
    if-eqz v2, :cond_6

    .line 744148
    invoke-virtual {v3, v4, v8}, LX/186;->a(IZ)V

    .line 744149
    :cond_6
    invoke-virtual {v3, v6, v7}, LX/186;->b(II)V

    .line 744150
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_7
    move v2, v4

    move v7, v4

    move v8, v4

    goto :goto_1
.end method
