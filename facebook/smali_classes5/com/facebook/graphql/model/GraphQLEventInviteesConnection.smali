.class public final Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventInviteesConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventInviteesConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventInviteesEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749271
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749270
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 749268
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 749269
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventInviteesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 749265
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749266
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->f:Ljava/util/List;

    .line 749267
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 749262
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749263
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->g:Ljava/util/List;

    .line 749264
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749259
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749260
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 749261
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749272
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 749273
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 749274
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->i:I

    return v0
.end method

.method private n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749226
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 749227
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 749228
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->j:I

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 749210
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 749211
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 749212
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 749213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749214
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 749215
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 749216
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 749217
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 749218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 749219
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 749220
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 749221
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 749222
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 749223
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 749224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749225
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 749241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749242
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 749243
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 749244
    if-eqz v1, :cond_0

    .line 749245
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 749246
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->f:Ljava/util/List;

    .line 749247
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 749248
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 749249
    if-eqz v1, :cond_1

    .line 749250
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 749251
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 749252
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 749253
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 749254
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 749255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 749256
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 749257
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749258
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 749229
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->e:I

    .line 749230
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 749231
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 749232
    if-eqz v0, :cond_0

    .line 749233
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 749234
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 749235
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 749236
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->e:I

    .line 749237
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->i:I

    .line 749238
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->j:I

    .line 749239
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 749240
    const v0, -0x1c7adc31

    return v0
.end method
