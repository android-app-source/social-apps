.class public final Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782113
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782112
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782110
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782111
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782107
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782108
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->e:Ljava/lang/String;

    .line 782109
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782104
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782105
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->f:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->f:Ljava/util/List;

    .line 782106
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782101
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782102
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->g:Ljava/lang/String;

    .line 782103
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782098
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->h:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782099
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->h:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->h:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 782100
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->h:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782095
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782096
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->i:Ljava/lang/String;

    .line 782097
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782066
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782067
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j:Ljava/lang/String;

    .line 782068
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782092
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782093
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->k:Ljava/lang/String;

    .line 782094
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 782074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 782076
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 782077
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 782078
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 782079
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 782080
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 782081
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 782082
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 782083
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 782084
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 782085
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->m()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 782086
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 782087
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 782088
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 782089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782090
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 782091
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->m()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 782071
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782073
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782070
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782069
    const v0, -0x6ec8414f

    return v0
.end method
