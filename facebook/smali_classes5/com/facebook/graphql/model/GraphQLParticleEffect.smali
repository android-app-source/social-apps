.class public final Lcom/facebook/graphql/model/GraphQLParticleEffect;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffect$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffect$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772715
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffect$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772730
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffect$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772728
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 772729
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772725
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772726
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l:Ljava/lang/String;

    .line 772727
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772722
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772723
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m:Ljava/lang/String;

    .line 772724
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 772719
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772720
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772721
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->n:Z

    return v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772716
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772717
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772718
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 772637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 772639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 772640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 772641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 772642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 772643
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 772644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 772645
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 772646
    const/16 v8, 0xb

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 772647
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 772648
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 772649
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 772650
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 772651
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 772652
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 772653
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 772654
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 772655
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 772656
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->s()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 772657
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 772658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 772687
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 772689
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 772691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 772692
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772693
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 772694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 772695
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 772696
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 772697
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772698
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 772699
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    .line 772700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 772701
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 772702
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    .line 772703
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 772704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 772705
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 772706
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 772707
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 772708
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 772709
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 772710
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 772711
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 772712
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772713
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772714
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 772682
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 772683
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->h:Z

    .line 772684
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k:Z

    .line 772685
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->n:Z

    .line 772686
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 772681
    const v0, -0x70589c29

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772675
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772676
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772677
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772672
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772673
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    .line 772674
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772669
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772670
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772671
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->h:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772666
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772667
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->i:Ljava/lang/String;

    .line 772668
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772663
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772664
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 772665
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772660
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772661
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772662
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k:Z

    return v0
.end method
