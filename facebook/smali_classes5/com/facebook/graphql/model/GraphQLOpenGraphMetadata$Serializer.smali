.class public final Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 770234
    const-class v0, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 770235
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 770236
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 770237
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 770238
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/2uX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 770239
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 770240
    check-cast p1, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata$Serializer;->a(Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;LX/0nX;LX/0my;)V

    return-void
.end method
