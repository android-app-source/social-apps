.class public final Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public f:D

.field public g:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752356
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752328
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752354
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752355
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 752346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752347
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 752348
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 752349
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->j()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 752350
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->k()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 752351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752352
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 752353
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 752343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752345
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752340
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752341
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 752342
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 752336
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 752337
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->f:D

    .line 752338
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->g:D

    .line 752339
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 752335
    const v0, 0x11216017

    return v0
.end method

.method public final j()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752332
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752333
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752334
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->f:D

    return-wide v0
.end method

.method public final k()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752329
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752330
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752331
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->g:D

    return-wide v0
.end method
