.class public final Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 775423
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 775424
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775422
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 775384
    const/16 v0, 0x1c7

    .line 775385
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 775386
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 775387
    const/4 v4, 0x0

    .line 775388
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_6

    .line 775389
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 775390
    :goto_0
    move v2, v4

    .line 775391
    if-eqz v1, :cond_0

    .line 775392
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 775393
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 775394
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 775395
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 775396
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 775397
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 775398
    move-object v2, v1

    .line 775399
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;-><init>()V

    .line 775400
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 775401
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 775402
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 775403
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 775404
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 775405
    :cond_1
    return-object v1

    .line 775406
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 775407
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_5

    .line 775408
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 775409
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 775410
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v6, :cond_3

    .line 775411
    const-string p0, "edges"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 775412
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 775413
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, p0, :cond_4

    .line 775414
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, p0, :cond_4

    .line 775415
    invoke-static {p1, v3}, LX/4RW;->b(LX/15w;LX/186;)I

    move-result v6

    .line 775416
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 775417
    :cond_4
    invoke-static {v2, v3}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 775418
    goto :goto_1

    .line 775419
    :cond_5
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 775420
    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 775421
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_6
    move v2, v4

    goto :goto_1
.end method
