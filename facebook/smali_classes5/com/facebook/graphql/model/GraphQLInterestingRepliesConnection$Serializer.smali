.class public final Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 762211
    const-class v0, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 762212
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 762210
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 762214
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 762215
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/3b8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 762216
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 762213
    check-cast p1, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;LX/0nX;LX/0my;)V

    return-void
.end method
