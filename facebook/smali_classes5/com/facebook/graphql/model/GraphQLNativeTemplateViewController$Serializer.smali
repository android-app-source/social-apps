.class public final Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767698
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 767699
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767700
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 767701
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 767702
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 767703
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 767704
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 767705
    if-eqz v2, :cond_0

    .line 767706
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767707
    invoke-static {v1, v0, p0, p1}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 767708
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 767709
    if-eqz v2, :cond_1

    .line 767710
    const-string p0, "analytics_module"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767711
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 767712
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 767713
    if-eqz v2, :cond_2

    .line 767714
    const-string p0, "background_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767715
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 767716
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 767717
    if-eqz v2, :cond_3

    .line 767718
    const-string p0, "can_ptr"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767719
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 767720
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767721
    if-eqz v2, :cond_4

    .line 767722
    const-string p0, "native_template_view"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767723
    invoke-static {v1, v2, p1, p2}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 767724
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 767725
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 767726
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;LX/0nX;LX/0my;)V

    return-void
.end method
