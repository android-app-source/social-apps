.class public final Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 775607
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 775608
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775606
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 775564
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 775565
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    .line 775566
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 775567
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 775568
    if-eqz v2, :cond_0

    .line 775569
    const-string v3, "instant_experience_app_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775570
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 775571
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 775572
    if-eqz v2, :cond_1

    .line 775573
    const-string v3, "instant_experience_link_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775574
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 775575
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 775576
    if-eqz v2, :cond_2

    .line 775577
    const-string v2, "instant_experience_domain_whitelist"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775578
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 775579
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775580
    if-eqz v2, :cond_3

    .line 775581
    const-string v3, "instant_experience_app"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775582
    invoke-static {v1, v2, p1, p2}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 775583
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 775584
    if-eqz v2, :cond_4

    .line 775585
    const-string v3, "instant_experience_user_app_scoped_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775586
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 775587
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 775588
    if-eqz v2, :cond_5

    .line 775589
    const-string v3, "instant_experience_user_page_scoped_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775590
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 775591
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775592
    if-eqz v2, :cond_6

    .line 775593
    const-string v3, "instant_experience_feature_enabled_list"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775594
    invoke-static {v1, v2, p1}, LX/4RY;->a(LX/15i;ILX/0nX;)V

    .line 775595
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775596
    if-eqz v2, :cond_7

    .line 775597
    const-string v3, "instant_experience_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775598
    invoke-static {v1, v2, p1, p2}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 775599
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 775600
    if-eqz v2, :cond_8

    .line 775601
    const-string v3, "instant_experience_ad_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775602
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 775603
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 775604
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 775605
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;LX/0nX;LX/0my;)V

    return-void
.end method
