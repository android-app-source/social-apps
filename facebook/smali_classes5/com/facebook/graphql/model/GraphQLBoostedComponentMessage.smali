.class public final Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLError;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745360
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745359
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 745357
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 745358
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    .line 745356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 745353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    return-object v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745348
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745349
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745350
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 745331
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745332
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 745333
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j()Lcom/facebook/graphql/model/GraphQLError;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 745334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 745335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 745336
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 745337
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 745338
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 745339
    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    if-ne v0, v5, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 745340
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->n()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-ne v2, v5, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 745341
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 745342
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 745343
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 745344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 745346
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v0

    goto :goto_0

    .line 745347
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->n()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 745308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745309
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 745310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 745311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 745312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 745313
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 745314
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j()Lcom/facebook/graphql/model/GraphQLError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 745315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j()Lcom/facebook/graphql/model/GraphQLError;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLError;

    .line 745316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j()Lcom/facebook/graphql/model/GraphQLError;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 745317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 745318
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->f:Lcom/facebook/graphql/model/GraphQLError;

    .line 745319
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 745320
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745321
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 745322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 745323
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745324
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 745325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745326
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 745327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 745328
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745329
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745330
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 745307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->e:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 745302
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745303
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k:Z

    .line 745304
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 745292
    const v0, -0x2d2d0034

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLError;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->f:Lcom/facebook/graphql/model/GraphQLError;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->f:Lcom/facebook/graphql/model/GraphQLError;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLError;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLError;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->f:Lcom/facebook/graphql/model/GraphQLError;

    .line 745301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->f:Lcom/facebook/graphql/model/GraphQLError;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
