.class public final Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743212
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743211
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 743209
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 743210
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743206
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743207
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->f:Ljava/lang/String;

    .line 743208
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743204
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 743205
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 743161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v0

    .line 743163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 743164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 743165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 743166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 743167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 743168
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 743169
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 743170
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 743171
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 743172
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 743173
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 743174
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 743175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743176
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 743190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 743192
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 743193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 743194
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;

    .line 743195
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 743196
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 743197
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 743198
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 743199
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;

    .line 743200
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 743201
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743202
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743187
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->e:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743188
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->e:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x0

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 743189
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->e:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->h:Ljava/lang/String;

    .line 743186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 743183
    const v0, 0x46703b55

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->i:Ljava/lang/String;

    .line 743182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j:Ljava/lang/String;

    .line 743179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j:Ljava/lang/String;

    return-object v0
.end method
