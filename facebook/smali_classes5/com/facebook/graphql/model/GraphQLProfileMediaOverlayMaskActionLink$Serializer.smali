.class public final Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 776904
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 776905
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 776952
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 776907
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 776908
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v8, 0x0

    const/16 v6, 0x9

    const/4 v5, 0x0

    .line 776909
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 776910
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 776911
    if-eqz v2, :cond_0

    .line 776912
    const-string v3, "application"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776913
    invoke-static {v1, v2, p1, p2}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 776914
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 776915
    cmp-long v4, v2, v8

    if-eqz v4, :cond_1

    .line 776916
    const-string v4, "default_expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776917
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 776918
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776919
    if-eqz v2, :cond_2

    .line 776920
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776921
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776922
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776923
    if-eqz v2, :cond_3

    .line 776924
    const-string v3, "mask"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776925
    invoke-static {v1, v2, p1}, LX/4Rr;->a(LX/15i;ILX/0nX;)V

    .line 776926
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776927
    if-eqz v2, :cond_4

    .line 776928
    const-string v3, "not_installed_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776929
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776930
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 776931
    if-eqz v2, :cond_5

    .line 776932
    const-string v3, "not_installed_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776933
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776934
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776935
    if-eqz v2, :cond_6

    .line 776936
    const-string v3, "profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776937
    invoke-static {v1, v2, p1, p2}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776938
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 776939
    if-eqz v2, :cond_7

    .line 776940
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776941
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776942
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 776943
    if-eqz v2, :cond_8

    .line 776944
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776945
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776946
    :cond_8
    invoke-virtual {v1, v0, v6, v5}, LX/15i;->a(IIS)S

    move-result v2

    .line 776947
    if-eqz v2, :cond_9

    .line 776948
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776949
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, v6, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776950
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 776951
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 776906
    check-cast p1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
