.class public final Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 775710
    const-class v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 775711
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775712
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 775713
    const/16 v0, 0x26e

    invoke-static {p1, v0}, LX/4RZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    .line 775714
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;-><init>()V

    .line 775715
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 775716
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 775717
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 775718
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 775719
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 775720
    :cond_0
    return-object v1
.end method
