.class public final Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774942
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774941
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774939
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774940
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 774927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 774929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 774930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 774931
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 774932
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 774933
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 774934
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->k()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 774935
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 774936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774937
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 774938
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->k()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 774943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->e:Ljava/util/List;

    .line 774945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 774924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774926
    return-object p0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 774923
    const v0, -0x5940f6fd

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 774920
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774921
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->f:Ljava/util/List;

    .line 774922
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774917
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->g:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774918
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->g:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->g:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    .line 774919
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->g:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774914
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774915
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->h:Ljava/lang/String;

    .line 774916
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->h:Ljava/lang/String;

    return-object v0
.end method
