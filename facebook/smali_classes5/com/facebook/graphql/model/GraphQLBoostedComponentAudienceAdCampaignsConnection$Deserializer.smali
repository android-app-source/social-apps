.class public final Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 745195
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 745196
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 745197
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 745198
    const/16 v0, 0x22a

    .line 745199
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 745200
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 745201
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 745202
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_6

    .line 745203
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 745204
    :goto_0
    move v2, v4

    .line 745205
    if-eqz v1, :cond_0

    .line 745206
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 745207
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 745208
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 745209
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 745210
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 745211
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 745212
    move-object v2, v1

    .line 745213
    new-instance v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;-><init>()V

    .line 745214
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 745215
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 745216
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745217
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 745218
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 745219
    :cond_1
    return-object v1

    .line 745220
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_4

    .line 745221
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 745222
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 745223
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 745224
    const-string p0, "count"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 745225
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v2

    move v7, v2

    move v2, v6

    goto :goto_1

    .line 745226
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 745227
    :cond_4
    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 745228
    if-eqz v2, :cond_5

    .line 745229
    invoke-virtual {v3, v4, v7, v4}, LX/186;->a(III)V

    .line 745230
    :cond_5
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto :goto_0

    :cond_6
    move v2, v4

    move v7, v4

    goto :goto_1
.end method
