.class public final Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752662
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752661
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752659
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752660
    return-void
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 752658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private l()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752653
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752654
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752655
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->h:D

    return-wide v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 752643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->k()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 752645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 752646
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 752647
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 752648
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 752649
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 752650
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->l()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 752651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752640
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752641
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->f:Ljava/util/List;

    .line 752642
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 752619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752620
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->k()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 752621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->k()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 752622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->k()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 752623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;

    .line 752624
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 752625
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 752626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 752627
    if-eqz v2, :cond_1

    .line 752628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;

    .line 752629
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->f:Ljava/util/List;

    move-object v1, v0

    .line 752630
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752631
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 752636
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 752637
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->g:I

    .line 752638
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->h:D

    .line 752639
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 752635
    const v0, 0x39f68851    # 4.702234E-4f

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752632
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752633
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752634
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->g:I

    return v0
.end method
