.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786411
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 786412
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 786413
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 786414
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 786415
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 786416
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 786417
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;LX/0nX;LX/0my;)V

    return-void
.end method
