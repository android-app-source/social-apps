.class public final Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748381
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748380
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 748394
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748395
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 748396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748397
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 748398
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 748399
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 748400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748401
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 748383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748384
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748385
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748386
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 748387
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;

    .line 748388
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748389
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748390
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 748382
    const v0, 0xae54c81

    return v0
.end method
