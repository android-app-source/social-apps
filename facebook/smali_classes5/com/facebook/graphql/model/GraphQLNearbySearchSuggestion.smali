.class public final Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 768039
    const-class v0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 768042
    const-class v0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 768040
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 768041
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 768004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 768005
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 768006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 768007
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 768008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 768009
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 768010
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 768011
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 768012
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 768013
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 768014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 768015
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 768016
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 768017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 768018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 768019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 768020
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    .line 768021
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 768022
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 768023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 768025
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    .line 768026
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768027
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 768028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 768030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    .line 768031
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768032
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 768033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 768034
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 768035
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    .line 768036
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768037
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 768038
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 768001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 768002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 768003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->e:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 768000
    const v0, 0x457b568b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767997
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767998
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767999
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767991
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->h:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767992
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->h:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 767993
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;->h:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method
