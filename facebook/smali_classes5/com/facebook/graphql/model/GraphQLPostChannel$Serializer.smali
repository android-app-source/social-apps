.class public final Lcom/facebook/graphql/model/GraphQLPostChannel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPostChannel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 776025
    const-class v0, Lcom/facebook/graphql/model/GraphQLPostChannel;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPostChannel$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPostChannel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 776026
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775986
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPostChannel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 775988
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 775989
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 775990
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 775991
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775992
    if-eqz v2, :cond_0

    .line 775993
    const-string v3, "blurredCoverPhoto"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775994
    invoke-static {v1, v2, p1, p2}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 775995
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 775996
    if-eqz v2, :cond_1

    .line 775997
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 775998
    invoke-static {v1, v2, p1, p2}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 775999
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 776000
    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    .line 776001
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776002
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 776003
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776004
    if-eqz v2, :cond_3

    .line 776005
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776006
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776007
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 776008
    if-eqz v2, :cond_4

    .line 776009
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776010
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776011
    :cond_4
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776012
    if-eqz v2, :cond_5

    .line 776013
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776014
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776015
    :cond_5
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 776016
    if-eqz v2, :cond_6

    .line 776017
    const-string v3, "titleForSummary"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776018
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776019
    :cond_6
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 776020
    if-eqz v2, :cond_7

    .line 776021
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 776022
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 776023
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 776024
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 775987
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPostChannel;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPostChannel$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPostChannel;LX/0nX;LX/0my;)V

    return-void
.end method
