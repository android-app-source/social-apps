.class public Lcom/facebook/graphql/model/GraphQLMapTileSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMapTile;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764821
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTile;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMapTileSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMapTileSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764822
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764839
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMapTile;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764833
    if-nez p0, :cond_0

    .line 764834
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 764835
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764836
    invoke-static {p0, p1, p2}, Lcom/facebook/graphql/model/GraphQLMapTileSerializer;->b(Lcom/facebook/graphql/model/GraphQLMapTile;LX/0nX;LX/0my;)V

    .line 764837
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764838
    return-void
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLMapTile;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 764824
    const-string v0, "tile_key"

    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->id:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 764825
    const-string v0, "creation_time"

    iget-wide v2, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->creationTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 764826
    const-string v0, "ttl"

    iget-wide v2, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->timeToLiveInSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 764827
    const-string v0, "bounds"

    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 764828
    const-string v0, "min_zoom"

    iget v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->minZoom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 764829
    const-string v0, "max_zoom"

    iget v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->maxZoom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 764830
    const-string v0, "placesRenderPriority1"

    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->places:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 764831
    const-string v0, "placesRenderPriority2"

    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->backgroundPlaces:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 764832
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764823
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMapTile;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMapTileSerializer;->a(Lcom/facebook/graphql/model/GraphQLMapTile;LX/0nX;LX/0my;)V

    return-void
.end method
