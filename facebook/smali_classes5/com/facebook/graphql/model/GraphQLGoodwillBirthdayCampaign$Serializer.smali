.class public final Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 755118
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 755119
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 755117
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 755071
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 755072
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 755073
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 755074
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755075
    if-eqz v2, :cond_0

    .line 755076
    const-string p0, "campaign_owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755077
    invoke-static {v1, v2, p1, p2}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 755078
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755079
    if-eqz v2, :cond_1

    .line 755080
    const-string p0, "confirmation_accent_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755081
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 755082
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755083
    if-eqz v2, :cond_2

    .line 755084
    const-string p0, "confirmation_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755085
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 755086
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 755087
    if-eqz v2, :cond_3

    .line 755088
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755089
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 755090
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755091
    if-eqz v2, :cond_4

    .line 755092
    const-string p0, "image_overlays"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755093
    invoke-static {v1, v2, p1, p2}, LX/4Ob;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 755094
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755095
    if-eqz v2, :cond_5

    .line 755096
    const-string p0, "media_attachments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755097
    invoke-static {v1, v2, p1, p2}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 755098
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755099
    if-eqz v2, :cond_6

    .line 755100
    const-string p0, "posting_actors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755101
    invoke-static {v1, v2, p1, p2}, LX/4NP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 755102
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755103
    if-eqz v2, :cond_7

    .line 755104
    const-string p0, "social_context"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755105
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 755106
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 755107
    if-eqz v2, :cond_8

    .line 755108
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755109
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 755110
    :cond_8
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 755111
    if-eqz v2, :cond_9

    .line 755112
    const-string p0, "video_campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 755113
    invoke-static {v1, v2, p1, p2}, LX/4Nk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 755114
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 755115
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 755116
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;->a(Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;LX/0nX;LX/0my;)V

    return-void
.end method
