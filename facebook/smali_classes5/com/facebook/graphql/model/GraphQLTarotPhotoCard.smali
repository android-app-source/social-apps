.class public final Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786935
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786934
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 786932
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 786933
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->e:Ljava/lang/String;

    .line 786931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->f:Ljava/lang/String;

    .line 786928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786923
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786924
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 786925
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786936
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786937
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 786938
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786920
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786921
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->i:Ljava/lang/String;

    .line 786922
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786917
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786918
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 786919
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786914
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786915
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->k:Ljava/lang/String;

    .line 786916
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 786896
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786897
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 786898
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 786899
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 786900
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 786901
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 786902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->o()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 786903
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 786904
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 786905
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 786906
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 786907
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 786908
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 786909
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 786910
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 786911
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 786912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786913
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 786878
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786879
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 786880
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 786881
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 786882
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    .line 786883
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 786884
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 786885
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 786886
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 786887
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    .line 786888
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 786889
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->o()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 786890
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->o()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 786891
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->o()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 786892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    .line 786893
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->j:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 786894
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786895
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786877
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 786876
    const v0, 0x6ed6eb58

    return v0
.end method
