.class public final Lcom/facebook/graphql/model/GraphQLFrameTextAsset;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameTextAsset$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameTextAsset$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:D

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752172
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752171
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752173
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752174
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752175
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752176
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->h:Ljava/lang/String;

    .line 752177
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->h:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752178
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752179
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p:Ljava/lang/String;

    .line 752180
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 752181
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 752183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 752184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 752185
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 752186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 752187
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 752188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 752189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 752190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 752191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 752192
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->u()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 752193
    const/16 v11, 0xc

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 752194
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 752195
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 752196
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 752197
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 752198
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 752199
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 752200
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 752201
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 752202
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 752203
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 752204
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 752205
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 752206
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752207
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 752208
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 752210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 752211
    if-eqz v1, :cond_6

    .line 752212
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752213
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->e:Ljava/util/List;

    move-object v1, v0

    .line 752214
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 752215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    .line 752216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 752217
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752218
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    .line 752219
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 752220
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 752222
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752223
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752224
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 752225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 752227
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752228
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752229
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 752230
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 752232
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752233
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752234
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 752235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752236
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 752237
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    .line 752238
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752239
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752240
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752241
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 752242
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 752243
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k:D

    .line 752244
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 752143
    const v0, -0x1e65890a

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->e:Ljava/util/List;

    .line 752247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    .line 752170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->f:Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->g:Ljava/lang/String;

    .line 752167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->i:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    .line 752161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j:Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    return-object v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752156
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752157
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752158
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k:D

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l:Ljava/lang/String;

    .line 752155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m:Ljava/lang/String;

    .line 752152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752145
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    .line 752146
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o:Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    return-object v0
.end method
