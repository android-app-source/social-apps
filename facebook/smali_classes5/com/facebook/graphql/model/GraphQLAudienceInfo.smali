.class public final Lcom/facebook/graphql/model/GraphQLAudienceInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAudienceInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAudienceInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634762
    const-class v0, Lcom/facebook/graphql/model/GraphQLAudienceInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634761
    const-class v0, Lcom/facebook/graphql/model/GraphQLAudienceInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634759
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634760
    return-void
.end method

.method public constructor <init>(LX/3lc;)V
    .locals 1

    .prologue
    .line 634752
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634753
    iget-object v0, p1, LX/3lc;->b:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634754
    iget-boolean v0, p1, LX/3lc;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->f:Z

    .line 634755
    iget-boolean v0, p1, LX/3lc;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->g:Z

    .line 634756
    iget-boolean v0, p1, LX/3lc;->e:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->h:Z

    .line 634757
    iget-boolean v0, p1, LX/3lc;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->i:Z

    .line 634758
    return-void
.end method

.method private j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634749
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634750
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634751
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->f:Z

    return v0
.end method

.method private k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634746
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634747
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634748
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->g:Z

    return v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634712
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634713
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634714
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->h:Z

    return v0
.end method

.method private m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634743
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634744
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634745
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->i:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 634733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 634735
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 634736
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 634737
    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->j()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 634738
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->k()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 634739
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 634740
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 634741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634742
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 634725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634727
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 634729
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 634730
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634731
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634732
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634722
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 634723
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634724
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->e:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 634716
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 634717
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->f:Z

    .line 634718
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->g:Z

    .line 634719
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->h:Z

    .line 634720
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->i:Z

    .line 634721
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 634715
    const v0, -0x5d378b0e

    return v0
.end method
