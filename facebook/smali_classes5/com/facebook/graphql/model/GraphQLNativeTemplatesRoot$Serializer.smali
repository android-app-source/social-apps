.class public final Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767821
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 767822
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767820
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 767811
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 767812
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 767813
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 767814
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767815
    if-eqz v2, :cond_0

    .line 767816
    const-string p0, "native_template_bundles"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767817
    invoke-static {v1, v2, p1, p2}, LX/4Pk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 767818
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 767819
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 767810
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;LX/0nX;LX/0my;)V

    return-void
.end method
