.class public final Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 771465
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 771466
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 771467
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 771468
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 771469
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 771470
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 771471
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 771472
    if-eqz p0, :cond_0

    .line 771473
    const-string p2, "error"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 771474
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 771475
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 771476
    if-eqz p0, :cond_1

    .line 771477
    const-string p2, "input_name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 771478
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 771479
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 771480
    if-eqz p0, :cond_2

    .line 771481
    const-string p2, "suggested_name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 771482
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 771483
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 771484
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 771485
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;LX/0nX;LX/0my;)V

    return-void
.end method
