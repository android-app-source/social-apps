.class public final Lcom/facebook/graphql/model/GraphQLMaskEffect;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMaskEffect$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMaskEffect$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLNativeMask;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765065
    const-class v0, Lcom/facebook/graphql/model/GraphQLMaskEffect$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765071
    const-class v0, Lcom/facebook/graphql/model/GraphQLMaskEffect$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765069
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 765070
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765066
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765067
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->i:Ljava/lang/String;

    .line 765068
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->i:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765062
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765063
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l:Ljava/lang/String;

    .line 765064
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765059
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765060
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765061
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765056
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765057
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n:Ljava/lang/String;

    .line 765058
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 765034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 765036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 765037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l()Lcom/facebook/graphql/model/GraphQLNativeMask;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 765038
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 765039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 765040
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 765041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 765042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->s()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 765043
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 765044
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 765045
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 765046
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 765047
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 765048
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 765049
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 765050
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 765051
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 765052
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 765053
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 765054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765055
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 765072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 765074
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 765075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 765076
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 765077
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 765078
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 765079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 765080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 765081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 765082
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765083
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l()Lcom/facebook/graphql/model/GraphQLNativeMask;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 765084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l()Lcom/facebook/graphql/model/GraphQLNativeMask;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeMask;

    .line 765085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l()Lcom/facebook/graphql/model/GraphQLNativeMask;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 765086
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 765087
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;->g:Lcom/facebook/graphql/model/GraphQLNativeMask;

    .line 765088
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 765089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 765090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 765091
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 765092
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 765093
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 765094
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 765095
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 765096
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 765097
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765098
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765099
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765033
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 765029
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 765030
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->h:Z

    .line 765031
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k:Z

    .line 765032
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 765028
    const v0, -0x160d5843

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765025
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765026
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 765027
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765022
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765023
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765024
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLNativeMask;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765010
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->g:Lcom/facebook/graphql/model/GraphQLNativeMask;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765011
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->g:Lcom/facebook/graphql/model/GraphQLNativeMask;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLNativeMask;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeMask;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->g:Lcom/facebook/graphql/model/GraphQLNativeMask;

    .line 765012
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->g:Lcom/facebook/graphql/model/GraphQLNativeMask;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 765019
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 765020
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 765021
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->h:Z

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765016
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765017
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 765018
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    return-object v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 765013
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 765014
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 765015
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k:Z

    return v0
.end method
