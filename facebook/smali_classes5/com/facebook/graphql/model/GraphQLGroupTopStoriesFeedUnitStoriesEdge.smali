.class public final Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760643
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760642
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 760639
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 760640
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    .line 760641
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 760636
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    if-nez v0, :cond_0

    .line 760637
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    .line 760638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 760630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 760632
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 760633
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 760634
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760635
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 760622
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 760624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 760625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 760626
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;

    .line 760627
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 760628
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760629
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 760621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 760618
    const v0, -0x37ce192f

    return v0
.end method
