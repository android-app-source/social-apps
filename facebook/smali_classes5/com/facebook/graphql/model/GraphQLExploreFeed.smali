.class public final Lcom/facebook/graphql/model/GraphQLExploreFeed;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLExploreFeed$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLExploreFeed$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Z

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;",
            ">;"
        }
    .end annotation
.end field

.field public F:Z

.field public G:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public H:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:I

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 750687
    const-class v0, Lcom/facebook/graphql/model/GraphQLExploreFeed$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 750694
    const-class v0, Lcom/facebook/graphql/model/GraphQLExploreFeed$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 750695
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 750696
    return-void
.end method

.method private A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750697
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750698
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750699
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u:Z

    return v0
.end method

.method private B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 750700
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750701
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750702
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->v:Z

    return v0
.end method

.method private C()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750703
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750704
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750705
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->w:Z

    return v0
.end method

.method private D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750706
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750707
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750708
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->x:Z

    return v0
.end method

.method private E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750709
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750710
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750711
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->y:I

    return v0
.end method

.method private F()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750712
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750713
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750714
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z:I

    return v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750715
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750716
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750717
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750718
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750719
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750720
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private I()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750757
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750758
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750759
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->D:Z

    return v0
.end method

.method private J()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 750721
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750722
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E:Ljava/util/List;

    .line 750723
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private K()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750724
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750725
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750726
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->F:Z

    return v0
.end method

.method private L()Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750727
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750728
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 750729
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750730
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750731
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H:Lcom/facebook/graphql/model/GraphQLNode;

    .line 750732
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 750733
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k:Z

    .line 750734
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 750735
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 750736
    if-eqz v0, :cond_0

    .line 750737
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 750738
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 750739
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->x:Z

    .line 750740
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 750741
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 750742
    if-eqz v0, :cond_0

    .line 750743
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 750744
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 750745
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->D:Z

    .line 750746
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 750747
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 750748
    if-eqz v0, :cond_0

    .line 750749
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 750750
    :cond_0
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->e:Ljava/lang/String;

    .line 750753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->e:Ljava/lang/String;

    return-object v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750754
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750755
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750756
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->f:Z

    return v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750688
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750689
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750690
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750691
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750692
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750693
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750581
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750582
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750583
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750578
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750579
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750580
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k:Z

    return v0
.end method

.method private s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750575
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750576
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750577
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l:I

    return v0
.end method

.method private t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750572
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750573
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750574
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->n:Z

    return v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750569
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750570
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750571
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750566
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750567
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p:Ljava/lang/String;

    .line 750568
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p:Ljava/lang/String;

    return-object v0
.end method

.method private w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750563
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750564
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750565
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q:Z

    return v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750560
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750561
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750562
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->r:Z

    return v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750557
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750558
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750559
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->s:Z

    return v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750554
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750555
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 750556
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 750505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 750506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->m()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 750507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 750508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 750509
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 750510
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 750511
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 750512
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 750513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->v()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 750514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 750515
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 750516
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 750517
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 750518
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->J()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 750519
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->M()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 750520
    const/16 v15, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 750521
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 750522
    const/4 v1, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->n()Z

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, LX/186;->a(IZ)V

    .line 750523
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 750524
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 750525
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 750526
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 750527
    const/4 v1, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->r()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750528
    const/16 v1, 0x8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->s()I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 750529
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 750530
    const/16 v1, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750531
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 750532
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 750533
    const/16 v1, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->w()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750534
    const/16 v1, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->x()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750535
    const/16 v1, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->y()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750536
    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 750537
    const/16 v1, 0x11

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750538
    const/16 v1, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750539
    const/16 v1, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750540
    const/16 v1, 0x14

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->D()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750541
    const/16 v1, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E()I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 750542
    const/16 v1, 0x16

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->F()I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 750543
    const/16 v1, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 750544
    const/16 v1, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 750545
    const/16 v1, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 750546
    const/16 v1, 0x1a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->I()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750547
    const/16 v1, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 750548
    const/16 v1, 0x1c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->K()Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 750549
    const/16 v2, 0x1d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->L()Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 750550
    const/16 v1, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 750551
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 750552
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1

    .line 750553
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->L()Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 750584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 750585
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 750586
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750587
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 750588
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750589
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750590
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->J()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 750591
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->J()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 750592
    if-eqz v2, :cond_1

    .line 750593
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750594
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->E:Ljava/util/List;

    move-object v1, v0

    .line 750595
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 750596
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 750597
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 750598
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750599
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750600
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 750601
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 750602
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 750603
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750604
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750605
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 750606
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 750607
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 750608
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750609
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750610
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 750611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 750612
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 750613
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750614
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 750615
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->M()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 750616
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->M()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 750617
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->M()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 750618
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750619
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->H:Lcom/facebook/graphql/model/GraphQLNode;

    .line 750620
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 750621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 750622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 750623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750624
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->t:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 750625
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 750626
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750627
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 750628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750629
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750630
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 750631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 750633
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 750634
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750635
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 750636
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 750637
    new-instance v0, LX/4WF;

    invoke-direct {v0, p1}, LX/4WF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 750639
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 750640
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->f:Z

    .line 750641
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k:Z

    .line 750642
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l:I

    .line 750643
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->n:Z

    .line 750644
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->q:Z

    .line 750645
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->r:Z

    .line 750646
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->s:Z

    .line 750647
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->u:Z

    .line 750648
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->v:Z

    .line 750649
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->w:Z

    .line 750650
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->x:Z

    .line 750651
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->y:I

    .line 750652
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->z:I

    .line 750653
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->D:Z

    .line 750654
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->F:Z

    .line 750655
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 750656
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750657
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 750658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 750659
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    .line 750660
    :goto_0
    return-void

    .line 750661
    :cond_0
    const-string v0, "show_audience_header"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 750662
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->I()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 750663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 750664
    const/16 v0, 0x1a

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 750665
    :cond_1
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750666
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->D()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 750667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 750668
    const/16 v0, 0x14

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 750669
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 750670
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 750671
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->a(Z)V

    .line 750672
    :cond_0
    :goto_0
    return-void

    .line 750673
    :cond_1
    const-string v0, "show_audience_header"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750674
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->c(Z)V

    goto :goto_0

    .line 750675
    :cond_2
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750676
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->b(Z)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 750677
    const v0, 0xd0d7ab1

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j:Ljava/lang/String;

    .line 750680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750681
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750682
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->m:Ljava/lang/String;

    .line 750683
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750684
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750685
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 750686
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExploreFeed;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
