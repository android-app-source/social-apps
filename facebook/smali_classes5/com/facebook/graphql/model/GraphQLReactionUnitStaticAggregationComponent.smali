.class public final Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 780937
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 780938
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 780939
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 780940
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780944
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780945
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->e:Ljava/lang/String;

    .line 780946
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780941
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780942
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 780943
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method private m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780947
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780948
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780949
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->h:Z

    return v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780931
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780932
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780933
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->i:Z

    return v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780934
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780935
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780936
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j:Z

    return v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 780911
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 780912
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 780913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 780914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 780915
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->p()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 780916
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 780917
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 780918
    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v4, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 780919
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 780920
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 780921
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 780922
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 780923
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 780924
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 780925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 780926
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 780927
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 780898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 780899
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->p()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 780900
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->p()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780901
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->p()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 780902
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;

    .line 780903
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->l:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780904
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 780905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780906
    if-eqz v2, :cond_1

    .line 780907
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;

    .line 780908
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k:Ljava/util/List;

    move-object v1, v0

    .line 780909
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 780910
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780895
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780896
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->g:Ljava/lang/String;

    .line 780897
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 780890
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 780891
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->h:Z

    .line 780892
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->i:Z

    .line 780893
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j:Z

    .line 780894
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 780889
    const v0, -0x56610eea

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780886
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780887
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k:Ljava/util/List;

    .line 780888
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
