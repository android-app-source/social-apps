.class public final Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744673
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744672
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 744670
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 744671
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744667
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744668
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 744669
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 744666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744661
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744662
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744663
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744617
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->h:Ljava/lang/String;

    .line 744619
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744658
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744659
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->i:Ljava/lang/String;

    .line 744660
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 744655
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744656
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 744657
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 744639
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744640
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 744641
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 744642
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 744643
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 744644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 744645
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 744646
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 744647
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 744648
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 744649
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 744650
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 744651
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->n()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 744652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 744654
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->n()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 744621
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 744623
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 744624
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 744625
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;

    .line 744626
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 744627
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 744628
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 744629
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->j()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 744630
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;

    .line 744631
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 744632
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 744633
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744634
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 744635
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;

    .line 744636
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744637
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744638
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 744620
    const v0, 0x1172f9ae

    return v0
.end method
