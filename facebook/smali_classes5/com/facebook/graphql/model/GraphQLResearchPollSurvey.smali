.class public final Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollSurvey$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollSurvey$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;",
            ">;"
        }
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781569
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781568
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781566
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 781567
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j:Ljava/lang/String;

    .line 781565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 781546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 781548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 781549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 781550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 781551
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 781552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 781553
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 781554
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 781555
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 781556
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 781557
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 781558
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->n()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 781559
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 781560
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 781561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 781523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 781525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 781526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 781527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 781528
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 781529
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 781530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 781532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 781533
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781534
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 781535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 781536
    if-eqz v2, :cond_2

    .line 781537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 781538
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->h:Ljava/util/List;

    move-object v1, v0

    .line 781539
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 781540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    .line 781541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 781542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 781543
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    .line 781544
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781545
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 781520
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 781521
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->i:J

    .line 781522
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 781519
    const v0, 0x2cdc7414

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->e:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->e:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 781518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->e:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->f:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781510
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781511
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->g:Ljava/lang/String;

    .line 781512
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 781507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->h:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->h:Ljava/util/List;

    .line 781509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 781504
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 781505
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 781506
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->i:J

    return-wide v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    .line 781503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k:Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    return-object v0
.end method
