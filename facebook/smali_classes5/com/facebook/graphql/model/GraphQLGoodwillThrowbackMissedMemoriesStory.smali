.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jW;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:J

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 757142
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 757157
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 757153
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 757154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x270c0443

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 757155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k:LX/0x2;

    .line 757156
    return-void
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g:Ljava/util/List;

    .line 757152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->i:Ljava/lang/String;

    .line 757149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 757144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757145
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 757146
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 757143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 757141
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 757158
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 757159
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 757160
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->f:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 757138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k:LX/0x2;

    if-nez v0, :cond_0

    .line 757139
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k:LX/0x2;

    .line 757140
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 757125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 757126
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->l()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 757127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 757128
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 757129
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 757130
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 757131
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 757132
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 757133
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 757134
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 757135
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 757136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 757137
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 757091
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 757092
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 757093
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 757094
    if-eqz v1, :cond_0

    .line 757095
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    .line 757096
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g:Ljava/util/List;

    .line 757097
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 757098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 757099
    if-eqz v1, :cond_1

    .line 757100
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    .line 757101
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->h:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 757102
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 757103
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 757104
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 757105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;

    .line 757106
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 757107
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 757108
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 757123
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->f:J

    .line 757124
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 757120
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 757121
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->f:J

    .line 757122
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 757116
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 757117
    :goto_0
    return-object v0

    .line 757118
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 757119
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 757114
    const v0, -0x270c0443

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 757112
    const/4 v0, 0x0

    move-object v0, v0

    .line 757113
    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757109
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 757110
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->h:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->h:Ljava/util/List;

    .line 757111
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackMissedMemoriesStory;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
