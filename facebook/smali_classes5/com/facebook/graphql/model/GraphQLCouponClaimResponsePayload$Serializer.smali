.class public final Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 746892
    const-class v0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 746893
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746894
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 746879
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 746880
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 746881
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 746882
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(IIS)S

    move-result v2

    .line 746883
    if-eqz v2, :cond_0

    .line 746884
    const-string v2, "claim_result"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 746885
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 746886
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 746887
    if-eqz v2, :cond_1

    .line 746888
    const-string p0, "claim_result_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 746889
    invoke-static {v1, v2, p1, p2}, LX/4L4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 746890
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 746891
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 746878
    check-cast p1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;->a(Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;LX/0nX;LX/0my;)V

    return-void
.end method
