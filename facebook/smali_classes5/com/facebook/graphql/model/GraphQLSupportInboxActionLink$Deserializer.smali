.class public final Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 785937
    const-class v0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 785938
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 785936
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 785890
    const/16 v0, 0x258

    .line 785891
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 785892
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 785893
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 785894
    const/4 v2, 0x0

    .line 785895
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 785896
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 785897
    :goto_0
    move v2, v4

    .line 785898
    if-eqz v1, :cond_0

    .line 785899
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 785900
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 785901
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 785902
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 785903
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 785904
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 785905
    move-object v2, v1

    .line 785906
    new-instance v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;-><init>()V

    .line 785907
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 785908
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 785909
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 785910
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 785911
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 785912
    :cond_1
    return-object v1

    .line 785913
    :cond_2
    const-string p0, "link_type"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 785914
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    move-object v7, v2

    move v2, v6

    .line 785915
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_8

    .line 785916
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 785917
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 785918
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 785919
    const-string p0, "item"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 785920
    invoke-static {p1, v3}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 785921
    :cond_4
    const-string p0, "support_item"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 785922
    invoke-static {p1, v3}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 785923
    :cond_5
    const-string p0, "title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 785924
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 785925
    :cond_6
    const-string p0, "url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 785926
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 785927
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 785928
    :cond_8
    const/4 v12, 0x5

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 785929
    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 785930
    invoke-virtual {v3, v6, v10}, LX/186;->b(II)V

    .line 785931
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 785932
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 785933
    if-eqz v2, :cond_9

    .line 785934
    const/4 v2, 0x4

    invoke-virtual {v3, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785935
    :cond_9
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_a
    move-object v7, v2

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v4

    move v2, v4

    goto :goto_1
.end method
