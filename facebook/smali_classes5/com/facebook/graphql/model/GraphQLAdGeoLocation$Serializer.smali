.class public final Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAdGeoLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 743707
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 743708
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 743706
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLAdGeoLocation;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 743648
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 743649
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v8, 0x6

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 743650
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 743651
    invoke-virtual {v1, v0, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743652
    if-eqz v2, :cond_0

    .line 743653
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743654
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743655
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743656
    if-eqz v2, :cond_1

    .line 743657
    const-string v3, "country_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743658
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743659
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743660
    if-eqz v2, :cond_2

    .line 743661
    const-string v3, "display_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743662
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743663
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743664
    if-eqz v2, :cond_3

    .line 743665
    const-string v3, "distance_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743666
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743667
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743668
    if-eqz v2, :cond_4

    .line 743669
    const-string v3, "key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743670
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743671
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 743672
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_5

    .line 743673
    const-string v4, "latitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743674
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 743675
    :cond_5
    invoke-virtual {v1, v0, v8, v5}, LX/15i;->a(IIS)S

    move-result v2

    .line 743676
    if-eqz v2, :cond_6

    .line 743677
    const-string v2, "location_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743678
    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v1, v0, v8, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743679
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 743680
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_7

    .line 743681
    const-string v4, "longitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743682
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 743683
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743684
    if-eqz v2, :cond_8

    .line 743685
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743686
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743687
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 743688
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_9

    .line 743689
    const-string v4, "radius"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743690
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 743691
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743692
    if-eqz v2, :cond_a

    .line 743693
    const-string v3, "region_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743694
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743695
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 743696
    if-eqz v2, :cond_b

    .line 743697
    const-string v3, "supports_city"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743698
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 743699
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 743700
    if-eqz v2, :cond_c

    .line 743701
    const-string v3, "supports_region"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743702
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 743703
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 743704
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 743705
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;->a(Lcom/facebook/graphql/model/GraphQLAdGeoLocation;LX/0nX;LX/0my;)V

    return-void
.end method
