.class public final Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785785
    const-class v0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785786
    const-class v0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 785783
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 785784
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 785765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 785767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 785768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 785769
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 785770
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 785771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 785772
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 785773
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 785774
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 785775
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    if-ne v0, v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785776
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 785777
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 785778
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 785779
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 785780
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785781
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 785782
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 785752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 785754
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 785755
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 785756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;

    .line 785757
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 785758
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785759
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 785761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;

    .line 785762
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785763
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785764
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785749
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785750
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->e:Ljava/lang/String;

    .line 785751
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 785787
    const v0, -0x4b647577

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785746
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785747
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 785748
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785743
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->g:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785744
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->g:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->g:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 785745
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->g:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785740
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785741
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->h:Ljava/lang/String;

    .line 785742
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785737
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785738
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->i:Ljava/lang/String;

    .line 785739
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785731
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785732
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j:Ljava/lang/String;

    .line 785733
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785734
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785735
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785736
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
