.class public final Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744785
    const-class v0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 744786
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744787
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 744788
    const/16 v0, 0x275

    .line 744789
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 744790
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 744791
    const/4 v4, 0x0

    .line 744792
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_6

    .line 744793
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 744794
    :goto_0
    move v2, v4

    .line 744795
    if-eqz v1, :cond_0

    .line 744796
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 744797
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 744798
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 744799
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 744800
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 744801
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 744802
    move-object v2, v1

    .line 744803
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;-><init>()V

    .line 744804
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 744805
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 744806
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 744807
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 744808
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 744809
    :cond_1
    return-object v1

    .line 744810
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 744811
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 744812
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 744813
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 744814
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v7, :cond_3

    .line 744815
    const-string p0, "client_mutation_id"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 744816
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 744817
    :cond_4
    const-string p0, "story"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 744818
    invoke-static {p1, v3}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 744819
    :cond_5
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 744820
    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 744821
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 744822
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_6
    move v2, v4

    move v6, v4

    goto :goto_1
.end method
