.class public final Lcom/facebook/graphql/model/GraphQLNearbySearchQuery$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNearbySearchQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767883
    const-class v0, Lcom/facebook/graphql/model/GraphQLNearbySearchQuery;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNearbySearchQuery$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNearbySearchQuery$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 767884
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767886
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNearbySearchQuery;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 767887
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 767888
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 767889
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 767890
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 767891
    if-eqz p0, :cond_0

    .line 767892
    const-string p0, "query_suggestions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767893
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 767894
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 767895
    if-eqz p0, :cond_1

    .line 767896
    const-string p2, "search_session_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767897
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 767898
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 767899
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 767885
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNearbySearchQuery;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNearbySearchQuery$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNearbySearchQuery;LX/0nX;LX/0my;)V

    return-void
.end method
