.class public final Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 745231
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 745232
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 745233
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 745234
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 745235
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 745236
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 745237
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 745238
    if-eqz p0, :cond_0

    .line 745239
    const-string p2, "count"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 745240
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 745241
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 745242
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 745243
    check-cast p1, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLBoostedComponentAudienceAdCampaignsConnection;LX/0nX;LX/0my;)V

    return-void
.end method
