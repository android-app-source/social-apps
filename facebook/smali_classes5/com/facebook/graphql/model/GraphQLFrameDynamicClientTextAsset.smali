.class public final Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751857
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751880
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 751878
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 751879
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 751875
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 751876
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 751877
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 751868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751869
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 751870
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 751871
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->j()Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 751872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751873
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 751874
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->j()Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 751865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751867
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 751862
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 751863
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->e:I

    .line 751864
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 751861
    const v0, -0x2218265a

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->f:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->f:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->f:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    .line 751860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->f:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    return-object v0
.end method
