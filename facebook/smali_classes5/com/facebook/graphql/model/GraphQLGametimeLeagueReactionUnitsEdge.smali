.class public final Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLReactionUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754705
    const-class v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754704
    const-class v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 754702
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 754703
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLReactionUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754706
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->e:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754707
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->e:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->e:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 754708
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->e:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 754696
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754697
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 754698
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 754699
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 754700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754701
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 754688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754689
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754690
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 754691
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 754692
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;

    .line 754693
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;->e:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 754694
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754695
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 754687
    const v0, 0xda2b255

    return v0
.end method
