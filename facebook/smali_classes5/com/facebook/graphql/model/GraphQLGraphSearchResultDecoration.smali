.class public final Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758775
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758774
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758772
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758773
    return-void
.end method

.method public constructor <init>(LX/4Wh;)V
    .locals 1

    .prologue
    .line 758757
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758758
    iget-object v0, p1, LX/4Wh;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o:Ljava/lang/String;

    .line 758759
    iget-object v0, p1, LX/4Wh;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p:Ljava/lang/String;

    .line 758760
    iget-object v0, p1, LX/4Wh;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    .line 758761
    iget-object v0, p1, LX/4Wh;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 758762
    iget-object v0, p1, LX/4Wh;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    .line 758763
    iget-object v0, p1, LX/4Wh;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    .line 758764
    iget-object v0, p1, LX/4Wh;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    .line 758765
    iget-object v0, p1, LX/4Wh;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->i:Ljava/util/List;

    .line 758766
    iget-object v0, p1, LX/4Wh;->j:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    .line 758767
    iget-boolean v0, p1, LX/4Wh;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n:Z

    .line 758768
    iget-object v0, p1, LX/4Wh;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k:Ljava/lang/String;

    .line 758769
    iget-object v0, p1, LX/4Wh;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758770
    iget-object v0, p1, LX/4Wh;->n:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758771
    return-void
.end method

.method private o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758642
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758643
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    .line 758644
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758754
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758755
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    .line 758756
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758748
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758749
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758750
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    return-object v0
.end method

.method private s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 758745
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 758746
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 758747
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n:Z

    return v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o:Ljava/lang/String;

    .line 758744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p:Ljava/lang/String;

    .line 758741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 758710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758711
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 758712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 758713
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 758714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 758715
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 758716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 758717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 758718
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 758719
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 758720
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 758721
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->u()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 758722
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 758723
    const/16 v12, 0xd

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 758724
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 758725
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 758726
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 758727
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 758728
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 758729
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 758730
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 758731
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 758732
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 758733
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->s()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 758734
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 758735
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 758736
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 758737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 758667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758668
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 758669
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->o()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 758670
    if-eqz v1, :cond_8

    .line 758671
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758672
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->e:Ljava/util/List;

    move-object v1, v0

    .line 758673
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 758675
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 758676
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758677
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 758678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 758679
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758680
    if-eqz v2, :cond_1

    .line 758681
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758682
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    move-object v1, v0

    .line 758683
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 758684
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758685
    if-eqz v2, :cond_2

    .line 758686
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758687
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->g:Ljava/util/List;

    move-object v1, v0

    .line 758688
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 758689
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758690
    if-eqz v2, :cond_3

    .line 758691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758692
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    move-object v1, v0

    .line 758693
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 758694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 758695
    if-eqz v2, :cond_4

    .line 758696
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758697
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    move-object v1, v0

    .line 758698
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 758699
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758700
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 758701
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758702
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758703
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 758704
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758705
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 758706
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758707
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 758708
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758709
    if-nez v1, :cond_7

    :goto_1
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 758666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 758661
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 758662
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n:Z

    .line 758663
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 758660
    const v0, 0x16973d43

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758657
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758658
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    .line 758659
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758654
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758655
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->i:Ljava/util/List;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->i:Ljava/util/List;

    .line 758656
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758651
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758652
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    .line 758653
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758648
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758649
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k:Ljava/lang/String;

    .line 758650
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758645
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758646
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    .line 758647
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
