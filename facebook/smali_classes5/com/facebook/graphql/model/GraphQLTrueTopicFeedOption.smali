.class public final Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788796
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788797
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 788798
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 788799
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 788809
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->g:Z

    .line 788810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 788811
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 788812
    if-eqz v0, :cond_0

    .line 788813
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 788814
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788800
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788801
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->e:Ljava/lang/String;

    .line 788802
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788803
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788804
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->f:Ljava/lang/String;

    .line 788805
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 788806
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 788807
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 788808
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->g:Z

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788790
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788791
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788792
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTopicFeedOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    .line 788795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788744
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788745
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->j:Ljava/lang/String;

    .line 788746
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 788747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788748
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 788749
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 788750
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 788751
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->n()Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 788752
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 788753
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 788754
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 788755
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 788756
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 788757
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 788758
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 788759
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 788760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788761
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 788762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788763
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 788764
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 788765
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 788766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;

    .line 788767
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788768
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->n()Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 788769
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->n()Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    .line 788770
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->n()Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 788771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;

    .line 788772
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->i:Lcom/facebook/graphql/model/GraphQLTopicFeedOption;

    .line 788773
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788774
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 788775
    new-instance v0, LX/4ZK;

    invoke-direct {v0, p1}, LX/4ZK;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 788777
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 788778
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->g:Z

    .line 788779
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 788780
    const-string v0, "is_checked"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788781
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 788782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 788783
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 788784
    :goto_0
    return-void

    .line 788785
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 788787
    const-string v0, "is_checked"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788788
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;->a(Z)V

    .line 788789
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 788786
    const v0, -0x201e1d4c

    return v0
.end method
