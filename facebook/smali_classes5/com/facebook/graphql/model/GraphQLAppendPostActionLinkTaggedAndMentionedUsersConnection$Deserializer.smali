.class public final Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744723
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 744724
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744703
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 744704
    const/16 v0, 0xa9

    .line 744705
    const/4 v1, 0x1

    const/4 p2, 0x0

    .line 744706
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 744707
    invoke-static {p1, v3}, LX/4Kt;->a(LX/15w;LX/186;)I

    move-result v2

    .line 744708
    if-eqz v1, :cond_0

    .line 744709
    const/4 p0, 0x2

    invoke-virtual {v3, p0}, LX/186;->c(I)V

    .line 744710
    invoke-virtual {v3, p2, v0, p2}, LX/186;->a(ISI)V

    .line 744711
    const/4 p0, 0x1

    invoke-virtual {v3, p0, v2}, LX/186;->b(II)V

    .line 744712
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 744713
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 744714
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 744715
    move-object v2, v1

    .line 744716
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;-><init>()V

    .line 744717
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 744718
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 744719
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 744720
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 744721
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 744722
    :cond_1
    return-object v1
.end method
