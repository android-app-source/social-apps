.class public final Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766334
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 766335
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766344
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 766336
    const/16 v0, 0x281

    invoke-static {p1, v0}, LX/4PY;->a(LX/15w;S)LX/15i;

    move-result-object v2

    .line 766337
    new-instance v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;-><init>()V

    .line 766338
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 766339
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 766340
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 766341
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 766342
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 766343
    :cond_0
    return-object v1
.end method
