.class public final Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 750902
    const-class v0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 750901
    const-class v0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 750899
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 750900
    return-void
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 750896
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 750897
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 750898
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->e:J

    return-wide v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750893
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750894
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->f:Ljava/lang/String;

    .line 750895
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 750872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->g:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->g:Ljava/util/List;

    .line 750874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750890
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750891
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->h:Ljava/lang/String;

    .line 750892
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->i:Ljava/lang/String;

    .line 750889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 750886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750903
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750904
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->k:Ljava/lang/String;

    .line 750905
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750881
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750882
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->l:Ljava/lang/String;

    .line 750883
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750878
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750879
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m:Lcom/facebook/graphql/model/GraphQLActor;

    .line 750880
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750875
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 750876
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->n:Ljava/lang/String;

    .line 750877
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 750849
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 750850
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 750851
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->l()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 750852
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 750853
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 750854
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->o()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 750855
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 750856
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->q()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 750857
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->r()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 750858
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->s()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 750859
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 750860
    const/4 v3, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 750861
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 750862
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 750863
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 750864
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 750865
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 750866
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 750867
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 750868
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 750869
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 750870
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 750871
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 750836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 750837
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->o()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 750838
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->o()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 750839
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->o()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 750840
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;

    .line 750841
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->j:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 750842
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->r()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 750843
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->r()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 750844
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->r()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 750845
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;

    .line 750846
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->m:Lcom/facebook/graphql/model/GraphQLActor;

    .line 750847
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 750848
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 750835
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 750832
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 750833
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLExternalMusicAlbum;->e:J

    .line 750834
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 750831
    const v0, -0x78df566b

    return v0
.end method
