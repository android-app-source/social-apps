.class public final Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMisinformationAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766584
    const-class v0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766581
    const-class v0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 766582
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 766583
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766594
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766595
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->e:Ljava/lang/String;

    .line 766596
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766585
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766586
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->f:Ljava/lang/String;

    .line 766587
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766588
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766589
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->g:Ljava/lang/String;

    .line 766590
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766591
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766592
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->h:Ljava/lang/String;

    .line 766593
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766575
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766576
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 766577
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766578
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766579
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->j:Ljava/lang/String;

    .line 766580
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->k:Ljava/lang/String;

    .line 766574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766569
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766570
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->l:Ljava/lang/String;

    .line 766571
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766566
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766567
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m:Ljava/lang/String;

    .line 766568
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->n:Ljava/lang/String;

    .line 766565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMisinformationAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766560
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766561
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLMisinformationAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o:Ljava/util/List;

    .line 766562
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 766525
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 766527
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 766528
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 766529
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 766530
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 766531
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 766532
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 766533
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 766534
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 766535
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->s()LX/0Px;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 766536
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 766537
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 766538
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 766539
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 766540
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 766541
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 766542
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 766543
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 766544
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 766545
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 766546
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 766547
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 766548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766549
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 766550
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 766552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766553
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->s()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 766554
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->s()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 766555
    if-eqz v1, :cond_0

    .line 766556
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;

    .line 766557
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;->o:Ljava/util/List;

    .line 766558
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766559
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 766551
    const v0, -0x67292209

    return v0
.end method
