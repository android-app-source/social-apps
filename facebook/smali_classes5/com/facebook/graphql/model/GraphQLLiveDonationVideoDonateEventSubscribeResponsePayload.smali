.class public final Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764321
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764320
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764318
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764319
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764280
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764281
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764282
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764315
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764316
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    .line 764317
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764312
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764313
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 764314
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 764302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764303
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 764304
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 764305
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->k()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 764306
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 764307
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 764308
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 764309
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 764310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764311
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764284
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764285
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 764286
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    .line 764287
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 764288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    .line 764289
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLDonationForFundraiser;

    .line 764290
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->k()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 764291
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->k()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 764292
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->k()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 764293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    .line 764294
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->g:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 764295
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 764296
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764297
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 764298
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    .line 764299
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764300
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764301
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764283
    const v0, -0x340f03af    # -3.1586466E7f

    return v0
.end method
