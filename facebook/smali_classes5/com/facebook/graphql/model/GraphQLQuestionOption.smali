.class public final Lcom/facebook/graphql/model/GraphQLQuestionOption;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLQuestionOption$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLQuestionOption$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777374
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuestionOption$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777375
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuestionOption$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 777376
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 777377
    return-void
.end method

.method public constructor <init>(LX/4YQ;)V
    .locals 1

    .prologue
    .line 777378
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 777379
    iget-object v0, p1, LX/4YQ;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->e:Ljava/lang/String;

    .line 777380
    iget-object v0, p1, LX/4YQ;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777381
    iget-object v0, p1, LX/4YQ;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->g:Ljava/lang/String;

    .line 777382
    iget-boolean v0, p1, LX/4YQ;->e:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->h:Z

    .line 777383
    iget-object v0, p1, LX/4YQ;->f:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 777384
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 777385
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->h:Z

    .line 777386
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 777387
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 777388
    if-eqz v0, :cond_0

    .line 777389
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 777390
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 777391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 777393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 777394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 777395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 777396
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 777397
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 777398
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 777399
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 777400
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 777401
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 777402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777403
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 777404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 777406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 777408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 777409
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777410
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 777411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 777412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 777413
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 777414
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 777415
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777416
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 777357
    new-instance v0, LX/4YR;

    invoke-direct {v0, p1}, LX/4YR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 777371
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 777372
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->h:Z

    .line 777373
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 777358
    const-string v0, "viewer_has_voted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777359
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 777360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 777361
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 777362
    :goto_0
    return-void

    .line 777363
    :cond_0
    const-string v0, "voters.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    .line 777365
    if-eqz v0, :cond_1

    .line 777366
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 777367
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 777368
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 777369
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 777346
    const-string v0, "viewer_has_voted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777347
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->a(Z)V

    .line 777348
    :cond_0
    :goto_0
    return-void

    .line 777349
    :cond_1
    const-string v0, "voters.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    .line 777351
    if-eqz v0, :cond_0

    .line 777352
    if-eqz p3, :cond_2

    .line 777353
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 777354
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a(I)V

    .line 777355
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    goto :goto_0

    .line 777356
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a(I)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 777345
    const v0, 0x38f9781b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->e:Ljava/lang/String;

    .line 777344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->g:Ljava/lang/String;

    .line 777338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 777333
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 777334
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 777335
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->h:Z

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 777332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuestionOption;->i:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    return-object v0
.end method
