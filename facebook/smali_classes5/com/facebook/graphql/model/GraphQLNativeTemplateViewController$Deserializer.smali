.class public final Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767696
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 767697
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767695
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 767650
    const/16 v0, 0x21c

    .line 767651
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 767652
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 767653
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 767654
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_b

    .line 767655
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 767656
    :goto_0
    move v2, v4

    .line 767657
    if-eqz v1, :cond_0

    .line 767658
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 767659
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 767660
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 767661
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 767662
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 767663
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 767664
    move-object v2, v1

    .line 767665
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNativeTemplateViewController;-><init>()V

    .line 767666
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 767667
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 767668
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 767669
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 767670
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 767671
    :cond_1
    return-object v1

    .line 767672
    :cond_2
    const-string p0, "can_ptr"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 767673
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v2

    move v8, v2

    move v2, v6

    .line 767674
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_9

    .line 767675
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 767676
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 767677
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 767678
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 767679
    :cond_4
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, LX/186;->a(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 767680
    :cond_5
    const-string p0, "analytics_module"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 767681
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 767682
    :cond_6
    const-string p0, "background_color"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 767683
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 767684
    :cond_7
    const-string p0, "native_template_view"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 767685
    invoke-static {p1, v3}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 767686
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 767687
    :cond_9
    const/4 v12, 0x6

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 767688
    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 767689
    invoke-virtual {v3, v6, v10}, LX/186;->b(II)V

    .line 767690
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 767691
    if-eqz v2, :cond_a

    .line 767692
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v8}, LX/186;->a(IZ)V

    .line 767693
    :cond_a
    const/4 v2, 0x5

    invoke-virtual {v3, v2, v7}, LX/186;->b(II)V

    .line 767694
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_b
    move v2, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v4

    goto/16 :goto_1
.end method
