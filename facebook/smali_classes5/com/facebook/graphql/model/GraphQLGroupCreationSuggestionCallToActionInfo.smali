.class public final Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759706
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759705
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759703
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759704
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759708
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759709
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 759697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759698
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->a()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 759699
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 759700
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 759701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759702
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759690
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->a()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759691
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->a()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759692
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->a()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759693
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;

    .line 759694
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionCallToActionInfo;->e:Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestion;

    .line 759695
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759696
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759688
    const v0, -0x1d846281

    return v0
.end method
