.class public final Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData$Serializer;
.end annotation


# instance fields
.field public e:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

.field public i:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745446
    const-class v0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745447
    const-class v0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 745448
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 745449
    return-void
.end method

.method private j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 745453
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745454
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745455
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->e:I

    return v0
.end method

.method private k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745450
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745451
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745452
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->g:I

    return v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745443
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745444
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 745445
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745423
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->i:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745424
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->i:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->i:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 745425
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->i:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 745431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 745433
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 745434
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->j()I

    move-result v2

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 745435
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 745436
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->k()I

    move-result v2

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 745437
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    if-ne v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 745438
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 745439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 745441
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v0

    goto :goto_0

    .line 745442
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 745415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 745417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 745418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 745419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 745420
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 745421
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745422
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745412
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745413
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 745414
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->f:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 745426
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745427
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->e:I

    .line 745428
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->g:I

    .line 745429
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 745430
    const v0, -0x128cb8b8

    return v0
.end method
