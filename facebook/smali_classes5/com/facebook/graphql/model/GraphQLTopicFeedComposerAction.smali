.class public final Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788479
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788452
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 788477
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 788478
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788474
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788475
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788476
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 788480
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788481
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    .line 788482
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->g:Ljava/lang/String;

    .line 788473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 788461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788462
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 788463
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 788464
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 788465
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 788466
    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->j()Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 788467
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 788468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788469
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 788470
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->j()Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 788453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788454
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 788455
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 788456
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 788457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;

    .line 788458
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicFeedComposerAction;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788459
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788460
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 788451
    const v0, -0x234e93dd

    return v0
.end method
