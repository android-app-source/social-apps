.class public final Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761982
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761981
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 761979
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 761980
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 761969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761970
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 761971
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 761972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 761973
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 761974
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 761975
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 761976
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 761977
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761978
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 761961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 761963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 761964
    if-eqz v1, :cond_0

    .line 761965
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;

    .line 761966
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->f:Ljava/util/List;

    .line 761967
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761968
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761958
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761959
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->e:Ljava/lang/String;

    .line 761960
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 761957
    const v0, -0x2cba6393

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 761951
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761952
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->f:Ljava/util/List;

    .line 761953
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761954
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761955
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->g:Ljava/lang/String;

    .line 761956
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method
