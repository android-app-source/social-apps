.class public final Lcom/facebook/graphql/model/GraphQLReactionUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 779104
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 779103
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 779042
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 779043
    return-void
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    .line 779102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    return-object v0
.end method

.method private p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779097
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779098
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779099
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->g:Z

    return v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->i:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->i:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->i:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 779090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method

.method private t()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 779085
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779086
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779087
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 779105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 779106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 779107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 779108
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 779109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 779110
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 779111
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->s()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 779112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n()LX/0Px;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 779113
    const/16 v8, 0xb

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 779114
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 779115
    const/4 v8, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v0

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    if-ne v0, v9, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v8, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779116
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->p()Z

    move-result v8

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 779117
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 779118
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 779119
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-ne v2, v3, :cond_2

    :goto_2
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779120
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 779121
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 779122
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 779123
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->t()I

    move-result v1

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 779124
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 779125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 779126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 779127
    goto/16 :goto_0

    .line 779128
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v0

    goto :goto_1

    .line 779129
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 779062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 779063
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 779064
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 779065
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 779066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 779067
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779068
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 779069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779070
    if-eqz v2, :cond_1

    .line 779071
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 779072
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o:Ljava/util/List;

    move-object v1, v0

    .line 779073
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 779074
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 779076
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 779077
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779078
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->s()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 779079
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->s()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 779080
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->s()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 779081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 779082
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 779083
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 779084
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 779057
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 779058
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->g:Z

    .line 779059
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n:I

    .line 779060
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 779056
    const v0, -0x5c7cc093

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779050
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 779051
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 779052
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 779053
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 779054
    const/4 v0, 0x0

    .line 779055
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779047
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779048
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->h:Ljava/lang/String;

    .line 779049
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779044
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779045
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 779046
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k:Ljava/lang/String;

    .line 779041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779036
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779037
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o:Ljava/util/List;

    .line 779038
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnit;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
