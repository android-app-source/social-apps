.class public final Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 743857
    const-class v0, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 743858
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 743834
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 743836
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 743837
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v4, 0x0

    .line 743838
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 743839
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 743840
    if-eqz v2, :cond_0

    .line 743841
    const-string v3, "album"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743842
    invoke-static {v1, v2, p1, p2}, LX/4Ko;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 743843
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743844
    if-eqz v2, :cond_1

    .line 743845
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743846
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743847
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 743848
    if-eqz v2, :cond_2

    .line 743849
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743850
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743851
    :cond_2
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 743852
    if-eqz v2, :cond_3

    .line 743853
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743854
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 743855
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 743856
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 743835
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
