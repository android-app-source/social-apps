.class public final Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 747332
    const-class v0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 747333
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 747334
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 747335
    const/16 v0, 0x27b

    .line 747336
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 747337
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 747338
    const/4 v4, 0x0

    .line 747339
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_6

    .line 747340
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747341
    :goto_0
    move v2, v4

    .line 747342
    if-eqz v1, :cond_0

    .line 747343
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 747344
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 747345
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747346
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 747347
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 747348
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 747349
    move-object v2, v1

    .line 747350
    new-instance v1, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;-><init>()V

    .line 747351
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 747352
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 747353
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 747354
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 747355
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 747356
    :cond_1
    return-object v1

    .line 747357
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747358
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 747359
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 747360
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 747361
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v7, :cond_3

    .line 747362
    const-string p0, "client_mutation_id"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 747363
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 747364
    :cond_4
    const-string p0, "story"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 747365
    invoke-static {p1, v3}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 747366
    :cond_5
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 747367
    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 747368
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747369
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_6
    move v2, v4

    move v6, v4

    goto :goto_1
.end method
