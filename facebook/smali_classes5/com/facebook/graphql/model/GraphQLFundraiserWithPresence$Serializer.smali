.class public final Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 754367
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 754368
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 754370
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 754371
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 754372
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 754373
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 754374
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 754375
    if-eqz v2, :cond_0

    .line 754376
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754377
    invoke-static {v1, v0, p0, p1}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 754378
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 754379
    if-eqz v2, :cond_1

    .line 754380
    const-string p0, "posted_item_privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754381
    invoke-static {v1, v2, p1, p2}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 754382
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 754383
    if-eqz v2, :cond_2

    .line 754384
    const-string p0, "can_viewer_delete"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754385
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 754386
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 754387
    if-eqz v2, :cond_3

    .line 754388
    const-string p0, "can_viewer_edit"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754389
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 754390
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 754391
    if-eqz v2, :cond_4

    .line 754392
    const-string p0, "can_viewer_post"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754393
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 754394
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 754395
    if-eqz v2, :cond_5

    .line 754396
    const-string p0, "can_viewer_report"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754397
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 754398
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 754399
    if-eqz v2, :cond_6

    .line 754400
    const-string p0, "is_viewer_following"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754401
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 754402
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 754403
    if-eqz v2, :cond_7

    .line 754404
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 754405
    invoke-static {v1, v2, p1, p2}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 754406
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 754407
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 754369
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFundraiserWithPresence;LX/0nX;LX/0my;)V

    return-void
.end method
