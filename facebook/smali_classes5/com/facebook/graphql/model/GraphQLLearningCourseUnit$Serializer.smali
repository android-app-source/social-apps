.class public final Lcom/facebook/graphql/model/GraphQLLearningCourseUnit$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLearningCourseUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 763737
    const-class v0, Lcom/facebook/graphql/model/GraphQLLearningCourseUnit;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLearningCourseUnit$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLearningCourseUnit$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 763738
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 763739
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLearningCourseUnit;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 763740
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 763741
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 763742
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 763743
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 763744
    if-eqz p0, :cond_0

    .line 763745
    const-string p2, "completed"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763746
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 763747
    :cond_0
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 763748
    if-eqz p0, :cond_1

    .line 763749
    const-string p2, "description"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763750
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763751
    :cond_1
    const/4 p0, 0x3

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 763752
    if-eqz p0, :cond_2

    .line 763753
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763754
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763755
    :cond_2
    const/4 p0, 0x4

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 763756
    if-eqz p0, :cond_3

    .line 763757
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763758
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763759
    :cond_3
    const/4 p0, 0x5

    const/4 p2, 0x0

    invoke-virtual {v1, v0, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 763760
    if-eqz p0, :cond_4

    .line 763761
    const-string p2, "progress"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763762
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 763763
    :cond_4
    const/4 p0, 0x6

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 763764
    if-eqz p0, :cond_5

    .line 763765
    const-string p2, "url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763766
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763767
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 763768
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 763769
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLearningCourseUnit;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLearningCourseUnit$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLearningCourseUnit;LX/0nX;LX/0my;)V

    return-void
.end method
