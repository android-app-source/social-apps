.class public final Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public h:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763269
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763230
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 763231
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 763232
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 763233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 763235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 763236
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 763237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 763238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 763239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 763240
    const/16 v7, 0xb

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 763241
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 763242
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 763243
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-ne v0, v7, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 763244
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v2

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-ne v2, v7, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 763245
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 763246
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 763247
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 763248
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 763249
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 763250
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 763251
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 763252
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763253
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 763254
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v0

    goto :goto_0

    .line 763255
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763270
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763271
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->e:Ljava/util/List;

    .line 763272
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 763256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 763258
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    .line 763259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 763260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 763261
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    .line 763262
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763263
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 763264
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 763265
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->i:Z

    .line 763266
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j:Z

    .line 763267
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k:Z

    .line 763268
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 763226
    const v0, 0x49b4f8e2    # 1482524.2f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->f:Ljava/lang/String;

    .line 763229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 763225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 763222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763199
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 763200
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 763201
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->i:Z

    return v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763217
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 763218
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 763219
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763214
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 763215
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 763216
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763211
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763212
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l:Ljava/lang/String;

    .line 763213
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763208
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763209
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m:Ljava/lang/String;

    .line 763210
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n:Ljava/util/List;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n:Ljava/util/List;

    .line 763207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763202
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763203
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    .line 763204
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    return-object v0
.end method
