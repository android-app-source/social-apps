.class public final Lcom/facebook/graphql/model/GraphQLFundraiserForStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753168
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753169
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 753170
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 753171
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753172
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753173
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->e:Ljava/lang/String;

    .line 753174
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753175
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753176
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753177
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->f:Z

    return v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753043
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753044
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753045
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->g:Z

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753178
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->h:Lcom/facebook/graphql/model/GraphQLCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753179
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->h:Lcom/facebook/graphql/model/GraphQLCharity;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->h:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753180
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->h:Lcom/facebook/graphql/model/GraphQLCharity;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753181
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753182
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753183
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753187
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753188
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753189
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753190
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753191
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753192
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m:Ljava/lang/String;

    .line 753164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o:Ljava/lang/String;

    .line 753161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p:Ljava/lang/String;

    .line 753158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r:Ljava/lang/String;

    .line 753152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753145
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753146
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753141
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753142
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    .line 753143
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 753105
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753106
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 753107
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 753108
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 753109
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 753110
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 753111
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 753112
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 753113
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 753114
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 753115
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 753116
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 753117
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->w()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 753118
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 753119
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->y()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 753120
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->z()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 753121
    const/16 v17, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 753122
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 753123
    const/4 v2, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k()Z

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753124
    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l()Z

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753125
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 753126
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 753127
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 753128
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 753129
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 753130
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 753131
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 753132
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 753133
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 753134
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 753135
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 753136
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 753137
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 753138
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753139
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753140
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 753052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753053
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 753054
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753055
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 753056
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753057
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753058
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 753059
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753060
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 753061
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753062
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->h:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753063
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 753064
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753065
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 753066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753067
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753068
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->z()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 753069
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->z()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    .line 753070
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->z()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 753071
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753072
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->u:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    .line 753073
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 753074
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 753076
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753077
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753078
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 753079
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753080
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 753081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753082
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753083
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 753084
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753085
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 753086
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753087
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753088
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 753089
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753090
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 753091
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753092
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753093
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 753094
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753095
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 753096
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753097
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753098
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->y()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 753099
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->y()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 753100
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->y()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 753101
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    .line 753102
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->t:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753103
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753104
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 753047
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 753048
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->f:Z

    .line 753049
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;->g:Z

    .line 753050
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 753046
    const v0, 0x291507f7

    return v0
.end method
