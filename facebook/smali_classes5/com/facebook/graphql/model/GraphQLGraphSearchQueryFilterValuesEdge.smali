.class public final Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758569
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758578
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758570
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758571
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 758572
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 758574
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 758575
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 758576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758577
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 758560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 758564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;

    .line 758565
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758566
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758567
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758557
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758559
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 758568
    const v0, 0xa13e9a9

    return v0
.end method
