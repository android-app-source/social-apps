.class public final Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759828
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759790
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759826
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759827
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 759816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 759818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 759819
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 759820
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->a()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 759821
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->j()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 759822
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 759823
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 759824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759825
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759809
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 759811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    .line 759813
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 759814
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759815
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 759804
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 759805
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->e:Z

    .line 759806
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->f:Z

    .line 759807
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 759801
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 759802
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 759803
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759800
    const v0, -0x68faebbe    # -4.3000187E-25f

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 759797
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 759798
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 759799
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->f:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759794
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759795
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->g:Ljava/lang/String;

    .line 759796
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 759793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->h:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    return-object v0
.end method
