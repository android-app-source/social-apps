.class public final Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745805
    const-class v0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745804
    const-class v0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 745858
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 745859
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x1757d8b4

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 745860
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->l:LX/0x2;

    .line 745861
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->f:Ljava/lang/String;

    .line 745857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->j:Ljava/lang/String;

    .line 745854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->k:Ljava/lang/String;

    .line 745851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 745848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->h:Ljava/lang/String;

    .line 745847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745842
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745843
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745844
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->i:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 745839
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->l:LX/0x2;

    if-nez v0, :cond_0

    .line 745840
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->l:LX/0x2;

    .line 745841
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->l:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 745824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745825
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 745826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 745827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 745828
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 745829
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 745830
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 745831
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 745832
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 745833
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 745834
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 745835
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 745836
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 745837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745838
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 745821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745822
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745823
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 745818
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->i:J

    .line 745819
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 745815
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745816
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->i:J

    .line 745817
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 745810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 745811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 745812
    :goto_0
    return-object v0

    .line 745813
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 745814
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 745809
    const v0, 0x1757d8b4

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745806
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745807
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g:Ljava/lang/String;

    .line 745808
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method
