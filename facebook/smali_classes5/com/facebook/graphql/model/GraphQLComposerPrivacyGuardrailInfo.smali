.class public final Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:J

.field public h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634698
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634697
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634695
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634696
    return-void
.end method

.method public constructor <init>(LX/3ld;)V
    .locals 2

    .prologue
    .line 634689
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634690
    iget-object v0, p1, LX/3ld;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634691
    iget-boolean v0, p1, LX/3ld;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->f:Z

    .line 634692
    iget-wide v0, p1, LX/3ld;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->g:J

    .line 634693
    iget-object v0, p1, LX/3ld;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634694
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 634679
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 634681
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 634682
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 634683
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 634684
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->j()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 634685
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->k()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 634686
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 634687
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634688
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 634699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634701
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 634703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634704
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634705
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 634706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634707
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 634708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634709
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634710
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634711
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634676
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 634677
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634678
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 634672
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 634673
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->f:Z

    .line 634674
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->g:J

    .line 634675
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 634671
    const v0, 0x5744f7bd

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634668
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634669
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634670
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->f:Z

    return v0
.end method

.method public final k()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 634665
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 634666
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634667
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->g:J

    return-wide v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634662
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 634663
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634664
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method
