.class public final Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767964
    const-class v0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 767965
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767922
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 767923
    const/16 v0, 0x1cb

    .line 767924
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 767925
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 767926
    const/4 v4, 0x0

    .line 767927
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 767928
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 767929
    :goto_0
    move v2, v4

    .line 767930
    if-eqz v1, :cond_0

    .line 767931
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 767932
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 767933
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 767934
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 767935
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 767936
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 767937
    move-object v2, v1

    .line 767938
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;-><init>()V

    .line 767939
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 767940
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 767941
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 767942
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 767943
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 767944
    :cond_1
    return-object v1

    .line 767945
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 767946
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_7

    .line 767947
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 767948
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 767949
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v9, :cond_3

    .line 767950
    const-string p0, "bounds"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 767951
    invoke-static {p1, v3}, LX/4NK;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 767952
    :cond_4
    const-string p0, "results_title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 767953
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 767954
    :cond_5
    const-string p0, "suggestion_text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 767955
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 767956
    :cond_6
    const-string p0, "topic"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 767957
    invoke-static {p1, v3}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 767958
    :cond_7
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 767959
    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 767960
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 767961
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 767962
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 767963
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_8
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    goto :goto_1
.end method
