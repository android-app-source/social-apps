.class public final Lcom/facebook/graphql/model/GraphQLNote;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNote$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNote$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public C:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/graphql/model/GraphQLComposedDocument;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 769663
    const-class v0, Lcom/facebook/graphql/model/GraphQLNote$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 769662
    const-class v0, Lcom/facebook/graphql/model/GraphQLNote$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 769664
    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 769665
    return-void
.end method

.method private A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 769666
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 769667
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 769668
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->v:Z

    return v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLComposedDocument;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769669
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->w:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769670
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->w:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->w:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 769671
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->w:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769672
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->x:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769673
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->x:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->x:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 769674
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->x:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769675
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769676
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->y:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->y:Ljava/lang/String;

    .line 769677
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->y:Ljava/lang/String;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769699
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769700
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769701
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->A:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->A:Ljava/lang/String;

    .line 769680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->A:Ljava/lang/String;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 769681
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769682
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 769683
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769684
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->C:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769685
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->C:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769686
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->C:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769687
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->D:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769688
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->D:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769689
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->D:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769690
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769691
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769692
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769693
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769694
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769695
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769696
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769697
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769698
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 769658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->i:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->i:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->i:Lcom/facebook/graphql/model/GraphQLActor;

    .line 769661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->i:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769462
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769463
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->j:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->j:Ljava/lang/String;

    .line 769464
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769468
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769469
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769470
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->l:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->l:Ljava/lang/String;

    .line 769473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769474
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769475
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->m:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->m:Ljava/lang/String;

    .line 769476
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769477
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769478
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 769479
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769480
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769481
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769482
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769483
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769484
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769485
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769486
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769487
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769488
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769489
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769490
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769491
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->s:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769493
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->s:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769494
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->s:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 769497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769498
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->u:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769499
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->u:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769500
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->u:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 27

    .prologue
    .line 769501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 769502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->j()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 769503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->k()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 769504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 769505
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 769506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 769507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 769508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 769509
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->q()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 769510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->r()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 769511
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 769512
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 769513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 769514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 769515
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 769516
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 769517
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 769518
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 769519
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->B()Lcom/facebook/graphql/model/GraphQLComposedDocument;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 769520
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->C()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 769521
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->D()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 769522
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 769523
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->F()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 769524
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 769525
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 769526
    const/16 v26, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 769527
    const/16 v26, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 769528
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 769529
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 769530
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 769531
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 769532
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 769533
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 769534
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 769535
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 769536
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 769537
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 769538
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 769539
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 769540
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 769541
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769542
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769543
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769544
    const/16 v2, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 769545
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769546
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769547
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769548
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769549
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769550
    const/16 v3, 0x19

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->G()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 769551
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769552
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 769553
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 769554
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 769555
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLNote;->G()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 769556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 769557
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->j()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 769558
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->j()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769559
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->j()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 769560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769561
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769562
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->k()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 769563
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->k()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769564
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->k()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 769565
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769566
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 769567
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 769568
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769569
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 769570
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769571
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769572
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 769573
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 769574
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 769575
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769576
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 769577
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 769578
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 769579
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 769580
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769581
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->i:Lcom/facebook/graphql/model/GraphQLActor;

    .line 769582
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 769583
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769584
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 769585
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769586
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769587
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 769588
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 769589
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 769590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769591
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 769592
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 769593
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769594
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 769595
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769596
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769597
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 769598
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769599
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 769600
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769601
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769602
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 769603
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769604
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 769605
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769606
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769607
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 769608
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769609
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 769610
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769611
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769612
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 769613
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769614
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 769615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769616
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769617
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 769618
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769619
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 769620
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769621
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769622
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 769623
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 769624
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 769625
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769626
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 769627
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 769628
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769629
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 769630
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769631
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769632
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->B()Lcom/facebook/graphql/model/GraphQLComposedDocument;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 769633
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->B()Lcom/facebook/graphql/model/GraphQLComposedDocument;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 769634
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->B()Lcom/facebook/graphql/model/GraphQLComposedDocument;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 769635
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769636
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->w:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 769637
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 769638
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769639
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 769640
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769641
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769642
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->C()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 769643
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->C()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 769644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->C()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 769645
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769646
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->x:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 769647
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 769648
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769649
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 769650
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNote;

    .line 769651
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNote;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769652
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 769653
    if-nez v1, :cond_13

    :goto_0
    return-object p0

    :cond_13
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769654
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNote;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 769465
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 769466
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNote;->v:Z

    .line 769467
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 769655
    const v0, 0x252412

    return v0
.end method
