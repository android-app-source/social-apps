.class public final Lcom/facebook/graphql/model/GraphQLNativeMask;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNativeMask$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNativeMask$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMask3DAsset;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767440
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeMask$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767439
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeMask$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 767437
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 767438
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->i:Ljava/lang/String;

    .line 767436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 767394
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 767396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 767397
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 767398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 767399
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 767400
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 767401
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 767402
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 767403
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 767404
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 767405
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 767406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767407
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 767421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 767423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 767424
    if-eqz v1, :cond_2

    .line 767425
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeMask;

    .line 767426
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLNativeMask;->e:Ljava/util/List;

    move-object v1, v0

    .line 767427
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 767428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    .line 767429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 767430
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNativeMask;

    .line 767431
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNativeMask;->h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    .line 767432
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767433
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeMask;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 767420
    const v0, 0x2fd4c3c3

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMask3DAsset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 767417
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767418
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->e:Ljava/util/List;

    .line 767419
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767414
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767415
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->f:Ljava/lang/String;

    .line 767416
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767411
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767412
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->g:Ljava/lang/String;

    .line 767413
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    .line 767410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNativeMask;->h:Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    return-object v0
.end method
