.class public final Lcom/facebook/graphql/model/GraphQLPlaceListItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceListItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceListItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775078
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775079
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 775021
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775022
    return-void
.end method

.method public constructor <init>(LX/4Y9;)V
    .locals 1

    .prologue
    .line 775071
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775072
    iget-object v0, p1, LX/4Y9;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->e:Ljava/lang/String;

    .line 775073
    iget-object v0, p1, LX/4Y9;->c:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775074
    iget-object v0, p1, LX/4Y9;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775075
    iget-object v0, p1, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 775076
    iget-object v0, p1, LX/4Y9;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->h:Ljava/lang/String;

    .line 775077
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 775057
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 775059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 775060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 775061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 775062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 775063
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 775064
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 775065
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 775066
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 775067
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 775068
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 775069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775070
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775039
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 775042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 775044
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775045
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 775046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 775048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 775049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775050
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 775051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 775052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 775053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 775054
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 775055
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775056
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775038
    const v0, 0x7dfc96d8

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->e:Ljava/lang/String;

    .line 775037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775032
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775033
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775034
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->f:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775029
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775030
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 775031
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->g:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->h:Ljava/lang/String;

    .line 775028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
