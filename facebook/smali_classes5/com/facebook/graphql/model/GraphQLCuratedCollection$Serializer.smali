.class public final Lcom/facebook/graphql/model/GraphQLCuratedCollection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCuratedCollection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 747099
    const-class v0, Lcom/facebook/graphql/model/GraphQLCuratedCollection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLCuratedCollection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCuratedCollection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 747100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 747101
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLCuratedCollection;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 747102
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 747103
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 747104
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 747105
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 747106
    if-eqz v2, :cond_0

    .line 747107
    const-string p0, "collection_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 747108
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 747109
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 747110
    if-eqz v2, :cond_1

    .line 747111
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 747112
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 747113
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 747114
    if-eqz v2, :cond_2

    .line 747115
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 747116
    invoke-static {v1, v2, p1, p2}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 747117
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 747118
    if-eqz v2, :cond_3

    .line 747119
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 747120
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 747121
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 747122
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 747123
    check-cast p1, Lcom/facebook/graphql/model/GraphQLCuratedCollection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLCuratedCollection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLCuratedCollection;LX/0nX;LX/0my;)V

    return-void
.end method
