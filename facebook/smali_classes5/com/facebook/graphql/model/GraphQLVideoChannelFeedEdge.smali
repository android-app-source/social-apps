.class public final Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 789350
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 789349
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 789347
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 789348
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 789341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 789342
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v0

    .line 789343
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 789344
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 789345
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 789346
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 789329
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 789330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 789331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 789332
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 789333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;

    .line 789334
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 789335
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 789336
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789338
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->e:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789339
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->e:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x0

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 789340
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannelFeedEdge;->e:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 789337
    const v0, 0x6a81e8c3

    return v0
.end method
