.class public Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMapTileNode;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764809
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileNode;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764810
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764811
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMapTileNode;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764812
    if-nez p0, :cond_0

    .line 764813
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 764814
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764815
    invoke-static {p0, p1, p2}, Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;->b(Lcom/facebook/graphql/model/GraphQLMapTileNode;LX/0nX;LX/0my;)V

    .line 764816
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764817
    return-void
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLMapTileNode;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 764818
    const-string v0, "node"

    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLMapTileNode;->tile:Lcom/facebook/graphql/model/GraphQLMapTile;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 764819
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764820
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMapTileNode;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;->a(Lcom/facebook/graphql/model/GraphQLMapTileNode;LX/0nX;LX/0my;)V

    return-void
.end method
