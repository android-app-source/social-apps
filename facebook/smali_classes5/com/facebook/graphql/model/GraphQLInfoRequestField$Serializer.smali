.class public final Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInfoRequestField;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 761724
    const-class v0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 761725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 761752
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLInfoRequestField;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 761727
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 761728
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 761729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 761730
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(IIS)S

    move-result v2

    .line 761731
    if-eqz v2, :cond_0

    .line 761732
    const-string v2, "field_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761733
    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761734
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761735
    if-eqz v2, :cond_1

    .line 761736
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761737
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761738
    :cond_1
    invoke-virtual {v1, v0, p2, p0}, LX/15i;->a(IIS)S

    move-result v2

    .line 761739
    if-eqz v2, :cond_2

    .line 761740
    const-string v2, "status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761741
    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v1, v0, p2, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761742
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761743
    if-eqz v2, :cond_3

    .line 761744
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761745
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761746
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761747
    if-eqz v2, :cond_4

    .line 761748
    const-string v3, "cache_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761749
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761750
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 761751
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 761726
    check-cast p1, Lcom/facebook/graphql/model/GraphQLInfoRequestField;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;->a(Lcom/facebook/graphql/model/GraphQLInfoRequestField;LX/0nX;LX/0my;)V

    return-void
.end method
