.class public Lcom/facebook/graphql/model/GraphQLMapTileNode;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMapTileNodeDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTileNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final tile:Lcom/facebook/graphql/model/GraphQLMapTile;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "node"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764786
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileNodeDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764787
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileNodeSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 764778
    new-instance v0, LX/4XA;

    invoke-direct {v0}, LX/4XA;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/GraphQLMapTileNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764781
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTileNode;->tile:Lcom/facebook/graphql/model/GraphQLMapTile;

    .line 764782
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 764783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764784
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMapTile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTileNode;->tile:Lcom/facebook/graphql/model/GraphQLMapTile;

    .line 764785
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLMapTile;
    .locals 1

    .prologue
    .line 764779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTileNode;->tile:Lcom/facebook/graphql/model/GraphQLMapTile;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 764777
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 764775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTileNode;->a()Lcom/facebook/graphql/model/GraphQLMapTile;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 764776
    return-void
.end method
