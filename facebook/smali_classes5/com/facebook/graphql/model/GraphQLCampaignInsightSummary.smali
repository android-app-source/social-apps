.class public final Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:J

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745553
    const-class v0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 745554
    const-class v0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 745555
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 745556
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v12, 0x0

    .line 745557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 745559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 745560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 745561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 745562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 745563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 745564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 745565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->u()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 745566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->x()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 745567
    const/16 v11, 0x10

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 745568
    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 745569
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 745570
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 745571
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 745572
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 745573
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 745574
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 745575
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->p()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 745576
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 745577
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->r()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 745578
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 745579
    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 745580
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 745581
    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->v()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 745582
    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->w()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 745583
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 745584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 745586
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 745588
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->e:Ljava/lang/String;

    .line 745591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 745592
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745593
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->h:Z

    .line 745594
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j:I

    .line 745595
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->l:I

    .line 745596
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->n:I

    .line 745597
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->p:I

    .line 745598
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->r:J

    .line 745599
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s:J

    .line 745600
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 745601
    const v0, 0x7b59a63e

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745603
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->f:Ljava/lang/String;

    .line 745604
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745514
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745515
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->g:Ljava/lang/String;

    .line 745516
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745550
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745551
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745552
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->h:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745605
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745606
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->i:Ljava/lang/String;

    .line 745607
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745547
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745548
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745549
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j:I

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k:Ljava/lang/String;

    .line 745546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745541
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745542
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745543
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->l:I

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745538
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745539
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m:Ljava/lang/String;

    .line 745540
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 745535
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745536
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745537
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->n:I

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745532
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745533
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o:Ljava/lang/String;

    .line 745534
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745529
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745530
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745531
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->p:I

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745526
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745527
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q:Ljava/lang/String;

    .line 745528
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final v()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745523
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745524
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745525
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->r:J

    return-wide v0
.end method

.method public final w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 745520
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 745521
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 745522
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s:J

    return-wide v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 745517
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 745518
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t:Ljava/lang/String;

    .line 745519
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t:Ljava/lang/String;

    return-object v0
.end method
