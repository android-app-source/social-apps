.class public final Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764633
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764632
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764630
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764631
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 764636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764627
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764628
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764629
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 764619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764620
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 764621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->j()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 764622
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 764623
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 764624
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 764625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764626
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764607
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 764608
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 764609
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 764610
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;

    .line 764611
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 764612
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->j()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 764613
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->j()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764614
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->j()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 764615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;

    .line 764616
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;->f:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764617
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764618
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764605
    const v0, 0x602953b6

    return v0
.end method
