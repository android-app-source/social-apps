.class public final Lcom/facebook/graphql/model/GraphQLAdAccount;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAdAccount$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAdAccount$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

.field public g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Z

.field public z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743589
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdAccount$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743590
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdAccount$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 743591
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 743592
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLTimezoneInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743593
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743594
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    .line 743595
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743596
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743597
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->w:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->w:Ljava/lang/String;

    .line 743598
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->w:Ljava/lang/String;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743599
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743600
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743601
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743602
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743603
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743604
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->y:Z

    return v0
.end method

.method private E()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743605
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743606
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743607
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->z:Z

    return v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743608
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743609
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->e:Ljava/lang/String;

    .line 743610
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743611
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->f:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743612
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->f:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->f:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    .line 743613
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->f:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743614
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743615
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743616
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743617
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743619
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743464
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743465
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743466
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->i:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743620
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743621
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->j:Ljava/lang/String;

    .line 743622
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->k:Ljava/lang/String;

    .line 743588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743623
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->l:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743624
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->l:Lcom/facebook/graphql/model/FeedUnit;

    const/16 v1, 0x8

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->l:Lcom/facebook/graphql/model/FeedUnit;

    .line 743625
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->l:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 743461
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743462
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743463
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->m:Z

    return v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743467
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743468
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->n:Ljava/lang/String;

    .line 743469
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->n:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743470
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743471
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->o:Ljava/lang/String;

    .line 743472
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743473
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743474
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743475
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743476
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743477
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743478
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743479
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743480
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->r:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->r:Ljava/lang/String;

    .line 743481
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->r:Ljava/lang/String;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743482
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743483
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->s:Ljava/lang/String;

    .line 743484
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->s:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743485
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743486
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743487
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->u:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->u:Ljava/lang/String;

    .line 743490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->u:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 743491
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743492
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 743493
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 743494
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->m()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 743495
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 743496
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 743497
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->q()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    sget-object v8, LX/16Z;->a:LX/16Z;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v7

    .line 743498
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->s()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 743499
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->t()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 743500
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 743501
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->v()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 743502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->w()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 743503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->x()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 743504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->y()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 743505
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->z()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 743506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->A()Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 743507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->B()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 743508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->C()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 743509
    const/16 v19, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 743510
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 743511
    const/16 v19, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->k()Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-result-object v2

    sget-object v20, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 743512
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 743513
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 743514
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->n()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 743515
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 743516
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 743517
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 743518
    const/16 v2, 0x9

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->r()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 743519
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 743520
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 743521
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 743522
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 743523
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 743524
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 743525
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 743526
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 743527
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 743528
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 743529
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 743530
    const/16 v2, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 743531
    const/16 v2, 0x16

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->E()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 743532
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743533
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 743534
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->k()Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 743535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743536
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 743537
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743538
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 743539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743540
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743541
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->m()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 743542
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->m()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743543
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->m()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 743544
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743545
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->h:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743546
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->C()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 743547
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->C()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743548
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->C()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 743549
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743550
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->x:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743551
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->q()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 743552
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->q()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 743553
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->q()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 743554
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743555
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->l:Lcom/facebook/graphql/model/FeedUnit;

    .line 743556
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 743557
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743558
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 743559
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743560
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->p:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743561
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->v()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 743562
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->v()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743563
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->v()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 743564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743565
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743566
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->y()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 743567
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->y()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743568
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->y()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 743569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743570
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->t:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 743571
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->A()Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 743572
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->A()Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    .line 743573
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->A()Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 743574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 743575
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdAccount;->v:Lcom/facebook/graphql/model/GraphQLTimezoneInfo;

    .line 743576
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743577
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743578
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdAccount;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 743579
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 743580
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->i:Z

    .line 743581
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->m:Z

    .line 743582
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->y:Z

    .line 743583
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdAccount;->z:Z

    .line 743584
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 743585
    const v0, 0x5661d86a

    return v0
.end method
