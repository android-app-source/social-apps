.class public final Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781244
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781245
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781177
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 781178
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781241
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781242
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l:Ljava/lang/String;

    .line 781243
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 781222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 781224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 781225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 781226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 781227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 781228
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 781229
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 781230
    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j()Z

    move-result v7

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 781231
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 781232
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 781233
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 781234
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 781235
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 781236
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 781237
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 781238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 781240
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 781204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 781206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 781208
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781209
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781210
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 781211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    .line 781212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 781213
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781214
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    .line 781215
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 781216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    .line 781217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 781218
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781219
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    .line 781220
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781221
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 781200
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 781201
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->e:Z

    .line 781202
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 781246
    const v0, 0x3be7b91

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 781197
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 781198
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 781199
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->e:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781194
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781195
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->f:Ljava/lang/String;

    .line 781196
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781191
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781192
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 781193
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->g:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    .line 781190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->h:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionRespondersConnection;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 781185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->i:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->i:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->i:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    .line 781187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->i:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781182
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781183
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j:Ljava/lang/String;

    .line 781184
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781179
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781180
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    .line 781181
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k:Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    return-object v0
.end method
