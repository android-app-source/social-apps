.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786443
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 786444
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 786445
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 786446
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 786447
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 786448
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 786449
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786450
    if-eqz v2, :cond_0

    .line 786451
    const-string p0, "associated_places_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786452
    invoke-static {v1, v2, p1}, LX/4RL;->a(LX/15i;ILX/0nX;)V

    .line 786453
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786454
    if-eqz v2, :cond_1

    .line 786455
    const-string p0, "cursor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786456
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786457
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786458
    if-eqz v2, :cond_2

    .line 786459
    const-string p0, "custom_icon_suggestions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786460
    invoke-static {v1, v2, p1, p2}, LX/3Ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 786461
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786462
    if-eqz v2, :cond_3

    .line 786463
    const-string p0, "display_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786464
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786465
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786466
    if-eqz v2, :cond_4

    .line 786467
    const-string p0, "icon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786468
    invoke-static {v1, v2, p1, p2}, LX/3Ar;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 786469
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786470
    if-eqz v2, :cond_5

    .line 786471
    const-string p0, "iconImageLarge"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786472
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 786473
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786474
    if-eqz v2, :cond_6

    .line 786475
    const-string p0, "node"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786476
    invoke-static {v1, v2, p1, p2}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 786477
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 786478
    if-eqz v2, :cond_7

    .line 786479
    const-string p0, "show_attachment_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786480
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 786481
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786482
    if-eqz v2, :cond_8

    .line 786483
    const-string p0, "subtext"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786484
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 786485
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786486
    if-eqz v2, :cond_9

    .line 786487
    const-string p0, "tracking"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786488
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786489
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 786490
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 786491
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;LX/0nX;LX/0my;)V

    return-void
.end method
