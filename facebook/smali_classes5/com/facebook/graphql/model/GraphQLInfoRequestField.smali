.class public final Lcom/facebook/graphql/model/GraphQLInfoRequestField;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInfoRequestField$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

.field public h:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761803
    const-class v0, Lcom/facebook/graphql/model/GraphQLInfoRequestField$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761802
    const-class v0, Lcom/facebook/graphql/model/GraphQLInfoRequestField$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 761800
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 761801
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V
    .locals 3

    .prologue
    .line 761794
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 761795
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 761796
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 761797
    if-eqz v0, :cond_0

    .line 761798
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 761799
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 761791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->e:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->e:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->e:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 761793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->e:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761788
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761789
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->f:Ljava/lang/String;

    .line 761790
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 761768
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761769
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 761770
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->g:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->h:Ljava/lang/String;

    .line 761787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761804
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761805
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->i:Ljava/lang/String;

    .line 761806
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 761771
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761772
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 761773
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 761774
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 761775
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 761776
    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    if-ne v0, v6, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 761777
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 761778
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->l()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    if-ne v2, v5, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 761779
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 761780
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 761781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 761783
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v0

    goto :goto_0

    .line 761784
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->l()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 761765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761767
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 761764
    new-instance v0, LX/4Wy;

    invoke-direct {v0, p1}, LX/4Wy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761763
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 761757
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761758
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->l()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 761759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 761760
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 761761
    :goto_0
    return-void

    .line 761762
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 761754
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761755
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLInfoRequestField;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V

    .line 761756
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 761753
    const v0, -0x51dd9867

    return v0
.end method
