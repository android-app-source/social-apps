.class public final Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 753247
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 753248
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 753249
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 753250
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 753251
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 753252
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 753253
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753254
    if-eqz v2, :cond_0

    .line 753255
    const-string v3, "campaign_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753256
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753257
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753258
    if-eqz v2, :cond_1

    .line 753259
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753260
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753261
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753262
    if-eqz v2, :cond_2

    .line 753263
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753264
    invoke-static {v1, v2, p1, p2}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753265
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753266
    if-eqz v2, :cond_3

    .line 753267
    const-string v3, "posted_item_privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753268
    invoke-static {v1, v2, p1, p2}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 753269
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753270
    if-eqz v2, :cond_4

    .line 753271
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753272
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753273
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753274
    if-eqz v2, :cond_5

    .line 753275
    const-string v3, "can_invite_to_campaign"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753276
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753277
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753278
    if-eqz v2, :cond_6

    .line 753279
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753280
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753281
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753282
    if-eqz v2, :cond_7

    .line 753283
    const-string v3, "can_viewer_edit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753284
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753285
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753286
    if-eqz v2, :cond_8

    .line 753287
    const-string v3, "can_viewer_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753288
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753289
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753290
    if-eqz v2, :cond_9

    .line 753291
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753292
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753293
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753294
    if-eqz v2, :cond_a

    .line 753295
    const-string v3, "fundraiser_page_subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753296
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753297
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753298
    if-eqz v2, :cond_b

    .line 753299
    const-string v3, "is_viewer_following"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753300
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753301
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753302
    if-eqz v2, :cond_c

    .line 753303
    const-string v3, "logo_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753304
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 753305
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753306
    if-eqz v2, :cond_d

    .line 753307
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753308
    invoke-static {v1, v2, p1, p2}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 753309
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753310
    if-eqz v2, :cond_e

    .line 753311
    const-string v3, "donors_social_context_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753312
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753313
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753314
    if-eqz v2, :cond_f

    .line 753315
    const-string v3, "fundraiser_progress_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753316
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753317
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 753318
    if-eqz v2, :cond_10

    .line 753319
    const-string v3, "has_goal_amount"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753320
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 753321
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 753322
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_11

    .line 753323
    const-string v4, "percent_of_goal_reached"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753324
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 753325
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753326
    if-eqz v2, :cond_12

    .line 753327
    const-string v3, "user_donors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753328
    invoke-static {v1, v2, p1, p2}, LX/4N6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 753329
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 753330
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 753331
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;LX/0nX;LX/0my;)V

    return-void
.end method
