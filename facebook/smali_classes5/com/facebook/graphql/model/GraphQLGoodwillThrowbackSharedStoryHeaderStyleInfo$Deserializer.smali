.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 757481
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 757482
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 757483
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 757484
    const/16 v0, 0x25d

    .line 757485
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 757486
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 757487
    const/4 v4, 0x0

    .line 757488
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_6

    .line 757489
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 757490
    :goto_0
    move v2, v4

    .line 757491
    if-eqz v1, :cond_0

    .line 757492
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 757493
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 757494
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 757495
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 757496
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 757497
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 757498
    move-object v2, v1

    .line 757499
    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSharedStoryHeaderStyleInfo;-><init>()V

    .line 757500
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 757501
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 757502
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 757503
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 757504
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 757505
    :cond_1
    return-object v1

    .line 757506
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 757507
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 757508
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 757509
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 757510
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v7, :cond_3

    .line 757511
    const-string p0, "accent_images"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 757512
    invoke-static {p1, v3}, LX/2ax;->b(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 757513
    :cond_4
    const-string p0, "background_color"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 757514
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 757515
    :cond_5
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 757516
    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 757517
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 757518
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_6
    move v2, v4

    move v6, v4

    goto :goto_1
.end method
