.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLSticker;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public ac:I

.field public ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:J

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756836
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756392
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 756390
    const/16 v0, 0x49

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 756391
    return-void
.end method

.method private aA()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756387
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756388
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q:Ljava/util/List;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q:Ljava/util/List;

    .line 756389
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aB()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756384
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756385
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R:Ljava/util/List;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R:Ljava/util/List;

    .line 756386
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756381
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756382
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756383
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->v:Z

    return v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756378
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756379
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    .line 756380
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    return-object v0
.end method

.method public final C()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756375
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756376
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756377
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->x:J

    return-wide v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756372
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756373
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->y:Ljava/lang/String;

    .line 756374
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 756368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756363
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756365
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756360
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756361
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 756362
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 756359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 756353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756348
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756349
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 756350
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    return-object v0
.end method

.method public final M()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756345
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756346
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756347
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H:Z

    return v0
.end method

.method public final N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I:Ljava/lang/String;

    .line 756344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J:Ljava/lang/String;

    .line 756341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756399
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756400
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756401
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756396
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756397
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 756398
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    return-object v0
.end method

.method public final R()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->M:Ljava/lang/String;

    .line 756835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756830
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756831
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756832
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756827
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756828
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756829
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 756826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756821
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756822
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 756823
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756818
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756819
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756820
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLSticker;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756815
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U:Lcom/facebook/graphql/model/GraphQLSticker;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756816
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U:Lcom/facebook/graphql/model/GraphQLSticker;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLSticker;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSticker;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 756817
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U:Lcom/facebook/graphql/model/GraphQLSticker;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756812
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756813
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V:Ljava/lang/String;

    .line 756814
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756809
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756810
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 756811
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 64

    .prologue
    .line 756671
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756672
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 756673
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 756674
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 756675
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 756676
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 756677
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 756678
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 756679
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 756680
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->r()LX/0Px;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 756681
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 756682
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->t()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 756683
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->u()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 756684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 756685
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 756686
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 756687
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 756688
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 756689
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 756690
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 756691
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 756692
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 756693
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 756694
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 756695
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 756696
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 756697
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 756698
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 756699
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 756700
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 756701
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 756702
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aA()LX/0Px;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v32

    .line 756703
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aB()LX/0Px;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v33

    .line 756704
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 756705
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 756706
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 756707
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 756708
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 756709
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 756710
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 756711
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 756712
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 756713
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae()LX/0Px;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v43

    .line 756714
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 756715
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 756716
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 756717
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 756718
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 756719
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 756720
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v50

    .line 756721
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 756722
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 756723
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 756724
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq()LX/0Px;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v54

    .line 756725
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 756726
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as()LX/0Px;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v56

    .line 756727
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 756728
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 756729
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 756730
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw()Ljava/lang/String;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v60

    .line 756731
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ax()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 756732
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->az()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 756733
    const/16 v63, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 756734
    const/16 v63, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 756735
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 756736
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 756737
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 756738
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 756739
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 756740
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 756741
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 756742
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 756743
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 756744
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 756745
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 756746
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756747
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756748
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756749
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756750
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756751
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756752
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 756753
    const/16 v3, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 756754
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 756755
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756756
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756757
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756758
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756759
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756760
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756761
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756762
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756763
    const/16 v2, 0x1d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->M()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 756764
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756765
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756766
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756767
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756768
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756769
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756770
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756771
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756772
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756773
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756774
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756775
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756776
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756777
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756778
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756779
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756780
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756781
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756782
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756783
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756784
    const/16 v2, 0x32

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 756785
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756786
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756787
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756788
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756789
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756790
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756791
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756792
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756793
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756794
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756795
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756796
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756797
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756798
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756799
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756800
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756801
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756802
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756803
    const/16 v3, 0x46

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ay()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 756804
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 756805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756806
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 756807
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 756808
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ay()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 756423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 756425
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 756426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 756427
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756428
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756429
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 756430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756431
    if-eqz v2, :cond_1

    .line 756432
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756433
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->g:Ljava/util/List;

    move-object v1, v0

    .line 756434
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 756435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756436
    if-eqz v2, :cond_2

    .line 756437
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756438
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->h:Ljava/util/List;

    move-object v1, v0

    .line 756439
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 756440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756441
    if-eqz v2, :cond_3

    .line 756442
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756443
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 756444
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 756445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 756446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 756447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756448
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 756449
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 756450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 756451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 756452
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756453
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 756454
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 756455
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 756456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 756457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756458
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 756459
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 756460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756461
    if-eqz v2, :cond_7

    .line 756462
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756463
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m:Ljava/util/List;

    move-object v1, v0

    .line 756464
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 756465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 756466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 756467
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756468
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 756469
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->t()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 756470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->t()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756471
    if-eqz v2, :cond_9

    .line 756472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756473
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o:Ljava/util/List;

    move-object v1, v0

    .line 756474
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->az()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 756475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->az()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 756476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->az()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 756477
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756478
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 756479
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 756480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 756482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756483
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756484
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 756485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    .line 756486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 756487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756488
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    .line 756489
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 756490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756491
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 756492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756493
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756494
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 756495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 756496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 756497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756498
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->A:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 756499
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 756500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 756502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756503
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756504
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 756505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 756506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 756507
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756508
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756509
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 756510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 756511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 756512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756513
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 756514
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 756515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 756516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 756517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756518
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 756519
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 756520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 756522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756523
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756524
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 756525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 756526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 756527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756528
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 756529
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 756530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 756531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 756532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756533
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 756534
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 756535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 756537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756538
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 756539
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 756540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 756541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 756542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756543
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 756544
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 756545
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 756547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756548
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->N:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756549
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 756550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 756552
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756553
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756554
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 756555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756556
    if-eqz v2, :cond_1a

    .line 756557
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756558
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap:Ljava/util/List;

    move-object v1, v0

    .line 756559
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 756560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 756561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 756562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756563
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->P:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 756564
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aA()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 756565
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aA()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756566
    if-eqz v2, :cond_1c

    .line 756567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756568
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Q:Ljava/util/List;

    move-object v1, v0

    .line 756569
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aB()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 756570
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aB()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 756571
    if-eqz v2, :cond_1d

    .line 756572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756573
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->R:Ljava/util/List;

    move-object v1, v0

    .line 756574
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 756575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 756576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->V()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 756577
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756578
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->S:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 756579
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 756580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 756582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756583
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->T:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756584
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 756585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 756586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 756587
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756588
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 756589
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 756590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 756591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 756592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756593
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->U:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 756594
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 756595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 756596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 756597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756598
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->W:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 756599
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 756600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 756601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 756602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756603
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 756604
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 756605
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 756606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 756607
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756608
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 756609
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 756610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 756611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 756612
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756613
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 756614
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 756615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 756616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 756617
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756618
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 756619
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 756620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756621
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 756622
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756623
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756624
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 756625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 756627
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756628
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756629
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 756630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 756631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 756632
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756633
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 756634
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ax()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 756635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ax()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 756636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ax()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 756637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756638
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 756639
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 756640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 756642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756643
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756644
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 756645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 756647
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756648
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756649
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 756650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 756651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 756652
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756653
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 756654
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 756655
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 756656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 756657
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756658
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 756659
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 756660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 756661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 756662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756663
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am:Lcom/facebook/graphql/model/GraphQLActor;

    .line 756664
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 756665
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 756666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 756667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    .line 756668
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 756669
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756670
    if-nez v1, :cond_31

    :goto_0
    return-object p0

    :cond_31
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->O()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 756411
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 756412
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q:Z

    .line 756413
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->r:Z

    .line 756414
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s:Z

    .line 756415
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->t:Z

    .line 756416
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->u:Z

    .line 756417
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->v:Z

    .line 756418
    const/16 v0, 0x13

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->x:J

    .line 756419
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->H:Z

    .line 756420
    const/16 v0, 0x32

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac:I

    .line 756421
    return-void
.end method

.method public final aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 756410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->X:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    return-object v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756405
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756406
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 756407
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756402
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756403
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 756404
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Z:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756369
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756370
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 756371
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method public final ae()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab:Ljava/util/List;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab:Ljava/util/List;

    .line 756395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ab:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final af()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756272
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756273
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756274
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ac:I

    return v0
.end method

.method public final ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756269
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756270
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756271
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756266
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756267
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756268
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 756265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->af:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ak()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756221
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756222
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756223
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ah:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final al()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756257
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756258
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 756259
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ai:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final am()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756255
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj:Ljava/lang/String;

    const/16 v1, 0x3a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj:Ljava/lang/String;

    .line 756256
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public final an()Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 756253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ak:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    return-object v0
.end method

.method public final ao()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756278
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756279
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al:Ljava/lang/String;

    const/16 v1, 0x3c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al:Ljava/lang/String;

    .line 756280
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->al:Ljava/lang/String;

    return-object v0
.end method

.method public final ap()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756248
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756249
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am:Lcom/facebook/graphql/model/GraphQLActor;

    .line 756250
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->am:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final aq()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756218
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756219
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an:Ljava/util/List;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an:Ljava/util/List;

    .line 756220
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->an:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ar()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756224
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756225
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 756226
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ao:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    return-object v0
.end method

.method public final as()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap:Ljava/util/List;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap:Ljava/util/List;

    .line 756229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ap:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756230
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756231
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 756232
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aq:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ar:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    return-object v0
.end method

.method public final av()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->as:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aw()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at:Ljava/lang/String;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at:Ljava/lang/String;

    .line 756241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->at:Ljava/lang/String;

    return-object v0
.end method

.method public final ax()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 756244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->au:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    return-object v0
.end method

.method public final ay()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 756247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->av:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    return-object v0
.end method

.method public final az()Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 756307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aw:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 756335
    const v0, -0x196c2640

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756329
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 756330
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 756331
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 756332
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 756333
    const/4 v0, 0x0

    .line 756334
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756326
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756327
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756328
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756323
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756324
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->g:Ljava/util/List;

    .line 756325
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756320
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756321
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->h:Ljava/util/List;

    .line 756322
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756317
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756318
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->i:Ljava/util/List;

    .line 756319
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756314
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756315
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 756316
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756311
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756312
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 756313
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756308
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756309
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 756310
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756275
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756276
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m:Ljava/util/List;

    .line 756277
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756302
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756303
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 756304
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o:Ljava/util/List;

    .line 756301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p:Ljava/lang/String;

    .line 756298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756293
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756294
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756295
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->q:Z

    return v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756290
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756291
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756292
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->r:Z

    return v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756287
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756288
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756289
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->s:Z

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756284
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756285
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756286
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->t:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 756281
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756282
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756283
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->u:Z

    return v0
.end method
