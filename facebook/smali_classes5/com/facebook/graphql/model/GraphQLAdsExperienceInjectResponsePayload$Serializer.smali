.class public final Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744153
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 744154
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744155
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 744156
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 744157
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 744158
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 744159
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 744160
    if-eqz v2, :cond_0

    .line 744161
    const-string p0, "inject_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744162
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 744163
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 744164
    if-eqz v2, :cond_1

    .line 744165
    const-string p0, "viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744166
    invoke-static {v1, v2, p1, p2}, LX/262;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 744167
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 744168
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 744169
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;->a(Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;LX/0nX;LX/0my;)V

    return-void
.end method
