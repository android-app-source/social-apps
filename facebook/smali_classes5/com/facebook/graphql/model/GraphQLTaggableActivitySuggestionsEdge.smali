.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786523
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786524
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 786525
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 786526
    return-void
.end method

.method public constructor <init>(LX/4Z7;)V
    .locals 1

    .prologue
    .line 786586
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 786587
    iget-object v0, p1, LX/4Z7;->b:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    .line 786588
    iget-object v0, p1, LX/4Z7;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->f:Ljava/lang/String;

    .line 786589
    iget-object v0, p1, LX/4Z7;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    .line 786590
    iget-object v0, p1, LX/4Z7;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->h:Ljava/lang/String;

    .line 786591
    iget-object v0, p1, LX/4Z7;->f:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 786592
    iget-object v0, p1, LX/4Z7;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 786593
    iget-object v0, p1, LX/4Z7;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 786594
    iget-boolean v0, p1, LX/4Z7;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->l:Z

    .line 786595
    iget-object v0, p1, LX/4Z7;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786596
    iget-object v0, p1, LX/4Z7;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n:Ljava/lang/String;

    .line 786597
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 786527
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 786529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 786530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 786531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 786532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 786533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 786534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 786535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 786536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 786537
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 786538
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 786539
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 786540
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 786541
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 786542
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 786543
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 786544
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 786545
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 786546
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 786547
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 786548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786549
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 786550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 786552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    .line 786553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 786554
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786555
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    .line 786556
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 786557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 786558
    if-eqz v2, :cond_1

    .line 786559
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786560
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    move-object v1, v0

    .line 786561
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 786562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 786563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 786564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786565
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 786566
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 786567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 786568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 786569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786570
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 786571
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 786572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 786573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 786574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786575
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 786576
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 786577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 786579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    .line 786580
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786581
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786582
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    .line 786585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->e:Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 786519
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 786520
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->l:Z

    .line 786521
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 786522
    const v0, -0x337cd0ca

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786493
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->f:Ljava/lang/String;

    .line 786494
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 786495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    .line 786497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->h:Ljava/lang/String;

    .line 786518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786498
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786499
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 786500
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->i:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 786503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786504
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786505
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 786506
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 786507
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 786508
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 786509
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->l:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786510
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786511
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786512
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n:Ljava/lang/String;

    .line 786515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n:Ljava/lang/String;

    return-object v0
.end method
