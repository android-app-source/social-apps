.class public final Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:I

.field public l:I

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772818
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772761
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772816
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 772817
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772813
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772814
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->i:Ljava/lang/String;

    .line 772815
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->i:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772810
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772811
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m:Ljava/lang/String;

    .line 772812
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 772793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 772795
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 772796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 772797
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 772798
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->j()Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 772799
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 772800
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 772801
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 772802
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 772803
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 772804
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->o()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 772805
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->p()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 772806
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 772807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772808
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 772809
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->j()Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 772785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 772787
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 772788
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 772789
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;

    .line 772790
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772791
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772792
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772784
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 772819
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 772820
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->g:I

    .line 772821
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->h:I

    .line 772822
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->j:I

    .line 772823
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k:I

    .line 772824
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->l:I

    .line 772825
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 772783
    const v0, 0x66c3396d

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->e:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->e:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->e:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    .line 772782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->e:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 772779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772774
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772775
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772776
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->g:I

    return v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772771
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772772
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772773
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->h:I

    return v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772768
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772769
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772770
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->j:I

    return v0
.end method

.method public final o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772765
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772766
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772767
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k:I

    return v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 772762
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 772763
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 772764
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->l:I

    return v0
.end method
