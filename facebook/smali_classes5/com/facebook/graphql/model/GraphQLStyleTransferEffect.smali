.class public final Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStyleTransferEffect$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStyleTransferEffect$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785527
    const-class v0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785528
    const-class v0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 785529
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 785530
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785531
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785532
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n:Ljava/lang/String;

    .line 785533
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785577
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785578
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o:Ljava/lang/String;

    .line 785579
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 785534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 785536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 785537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 785538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 785539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 785540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 785541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 785542
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->s()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 785543
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->t()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 785544
    const/16 v9, 0xb

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 785545
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 785546
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 785547
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 785548
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 785549
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 785550
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 785551
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 785552
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 785553
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 785554
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 785555
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 785556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 785558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 785560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 785562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 785563
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785564
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 785566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 785567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 785568
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 785569
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 785570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 785571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 785572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 785573
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 785574
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785575
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 785522
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 785523
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->g:Z

    .line 785524
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j:Z

    .line 785525
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 785526
    const v0, -0x746516b3

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785519
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785520
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785521
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 785518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785513
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 785514
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 785515
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->g:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785510
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785511
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->h:Ljava/lang/String;

    .line 785512
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 785509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->i:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    return-object v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785504
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 785505
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 785506
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k:Ljava/lang/String;

    .line 785503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785498
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785499
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l:Ljava/lang/String;

    .line 785500
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m:Ljava/lang/String;

    .line 785497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m:Ljava/lang/String;

    return-object v0
.end method
