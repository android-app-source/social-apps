.class public final Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764897
    const-class v0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764898
    const-class v0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764899
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764900
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764901
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764902
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->e:Ljava/lang/String;

    .line 764903
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764904
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->f:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764905
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->f:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->f:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    .line 764906
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->f:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 764907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764908
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 764909
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 764910
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 764911
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->j()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764913
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 764914
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMarketplaceAttachmentStyleInfo;->j()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 764915
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764917
    return-object p0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764918
    const v0, 0x6d176bf1

    return v0
.end method
