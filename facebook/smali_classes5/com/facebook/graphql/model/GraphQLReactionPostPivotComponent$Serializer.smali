.class public final Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 778291
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 778292
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 778319
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 778294
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 778295
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 778296
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 778297
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 778298
    if-eqz v2, :cond_0

    .line 778299
    const-string v3, "component_logical_path"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778300
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778301
    :cond_0
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 778302
    if-eqz v2, :cond_1

    .line 778303
    const-string v2, "component_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778304
    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778305
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 778306
    if-eqz v2, :cond_2

    .line 778307
    const-string v3, "component_tracking_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778308
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778309
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 778310
    if-eqz v2, :cond_3

    .line 778311
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778312
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 778313
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 778314
    if-eqz v2, :cond_4

    .line 778315
    const-string v3, "action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778316
    invoke-static {v1, v2, p1, p2}, LX/4SH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 778317
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 778318
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 778293
    check-cast p1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;->a(Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;LX/0nX;LX/0my;)V

    return-void
.end method
