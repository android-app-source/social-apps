.class public final Lcom/facebook/graphql/model/GraphQLMemeCategory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMemeCategory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMemeCategory$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765980
    const-class v0, Lcom/facebook/graphql/model/GraphQLMemeCategory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765990
    const-class v0, Lcom/facebook/graphql/model/GraphQLMemeCategory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765991
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 765992
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765993
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765994
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->i:Ljava/lang/String;

    .line 765995
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 765996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765997
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 765998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 765999
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 766000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 766001
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 766002
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 766003
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 766004
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 766005
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 766006
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 766007
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 766008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766009
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 765981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 765983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    .line 765984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 765985
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMemeCategory;

    .line 765986
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMemeCategory;->f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    .line 765987
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765988
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765989
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 765967
    const v0, -0x6834a592

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->e:Ljava/lang/String;

    .line 765970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765971
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765972
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    .line 765973
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->f:Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 765974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->g:Ljava/util/List;

    .line 765976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->h:Ljava/lang/String;

    .line 765979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMemeCategory;->h:Ljava/lang/String;

    return-object v0
.end method
