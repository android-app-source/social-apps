.class public final Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public i:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759959
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759958
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759956
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759957
    return-void
.end method

.method private a()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 759953
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759954
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->e:Ljava/util/List;

    .line 759955
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->f:Ljava/lang/String;

    .line 759952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759947
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759948
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->g:Ljava/lang/String;

    .line 759949
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 759913
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759914
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 759915
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759944
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759945
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 759946
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 759941
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759942
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->j:Ljava/util/List;

    .line 759943
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 759925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759926
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 759927
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 759928
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 759929
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->m()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 759930
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 759931
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 759932
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 759933
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 759934
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 759935
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 759936
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 759937
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 759938
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759939
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 759940
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759917
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759918
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->m()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759919
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->m()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 759920
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->m()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;

    .line 759922
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupMallHoistedStoriesActionLink;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 759923
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759924
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759916
    const v0, 0x2a27654

    return v0
.end method
