.class public final Lcom/facebook/graphql/model/GraphQLSearchElectionRace;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionRace$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionRace$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;",
            ">;"
        }
    .end annotation
.end field

.field public f:D

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782468
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782469
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782430
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782431
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 782456
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 782458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 782459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 782460
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 782461
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 782462
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->j()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 782463
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 782464
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 782465
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->m()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 782466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782467
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782453
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782454
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->e:Ljava/util/List;

    .line 782455
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 782445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 782447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 782448
    if-eqz v1, :cond_0

    .line 782449
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;

    .line 782450
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->e:Ljava/util/List;

    .line 782451
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782452
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 782470
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 782471
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->f:D

    .line 782472
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->i:D

    .line 782473
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782444
    const v0, 0x6b2dd872

    return v0
.end method

.method public final j()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782441
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782442
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782443
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->f:D

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782438
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782439
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->g:Ljava/lang/String;

    .line 782440
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782435
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782436
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->h:Ljava/lang/String;

    .line 782437
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782432
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782433
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782434
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->i:D

    return-wide v0
.end method
