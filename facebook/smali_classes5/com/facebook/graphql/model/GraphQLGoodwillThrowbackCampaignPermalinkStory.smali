.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jW;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756066
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 756067
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 756068
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 756069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x5b1d8af8

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 756070
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    .line 756071
    return-void
.end method

.method public constructor <init>(LX/4WY;)V
    .locals 2

    .prologue
    .line 755997
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x5b1d8af8

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 755999
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    .line 756000
    iget-object v0, p1, LX/4WY;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756001
    iget-object v0, p1, LX/4WY;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756002
    iget-wide v0, p1, LX/4WY;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->h:J

    .line 756003
    iget-object v0, p1, LX/4WY;->e:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756004
    iget-object v0, p1, LX/4WY;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->i:Ljava/lang/String;

    .line 756005
    iget-object v0, p1, LX/4WY;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756006
    iget-object v0, p1, LX/4WY;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756007
    iget-object v0, p1, LX/4WY;->i:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    .line 756008
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 756072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 756076
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 756073
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 756074
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 756075
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->h:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 756077
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    if-nez v0, :cond_0

    .line 756078
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    .line 756079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 756080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 756082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 756083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 756084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 756085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 756086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 756087
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 756088
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 756089
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 756090
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 756091
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 756092
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 756093
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 756094
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 756095
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756096
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 756038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 756039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 756040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 756042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 756043
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756044
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 756045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 756046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 756047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 756048
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756049
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 756050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 756052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 756053
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756054
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 756055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 756057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 756058
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756059
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 756060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 756062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 756063
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756064
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 756065
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 756097
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->h:J

    .line 756098
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 756035
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 756036
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->h:J

    .line 756037
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 756031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 756032
    :goto_0
    return-object v0

    .line 756033
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 756034
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 756029
    const v0, 0x5b1d8af8

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 756027
    const/4 v0, 0x0

    move-object v0, v0

    .line 756028
    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756024
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756025
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 756026
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->f:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756021
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756022
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 756023
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756018
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756019
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->i:Ljava/lang/String;

    .line 756020
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756015
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756017
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756012
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756013
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 756014
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 756009
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 756010
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 756011
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method
