.class public final Lcom/facebook/graphql/model/GraphQLCuratedCollection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 747055
    const-class v0, Lcom/facebook/graphql/model/GraphQLCuratedCollection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLCuratedCollection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCuratedCollection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 747056
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 747057
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 747058
    const/16 v0, 0x2aa

    .line 747059
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 747060
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 747061
    const/4 v4, 0x0

    .line 747062
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 747063
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747064
    :goto_0
    move v2, v4

    .line 747065
    if-eqz v1, :cond_0

    .line 747066
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 747067
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 747068
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747069
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 747070
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 747071
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 747072
    move-object v2, v1

    .line 747073
    new-instance v1, Lcom/facebook/graphql/model/GraphQLCuratedCollection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCuratedCollection;-><init>()V

    .line 747074
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 747075
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 747076
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 747077
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 747078
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 747079
    :cond_1
    return-object v1

    .line 747080
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747081
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_7

    .line 747082
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 747083
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 747084
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v9, :cond_3

    .line 747085
    const-string p0, "collection_title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 747086
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 747087
    :cond_4
    const-string p0, "id"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 747088
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 747089
    :cond_5
    const-string p0, "owner"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 747090
    invoke-static {p1, v3}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 747091
    :cond_6
    const-string p0, "url"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 747092
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 747093
    :cond_7
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 747094
    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 747095
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 747096
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 747097
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747098
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_8
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    goto :goto_1
.end method
