.class public final Lcom/facebook/graphql/model/GraphQLSouvenir;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenir$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783216
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenir$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783215
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 783213
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 783214
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783210
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783211
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 783212
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783207
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783208
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->f:Ljava/lang/String;

    .line 783209
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783204
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783205
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->g:Ljava/lang/String;

    .line 783206
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783201
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    .line 783203
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783139
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->i:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783140
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->i:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->i:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 783141
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->i:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783198
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783199
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783200
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783195
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783196
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783197
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->l:Ljava/lang/String;

    .line 783194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenir;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 783172
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783173
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 783174
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 783175
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 783176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->m()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 783177
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->n()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 783178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 783179
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 783180
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 783181
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 783182
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 783183
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 783184
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 783185
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 783186
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 783187
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 783188
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 783189
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 783190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783191
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 783144
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783145
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783146
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 783147
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 783148
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    .line 783149
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenir;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 783150
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->m()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 783151
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->m()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    .line 783152
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->m()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 783153
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    .line 783154
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenir;->h:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    .line 783155
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->n()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 783156
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->n()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 783157
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->n()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 783158
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    .line 783159
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenir;->i:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 783160
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 783161
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783162
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 783163
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    .line 783164
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenir;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783165
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 783166
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783167
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 783168
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    .line 783169
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenir;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 783170
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783171
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783143
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenir;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 783142
    const v0, 0x6c2aa72f

    return v0
.end method
