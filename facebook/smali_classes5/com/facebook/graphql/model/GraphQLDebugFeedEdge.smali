.class public final Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1pT;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLDebugFeedEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLDebugFeedEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:D

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 576701
    const-class v0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 576707
    const-class v0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 576705
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 576706
    return-void
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 576702
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 576703
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 576704
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->j:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 576682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 576683
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 576684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 576685
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 576686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v4, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v4

    .line 576687
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 576688
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 576689
    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->a()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v0, v7, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 576690
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 576691
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 576692
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 576693
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 576694
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 576695
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 576696
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 576697
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 576698
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 576699
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 576700
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->a()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 576674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 576675
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 576676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 576677
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 576678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;

    .line 576679
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 576680
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 576681
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 576671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 576673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 576708
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 576709
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->h:Z

    .line 576710
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->j:Z

    .line 576711
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->l:D

    .line 576712
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 576670
    const v0, -0x57131032

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576667
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576668
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->f:Ljava/lang/String;

    .line 576669
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->g:Ljava/lang/String;

    .line 576666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 576661
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 576662
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 576663
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->h:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576649
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576650
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->i:Ljava/lang/String;

    .line 576651
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576658
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576659
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x6

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 576660
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 576655
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 576656
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 576657
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->l:D

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576652
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 576653
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->m:Ljava/lang/String;

    .line 576654
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDebugFeedEdge;->m:Ljava/lang/String;

    return-object v0
.end method
