.class public final Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 749819
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 749820
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 749792
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 749794
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 749795
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 749796
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 749797
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 749798
    if-eqz v2, :cond_0

    .line 749799
    const-string v3, "amount"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749800
    invoke-static {v1, v2, p1}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 749801
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 749802
    if-eqz v2, :cond_1

    .line 749803
    const-string v3, "charge_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749804
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749805
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 749806
    if-eqz v2, :cond_2

    .line 749807
    const-string v3, "label"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749808
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749809
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 749810
    if-eqz v2, :cond_3

    .line 749811
    const-string v3, "order"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749812
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 749813
    :cond_3
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 749814
    if-eqz v2, :cond_4

    .line 749815
    const-string v2, "fee_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749816
    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749817
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 749818
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 749793
    check-cast p1, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;->a(Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;LX/0nX;LX/0my;)V

    return-void
.end method
