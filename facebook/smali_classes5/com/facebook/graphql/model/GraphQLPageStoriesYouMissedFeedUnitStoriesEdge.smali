.class public final Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771803
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771802
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 771800
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 771801
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 771794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771795
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 771796
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 771797
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 771798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 771782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 771785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 771786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;

    .line 771787
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 771788
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771789
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 771793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 771790
    const v0, -0x4c5f7628

    return v0
.end method
