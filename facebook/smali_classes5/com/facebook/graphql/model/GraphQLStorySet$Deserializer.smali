.class public final Lcom/facebook/graphql/model/GraphQLStorySet$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 784865
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLStorySet$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLStorySet$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 784866
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 784867
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 784868
    const/16 v0, 0xa

    invoke-static {p1, v0}, LX/4TJ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    .line 784869
    new-instance v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;-><init>()V

    .line 784870
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 784871
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 784872
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 784873
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 784874
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 784875
    :cond_0
    return-object v1
.end method
