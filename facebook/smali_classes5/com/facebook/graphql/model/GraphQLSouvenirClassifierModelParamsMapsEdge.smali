.class public final Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783569
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783568
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 783582
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 783583
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    .line 783581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 783584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783585
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->a()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 783586
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 783587
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 783588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783589
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 783571
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783572
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->a()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783573
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->a()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    .line 783574
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->a()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 783575
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;

    .line 783576
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsEdge;->e:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMap;

    .line 783577
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783578
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 783570
    const v0, -0x77367cad

    return v0
.end method
