.class public final Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746895
    const-class v0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746896
    const-class v0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 746897
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 746898
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746899
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746900
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 746901
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 746902
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746903
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 746904
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 746905
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746906
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 746907
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 746908
    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->a()Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 746909
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 746910
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746911
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 746912
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->a()Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 746913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746914
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 746915
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 746916
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 746917
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;

    .line 746918
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 746919
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746920
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 746921
    const v0, -0x1ac68c69

    return v0
.end method
