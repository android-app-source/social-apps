.class public final Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 782753
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 782754
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 782755
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 782756
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 782757
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 782758
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 782759
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782760
    if-eqz v2, :cond_0

    .line 782761
    const-string p0, "category"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782762
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782763
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 782764
    if-eqz v2, :cond_1

    .line 782765
    const-string p0, "node"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782766
    invoke-static {v1, v2, p1, p2}, LX/4So;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 782767
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782768
    if-eqz v2, :cond_2

    .line 782769
    const-string p0, "subtext"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782770
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782771
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 782772
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 782773
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;->a(Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;LX/0nX;LX/0my;)V

    return-void
.end method
