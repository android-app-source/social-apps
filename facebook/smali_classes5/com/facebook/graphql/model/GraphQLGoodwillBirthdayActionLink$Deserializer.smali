.class public final Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 754996
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 754997
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 754995
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 754955
    const/16 v0, 0x249

    .line 754956
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 754957
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 754958
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 754959
    const/4 v2, 0x0

    .line 754960
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 754961
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 754962
    :goto_0
    move v2, v4

    .line 754963
    if-eqz v1, :cond_0

    .line 754964
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 754965
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 754966
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 754967
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 754968
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 754969
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 754970
    move-object v2, v1

    .line 754971
    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayActionLink;-><init>()V

    .line 754972
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 754973
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 754974
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 754975
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 754976
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 754977
    :cond_1
    return-object v1

    .line 754978
    :cond_2
    const-string p0, "link_type"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 754979
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    move-object v7, v2

    move v2, v6

    .line 754980
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_6

    .line 754981
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 754982
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 754983
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v10, :cond_3

    .line 754984
    const-string p0, "title"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 754985
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 754986
    :cond_4
    const-string p0, "url"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 754987
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 754988
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 754989
    :cond_6
    const/4 v10, 0x3

    invoke-virtual {v3, v10}, LX/186;->c(I)V

    .line 754990
    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 754991
    invoke-virtual {v3, v6, v8}, LX/186;->b(II)V

    .line 754992
    if-eqz v2, :cond_7

    .line 754993
    const/4 v2, 0x2

    invoke-virtual {v3, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 754994
    :cond_7
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_8
    move-object v7, v2

    move v8, v4

    move v9, v4

    move v2, v4

    goto :goto_1
.end method
