.class public final Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786696
    const-class v0, Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 786697
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 786695
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 786663
    const/16 v0, 0x7b

    .line 786664
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 786665
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 786666
    const/4 v4, 0x0

    .line 786667
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 786668
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786669
    :goto_0
    move v2, v4

    .line 786670
    if-eqz v1, :cond_0

    .line 786671
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 786672
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 786673
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 786674
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 786675
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 786676
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 786677
    move-object v2, v1

    .line 786678
    new-instance v1, Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTargetingDescriptionSentence;-><init>()V

    .line 786679
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 786680
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 786681
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 786682
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 786683
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 786684
    :cond_1
    return-object v1

    .line 786685
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786686
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_4

    .line 786687
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 786688
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 786689
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v6, :cond_3

    .line 786690
    const-string p0, "text"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 786691
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 786692
    :cond_4
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 786693
    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 786694
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_1
.end method
