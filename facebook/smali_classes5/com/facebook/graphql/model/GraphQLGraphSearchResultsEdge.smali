.class public final Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758926
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758925
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758882
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758883
    return-void
.end method

.method public constructor <init>(LX/4Wi;)V
    .locals 1

    .prologue
    .line 758918
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758919
    iget-object v0, p1, LX/4Wi;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->e:Ljava/lang/String;

    .line 758920
    iget-object v0, p1, LX/4Wi;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->i:Ljava/lang/String;

    .line 758921
    iget-object v0, p1, LX/4Wi;->d:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 758922
    iget-object v0, p1, LX/4Wi;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758923
    iget-object v0, p1, LX/4Wi;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->h:Ljava/lang/String;

    .line 758924
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 758904
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 758906
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 758907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 758908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 758909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 758910
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 758911
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 758912
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 758913
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 758914
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 758915
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 758916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758917
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 758891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 758894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 758895
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 758896
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 758897
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 758898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 758900
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 758901
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758902
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758903
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->e:Ljava/lang/String;

    .line 758929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 758890
    const v0, -0x12e2f383

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 758889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 758886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->h:Ljava/lang/String;

    .line 758881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->i:Ljava/lang/String;

    .line 758878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->i:Ljava/lang/String;

    return-object v0
.end method
