.class public final Lcom/facebook/graphql/model/GraphQLFormattedText$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFormattedText;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 751718
    const-class v0, Lcom/facebook/graphql/model/GraphQLFormattedText;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFormattedText$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFormattedText$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 751719
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 751717
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFormattedText;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 751720
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 751721
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 751722
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 751723
    invoke-virtual {v1, v0, p2, p2}, LX/15i;->a(IIS)S

    move-result p0

    .line 751724
    if-eqz p0, :cond_0

    .line 751725
    const-string p0, "formattype"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751726
    const-class p0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v1, v0, p2, p0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751727
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 751728
    if-eqz p0, :cond_1

    .line 751729
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751730
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751731
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 751732
    if-eqz p0, :cond_2

    .line 751733
    const-string p2, "url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751734
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751735
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 751736
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 751716
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFormattedText;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFormattedText$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFormattedText;LX/0nX;LX/0my;)V

    return-void
.end method
