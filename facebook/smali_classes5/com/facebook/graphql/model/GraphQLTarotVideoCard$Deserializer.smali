.class public final Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786968
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 786969
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 787020
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 786970
    const/16 v0, 0x296

    .line 786971
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 786972
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 786973
    const/4 v4, 0x0

    .line 786974
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 786975
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786976
    :goto_0
    move v2, v4

    .line 786977
    if-eqz v1, :cond_0

    .line 786978
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 786979
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 786980
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 786981
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 786982
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 786983
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 786984
    move-object v2, v1

    .line 786985
    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;-><init>()V

    .line 786986
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 786987
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 786988
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 786989
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 786990
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 786991
    :cond_1
    return-object v1

    .line 786992
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786993
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_a

    .line 786994
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 786995
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 786996
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 786997
    const-string p0, "card_description"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 786998
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 786999
    :cond_4
    const-string p0, "card_title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 787000
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 787001
    :cond_5
    const-string p0, "featured_article"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 787002
    invoke-static {p1, v3}, LX/4Og;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 787003
    :cond_6
    const-string p0, "feedback"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 787004
    invoke-static {p1, v3}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 787005
    :cond_7
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 787006
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 787007
    :cond_8
    const-string p0, "url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 787008
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 787009
    :cond_9
    const-string p0, "video"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 787010
    invoke-static {p1, v3}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 787011
    :cond_a
    const/4 v12, 0x7

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 787012
    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 787013
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 787014
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 787015
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 787016
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 787017
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 787018
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 787019
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_b
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v4

    goto/16 :goto_1
.end method
