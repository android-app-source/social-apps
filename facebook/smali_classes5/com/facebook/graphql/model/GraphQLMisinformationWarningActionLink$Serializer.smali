.class public final Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766523
    const-class v0, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 766524
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766472
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 766474
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 766475
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 766476
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 766477
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766478
    if-eqz v2, :cond_0

    .line 766479
    const-string v3, "alert_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766480
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766481
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766482
    if-eqz v2, :cond_1

    .line 766483
    const-string v3, "cta_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766484
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766485
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766486
    if-eqz v2, :cond_2

    .line 766487
    const-string v3, "dispute_form_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766488
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766489
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766490
    if-eqz v2, :cond_3

    .line 766491
    const-string v3, "dispute_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766492
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766493
    :cond_3
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 766494
    if-eqz v2, :cond_4

    .line 766495
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766496
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766497
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766498
    if-eqz v2, :cond_5

    .line 766499
    const-string v3, "reshare_alert_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766500
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766501
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766502
    if-eqz v2, :cond_6

    .line 766503
    const-string v3, "reshare_alert_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766504
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766505
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766506
    if-eqz v2, :cond_7

    .line 766507
    const-string v3, "subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766508
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766509
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766510
    if-eqz v2, :cond_8

    .line 766511
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766512
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766513
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766514
    if-eqz v2, :cond_9

    .line 766515
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766516
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766517
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766518
    if-eqz v2, :cond_a

    .line 766519
    const-string v3, "actions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766520
    invoke-static {v1, v2, p1, p2}, LX/4Pa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 766521
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 766522
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 766473
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLMisinformationWarningActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
