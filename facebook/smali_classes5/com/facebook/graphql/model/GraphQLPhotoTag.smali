.class public final Lcom/facebook/graphql/model/GraphQLPhotoTag;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTag$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTag$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774352
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTag$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774351
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTag$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774349
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774350
    return-void
.end method

.method public constructor <init>(LX/4Y1;)V
    .locals 2

    .prologue
    .line 774341
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774342
    iget-boolean v0, p1, LX/4Y1;->b:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->e:Z

    .line 774343
    iget-object v0, p1, LX/4Y1;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j:Ljava/lang/String;

    .line 774344
    iget-object v0, p1, LX/4Y1;->d:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 774345
    iget-object v0, p1, LX/4Y1;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->i:Ljava/lang/String;

    .line 774346
    iget-object v0, p1, LX/4Y1;->f:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    .line 774347
    iget-wide v0, p1, LX/4Y1;->g:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->h:J

    .line 774348
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 774327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774328
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 774329
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 774330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 774331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 774332
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 774333
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->a()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 774334
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 774335
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 774336
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 774337
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 774338
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 774339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774340
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 774291
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 774293
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 774294
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 774295
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 774296
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 774297
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 774298
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 774299
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 774300
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 774301
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    .line 774302
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774303
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 774323
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 774324
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->e:Z

    .line 774325
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->h:J

    .line 774326
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 774320
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774321
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774322
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 774319
    const v0, -0x481997d8

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774316
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774317
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 774318
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774313
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774314
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    .line 774315
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->g:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final l()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774310
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774311
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774312
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->h:J

    return-wide v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774307
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774308
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->i:Ljava/lang/String;

    .line 774309
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j:Ljava/lang/String;

    .line 774306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j:Ljava/lang/String;

    return-object v0
.end method
