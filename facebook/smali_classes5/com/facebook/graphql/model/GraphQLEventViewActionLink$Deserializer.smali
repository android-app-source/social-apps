.class public final Lcom/facebook/graphql/model/GraphQLEventViewActionLink$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 750015
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventViewActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventViewActionLink$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLEventViewActionLink$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 750016
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 750017
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 750018
    const/16 v0, 0x96

    .line 750019
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 750020
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 750021
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 750022
    const/4 v2, 0x0

    .line 750023
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 750024
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 750025
    :goto_0
    move v2, v4

    .line 750026
    if-eqz v1, :cond_0

    .line 750027
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 750028
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 750029
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 750030
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 750031
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 750032
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 750033
    move-object v2, v1

    .line 750034
    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventViewActionLink;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLEventViewActionLink;-><init>()V

    .line 750035
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 750036
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 750037
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 750038
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 750039
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 750040
    :cond_1
    return-object v1

    .line 750041
    :cond_2
    const-string p0, "link_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 750042
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    move-object v7, v2

    move v2, v6

    .line 750043
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_7

    .line 750044
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 750045
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 750046
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v11, :cond_3

    .line 750047
    const-string p0, "event"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 750048
    invoke-static {p1, v3}, LX/4M6;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 750049
    :cond_4
    const-string p0, "title"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 750050
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 750051
    :cond_5
    const-string p0, "url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 750052
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 750053
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 750054
    :cond_7
    const/4 v11, 0x4

    invoke-virtual {v3, v11}, LX/186;->c(I)V

    .line 750055
    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 750056
    invoke-virtual {v3, v6, v9}, LX/186;->b(II)V

    .line 750057
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 750058
    if-eqz v2, :cond_8

    .line 750059
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 750060
    :cond_8
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_9
    move-object v7, v2

    move v8, v4

    move v9, v4

    move v10, v4

    move v2, v4

    goto :goto_1
.end method
