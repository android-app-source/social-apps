.class public final Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity$Serializer;
.end annotation


# instance fields
.field public A:D

.field public B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Z

.field public L:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Z

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Z

.field public X:J

.field public Y:Z

.field public Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753859
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753860
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 753861
    const/16 v0, 0x39

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 753862
    return-void
.end method

.method private A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753863
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753864
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753865
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->v:Z

    return v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753866
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753867
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753868
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753869
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753870
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x:Ljava/lang/String;

    .line 753871
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x:Ljava/lang/String;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y:Ljava/lang/String;

    .line 753874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y:Ljava/lang/String;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753875
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753876
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753877
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private F()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753878
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753879
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753880
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->A:D

    return-wide v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753881
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753882
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753883
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753890
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753891
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753892
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753893
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753894
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753895
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753896
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753897
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753898
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private N()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753899
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753900
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753901
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753935
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753936
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753932
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753933
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753934
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K:Z

    return v0
.end method

.method private Q()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 753928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private R()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753923
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753924
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753925
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753920
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753921
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N:Ljava/lang/String;

    .line 753922
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N:Ljava/lang/String;

    return-object v0
.end method

.method private T()Lcom/facebook/graphql/model/GraphQLCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753938
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O:Lcom/facebook/graphql/model/GraphQLCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753939
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O:Lcom/facebook/graphql/model/GraphQLCharity;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753940
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O:Lcom/facebook/graphql/model/GraphQLCharity;

    return-object v0
.end method

.method private U()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753917
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753918
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->P:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->P:Ljava/lang/String;

    .line 753919
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->P:Ljava/lang/String;

    return-object v0
.end method

.method private V()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753914
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753915
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q:Ljava/lang/String;

    .line 753916
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q:Ljava/lang/String;

    return-object v0
.end method

.method private W()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753911
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753912
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753913
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753550
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753551
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->S:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->S:Ljava/lang/String;

    .line 753552
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->S:Ljava/lang/String;

    return-object v0
.end method

.method private Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753908
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753909
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753910
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private Z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 753905
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753906
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753907
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->U:Z

    return v0
.end method

.method private aa()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753856
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753857
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->V:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->V:Ljava/lang/String;

    .line 753858
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->V:Ljava/lang/String;

    return-object v0
.end method

.method private ab()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753902
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753903
    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753904
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W:Z

    return v0
.end method

.method private ac()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753553
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753554
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753555
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->X:J

    return-wide v0
.end method

.method private ad()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753547
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753548
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753549
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y:Z

    return v0
.end method

.method private ae()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 753546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    return-object v0
.end method

.method private af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ag()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753538
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753539
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753540
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    return-object v0
.end method

.method private ah()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753511
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753512
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753513
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753535
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753536
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753537
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753532
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753533
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->e:Ljava/lang/String;

    .line 753534
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753529
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753530
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753531
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->f:Z

    return v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753526
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753527
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753528
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->g:Z

    return v0
.end method

.method private m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753523
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753524
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753525
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->h:Z

    return v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753520
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753521
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753522
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->i:Z

    return v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753517
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753518
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    .line 753519
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753514
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753515
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753516
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753556
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753557
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753558
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753559
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753560
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753561
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753562
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753563
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753564
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753565
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753566
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753567
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753568
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753569
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753570
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753571
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753572
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753573
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q:Z

    return v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753574
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753575
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753576
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753577
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753578
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s:Ljava/lang/String;

    .line 753579
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753580
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753581
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753582
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 43

    .prologue
    .line 753586
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753587
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 753588
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 753589
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 753590
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 753591
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 753592
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 753593
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 753594
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 753595
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 753596
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 753597
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 753598
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 753599
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 753600
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 753601
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 753602
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 753603
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 753604
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 753605
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 753606
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 753607
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 753608
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 753609
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 753610
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 753611
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 753612
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 753613
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 753614
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->S()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 753615
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 753616
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->U()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 753617
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->V()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 753618
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 753619
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->X()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 753620
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 753621
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 753622
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ae()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 753623
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 753624
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ag()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 753625
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 753626
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 753627
    const/16 v42, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 753628
    const/16 v42, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 753629
    const/4 v2, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k()Z

    move-result v42

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753630
    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l()Z

    move-result v42

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753631
    const/4 v2, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m()Z

    move-result v42

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753632
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n()Z

    move-result v42

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 753633
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 753634
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 753635
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 753636
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 753637
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 753638
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 753639
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 753640
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753641
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 753642
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 753643
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 753644
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 753645
    const/16 v2, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753646
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 753647
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 753648
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753649
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753650
    const/16 v3, 0x18

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 753651
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753652
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753653
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753654
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753655
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753656
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753657
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753658
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753659
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753660
    const/16 v2, 0x22

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->P()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753661
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753662
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753663
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753664
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753665
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753666
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753667
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753668
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753669
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753670
    const/16 v2, 0x2d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753671
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753672
    const/16 v2, 0x2f

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753673
    const/16 v3, 0x30

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 753674
    const/16 v2, 0x31

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 753675
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753676
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753677
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753678
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753679
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 753680
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753681
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 753682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753683
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 753684
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753685
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 753686
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753687
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 753688
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 753689
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753690
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 753691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753692
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753693
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 753694
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    .line 753695
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 753696
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753697
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->j:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    .line 753698
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 753699
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753700
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 753701
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753702
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753703
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 753704
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753705
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 753706
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753707
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753708
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 753709
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753710
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 753711
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753712
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753713
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 753714
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753715
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 753716
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753717
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753718
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 753719
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753720
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 753721
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753722
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753723
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 753724
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753725
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 753726
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753727
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753728
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 753729
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753730
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 753731
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753732
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753733
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 753734
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753735
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 753736
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753737
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753738
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ae()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 753739
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ae()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 753740
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ae()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 753741
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753742
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Z:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 753743
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 753744
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753745
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 753746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753747
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->r:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753748
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 753749
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753750
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 753751
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753752
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753753
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 753754
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753755
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 753756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753757
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753758
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 753759
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753760
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 753761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753762
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753763
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 753764
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 753765
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 753766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753767
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->z:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753768
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 753769
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753770
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 753771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753772
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->B:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753773
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 753774
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753775
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 753776
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753777
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->C:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753778
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 753779
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753780
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 753781
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753782
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753783
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 753784
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753785
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 753786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753787
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753788
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 753789
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753790
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 753791
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753792
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753793
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 753794
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753795
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 753796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753797
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753798
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 753799
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753800
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 753801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753802
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753803
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 753804
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753805
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 753806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753807
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753808
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 753809
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753810
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 753811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753812
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 753813
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 753814
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753815
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 753816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753817
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753818
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 753819
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753820
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 753821
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753822
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753823
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 753824
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 753825
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Q()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 753826
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753827
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->L:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 753828
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 753829
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753830
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 753831
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753832
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753833
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ag()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 753834
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ag()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753835
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ag()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 753836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 753837
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->ab:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753838
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753839
    if-nez v1, :cond_1f

    :goto_0
    return-object p0

    :cond_1f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753840
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 753841
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 753842
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->f:Z

    .line 753843
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->g:Z

    .line 753844
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->h:Z

    .line 753845
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->i:Z

    .line 753846
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->q:Z

    .line 753847
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->v:Z

    .line 753848
    const/16 v0, 0x18

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->A:D

    .line 753849
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->K:Z

    .line 753850
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->U:Z

    .line 753851
    const/16 v0, 0x2f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->W:Z

    .line 753852
    const/16 v0, 0x30

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->X:J

    .line 753853
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;->Y:Z

    .line 753854
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 753855
    const v0, -0x4e6785e3

    return v0
.end method
