.class public final Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionStoryAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionStoryAction$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLJobOpening;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

.field public G:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

.field public J:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLReactionUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLProductItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Z

.field public Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLOfferView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

.field public j:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field public t:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLFundraiser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLFundraiser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778813
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778762
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 778763
    const/16 v0, 0x38

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 778764
    return-void
.end method

.method private ak()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 778765
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778766
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B:Ljava/util/List;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B:Ljava/util/List;

    .line 778767
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778768
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778769
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w:Ljava/lang/String;

    .line 778770
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLFundraiser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x:Lcom/facebook/graphql/model/GraphQLFundraiser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x:Lcom/facebook/graphql/model/GraphQLFundraiser;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x:Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x:Lcom/facebook/graphql/model/GraphQLFundraiser;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLFundraiser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y:Lcom/facebook/graphql/model/GraphQLFundraiser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y:Lcom/facebook/graphql/model/GraphQLFundraiser;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y:Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y:Lcom/facebook/graphql/model/GraphQLFundraiser;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 778779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 778780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 778782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLJobOpening;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C:Lcom/facebook/graphql/model/GraphQLJobOpening;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C:Lcom/facebook/graphql/model/GraphQLJobOpening;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLJobOpening;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 778785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C:Lcom/facebook/graphql/model/GraphQLJobOpening;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 778788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778789
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778790
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778791
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 778794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778795
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778796
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778797
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H:Ljava/lang/String;

    .line 778800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778838
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778839
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    .line 778840
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778801
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778802
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J:Ljava/lang/String;

    .line 778803
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778835
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778836
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778837
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778833
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L:Ljava/lang/String;

    .line 778834
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M:Ljava/lang/String;

    .line 778831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 778828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778823
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778824
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778825
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778841
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778842
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P:Ljava/lang/String;

    .line 778843
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778820
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778821
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q:Ljava/lang/String;

    .line 778822
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final U()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 778817
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778818
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R:Ljava/util/List;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R:Ljava/util/List;

    .line 778819
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLReactionUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778814
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778815
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 778816
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLProductItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778810
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T:Lcom/facebook/graphql/model/GraphQLProductItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778811
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T:Lcom/facebook/graphql/model/GraphQLProductItem;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 778812
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T:Lcom/facebook/graphql/model/GraphQLProductItem;

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778807
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778808
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U:Ljava/lang/String;

    .line 778809
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778804
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778805
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V:Ljava/lang/String;

    .line 778806
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778646
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778647
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W:Lcom/facebook/graphql/model/GraphQLStory;

    .line 778648
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 50

    .prologue
    .line 778649
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 778651
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 778652
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 778653
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 778654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 778655
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 778656
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 778657
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 778658
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 778659
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 778660
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 778661
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 778662
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 778663
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 778664
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 778665
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 778666
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 778667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 778668
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ak()LX/0Px;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v20

    .line 778669
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 778670
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 778671
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 778672
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 778673
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 778674
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 778675
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 778676
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 778677
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 778678
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 778679
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 778680
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 778681
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 778682
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U()LX/0Px;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v34

    .line 778683
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 778684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 778685
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->X()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 778686
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 778687
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 778688
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 778689
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 778690
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 778691
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 778692
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 778693
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 778694
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ah()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 778695
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 778696
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 778697
    const/16 v49, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 778698
    const/16 v49, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 778699
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 778700
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 778701
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 778702
    const/4 v3, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778703
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 778704
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 778705
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 778706
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 778707
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 778708
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 778709
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 778710
    const/16 v3, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778711
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 778712
    const/16 v3, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778713
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 778714
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 778715
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 778716
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778717
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778718
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778719
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778720
    const/16 v3, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778721
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778722
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778723
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778724
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778725
    const/16 v3, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778726
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778727
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778728
    const/16 v3, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778729
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778730
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778731
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778732
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778733
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778734
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778735
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778736
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778737
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778738
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778739
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778740
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778741
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778742
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778743
    const/16 v2, 0x2d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 778744
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778745
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778746
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778747
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778748
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778749
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778750
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778751
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778752
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 778753
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778754
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 778755
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 778756
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    goto/16 :goto_1

    .line 778757
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v2

    goto/16 :goto_2

    .line 778758
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v2

    goto/16 :goto_3

    .line 778759
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 778760
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v2

    goto/16 :goto_5

    .line 778761
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v2

    goto/16 :goto_6
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 778440
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 778444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778445
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778446
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 778447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 778449
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778450
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778451
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 778452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 778453
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 778454
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778455
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 778456
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 778457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 778458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 778459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778460
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 778461
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 778462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 778464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778465
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778466
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 778467
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    .line 778468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 778469
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778470
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    .line 778471
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 778472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 778473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 778474
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778475
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 778476
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 778477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 778478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 778479
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778480
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 778481
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 778482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 778484
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778485
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778486
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 778487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 778489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778490
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778491
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 778492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 778493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 778494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778495
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v:Lcom/facebook/graphql/model/GraphQLUser;

    .line 778496
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 778497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 778499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778500
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x:Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778501
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 778502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 778504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778505
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y:Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 778506
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 778507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 778508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 778509
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778510
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 778511
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ak()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 778512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ak()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 778513
    if-eqz v2, :cond_e

    .line 778514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778515
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B:Ljava/util/List;

    move-object v1, v0

    .line 778516
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 778517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 778518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 778519
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778520
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 778521
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 778522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 778523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 778524
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778525
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 778526
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 778527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 778528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 778529
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778530
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778531
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ah()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 778532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ah()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 778533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ah()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 778534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778535
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae:Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 778536
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 778537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 778538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 778539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778540
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778541
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 778542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 778543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 778544
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778545
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K:Lcom/facebook/graphql/model/GraphQLPage;

    .line 778546
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 778547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 778548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 778549
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778550
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 778551
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 778552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 778554
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778555
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778556
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 778557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 778558
    if-eqz v2, :cond_17

    .line 778559
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778560
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R:Ljava/util/List;

    move-object v1, v0

    .line 778561
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 778562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 778563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 778564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778565
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S:Lcom/facebook/graphql/model/GraphQLReactionUnit;

    .line 778566
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 778567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 778568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 778569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778570
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 778571
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 778572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    .line 778573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 778574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778575
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    .line 778576
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 778577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 778578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 778579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778580
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W:Lcom/facebook/graphql/model/GraphQLStory;

    .line 778581
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 778582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 778583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 778584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778585
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z:Lcom/facebook/graphql/model/GraphQLNode;

    .line 778586
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 778587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    .line 778588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 778589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778590
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    .line 778591
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 778592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 778593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 778594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778595
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 778596
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778597
    if-nez v1, :cond_1f

    :goto_0
    return-object p0

    :cond_1f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778405
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 778406
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 778407
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 778408
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 778409
    const/4 v0, 0x0

    .line 778410
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 778436
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 778437
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l:Z

    .line 778438
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->X:Z

    .line 778439
    return-void
.end method

.method public final aa()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 778433
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 778434
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 778435
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->X:Z

    return v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y:Ljava/lang/String;

    .line 778432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778643
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778644
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z:Lcom/facebook/graphql/model/GraphQLNode;

    .line 778645
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa:Ljava/lang/String;

    .line 778429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778424
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778425
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab:Ljava/lang/String;

    const/16 v1, 0x31

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab:Ljava/lang/String;

    .line 778426
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 778423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    return-object v0
.end method

.method public final ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778399
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778400
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    .line 778401
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad:Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLOfferView;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778402
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae:Lcom/facebook/graphql/model/GraphQLOfferView;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778403
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae:Lcom/facebook/graphql/model/GraphQLOfferView;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLOfferView;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOfferView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae:Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 778404
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae:Lcom/facebook/graphql/model/GraphQLOfferView;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778396
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778397
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    .line 778398
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af:Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778411
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778412
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    .line 778413
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag:Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 778414
    const v0, 0x4735e982

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778415
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778416
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778417
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 778600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->h:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778602
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 778603
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 778606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778607
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778608
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 778609
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778610
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 778611
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 778612
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m:Lcom/facebook/graphql/model/GraphQLComment;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 778615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778616
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778617
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n:Ljava/lang/String;

    .line 778618
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o:Ljava/lang/String;

    .line 778621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778622
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778623
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 778624
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778625
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778626
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 778627
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778628
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778629
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r:Ljava/lang/String;

    .line 778630
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 778633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t:Lcom/facebook/graphql/model/GraphQLEvent;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u:Lcom/facebook/graphql/model/GraphQLEvent;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 778639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778640
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778641
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v:Lcom/facebook/graphql/model/GraphQLUser;

    .line 778642
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method
