.class public final Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public C:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Z

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762984
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762933
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 762934
    const/16 v0, 0x2f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 762935
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762936
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762937
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 762938
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762939
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762940
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x:Ljava/lang/String;

    .line 762941
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x:Ljava/lang/String;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762942
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762943
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->y:Ljava/lang/String;

    .line 762944
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->y:Ljava/lang/String;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 762945
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762946
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 762947
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762948
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762949
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A:Ljava/lang/String;

    .line 762950
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A:Ljava/lang/String;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 762951
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->B:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762952
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->B:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->B:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 762953
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->B:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762954
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762955
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 762956
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762957
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762958
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D:Ljava/lang/String;

    .line 762959
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D:Ljava/lang/String;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762960
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762961
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->E:Ljava/lang/String;

    .line 762962
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->E:Ljava/lang/String;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762963
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762964
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F:Lcom/facebook/graphql/model/GraphQLPage;

    .line 762965
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762997
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762998
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G:Ljava/lang/String;

    .line 762999
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G:Ljava/lang/String;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762966
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762967
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->H:Ljava/lang/String;

    .line 762968
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->H:Ljava/lang/String;

    return-object v0
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762969
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762970
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->I:Ljava/lang/String;

    .line 762971
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->I:Ljava/lang/String;

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762972
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762973
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J:Ljava/lang/String;

    .line 762974
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J:Ljava/lang/String;

    return-object v0
.end method

.method private O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762975
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762976
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->K:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->K:Ljava/lang/String;

    .line 762977
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->K:Ljava/lang/String;

    return-object v0
.end method

.method private P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762978
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762979
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->L:Ljava/lang/String;

    .line 762980
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->L:Ljava/lang/String;

    return-object v0
.end method

.method private Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762981
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762982
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->M:Ljava/lang/String;

    .line 762983
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->M:Ljava/lang/String;

    return-object v0
.end method

.method private R()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762924
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762925
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->N:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->N:Ljava/lang/String;

    .line 762926
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->N:Ljava/lang/String;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762985
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762986
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->O:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->O:Ljava/lang/String;

    .line 762987
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->O:Ljava/lang/String;

    return-object v0
.end method

.method private T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762989
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->P:Ljava/lang/String;

    .line 762990
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->P:Ljava/lang/String;

    return-object v0
.end method

.method private U()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 762991
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 762992
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 762993
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->Q:Z

    return v0
.end method

.method private V()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->R:Ljava/lang/String;

    .line 762996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->R:Ljava/lang/String;

    return-object v0
.end method

.method private W()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->S:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->S:Ljava/lang/String;

    .line 762929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->S:Ljava/lang/String;

    return-object v0
.end method

.method private X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762930
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->T:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762931
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->T:Ljava/lang/String;

    .line 762932
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->T:Ljava/lang/String;

    return-object v0
.end method

.method private Y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->U:Ljava/lang/String;

    .line 762728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->U:Ljava/lang/String;

    return-object v0
.end method

.method private Z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762729
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762730
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->V:Ljava/lang/String;

    .line 762731
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->V:Ljava/lang/String;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762732
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762733
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->e:Ljava/lang/String;

    .line 762734
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->e:Ljava/lang/String;

    return-object v0
.end method

.method private aa()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762735
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762736
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->W:Ljava/lang/String;

    .line 762737
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->W:Ljava/lang/String;

    return-object v0
.end method

.method private ab()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762738
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->X:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762739
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->X:Ljava/lang/String;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->X:Ljava/lang/String;

    .line 762740
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->X:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762741
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762742
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->f:Ljava/lang/String;

    .line 762743
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 762744
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 762745
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 762746
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->g:I

    return v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 762747
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 762748
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 762749
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->h:I

    return v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->i:Ljava/lang/String;

    .line 762752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762753
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762754
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->j:Ljava/lang/String;

    .line 762755
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 762756
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762757
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k:Ljava/util/List;

    .line 762758
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->l:Ljava/lang/String;

    .line 762761
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762762
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762763
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->m:Ljava/lang/String;

    .line 762764
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762765
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762766
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->n:Ljava/lang/String;

    .line 762767
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762768
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762769
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o:Ljava/lang/String;

    .line 762770
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->p:Ljava/lang/String;

    .line 762773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->p:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->q:Ljava/lang/String;

    .line 762776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->q:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->r:Ljava/lang/String;

    .line 762779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->r:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->s:Ljava/lang/String;

    .line 762782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->s:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 762785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->u:Ljava/lang/String;

    .line 762788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->u:Ljava/lang/String;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762789
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762790
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 762791
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 44

    .prologue
    .line 762792
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762793
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 762794
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->j()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 762795
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->m()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 762796
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 762797
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 762798
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 762799
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->q()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 762800
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->r()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 762801
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->s()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 762802
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 762803
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->u()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 762804
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 762805
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 762806
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 762807
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->y()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 762808
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 762809
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 762810
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->B()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 762811
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 762812
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->E()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 762813
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 762814
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->H()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 762815
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->I()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 762816
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 762817
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->K()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 762818
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->L()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 762819
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->M()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 762820
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->N()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 762821
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->O()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 762822
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->P()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 762823
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->Q()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 762824
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->R()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 762825
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->S()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 762826
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->T()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 762827
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->V()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 762828
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->W()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 762829
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->X()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 762830
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->Y()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 762831
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->Z()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 762832
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->aa()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    .line 762833
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->ab()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 762834
    const/16 v43, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 762835
    const/16 v43, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 762836
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 762837
    const/4 v2, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k()I

    move-result v3

    const/16 v43, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v3, v1}, LX/186;->a(III)V

    .line 762838
    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->l()I

    move-result v3

    const/16 v43, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v3, v1}, LX/186;->a(III)V

    .line 762839
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 762840
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 762841
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 762842
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 762843
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 762844
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 762845
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 762846
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 762847
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 762848
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 762849
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 762850
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 762851
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762852
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762853
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762854
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762855
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762856
    const/16 v3, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 762857
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762858
    const/16 v3, 0x17

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 762859
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762860
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762861
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762862
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762863
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762864
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762865
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762866
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762867
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762868
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762869
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762870
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762871
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762872
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762873
    const/16 v2, 0x26

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->U()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 762874
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762875
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762876
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762877
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762878
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762879
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762880
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 762881
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762882
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 762883
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->D()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    goto/16 :goto_0

    .line 762884
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 762885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762886
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 762887
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->o()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 762888
    if-eqz v1, :cond_6

    .line 762889
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762890
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->k:Ljava/util/List;

    move-object v1, v0

    .line 762891
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 762892
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 762893
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->x()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 762894
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762895
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->t:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 762896
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 762897
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 762898
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 762899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762900
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->v:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 762901
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 762902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 762903
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->A()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 762904
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762905
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->w:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 762906
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 762907
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 762908
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 762909
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762910
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 762911
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 762912
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 762913
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 762914
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    .line 762915
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->F:Lcom/facebook/graphql/model/GraphQLPage;

    .line 762916
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762917
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 762918
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 762919
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->g:I

    .line 762920
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->h:I

    .line 762921
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;->Q:Z

    .line 762922
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 762923
    const v0, 0x46a1c4a4

    return v0
.end method
