.class public final Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758232
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 758231
    const-class v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758229
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758230
    return-void
.end method

.method public constructor <init>(LX/4Wd;)V
    .locals 1

    .prologue
    .line 758218
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 758219
    iget-object v0, p1, LX/4Wd;->b:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758220
    iget-object v0, p1, LX/4Wd;->c:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    .line 758221
    iget-object v0, p1, LX/4Wd;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->g:Ljava/lang/String;

    .line 758222
    iget-object v0, p1, LX/4Wd;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    .line 758223
    iget-object v0, p1, LX/4Wd;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->i:Ljava/lang/String;

    .line 758224
    iget-object v0, p1, LX/4Wd;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j:Ljava/lang/String;

    .line 758225
    iget-object v0, p1, LX/4Wd;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k:Ljava/lang/String;

    .line 758226
    iget-object v0, p1, LX/4Wd;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l:Ljava/lang/String;

    .line 758227
    iget-object v0, p1, LX/4Wd;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m:Ljava/lang/String;

    .line 758228
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 758196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758197
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 758198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 758199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 758200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 758201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 758202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 758203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 758204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 758205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 758206
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 758207
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 758208
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 758209
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 758210
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 758211
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 758212
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 758213
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 758214
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 758215
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 758216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758217
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 758178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 758179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 758182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    .line 758183
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758184
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 758185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    .line 758186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 758187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    .line 758188
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    .line 758189
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 758190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    .line 758191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 758192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    .line 758193
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    .line 758194
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 758195
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 758176
    const v0, -0x55db0bf6

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 758235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758173
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758174
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    .line 758175
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758170
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758171
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->g:Ljava/lang/String;

    .line 758172
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758167
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758168
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    .line 758169
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->h:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758164
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758165
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->i:Ljava/lang/String;

    .line 758166
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758161
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758162
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j:Ljava/lang/String;

    .line 758163
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758158
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758159
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k:Ljava/lang/String;

    .line 758160
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l:Ljava/lang/String;

    .line 758157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 758152
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 758153
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m:Ljava/lang/String;

    .line 758154
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m:Ljava/lang/String;

    return-object v0
.end method
