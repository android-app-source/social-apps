.class public final Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 785136
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 785137
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 785139
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 785140
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 785141
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 785142
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 785143
    invoke-virtual {v1, v0, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 785144
    if-eqz v2, :cond_0

    .line 785145
    const-string v3, "allow_write_in_response"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785146
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 785147
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 785148
    if-eqz v2, :cond_1

    .line 785149
    const-string v3, "body"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785150
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 785151
    :cond_1
    invoke-virtual {v1, v0, v5, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 785152
    if-eqz v2, :cond_2

    .line 785153
    const-string v2, "custom_question_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785154
    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785155
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 785156
    if-eqz v2, :cond_3

    .line 785157
    const-string v3, "is_required"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785158
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 785159
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 785160
    if-eqz v2, :cond_4

    .line 785161
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785162
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 785163
    :cond_4
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 785164
    if-eqz v2, :cond_5

    .line 785165
    const-string v2, "question_class"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785166
    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785167
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 785168
    if-eqz v2, :cond_6

    .line 785169
    const-string v3, "question_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785170
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785171
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 785172
    if-eqz v2, :cond_7

    .line 785173
    const-string v3, "response_options"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785174
    invoke-static {v1, v2, p1, p2}, LX/4TS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 785175
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 785176
    if-eqz v2, :cond_8

    .line 785177
    const-string v3, "subquestion_labels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785178
    invoke-static {v1, v2, p1, p2}, LX/2aE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 785179
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 785180
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 785138
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;->a(Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;LX/0nX;LX/0my;)V

    return-void
.end method
