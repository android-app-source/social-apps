.class public final Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775677
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775676
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 775674
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775675
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->e:Ljava/lang/String;

    .line 775673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775668
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775669
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->f:Ljava/lang/String;

    .line 775670
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 775656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->g:Ljava/util/List;

    .line 775658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775665
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775666
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 775667
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->i:Ljava/lang/String;

    .line 775680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775662
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775663
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->j:Ljava/lang/String;

    .line 775664
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 775661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->m:Ljava/lang/String;

    .line 775652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 775628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775629
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 775630
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 775631
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 775632
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 775633
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 775634
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 775635
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->o()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 775636
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 775637
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 775638
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 775639
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 775640
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 775641
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 775642
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 775643
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 775644
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 775645
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 775646
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 775647
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 775648
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775649
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775610
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775612
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 775613
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;

    .line 775615
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 775616
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->o()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 775617
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->o()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 775618
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->o()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 775619
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;

    .line 775620
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->k:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 775621
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 775622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 775623
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 775624
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;

    .line 775625
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceAttachmentStyleInfo;->l:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775626
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775627
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775609
    const v0, -0x3659e4dc    # -1360740.5f

    return v0
.end method
