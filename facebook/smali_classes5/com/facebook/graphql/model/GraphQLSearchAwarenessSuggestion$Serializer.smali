.class public final Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 782029
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 782030
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 782031
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 782032
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 782033
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x4

    const/4 p0, 0x2

    .line 782034
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 782035
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782036
    if-eqz v2, :cond_0

    .line 782037
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782038
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782039
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 782040
    if-eqz v2, :cond_1

    .line 782041
    const-string v2, "keywords_suggestions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782042
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 782043
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782044
    if-eqz v2, :cond_2

    .line 782045
    const-string v3, "suggestion_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782046
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782047
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, LX/15i;->a(IIS)S

    move-result v2

    .line 782048
    if-eqz v2, :cond_3

    .line 782049
    const-string v2, "suggestion_template"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782050
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v1, v0, p2, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782051
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782052
    if-eqz v2, :cond_4

    .line 782053
    const-string v3, "suggestion_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782054
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782055
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782056
    if-eqz v2, :cond_5

    .line 782057
    const-string v3, "test_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782058
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782059
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 782060
    if-eqz v2, :cond_6

    .line 782061
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 782062
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 782063
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 782064
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 782065
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion$Serializer;->a(Lcom/facebook/graphql/model/GraphQLSearchAwarenessSuggestion;LX/0nX;LX/0my;)V

    return-void
.end method
