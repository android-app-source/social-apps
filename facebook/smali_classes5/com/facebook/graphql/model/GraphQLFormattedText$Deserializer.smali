.class public final Lcom/facebook/graphql/model/GraphQLFormattedText$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 751673
    const-class v0, Lcom/facebook/graphql/model/GraphQLFormattedText;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFormattedText$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFormattedText$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 751674
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 751675
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 751676
    const/16 v0, 0x2ac

    .line 751677
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 751678
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 751679
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 751680
    const/4 v2, 0x0

    .line 751681
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 751682
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 751683
    :goto_0
    move v2, v4

    .line 751684
    if-eqz v1, :cond_0

    .line 751685
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 751686
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 751687
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 751688
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 751689
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 751690
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 751691
    move-object v2, v1

    .line 751692
    new-instance v1, Lcom/facebook/graphql/model/GraphQLFormattedText;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFormattedText;-><init>()V

    .line 751693
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 751694
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 751695
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 751696
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 751697
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 751698
    :cond_1
    return-object v1

    .line 751699
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_6

    .line 751700
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 751701
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 751702
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 751703
    const-string p0, "formattype"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 751704
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v2

    move-object v9, v2

    move v2, v6

    goto :goto_1

    .line 751705
    :cond_3
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 751706
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 751707
    :cond_4
    const-string p0, "url"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 751708
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 751709
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 751710
    :cond_6
    const/4 v10, 0x3

    invoke-virtual {v3, v10}, LX/186;->c(I)V

    .line 751711
    if-eqz v2, :cond_7

    .line 751712
    invoke-virtual {v3, v4, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 751713
    :cond_7
    invoke-virtual {v3, v6, v8}, LX/186;->b(II)V

    .line 751714
    const/4 v2, 0x2

    invoke-virtual {v3, v2, v7}, LX/186;->b(II)V

    .line 751715
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_8
    move v7, v4

    move v8, v4

    move-object v9, v2

    move v2, v4

    goto :goto_1
.end method
