.class public final Lcom/facebook/graphql/model/GraphQLSearchElectionsData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionsData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionsData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782565
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782568
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782566
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782567
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782559
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782560
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->e:Ljava/lang/String;

    .line 782561
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782562
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782563
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 782564
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782556
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782557
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->g:Ljava/lang/String;

    .line 782558
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 782546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782547
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 782548
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->k()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 782549
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 782550
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 782551
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 782552
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 782553
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 782554
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782555
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 782538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782539
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->k()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 782540
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->k()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 782541
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->k()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 782542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;

    .line 782543
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->f:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 782544
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782545
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782537
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionsData;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782536
    const v0, -0x3aef6a4

    return v0
.end method
