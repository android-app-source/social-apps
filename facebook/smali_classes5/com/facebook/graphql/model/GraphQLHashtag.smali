.class public final Lcom/facebook/graphql/model/GraphQLHashtag;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLHashtag$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761417
    const-class v0, Lcom/facebook/graphql/model/GraphQLHashtag$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 761418
    const-class v0, Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 761419
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 761420
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->v:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->v:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->v:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 761423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->v:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761424
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761425
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->w:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->w:Ljava/lang/String;

    .line 761426
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->w:Ljava/lang/String;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->x:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->x:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->x:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->y:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->y:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->y:Lcom/facebook/graphql/model/GraphQLNode;

    .line 761432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->y:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761433
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761434
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761435
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761436
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761437
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 761438
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    return-object v0
.end method

.method private G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761439
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761440
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->B:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->B:Ljava/lang/String;

    .line 761441
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->B:Ljava/lang/String;

    return-object v0
.end method

.method private H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761442
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761443
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->C:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->C:Ljava/lang/String;

    .line 761444
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->C:Ljava/lang/String;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761445
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->D:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761446
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->D:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761447
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->D:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761448
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->E:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761449
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->E:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761450
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->E:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 761451
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761452
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->e:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->e:Ljava/util/List;

    .line 761453
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761454
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761455
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761456
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->g:Ljava/lang/String;

    .line 761459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761414
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761415
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761416
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->j:Ljava/lang/String;

    .line 761225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 761229
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761230
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->k:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->k:Ljava/util/List;

    .line 761231
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761236
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761237
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761238
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761239
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761240
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761241
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761242
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761243
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761244
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->q:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761245
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->q:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->q:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 761246
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->q:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761247
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761248
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761249
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 761250
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 761251
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 761252
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->s:Z

    return v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761253
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761254
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->t:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->t:Ljava/lang/String;

    .line 761255
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->t:Ljava/lang/String;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 761257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 761258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 29

    .prologue
    .line 761259
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761260
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->j()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 761261
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 761262
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 761263
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 761264
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 761265
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 761266
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->p()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 761267
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 761268
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 761269
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 761270
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 761271
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 761272
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->v()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 761273
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 761274
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->y()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 761275
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 761276
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->A()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 761277
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->B()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 761278
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 761279
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->D()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 761280
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 761281
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->F()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 761282
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->G()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 761283
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->H()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 761284
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 761285
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 761286
    const/16 v28, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 761287
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 761288
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 761289
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 761290
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 761291
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 761292
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 761293
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 761294
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 761295
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 761296
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 761297
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 761298
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 761299
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 761300
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 761301
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 761302
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761303
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761304
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761305
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761306
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761307
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761308
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761309
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761310
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761311
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761312
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761313
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 761314
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761315
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 761316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 761317
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 761318
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761319
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 761320
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761321
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761322
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 761323
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761324
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 761325
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761326
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761327
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 761328
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761329
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 761330
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761331
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761332
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 761333
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761334
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 761335
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761336
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761337
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 761338
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761339
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 761340
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761341
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761342
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 761343
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761344
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 761345
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761346
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761347
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 761348
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761349
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 761350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761351
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761352
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 761353
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761354
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 761355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761356
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761357
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 761358
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761359
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 761360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761361
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761362
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->v()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 761363
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->v()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 761364
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->v()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 761365
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761366
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->q:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 761367
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 761368
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761369
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 761370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761371
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761372
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 761373
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761374
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 761375
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761376
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761377
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 761378
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 761379
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 761380
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761381
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 761382
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->A()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 761383
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->A()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 761384
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->A()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 761385
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761386
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->v:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 761387
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 761388
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761389
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 761390
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761391
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761392
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->D()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 761393
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->D()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 761394
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->D()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 761395
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761396
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->y:Lcom/facebook/graphql/model/GraphQLNode;

    .line 761397
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 761398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 761399
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 761400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761401
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761402
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->F()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 761403
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->F()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 761404
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->F()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 761405
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLHashtag;

    .line 761406
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLHashtag;->A:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 761407
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 761408
    if-nez v1, :cond_12

    :goto_0
    return-object p0

    :cond_12
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 761409
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLHashtag;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 761410
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 761411
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLHashtag;->s:Z

    .line 761412
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 761413
    const v0, -0x7333ac54

    return v0
.end method
