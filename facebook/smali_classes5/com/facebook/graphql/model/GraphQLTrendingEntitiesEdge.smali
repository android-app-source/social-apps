.class public final Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788683
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788682
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 788680
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 788681
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788677
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788678
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 788679
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788674
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788675
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->f:Ljava/lang/String;

    .line 788676
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 788673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788642
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788643
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->h:Ljava/lang/String;

    .line 788644
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 788659
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788660
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 788661
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 788662
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 788663
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 788664
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 788665
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 788666
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 788667
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 788668
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 788669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788670
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 788646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788647
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 788648
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 788649
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 788650
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;

    .line 788651
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 788652
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 788653
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 788654
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 788655
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;

    .line 788656
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 788657
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788658
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 788645
    const v0, -0x30b850fd

    return v0
.end method
