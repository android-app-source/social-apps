.class public final Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 775199
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 775200
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775201
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 775202
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 775203
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/4RQ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 775204
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 775205
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;LX/0nX;LX/0my;)V

    return-void
.end method
