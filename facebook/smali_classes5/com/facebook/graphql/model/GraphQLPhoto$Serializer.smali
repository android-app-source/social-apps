.class public final Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPhoto;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 774188
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 774189
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 774187
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPhoto;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 774190
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 774191
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 774192
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 774186
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;->a(Lcom/facebook/graphql/model/GraphQLPhoto;LX/0nX;LX/0my;)V

    return-void
.end method
