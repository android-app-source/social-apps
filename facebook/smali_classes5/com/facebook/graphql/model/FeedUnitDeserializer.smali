.class public final Lcom/facebook/graphql/model/FeedUnitDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 742947
    const-class v0, Lcom/facebook/graphql/model/FeedUnit;

    new-instance v1, Lcom/facebook/graphql/model/FeedUnitDeserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/FeedUnitDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 742948
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 742949
    const-class v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    .line 742950
    return-void
.end method

.method private static a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 2

    .prologue
    .line 742938
    :cond_0
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_3

    .line 742939
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 742940
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 742941
    const-string v1, "__type__"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "__typename"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742942
    :cond_1
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 742943
    :goto_0
    return-object v0

    .line 742944
    :cond_2
    :goto_1
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_0

    .line 742945
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 742946
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 3

    .prologue
    .line 742853
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 742854
    invoke-static {p1}, Lcom/facebook/graphql/model/FeedUnitDeserializer;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 742855
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 742856
    if-nez v0, :cond_0

    .line 742857
    const-string v0, "Story"

    .line 742858
    :cond_0
    new-instance v1, LX/2aC;

    invoke-direct {v1, p1}, LX/2aC;-><init>(LX/15w;)V

    .line 742859
    invoke-static {v0}, LX/23C;->a(Ljava/lang/String;)S

    move-result v2

    .line 742860
    sparse-switch v2, :sswitch_data_0

    .line 742861
    const-string v2, "UnknownFeedUnit"

    invoke-static {v2}, LX/23C;->a(Ljava/lang/String;)S

    move-result v2

    invoke-static {v1, v2}, LX/4U8;->a(LX/15w;S)LX/15i;

    move-result-object v2

    :goto_0
    move-object v0, v2

    .line 742862
    check-cast v0, LX/15i;

    .line 742863
    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Object;)V

    .line 742864
    invoke-static {}, LX/0sR;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742865
    invoke-static {p1}, LX/0sR;->b(Ljava/lang/Object;)LX/49J;

    move-result-object v1

    .line 742866
    if-eqz v1, :cond_1

    .line 742867
    invoke-virtual {p1}, LX/15w;->j()LX/12V;

    move-result-object v2

    invoke-static {v2}, LX/0sR;->a(LX/12V;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/49J;->a(Ljava/lang/String;)V

    .line 742868
    invoke-static {v0, v1}, LX/0sR;->a(Ljava/lang/Object;LX/49J;)Z

    .line 742869
    :cond_1
    invoke-static {v0}, LX/4XL;->a(LX/15i;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0

    .line 742870
    :sswitch_0
    invoke-static {v1, v2}, LX/4Kb;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742871
    :sswitch_1
    invoke-static {v1, v2}, LX/4Ku;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742872
    :sswitch_2
    invoke-static {v1, v2}, LX/4L8;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742873
    :sswitch_3
    invoke-static {v1, v2}, LX/4LD;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742874
    :sswitch_4
    invoke-static {v1, v2}, LX/4LG;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742875
    :sswitch_5
    invoke-static {v1, v2}, LX/4LR;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742876
    :sswitch_6
    invoke-static {v1, v2}, LX/4Lb;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742877
    :sswitch_7
    invoke-static {v1, v2}, LX/4Lg;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742878
    :sswitch_8
    invoke-static {v1, v2}, LX/4M1;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742879
    :sswitch_9
    invoke-static {v1, v2}, LX/4MR;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742880
    :sswitch_a
    invoke-static {v1, v2}, LX/4Mf;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742881
    :sswitch_b
    invoke-static {v1, v2}, LX/4Mg;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742882
    :sswitch_c
    invoke-static {v1, v2}, LX/4Mh;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742883
    :sswitch_d
    invoke-static {v1, v2}, LX/4N1;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742884
    :sswitch_e
    invoke-static {v1, v2}, LX/4NM;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742885
    :sswitch_f
    invoke-static {v1, v2}, LX/4NR;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto :goto_0

    .line 742886
    :sswitch_10
    invoke-static {v1, v2}, LX/4NS;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742887
    :sswitch_11
    invoke-static {v1, v2}, LX/4NT;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742888
    :sswitch_12
    invoke-static {v1, v2}, LX/4NZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742889
    :sswitch_13
    invoke-static {v1, v2}, LX/4Na;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742890
    :sswitch_14
    invoke-static {v1, v2}, LX/4Nb;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742891
    :sswitch_15
    invoke-static {v1, v2}, LX/4Nh;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742892
    :sswitch_16
    invoke-static {v1, v2}, LX/4O3;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742893
    :sswitch_17
    invoke-static {v1, v2}, LX/4OQ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742894
    :sswitch_18
    invoke-static {v1, v2}, LX/4OS;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742895
    :sswitch_19
    invoke-static {v1, v2}, LX/4OV;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742896
    :sswitch_1a
    invoke-static {v1, v2}, LX/4OW;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742897
    :sswitch_1b
    invoke-static {v1, v2}, LX/2za;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742898
    :sswitch_1c
    invoke-static {v1, v2}, LX/4OZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742899
    :sswitch_1d
    invoke-static {v1, v2}, LX/4Oe;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742900
    :sswitch_1e
    invoke-static {v1, v2}, LX/4On;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742901
    :sswitch_1f
    invoke-static {v1, v2}, LX/4PV;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742902
    :sswitch_20
    invoke-static {v1, v2}, LX/4PY;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742903
    :sswitch_21
    invoke-static {v1, v2}, LX/4Pc;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742904
    :sswitch_22
    invoke-static {v1, v2}, LX/4Pu;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742905
    :sswitch_23
    invoke-static {v1, v2}, LX/4Pv;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742906
    :sswitch_24
    invoke-static {v1, v2}, LX/4Q1;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742907
    :sswitch_25
    invoke-static {v1, v2}, LX/4Q9;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742908
    :sswitch_26
    invoke-static {v1, v2}, LX/4QM;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742909
    :sswitch_27
    invoke-static {v1, v2}, LX/4QU;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742910
    :sswitch_28
    invoke-static {v1, v2}, LX/4QX;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742911
    :sswitch_29
    invoke-static {v1, v2}, LX/4QZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742912
    :sswitch_2a
    invoke-static {v1, v2}, LX/4Qc;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742913
    :sswitch_2b
    invoke-static {v1, v2}, LX/4Qf;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742914
    :sswitch_2c
    invoke-static {v1, v2}, LX/4Qk;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742915
    :sswitch_2d
    invoke-static {v1, v2}, LX/4Qm;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742916
    :sswitch_2e
    invoke-static {v1, v2}, LX/4R3;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742917
    :sswitch_2f
    invoke-static {v1, v2}, LX/4R7;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742918
    :sswitch_30
    invoke-static {v1, v2}, LX/4R9;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742919
    :sswitch_31
    invoke-static {v1, v2}, LX/4RS;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742920
    :sswitch_32
    invoke-static {v1, v2}, LX/4RZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742921
    :sswitch_33
    invoke-static {v1, v2}, LX/4Rl;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742922
    :sswitch_34
    invoke-static {v1, v2}, LX/4Ru;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742923
    :sswitch_35
    invoke-static {v1, v2}, LX/4S5;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742924
    :sswitch_36
    invoke-static {v1, v2}, LX/4S7;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742925
    :sswitch_37
    invoke-static {v1, v2}, LX/4SR;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742926
    :sswitch_38
    invoke-static {v1, v2}, LX/4SZ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742927
    :sswitch_39
    invoke-static {v1, v2}, LX/4Se;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742928
    :sswitch_3a
    invoke-static {v1, v2}, LX/4St;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742929
    :sswitch_3b
    invoke-static {v1, v2}, LX/2aD;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742930
    :sswitch_3c
    invoke-static {v1, v2}, LX/4TG;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742931
    :sswitch_3d
    invoke-static {v1, v2}, LX/4TJ;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742932
    :sswitch_3e
    invoke-static {v1, v2}, LX/4Ta;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742933
    :sswitch_3f
    invoke-static {v1, v2}, LX/4Ty;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742934
    :sswitch_40
    invoke-static {v1, v2}, LX/4U8;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742935
    :sswitch_41
    invoke-static {v1, v2}, LX/4UA;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742936
    :sswitch_42
    invoke-static {v1, v2}, LX/4UD;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    .line 742937
    :sswitch_43
    invoke-static {v1, v2}, LX/4UT;->a(LX/15w;S)LX/15i;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_3b
        0xa -> :sswitch_3d
        0x3a -> :sswitch_7
        0x3c -> :sswitch_2d
        0x3d -> :sswitch_2c
        0x3e -> :sswitch_1
        0x3f -> :sswitch_42
        0x40 -> :sswitch_37
        0x47 -> :sswitch_15
        0x48 -> :sswitch_f
        0x4b -> :sswitch_30
        0x4c -> :sswitch_3c
        0x4d -> :sswitch_2e
        0x4e -> :sswitch_3f
        0x51 -> :sswitch_2b
        0x53 -> :sswitch_41
        0x54 -> :sswitch_38
        0x55 -> :sswitch_2f
        0xb2 -> :sswitch_6
        0xb3 -> :sswitch_e
        0xb4 -> :sswitch_16
        0xb5 -> :sswitch_18
        0xb6 -> :sswitch_1c
        0xb7 -> :sswitch_27
        0xb8 -> :sswitch_28
        0xb9 -> :sswitch_29
        0xba -> :sswitch_25
        0xbb -> :sswitch_35
        0xbc -> :sswitch_39
        0xbd -> :sswitch_3e
        0xd0 -> :sswitch_2
        0xd1 -> :sswitch_d
        0xd2 -> :sswitch_1a
        0xd3 -> :sswitch_1b
        0xd4 -> :sswitch_1d
        0xd5 -> :sswitch_21
        0xd6 -> :sswitch_31
        0xd7 -> :sswitch_3a
        0xd8 -> :sswitch_3
        0xd9 -> :sswitch_8
        0xda -> :sswitch_43
        0xdb -> :sswitch_17
        0x1a3 -> :sswitch_13
        0x1a6 -> :sswitch_12
        0x1a7 -> :sswitch_14
        0x1a8 -> :sswitch_10
        0x1ac -> :sswitch_a
        0x1ad -> :sswitch_b
        0x1ae -> :sswitch_c
        0x1af -> :sswitch_22
        0x1b0 -> :sswitch_23
        0x1b1 -> :sswitch_24
        0x1b2 -> :sswitch_34
        0x1b4 -> :sswitch_40
        0x24a -> :sswitch_4
        0x24e -> :sswitch_19
        0x261 -> :sswitch_5
        0x263 -> :sswitch_11
        0x265 -> :sswitch_33
        0x26b -> :sswitch_9
        0x26e -> :sswitch_32
        0x278 -> :sswitch_1e
        0x281 -> :sswitch_20
        0x287 -> :sswitch_0
        0x289 -> :sswitch_36
        0x28d -> :sswitch_2a
        0x291 -> :sswitch_1f
        0x299 -> :sswitch_26
    .end sparse-switch
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 742852
    invoke-virtual {p0, p1, p2}, Lcom/facebook/graphql/model/FeedUnitDeserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
