.class public final Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782804
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782805
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782799
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782800
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782801
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782802
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->e:Ljava/lang/String;

    .line 782803
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782796
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782797
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782798
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->g:Ljava/lang/String;

    .line 782795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 782783
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782784
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 782785
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 782786
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 782787
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 782788
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 782789
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 782790
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 782791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782792
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 782775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 782777
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782778
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 782779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;

    .line 782780
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782781
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782782
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782774
    const v0, 0x272fe664

    return v0
.end method
