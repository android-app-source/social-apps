.class public final Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765397
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765396
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765382
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 765383
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 765398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 765400
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 765401
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 765402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765403
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 765393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->e:Ljava/util/List;

    .line 765395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 765385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765386
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 765387
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 765388
    if-eqz v1, :cond_0

    .line 765389
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    .line 765390
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->e:Ljava/util/List;

    .line 765391
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765392
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 765384
    const v0, -0x699cc5df

    return v0
.end method
