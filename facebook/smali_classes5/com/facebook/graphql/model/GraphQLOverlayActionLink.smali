.class public final Lcom/facebook/graphql/model/GraphQLOverlayActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 770378
    const-class v0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 770377
    const-class v0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 770366
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 770367
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770374
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770375
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770376
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770371
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770372
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 770373
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770368
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770369
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->g:Ljava/lang/String;

    .line 770370
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770379
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770380
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->h:Ljava/lang/String;

    .line 770381
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 770344
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770345
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 770346
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 770327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 770328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 770329
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 770330
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 770331
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 770332
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 770333
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 770334
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 770335
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 770336
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 770337
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 770338
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 770339
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 770340
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 770341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 770342
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 770343
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 770347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 770348
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 770349
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770350
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 770351
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;

    .line 770352
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770353
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 770354
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 770355
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 770356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;

    .line 770357
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 770358
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 770359
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 770360
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->j()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 770361
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;

    .line 770362
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;->f:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 770363
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 770364
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 770365
    const v0, -0x4b252c60

    return v0
.end method
