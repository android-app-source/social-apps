.class public final Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jW;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755730
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755731
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 755732
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755733
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0xb92b71b

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 755734
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x:LX/0x2;

    .line 755735
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u:Ljava/lang/String;

    .line 755738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLMediaSet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLMediaSet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLMediaSet;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 755741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLMediaSet;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 755745
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 755746
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 755747
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 755748
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 755749
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 755750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x:LX/0x2;

    if-nez v0, :cond_0

    .line 755751
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x:LX/0x2;

    .line 755752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 24

    .prologue
    .line 755753
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755754
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 755755
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 755756
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 755757
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 755758
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 755759
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 755760
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 755761
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 755762
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 755763
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 755764
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 755765
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 755766
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 755767
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 755768
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->A()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 755769
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 755770
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 755771
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 755772
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 755773
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 755774
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 755775
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 755776
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 755777
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 755778
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 755779
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 755780
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 755781
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 755782
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755783
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755784
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755785
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755786
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755787
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755788
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755789
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755790
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755791
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 755792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 755794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755795
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 755796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755797
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755798
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 755799
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 755800
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 755801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755802
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLUser;

    .line 755803
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 755804
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 755805
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 755806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755807
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 755808
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 755809
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 755811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755812
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755813
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 755814
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755815
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 755816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755817
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755818
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 755819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 755821
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755822
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755823
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 755824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755825
    if-eqz v2, :cond_6

    .line 755826
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755827
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m:Ljava/util/List;

    move-object v1, v0

    .line 755828
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 755829
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755830
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 755831
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755832
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755833
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 755834
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 755836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755837
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755838
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 755839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 755841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755842
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755843
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 755844
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755845
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 755846
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755847
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755848
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 755849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 755851
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755852
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755853
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 755854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 755856
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755857
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755858
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 755859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 755861
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 755862
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755863
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755864
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755865
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 755866
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g:J

    .line 755867
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 755722
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 755723
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g:J

    .line 755724
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755725
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 755726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 755727
    :goto_0
    return-object v0

    .line 755728
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 755729
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 755677
    const v0, 0xb92b71b

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 755678
    const/4 v0, 0x0

    move-object v0, v0

    .line 755679
    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755680
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755681
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755682
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755683
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755684
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLUser;

    .line 755685
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755686
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755687
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->i:Ljava/lang/String;

    .line 755688
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755692
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755693
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755694
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m:Ljava/util/List;

    .line 755697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755698
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755699
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755700
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755701
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755702
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755703
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755704
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755705
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755706
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755708
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q:Ljava/lang/String;

    .line 755709
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755710
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755711
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755712
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755713
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755714
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755715
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755716
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755717
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755718
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755719
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755720
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755721
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
