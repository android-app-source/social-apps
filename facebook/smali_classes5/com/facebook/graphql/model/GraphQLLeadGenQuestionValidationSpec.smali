.class public final Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public i:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763604
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763603
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 763601
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 763602
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->e:Ljava/lang/String;

    .line 763600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->f:Ljava/util/List;

    .line 763597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763605
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763606
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->g:Ljava/lang/String;

    .line 763607
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 763594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->h:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 763578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763579
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 763580
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 763581
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 763582
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 763583
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 763584
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 763585
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 763586
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->m()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    if-ne v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 763587
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 763588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763589
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 763590
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->m()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    move-result-object v0

    goto :goto_0

    .line 763591
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 763575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763577
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->i:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->i:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->i:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 763574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->i:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 763571
    const v0, 0xca1dcae

    return v0
.end method
