.class public final Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775332
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775331
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 775328
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k:LX/0x2;

    .line 775330
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775288
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775289
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->e:Ljava/lang/String;

    .line 775290
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->f:Ljava/lang/String;

    .line 775327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 775322
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k:LX/0x2;

    if-nez v0, :cond_0

    .line 775323
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k:LX/0x2;

    .line 775324
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 775306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775307
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 775308
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 775309
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 775310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 775311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 775312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 775313
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 775314
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 775315
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 775316
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 775317
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 775318
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 775319
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 775320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775321
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 775336
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 775338
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775339
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 775340
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 775342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 775343
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775344
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 775345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 775346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 775347
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 775348
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 775349
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775350
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775305
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775304
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775302
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->j:Ljava/lang/String;

    .line 775303
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775300
    const v0, 0x1df71334

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775297
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775298
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775299
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775294
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775295
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775296
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 775293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method
