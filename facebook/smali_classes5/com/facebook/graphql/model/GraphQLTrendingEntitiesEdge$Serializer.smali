.class public final Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 788617
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 788618
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 788620
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 788621
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 788622
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 788623
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 788624
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 788625
    if-eqz v2, :cond_0

    .line 788626
    const-string p0, "node"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 788627
    invoke-static {v1, v2, p1, p2}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 788628
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 788629
    if-eqz v2, :cond_1

    .line 788630
    const-string p0, "promote_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 788631
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 788632
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 788633
    if-eqz v2, :cond_2

    .line 788634
    const-string p0, "query"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 788635
    invoke-static {v1, v2, p1, p2}, LX/4Np;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 788636
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 788637
    if-eqz v2, :cond_3

    .line 788638
    const-string p0, "tracking"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 788639
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 788640
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 788641
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 788619
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTrendingEntitiesEdge;LX/0nX;LX/0my;)V

    return-void
.end method
