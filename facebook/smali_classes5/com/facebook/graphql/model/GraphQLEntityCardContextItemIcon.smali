.class public final Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748272
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748271
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 748269
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748270
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 748273
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748274
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    .line 748275
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 748261
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748262
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 748263
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 748264
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 748265
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->j()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 748266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748267
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 748268
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->j()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 748253
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 748256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 748257
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 748258
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748259
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748260
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748250
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748251
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748252
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 748249
    const v0, 0x63601ac8    # 4.1340005E21f

    return v0
.end method
