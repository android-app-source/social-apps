.class public final Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786786
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 786787
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 786788
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 786789
    const/16 v0, 0x295

    .line 786790
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 786791
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 786792
    const/4 v4, 0x0

    .line 786793
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 786794
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786795
    :goto_0
    move v2, v4

    .line 786796
    if-eqz v1, :cond_0

    .line 786797
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 786798
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 786799
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 786800
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 786801
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 786802
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 786803
    move-object v2, v1

    .line 786804
    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;-><init>()V

    .line 786805
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 786806
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 786807
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 786808
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 786809
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 786810
    :cond_1
    return-object v1

    .line 786811
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 786812
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_a

    .line 786813
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 786814
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 786815
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 786816
    const-string p0, "card_description"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 786817
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 786818
    :cond_4
    const-string p0, "card_title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 786819
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 786820
    :cond_5
    const-string p0, "featured_article"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 786821
    invoke-static {p1, v3}, LX/4Og;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 786822
    :cond_6
    const-string p0, "feedback"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 786823
    invoke-static {p1, v3}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 786824
    :cond_7
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 786825
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 786826
    :cond_8
    const-string p0, "photo"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 786827
    invoke-static {p1, v3}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 786828
    :cond_9
    const-string p0, "url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 786829
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 786830
    :cond_a
    const/4 v12, 0x7

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 786831
    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 786832
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 786833
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 786834
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 786835
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 786836
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 786837
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 786838
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_b
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v4

    goto/16 :goto_1
.end method
