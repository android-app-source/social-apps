.class public final Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public g:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746401
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 746365
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 746399
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 746400
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 746396
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746397
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->e:Ljava/lang/String;

    .line 746398
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 746395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 746390
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->g:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746391
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->g:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->g:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 746392
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->g:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 746376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746377
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 746378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 746379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 746380
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 746381
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 746382
    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v0, v5, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v4, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 746383
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v4, v5, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 746384
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 746385
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 746386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746387
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 746388
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    goto :goto_0

    .line 746389
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 746373
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 746374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 746375
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 746370
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746371
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->h:Ljava/lang/String;

    .line 746372
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 746369
    const v0, 0x278a7d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 746366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 746367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->i:Ljava/lang/String;

    .line 746368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->i:Ljava/lang/String;

    return-object v0
.end method
