.class public final Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 769966
    const-class v0, Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 769967
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 769969
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 769970
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 769971
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 769972
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 769973
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769974
    if-eqz v2, :cond_1

    .line 769975
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769976
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 769977
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 769978
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/4Pz;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769979
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 769980
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 769981
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 769982
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 769968
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNotificationStoriesDeltaConnection;LX/0nX;LX/0my;)V

    return-void
.end method
