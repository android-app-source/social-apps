.class public final Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter$Serializer;
.end annotation


# instance fields
.field public A:D

.field public B:D

.field public C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:D

.field public E:D

.field public F:D

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:D

.field public g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:D

.field public m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:D

.field public p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:D

.field public u:I

.field public v:D

.field public w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:D

.field public y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 773118
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 773119
    const-class v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 773120
    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 773121
    return-void
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G:Ljava/lang/String;

    .line 773124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final A()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773125
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773126
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773127
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->v:D

    return-wide v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773128
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773129
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773130
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final C()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773131
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773132
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773133
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->x:D

    return-wide v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773134
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773135
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773136
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    return-object v0
.end method

.method public final E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773207
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773208
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773209
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->z:I

    return v0
.end method

.method public final F()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773137
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773138
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773139
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->A:D

    return-wide v0
.end method

.method public final G()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773140
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773141
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773142
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B:D

    return-wide v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773144
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773145
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final I()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773146
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773147
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773148
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D:D

    return-wide v0
.end method

.method public final J()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773149
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773150
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773151
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->E:D

    return-wide v0
.end method

.method public final K()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 773152
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773153
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773154
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->F:D

    return-wide v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    .line 773157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 773158
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773159
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 773160
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 773161
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 773162
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 773163
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 773164
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 773165
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 773166
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 773167
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 773168
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 773169
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 773170
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 773171
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 773172
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->M()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 773173
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 773174
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 773175
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 773176
    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773177
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 773178
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 773179
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 773180
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 773181
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 773182
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->q()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773183
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 773184
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 773185
    const/16 v3, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->t()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773186
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 773187
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 773188
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773189
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->x()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 773190
    const/16 v3, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773191
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->z()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 773192
    const/16 v3, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->A()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773193
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773194
    const/16 v3, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773195
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773196
    const/16 v2, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 773197
    const/16 v3, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->F()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773198
    const/16 v3, 0x17

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773199
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773200
    const/16 v3, 0x19

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->I()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773201
    const/16 v3, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->J()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773202
    const/16 v3, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->K()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 773203
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773204
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 773205
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 773206
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 773054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 773056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    .line 773057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 773058
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773059
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    .line 773060
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 773061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    .line 773062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 773063
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773064
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    .line 773065
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 773066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 773068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773069
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773070
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 773071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 773073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773074
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773075
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 773076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 773078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773079
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773080
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 773081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 773083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773084
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773085
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 773086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 773088
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773089
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773090
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 773091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 773093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773094
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773095
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 773096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 773098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773099
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773100
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 773101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773102
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 773103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773104
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773105
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 773106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 773108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773109
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773110
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 773111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    .line 773112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 773113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    .line 773114
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    .line 773115
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 773116
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 772985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 772986
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->f:D

    .line 772987
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l:D

    .line 772988
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->o:D

    .line 772989
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->q:I

    .line 772990
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s:I

    .line 772991
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->t:D

    .line 772992
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u:I

    .line 772993
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->v:D

    .line 772994
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->x:D

    .line 772995
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->z:I

    .line 772996
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->A:D

    .line 772997
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B:D

    .line 772998
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D:D

    .line 772999
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->E:D

    .line 773000
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->F:D

    .line 773001
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 773005
    const v0, 0x2afc3f97

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773006
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773007
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    .line 773008
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->e:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    return-object v0
.end method

.method public final k()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773009
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773010
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773011
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->f:D

    return-wide v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773012
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773013
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    .line 773014
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->g:Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773015
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->h:Ljava/lang/String;

    .line 773017
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773018
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773019
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773020
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->i:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773021
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773022
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j:Ljava/lang/String;

    .line 773023
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773024
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773025
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773026
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final q()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773027
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773028
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773029
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l:D

    return-wide v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773030
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773031
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773032
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773033
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773034
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773035
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final t()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773002
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773003
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773004
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->o:D

    return-wide v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773036
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773037
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    .line 773038
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p:Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    return-object v0
.end method

.method public final v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773039
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773040
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773041
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->q:I

    return v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 773042
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 773043
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    .line 773044
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r:Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    return-object v0
.end method

.method public final x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773045
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773046
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773047
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s:I

    return v0
.end method

.method public final y()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773048
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773049
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773050
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->t:D

    return-wide v0
.end method

.method public final z()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 773051
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 773052
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 773053
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u:I

    return v0
.end method
