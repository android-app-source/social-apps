.class public final Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771509
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771486
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 771507
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 771508
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771504
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771505
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->e:Ljava/lang/String;

    .line 771506
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771510
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771511
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->f:Ljava/lang/String;

    .line 771512
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->g:Ljava/lang/String;

    .line 771503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 771491
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771492
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 771493
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 771494
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageNameCheckResult;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 771495
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 771496
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 771497
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 771498
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 771499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771500
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 771488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771490
    return-object p0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 771487
    const v0, 0x4ee0390b

    return v0
.end method
