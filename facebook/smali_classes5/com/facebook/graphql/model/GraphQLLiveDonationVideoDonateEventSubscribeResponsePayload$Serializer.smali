.class public final Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764259
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764260
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764261
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 764262
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 764263
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 764264
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764265
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764266
    if-eqz v2, :cond_0

    .line 764267
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764268
    invoke-static {v1, v2, p1, p2}, LX/4UG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 764269
    :cond_0
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764270
    if-eqz v2, :cond_1

    .line 764271
    const-string p0, "donation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764272
    invoke-static {v1, v2, p1, p2}, LX/4Ln;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 764273
    :cond_1
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764274
    if-eqz v2, :cond_2

    .line 764275
    const-string p0, "fundraiser_to_charity"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764276
    invoke-static {v1, v2, p1, p2}, LX/4NC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 764277
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764278
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764279
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;LX/0nX;LX/0my;)V

    return-void
.end method
