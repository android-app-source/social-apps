.class public Lcom/facebook/graphql/model/extras/FeedUnitExtra;
.super Lcom/facebook/graphql/model/extras/BaseExtra;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/extras/FeedUnitExtra;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 790289
    new-instance v0, LX/4Zr;

    invoke-direct {v0}, LX/4Zr;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/extras/FeedUnitExtra;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 790307
    invoke-direct {p0}, Lcom/facebook/graphql/model/extras/BaseExtra;-><init>()V

    .line 790308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/extras/FeedUnitExtra;->a:LX/0Px;

    .line 790309
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 790310
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/extras/BaseExtra;-><init>(B)V

    .line 790311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/extras/FeedUnitExtra;->a:LX/0Px;

    .line 790312
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/186;)I
    .locals 3

    .prologue
    .line 790299
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/graphql/model/extras/BaseExtra;->a(LX/186;)I

    move-result v0

    .line 790300
    iget-object v1, p0, Lcom/facebook/graphql/model/extras/FeedUnitExtra;->a:LX/0Px;

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 790301
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 790302
    if-lez v0, :cond_0

    .line 790303
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 790304
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 790305
    invoke-virtual {p1}, LX/186;->d()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 790306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/15i;I)V
    .locals 1

    .prologue
    .line 790290
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v0

    .line 790291
    invoke-super {p0, p1, v0}, Lcom/facebook/graphql/model/extras/BaseExtra;->a(LX/15i;I)V

    .line 790292
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    .line 790293
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    :goto_0
    move-object v0, p1

    .line 790294
    iput-object v0, p0, Lcom/facebook/graphql/model/extras/FeedUnitExtra;->a:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 790295
    monitor-exit p0

    return-void

    .line 790296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 790297
    :cond_0
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 790298
    goto :goto_0
.end method
