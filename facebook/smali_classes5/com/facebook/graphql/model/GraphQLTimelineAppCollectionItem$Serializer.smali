.class public final Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 787405
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 787406
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 787404
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 787310
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 787311
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v8, 0x0

    const/16 v6, 0x16

    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 787312
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 787313
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787314
    if-eqz v2, :cond_0

    .line 787315
    const-string v3, "attribution_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787316
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787317
    :cond_0
    invoke-virtual {v1, v0, v4, v5}, LX/15i;->a(IIS)S

    move-result v2

    .line 787318
    if-eqz v2, :cond_1

    .line 787319
    const-string v2, "collection_item_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787320
    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    invoke-virtual {v1, v0, v4, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787321
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 787322
    cmp-long v4, v2, v8

    if-eqz v4, :cond_2

    .line 787323
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787324
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 787325
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787326
    if-eqz v2, :cond_3

    .line 787327
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787328
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787329
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787330
    if-eqz v2, :cond_4

    .line 787331
    const-string v3, "global_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787332
    invoke-static {v1, v2, p1, p2}, LX/4MV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 787333
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787334
    if-eqz v2, :cond_5

    .line 787335
    const-string v3, "icon_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787336
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787337
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787338
    if-eqz v2, :cond_6

    .line 787339
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787340
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787341
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787342
    if-eqz v2, :cond_7

    .line 787343
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787344
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787345
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787346
    if-eqz v2, :cond_8

    .line 787347
    const-string v3, "listImage"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787348
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787349
    :cond_8
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787350
    if-eqz v2, :cond_9

    .line 787351
    const-string v3, "locally_updated_containing_collection_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787352
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787353
    :cond_9
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787354
    if-eqz v2, :cond_a

    .line 787355
    const-string v3, "node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787356
    invoke-static {v1, v2, p1, p2}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787357
    :cond_a
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787358
    if-eqz v2, :cond_b

    .line 787359
    const-string v3, "permalink_node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787360
    invoke-static {v1, v2, p1, p2}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787361
    :cond_b
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787362
    if-eqz v2, :cond_c

    .line 787363
    const-string v3, "profileImageLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787364
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787365
    :cond_c
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787366
    if-eqz v2, :cond_d

    .line 787367
    const-string v3, "profileImageSmall"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787368
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787369
    :cond_d
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787370
    if-eqz v2, :cond_e

    .line 787371
    const-string v3, "rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787372
    invoke-static {v1, v2, p1}, LX/2sB;->a(LX/15i;ILX/0nX;)V

    .line 787373
    :cond_e
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787374
    if-eqz v2, :cond_f

    .line 787375
    const-string v3, "source_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787376
    invoke-static {v1, v2, p1, p2}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 787377
    :cond_f
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787378
    if-eqz v2, :cond_10

    .line 787379
    const-string v3, "subtitle_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787380
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787381
    :cond_10
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787382
    if-eqz v2, :cond_11

    .line 787383
    const-string v3, "tableImage"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787384
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 787385
    :cond_11
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787386
    if-eqz v2, :cond_12

    .line 787387
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787388
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787389
    :cond_12
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787390
    if-eqz v2, :cond_13

    .line 787391
    const-string v3, "titleForSummary"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787392
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787393
    :cond_13
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787394
    if-eqz v2, :cond_14

    .line 787395
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787396
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787397
    :cond_14
    invoke-virtual {v1, v0, v6, v5}, LX/15i;->a(IIS)S

    move-result v2

    .line 787398
    if-eqz v2, :cond_15

    .line 787399
    const-string v2, "viewed_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787400
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-virtual {v1, v0, v6, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787401
    :cond_15
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 787402
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 787403
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;LX/0nX;LX/0my;)V

    return-void
.end method
