.class public final Lcom/facebook/graphql/model/GraphQLTimelineSection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineSection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineSection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787782
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineSection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787783
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineSection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787804
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787805
    return-void
.end method

.method public constructor <init>(LX/4ZC;)V
    .locals 1

    .prologue
    .line 787784
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787785
    iget-object v0, p1, LX/4ZC;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->e:Ljava/lang/String;

    .line 787786
    iget-object v0, p1, LX/4ZC;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->f:Ljava/lang/String;

    .line 787787
    iget-object v0, p1, LX/4ZC;->d:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 787788
    iget-object v0, p1, LX/4ZC;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->h:Ljava/lang/String;

    .line 787789
    iget v0, p1, LX/4ZC;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->i:I

    .line 787790
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 787791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787792
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 787793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 787794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 787795
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 787796
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 787797
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 787798
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 787799
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 787800
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 787801
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->n()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 787802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 787773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 787775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 787776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 787777
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 787778
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 787779
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787780
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 787770
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 787771
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->i:I

    .line 787772
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 787769
    const v0, 0x4357df44

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787766
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787767
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->e:Ljava/lang/String;

    .line 787768
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787763
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787764
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->f:Ljava/lang/String;

    .line 787765
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787760
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787761
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 787762
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787757
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787758
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->h:Ljava/lang/String;

    .line 787759
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 787754
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 787755
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 787756
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSection;->i:I

    return v0
.end method
