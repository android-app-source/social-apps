.class public final Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLOverlayActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 770292
    const-class v0, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 770293
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 770294
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLOverlayActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 770295
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 770296
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 770297
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 770298
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 770299
    if-eqz v2, :cond_0

    .line 770300
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770301
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 770302
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 770303
    if-eqz v2, :cond_1

    .line 770304
    const-string v3, "info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770305
    invoke-static {v1, v2, p1, p2}, LX/4Q7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 770306
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 770307
    if-eqz v2, :cond_2

    .line 770308
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770309
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 770310
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 770311
    if-eqz v2, :cond_3

    .line 770312
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770313
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 770314
    :cond_3
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 770315
    if-eqz v2, :cond_4

    .line 770316
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770317
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 770318
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 770319
    if-eqz v2, :cond_5

    .line 770320
    const-string v3, "icon"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 770321
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 770322
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 770323
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 770324
    check-cast p1, Lcom/facebook/graphql/model/GraphQLOverlayActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLOverlayActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLOverlayActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
