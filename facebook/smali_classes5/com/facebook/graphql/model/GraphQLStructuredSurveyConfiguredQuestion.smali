.class public final Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public h:Z

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785184
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785259
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 785257
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 785258
    return-void
.end method

.method private a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 785254
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 785255
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 785256
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->e:Z

    return v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785248
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->g:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785249
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->g:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->g:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 785250
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->g:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    return-object v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785245
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 785246
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 785247
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->h:Z

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 785241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k:Ljava/lang/String;

    .line 785238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 785181
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785182
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l:Ljava/util/List;

    .line 785183
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 785233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m:Ljava/util/List;

    .line 785235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 785213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785214
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 785215
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 785216
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 785217
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->p()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 785218
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->q()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 785219
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 785220
    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->a()Z

    move-result v7

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 785221
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 785222
    const/4 v6, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    if-ne v0, v7, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v6, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785223
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l()Z

    move-result v6

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 785224
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 785225
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne v2, v6, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785226
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 785227
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 785228
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 785229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785230
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 785231
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->k()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    move-result-object v0

    goto :goto_0

    .line 785232
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 785190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785191
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 785192
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785193
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 785194
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    .line 785195
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785196
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785197
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785198
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 785199
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    .line 785200
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785201
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 785202
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 785203
    if-eqz v2, :cond_2

    .line 785204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    .line 785205
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->l:Ljava/util/List;

    move-object v1, v0

    .line 785206
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 785207
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 785208
    if-eqz v2, :cond_3

    .line 785209
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;

    .line 785210
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->m:Ljava/util/List;

    move-object v1, v0

    .line 785211
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785212
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 785186
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 785187
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->e:Z

    .line 785188
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyConfiguredQuestion;->h:Z

    .line 785189
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 785185
    const v0, -0x7e860431

    return v0
.end method
