.class public final Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 743788
    const-class v0, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 743789
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 743790
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 743791
    const/16 v0, 0x22b

    .line 743792
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 743793
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 743794
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 743795
    const/4 v2, 0x0

    .line 743796
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 743797
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 743798
    :goto_0
    move v2, v4

    .line 743799
    if-eqz v1, :cond_0

    .line 743800
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 743801
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 743802
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 743803
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 743804
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 743805
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 743806
    move-object v2, v1

    .line 743807
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAddToAlbumActionLink;-><init>()V

    .line 743808
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 743809
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 743810
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 743811
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 743812
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 743813
    :cond_1
    return-object v1

    .line 743814
    :cond_2
    const-string p0, "link_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 743815
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    move-object v7, v2

    move v2, v6

    .line 743816
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_7

    .line 743817
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 743818
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 743819
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v11, :cond_3

    .line 743820
    const-string p0, "album"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 743821
    invoke-static {p1, v3}, LX/4Ko;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 743822
    :cond_4
    const-string p0, "title"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 743823
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 743824
    :cond_5
    const-string p0, "url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 743825
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 743826
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 743827
    :cond_7
    const/4 v11, 0x4

    invoke-virtual {v3, v11}, LX/186;->c(I)V

    .line 743828
    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 743829
    invoke-virtual {v3, v6, v9}, LX/186;->b(II)V

    .line 743830
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 743831
    if-eqz v2, :cond_8

    .line 743832
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 743833
    :cond_8
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_9
    move-object v7, v2

    move v8, v4

    move v9, v4

    move v10, v4

    move v2, v4

    goto :goto_1
.end method
