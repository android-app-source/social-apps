.class public final Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764588
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764589
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764590
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 764591
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 764592
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 764593
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764594
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764595
    if-eqz v2, :cond_0

    .line 764596
    const-string p0, "broadcaster"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764597
    invoke-static {v1, v2, p1, p2}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 764598
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764599
    if-eqz v2, :cond_1

    .line 764600
    const-string p0, "video_broadcast_schedule"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764601
    invoke-static {v1, v2, p1, p2}, LX/4UC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 764602
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764603
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764604
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLiveVideoScheduleAttachmentStyleInfo;LX/0nX;LX/0my;)V

    return-void
.end method
