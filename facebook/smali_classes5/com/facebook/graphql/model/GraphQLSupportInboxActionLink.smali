.class public final Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786014
    const-class v0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786013
    const-class v0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 786011
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 786012
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786008
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->e:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786009
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->e:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 786010
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->e:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786005
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->f:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786006
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->f:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 786007
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->f:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786002
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786003
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->g:Ljava/lang/String;

    .line 786004
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785999
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786000
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->h:Ljava/lang/String;

    .line 786001
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 785970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->i:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 785985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785986
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 785987
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 785988
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 785989
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 785990
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 785991
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 785992
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 785993
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 785994
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 785995
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 785998
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->m()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 785972
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785973
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 785974
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 785975
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 785976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    .line 785977
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 785978
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785979
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 785980
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 785981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    .line 785982
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 785983
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785984
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 785971
    const v0, 0xf0b12c7

    return v0
.end method
