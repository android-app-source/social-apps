.class public final Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743111
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743071
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 743109
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 743110
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743103
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 743104
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 743105
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 743106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 743107
    const/4 v0, 0x0

    .line 743108
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 743102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 743087
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743088
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 743089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 743090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 743091
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 743092
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 743093
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 743094
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 743095
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 743096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743097
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 743098
    goto :goto_0

    .line 743099
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 743079
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 743081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 743082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 743083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 743084
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 743085
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743086
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743076
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743077
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 743078
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 743075
    const v0, -0x39454692

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->g:Ljava/lang/String;

    .line 743074
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->g:Ljava/lang/String;

    return-object v0
.end method
