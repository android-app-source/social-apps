.class public final Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 778129
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 778130
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 778131
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 778108
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 778109
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 778110
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 778111
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 778112
    if-eqz v2, :cond_0

    .line 778113
    const-string v3, "component_logical_path"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778114
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778115
    :cond_0
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 778116
    if-eqz v2, :cond_1

    .line 778117
    const-string v2, "component_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778118
    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778119
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 778120
    if-eqz v2, :cond_2

    .line 778121
    const-string v3, "component_tracking_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778122
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 778123
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 778124
    if-eqz v2, :cond_3

    .line 778125
    const-string v3, "friending_possibility"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 778126
    invoke-static {v1, v2, p1, p2}, LX/4SF;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 778127
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 778128
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 778107
    check-cast p1, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;->a(Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;LX/0nX;LX/0my;)V

    return-void
.end method
