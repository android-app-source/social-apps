.class public final Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766133
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 766134
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766132
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 766090
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 766091
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 766092
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 766093
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 766094
    if-eqz v2, :cond_0

    .line 766095
    const-string v3, "can_stop_sending_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766096
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 766097
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766098
    if-eqz v2, :cond_1

    .line 766099
    const-string v3, "coordinate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766100
    invoke-static {v1, v2, p1}, LX/4LW;->a(LX/15i;ILX/0nX;)V

    .line 766101
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 766102
    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    .line 766103
    const-string v4, "expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766104
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 766105
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766106
    if-eqz v2, :cond_3

    .line 766107
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766108
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766109
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766110
    if-eqz v2, :cond_4

    .line 766111
    const-string v3, "sender"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766112
    invoke-static {v1, v2, p1, p2}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 766113
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 766114
    if-eqz v2, :cond_5

    .line 766115
    const-string v3, "should_show_eta"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766116
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 766117
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766118
    if-eqz v2, :cond_6

    .line 766119
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766120
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766121
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766122
    if-eqz v2, :cond_7

    .line 766123
    const-string v3, "location_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766124
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766125
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766126
    if-eqz v2, :cond_8

    .line 766127
    const-string v3, "offline_threading_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766128
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766129
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 766130
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 766131
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;->a(Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;LX/0nX;LX/0my;)V

    return-void
.end method
