.class public final Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 777160
    const-class v0, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 777161
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 777120
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 777121
    const/16 v0, 0x16a

    .line 777122
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 777123
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 777124
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 777125
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_8

    .line 777126
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 777127
    :goto_0
    move v2, v4

    .line 777128
    if-eqz v1, :cond_0

    .line 777129
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 777130
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 777131
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 777132
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 777133
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 777134
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 777135
    move-object v2, v1

    .line 777136
    new-instance v1, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;-><init>()V

    .line 777137
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 777138
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 777139
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 777140
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 777141
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 777142
    :cond_1
    return-object v1

    .line 777143
    :cond_2
    const-string p0, "required"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 777144
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v2

    move v8, v2

    move v2, v6

    .line 777145
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_6

    .line 777146
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 777147
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 777148
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v10, :cond_3

    .line 777149
    const-string p0, "name"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 777150
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 777151
    :cond_4
    const-string p0, "string_value"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 777152
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 777153
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 777154
    :cond_6
    const/4 v10, 0x3

    invoke-virtual {v3, v10}, LX/186;->c(I)V

    .line 777155
    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 777156
    if-eqz v2, :cond_7

    .line 777157
    invoke-virtual {v3, v6, v8}, LX/186;->a(IZ)V

    .line 777158
    :cond_7
    const/4 v2, 0x2

    invoke-virtual {v3, v2, v7}, LX/186;->b(II)V

    .line 777159
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_8
    move v2, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_1
.end method
