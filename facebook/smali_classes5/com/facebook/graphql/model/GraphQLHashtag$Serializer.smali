.class public final Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLHashtag;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 761108
    const-class v0, Lcom/facebook/graphql/model/GraphQLHashtag;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 761109
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 761107
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLHashtag;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 761110
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 761111
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x7

    const/4 v3, 0x1

    .line 761112
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 761113
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 761114
    if-eqz v2, :cond_0

    .line 761115
    const-string v2, "android_urls"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761116
    invoke-virtual {v1, v0, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 761117
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761118
    if-eqz v2, :cond_1

    .line 761119
    const-string v3, "feedAwesomizerProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761120
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761121
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761122
    if-eqz v2, :cond_2

    .line 761123
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761124
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761125
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761126
    if-eqz v2, :cond_3

    .line 761127
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761128
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761129
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761130
    if-eqz v2, :cond_4

    .line 761131
    const-string v3, "imageHighOrig"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761132
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761133
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761134
    if-eqz v2, :cond_5

    .line 761135
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761136
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761137
    :cond_5
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 761138
    if-eqz v2, :cond_6

    .line 761139
    const-string v2, "name_search_tokens"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761140
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 761141
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761142
    if-eqz v2, :cond_7

    .line 761143
    const-string v3, "profileImageLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761144
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761145
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761146
    if-eqz v2, :cond_8

    .line 761147
    const-string v3, "profileImageSmall"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761148
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761149
    :cond_8
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761150
    if-eqz v2, :cond_9

    .line 761151
    const-string v3, "profilePicture50"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761152
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761153
    :cond_9
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761154
    if-eqz v2, :cond_a

    .line 761155
    const-string v3, "profilePictureHighRes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761156
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761157
    :cond_a
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761158
    if-eqz v2, :cond_b

    .line 761159
    const-string v3, "profilePictureLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761160
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761161
    :cond_b
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761162
    if-eqz v2, :cond_c

    .line 761163
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761164
    invoke-static {v1, v2, p1, p2}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 761165
    :cond_c
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761166
    if-eqz v2, :cond_d

    .line 761167
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761168
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761169
    :cond_d
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 761170
    if-eqz v2, :cond_e

    .line 761171
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761172
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 761173
    :cond_e
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761174
    if-eqz v2, :cond_f

    .line 761175
    const-string v3, "related_article_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761176
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761177
    :cond_f
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761178
    if-eqz v2, :cond_10

    .line 761179
    const-string v3, "social_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761180
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 761181
    :cond_10
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761182
    if-eqz v2, :cond_11

    .line 761183
    const-string v3, "streaming_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761184
    invoke-static {v1, v2, p1}, LX/4TN;->a(LX/15i;ILX/0nX;)V

    .line 761185
    :cond_11
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761186
    if-eqz v2, :cond_12

    .line 761187
    const-string v3, "tag"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761188
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761189
    :cond_12
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761190
    if-eqz v2, :cond_13

    .line 761191
    const-string v3, "taggable_object_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761192
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761193
    :cond_13
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761194
    if-eqz v2, :cond_14

    .line 761195
    const-string v3, "top_headline_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761196
    invoke-static {v1, v2, p1, p2}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 761197
    :cond_14
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761198
    if-eqz v2, :cond_15

    .line 761199
    const-string v3, "topic_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761200
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761201
    :cond_15
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761202
    if-eqz v2, :cond_16

    .line 761203
    const-string v3, "trending_topic_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761204
    invoke-static {v1, v2, p1, p2}, LX/4U4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 761205
    :cond_16
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761206
    if-eqz v2, :cond_17

    .line 761207
    const-string v3, "trending_topic_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761208
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761209
    :cond_17
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 761210
    if-eqz v2, :cond_18

    .line 761211
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761212
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 761213
    :cond_18
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761214
    if-eqz v2, :cond_19

    .line 761215
    const-string v3, "profilePicture180"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761216
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761217
    :cond_19
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 761218
    if-eqz v2, :cond_1a

    .line 761219
    const-string v3, "publisher_profile_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 761220
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 761221
    :cond_1a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 761222
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 761106
    check-cast p1, Lcom/facebook/graphql/model/GraphQLHashtag;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLHashtag$Serializer;->a(Lcom/facebook/graphql/model/GraphQLHashtag;LX/0nX;LX/0my;)V

    return-void
.end method
