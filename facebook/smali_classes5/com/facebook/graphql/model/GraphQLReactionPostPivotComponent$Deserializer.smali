.class public final Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 778242
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 778243
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 778244
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 778245
    const/16 v0, 0x1e1

    .line 778246
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 778247
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 778248
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 778249
    const/4 v2, 0x0

    .line 778250
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 778251
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 778252
    :goto_0
    move v2, v4

    .line 778253
    if-eqz v1, :cond_0

    .line 778254
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 778255
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 778256
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 778257
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 778258
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 778259
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 778260
    move-object v2, v1

    .line 778261
    new-instance v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;-><init>()V

    .line 778262
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 778263
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 778264
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 778265
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 778266
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 778267
    :cond_1
    return-object v1

    .line 778268
    :cond_2
    const-string p0, "component_style"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 778269
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    move-object v10, v2

    move v2, v6

    .line 778270
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_8

    .line 778271
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 778272
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 778273
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 778274
    const-string p0, "component_logical_path"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 778275
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 778276
    :cond_4
    const-string p0, "component_tracking_data"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 778277
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 778278
    :cond_5
    const-string p0, "message"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 778279
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 778280
    :cond_6
    const-string p0, "action"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 778281
    invoke-static {p1, v3}, LX/4SH;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 778282
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 778283
    :cond_8
    const/4 v12, 0x5

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 778284
    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 778285
    if-eqz v2, :cond_9

    .line 778286
    invoke-virtual {v3, v6, v10}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778287
    :cond_9
    const/4 v2, 0x2

    invoke-virtual {v3, v2, v9}, LX/186;->b(II)V

    .line 778288
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v8}, LX/186;->b(II)V

    .line 778289
    const/4 v2, 0x4

    invoke-virtual {v3, v2, v7}, LX/186;->b(II)V

    .line 778290
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_a
    move v7, v4

    move v8, v4

    move v9, v4

    move-object v10, v2

    move v11, v4

    move v2, v4

    goto :goto_1
.end method
