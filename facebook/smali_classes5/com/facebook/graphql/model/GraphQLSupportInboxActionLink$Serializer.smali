.class public final Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 785939
    const-class v0, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 785940
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 785942
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 785943
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 785944
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 785945
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 785946
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 785947
    if-eqz v2, :cond_0

    .line 785948
    const-string v3, "item"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785949
    invoke-static {v1, v2, p1, p2}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 785950
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 785951
    if-eqz v2, :cond_1

    .line 785952
    const-string v3, "support_item"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785953
    invoke-static {v1, v2, p1, p2}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 785954
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 785955
    if-eqz v2, :cond_2

    .line 785956
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785957
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785958
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 785959
    if-eqz v2, :cond_3

    .line 785960
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785961
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785962
    :cond_3
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 785963
    if-eqz v2, :cond_4

    .line 785964
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 785965
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 785966
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 785967
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 785941
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLSupportInboxActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
