.class public final Lcom/facebook/graphql/model/GraphQLEventTicketActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventTicketActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 749708
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventTicketActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventTicketActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLEventTicketActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 749709
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 749710
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLEventTicketActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 749711
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 749712
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v4, 0x0

    .line 749713
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 749714
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 749715
    if-eqz v2, :cond_0

    .line 749716
    const-string v3, "event"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749717
    invoke-static {v1, v2, p1, p2}, LX/4M6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 749718
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 749719
    if-eqz v2, :cond_1

    .line 749720
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749721
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749722
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 749723
    if-eqz v2, :cond_2

    .line 749724
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749725
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749726
    :cond_2
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 749727
    if-eqz v2, :cond_3

    .line 749728
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 749729
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 749730
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 749731
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 749732
    check-cast p1, Lcom/facebook/graphql/model/GraphQLEventTicketActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLEventTicketActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLEventTicketActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
