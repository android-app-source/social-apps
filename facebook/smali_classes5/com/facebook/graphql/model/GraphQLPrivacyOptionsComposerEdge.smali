.class public final Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578292
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578293
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 578305
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 578306
    return-void
.end method

.method public constructor <init>(LX/2sW;)V
    .locals 1

    .prologue
    .line 578256
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 578257
    iget-boolean v0, p1, LX/2sW;->b:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->e:Z

    .line 578258
    iget-boolean v0, p1, LX/2sW;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->f:Z

    .line 578259
    iget-boolean v0, p1, LX/2sW;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->g:Z

    .line 578260
    iget-object v0, p1, LX/2sW;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578261
    iget-object v0, p1, LX/2sW;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578262
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 578294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 578296
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 578297
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578298
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->j()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578299
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->k()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578300
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 578301
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 578302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578303
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 578304
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 578279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 578283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;

    .line 578284
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578285
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578286
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 578287
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 578288
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->e:Z

    .line 578289
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->f:Z

    .line 578290
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->g:Z

    .line 578291
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 578276
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 578277
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578278
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 578275
    const v0, -0x595cbcd

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 578272
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 578273
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578274
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->f:Z

    return v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 578269
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 578270
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578271
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->g:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 578266
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 578267
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578268
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 578263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 578264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-object v0
.end method
