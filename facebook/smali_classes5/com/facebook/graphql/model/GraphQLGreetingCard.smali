.class public final Lcom/facebook/graphql/model/GraphQLGreetingCard;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCard$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCard$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759077
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCard$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759076
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCard$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759074
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759075
    return-void
.end method

.method public constructor <init>(LX/4Wk;)V
    .locals 1

    .prologue
    .line 759066
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759067
    iget-object v0, p1, LX/4Wk;->b:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 759068
    iget-object v0, p1, LX/4Wk;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->f:Ljava/lang/String;

    .line 759069
    iget-object v0, p1, LX/4Wk;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 759070
    iget-object v0, p1, LX/4Wk;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 759071
    iget-object v0, p1, LX/4Wk;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->i:Ljava/lang/String;

    .line 759072
    iget-object v0, p1, LX/4Wk;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j:Ljava/lang/String;

    .line 759073
    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759063
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759064
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j:Ljava/lang/String;

    .line 759065
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 759047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 759049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 759050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 759051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 759052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 759053
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 759054
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 759055
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 759056
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 759057
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 759058
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 759059
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 759060
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 759061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759062
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759078
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 759081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 759083
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 759084
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 759085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 759086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 759087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 759088
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 759089
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 759090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 759091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 759092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 759093
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 759094
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759095
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759045
    const v0, -0x29f300f7

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759042
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759043
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 759044
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->f:Ljava/lang/String;

    .line 759041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759036
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759037
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 759038
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759033
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759034
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 759035
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->h:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759030
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759031
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->i:Ljava/lang/String;

    .line 759032
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCard;->i:Ljava/lang/String;

    return-object v0
.end method
