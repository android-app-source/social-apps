.class public final Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766750
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766751
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 766752
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 766753
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->k:LX/0x2;

    .line 766754
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766761
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766762
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 766763
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766755
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766756
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->f:Ljava/lang/String;

    .line 766757
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766747
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766748
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766749
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766758
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->h:Ljava/util/List;

    .line 766760
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766744
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766745
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 766746
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 766698
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->k:LX/0x2;

    if-nez v0, :cond_0

    .line 766699
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->k:LX/0x2;

    .line 766700
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->k:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 766728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766729
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 766730
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 766731
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 766732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->m()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 766733
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 766734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 766735
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 766736
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 766737
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 766738
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 766739
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 766740
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 766741
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 766742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766743
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 766705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766706
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 766707
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 766708
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 766709
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    .line 766710
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 766711
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 766712
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766713
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 766714
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    .line 766715
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766716
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 766717
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 766718
    if-eqz v2, :cond_2

    .line 766719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    .line 766720
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->h:Ljava/util/List;

    move-object v1, v0

    .line 766721
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 766722
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 766723
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 766724
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    .line 766725
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 766726
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766727
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766702
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766703
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->j:Ljava/lang/String;

    .line 766704
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 766701
    const v0, 0x666202db

    return v0
.end method
