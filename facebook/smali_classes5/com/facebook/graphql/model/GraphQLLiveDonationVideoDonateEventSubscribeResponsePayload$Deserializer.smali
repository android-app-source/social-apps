.class public final Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764257
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 764258
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764218
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 764219
    const/16 v0, 0x244

    .line 764220
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 764221
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 764222
    const/4 v2, 0x0

    .line 764223
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_7

    .line 764224
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 764225
    :goto_0
    move v2, v2

    .line 764226
    if-eqz v1, :cond_0

    .line 764227
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 764228
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 764229
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 764230
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 764231
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 764232
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 764233
    move-object v2, v1

    .line 764234
    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveDonationVideoDonateEventSubscribeResponsePayload;-><init>()V

    .line 764235
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 764236
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 764237
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 764238
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 764239
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 764240
    :cond_1
    return-object v1

    .line 764241
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 764242
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_6

    .line 764243
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 764244
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 764245
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v7, :cond_3

    .line 764246
    const-string p0, "video"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 764247
    invoke-static {p1, v3}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 764248
    :cond_4
    const-string p0, "donation"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 764249
    invoke-static {p1, v3}, LX/4Ln;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 764250
    :cond_5
    const-string p0, "fundraiser_to_charity"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 764251
    invoke-static {p1, v3}, LX/4NC;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 764252
    :cond_6
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 764253
    const/4 v7, 0x2

    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 764254
    const/4 v6, 0x3

    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 764255
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 764256
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v4, v2

    move v6, v2

    goto :goto_1
.end method
