.class public final Lcom/facebook/graphql/model/GraphQLFundraiser;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiser$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiser$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752799
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiser$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 752798
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiser$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 752796
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 752797
    return-void
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 752795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 752790
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 752791
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 752792
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->k:Z

    return v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752787
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752788
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752789
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752784
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752785
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 752786
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 752762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752763
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 752764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 752765
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 752766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 752767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 752768
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->o()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 752769
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 752770
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 752771
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 752772
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 752773
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 752774
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 752775
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 752776
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 752777
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 752778
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 752779
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 752780
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 752781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 752783
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 752800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 752801
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 752802
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752803
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 752804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 752805
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiser;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752806
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 752807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752808
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 752809
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 752810
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752811
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 752812
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 752813
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 752814
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 752815
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiser;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 752816
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 752817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 752818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 752819
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 752820
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiser;->i:Lcom/facebook/graphql/model/GraphQLActor;

    .line 752821
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->o()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 752822
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->o()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 752823
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->o()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 752824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiser;

    .line 752825
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiser;->j:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 752826
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 752827
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiser;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 752746
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 752747
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->k:Z

    .line 752748
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 752745
    const v0, -0x5be312d5

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752739
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 752740
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 752741
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 752742
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 752743
    const/4 v0, 0x0

    .line 752744
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->f:Ljava/lang/String;

    .line 752752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752753
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752754
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 752755
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752756
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752757
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->h:Ljava/lang/String;

    .line 752758
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 752759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->i:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 752760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->i:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->i:Lcom/facebook/graphql/model/GraphQLActor;

    .line 752761
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiser;->i:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method
