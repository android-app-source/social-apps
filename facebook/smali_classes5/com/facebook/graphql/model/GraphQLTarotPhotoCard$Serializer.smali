.class public final Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 786839
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 786840
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 786841
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 786842
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 786843
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 786844
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 786845
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786846
    if-eqz v2, :cond_0

    .line 786847
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786848
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786849
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786850
    if-eqz v2, :cond_1

    .line 786851
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786852
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786853
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786854
    if-eqz v2, :cond_2

    .line 786855
    const-string p0, "featured_article"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786856
    invoke-static {v1, v2, p1, p2}, LX/4Og;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 786857
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786858
    if-eqz v2, :cond_3

    .line 786859
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786860
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 786861
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786862
    if-eqz v2, :cond_4

    .line 786863
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786864
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786865
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 786866
    if-eqz v2, :cond_5

    .line 786867
    const-string p0, "photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786868
    invoke-static {v1, v2, p1, p2}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 786869
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 786870
    if-eqz v2, :cond_6

    .line 786871
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 786872
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 786873
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 786874
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 786875
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTarotPhotoCard$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTarotPhotoCard;LX/0nX;LX/0my;)V

    return-void
.end method
