.class public final Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744866
    const-class v0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744865
    const-class v0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 744840
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 744841
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744862
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744863
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->e:Ljava/lang/String;

    .line 744864
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744859
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744860
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 744861
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 744851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744852
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 744853
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 744854
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 744855
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 744856
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 744857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744858
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 744843
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744844
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 744845
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 744846
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 744847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;

    .line 744848
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAssociatePostToFundraiserForStoryResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 744849
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744850
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 744842
    const v0, 0x24dfec5d

    return v0
.end method
