.class public final Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Z

.field public g:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778238
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778237
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 778235
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 778236
    return-void
.end method

.method private a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 778232
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 778233
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 778234
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->e:Z

    return v0
.end method

.method private j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778229
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 778230
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 778231
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->f:Z

    return v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->g:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->g:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->g:Lcom/facebook/graphql/model/GraphQLUser;

    .line 778241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->g:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 778226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->h:Ljava/util/List;

    .line 778228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 778216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778217
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->k()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 778218
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->l()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 778219
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 778220
    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->a()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 778221
    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->j()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 778222
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 778223
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 778224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778225
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 778203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778204
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->k()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778205
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->k()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 778206
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->k()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 778207
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 778208
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->g:Lcom/facebook/graphql/model/GraphQLUser;

    .line 778209
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 778210
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 778211
    if-eqz v2, :cond_1

    .line 778212
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 778213
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->h:Ljava/util/List;

    move-object v1, v0

    .line 778214
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778215
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 778199
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 778200
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->e:Z

    .line 778201
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;->f:Z

    .line 778202
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 778198
    const v0, -0x5b53e988

    return v0
.end method
