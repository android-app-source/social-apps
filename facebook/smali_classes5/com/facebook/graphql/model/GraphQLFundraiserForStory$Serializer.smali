.class public final Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFundraiserForStory;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 753041
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 753042
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 753040
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFundraiserForStory;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 752966
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 752967
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 752968
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 752969
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 752970
    if-eqz v2, :cond_0

    .line 752971
    const-string p0, "campaign_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752972
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 752973
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 752974
    if-eqz v2, :cond_1

    .line 752975
    const-string p0, "can_donate"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752976
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 752977
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 752978
    if-eqz v2, :cond_2

    .line 752979
    const-string p0, "can_invite_to_campaign"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752980
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 752981
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 752982
    if-eqz v2, :cond_3

    .line 752983
    const-string p0, "charity_interface"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752984
    invoke-static {v1, v2, p1, p2}, LX/4LB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 752985
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 752986
    if-eqz v2, :cond_4

    .line 752987
    const-string p0, "full_width_post_donation_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752988
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 752989
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 752990
    if-eqz v2, :cond_5

    .line 752991
    const-string p0, "fundraiser_detailed_progress_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752992
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 752993
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 752994
    if-eqz v2, :cond_6

    .line 752995
    const-string p0, "fundraiser_for_charity_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 752996
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 752997
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 752998
    if-eqz v2, :cond_7

    .line 752999
    const-string p0, "fundraiser_page_subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753000
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753001
    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753002
    if-eqz v2, :cond_8

    .line 753003
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753004
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753005
    :cond_8
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753006
    if-eqz v2, :cond_9

    .line 753007
    const-string p0, "logo_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753008
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 753009
    :cond_9
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753010
    if-eqz v2, :cond_a

    .line 753011
    const-string p0, "mobile_donate_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753012
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753013
    :cond_a
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753014
    if-eqz v2, :cond_b

    .line 753015
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753016
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753017
    :cond_b
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753018
    if-eqz v2, :cond_c

    .line 753019
    const-string p0, "all_donations_summary_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753020
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753021
    :cond_c
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 753022
    if-eqz v2, :cond_d

    .line 753023
    const-string p0, "detailed_amount_raised_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753024
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 753025
    :cond_d
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753026
    if-eqz v2, :cond_e

    .line 753027
    const-string p0, "donors_social_context_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753028
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753029
    :cond_e
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753030
    if-eqz v2, :cond_f

    .line 753031
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753032
    invoke-static {v1, v2, p1, p2}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 753033
    :cond_f
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 753034
    if-eqz v2, :cond_10

    .line 753035
    const-string p0, "friend_donors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 753036
    invoke-static {v1, v2, p1, p2}, LX/4N8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 753037
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 753038
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 753039
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFundraiserForStory;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFundraiserForStory$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFundraiserForStory;LX/0nX;LX/0my;)V

    return-void
.end method
