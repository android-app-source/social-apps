.class public final Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:I

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774645
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 774646
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774621
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 774622
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->h:Ljava/lang/String;

    .line 774649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->h:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l:Ljava/lang/String;

    .line 774652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n:Ljava/lang/String;

    .line 774655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 774656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774657
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 774658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 774659
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 774660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 774661
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 774662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 774663
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 774664
    const/16 v7, 0xb

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 774665
    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->j()I

    move-result v8

    invoke-virtual {p1, v7, v8, v9}, LX/186;->a(III)V

    .line 774666
    const/4 v7, 0x2

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 774667
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 774668
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 774669
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 774670
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 774671
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 774672
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 774673
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 774674
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 774675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774676
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 774677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 774679
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 774680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 774681
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    .line 774682
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 774683
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 774684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 774685
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 774686
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    .line 774687
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 774688
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 774689
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 774640
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 774641
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->e:I

    .line 774642
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->i:I

    .line 774643
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->j:I

    .line 774644
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 774638
    const v0, -0x3b14ce17

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774635
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774636
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774637
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->e:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774632
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774633
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->f:Ljava/lang/String;

    .line 774634
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774629
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774630
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 774631
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->g:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    return-object v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774626
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774627
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774628
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->i:I

    return v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 774623
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 774624
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 774625
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->j:I

    return v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774618
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774619
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 774620
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 774615
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 774616
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m:Ljava/lang/String;

    .line 774617
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m:Ljava/lang/String;

    return-object v0
.end method
