.class public final Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnitComponent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionUnitComponent$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public D:D

.field public E:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

.field public F:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:I

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Z

.field public aE:Z

.field public aF:Z

.field public aG:Z

.field public aH:Z

.field public aI:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:D

.field public aO:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;",
            ">;"
        }
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation
.end field

.field public af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public ay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

.field public bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public bD:D

.field public bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:D

.field public bG:D

.field public bH:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

.field public bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bR:D

.field public bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bU:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bV:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

.field public bW:Lcom/facebook/graphql/model/GraphQLProductItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bX:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductItem;",
            ">;"
        }
    .end annotation
.end field

.field public bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Z

.field public bf:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;"
        }
    .end annotation
.end field

.field public bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:D

.field public bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public bm:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:D

.field public bq:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public bs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;",
            ">;"
        }
    .end annotation
.end field

.field public bt:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVect2;",
            ">;"
        }
    .end annotation
.end field

.field public bu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cA:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cG:I

.field public cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cI:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation
.end field

.field public cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation
.end field

.field public cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation
.end field

.field public cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation
.end field

.field public cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cb:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cc:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ce:D

.field public cf:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation
.end field

.field public cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public co:J

.field public cp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cr:I

.field public cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ct:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cz:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:I

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 780631
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 780632
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 780633
    const/16 v0, 0xd7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 780634
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780635
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780636
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 780637
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private B()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 780638
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780639
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780640
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n:I

    return v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780644
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780645
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p:Ljava/lang/String;

    .line 780646
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p:Ljava/lang/String;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 780649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 780655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    return-object v0
.end method

.method private H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t:Ljava/lang/String;

    .line 780658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t:Ljava/lang/String;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u:Ljava/lang/String;

    .line 780661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u:Ljava/lang/String;

    return-object v0
.end method

.method private J()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780625
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780626
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v:Ljava/lang/String;

    .line 780627
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v:Ljava/lang/String;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780665
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780666
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780667
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780668
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780669
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 780670
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780674
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780675
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z:Ljava/lang/String;

    .line 780676
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z:Ljava/lang/String;

    return-object v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780677
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780678
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780679
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780680
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780681
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B:Lcom/facebook/graphql/model/GraphQLComment;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B:Lcom/facebook/graphql/model/GraphQLComment;

    .line 780682
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method

.method private Q()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780683
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780684
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780685
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->D:D

    return-wide v0
.end method

.method private R()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780686
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780687
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    .line 780688
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    return-object v0
.end method

.method private S()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    .line 780691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    return-object v0
.end method

.method private T()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780692
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780693
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780694
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private V()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780698
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780699
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780700
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->I:I

    return v0
.end method

.method private W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780547
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780548
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K:Ljava/lang/String;

    .line 780549
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K:Ljava/lang/String;

    return-object v0
.end method

.method private Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780550
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780551
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780552
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private Z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780553
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780554
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780555
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aA()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780556
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780557
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780558
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780559
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780560
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780561
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aC()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780562
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780563
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap:Ljava/lang/String;

    const/16 v1, 0x45

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap:Ljava/lang/String;

    .line 780564
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap:Ljava/lang/String;

    return-object v0
.end method

.method private aD()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780565
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780566
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq:Ljava/util/List;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimeRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq:Ljava/util/List;

    .line 780567
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aE()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780568
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780569
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780570
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aF()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780571
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780572
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780573
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aG()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780574
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780575
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->au:Ljava/lang/String;

    const/16 v1, 0x4a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->au:Ljava/lang/String;

    .line 780576
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->au:Ljava/lang/String;

    return-object v0
.end method

.method private aH()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780577
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780578
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780579
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aI()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780580
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780581
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780582
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aJ()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 780585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method private aK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aL()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    return-object v0
.end method

.method private aM()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 780594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    return-object v0
.end method

.method private aN()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aC:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aC:Ljava/lang/String;

    const/16 v1, 0x52

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aC:Ljava/lang/String;

    .line 780597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aC:Ljava/lang/String;

    return-object v0
.end method

.method private aO()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780598
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780599
    const/16 v0, 0xa

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780600
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aD:Z

    return v0
.end method

.method private aP()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780601
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780602
    const/16 v0, 0xa

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780603
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE:Z

    return v0
.end method

.method private aQ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780604
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780605
    const/16 v0, 0xa

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780606
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF:Z

    return v0
.end method

.method private aR()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780607
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780608
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780609
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aG:Z

    return v0
.end method

.method private aS()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780610
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780611
    const/16 v0, 0xa

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780612
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH:Z

    return v0
.end method

.method private aT()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 780613
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780614
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780615
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI:Z

    return v0
.end method

.method private aU()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780616
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780617
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x59

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 780618
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aW()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780622
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780623
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL:Ljava/lang/String;

    const/16 v1, 0x5b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL:Ljava/lang/String;

    .line 780624
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL:Ljava/lang/String;

    return-object v0
.end method

.method private aX()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM:Ljava/lang/String;

    .line 780781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM:Ljava/lang/String;

    return-object v0
.end method

.method private aY()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780743
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780744
    const/16 v0, 0xb

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780745
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aN:D

    return-wide v0
.end method

.method private aZ()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 780787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private aa()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780788
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780789
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->N:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->N:Ljava/lang/String;

    .line 780790
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->N:Ljava/lang/String;

    return-object v0
.end method

.method private ab()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O:Ljava/lang/String;

    .line 780793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O:Ljava/lang/String;

    return-object v0
.end method

.method private ac()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780794
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780795
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780796
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ad()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780797
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780798
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q:Ljava/util/List;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q:Ljava/util/List;

    .line 780799
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ae()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780800
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780801
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 780802
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method private af()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780803
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780804
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S:Ljava/lang/String;

    .line 780805
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S:Ljava/lang/String;

    return-object v0
.end method

.method private ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780806
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780807
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780808
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ah()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780809
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780810
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780811
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ai()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780812
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780813
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780814
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780815
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780816
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780817
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ak()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780818
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780819
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X:Lcom/facebook/graphql/model/FeedUnit;

    const/16 v1, 0x31

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X:Lcom/facebook/graphql/model/FeedUnit;

    .line 780820
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private al()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780854
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780855
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780856
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780821
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780822
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780823
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private an()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780827
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780828
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780829
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ap()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780830
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780831
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac:Ljava/util/List;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac:Ljava/util/List;

    .line 780832
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aq()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 780835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    return-object v0
.end method

.method private ar()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae:Ljava/util/List;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae:Ljava/util/List;

    .line 780838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private as()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780839
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->af:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780840
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->af:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->af:Ljava/lang/String;

    .line 780841
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->af:Ljava/lang/String;

    return-object v0
.end method

.method private at()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780842
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780843
    const/4 v0, 0x7

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780844
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag:Z

    return v0
.end method

.method private au()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780845
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780846
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780847
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah:Z

    return v0
.end method

.method private av()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780848
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780849
    const/4 v0, 0x7

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780850
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai:Z

    return v0
.end method

.method private aw()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780782
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780783
    const/4 v0, 0x7

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780784
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj:Z

    return v0
.end method

.method private ax()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780851
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780852
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 780853
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method private ay()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780704
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780705
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780706
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private az()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780708
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780709
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bA()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780710
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780711
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs:Ljava/util/List;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs:Ljava/util/List;

    .line 780712
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bB()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVect2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780713
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780714
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt:Ljava/util/List;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt:Ljava/util/List;

    .line 780715
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bC()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780716
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780717
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu:Ljava/lang/String;

    const/16 v1, 0x7e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu:Ljava/lang/String;

    .line 780718
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu:Ljava/lang/String;

    return-object v0
.end method

.method private bD()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780719
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780720
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 780721
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method

.method private bE()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780722
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780723
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x82

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780724
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bF()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780725
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780726
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx:Ljava/util/List;

    const/16 v1, 0x83

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx:Ljava/util/List;

    .line 780727
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bG()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780728
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780729
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by:Ljava/lang/String;

    const/16 v1, 0x84

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by:Ljava/lang/String;

    .line 780730
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by:Ljava/lang/String;

    return-object v0
.end method

.method private bH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780731
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780732
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz:Ljava/lang/String;

    const/16 v1, 0x85

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz:Ljava/lang/String;

    .line 780733
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz:Ljava/lang/String;

    return-object v0
.end method

.method private bI()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780734
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780735
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    const/16 v1, 0x86

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 780736
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    return-object v0
.end method

.method private bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780737
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780738
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x87

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780739
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bK()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780740
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780741
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC:Ljava/util/List;

    const/16 v1, 0x88

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC:Ljava/util/List;

    .line 780742
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bL()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780701
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780702
    const/16 v0, 0x11

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780703
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD:D

    return-wide v0
.end method

.method private bM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780746
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780747
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780748
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bN()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780749
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780750
    const/16 v0, 0x11

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780751
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bF:D

    return-wide v0
.end method

.method private bO()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780752
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bH:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780753
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bH:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    const/16 v1, 0x8d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bH:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 780754
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bH:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    return-object v0
.end method

.method private bP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780755
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780756
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780757
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bQ()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780758
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x8f

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ:Lcom/facebook/graphql/model/GraphQLUser;

    .line 780760
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780761
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780762
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x90

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780763
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780764
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780765
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x91

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780766
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM:Ljava/lang/String;

    const/16 v1, 0x92

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM:Ljava/lang/String;

    .line 780769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM:Ljava/lang/String;

    return-object v0
.end method

.method private bU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780770
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780771
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x93

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780772
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bV()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780773
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780774
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP:Ljava/lang/String;

    const/16 v1, 0x95

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP:Ljava/lang/String;

    .line 780775
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP:Ljava/lang/String;

    return-object v0
.end method

.method private bW()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780776
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780777
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ:Ljava/lang/String;

    const/16 v1, 0x96

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ:Ljava/lang/String;

    .line 780778
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ:Ljava/lang/String;

    return-object v0
.end method

.method private bX()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780662
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780663
    const/16 v0, 0x12

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780664
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR:D

    return-wide v0
.end method

.method private bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780628
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780629
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x98

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780630
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bZ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x99

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ba()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779244
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x62

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779245
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779246
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779247
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779248
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bd()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779249
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779250
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 779251
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    return-object v0
.end method

.method private be()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779252
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aW:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779253
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aW:Ljava/lang/String;

    const/16 v1, 0x66

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aW:Ljava/lang/String;

    .line 779254
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aW:Ljava/lang/String;

    return-object v0
.end method

.method private bf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779255
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779256
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779257
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bg()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779258
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aY:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779259
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aY:Ljava/lang/String;

    const/16 v1, 0x68

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aY:Ljava/lang/String;

    .line 779260
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aY:Ljava/lang/String;

    return-object v0
.end method

.method private bh()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779261
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779262
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ:Ljava/lang/String;

    const/16 v1, 0x69

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ:Ljava/lang/String;

    .line 779263
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method private bi()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779264
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779265
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    const/16 v1, 0x6a

    const-class v2, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 779266
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    return-object v0
.end method

.method private bj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779267
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779268
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x6b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779269
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bk()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779270
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779271
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x6c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779272
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bl()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779273
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779274
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x6d

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd:Lcom/facebook/graphql/model/GraphQLStory;

    .line 779275
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private bm()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779237
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779238
    const/16 v0, 0xd

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779239
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->be:Z

    return v0
.end method

.method private bn()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779279
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779280
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf:Ljava/util/List;

    const/16 v1, 0x6f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf:Ljava/util/List;

    .line 779281
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779282
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779283
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779284
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bp()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779285
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779286
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x71

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779287
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private bq()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779288
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779289
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779290
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi:D

    return-wide v0
.end method

.method private br()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bs()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779294
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779295
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x74

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 779296
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private bt()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779297
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779298
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl:Ljava/util/List;

    const/16 v1, 0x75

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl:Ljava/util/List;

    .line 779299
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bu()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779300
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779301
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm:Ljava/util/List;

    const/16 v1, 0x76

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm:Ljava/util/List;

    .line 779302
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779303
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779304
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x77

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779305
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bw()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779306
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779307
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779308
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private bx()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779309
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779310
    const/16 v0, 0xf

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779311
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp:D

    return-wide v0
.end method

.method private by()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779312
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779313
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779314
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private bz()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779276
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779277
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x7b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 779278
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method private cA()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cC()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx:Ljava/lang/String;

    const/16 v1, 0xb8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx:Ljava/lang/String;

    .line 779170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx:Ljava/lang/String;

    return-object v0
.end method

.method private cD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cE()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779174
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779175
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0xba

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz:Lcom/facebook/graphql/model/GraphQLActor;

    .line 779176
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private cF()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA:Lcom/facebook/graphql/model/GraphQLVideo;

    const/16 v1, 0xbb

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 779179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method private cG()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB:Ljava/lang/String;

    const/16 v1, 0xbd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB:Ljava/lang/String;

    .line 779182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB:Ljava/lang/String;

    return-object v0
.end method

.method private cH()Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780463
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780464
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    const/16 v1, 0xbe

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    .line 780465
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    return-object v0
.end method

.method private cI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xbf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779189
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779190
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779191
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cK()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF:Ljava/lang/String;

    const/16 v1, 0xc1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF:Ljava/lang/String;

    .line 779194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF:Ljava/lang/String;

    return-object v0
.end method

.method private cL()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 779195
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 779196
    const/16 v0, 0x18

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 779197
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cG:I

    return v0
.end method

.method private cM()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779198
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779199
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI:Ljava/util/List;

    const/16 v1, 0xc4

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI:Ljava/util/List;

    .line 779200
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cN()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779201
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc5

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779203
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cO()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779204
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779205
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc6

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779206
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cP()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779207
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779208
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc7

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779209
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cQ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779210
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779211
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc8

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779212
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cR()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779213
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779214
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc9

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779215
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cS()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779216
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779217
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO:Ljava/util/List;

    const/16 v1, 0xca

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO:Ljava/util/List;

    .line 779218
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cT()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779219
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779220
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xcb

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779221
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cU()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779222
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779223
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xcc

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779224
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cV()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779225
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779226
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xcd

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779227
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cW()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779228
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779229
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xce

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779230
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cX()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 779231
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779232
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT:Ljava/util/List;

    const/16 v1, 0xcf

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT:Ljava/util/List;

    .line 779233
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cY()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779234
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779235
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xd0

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779236
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private cZ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xd1

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private ca()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780505
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780506
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x9a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780507
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private cb()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780469
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bV:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780470
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bV:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    const/16 v1, 0x9b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bV:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 780471
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bV:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    return-object v0
.end method

.method private cc()Lcom/facebook/graphql/model/GraphQLProductItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780472
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW:Lcom/facebook/graphql/model/GraphQLProductItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780473
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW:Lcom/facebook/graphql/model/GraphQLProductItem;

    const/16 v1, 0x9c

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 780474
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW:Lcom/facebook/graphql/model/GraphQLProductItem;

    return-object v0
.end method

.method private cd()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780475
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780476
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX:Ljava/util/List;

    const/16 v1, 0x9d

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX:Ljava/util/List;

    .line 780477
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ce()Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780478
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780479
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    const/16 v1, 0x9e

    const-class v2, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 780480
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    return-object v0
.end method

.method private cf()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780481
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780482
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ:Ljava/lang/String;

    const/16 v1, 0x9f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ:Ljava/lang/String;

    .line 780483
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ:Ljava/lang/String;

    return-object v0
.end method

.method private cg()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780484
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780485
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780486
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ch()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780487
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780488
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb:Ljava/lang/String;

    const/16 v1, 0xa1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb:Ljava/lang/String;

    .line 780489
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb:Ljava/lang/String;

    return-object v0
.end method

.method private ci()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780490
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780491
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0xa2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc:Lcom/facebook/graphql/model/GraphQLStory;

    .line 780492
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private cj()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780493
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780494
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    const/16 v1, 0xa3

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780495
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    return-object v0
.end method

.method private ck()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780496
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780497
    const/16 v0, 0x14

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780498
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce:D

    return-wide v0
.end method

.method private cl()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780499
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780500
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf:Ljava/util/List;

    const/16 v1, 0xa5

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf:Ljava/util/List;

    .line 780501
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780502
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780503
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780504
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780466
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780467
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780468
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private co()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780508
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780509
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xaa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780510
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780511
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780512
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xab

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780513
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780514
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780515
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xac

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780516
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cr()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780517
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780518
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xad

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780519
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cs()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780520
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780521
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn:Ljava/lang/String;

    const/16 v1, 0xae

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn:Ljava/lang/String;

    .line 780522
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn:Ljava/lang/String;

    return-object v0
.end method

.method private ct()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780523
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780524
    const/16 v0, 0x15

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780525
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co:J

    return-wide v0
.end method

.method private cu()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780526
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780527
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp:Ljava/lang/String;

    const/16 v1, 0xb0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp:Ljava/lang/String;

    .line 780528
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp:Ljava/lang/String;

    return-object v0
.end method

.method private cv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780529
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780530
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780531
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cw()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780532
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780533
    const/16 v0, 0x16

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780534
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr:I

    return v0
.end method

.method private cx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780535
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780536
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780537
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private cy()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780538
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ct:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780539
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ct:Ljava/util/List;

    const/16 v1, 0xb4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ct:Ljava/util/List;

    .line 780540
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ct:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private da()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW:Ljava/util/List;

    const/16 v1, 0xd2

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW:Ljava/util/List;

    .line 780429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private db()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779318
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779319
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xd3

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779320
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private dc()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779321
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779322
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xd4

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779323
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private dd()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779324
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779325
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xd5

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779326
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 779341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 171

    .prologue
    .line 779342
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 779343
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/String;)I

    move-result v4

    .line 779344
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 779345
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 779346
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 779347
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 779348
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 779349
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 779350
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 779351
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 779352
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->D()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 779353
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 779354
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 779355
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 779356
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->I()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 779357
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 779358
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 779359
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 779360
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 779361
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->N()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 779362
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 779363
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 779364
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 779365
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 779366
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 779367
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 779368
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 779369
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 779370
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 779371
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 779372
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 779373
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad()LX/0Px;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v34

    .line 779374
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->af()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 779375
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 779376
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 779377
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 779378
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 779379
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v40

    sget-object v41, LX/16Z;->a:LX/16Z;

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v40

    .line 779380
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 779381
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 779382
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 779383
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 779384
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap()LX/0Px;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v45

    .line 779385
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 779386
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar()LX/0Px;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v47

    .line 779387
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 779388
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 779389
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 779390
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 779391
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 779392
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aC()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 779393
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aD()LX/0Px;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v54

    .line 779394
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 779395
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 779396
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 779397
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aG()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 779398
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 779399
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 779400
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v61

    .line 779401
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 779402
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 779403
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aN()Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v64

    .line 779404
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 779405
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 779406
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aW()Ljava/lang/String;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v67

    .line 779407
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX()Ljava/lang/String;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v68

    .line 779408
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 779409
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o()LX/0Px;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v70

    .line 779410
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 779411
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 779412
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 779413
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 779414
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 779415
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 779416
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->be()Ljava/lang/String;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v77

    .line 779417
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 779418
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg()Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v79

    .line 779419
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh()Ljava/lang/String;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v80

    .line 779420
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v81

    .line 779421
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v82

    move-object/from16 v0, p1

    move-object/from16 v1, v82

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 779422
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v83

    move-object/from16 v0, p1

    move-object/from16 v1, v83

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v83

    .line 779423
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v84

    move-object/from16 v0, p1

    move-object/from16 v1, v84

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v84

    .line 779424
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn()LX/0Px;

    move-result-object v85

    move-object/from16 v0, p1

    move-object/from16 v1, v85

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v85

    .line 779425
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v86

    move-object/from16 v0, p1

    move-object/from16 v1, v86

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v86

    .line 779426
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v87

    move-object/from16 v0, p1

    move-object/from16 v1, v87

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v87

    .line 779427
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v88

    move-object/from16 v0, p1

    move-object/from16 v1, v88

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v88

    .line 779428
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v89

    move-object/from16 v0, p1

    move-object/from16 v1, v89

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v89

    .line 779429
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt()LX/0Px;

    move-result-object v90

    move-object/from16 v0, p1

    move-object/from16 v1, v90

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v90

    .line 779430
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu()LX/0Px;

    move-result-object v91

    move-object/from16 v0, p1

    move-object/from16 v1, v91

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v91

    .line 779431
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v92

    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v92

    .line 779432
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v93

    move-object/from16 v0, p1

    move-object/from16 v1, v93

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v93

    .line 779433
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v94

    move-object/from16 v0, p1

    move-object/from16 v1, v94

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v94

    .line 779434
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA()LX/0Px;

    move-result-object v95

    move-object/from16 v0, p1

    move-object/from16 v1, v95

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v95

    .line 779435
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB()LX/0Px;

    move-result-object v96

    move-object/from16 v0, p1

    move-object/from16 v1, v96

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v96

    .line 779436
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC()Ljava/lang/String;

    move-result-object v97

    move-object/from16 v0, p1

    move-object/from16 v1, v97

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v97

    .line 779437
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v98

    move-object/from16 v0, p1

    move-object/from16 v1, v98

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v98

    .line 779438
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v99

    move-object/from16 v0, p1

    move-object/from16 v1, v99

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v99

    .line 779439
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bF()LX/0Px;

    move-result-object v100

    move-object/from16 v0, p1

    move-object/from16 v1, v100

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v100

    .line 779440
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bG()Ljava/lang/String;

    move-result-object v101

    move-object/from16 v0, p1

    move-object/from16 v1, v101

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v101

    .line 779441
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bH()Ljava/lang/String;

    move-result-object v102

    move-object/from16 v0, p1

    move-object/from16 v1, v102

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v102

    .line 779442
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v103

    move-object/from16 v0, p1

    move-object/from16 v1, v103

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v103

    .line 779443
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK()LX/0Px;

    move-result-object v104

    move-object/from16 v0, p1

    move-object/from16 v1, v104

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v104

    .line 779444
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v105

    move-object/from16 v0, p1

    move-object/from16 v1, v105

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v105

    .line 779445
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v106

    move-object/from16 v0, p1

    move-object/from16 v1, v106

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v106

    .line 779446
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v107

    move-object/from16 v0, p1

    move-object/from16 v1, v107

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v107

    .line 779447
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v108

    move-object/from16 v0, p1

    move-object/from16 v1, v108

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v108

    .line 779448
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v109

    move-object/from16 v0, p1

    move-object/from16 v1, v109

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v109

    .line 779449
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT()Ljava/lang/String;

    move-result-object v110

    move-object/from16 v0, p1

    move-object/from16 v1, v110

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v110

    .line 779450
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v111

    move-object/from16 v0, p1

    move-object/from16 v1, v111

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v111

    .line 779451
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v112

    move-object/from16 v0, p1

    move-object/from16 v1, v112

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v112

    .line 779452
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bV()Ljava/lang/String;

    move-result-object v113

    move-object/from16 v0, p1

    move-object/from16 v1, v113

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v113

    .line 779453
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW()Ljava/lang/String;

    move-result-object v114

    move-object/from16 v0, p1

    move-object/from16 v1, v114

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v114

    .line 779454
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v115

    move-object/from16 v0, p1

    move-object/from16 v1, v115

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v115

    .line 779455
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v116

    move-object/from16 v0, p1

    move-object/from16 v1, v116

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v116

    .line 779456
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v117

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v117

    .line 779457
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v118

    move-object/from16 v0, p1

    move-object/from16 v1, v118

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v118

    .line 779458
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd()LX/0Px;

    move-result-object v119

    move-object/from16 v0, p1

    move-object/from16 v1, v119

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v119

    .line 779459
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce()Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    move-result-object v120

    move-object/from16 v0, p1

    move-object/from16 v1, v120

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v120

    .line 779460
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf()Ljava/lang/String;

    move-result-object v121

    move-object/from16 v0, p1

    move-object/from16 v1, v121

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v121

    .line 779461
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v122

    move-object/from16 v0, p1

    move-object/from16 v1, v122

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v122

    .line 779462
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch()Ljava/lang/String;

    move-result-object v123

    move-object/from16 v0, p1

    move-object/from16 v1, v123

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v123

    .line 779463
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v124

    move-object/from16 v0, p1

    move-object/from16 v1, v124

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v124

    .line 779464
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v125

    move-object/from16 v0, p1

    move-object/from16 v1, v125

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v125

    .line 779465
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl()LX/0Px;

    move-result-object v126

    move-object/from16 v0, p1

    move-object/from16 v1, v126

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v126

    .line 779466
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v127

    move-object/from16 v0, p1

    move-object/from16 v1, v127

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v127

    .line 779467
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v128

    move-object/from16 v0, p1

    move-object/from16 v1, v128

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v128

    .line 779468
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v129

    move-object/from16 v0, p1

    move-object/from16 v1, v129

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v129

    .line 779469
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v130

    move-object/from16 v0, p1

    move-object/from16 v1, v130

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v130

    .line 779470
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v131

    move-object/from16 v0, p1

    move-object/from16 v1, v131

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v131

    .line 779471
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v132

    move-object/from16 v0, p1

    move-object/from16 v1, v132

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v132

    .line 779472
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v133

    move-object/from16 v0, p1

    move-object/from16 v1, v133

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v133

    .line 779473
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs()Ljava/lang/String;

    move-result-object v134

    move-object/from16 v0, p1

    move-object/from16 v1, v134

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v134

    .line 779474
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu()Ljava/lang/String;

    move-result-object v135

    move-object/from16 v0, p1

    move-object/from16 v1, v135

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v135

    .line 779475
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v136

    move-object/from16 v0, p1

    move-object/from16 v1, v136

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v136

    .line 779476
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v137

    move-object/from16 v0, p1

    move-object/from16 v1, v137

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v137

    .line 779477
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy()LX/0Px;

    move-result-object v138

    move-object/from16 v0, p1

    move-object/from16 v1, v138

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v138

    .line 779478
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v139

    move-object/from16 v0, p1

    move-object/from16 v1, v139

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v139

    .line 779479
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v140

    move-object/from16 v0, p1

    move-object/from16 v1, v140

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v140

    .line 779480
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v141

    move-object/from16 v0, p1

    move-object/from16 v1, v141

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v141

    .line 779481
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC()Ljava/lang/String;

    move-result-object v142

    move-object/from16 v0, p1

    move-object/from16 v1, v142

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v142

    .line 779482
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v143

    move-object/from16 v0, p1

    move-object/from16 v1, v143

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v143

    .line 779483
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v144

    move-object/from16 v0, p1

    move-object/from16 v1, v144

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v144

    .line 779484
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v145

    move-object/from16 v0, p1

    move-object/from16 v1, v145

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v145

    .line 779485
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cG()Ljava/lang/String;

    move-result-object v146

    move-object/from16 v0, p1

    move-object/from16 v1, v146

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v146

    .line 779486
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH()Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    move-result-object v147

    move-object/from16 v0, p1

    move-object/from16 v1, v147

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v147

    .line 779487
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v148

    move-object/from16 v0, p1

    move-object/from16 v1, v148

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v148

    .line 779488
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v149

    move-object/from16 v0, p1

    move-object/from16 v1, v149

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v149

    .line 779489
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK()Ljava/lang/String;

    move-result-object v150

    move-object/from16 v0, p1

    move-object/from16 v1, v150

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v150

    .line 779490
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v151

    move-object/from16 v0, p1

    move-object/from16 v1, v151

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v151

    .line 779491
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM()LX/0Px;

    move-result-object v152

    move-object/from16 v0, p1

    move-object/from16 v1, v152

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v152

    .line 779492
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v153

    move-object/from16 v0, p1

    move-object/from16 v1, v153

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v153

    .line 779493
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v154

    move-object/from16 v0, p1

    move-object/from16 v1, v154

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v154

    .line 779494
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v155

    move-object/from16 v0, p1

    move-object/from16 v1, v155

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v155

    .line 779495
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v156

    move-object/from16 v0, p1

    move-object/from16 v1, v156

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v156

    .line 779496
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v157

    move-object/from16 v0, p1

    move-object/from16 v1, v157

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v157

    .line 779497
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS()LX/0Px;

    move-result-object v158

    move-object/from16 v0, p1

    move-object/from16 v1, v158

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v158

    .line 779498
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v159

    move-object/from16 v0, p1

    move-object/from16 v1, v159

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v159

    .line 779499
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v160

    move-object/from16 v0, p1

    move-object/from16 v1, v160

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v160

    .line 779500
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v161

    move-object/from16 v0, p1

    move-object/from16 v1, v161

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v161

    .line 779501
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v162

    move-object/from16 v0, p1

    move-object/from16 v1, v162

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v162

    .line 779502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX()LX/0Px;

    move-result-object v163

    move-object/from16 v0, p1

    move-object/from16 v1, v163

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v163

    .line 779503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v164

    move-object/from16 v0, p1

    move-object/from16 v1, v164

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v164

    .line 779504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v165

    move-object/from16 v0, p1

    move-object/from16 v1, v165

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v165

    .line 779505
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->da()LX/0Px;

    move-result-object v166

    move-object/from16 v0, p1

    move-object/from16 v1, v166

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v166

    .line 779506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->db()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v167

    move-object/from16 v0, p1

    move-object/from16 v1, v167

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v167

    .line 779507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dc()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v168

    move-object/from16 v0, p1

    move-object/from16 v1, v168

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v168

    .line 779508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dd()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v169

    move-object/from16 v0, p1

    move-object/from16 v1, v169

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v169

    .line 779509
    const/16 v170, 0xd6

    move-object/from16 v0, p1

    move/from16 v1, v170

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 779510
    const/16 v170, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v170

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 779511
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 779512
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 779513
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 779514
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 779515
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 779516
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 779517
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 779518
    const/16 v4, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 779519
    const/16 v4, 0x9

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 779520
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 779521
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 779522
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 779523
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 779524
    const/16 v5, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-ne v4, v6, :cond_1

    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779525
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779526
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779527
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779528
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779529
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779530
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779531
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779532
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779533
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779534
    const/16 v5, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v4, v6, :cond_2

    const/4 v4, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779535
    const/16 v5, 0x1c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779536
    const/16 v5, 0x1d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    if-ne v4, v6, :cond_3

    const/4 v4, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779537
    const/16 v5, 0x1e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    if-ne v4, v6, :cond_4

    const/4 v4, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779538
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779539
    const/16 v4, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779540
    const/16 v4, 0x21

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 779541
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779542
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779543
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779544
    const/16 v4, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779545
    const/16 v4, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779546
    const/16 v4, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779547
    const/16 v4, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779548
    const/16 v4, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779549
    const/16 v5, 0x2a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v4, v6, :cond_5

    const/4 v4, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779550
    const/16 v4, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779551
    const/16 v4, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779552
    const/16 v4, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779553
    const/16 v4, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779554
    const/16 v4, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779555
    const/16 v4, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779556
    const/16 v4, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779557
    const/16 v4, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779558
    const/16 v4, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779559
    const/16 v4, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779560
    const/16 v4, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779561
    const/16 v4, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779562
    const/16 v4, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779563
    const/16 v4, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779564
    const/16 v4, 0x3a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779565
    const/16 v4, 0x3b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->au()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779566
    const/16 v4, 0x3c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779567
    const/16 v4, 0x3d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779568
    const/16 v5, 0x3e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v4, v6, :cond_6

    const/4 v4, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779569
    const/16 v4, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779570
    const/16 v4, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779571
    const/16 v4, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779572
    const/16 v4, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779573
    const/16 v4, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779574
    const/16 v4, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779575
    const/16 v4, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779576
    const/16 v4, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779577
    const/16 v4, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779578
    const/16 v4, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779579
    const/16 v4, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779580
    const/16 v4, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779581
    const/16 v5, 0x4d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne v4, v6, :cond_7

    const/4 v4, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779582
    const/16 v4, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779583
    const/16 v4, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779584
    const/16 v4, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779585
    const/16 v5, 0x51

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    if-ne v4, v6, :cond_8

    const/4 v4, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779586
    const/16 v4, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779587
    const/16 v4, 0x53

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779588
    const/16 v4, 0x54

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779589
    const/16 v4, 0x55

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779590
    const/16 v4, 0x56

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779591
    const/16 v4, 0x57

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779592
    const/16 v4, 0x58

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779593
    const/16 v4, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779594
    const/16 v4, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779595
    const/16 v4, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779596
    const/16 v4, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779597
    const/16 v5, 0x5d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aY()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779598
    const/16 v4, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779599
    const/16 v4, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779600
    const/16 v4, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779601
    const/16 v4, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779602
    const/16 v4, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779603
    const/16 v4, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779604
    const/16 v4, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779605
    const/16 v4, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779606
    const/16 v4, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779607
    const/16 v4, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779608
    const/16 v4, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779609
    const/16 v4, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779610
    const/16 v4, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779611
    const/16 v4, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779612
    const/16 v4, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779613
    const/16 v4, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779614
    const/16 v4, 0x6e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 779615
    const/16 v4, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779616
    const/16 v4, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779617
    const/16 v4, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779618
    const/16 v5, 0x72

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779619
    const/16 v4, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779620
    const/16 v4, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779621
    const/16 v4, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779622
    const/16 v4, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779623
    const/16 v4, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779624
    const/16 v4, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779625
    const/16 v5, 0x79

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779626
    const/16 v4, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779627
    const/16 v5, 0x7b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v4, v6, :cond_9

    const/4 v4, 0x0

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779628
    const/16 v4, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779629
    const/16 v4, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779630
    const/16 v4, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779631
    const/16 v4, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v98

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779632
    const/16 v4, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v99

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779633
    const/16 v4, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v100

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779634
    const/16 v4, 0x84

    move-object/from16 v0, p1

    move/from16 v1, v101

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779635
    const/16 v4, 0x85

    move-object/from16 v0, p1

    move/from16 v1, v102

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779636
    const/16 v5, 0x86

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    if-ne v4, v6, :cond_a

    const/4 v4, 0x0

    :goto_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779637
    const/16 v4, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v103

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779638
    const/16 v4, 0x88

    move-object/from16 v0, p1

    move/from16 v1, v104

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779639
    const/16 v5, 0x89

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779640
    const/16 v4, 0x8a

    move-object/from16 v0, p1

    move/from16 v1, v105

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779641
    const/16 v5, 0x8b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779642
    const/16 v5, 0x8c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779643
    const/16 v5, 0x8d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne v4, v6, :cond_b

    const/4 v4, 0x0

    :goto_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779644
    const/16 v4, 0x8e

    move-object/from16 v0, p1

    move/from16 v1, v106

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779645
    const/16 v4, 0x8f

    move-object/from16 v0, p1

    move/from16 v1, v107

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779646
    const/16 v4, 0x90

    move-object/from16 v0, p1

    move/from16 v1, v108

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779647
    const/16 v4, 0x91

    move-object/from16 v0, p1

    move/from16 v1, v109

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779648
    const/16 v4, 0x92

    move-object/from16 v0, p1

    move/from16 v1, v110

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779649
    const/16 v4, 0x93

    move-object/from16 v0, p1

    move/from16 v1, v111

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779650
    const/16 v4, 0x94

    move-object/from16 v0, p1

    move/from16 v1, v112

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779651
    const/16 v4, 0x95

    move-object/from16 v0, p1

    move/from16 v1, v113

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779652
    const/16 v4, 0x96

    move-object/from16 v0, p1

    move/from16 v1, v114

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779653
    const/16 v5, 0x97

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779654
    const/16 v4, 0x98

    move-object/from16 v0, p1

    move/from16 v1, v115

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779655
    const/16 v4, 0x99

    move-object/from16 v0, p1

    move/from16 v1, v116

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779656
    const/16 v4, 0x9a

    move-object/from16 v0, p1

    move/from16 v1, v117

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779657
    const/16 v5, 0x9b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    if-ne v4, v6, :cond_c

    const/4 v4, 0x0

    :goto_c
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 779658
    const/16 v4, 0x9c

    move-object/from16 v0, p1

    move/from16 v1, v118

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779659
    const/16 v4, 0x9d

    move-object/from16 v0, p1

    move/from16 v1, v119

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779660
    const/16 v4, 0x9e

    move-object/from16 v0, p1

    move/from16 v1, v120

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779661
    const/16 v4, 0x9f

    move-object/from16 v0, p1

    move/from16 v1, v121

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779662
    const/16 v4, 0xa0

    move-object/from16 v0, p1

    move/from16 v1, v122

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779663
    const/16 v4, 0xa1

    move-object/from16 v0, p1

    move/from16 v1, v123

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779664
    const/16 v4, 0xa2

    move-object/from16 v0, p1

    move/from16 v1, v124

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779665
    const/16 v4, 0xa3

    move-object/from16 v0, p1

    move/from16 v1, v125

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779666
    const/16 v5, 0xa4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 779667
    const/16 v4, 0xa5

    move-object/from16 v0, p1

    move/from16 v1, v126

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779668
    const/16 v4, 0xa6

    move-object/from16 v0, p1

    move/from16 v1, v127

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779669
    const/16 v4, 0xa8

    move-object/from16 v0, p1

    move/from16 v1, v128

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779670
    const/16 v4, 0xa9

    move-object/from16 v0, p1

    move/from16 v1, v129

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779671
    const/16 v4, 0xaa

    move-object/from16 v0, p1

    move/from16 v1, v130

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779672
    const/16 v4, 0xab

    move-object/from16 v0, p1

    move/from16 v1, v131

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779673
    const/16 v4, 0xac

    move-object/from16 v0, p1

    move/from16 v1, v132

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779674
    const/16 v4, 0xad

    move-object/from16 v0, p1

    move/from16 v1, v133

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779675
    const/16 v4, 0xae

    move-object/from16 v0, p1

    move/from16 v1, v134

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779676
    const/16 v5, 0xaf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ct()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 779677
    const/16 v4, 0xb0

    move-object/from16 v0, p1

    move/from16 v1, v135

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779678
    const/16 v4, 0xb1

    move-object/from16 v0, p1

    move/from16 v1, v136

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779679
    const/16 v4, 0xb2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 779680
    const/16 v4, 0xb3

    move-object/from16 v0, p1

    move/from16 v1, v137

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779681
    const/16 v4, 0xb4

    move-object/from16 v0, p1

    move/from16 v1, v138

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779682
    const/16 v4, 0xb5

    move-object/from16 v0, p1

    move/from16 v1, v139

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779683
    const/16 v4, 0xb6

    move-object/from16 v0, p1

    move/from16 v1, v140

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779684
    const/16 v4, 0xb7

    move-object/from16 v0, p1

    move/from16 v1, v141

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779685
    const/16 v4, 0xb8

    move-object/from16 v0, p1

    move/from16 v1, v142

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779686
    const/16 v4, 0xb9

    move-object/from16 v0, p1

    move/from16 v1, v143

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779687
    const/16 v4, 0xba

    move-object/from16 v0, p1

    move/from16 v1, v144

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779688
    const/16 v4, 0xbb

    move-object/from16 v0, p1

    move/from16 v1, v145

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779689
    const/16 v4, 0xbd

    move-object/from16 v0, p1

    move/from16 v1, v146

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779690
    const/16 v4, 0xbe

    move-object/from16 v0, p1

    move/from16 v1, v147

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779691
    const/16 v4, 0xbf

    move-object/from16 v0, p1

    move/from16 v1, v148

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779692
    const/16 v4, 0xc0

    move-object/from16 v0, p1

    move/from16 v1, v149

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779693
    const/16 v4, 0xc1

    move-object/from16 v0, p1

    move/from16 v1, v150

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779694
    const/16 v4, 0xc2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 779695
    const/16 v4, 0xc3

    move-object/from16 v0, p1

    move/from16 v1, v151

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779696
    const/16 v4, 0xc4

    move-object/from16 v0, p1

    move/from16 v1, v152

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779697
    const/16 v4, 0xc5

    move-object/from16 v0, p1

    move/from16 v1, v153

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779698
    const/16 v4, 0xc6

    move-object/from16 v0, p1

    move/from16 v1, v154

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779699
    const/16 v4, 0xc7

    move-object/from16 v0, p1

    move/from16 v1, v155

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779700
    const/16 v4, 0xc8

    move-object/from16 v0, p1

    move/from16 v1, v156

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779701
    const/16 v4, 0xc9

    move-object/from16 v0, p1

    move/from16 v1, v157

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779702
    const/16 v4, 0xca

    move-object/from16 v0, p1

    move/from16 v1, v158

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779703
    const/16 v4, 0xcb

    move-object/from16 v0, p1

    move/from16 v1, v159

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779704
    const/16 v4, 0xcc

    move-object/from16 v0, p1

    move/from16 v1, v160

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779705
    const/16 v4, 0xcd

    move-object/from16 v0, p1

    move/from16 v1, v161

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779706
    const/16 v4, 0xce

    move-object/from16 v0, p1

    move/from16 v1, v162

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779707
    const/16 v4, 0xcf

    move-object/from16 v0, p1

    move/from16 v1, v163

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779708
    const/16 v4, 0xd0

    move-object/from16 v0, p1

    move/from16 v1, v164

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779709
    const/16 v4, 0xd1

    move-object/from16 v0, p1

    move/from16 v1, v165

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779710
    const/16 v4, 0xd2

    move-object/from16 v0, p1

    move/from16 v1, v166

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779711
    const/16 v4, 0xd3

    move-object/from16 v0, p1

    move/from16 v1, v167

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779712
    const/16 v4, 0xd4

    move-object/from16 v0, p1

    move/from16 v1, v168

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779713
    const/16 v4, 0xd5

    move-object/from16 v0, p1

    move/from16 v1, v169

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 779714
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 779715
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4

    .line 779716
    :cond_0
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 779717
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v4

    goto/16 :goto_1

    .line 779718
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    goto/16 :goto_2

    .line 779719
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->R()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v4

    goto/16 :goto_3

    .line 779720
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->S()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    move-result-object v4

    goto/16 :goto_4

    .line 779721
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v4

    goto/16 :goto_5

    .line 779722
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ax()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    goto/16 :goto_6

    .line 779723
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v4

    goto/16 :goto_7

    .line 779724
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aM()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    move-result-object v4

    goto/16 :goto_8

    .line 779725
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bz()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v4

    goto/16 :goto_9

    .line 779726
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v4

    goto/16 :goto_a

    .line 779727
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v4

    goto/16 :goto_b

    .line 779728
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cb()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    move-result-object v4

    goto/16 :goto_c
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 779729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 779730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 779731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 779733
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779734
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779735
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 779736
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779737
    if-eqz v2, :cond_1

    .line 779738
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779739
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI:Ljava/util/List;

    move-object v1, v0

    .line 779740
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 779741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 779743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779745
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 779746
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779747
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 779748
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779749
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779750
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 779751
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779752
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 779753
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779754
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779755
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 779756
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779757
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 779758
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779759
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779760
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 779761
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779762
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 779763
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779764
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779765
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 779766
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 779767
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->z()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 779768
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779769
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 779770
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 779771
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 779772
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 779773
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779774
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 779775
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 779776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779777
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 779778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779779
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779780
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 779781
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 779783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779784
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779785
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 779786
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779787
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 779788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779789
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cK:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779790
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 779791
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 779792
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->E()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 779793
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779794
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 779795
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 779796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 779798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779799
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779800
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 779801
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779802
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 779803
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779804
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cL:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779805
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 779806
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 779808
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779809
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779810
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 779811
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 779812
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 779813
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779814
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->x:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 779815
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 779816
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 779817
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 779818
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779819
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->y:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779820
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 779821
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779822
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 779823
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779824
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779825
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 779826
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779827
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 779828
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779829
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cM:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779830
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 779831
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 779832
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 779833
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779834
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->B:Lcom/facebook/graphql/model/GraphQLComment;

    .line 779835
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 779836
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779837
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 779838
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779839
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779840
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 779841
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779842
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 779843
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779844
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779845
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 779846
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779847
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 779848
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779849
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779850
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 779851
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779852
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 779853
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779854
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->L:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779855
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 779856
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779857
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 779858
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779859
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779860
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 779861
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779862
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 779863
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779864
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779865
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 779866
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779867
    if-eqz v2, :cond_1b

    .line 779868
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779869
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Q:Ljava/util/List;

    move-object v1, v0

    .line 779870
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 779871
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779872
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 779873
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779874
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779875
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 779876
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779877
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 779878
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779879
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cN:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 779880
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 779881
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779882
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 779883
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779884
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779885
    :cond_1e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 779886
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779887
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 779888
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779889
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->V:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779890
    :cond_1f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 779891
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779892
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 779893
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779894
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779895
    :cond_20
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 779896
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 779897
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ak()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 779898
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779899
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->X:Lcom/facebook/graphql/model/FeedUnit;

    .line 779900
    :cond_21
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 779901
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 779903
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779904
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779905
    :cond_22
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 779906
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779907
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 779908
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779909
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->Z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779910
    :cond_23
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 779911
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 779912
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 779913
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779914
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aa:Lcom/facebook/graphql/model/GraphQLPage;

    .line 779915
    :cond_24
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 779916
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779917
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 779918
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779919
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779920
    :cond_25
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 779921
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779922
    if-eqz v2, :cond_26

    .line 779923
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779924
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cO:Ljava/util/List;

    move-object v1, v0

    .line 779925
    :cond_26
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 779926
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ap()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779927
    if-eqz v2, :cond_27

    .line 779928
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779929
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ac:Ljava/util/List;

    move-object v1, v0

    .line 779930
    :cond_27
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 779931
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 779932
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 779933
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779934
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ad:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 779935
    :cond_28
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 779936
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779937
    if-eqz v2, :cond_29

    .line 779938
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779939
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ae:Ljava/util/List;

    move-object v1, v0

    .line 779940
    :cond_29
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 779941
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779942
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 779943
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779944
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779945
    :cond_2a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 779946
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779947
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 779948
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779949
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779950
    :cond_2b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 779951
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779952
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 779953
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779954
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779955
    :cond_2c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 779956
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779957
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 779958
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779959
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779960
    :cond_2d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aD()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 779961
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aD()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779962
    if-eqz v2, :cond_2e

    .line 779963
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779964
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aq:Ljava/util/List;

    move-object v1, v0

    .line 779965
    :cond_2e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 779966
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779967
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 779968
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779969
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779970
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 779971
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 779973
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779974
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779975
    :cond_30
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 779976
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779977
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 779978
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779979
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779980
    :cond_31
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 779981
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779982
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 779983
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779984
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779985
    :cond_32
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 779986
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 779987
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 779988
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779989
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779990
    :cond_33
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 779991
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 779992
    if-eqz v2, :cond_34

    .line 779993
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779994
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay:Ljava/util/List;

    move-object v1, v0

    .line 779995
    :cond_34
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 779996
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 779997
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 779998
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 779999
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->az:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780000
    :cond_35
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 780001
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780002
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 780003
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780004
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cP:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780005
    :cond_36
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 780006
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780007
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 780008
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780009
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cQ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780010
    :cond_37
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 780011
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780012
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aL()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 780013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780014
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aA:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780015
    :cond_38
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 780016
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780017
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 780018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780019
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cR:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780020
    :cond_39
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 780021
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 780022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 780023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780024
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aJ:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 780025
    :cond_3a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 780026
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3b

    .line 780028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780029
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780030
    :cond_3b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 780031
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 780032
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aZ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_3c

    .line 780033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780034
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aO:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 780035
    :cond_3c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 780036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780037
    if-eqz v2, :cond_3d

    .line 780038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780039
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP:Ljava/util/List;

    move-object v1, v0

    .line 780040
    :cond_3d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3e

    .line 780041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3e

    .line 780043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780044
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780045
    :cond_3e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    if-eqz v0, :cond_3f

    .line 780046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 780047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    if-eq v2, v0, :cond_3f

    .line 780048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 780050
    :cond_3f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 780051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780052
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_40

    .line 780053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780054
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780055
    :cond_40
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 780056
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780057
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_41

    .line 780058
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780059
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780060
    :cond_41
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 780061
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 780062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v2

    if-eq v2, v0, :cond_42

    .line 780063
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780064
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aU:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 780065
    :cond_42
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 780066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_43

    .line 780068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780069
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780070
    :cond_43
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_44

    .line 780071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780072
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_44

    .line 780073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780074
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780075
    :cond_44
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 780076
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780077
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_45

    .line 780078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780079
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cS:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780080
    :cond_45
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_46

    .line 780081
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 780082
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v2

    if-eq v2, v0, :cond_46

    .line 780083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780084
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ba:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 780085
    :cond_46
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_47

    .line 780086
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780087
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_47

    .line 780088
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780089
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780090
    :cond_47
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_48

    .line 780091
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780092
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_48

    .line 780093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780094
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780095
    :cond_48
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 780096
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 780097
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_49

    .line 780098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780099
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bd:Lcom/facebook/graphql/model/GraphQLStory;

    .line 780100
    :cond_49
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4a

    .line 780101
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780102
    if-eqz v2, :cond_4a

    .line 780103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780104
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cT:Ljava/util/List;

    move-object v1, v0

    .line 780105
    :cond_4a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4b

    .line 780106
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780107
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4b

    .line 780108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780109
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780110
    :cond_4b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 780111
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 780112
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_4c

    .line 780113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780114
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bh:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780115
    :cond_4c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_4d

    .line 780116
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780117
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_4d

    .line 780118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780119
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cU:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780120
    :cond_4d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4e

    .line 780121
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780122
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->br()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4e

    .line 780123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780124
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780125
    :cond_4e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_4f

    .line 780126
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 780127
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_4f

    .line 780128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780129
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bk:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 780130
    :cond_4f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_50

    .line 780131
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780132
    if-eqz v2, :cond_50

    .line 780133
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780134
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bl:Ljava/util/List;

    move-object v1, v0

    .line 780135
    :cond_50
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_51

    .line 780136
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bu()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780137
    if-eqz v2, :cond_51

    .line 780138
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780139
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bm:Ljava/util/List;

    move-object v1, v0

    .line 780140
    :cond_51
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_52

    .line 780141
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780142
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_52

    .line 780143
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780144
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bn:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780145
    :cond_52
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_53

    .line 780146
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 780147
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_53

    .line 780148
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780149
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bo:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780150
    :cond_53
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_54

    .line 780151
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 780152
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->by()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_54

    .line 780153
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780154
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bq:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780155
    :cond_54
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_55

    .line 780156
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bA()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780157
    if-eqz v2, :cond_55

    .line 780158
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780159
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bs:Ljava/util/List;

    move-object v1, v0

    .line 780160
    :cond_55
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_56

    .line 780161
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780162
    if-eqz v2, :cond_56

    .line 780163
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780164
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bt:Ljava/util/List;

    move-object v1, v0

    .line 780165
    :cond_56
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_57

    .line 780166
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 780167
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_57

    .line 780168
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780169
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bv:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 780170
    :cond_57
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_58

    .line 780171
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 780172
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_58

    .line 780173
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780174
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780175
    :cond_58
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bF()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_59

    .line 780176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bF()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780177
    if-eqz v2, :cond_59

    .line 780178
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780179
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bx:Ljava/util/List;

    move-object v1, v0

    .line 780180
    :cond_59
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_5a

    .line 780181
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780182
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_5a

    .line 780183
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780184
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cV:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780185
    :cond_5a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5b

    .line 780186
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780187
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5b

    .line 780188
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780189
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780190
    :cond_5b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5c

    .line 780191
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780192
    if-eqz v2, :cond_5c

    .line 780193
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780194
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bC:Ljava/util/List;

    move-object v1, v0

    .line 780195
    :cond_5c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5d

    .line 780196
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780197
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5d

    .line 780198
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780199
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780200
    :cond_5d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5e

    .line 780201
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780202
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5e

    .line 780203
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780204
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780205
    :cond_5e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_5f

    .line 780206
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 780207
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_5f

    .line 780208
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780209
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bJ:Lcom/facebook/graphql/model/GraphQLUser;

    .line 780210
    :cond_5f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_60

    .line 780211
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780212
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_60

    .line 780213
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780214
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780215
    :cond_60
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_61

    .line 780216
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780217
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_61

    .line 780218
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780219
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780220
    :cond_61
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_62

    .line 780221
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780222
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_62

    .line 780223
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780224
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780225
    :cond_62
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_63

    .line 780226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_63

    .line 780228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780229
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780230
    :cond_63
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_64

    .line 780231
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780232
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_64

    .line 780233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780234
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780235
    :cond_64
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_65

    .line 780236
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780237
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bZ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_65

    .line 780238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780239
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780240
    :cond_65
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_66

    .line 780241
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 780242
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_66

    .line 780243
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780244
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bU:Lcom/facebook/graphql/model/GraphQLPage;

    .line 780245
    :cond_66
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->da()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_67

    .line 780246
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->da()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780247
    if-eqz v2, :cond_67

    .line 780248
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780249
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cW:Ljava/util/List;

    move-object v1, v0

    .line 780250
    :cond_67
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->db()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_68

    .line 780251
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->db()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780252
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->db()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_68

    .line 780253
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780254
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cX:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780255
    :cond_68
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    if-eqz v0, :cond_69

    .line 780256
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 780257
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v2

    if-eq v2, v0, :cond_69

    .line 780258
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780259
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bW:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 780260
    :cond_69
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6a

    .line 780261
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780262
    if-eqz v2, :cond_6a

    .line 780263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780264
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bX:Ljava/util/List;

    move-object v1, v0

    .line 780265
    :cond_6a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce()Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 780266
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce()Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 780267
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce()Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    move-result-object v2

    if-eq v2, v0, :cond_6b

    .line 780268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780269
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bY:Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 780270
    :cond_6b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6c

    .line 780271
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780272
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6c

    .line 780273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780274
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ca:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780275
    :cond_6c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_6d

    .line 780276
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 780277
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_6d

    .line 780278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780279
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cc:Lcom/facebook/graphql/model/GraphQLStory;

    .line 780280
    :cond_6d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v0

    if-eqz v0, :cond_6e

    .line 780281
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780282
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj()Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-result-object v2

    if-eq v2, v0, :cond_6e

    .line 780283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780284
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cd:Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780285
    :cond_6e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6f

    .line 780286
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 780287
    if-eqz v2, :cond_6f

    .line 780288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780289
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cf:Ljava/util/List;

    move-object v1, v0

    .line 780290
    :cond_6f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_70

    .line 780291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_70

    .line 780293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780294
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780295
    :cond_70
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_71

    .line 780296
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780297
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_71

    .line 780298
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780299
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ch:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780300
    :cond_71
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_72

    .line 780301
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780302
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_72

    .line 780303
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780304
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ci:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780305
    :cond_72
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_73

    .line 780306
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780307
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_73

    .line 780308
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780309
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780310
    :cond_73
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_74

    .line 780311
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780312
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_74

    .line 780313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780314
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ck:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780315
    :cond_74
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_75

    .line 780316
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780317
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_75

    .line 780318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780319
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780320
    :cond_75
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_76

    .line 780321
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780322
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_76

    .line 780323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780324
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780325
    :cond_76
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_77

    .line 780326
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780327
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_77

    .line 780328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780329
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780330
    :cond_77
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_78

    .line 780331
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780332
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_78

    .line 780333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780334
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780335
    :cond_78
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_79

    .line 780336
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780337
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_79

    .line 780338
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780339
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780340
    :cond_79
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7a

    .line 780341
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780342
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7a

    .line 780343
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780344
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780345
    :cond_7a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7b

    .line 780346
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780347
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7b

    .line 780348
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780349
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780350
    :cond_7b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7c

    .line 780351
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780352
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7c

    .line 780353
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780354
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cy:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780355
    :cond_7c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_7d

    .line 780356
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 780357
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_7d

    .line 780358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780359
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cz:Lcom/facebook/graphql/model/GraphQLActor;

    .line 780360
    :cond_7d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_7e

    .line 780361
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 780362
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cF()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_7e

    .line 780363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780364
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cA:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 780365
    :cond_7e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH()Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    move-result-object v0

    if-eqz v0, :cond_7f

    .line 780366
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH()Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    .line 780367
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH()Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    move-result-object v2

    if-eq v2, v0, :cond_7f

    .line 780368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780369
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cC:Lcom/facebook/graphql/model/GraphQLVideoNotificationContextInfo;

    .line 780370
    :cond_7f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_80

    .line 780371
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780372
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_80

    .line 780373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780374
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780375
    :cond_80
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_81

    .line 780376
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780377
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_81

    .line 780378
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780379
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780380
    :cond_81
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dc()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_82

    .line 780381
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dc()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780382
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dc()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_82

    .line 780383
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780384
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cY:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780385
    :cond_82
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dd()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_83

    .line 780386
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dd()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780387
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->dd()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_83

    .line 780388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 780389
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cZ:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780390
    :cond_83
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 780391
    if-nez v1, :cond_84

    :goto_0
    return-object p0

    :cond_84
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780392
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 780393
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 780394
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 780395
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 780396
    const/4 v0, 0x0

    .line 780397
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 780398
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 780399
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m:I

    .line 780400
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n:I

    .line 780401
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->D:D

    .line 780402
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->I:I

    .line 780403
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ag:Z

    .line 780404
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ah:Z

    .line 780405
    const/16 v0, 0x3c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ai:Z

    .line 780406
    const/16 v0, 0x3d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aj:Z

    .line 780407
    const/16 v0, 0x53

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aD:Z

    .line 780408
    const/16 v0, 0x54

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aE:Z

    .line 780409
    const/16 v0, 0x55

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aF:Z

    .line 780410
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aG:Z

    .line 780411
    const/16 v0, 0x57

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aH:Z

    .line 780412
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aI:Z

    .line 780413
    const/16 v0, 0x5d

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aN:D

    .line 780414
    const/16 v0, 0x6e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->be:Z

    .line 780415
    const/16 v0, 0x72

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bi:D

    .line 780416
    const/16 v0, 0x79

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bp:D

    .line 780417
    const/16 v0, 0x89

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bD:D

    .line 780418
    const/16 v0, 0x8b

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bF:D

    .line 780419
    const/16 v0, 0x8c

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bG:D

    .line 780420
    const/16 v0, 0x97

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bR:D

    .line 780421
    const/16 v0, 0xa4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ce:D

    .line 780422
    const/16 v0, 0xaf

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->co:J

    .line 780423
    const/16 v0, 0xb2

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cr:I

    .line 780424
    const/16 v0, 0xc2

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cG:I

    .line 780425
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 780426
    const v0, -0x2605f7f0

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 779315
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 779316
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 779317
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780430
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780431
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780432
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m:I

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780433
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780434
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 780435
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->C:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780436
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780437
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 780438
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->as:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780439
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780440
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay:Ljava/util/List;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay:Ljava/util/List;

    .line 780441
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->ay:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780442
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780443
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP:Ljava/util/List;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP:Ljava/util/List;

    .line 780444
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aP:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780445
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780446
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 780447
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aR:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780448
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780449
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x65

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780450
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->aV:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 780451
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 780452
    const/16 v0, 0x11

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 780453
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bG:D

    return-wide v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780454
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780455
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x94

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780456
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->bO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 780459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 780460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 780461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/16 v1, 0xc3

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 780462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->cH:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method
