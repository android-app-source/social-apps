.class public final Lcom/facebook/graphql/model/GraphQLPageAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageAction$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLPageAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 770763
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 770754
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 770755
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 770756
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770757
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 770758
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 770759
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 770760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 770761
    const/4 v0, 0x0

    .line 770762
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 770764
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->f:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770765
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->f:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->f:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 770766
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->f:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770770
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770771
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 770772
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 770767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->h:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->h:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->h:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    .line 770769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->h:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770776
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770777
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770778
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770773
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770774
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770775
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770748
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770749
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->l:Ljava/lang/String;

    .line 770750
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->m:Ljava/lang/String;

    .line 770753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->n:Ljava/lang/String;

    .line 770655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLPageAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->o:Lcom/facebook/graphql/model/GraphQLPageAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->o:Lcom/facebook/graphql/model/GraphQLPageAction;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->o:Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->o:Lcom/facebook/graphql/model/GraphQLPageAction;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->p:Ljava/lang/String;

    .line 770649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->p:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770644
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770645
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->q:Ljava/lang/String;

    .line 770646
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->q:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 770656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->s:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->s:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->s:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 770658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->s:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLPageCallToAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 770661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 770745
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 770746
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770747
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageAction;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 770662
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 770663
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v1

    .line 770664
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 770665
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 770666
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 770667
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 770668
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 770669
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 770670
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->r()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 770671
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->s()Lcom/facebook/graphql/model/GraphQLPageAction;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 770672
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->t()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 770673
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 770674
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 770675
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->x()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 770676
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 770677
    const/16 v15, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 770678
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 770679
    const/4 v15, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->j()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    sget-object v16, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object/from16 v0, v16

    if-ne v1, v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 770680
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 770681
    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->l()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v1

    sget-object v15, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-ne v1, v15, :cond_2

    const/4 v1, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 770682
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 770683
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 770684
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 770685
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 770686
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 770687
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 770688
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 770689
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 770690
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 770691
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 770692
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->w()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v3, :cond_3

    const/4 v1, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 770693
    const/16 v1, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 770694
    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 770695
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 770696
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1

    .line 770697
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 770698
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->j()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    goto/16 :goto_1

    .line 770699
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->l()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v1

    goto :goto_2

    .line 770700
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->w()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    goto :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 770701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 770702
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->s()Lcom/facebook/graphql/model/GraphQLPageAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 770703
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->s()Lcom/facebook/graphql/model/GraphQLPageAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770704
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->s()Lcom/facebook/graphql/model/GraphQLPageAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 770705
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770706
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->o:Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770707
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->x()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 770708
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->x()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 770709
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->x()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 770710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770711
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->t:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 770712
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 770713
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770714
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 770715
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770716
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770717
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 770718
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770719
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 770720
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770721
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770722
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 770723
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770724
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 770725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770726
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770727
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 770728
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770729
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 770730
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770731
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770732
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 770733
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 770734
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 770735
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770736
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 770737
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 770738
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770739
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageAction;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 770740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageAction;

    .line 770741
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageAction;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 770742
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 770743
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 770744
    const v0, -0x5c1bbe3b

    return v0
.end method
