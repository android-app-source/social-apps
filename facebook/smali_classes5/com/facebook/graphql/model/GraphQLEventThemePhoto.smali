.class public final Lcom/facebook/graphql/model/GraphQLEventThemePhoto;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventThemePhoto$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventThemePhoto$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749646
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749647
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 749648
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 749649
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749640
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749641
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749642
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749643
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749644
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749645
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o:Ljava/lang/String;

    .line 749633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 749605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749606
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 749607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 749608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 749609
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 749610
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 749611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 749612
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 749613
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 749614
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 749615
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 749616
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->t()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 749617
    const/16 v11, 0xc

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 749618
    const/4 v11, 0x1

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 749619
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 749620
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 749621
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 749622
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 749623
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 749624
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 749625
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 749626
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 749627
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 749628
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 749629
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749630
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 749557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749558
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 749559
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749560
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 749561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749562
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749563
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 749564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 749566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749567
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749568
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 749569
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749570
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 749571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749572
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749573
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 749574
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749575
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 749576
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749577
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749578
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 749579
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749580
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 749581
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749582
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749583
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 749584
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749585
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 749586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749587
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749588
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 749589
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749590
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 749591
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749592
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749593
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 749594
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749595
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 749596
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749597
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749598
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 749599
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 749600
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 749601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 749602
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749603
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749604
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 749555
    const v0, -0x3543e3fd    # -6163969.5f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749552
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749553
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 749554
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749549
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749550
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->g:Ljava/lang/String;

    .line 749551
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->g:Ljava/lang/String;

    return-object v0
.end method
