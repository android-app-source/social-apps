.class public Lcom/facebook/graphql/model/GraphQLMapTile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMapTileDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMapTileSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final backgroundPlaces:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "placesRenderPriority2"
    .end annotation
.end field

.field public final bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bounds"
    .end annotation
.end field

.field public final creationTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creation_time"
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tile_key"
    .end annotation
.end field

.field public final maxZoom:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_zoom"
    .end annotation
.end field

.field public final minZoom:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "min_zoom"
    .end annotation
.end field

.field public final places:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "placesRenderPriority1"
    .end annotation
.end field

.field public final timeToLiveInSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ttl"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764742
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764718
    const-class v0, Lcom/facebook/graphql/model/GraphQLMapTileSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 764741
    new-instance v0, LX/4X9;

    invoke-direct {v0}, LX/4X9;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/GraphQLMapTile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 764731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764732
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->id:Ljava/lang/String;

    .line 764733
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 764734
    iput-wide v2, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->creationTime:J

    .line 764735
    iput-wide v2, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->timeToLiveInSeconds:J

    .line 764736
    iput v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->minZoom:F

    .line 764737
    iput v1, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->maxZoom:F

    .line 764738
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->places:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    .line 764739
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->backgroundPlaces:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    .line 764740
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 764721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764722
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->id:Ljava/lang/String;

    .line 764723
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->creationTime:J

    .line 764724
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->timeToLiveInSeconds:J

    .line 764725
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 764726
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->minZoom:F

    .line 764727
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->maxZoom:F

    .line 764728
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->places:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    .line 764729
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->backgroundPlaces:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    .line 764730
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 764720
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 764719
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->creationTime:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 764743
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->timeToLiveInSeconds:J

    return-wide v0
.end method

.method public final d()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 1

    .prologue
    .line 764703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 764704
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 764705
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->minZoom:F

    return v0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 764706
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->maxZoom:F

    return v0
.end method

.method public final g()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;
    .locals 1

    .prologue
    .line 764707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->places:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;
    .locals 1

    .prologue
    .line 764708
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMapTile;->backgroundPlaces:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 764709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 764710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 764711
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->c()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 764712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->d()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 764713
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->e()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 764714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->f()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 764715
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->g()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 764716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMapTile;->h()Lcom/facebook/graphql/model/GraphQLPlacesTileResultsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 764717
    return-void
.end method
