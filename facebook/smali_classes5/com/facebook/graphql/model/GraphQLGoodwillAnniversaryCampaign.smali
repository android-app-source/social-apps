.class public final Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754934
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754933
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 754778
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 754779
    return-void
.end method

.method private t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754930
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754931
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f:Ljava/util/List;

    .line 754932
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLMediaSet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m:Lcom/facebook/graphql/model/GraphQLMediaSet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m:Lcom/facebook/graphql/model/GraphQLMediaSet;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 754929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m:Lcom/facebook/graphql/model/GraphQLMediaSet;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754924
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754925
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754926
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754921
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754922
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754923
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754918
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754919
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s:Ljava/lang/String;

    .line 754920
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754915
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754916
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 754917
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 754879
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754880
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 754881
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 754882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 754883
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 754884
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 754885
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 754886
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 754887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 754888
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 754889
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 754890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 754891
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 754892
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 754893
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 754894
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 754895
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 754896
    const/16 v18, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 754897
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 754898
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 754899
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 754900
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 754901
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 754902
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 754903
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 754904
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 754905
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 754906
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 754907
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 754908
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 754909
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 754910
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 754911
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 754912
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 754913
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754914
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 754806
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754807
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 754809
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 754810
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754811
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 754812
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 754813
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 754814
    if-eqz v2, :cond_1

    .line 754815
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754816
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f:Ljava/util/List;

    move-object v1, v0

    .line 754817
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 754818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 754820
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754821
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754822
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 754823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 754824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 754825
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754826
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754827
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 754828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 754829
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 754830
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754831
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754832
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 754833
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754834
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 754835
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754836
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754837
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 754838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 754840
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754841
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754842
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 754843
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 754844
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 754845
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754846
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 754847
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 754848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 754849
    if-eqz v2, :cond_8

    .line 754850
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754851
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n:Ljava/util/List;

    move-object v1, v0

    .line 754852
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 754853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 754855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754856
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754857
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 754858
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 754859
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 754860
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754861
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754862
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 754863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754864
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 754865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754866
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754867
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 754868
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754869
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 754870
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754871
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754872
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 754873
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 754874
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->y()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 754875
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 754876
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->t:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 754877
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754878
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 754804
    const v0, 0x79beba11

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754801
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754802
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 754803
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754795
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754796
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754797
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 754794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754789
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754790
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754791
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l:Ljava/lang/String;

    .line 754785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n:Ljava/util/List;

    .line 754782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754775
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754776
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754777
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754772
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754773
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754774
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
