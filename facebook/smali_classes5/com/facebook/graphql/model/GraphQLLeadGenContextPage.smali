.class public final Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenContextPage$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenContextPage$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763061
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 763062
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 763079
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 763080
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 763063
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 763065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 763066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 763067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 763068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 763069
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 763070
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 763071
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 763072
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 763073
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 763074
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 763075
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 763076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763077
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 763078
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 763045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 763046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 763047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 763048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 763049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 763050
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 763051
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 763052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 763053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 763054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 763055
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 763056
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 763057
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763058
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763059
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 763060
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 763044
    const v0, 0x2a1e1a0a

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763041
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763042
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->f:Ljava/util/List;

    .line 763043
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 763038
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763039
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    .line 763040
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->h:Ljava/lang/String;

    .line 763037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763032
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763033
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 763034
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 763029
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 763030
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j:Ljava/lang/String;

    .line 763031
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j:Ljava/lang/String;

    return-object v0
.end method
