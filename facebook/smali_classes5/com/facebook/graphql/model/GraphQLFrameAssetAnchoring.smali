.class public final Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public f:D

.field public g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751811
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751824
    const-class v0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 751822
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 751823
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 751812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751813
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 751814
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    if-ne v0, v2, :cond_0

    move-object v0, v6

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 751815
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->j()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 751816
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    if-ne v1, v2, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 751817
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->l()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 751818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 751820
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    goto :goto_0

    .line 751821
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v6

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 751808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751809
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751810
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751825
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751826
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 751827
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 751804
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 751805
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->f:D

    .line 751806
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->h:D

    .line 751807
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 751794
    const v0, -0x53f9e556

    return v0
.end method

.method public final j()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751801
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 751802
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 751803
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->f:D

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 751800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    return-object v0
.end method

.method public final l()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 751795
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 751796
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 751797
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->h:D

    return-wide v0
.end method
