.class public final Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764549
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764509
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764547
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764548
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->e:Ljava/lang/String;

    .line 764546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->f:Ljava/lang/String;

    .line 764543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764538
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764539
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764540
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764535
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764536
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 764537
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764532
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764533
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->i:Ljava/lang/String;

    .line 764534
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 764518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764519
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 764520
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 764521
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 764522
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 764523
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 764524
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 764525
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 764526
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 764527
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764528
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 764529
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764530
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 764531
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764511
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 764512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764513
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 764514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;

    .line 764515
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 764516
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764517
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764508
    const v0, -0x6818e089

    return v0
.end method
