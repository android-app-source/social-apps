.class public final Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744987
    const-class v0, Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 744988
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744989
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 744990
    const/16 v0, 0x2ab

    .line 744991
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 744992
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 744993
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 744994
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_6

    .line 744995
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 744996
    :goto_0
    move v2, v4

    .line 744997
    if-eqz v1, :cond_0

    .line 744998
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 744999
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 745000
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 745001
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 745002
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 745003
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 745004
    move-object v2, v1

    .line 745005
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAymtPageSlideshowPostResponsePayload;-><init>()V

    .line 745006
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 745007
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 745008
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 745009
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 745010
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 745011
    :cond_1
    return-object v1

    .line 745012
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_4

    .line 745013
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 745014
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 745015
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 745016
    const-string p0, "post_status"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 745017
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v2

    move v7, v2

    move v2, v6

    goto :goto_1

    .line 745018
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 745019
    :cond_4
    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 745020
    if-eqz v2, :cond_5

    .line 745021
    invoke-virtual {v3, v4, v7}, LX/186;->a(IZ)V

    .line 745022
    :cond_5
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto :goto_0

    :cond_6
    move v2, v4

    move v7, v4

    goto :goto_1
.end method
