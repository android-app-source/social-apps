.class public final Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 777162
    const-class v0, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 777163
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 777164
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 777165
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 777166
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 777167
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 777168
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 777169
    if-eqz p0, :cond_0

    .line 777170
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777171
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777172
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 777173
    if-eqz p0, :cond_1

    .line 777174
    const-string p2, "required"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777175
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 777176
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 777177
    if-eqz p0, :cond_2

    .line 777178
    const-string p2, "string_value"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777179
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777180
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 777181
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 777182
    check-cast p1, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter$Serializer;->a(Lcom/facebook/graphql/model/GraphQLQPStringEnumTemplateParameter;LX/0nX;LX/0my;)V

    return-void
.end method
