.class public final Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLViewer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744196
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 744171
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 744197
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 744198
    return-void
.end method

.method private a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 744190
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 744191
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 744192
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->e:Z

    return v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLViewer;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 744193
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLViewer;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 744194
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLViewer;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLViewer;

    .line 744195
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLViewer;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 744183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744184
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 744185
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 744186
    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 744187
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 744188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744189
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 744175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 744177
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 744178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 744179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;

    .line 744180
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLViewer;

    .line 744181
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 744182
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 744172
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 744173
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdsExperienceInjectResponsePayload;->e:Z

    .line 744174
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 744170
    const v0, 0x560ea642

    return v0
.end method
