.class public final Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSavableFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSavableFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781884
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 781885
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781886
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 781887
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781888
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 781889
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 781890
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 781891
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 781892
    const/4 v0, 0x0

    .line 781893
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 781894
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 781895
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 781896
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 781897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781898
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 781899
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 781900
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 781901
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 781902
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 781903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781904
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 781905
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 781906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781907
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 781908
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 781909
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 781910
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;

    .line 781911
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavableFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 781912
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 781913
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 781914
    const v0, 0x17d40b84

    return v0
.end method
