.class public final Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767966
    const-class v0, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 767967
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767968
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 767969
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 767970
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 767971
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 767972
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767973
    if-eqz v2, :cond_0

    .line 767974
    const-string p0, "bounds"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767975
    invoke-static {v1, v2, p1}, LX/4NK;->a(LX/15i;ILX/0nX;)V

    .line 767976
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767977
    if-eqz v2, :cond_1

    .line 767978
    const-string p0, "results_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767979
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 767980
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767981
    if-eqz v2, :cond_2

    .line 767982
    const-string p0, "suggestion_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767983
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 767984
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767985
    if-eqz v2, :cond_3

    .line 767986
    const-string p0, "topic"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767987
    invoke-static {v1, v2, p1, p2}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 767988
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 767989
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 767990
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;LX/0nX;LX/0my;)V

    return-void
.end method
