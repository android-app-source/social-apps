.class public final Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/17w;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771287
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771282
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 771283
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 771284
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x7782fd44

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 771285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o:LX/0x2;

    .line 771286
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771288
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771289
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k:Ljava/lang/String;

    .line 771290
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 771306
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771294
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771295
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g:Ljava/lang/String;

    .line 771296
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 771297
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 771298
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 771299
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 771300
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 771301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o:LX/0x2;

    if-nez v0, :cond_0

    .line 771302
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o:LX/0x2;

    .line 771303
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 771304
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 771305
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 771243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771244
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 771245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 771246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 771247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 771248
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 771249
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 771250
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 771251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 771252
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 771253
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 771254
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 771255
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 771256
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 771257
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 771258
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 771259
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 771260
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 771261
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 771262
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771263
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 771264
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 771266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 771267
    if-eqz v1, :cond_3

    .line 771268
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    .line 771269
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 771270
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 771273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    .line 771274
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771275
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 771276
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771277
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 771278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    .line 771279
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771280
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771281
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 771207
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->h:J

    .line 771208
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 771209
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 771210
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->h:J

    .line 771211
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771212
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771213
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->m:Ljava/lang/String;

    .line 771214
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 771215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 771217
    :goto_0
    return-object v0

    .line 771218
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 771219
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 771220
    const v0, -0x7782fd44

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771221
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771222
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->f:Ljava/lang/String;

    .line 771223
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 771224
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771225
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->i:Ljava/util/List;

    .line 771226
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n:Ljava/lang/String;

    .line 771242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 771230
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 771231
    :cond_0
    const/4 v0, 0x0

    .line 771232
    :goto_0
    move v0, v0

    .line 771233
    if-eqz v0, :cond_1

    .line 771234
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;)LX/0Px;

    move-result-object v0

    .line 771235
    :goto_1
    move-object v0, v0

    .line 771236
    return-object v0

    .line 771237
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 771238
    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 771239
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
