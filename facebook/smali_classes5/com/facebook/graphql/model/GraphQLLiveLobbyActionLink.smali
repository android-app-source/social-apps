.class public final Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764393
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764394
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764395
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764396
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764397
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764398
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->e:Ljava/lang/String;

    .line 764399
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764400
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764401
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->f:Ljava/lang/String;

    .line 764402
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764403
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764404
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764405
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764406
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764407
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 764408
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->h:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 764409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764410
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 764411
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 764412
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 764413
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 764414
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 764415
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 764416
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 764417
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764419
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 764420
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->l()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764422
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 764423
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764424
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 764425
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;

    .line 764426
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 764427
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764428
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764429
    const v0, -0x2fcaae6

    return v0
.end method
