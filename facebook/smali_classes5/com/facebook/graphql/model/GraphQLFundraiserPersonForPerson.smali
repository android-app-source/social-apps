.class public final Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:D

.field public w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753454
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 753455
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 753456
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 753457
    return-void
.end method

.method private A()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753458
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753459
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753460
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v:D

    return-wide v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753461
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753462
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753463
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753464
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753465
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->e:Ljava/lang/String;

    .line 753466
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753467
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753468
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->f:Ljava/lang/String;

    .line 753469
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753470
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->g:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753471
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->g:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->g:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753472
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->g:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753473
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753474
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753475
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753476
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753477
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->i:Ljava/lang/String;

    .line 753478
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753479
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753480
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753481
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->j:Z

    return v0
.end method

.method private p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753448
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753449
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753450
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->k:Z

    return v0
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753451
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753452
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753453
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l:Z

    return v0
.end method

.method private r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753332
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753333
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753334
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m:Z

    return v0
.end method

.method private s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 753335
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753336
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753337
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->n:Z

    return v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753338
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753339
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753340
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753341
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753342
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753343
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->p:Z

    return v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753344
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753345
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753346
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753347
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753348
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753349
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753350
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753351
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753352
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753353
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 753354
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753355
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 753356
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 753357
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 753358
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->u:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 753359
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753360
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 753361
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 753362
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 753363
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 753364
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 753365
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 753366
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 753367
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 753368
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 753369
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 753370
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->B()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 753371
    const/16 v11, 0x13

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 753372
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 753373
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 753374
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 753375
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 753376
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 753377
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753378
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753379
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753380
    const/16 v0, 0x8

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753381
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753382
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 753383
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->u()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753384
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 753385
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 753386
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 753387
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 753388
    const/16 v0, 0x10

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->z()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 753389
    const/16 v1, 0x11

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->A()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 753390
    const/16 v0, 0x12

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 753391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753392
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 753393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 753394
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 753395
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753396
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 753397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753398
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753399
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 753400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753401
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 753402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753403
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753404
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 753405
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753406
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 753407
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753408
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 753409
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 753410
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 753411
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 753412
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753413
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 753414
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 753415
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 753416
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 753417
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753418
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->g:Lcom/facebook/graphql/model/GraphQLActor;

    .line 753419
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 753420
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753421
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 753422
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753423
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->h:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753424
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 753425
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753426
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 753427
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753428
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->r:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 753429
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->B()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 753430
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->B()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753431
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->B()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 753432
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;

    .line 753433
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->w:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 753434
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 753435
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 753436
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 753437
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 753438
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->j:Z

    .line 753439
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->k:Z

    .line 753440
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->l:Z

    .line 753441
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->m:Z

    .line 753442
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->n:Z

    .line 753443
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->p:Z

    .line 753444
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->u:Z

    .line 753445
    const/16 v0, 0x11

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonForPerson;->v:D

    .line 753446
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 753447
    const v0, 0x23637cfe

    return v0
.end method
