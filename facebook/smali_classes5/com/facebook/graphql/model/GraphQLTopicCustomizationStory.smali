.class public final Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLExploreFeed;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:J

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public v:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788368
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 788369
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 788370
    const/16 v0, 0x1c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 788371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x46799faf

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 788372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F:LX/0x2;

    .line 788373
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788374
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788375
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->q:Ljava/lang/String;

    .line 788376
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->q:Ljava/lang/String;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788377
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788378
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788379
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788380
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788381
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 788382
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788383
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788384
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 788385
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 788386
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788387
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 788388
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788389
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788390
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 788391
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method private G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788419
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788420
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->w:Ljava/lang/String;

    .line 788421
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->w:Ljava/lang/String;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788392
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788393
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 788394
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method private I()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 788395
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788396
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y:Ljava/util/List;

    .line 788397
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788398
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788399
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788400
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLExploreFeed;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788401
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788402
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 788403
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788404
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788405
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E:Ljava/lang/String;

    .line 788406
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E:Ljava/lang/String;

    return-object v0
.end method

.method private s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 788407
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788408
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->f:Ljava/util/List;

    .line 788409
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 788410
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788411
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g:Ljava/util/List;

    .line 788412
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788413
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788414
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788415
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 788416
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788417
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->i:Ljava/util/List;

    .line 788418
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 788362
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 788363
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 788364
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k:J

    return-wide v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788365
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788366
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 788367
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 788186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788189
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788190
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->p:Ljava/lang/String;

    .line 788191
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 788192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788193
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788194
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->l:Ljava/lang/String;

    .line 788195
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 788196
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 788197
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 788198
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->o:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 788199
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 788200
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F:LX/0x2;

    if-nez v0, :cond_0

    .line 788201
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F:LX/0x2;

    .line 788202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 788203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 788204
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 26

    .prologue
    .line 788205
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788206
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 788207
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 788208
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 788209
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 788210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 788211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 788212
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 788213
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 788214
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 788215
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 788216
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 788217
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 788218
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 788219
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 788220
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->G()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 788221
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->H()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 788222
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->I()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->d(Ljava/util/List;)I

    move-result v19

    .line 788223
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 788224
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 788225
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 788226
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->K()Lcom/facebook/graphql/model/GraphQLExploreFeed;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 788227
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->c()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 788228
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->L()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 788229
    const/16 v7, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 788230
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 788231
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 788232
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 788233
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 788234
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 788235
    const/4 v3, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 788236
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 788237
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 788238
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 788239
    const/16 v3, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 788240
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 788241
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 788242
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 788243
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 788244
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 788245
    const/16 v3, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 788246
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788247
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788248
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788249
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788250
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788251
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788252
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788253
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788254
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788255
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 788256
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788257
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 788258
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->E()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 788259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788260
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 788261
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 788262
    if-eqz v1, :cond_0

    .line 788263
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788264
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->f:Ljava/util/List;

    .line 788265
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 788266
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 788267
    if-eqz v1, :cond_1

    .line 788268
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788269
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 788270
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 788271
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 788272
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 788273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788274
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 788275
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 788276
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 788277
    if-eqz v2, :cond_3

    .line 788278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788279
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->i:Ljava/util/List;

    move-object v1, v0

    .line 788280
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 788281
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 788282
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 788283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788284
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 788285
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 788286
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 788287
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->y()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 788288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788289
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 788290
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 788291
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788292
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 788293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788294
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788295
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 788296
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 788297
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 788298
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788299
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 788300
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 788301
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 788302
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 788303
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788304
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 788305
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 788306
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 788307
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->F()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 788308
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788309
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 788310
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->H()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 788311
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->H()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 788312
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->H()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 788313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788314
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->x:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 788315
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 788316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    .line 788317
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 788318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788319
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    .line 788320
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 788321
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788322
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 788323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788324
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788325
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 788326
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788327
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 788328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788329
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788330
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->K()Lcom/facebook/graphql/model/GraphQLExploreFeed;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 788331
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->K()Lcom/facebook/graphql/model/GraphQLExploreFeed;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 788332
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->K()Lcom/facebook/graphql/model/GraphQLExploreFeed;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 788333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    .line 788334
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->C:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 788335
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 788336
    if-nez v1, :cond_f

    :goto_0
    return-object p0

    :cond_f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788337
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 788187
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->o:J

    .line 788188
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 788338
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 788339
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k:J

    .line 788340
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->o:J

    .line 788341
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D:Ljava/lang/String;

    .line 788344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 788345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 788346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 788347
    :goto_0
    return-object v0

    .line 788348
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 788349
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 788350
    const v0, -0x46799faf

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->j:Ljava/lang/String;

    .line 788353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    .line 788356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->z:Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 788357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 788358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 788359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 788360
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 788361
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
