.class public final Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLOffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Z

.field public e:Lcom/facebook/graphql/model/GraphQLAd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public h:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764152
    const-class v0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 764153
    const-class v0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 764154
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 764155
    return-void
.end method

.method private A()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 764156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w:Ljava/util/List;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w:Ljava/util/List;

    .line 764158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->x:Ljava/lang/String;

    .line 764161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->x:Ljava/lang/String;

    return-object v0
.end method

.method private C()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 764162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->y:Ljava/util/List;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->y:Ljava/util/List;

    .line 764164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 764167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A:Ljava/lang/String;

    .line 764170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A:Ljava/lang/String;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLOffer;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B:Lcom/facebook/graphql/model/GraphQLOffer;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B:Lcom/facebook/graphql/model/GraphQLOffer;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLOffer;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 764173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B:Lcom/facebook/graphql/model/GraphQLOffer;

    return-object v0
.end method

.method private G()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764174
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 764175
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 764176
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->C:Z

    return v0
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLAd;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->e:Lcom/facebook/graphql/model/GraphQLAd;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->e:Lcom/facebook/graphql/model/GraphQLAd;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLAd;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAd;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->e:Lcom/facebook/graphql/model/GraphQLAd;

    .line 764179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->e:Lcom/facebook/graphql/model/GraphQLAd;

    return-object v0
.end method

.method private j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764180
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 764181
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 764182
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->f:Z

    return v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->g:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->g:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->g:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 764185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->g:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->h:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->h:Lcom/facebook/graphql/model/GraphQLEvent;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->h:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 764188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->h:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764008
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764009
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->i:Ljava/lang/String;

    .line 764010
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->j:Ljava/lang/String;

    .line 764148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k:Ljava/lang/String;

    .line 764151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764005
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764006
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 764007
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764011
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->m:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764012
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->m:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->m:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 764013
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->m:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764014
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764015
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 764016
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764017
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764018
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->o:Ljava/lang/String;

    .line 764019
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 764020
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764021
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 764022
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 764025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r:Ljava/lang/String;

    .line 764028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764029
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764030
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s:Lcom/facebook/graphql/model/GraphQLPage;

    .line 764031
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764032
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764033
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t:Ljava/lang/String;

    .line 764034
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u:Ljava/lang/String;

    .line 764037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u:Ljava/lang/String;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 764038
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 764039
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->v:Ljava/lang/String;

    .line 764040
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->v:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 24

    .prologue
    .line 764041
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764042
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->a()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 764043
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 764044
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->m()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 764045
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 764046
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 764047
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 764048
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 764049
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 764050
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 764051
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->v()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 764052
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 764053
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->x()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 764054
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->y()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 764055
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 764056
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v16

    .line 764057
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 764058
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->C()LX/0Px;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v18

    .line 764059
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->D()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 764060
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->E()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 764061
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->F()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 764062
    const/16 v22, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 764063
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 764064
    const/4 v2, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->j()Z

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 764065
    const/16 v22, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v2

    sget-object v23, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-object/from16 v0, v23

    if-ne v2, v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764066
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 764067
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 764068
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 764069
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 764070
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 764071
    const/16 v3, 0x8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764072
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 764073
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 764074
    const/16 v3, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764075
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 764076
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 764077
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 764078
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 764079
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 764080
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 764081
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764082
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764083
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764084
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764085
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764086
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 764087
    const/16 v2, 0x18

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->G()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 764088
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764089
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 764090
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->k()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v2

    goto/16 :goto_0

    .line 764091
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    goto/16 :goto_1

    .line 764092
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->t()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    goto/16 :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 764094
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->a()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 764095
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->a()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAd;

    .line 764096
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->a()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 764097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764098
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->e:Lcom/facebook/graphql/model/GraphQLAd;

    .line 764099
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 764100
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 764101
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 764102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764103
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->h:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 764104
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 764105
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 764106
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 764107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764108
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 764109
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 764110
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 764111
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->r()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 764112
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764113
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->n:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 764114
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 764115
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 764116
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 764117
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764118
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 764119
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->D()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 764120
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->D()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 764121
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->D()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 764122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764123
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->z:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 764124
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->F()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 764125
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->F()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 764126
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->F()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 764127
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764128
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->B:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 764129
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 764130
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 764131
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 764132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764133
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->s:Lcom/facebook/graphql/model/GraphQLPage;

    .line 764134
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 764135
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->A()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 764136
    if-eqz v2, :cond_8

    .line 764137
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    .line 764138
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->w:Ljava/util/List;

    move-object v1, v0

    .line 764139
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 764140
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 764141
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 764142
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->f:Z

    .line 764143
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;->C:Z

    .line 764144
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 764145
    const v0, -0x1e53800c

    return v0
.end method
