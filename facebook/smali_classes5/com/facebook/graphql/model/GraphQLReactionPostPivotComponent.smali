.class public final Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778363
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778362
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 778360
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 778361
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->e:Ljava/lang/String;

    .line 778359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 778356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778364
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778365
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->g:Ljava/lang/String;

    .line 778366
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778320
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778321
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778322
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 778337
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778338
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 778339
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 778340
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 778341
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->m()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 778342
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 778343
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 778344
    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v4, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778345
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 778346
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 778347
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 778348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778349
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 778350
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 778324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778325
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->m()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778326
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->m()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778327
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->m()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 778328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    .line 778329
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->i:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 778330
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 778331
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778332
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 778333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;

    .line 778334
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionPostPivotComponent;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 778335
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778336
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 778323
    const v0, -0x5946be3c

    return v0
.end method
