.class public final Lcom/facebook/graphql/model/GraphQLTarotVideoCard;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787108
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787109
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787110
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787111
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787118
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787119
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->e:Ljava/lang/String;

    .line 787120
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787112
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787113
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->f:Ljava/lang/String;

    .line 787114
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787115
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787116
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 787117
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->i:Ljava/lang/String;

    .line 787104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787058
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787059
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->j:Ljava/lang/String;

    .line 787060
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 787101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 787081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787082
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 787083
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 787084
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 787085
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 787086
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 787087
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 787088
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->p()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 787089
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 787090
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 787091
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 787092
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 787093
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 787094
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 787095
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 787096
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 787097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 787063
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787064
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 787065
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 787066
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 787067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    .line 787068
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->g:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 787069
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 787070
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 787072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    .line 787073
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787074
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->p()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 787075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->p()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 787076
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->p()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 787077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    .line 787078
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->k:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 787079
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787080
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 787061
    const v0, -0xf2a8adf

    return v0
.end method
