.class public final Lcom/facebook/graphql/model/GraphQLMegaphone;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMegaphone$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMegaphone$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 583876
    const-class v0, Lcom/facebook/graphql/model/GraphQLMegaphone$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 583877
    const-class v0, Lcom/facebook/graphql/model/GraphQLMegaphone$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 583878
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 583879
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    .line 583880
    return-void
.end method

.method public constructor <init>(LX/4XF;)V
    .locals 1

    .prologue
    .line 583881
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 583882
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    .line 583883
    iget-object v0, p1, LX/4XF;->b:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    .line 583884
    iget-object v0, p1, LX/4XF;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->f:Ljava/lang/String;

    .line 583885
    iget-object v0, p1, LX/4XF;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->g:Ljava/lang/String;

    .line 583886
    iget-object v0, p1, LX/4XF;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583887
    iget-object v0, p1, LX/4XF;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->i:Ljava/lang/String;

    .line 583888
    iget-object v0, p1, LX/4XF;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 583889
    iget-object v0, p1, LX/4XF;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->k:Ljava/lang/String;

    .line 583890
    iget-boolean v0, p1, LX/4XF;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->l:Z

    .line 583891
    iget-object v0, p1, LX/4XF;->j:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 583892
    iget-object v0, p1, LX/4XF;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583893
    iget-object v0, p1, LX/4XF;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->o:Ljava/lang/String;

    .line 583894
    iget-object v0, p1, LX/4XF;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->p:Ljava/lang/String;

    .line 583895
    iget-object v0, p1, LX/4XF;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->q:Ljava/lang/String;

    .line 583896
    iget-object v0, p1, LX/4XF;->o:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    .line 583897
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583898
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583899
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->i:Ljava/lang/String;

    .line 583900
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 583957
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    if-nez v0, :cond_0

    .line 583958
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    .line 583959
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->r:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 583901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 583902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 583903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 583904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 583905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 583906
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->w()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 583907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 583908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 583909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 583910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->t()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 583911
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->u()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 583912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->v()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 583913
    const/16 v11, 0xd

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 583914
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 583915
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 583916
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 583917
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 583918
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 583919
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 583920
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 583921
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 583922
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->r()Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 583923
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 583924
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 583925
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 583926
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 583927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 583928
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 583929
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->r()Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 583930
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 583931
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 583932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    .line 583933
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 583934
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583935
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    .line 583936
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 583937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 583939
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583940
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583941
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 583942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 583943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 583944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583945
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 583946
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 583947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 583949
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 583950
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583951
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 583952
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583953
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 583954
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 583955
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->l:Z

    .line 583956
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 583872
    const v0, -0x7710bfe4

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    .line 583875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->e:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583869
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583870
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->f:Ljava/lang/String;

    .line 583871
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583866
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583867
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->g:Ljava/lang/String;

    .line 583868
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583863
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583864
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583865
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583860
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583861
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 583862
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583857
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583858
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->k:Ljava/lang/String;

    .line 583859
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 583854
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 583855
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 583856
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->l:Z

    return v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 583851
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583852
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 583853
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->m:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 583850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->o:Ljava/lang/String;

    .line 583847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583842
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583843
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->p:Ljava/lang/String;

    .line 583844
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583839
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 583840
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->q:Ljava/lang/String;

    .line 583841
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMegaphone;->q:Ljava/lang/String;

    return-object v0
.end method
