.class public final Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageOverlay;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755210
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755211
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 755212
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755213
    return-void
.end method

.method public constructor <init>(LX/4WV;)V
    .locals 1

    .prologue
    .line 755223
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755224
    iget-object v0, p1, LX/4WV;->b:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755225
    iget-object v0, p1, LX/4WV;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755226
    iget-object v0, p1, LX/4WV;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755227
    iget-object v0, p1, LX/4WV;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->h:Ljava/lang/String;

    .line 755228
    iget-object v0, p1, LX/4WV;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    .line 755229
    iget-object v0, p1, LX/4WV;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    .line 755230
    iget-object v0, p1, LX/4WV;->h:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755231
    iget-object v0, p1, LX/4WV;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755232
    iget-object v0, p1, LX/4WV;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m:Ljava/lang/String;

    .line 755233
    iget-object v0, p1, LX/4WV;->k:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755234
    return-void
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755217
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755218
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755219
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755204
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755205
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->h:Ljava/lang/String;

    .line 755206
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageOverlay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755207
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755208
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    .line 755209
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755201
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    .line 755203
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755198
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755199
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755200
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755195
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755196
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m:Ljava/lang/String;

    .line 755197
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 755168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 755170
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 755171
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 755172
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 755173
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->o()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 755174
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->p()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 755175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 755176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 755177
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 755178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->s()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 755179
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 755180
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 755181
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 755182
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 755183
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 755184
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 755185
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 755186
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 755187
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 755188
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 755189
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 755190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755191
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 755125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755126
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 755127
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755128
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 755129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755130
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755131
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 755132
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755133
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 755134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755135
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755136
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 755137
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755138
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 755139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755140
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755141
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 755142
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755143
    if-eqz v2, :cond_3

    .line 755144
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755145
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->i:Ljava/util/List;

    move-object v1, v0

    .line 755146
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 755147
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755148
    if-eqz v2, :cond_4

    .line 755149
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755150
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j:Ljava/util/List;

    move-object v1, v0

    .line 755151
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 755152
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 755154
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755155
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755156
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 755157
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755158
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 755159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755160
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755161
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->s()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 755162
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->s()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->s()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 755164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    .line 755165
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755166
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755167
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755124
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 755123
    const v0, -0x20151104

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755120
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755121
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755122
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->k:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    return-object v0
.end method
