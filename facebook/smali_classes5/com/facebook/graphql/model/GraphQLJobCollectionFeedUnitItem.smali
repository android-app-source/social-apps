.class public final Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLJobOpening;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762393
    const-class v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 762392
    const-class v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 762390
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 762391
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLJobOpening;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762387
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLJobOpening;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762388
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLJobOpening;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLJobOpening;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762389
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLJobOpening;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 762384
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 762385
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762386
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 762376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762377
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 762378
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 762379
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 762380
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 762381
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 762382
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762383
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 762362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 762363
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 762364
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762365
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 762366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;

    .line 762367
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLJobOpening;

    .line 762368
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 762369
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762370
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 762371
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;

    .line 762372
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 762373
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 762374
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 762375
    const v0, 0x5bcdc430

    return v0
.end method
