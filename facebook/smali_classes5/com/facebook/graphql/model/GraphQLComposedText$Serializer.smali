.class public final Lcom/facebook/graphql/model/GraphQLComposedText$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComposedText;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 746296
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedText;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLComposedText$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLComposedText$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 746297
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746312
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComposedText;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 746299
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 746300
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 746301
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 746302
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 746303
    if-eqz p0, :cond_0

    .line 746304
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 746305
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 746306
    :cond_0
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 746307
    if-eqz p0, :cond_1

    .line 746308
    const-string p2, "url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 746309
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 746310
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 746311
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 746298
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComposedText;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLComposedText$Serializer;->a(Lcom/facebook/graphql/model/GraphQLComposedText;LX/0nX;LX/0my;)V

    return-void
.end method
