.class public final Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771082
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 771081
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 771013
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 771014
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 771058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 771060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 771061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 771062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 771063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 771064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 771065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 771066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 771067
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 771068
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 771069
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 771070
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    if-ne v0, v8, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 771071
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 771072
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 771073
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 771074
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 771075
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 771076
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 771077
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 771078
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771079
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 771080
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 771040
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 771041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 771044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    .line 771045
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771046
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 771047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 771048
    if-eqz v2, :cond_1

    .line 771049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    .line 771050
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k:Ljava/util/List;

    move-object v1, v0

    .line 771051
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 771052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 771053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 771054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    .line 771055
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 771056
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 771057
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771037
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771038
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 771039
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 771034
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 771035
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j:Z

    .line 771036
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 771033
    const v0, -0x75858566

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771083
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771084
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->f:Ljava/lang/String;

    .line 771085
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 771030
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->g:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771031
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->g:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->g:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    .line 771032
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->g:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771027
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771028
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->h:Ljava/lang/String;

    .line 771029
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771024
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771025
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->i:Ljava/lang/String;

    .line 771026
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 771021
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 771022
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 771023
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j:Z

    return v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 771018
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771019
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k:Ljava/util/List;

    .line 771020
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771015
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 771017
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771010
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771011
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m:Ljava/lang/String;

    .line 771012
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 771008
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n:Ljava/lang/String;

    .line 771009
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n:Ljava/lang/String;

    return-object v0
.end method
