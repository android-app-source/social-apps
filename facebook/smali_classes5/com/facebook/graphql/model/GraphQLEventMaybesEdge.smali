.class public final Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMaybesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMaybesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field public g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749418
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749419
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 749416
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 749417
    return-void
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749413
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 749414
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 749415
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->f:J

    return-wide v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749420
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749421
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 749422
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 749404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 749406
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 749407
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 749408
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 749409
    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 749410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749411
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 749412
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 749396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749397
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 749398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 749399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 749400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;

    .line 749401
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 749402
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749403
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->e:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->e:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 749395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->e:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 749389
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 749390
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->f:J

    .line 749391
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 749392
    const v0, -0x1794ec7e

    return v0
.end method
