.class public final Lcom/facebook/graphql/model/GraphQLSouvenir$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 783042
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenir;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSouvenir$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSouvenir$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 783043
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 783044
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 783045
    const/16 v0, 0x6d

    .line 783046
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 783047
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 783048
    const/4 v2, 0x0

    .line 783049
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_c

    .line 783050
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 783051
    :goto_0
    move v2, v2

    .line 783052
    if-eqz v1, :cond_0

    .line 783053
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 783054
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 783055
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 783056
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 783057
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 783058
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 783059
    move-object v2, v1

    .line 783060
    new-instance v1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSouvenir;-><init>()V

    .line 783061
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 783062
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 783063
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 783064
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 783065
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 783066
    :cond_1
    return-object v1

    .line 783067
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 783068
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 783069
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 783070
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 783071
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v12, :cond_3

    .line 783072
    const-string p0, "container_post"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 783073
    invoke-static {p1, v3}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 783074
    :cond_4
    const-string p0, "formatting_string"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 783075
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 783076
    :cond_5
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 783077
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 783078
    :cond_6
    const-string p0, "media_elements"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 783079
    invoke-static {p1, v3}, LX/4T3;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 783080
    :cond_7
    const-string p0, "souvenir_cover_photo"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 783081
    invoke-static {p1, v3}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 783082
    :cond_8
    const-string p0, "title"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 783083
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 783084
    :cond_9
    const-string p0, "titleForSummary"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 783085
    invoke-static {p1, v3}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 783086
    :cond_a
    const-string p0, "url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 783087
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 783088
    :cond_b
    const/16 v12, 0x9

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 783089
    const/4 v12, 0x1

    invoke-virtual {v3, v12, v11}, LX/186;->b(II)V

    .line 783090
    const/4 v11, 0x2

    invoke-virtual {v3, v11, v10}, LX/186;->b(II)V

    .line 783091
    const/4 v10, 0x3

    invoke-virtual {v3, v10, v9}, LX/186;->b(II)V

    .line 783092
    const/4 v9, 0x4

    invoke-virtual {v3, v9, v8}, LX/186;->b(II)V

    .line 783093
    const/4 v8, 0x5

    invoke-virtual {v3, v8, v7}, LX/186;->b(II)V

    .line 783094
    const/4 v7, 0x6

    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 783095
    const/4 v6, 0x7

    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 783096
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 783097
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v4, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
