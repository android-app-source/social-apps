.class public final Lcom/facebook/graphql/model/GraphQLDocumentLogo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLDocumentLogo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLDocumentLogo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 747610
    const-class v0, Lcom/facebook/graphql/model/GraphQLDocumentLogo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 747609
    const-class v0, Lcom/facebook/graphql/model/GraphQLDocumentLogo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 747607
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 747608
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->e:Ljava/lang/String;

    .line 747606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747602
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->f:Ljava/lang/String;

    .line 747603
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->g:Ljava/lang/String;

    .line 747600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 747595
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 747596
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 747597
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->h:I

    return v0
.end method

.method private n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 747592
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 747593
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 747594
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->j:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 747567
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 747568
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 747569
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 747570
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 747571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 747572
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 747573
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 747574
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 747575
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 747576
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 747577
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 747578
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 747579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 747580
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 747589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 747590
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 747591
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->i:Ljava/lang/String;

    .line 747588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 747582
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 747583
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->h:I

    .line 747584
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;->j:I

    .line 747585
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 747581
    const v0, -0x4e07a67a

    return v0
.end method
