.class public final Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCardSlide$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCardSlide$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759176
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 759177
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 759190
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 759191
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 759178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 759180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 759181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 759182
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 759183
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 759184
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 759185
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->k()Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 759186
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 759187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759188
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 759189
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->k()Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 759158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 759159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 759162
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;

    .line 759163
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759164
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 759165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    .line 759166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 759167
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;

    .line 759168
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    .line 759169
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 759170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 759172
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;

    .line 759173
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759174
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 759175
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 759145
    const v0, 0x495a4c28    # 894146.5f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759152
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759153
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    .line 759154
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->f:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 759149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->g:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->g:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->g:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    .line 759151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->g:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 759146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 759147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 759148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
