.class public final Lcom/facebook/graphql/model/GraphQLSwipeableFrame;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSwipeableFrame$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSwipeableFrame$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786098
    const-class v0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 786099
    const-class v0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 786100
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 786101
    return-void
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o:Ljava/lang/String;

    .line 786104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q:Ljava/lang/String;

    .line 786173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 786105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 786107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 786108
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 786109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 786110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 786111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 786112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 786113
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 786114
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 786115
    const/16 v3, 0xd

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 786116
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 786117
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 786118
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 786119
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 786120
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 786121
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 786122
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 786123
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 786124
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 786125
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->s()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 786126
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 786127
    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->t()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 786128
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 786129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786130
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 786131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 786132
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 786133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786134
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 786135
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786136
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786137
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 786138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 786139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 786140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786141
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 786142
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 786143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    .line 786144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 786145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786146
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    .line 786147
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 786148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    .line 786149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 786150
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786151
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    .line 786152
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 786153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    .line 786154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 786155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786156
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    .line 786157
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 786158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 786159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 786160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 786161
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 786162
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 786163
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 786165
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 786166
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->h:J

    .line 786167
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k:Z

    .line 786168
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n:Z

    .line 786169
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->p:J

    .line 786170
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 786094
    const v0, -0x459f85a7

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786095
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786096
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 786097
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786064
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786065
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 786066
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786070
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786071
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    .line 786072
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->g:Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    return-object v0
.end method

.method public final m()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 786073
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 786074
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 786075
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->h:J

    return-wide v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786076
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786077
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    .line 786078
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->i:Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786067
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786068
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    .line 786069
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j:Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 786079
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 786080
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 786081
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k:Z

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786082
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786083
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l:Ljava/lang/String;

    .line 786084
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 786085
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 786086
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    .line 786087
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m:Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 786088
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 786089
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 786090
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n:Z

    return v0
.end method

.method public final t()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 786091
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 786092
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 786093
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->p:J

    return-wide v0
.end method
