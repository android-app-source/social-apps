.class public final Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 763896
    const-class v0, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 763897
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 763898
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 763899
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 763900
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x14

    const/16 v7, 0xb

    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 763901
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 763902
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 763903
    if-eqz v2, :cond_0

    .line 763904
    const-string v3, "ad"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763905
    invoke-static {v1, v2, p1}, LX/4Kf;->a(LX/15i;ILX/0nX;)V

    .line 763906
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 763907
    if-eqz v2, :cond_1

    .line 763908
    const-string v3, "can_watch_and_browse"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763909
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 763910
    :cond_1
    invoke-virtual {v1, v0, v5, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 763911
    if-eqz v2, :cond_2

    .line 763912
    const-string v2, "destination_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763913
    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763914
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763915
    if-eqz v2, :cond_3

    .line 763916
    const-string v3, "event"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763917
    invoke-static {v1, v2, p1, p2}, LX/4M6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 763918
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763919
    if-eqz v2, :cond_4

    .line 763920
    const-string v3, "header_color"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763921
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763922
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763923
    if-eqz v2, :cond_5

    .line 763924
    const-string v3, "link_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763925
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763926
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763927
    if-eqz v2, :cond_6

    .line 763928
    const-string v3, "link_display"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763929
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763930
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763931
    if-eqz v2, :cond_7

    .line 763932
    const-string v3, "link_icon_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763933
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 763934
    :cond_7
    invoke-virtual {v1, v0, v6, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 763935
    if-eqz v2, :cond_8

    .line 763936
    const-string v2, "link_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763937
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-virtual {v1, v0, v6, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763938
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763939
    if-eqz v2, :cond_9

    .line 763940
    const-string v3, "link_target_store_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763941
    invoke-static {v1, v2, p1, p2}, LX/4P6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 763942
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763943
    if-eqz v2, :cond_a

    .line 763944
    const-string v3, "link_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763945
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763946
    :cond_a
    invoke-virtual {v1, v0, v7, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 763947
    if-eqz v2, :cond_b

    .line 763948
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763949
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, v7, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763950
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763951
    if-eqz v2, :cond_c

    .line 763952
    const-string v3, "link_video_endscreen_icon"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763953
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 763954
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763955
    if-eqz v2, :cond_d

    .line 763956
    const-string v3, "logo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763957
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763958
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763959
    if-eqz v2, :cond_e

    .line 763960
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763961
    invoke-static {v1, v2, p1, p2}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 763962
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763963
    if-eqz v2, :cond_f

    .line 763964
    const-string v3, "stateful_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763965
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763966
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763967
    if-eqz v2, :cond_10

    .line 763968
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763969
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763970
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763971
    if-eqz v2, :cond_11

    .line 763972
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763973
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763974
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763975
    if-eqz v2, :cond_12

    .line 763976
    const-string v3, "video_annotations"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763977
    invoke-static {v1, v2, p1, p2}, LX/4UB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 763978
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763979
    if-eqz v2, :cond_13

    .line 763980
    const-string v3, "messenger_extensions_payment_privacy_policy"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763981
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763982
    :cond_13
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 763983
    if-eqz v2, :cond_14

    .line 763984
    const-string v2, "messenger_extensions_whitelisted_domains"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763985
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 763986
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763987
    if-eqz v2, :cond_15

    .line 763988
    const-string v3, "messenger_extensions_user_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763989
    invoke-static {v1, v2, p1}, LX/4PX;->a(LX/15i;ILX/0nX;)V

    .line 763990
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 763991
    if-eqz v2, :cond_16

    .line 763992
    const-string v3, "cannot_watch_and_browse_reason"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763993
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 763994
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 763995
    if-eqz v2, :cond_17

    .line 763996
    const-string v3, "offer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 763997
    invoke-static {v1, v2, p1, p2}, LX/4Q2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 763998
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 763999
    if-eqz v2, :cond_18

    .line 764000
    const-string v3, "is_on_fb_event_ticket_link"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764001
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 764002
    :cond_18
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764003
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764004
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLinkOpenActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
