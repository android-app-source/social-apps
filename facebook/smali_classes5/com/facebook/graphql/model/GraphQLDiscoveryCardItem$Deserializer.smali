.class public final Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 747414
    const-class v0, Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 747415
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 747448
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 747416
    const/16 v0, 0x5d

    .line 747417
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 747418
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 747419
    const/4 v2, 0x0

    .line 747420
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, p0, :cond_3

    .line 747421
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747422
    :goto_0
    move v2, v2

    .line 747423
    if-eqz v1, :cond_0

    .line 747424
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 747425
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 747426
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747427
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 747428
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 747429
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 747430
    move-object v2, v1

    .line 747431
    new-instance v1, Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLDiscoveryCardItem;-><init>()V

    .line 747432
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 747433
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 747434
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 747435
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 747436
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 747437
    :cond_1
    return-object v1

    .line 747438
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 747439
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_4

    .line 747440
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 747441
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 747442
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v4, :cond_3

    .line 747443
    const-string p0, "id"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 747444
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 747445
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 747446
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 747447
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    goto :goto_0
.end method
