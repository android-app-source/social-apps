.class public final Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 784016
    const-class v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 784017
    const-class v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784018
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 784019
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784020
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->e:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784021
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->e:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 784022
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->e:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->f:Ljava/lang/String;

    .line 784025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->g:Ljava/lang/String;

    .line 784028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784029
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784030
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->h:Ljava/lang/String;

    .line 784031
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 784032
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 784033
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 784034
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->i:J

    return-wide v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 784037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783938
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783939
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->k:Ljava/lang/String;

    .line 783940
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784013
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784014
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->l:Ljava/lang/String;

    .line 784015
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784010
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784011
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m:Lcom/facebook/graphql/model/GraphQLComment;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 784012
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784008
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->n:Ljava/lang/String;

    .line 784009
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->n:Ljava/lang/String;

    return-object v0
.end method

.method private t()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 784004
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 784005
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 784006
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o:I

    return v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 784001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 784002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p:Lcom/facebook/graphql/model/GraphQLStory;

    .line 784003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783998
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783999
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q:Ljava/lang/String;

    .line 784000
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 783970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783971
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 783972
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 783973
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 783974
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 783975
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 783976
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 783977
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 783978
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->r()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 783979
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 783980
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->u()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 783981
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->v()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 783982
    const/16 v4, 0xe

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 783983
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 783984
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 783985
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 783986
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 783987
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->n()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 783988
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 783989
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 783990
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 783991
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 783992
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 783993
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->t()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 783994
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 783995
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 783996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 783947
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783948
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783949
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 783950
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 783951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 783952
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 783953
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 783954
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 783955
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 783956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 783957
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 783958
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->r()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 783959
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->r()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 783960
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->r()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 783961
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 783962
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 783963
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->u()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 783964
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->u()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 783965
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->u()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 783966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;

    .line 783967
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->p:Lcom/facebook/graphql/model/GraphQLStory;

    .line 783968
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783969
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783946
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 783942
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 783943
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->i:J

    .line 783944
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchDataFact;->o:I

    .line 783945
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 783941
    const v0, 0x7794e0b2

    return v0
.end method
