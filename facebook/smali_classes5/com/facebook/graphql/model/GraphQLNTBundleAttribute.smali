.class public final Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNTBundleAttribute$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNTBundleAttribute$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767177
    const-class v0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 767176
    const-class v0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 767174
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 767175
    return-void
.end method

.method public constructor <init>(LX/4XN;)V
    .locals 1

    .prologue
    .line 767164
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 767165
    iget-object v0, p1, LX/4XN;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 767166
    iget-object v0, p1, LX/4XN;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    .line 767167
    iget-object v0, p1, LX/4XN;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 767168
    iget-object v0, p1, LX/4XN;->e:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    .line 767169
    iget-object v0, p1, LX/4XN;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->g:Ljava/lang/String;

    .line 767170
    iget-object v0, p1, LX/4XN;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767171
    iget-object v0, p1, LX/4XN;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 767172
    iget-object v0, p1, LX/4XN;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 767173
    return-void
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767158
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 767159
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 767160
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 767161
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 767162
    const/4 v0, 0x0

    .line 767163
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 767137
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767138
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->p()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->p()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 767139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 767140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 767141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 767142
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 767143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->m()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 767144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 767145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 767146
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 767147
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 767148
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 767149
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 767150
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 767151
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 767152
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 767153
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 767154
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 767155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767156
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 767157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 767104
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 767105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 767106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 767107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 767108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767109
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 767110
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 767111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 767112
    if-eqz v2, :cond_1

    .line 767113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767114
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    move-object v1, v0

    .line 767115
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 767116
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 767117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 767118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767119
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 767120
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 767121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 767122
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 767123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767124
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    .line 767125
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 767126
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 767128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767129
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767130
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 767131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 767132
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 767133
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 767134
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 767135
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 767136
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767082
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767083
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 767084
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 767103
    const v0, 0x23ed3594

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->g:Ljava/lang/String;

    .line 767102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 767099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 767096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 767091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    .line 767093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 767090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 767085
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 767086
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    .line 767087
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method
