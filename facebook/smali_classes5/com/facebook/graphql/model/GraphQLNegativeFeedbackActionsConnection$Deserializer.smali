.class public final Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 768079
    const-class v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 768080
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 768081
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 768082
    const/16 v0, 0xee

    .line 768083
    const/4 v1, 0x1

    const/4 p2, 0x0

    .line 768084
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 768085
    invoke-static {p1, v3}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    .line 768086
    if-eqz v1, :cond_0

    .line 768087
    const/4 p0, 0x2

    invoke-virtual {v3, p0}, LX/186;->c(I)V

    .line 768088
    invoke-virtual {v3, p2, v0, p2}, LX/186;->a(ISI)V

    .line 768089
    const/4 p0, 0x1

    invoke-virtual {v3, p0, v2}, LX/186;->b(II)V

    .line 768090
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 768091
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 768092
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 768093
    move-object v2, v1

    .line 768094
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;-><init>()V

    .line 768095
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 768096
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 768097
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 768098
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 768099
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 768100
    :cond_1
    return-object v1
.end method
