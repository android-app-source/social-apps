.class public final Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public q:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public r:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public t:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782708
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782707
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782705
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782706
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782699
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 782700
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 782701
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 782702
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 782703
    const/4 v0, 0x0

    .line 782704
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782696
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782697
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->f:Ljava/util/List;

    .line 782698
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782693
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782694
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782695
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->g:Z

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782690
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782691
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 782692
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782687
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782688
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->i:Ljava/lang/String;

    .line 782689
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782684
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782685
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782686
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->j:Z

    return v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782681
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782682
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->k:Ljava/lang/String;

    .line 782683
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 782680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782709
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782710
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 782711
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    return-object v0
.end method

.method private s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n:Ljava/util/List;

    .line 782600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782602
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->o:Ljava/lang/String;

    .line 782603
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 782606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782607
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782608
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 782609
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782610
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782611
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 782612
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 782615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782616
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782617
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782618
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->t:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 782619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782620
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 782621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 782622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 782623
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 782624
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 782625
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 782626
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 782627
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->s()LX/0Px;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->d(Ljava/util/List;)I

    move-result v8

    .line 782628
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 782629
    const/16 v10, 0x11

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 782630
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 782631
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 782632
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 782633
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 782634
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 782635
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->o()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 782636
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 782637
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 782638
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 782639
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 782640
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 782641
    const/16 v2, 0xc

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->u()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 782642
    const/16 v2, 0xd

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->v()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v3, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 782643
    const/16 v2, 0xe

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v3, :cond_3

    move-object v0, v1

    :goto_3
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 782644
    const/16 v0, 0xf

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->x()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v2, v3, :cond_4

    :goto_4
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 782645
    const/16 v0, 0x10

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->y()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 782646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782647
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 782648
    goto/16 :goto_0

    .line 782649
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->u()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    goto :goto_1

    .line 782650
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->v()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    goto :goto_2

    .line 782651
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    goto :goto_3

    .line 782652
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->x()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    goto :goto_4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 782653
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782654
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 782655
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 782656
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 782657
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782658
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 782659
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 782660
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 782661
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 782662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782663
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 782664
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 782665
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 782666
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->r()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 782667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;

    .line 782668
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->m:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 782669
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782670
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782671
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 782672
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 782673
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->g:Z

    .line 782674
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->j:Z

    .line 782675
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchSuggestionUnit;->t:Z

    .line 782676
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782677
    const v0, 0x223ed590

    return v0
.end method
