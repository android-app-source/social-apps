.class public final Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 769724
    const-class v0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 769725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 769772
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 769727
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 769728
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 769729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 769730
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 769731
    if-eqz v2, :cond_0

    .line 769732
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769733
    invoke-static {v1, v0, v4, p1}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 769734
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769735
    if-eqz v2, :cond_1

    .line 769736
    const-string v3, "bg_color"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769737
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769738
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769739
    if-eqz v2, :cond_2

    .line 769740
    const-string v3, "image_glyph"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769741
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769742
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769743
    if-eqz v2, :cond_3

    .line 769744
    const-string v3, "image_source"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769745
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 769746
    :cond_3
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 769747
    if-eqz v2, :cond_4

    .line 769748
    const-string v2, "style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769749
    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769750
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769751
    if-eqz v2, :cond_5

    .line 769752
    const-string v3, "subtext"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769753
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769754
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769755
    if-eqz v2, :cond_6

    .line 769756
    const-string v3, "tertiary_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769757
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769758
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 769759
    if-eqz v2, :cond_7

    .line 769760
    const-string v3, "tertiary_text_color"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769761
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 769762
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769763
    if-eqz v2, :cond_8

    .line 769764
    const-string v3, "text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769765
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769766
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 769767
    if-eqz v2, :cond_9

    .line 769768
    const-string v3, "undo_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 769769
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 769770
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 769771
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 769726
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;LX/0nX;LX/0my;)V

    return-void
.end method
