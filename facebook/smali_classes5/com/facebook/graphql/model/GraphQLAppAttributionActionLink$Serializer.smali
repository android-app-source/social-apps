.class public final Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744615
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 744616
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 744584
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 744585
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 744586
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v4, 0x0

    .line 744587
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 744588
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 744589
    if-eqz v2, :cond_0

    .line 744590
    const-string v3, "app_icon"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744591
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 744592
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 744593
    if-eqz v2, :cond_1

    .line 744594
    const-string v3, "application"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744595
    invoke-static {v1, v2, p1, p2}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 744596
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 744597
    if-eqz v2, :cond_2

    .line 744598
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744599
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 744600
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 744601
    if-eqz v2, :cond_3

    .line 744602
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744603
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 744604
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 744605
    if-eqz v2, :cond_4

    .line 744606
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744607
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 744608
    :cond_4
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 744609
    if-eqz v2, :cond_5

    .line 744610
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 744611
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 744612
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 744613
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 744614
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLAppAttributionActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
