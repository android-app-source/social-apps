.class public final Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749821
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 749864
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 749862
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 749863
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749859
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749860
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 749861
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749856
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749857
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->f:Ljava/lang/String;

    .line 749858
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 749853
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749854
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->g:Ljava/lang/String;

    .line 749855
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749850
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 749851
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 749852
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->h:I

    return v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 749847
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->i:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 749848
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->i:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->i:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    .line 749849
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->i:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 749834
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749835
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 749836
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 749837
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 749838
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 749839
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 749840
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 749841
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 749842
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 749843
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->m()Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 749844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749845
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 749846
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->m()Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 749826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 749827
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 749828
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 749829
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 749830
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;

    .line 749831
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->e:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 749832
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 749833
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 749823
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 749824
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventTicketAdditionalCharge;->h:I

    .line 749825
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 749822
    const v0, 0x1184dc01

    return v0
.end method
