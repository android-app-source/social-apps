.class public final Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTarotVideoCard;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 787021
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 787022
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 787023
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTarotVideoCard;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 787024
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 787025
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 787026
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 787027
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787028
    if-eqz v2, :cond_0

    .line 787029
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787030
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787031
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787032
    if-eqz v2, :cond_1

    .line 787033
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787034
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787035
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787036
    if-eqz v2, :cond_2

    .line 787037
    const-string p0, "featured_article"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787038
    invoke-static {v1, v2, p1, p2}, LX/4Og;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 787039
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787040
    if-eqz v2, :cond_3

    .line 787041
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787042
    invoke-static {v1, v2, p1, p2}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787043
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787044
    if-eqz v2, :cond_4

    .line 787045
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787046
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787047
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 787048
    if-eqz v2, :cond_5

    .line 787049
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787050
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 787051
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 787052
    if-eqz v2, :cond_6

    .line 787053
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 787054
    invoke-static {v1, v2, p1, p2}, LX/4UG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 787055
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 787056
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 787057
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTarotVideoCard;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLTarotVideoCard$Serializer;->a(Lcom/facebook/graphql/model/GraphQLTarotVideoCard;LX/0nX;LX/0my;)V

    return-void
.end method
