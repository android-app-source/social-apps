.class public final Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766663
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 766664
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766615
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 766616
    const/16 v0, 0x183

    .line 766617
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 766618
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 766619
    const/4 v4, 0x0

    .line 766620
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_a

    .line 766621
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 766622
    :goto_0
    move v2, v4

    .line 766623
    if-eqz v1, :cond_0

    .line 766624
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 766625
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 766626
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 766627
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 766628
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 766629
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 766630
    move-object v2, v1

    .line 766631
    new-instance v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;-><init>()V

    .line 766632
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 766633
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 766634
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 766635
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 766636
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 766637
    :cond_1
    return-object v1

    .line 766638
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 766639
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_9

    .line 766640
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 766641
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 766642
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v11, :cond_3

    .line 766643
    const-string p0, "aymt_channel_image"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 766644
    invoke-static {p1, v3}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 766645
    :cond_4
    const-string p0, "aymt_channel_url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 766646
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 766647
    :cond_5
    const-string p0, "aymt_hpp_channel"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 766648
    invoke-static {p1, v3}, LX/4KZ;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 766649
    :cond_6
    const-string p0, "context_rows"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 766650
    invoke-static {p1, v3}, LX/4Lv;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 766651
    :cond_7
    const-string p0, "profile"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 766652
    invoke-static {p1, v3}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 766653
    :cond_8
    const-string p0, "tracking"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 766654
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 766655
    :cond_9
    const/4 v11, 0x6

    invoke-virtual {v3, v11}, LX/186;->c(I)V

    .line 766656
    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 766657
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 766658
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 766659
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 766660
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 766661
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 766662
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_a
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto/16 :goto_1
.end method
