.class public final Lcom/facebook/graphql/model/GraphQLComposedText$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 746258
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedText;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLComposedText$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLComposedText$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 746259
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746260
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 746261
    const/16 v0, 0x245

    .line 746262
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 746263
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 746264
    const/4 v2, 0x0

    .line 746265
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_6

    .line 746266
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 746267
    :goto_0
    move v2, v2

    .line 746268
    if-eqz v1, :cond_0

    .line 746269
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 746270
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 746271
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 746272
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 746273
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 746274
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 746275
    move-object v2, v1

    .line 746276
    new-instance v1, Lcom/facebook/graphql/model/GraphQLComposedText;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLComposedText;-><init>()V

    .line 746277
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 746278
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 746279
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 746280
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 746281
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 746282
    :cond_1
    return-object v1

    .line 746283
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 746284
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_5

    .line 746285
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 746286
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 746287
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v6, :cond_3

    .line 746288
    const-string p0, "id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 746289
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 746290
    :cond_4
    const-string p0, "url"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 746291
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 746292
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 746293
    const/4 v6, 0x1

    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 746294
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 746295
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v4, v2

    goto :goto_1
.end method
