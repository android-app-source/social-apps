.class public final Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764479
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 764480
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764481
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 764482
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 764483
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v4, 0x0

    .line 764484
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 764485
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 764486
    if-eqz v2, :cond_0

    .line 764487
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764488
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764489
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 764490
    if-eqz v2, :cond_1

    .line 764491
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764492
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764493
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 764494
    if-eqz v2, :cond_2

    .line 764495
    const-string v3, "video_broadcast_schedule"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764496
    invoke-static {v1, v2, p1, p2}, LX/4UC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 764497
    :cond_2
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 764498
    if-eqz v2, :cond_3

    .line 764499
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764500
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764501
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 764502
    if-eqz v2, :cond_4

    .line 764503
    const-string v3, "selected_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 764504
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 764505
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 764506
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 764507
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLiveScheduleSubscribeActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
