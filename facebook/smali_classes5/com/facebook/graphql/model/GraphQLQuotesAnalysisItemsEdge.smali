.class public final Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777995
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777994
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 777992
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 777993
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 777974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 777976
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 777977
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 777978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777979
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 777984
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777985
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 777986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    .line 777987
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 777988
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;

    .line 777989
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    .line 777990
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777991
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777981
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777982
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    .line 777983
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItemsEdge;->e:Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 777980
    const v0, -0x77e6eeb6

    return v0
.end method
