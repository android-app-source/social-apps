.class public final Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787910
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787909
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787907
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787908
    return-void
.end method

.method public constructor <init>(LX/4ZE;)V
    .locals 1

    .prologue
    .line 787902
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787903
    iget-object v0, p1, LX/4ZE;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->e:Ljava/lang/String;

    .line 787904
    iget-object v0, p1, LX/4ZE;->c:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 787905
    iget-object v0, p1, LX/4ZE;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->g:Ljava/lang/String;

    .line 787906
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 787892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 787894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v1

    .line 787895
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 787896
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 787897
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 787898
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 787899
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 787900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787901
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 787884
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787885
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 787886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 787887
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 787888
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    .line 787889
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 787890
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787891
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787881
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787882
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->e:Ljava/lang/String;

    .line 787883
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 787880
    const v0, -0x381ae918

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787877
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787878
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 787879
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->f:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787874
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787875
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->g:Ljava/lang/String;

    .line 787876
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->g:Ljava/lang/String;

    return-object v0
.end method
