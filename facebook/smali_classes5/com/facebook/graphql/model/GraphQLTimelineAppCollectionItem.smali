.class public final Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

.field public g:J

.field public h:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787585
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 787586
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787587
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 787588
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y:Ljava/lang/String;

    .line 787597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y:Ljava/lang/String;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 787598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 787600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787622
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n:Ljava/lang/String;

    .line 787623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 787624
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 787625
    if-eqz v0, :cond_0

    .line 787626
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 787627
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787602
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787603
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 787619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    .line 787621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    return-object v0
.end method

.method private l()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 787616
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 787617
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 787618
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->g:J

    return-wide v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787610
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->i:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787611
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->i:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->i:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 787612
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->i:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787607
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787608
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787609
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k:Ljava/lang/String;

    .line 787606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787582
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787583
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787584
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787407
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787408
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787409
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787413
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787414
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n:Ljava/lang/String;

    .line 787415
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787416
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787417
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o:Lcom/facebook/graphql/model/GraphQLNode;

    .line 787418
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787419
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787420
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p:Lcom/facebook/graphql/model/GraphQLNode;

    .line 787421
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787422
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787423
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787424
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787425
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787426
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787427
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLRating;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787428
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s:Lcom/facebook/graphql/model/GraphQLRating;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787429
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s:Lcom/facebook/graphql/model/GraphQLRating;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLRating;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s:Lcom/facebook/graphql/model/GraphQLRating;

    .line 787430
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s:Lcom/facebook/graphql/model/GraphQLRating;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787431
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787432
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t:Lcom/facebook/graphql/model/FeedUnit;

    const/16 v1, 0x10

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t:Lcom/facebook/graphql/model/FeedUnit;

    .line 787433
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 787435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 26

    .prologue
    .line 787437
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787438
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 787439
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 787440
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 787441
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 787442
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 787443
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 787444
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 787445
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 787446
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 787447
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 787448
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 787449
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 787450
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 787451
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    sget-object v4, LX/16Z;->a:LX/16Z;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v20

    .line 787452
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 787453
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 787454
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 787455
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 787456
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->D()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 787457
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 787458
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 787459
    const/4 v3, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 787460
    const/4 v3, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 787461
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 787462
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 787463
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 787464
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 787465
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 787466
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 787467
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 787468
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 787469
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787470
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787471
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787472
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787473
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787474
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787475
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787476
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787477
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787478
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 787479
    const/16 v3, 0x16

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->E()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 787480
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787481
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 787482
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->k()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    move-result-object v2

    goto/16 :goto_0

    .line 787483
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->E()Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 787484
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787485
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 787486
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787487
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 787488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787489
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787490
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 787491
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787492
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 787493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787494
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 787495
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 787496
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 787497
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 787498
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787499
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->i:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 787500
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 787501
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787502
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 787503
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787504
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787505
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 787506
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787507
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 787508
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787509
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787510
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 787511
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 787513
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787514
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787515
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 787516
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 787517
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 787518
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787519
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->o:Lcom/facebook/graphql/model/GraphQLNode;

    .line 787520
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 787521
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 787522
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 787523
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787524
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p:Lcom/facebook/graphql/model/GraphQLNode;

    .line 787525
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 787526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787527
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 787528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787529
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787530
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 787531
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787532
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 787533
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787534
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787535
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 787536
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    .line 787537
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 787538
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787539
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s:Lcom/facebook/graphql/model/GraphQLRating;

    .line 787540
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 787541
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 787542
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->y()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 787543
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787544
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->t:Lcom/facebook/graphql/model/FeedUnit;

    .line 787545
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 787546
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787547
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 787548
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787549
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787550
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 787551
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 787552
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 787553
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787554
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787555
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 787556
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787557
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 787558
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787559
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787560
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 787561
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787562
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 787563
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;

    .line 787564
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 787565
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 787566
    if-nez v1, :cond_10

    :goto_0
    return-object p0

    :cond_10
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 787567
    new-instance v0, LX/4ZA;

    invoke-direct {v0, p1}, LX/4ZA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787568
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 787410
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 787411
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->g:J

    .line 787412
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 787569
    const-string v0, "locally_updated_containing_collection_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787570
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 787571
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 787572
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    .line 787573
    :goto_0
    return-void

    .line 787574
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 787575
    const-string v0, "locally_updated_containing_collection_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787576
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionItem;->a(Ljava/lang/String;)V

    .line 787577
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 787578
    const v0, -0x939b30f

    return v0
.end method
