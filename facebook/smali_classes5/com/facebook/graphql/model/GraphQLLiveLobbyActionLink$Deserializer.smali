.class public final Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764322
    const-class v0, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 764323
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 764324
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 764325
    const/16 v0, 0x250

    .line 764326
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 764327
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 764328
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 764329
    const/4 v2, 0x0

    .line 764330
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 764331
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 764332
    :goto_0
    move v2, v4

    .line 764333
    if-eqz v1, :cond_0

    .line 764334
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 764335
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 764336
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 764337
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 764338
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 764339
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 764340
    move-object v2, v1

    .line 764341
    new-instance v1, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLiveLobbyActionLink;-><init>()V

    .line 764342
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 764343
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 764344
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 764345
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 764346
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 764347
    :cond_1
    return-object v1

    .line 764348
    :cond_2
    const-string p0, "link_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 764349
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    move-object v7, v2

    move v2, v6

    .line 764350
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_7

    .line 764351
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 764352
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 764353
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v11, :cond_3

    .line 764354
    const-string p0, "title"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 764355
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 764356
    :cond_4
    const-string p0, "url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 764357
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 764358
    :cond_5
    const-string p0, "video"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 764359
    invoke-static {p1, v3}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 764360
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 764361
    :cond_7
    const/4 v11, 0x4

    invoke-virtual {v3, v11}, LX/186;->c(I)V

    .line 764362
    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 764363
    invoke-virtual {v3, v6, v9}, LX/186;->b(II)V

    .line 764364
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 764365
    if-eqz v2, :cond_8

    .line 764366
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 764367
    :cond_8
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_9
    move-object v7, v2

    move v8, v4

    move v9, v4

    move v10, v4

    move v2, v4

    goto :goto_1
.end method
