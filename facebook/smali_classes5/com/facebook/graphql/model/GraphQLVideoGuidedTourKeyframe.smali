.class public final Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:J

.field public g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 639091
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 639124
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 639122
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 639123
    return-void
.end method

.method public constructor <init>(LX/4ZT;)V
    .locals 2

    .prologue
    .line 639117
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 639118
    iget v0, p1, LX/4ZT;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->e:I

    .line 639119
    iget-wide v0, p1, LX/4ZT;->c:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->f:J

    .line 639120
    iget v0, p1, LX/4ZT;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->g:I

    .line 639121
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 639114
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 639115
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 639116
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 639107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 639108
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 639109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->a()I

    move-result v0

    invoke-virtual {p1, v6, v0, v6}, LX/186;->a(III)V

    .line 639110
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 639111
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 639112
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 639113
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 639104
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 639105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 639106
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 639099
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 639100
    invoke-virtual {p1, p2, v4, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->e:I

    .line 639101
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->f:J

    .line 639102
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->g:I

    .line 639103
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 639098
    const v0, -0x3f19de57

    return v0
.end method

.method public final j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 639095
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 639096
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 639097
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->f:J

    return-wide v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 639092
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 639093
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 639094
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->g:I

    return v0
.end method
