.class public final Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 766665
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 766666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766667
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 766668
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 766669
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 766670
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 766671
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766672
    if-eqz v2, :cond_0

    .line 766673
    const-string p0, "aymt_channel_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766674
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 766675
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766676
    if-eqz v2, :cond_1

    .line 766677
    const-string p0, "aymt_channel_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766678
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766679
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766680
    if-eqz v2, :cond_2

    .line 766681
    const-string p0, "aymt_hpp_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766682
    invoke-static {v1, v2, p1, p2}, LX/4KZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 766683
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766684
    if-eqz v2, :cond_3

    .line 766685
    const-string p0, "context_rows"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766686
    invoke-static {v1, v2, p1, p2}, LX/4Lv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 766687
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 766688
    if-eqz v2, :cond_4

    .line 766689
    const-string p0, "profile"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766690
    invoke-static {v1, v2, p1, p2}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 766691
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 766692
    if-eqz v2, :cond_5

    .line 766693
    const-string p0, "tracking"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 766694
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 766695
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 766696
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 766697
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem$Serializer;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnitItem;LX/0nX;LX/0my;)V

    return-void
.end method
