.class public final Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 743940
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 743941
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 743942
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 743943
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 743944
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 743945
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 743946
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 743947
    if-eqz v2, :cond_1

    .line 743948
    const-string v3, "edges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743949
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 743950
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 743951
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/4Kj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 743952
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 743953
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 743954
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 743955
    if-eqz v2, :cond_2

    .line 743956
    const-string v3, "page_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 743957
    invoke-static {v1, v2, p1}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 743958
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 743959
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 743960
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;LX/0nX;LX/0my;)V

    return-void
.end method
