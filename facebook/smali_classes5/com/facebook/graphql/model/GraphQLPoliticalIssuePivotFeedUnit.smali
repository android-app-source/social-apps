.class public final Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/17w;
.implements LX/16q;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775817
    const-class v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775818
    const-class v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 775819
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775820
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x3bf6f008

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 775821
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r:LX/0x2;

    .line 775822
    return-void
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775823
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775824
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->i:Ljava/lang/String;

    .line 775825
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->l:Ljava/lang/String;

    .line 775828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775843
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775844
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->p:Ljava/lang/String;

    .line 775845
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 775832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g:Ljava/lang/String;

    .line 775835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 775836
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 775837
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 775838
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 775839
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 775840
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r:LX/0x2;

    if-nez v0, :cond_0

    .line 775841
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r:LX/0x2;

    .line 775842
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 775788
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 775789
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 775790
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775791
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 775792
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 775793
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 775794
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 775795
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 775796
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->v()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 775797
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 775798
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 775799
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 775800
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 775801
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 775802
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 775803
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 775804
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 775805
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 775806
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 775807
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 775808
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 775809
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 775810
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 775811
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 775812
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 775813
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 775814
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 775815
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775816
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775762
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775763
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775764
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 775765
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775766
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 775767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    .line 775768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 775769
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 775770
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    .line 775771
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 775772
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775773
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 775774
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 775775
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775776
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 775777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 775779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 775780
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775781
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 775782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775783
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 775784
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 775785
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775786
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775787
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775759
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 775757
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->h:J

    .line 775758
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 775754
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 775755
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->h:J

    .line 775756
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o:Ljava/lang/String;

    .line 775753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 775746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 775748
    :goto_0
    return-object v0

    .line 775749
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 775750
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775745
    const v0, 0x3bf6f008

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->f:Ljava/lang/String;

    .line 775744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    .line 775741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775733
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775734
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775735
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 775732
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 775731
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775728
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775729
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775730
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
