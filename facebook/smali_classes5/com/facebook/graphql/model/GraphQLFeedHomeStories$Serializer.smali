.class public final Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 751113
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 751114
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 751115
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 751116
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 751117
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 751118
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 751119
    invoke-virtual {v1, v0, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 751120
    if-eqz v2, :cond_0

    .line 751121
    const-string v3, "approximate_new_unit_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751122
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 751123
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 751124
    if-eqz v2, :cond_1

    .line 751125
    const-string v3, "debug_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751126
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751127
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 751128
    if-eqz v2, :cond_3

    .line 751129
    const-string v3, "edges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751130
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 751131
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 751132
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/4Mc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 751133
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 751134
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 751135
    :cond_3
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 751136
    if-eqz v2, :cond_4

    .line 751137
    const-string v2, "low_engagement_deduplication_keys"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751138
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 751139
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 751140
    if-eqz v2, :cond_5

    .line 751141
    const-string v3, "no_feed_polling"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751142
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 751143
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 751144
    if-eqz v2, :cond_6

    .line 751145
    const-string v3, "page_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751146
    invoke-static {v1, v2, p1}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 751147
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 751148
    if-eqz v2, :cond_7

    .line 751149
    const-string v3, "promotion_unit_at_top"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751150
    invoke-static {v1, v2, p1}, LX/4Rt;->a(LX/15i;ILX/0nX;)V

    .line 751151
    :cond_7
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 751152
    if-eqz v2, :cond_8

    .line 751153
    const-string v3, "query_function"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751154
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751155
    :cond_8
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 751156
    if-eqz v2, :cond_9

    .line 751157
    const-string v3, "query_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 751158
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 751159
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 751160
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 751161
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0nX;LX/0my;)V

    return-void
.end method
