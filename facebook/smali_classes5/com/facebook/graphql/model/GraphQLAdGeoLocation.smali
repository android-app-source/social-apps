.class public final Lcom/facebook/graphql/model/GraphQLAdGeoLocation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:D

.field public k:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public l:D

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:D

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743710
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743709
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 743786
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 743787
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->e:Ljava/lang/String;

    .line 743785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->f:Ljava/lang/String;

    .line 743782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->g:Ljava/lang/String;

    .line 743779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->h:Ljava/lang/String;

    .line 743776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->i:Ljava/lang/String;

    .line 743773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743768
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743769
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743770
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->j:D

    return-wide v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743765
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->k:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743766
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->k:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->k:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 743767
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->k:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    return-object v0
.end method

.method private p()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743762
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743763
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743764
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->l:D

    return-wide v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->m:Ljava/lang/String;

    .line 743761
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 743756
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743757
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743758
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->n:D

    return-wide v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743753
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743754
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o:Ljava/lang/String;

    .line 743755
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743750
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743751
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743752
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->p:Z

    return v0
.end method

.method private u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743747
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743748
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743749
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->q:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 743722
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743723
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 743724
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 743725
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 743726
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 743727
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 743728
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 743729
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 743730
    const/16 v9, 0xd

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 743731
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 743732
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 743733
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 743734
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 743735
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 743736
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->n()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 743737
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 743738
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->p()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 743739
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 743740
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->r()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 743741
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 743742
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->t()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 743743
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->u()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 743744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743745
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 743746
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->o()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 743719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743720
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743721
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 743712
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 743713
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->j:D

    .line 743714
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->l:D

    .line 743715
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->n:D

    .line 743716
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->p:Z

    .line 743717
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAdGeoLocation;->q:Z

    .line 743718
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 743711
    const v0, -0x419661d

    return v0
.end method
