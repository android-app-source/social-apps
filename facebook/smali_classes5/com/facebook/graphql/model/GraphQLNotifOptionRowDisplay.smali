.class public final Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 769860
    const-class v0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 769859
    const-class v0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 769857
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 769858
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769851
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 769852
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 769853
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 769854
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 769855
    const/4 v0, 0x0

    .line 769856
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->f:Ljava/lang/String;

    .line 769850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->g:Ljava/lang/String;

    .line 769847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 769842
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->i:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769843
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->i:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->i:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 769844
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->i:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769839
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769840
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769841
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l:Ljava/lang/String;

    .line 769835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769830
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769831
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769832
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 769827
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 769828
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769829
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 769802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 769803
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 769804
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 769805
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 769806
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 769807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 769808
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 769809
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 769810
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 769811
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 769812
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 769813
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 769814
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 769815
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 769816
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 769817
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 769818
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 769819
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 769820
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 769821
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 769822
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 769823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 769824
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 769825
    goto/16 :goto_0

    .line 769826
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 769774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 769775
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 769776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 769777
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 769778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    .line 769779
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769780
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 769781
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 769783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    .line 769784
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769785
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 769786
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769787
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 769788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    .line 769789
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769790
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 769791
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769792
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 769793
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    .line 769794
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769795
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 769796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 769798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;

    .line 769799
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNotifOptionRowDisplay;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769800
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 769801
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 769773
    const v0, 0x3cc5a70d

    return v0
.end method
