.class public final Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 777697
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 777698
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 777700
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 777701
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 777702
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 777703
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 777704
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 777705
    if-eqz v2, :cond_0

    .line 777706
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777707
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777708
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 777709
    if-eqz v2, :cond_1

    .line 777710
    const-string v3, "item_attribution"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777711
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 777712
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 777713
    if-eqz v2, :cond_2

    .line 777714
    const-string v3, "savable_actors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777715
    invoke-static {v1, v2, p1, p2}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 777716
    :cond_2
    invoke-virtual {v1, v0, v5, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 777717
    if-eqz v2, :cond_3

    .line 777718
    const-string v2, "savable_container_category"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777719
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777720
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 777721
    if-eqz v2, :cond_4

    .line 777722
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777723
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777724
    :cond_4
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 777725
    if-eqz v2, :cond_5

    .line 777726
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 777727
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 777728
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 777729
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 777699
    check-cast p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;LX/0nX;LX/0my;)V

    return-void
.end method
