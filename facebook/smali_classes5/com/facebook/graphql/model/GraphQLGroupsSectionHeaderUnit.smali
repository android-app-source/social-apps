.class public final Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public x:J

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760857
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 760858
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 760859
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 760860
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x655e82e2

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 760861
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->D:LX/0x2;

    .line 760862
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760863
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760864
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760865
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760866
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760867
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 760868
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760869
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760870
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760871
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C:Ljava/lang/String;

    .line 760874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C:Ljava/lang/String;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760875
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760876
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->f:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->f:Ljava/util/List;

    .line 760877
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760878
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760879
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g:Ljava/util/List;

    .line 760880
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760881
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760882
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 760883
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 760886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m:Ljava/lang/String;

    .line 760889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760890
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760891
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 760892
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760893
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760894
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 760895
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760896
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760897
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 760898
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760899
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760900
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->q:Ljava/lang/String;

    .line 760901
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private v()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760694
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760695
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s:Ljava/util/List;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s:Ljava/util/List;

    .line 760696
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760854
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760855
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 760856
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760902
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760903
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w:Ljava/util/List;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w:Ljava/util/List;

    .line 760904
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private y()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760691
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 760692
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 760693
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->x:J

    return-wide v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760697
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760698
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->y:Ljava/lang/String;

    .line 760699
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->y:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 760700
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760701
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760702
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->i:Ljava/lang/String;

    .line 760703
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 760704
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 760705
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 760706
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 760707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->D:LX/0x2;

    if-nez v0, :cond_0

    .line 760708
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->D:LX/0x2;

    .line 760709
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->D:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 24

    .prologue
    .line 760710
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760711
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 760712
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 760713
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 760714
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->E_()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 760715
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 760716
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 760717
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->q()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 760718
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 760719
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 760720
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 760721
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 760722
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->d(Ljava/util/List;)I

    move-result v13

    .line 760723
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 760724
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->c()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 760725
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 760726
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->x()LX/0Px;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v17

    .line 760727
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 760728
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 760729
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 760730
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 760731
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->D()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 760732
    const/16 v23, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 760733
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 760734
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 760735
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 760736
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 760737
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 760738
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 760739
    const/4 v3, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 760740
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 760741
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 760742
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 760743
    const/16 v3, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 760744
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 760745
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 760746
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 760747
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 760748
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 760749
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760750
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760751
    const/16 v3, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->y()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 760752
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760753
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760754
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760755
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760756
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 760757
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760758
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 760759
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 760760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 760761
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 760762
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 760763
    if-eqz v1, :cond_0

    .line 760764
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760765
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->f:Ljava/util/List;

    .line 760766
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 760767
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 760768
    if-eqz v1, :cond_1

    .line 760769
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760770
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 760771
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 760772
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 760773
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 760774
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760775
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 760776
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 760777
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->x()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 760778
    if-eqz v2, :cond_3

    .line 760779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760780
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->w:Ljava/util/List;

    move-object v1, v0

    .line 760781
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 760782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 760783
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 760784
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760785
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 760786
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 760787
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 760788
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->p()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 760789
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760790
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 760791
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 760792
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760793
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 760794
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760795
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760796
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 760797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 760798
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 760799
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760800
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 760801
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 760802
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 760803
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 760804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760805
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->o:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 760806
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 760807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 760808
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 760809
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760810
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->A:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 760811
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 760812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 760813
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 760814
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760815
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 760816
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 760817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 760819
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760820
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760821
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 760822
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760823
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 760824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 760825
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760826
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 760827
    if-nez v1, :cond_d

    :goto_0
    return-object p0

    :cond_d
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760828
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 760829
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l:J

    .line 760830
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 760831
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 760832
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l:J

    .line 760833
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->x:J

    .line 760834
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760835
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->u:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->u:Ljava/lang/String;

    .line 760838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 760840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 760841
    :goto_0
    return-object v0

    .line 760842
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 760843
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 760844
    const v0, 0x655e82e2

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->h:Ljava/lang/String;

    .line 760847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 760850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->r:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 760851
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 760852
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 760853
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
