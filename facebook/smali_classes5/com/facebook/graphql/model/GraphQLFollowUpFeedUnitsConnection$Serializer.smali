.class public final Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 751528
    const-class v0, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 751529
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 751530
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 751525
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 751526
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/4Mi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 751527
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 751524
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection$Serializer;->a(Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;LX/0nX;LX/0my;)V

    return-void
.end method
