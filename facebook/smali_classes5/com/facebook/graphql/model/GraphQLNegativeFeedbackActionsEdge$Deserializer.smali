.class public final Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 768128
    const-class v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 768129
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 768127
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 768108
    const/16 v0, 0xef

    .line 768109
    const/4 v1, 0x1

    const/4 p2, 0x0

    .line 768110
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 768111
    invoke-static {p1, v3}, LX/2Zc;->b(LX/15w;LX/186;)I

    move-result v2

    .line 768112
    if-eqz v1, :cond_0

    .line 768113
    const/4 p0, 0x2

    invoke-virtual {v3, p0}, LX/186;->c(I)V

    .line 768114
    invoke-virtual {v3, p2, v0, p2}, LX/186;->a(ISI)V

    .line 768115
    const/4 p0, 0x1

    invoke-virtual {v3, p0, v2}, LX/186;->b(II)V

    .line 768116
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 768117
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 768118
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 768119
    move-object v2, v1

    .line 768120
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;-><init>()V

    .line 768121
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 768122
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 768123
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 768124
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 768125
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 768126
    :cond_1
    return-object v1
.end method
