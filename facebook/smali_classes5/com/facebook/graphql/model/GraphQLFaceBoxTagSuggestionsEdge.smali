.class public final Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751061
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 751060
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 751058
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 751059
    return-void
.end method

.method public constructor <init>(LX/4WI;)V
    .locals 1

    .prologue
    .line 751031
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 751032
    iget-object v0, p1, LX/4WI;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->e:Ljava/lang/String;

    .line 751033
    iget-object v0, p1, LX/4WI;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 751034
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 751050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 751052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 751053
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 751054
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 751055
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 751056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751057
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 751042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 751043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 751044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 751045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 751046
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;

    .line 751047
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 751048
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 751049
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->e:Ljava/lang/String;

    .line 751041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 751038
    const v0, -0x4e984300

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 751035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 751036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 751037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method
