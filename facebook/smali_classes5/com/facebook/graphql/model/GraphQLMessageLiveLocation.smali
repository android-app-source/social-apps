.class public final Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLCoordinate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766192
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766201
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 766199
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 766200
    return-void
.end method

.method private j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766196
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766197
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766198
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->e:Z

    return v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLCoordinate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766193
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->f:Lcom/facebook/graphql/model/GraphQLCoordinate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766194
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->f:Lcom/facebook/graphql/model/GraphQLCoordinate;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLCoordinate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCoordinate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->f:Lcom/facebook/graphql/model/GraphQLCoordinate;

    .line 766195
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->f:Lcom/facebook/graphql/model/GraphQLCoordinate;

    return-object v0
.end method

.method private l()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766189
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766190
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766191
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->g:J

    return-wide v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->h:Ljava/lang/String;

    .line 766188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->i:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->i:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->i:Lcom/facebook/graphql/model/GraphQLUser;

    .line 766185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->i:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766202
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766203
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766204
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->j:Z

    return v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k:Ljava/lang/String;

    .line 766182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->l:Ljava/lang/String;

    .line 766179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766174
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766175
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m:Ljava/lang/String;

    .line 766176
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 766135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766136
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k()Lcom/facebook/graphql/model/GraphQLCoordinate;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 766137
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 766138
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 766139
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 766140
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 766141
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 766142
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 766143
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->j()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 766144
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 766145
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 766146
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 766147
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 766148
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 766149
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 766150
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 766151
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 766152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766153
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 766161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766162
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k()Lcom/facebook/graphql/model/GraphQLCoordinate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 766163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k()Lcom/facebook/graphql/model/GraphQLCoordinate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCoordinate;

    .line 766164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->k()Lcom/facebook/graphql/model/GraphQLCoordinate;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 766165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;

    .line 766166
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->f:Lcom/facebook/graphql/model/GraphQLCoordinate;

    .line 766167
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 766168
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 766169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 766170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;

    .line 766171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->i:Lcom/facebook/graphql/model/GraphQLUser;

    .line 766172
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766173
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766160
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 766155
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 766156
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->e:Z

    .line 766157
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->g:J

    .line 766158
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMessageLiveLocation;->j:Z

    .line 766159
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 766154
    const v0, -0x16bdadb8

    return v0
.end method
