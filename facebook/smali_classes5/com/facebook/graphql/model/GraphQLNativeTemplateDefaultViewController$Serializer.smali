.class public final Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 767552
    const-class v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 767553
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767554
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 767555
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 767556
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 767557
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 767558
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 767559
    if-eqz v2, :cond_0

    .line 767560
    const-string p0, "analytics_module"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767561
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 767562
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 767563
    if-eqz v2, :cond_1

    .line 767564
    const-string p0, "background_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767565
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 767566
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 767567
    if-eqz v2, :cond_2

    .line 767568
    const-string p0, "can_ptr"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767569
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 767570
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 767571
    if-eqz v2, :cond_3

    .line 767572
    const-string p0, "native_template_view"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 767573
    invoke-static {v1, v2, p1, p2}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 767574
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 767575
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 767576
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController$Serializer;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateDefaultViewController;LX/0nX;LX/0my;)V

    return-void
.end method
