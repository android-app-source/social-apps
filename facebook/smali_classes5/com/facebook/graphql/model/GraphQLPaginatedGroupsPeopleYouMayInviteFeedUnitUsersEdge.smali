.class public final Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772191
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 772192
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772193
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 772194
    return-void
.end method

.method public constructor <init>(LX/4Xi;)V
    .locals 1

    .prologue
    .line 772209
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 772210
    iget-object v0, p1, LX/4Xi;->b:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 772211
    iget-object v0, p1, LX/4Xi;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->g:Ljava/lang/String;

    .line 772212
    iget-object v0, p1, LX/4Xi;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772213
    iget-object v0, p1, LX/4Xi;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 772214
    iget-object v0, p1, LX/4Xi;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->f:Ljava/lang/String;

    .line 772215
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 772195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 772197
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 772198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 772199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 772200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 772201
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 772202
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 772203
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 772204
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 772205
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 772206
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 772207
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772208
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 772170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 772172
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 772173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 772174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 772175
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 772176
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 772177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 772179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 772180
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772181
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 772182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 772183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 772184
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 772185
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 772186
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 772187
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 772190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->e:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772161
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772162
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->f:Ljava/lang/String;

    .line 772163
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 772160
    const v0, 0x40771b24

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772157
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->g:Ljava/lang/String;

    .line 772159
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772164
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772165
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772166
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 772167
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 772168
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 772169
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->i:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method
