.class public final Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783724
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 783725
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 783742
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 783743
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->i:Ljava/lang/String;

    .line 783728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 783729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 783731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 783732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 783733
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 783734
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 783735
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->k()Z

    move-result v3

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 783736
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 783737
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->m()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 783738
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 783739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783740
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 783741
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->m()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 783715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 783716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    .line 783718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 783719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;

    .line 783720
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    .line 783721
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 783722
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783723
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 783712
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 783713
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->f:Z

    .line 783714
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 783711
    const v0, 0x684f0b47

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783708
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783709
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->e:Ljava/lang/String;

    .line 783710
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 783705
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 783706
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 783707
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->f:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 783702
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783703
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    .line 783704
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->g:Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 783699
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 783700
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 783701
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->h:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    return-object v0
.end method
