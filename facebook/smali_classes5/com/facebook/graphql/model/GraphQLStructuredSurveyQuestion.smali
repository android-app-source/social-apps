.class public final Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785351
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 785350
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 785348
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 785349
    return-void
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->h:Ljava/lang/String;

    .line 785344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->h:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k:Ljava/lang/String;

    .line 785341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 785321
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785322
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 785323
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 785324
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 785325
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 785326
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 785327
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 785328
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 785329
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 785330
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 785331
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 785332
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 785333
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->l()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 785334
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 785335
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 785336
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785337
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 785338
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->l()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 785303
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785304
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 785305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785306
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 785307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    .line 785308
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785309
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785310
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785311
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 785312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    .line 785313
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785314
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 785315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 785316
    if-eqz v2, :cond_2

    .line 785317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    .line 785318
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j:Ljava/util/List;

    move-object v1, v0

    .line 785319
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 785320
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 785302
    const v0, 0x1acc9d31

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 785301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 785296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->f:Ljava/lang/String;

    .line 785298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 785293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 785295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 785290
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 785291
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j:Ljava/util/List;

    .line 785292
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
