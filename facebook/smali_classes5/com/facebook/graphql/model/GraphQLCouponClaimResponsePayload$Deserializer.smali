.class public final Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 746838
    const-class v0, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 746839
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746840
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 746841
    const/16 v0, 0x257

    .line 746842
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 746843
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 746844
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 746845
    const/4 v2, 0x0

    .line 746846
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 746847
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 746848
    :goto_0
    move v2, v4

    .line 746849
    if-eqz v1, :cond_0

    .line 746850
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 746851
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 746852
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 746853
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 746854
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 746855
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 746856
    move-object v2, v1

    .line 746857
    new-instance v1, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLCouponClaimResponsePayload;-><init>()V

    .line 746858
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 746859
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 746860
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 746861
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 746862
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 746863
    :cond_1
    return-object v1

    .line 746864
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_5

    .line 746865
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 746866
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 746867
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 746868
    const-string p0, "claim_result"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 746869
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    move-result-object v2

    move-object v8, v2

    move v2, v6

    goto :goto_1

    .line 746870
    :cond_3
    const-string p0, "claim_result_message"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 746871
    invoke-static {p1, v3}, LX/4L4;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 746872
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 746873
    :cond_5
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 746874
    if-eqz v2, :cond_6

    .line 746875
    invoke-virtual {v3, v4, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 746876
    :cond_6
    invoke-virtual {v3, v6, v7}, LX/186;->b(II)V

    .line 746877
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_7
    move v7, v4

    move-object v8, v2

    move v2, v4

    goto :goto_1
.end method
