.class public final Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSouvenir;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 783098
    const-class v0, Lcom/facebook/graphql/model/GraphQLSouvenir;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 783099
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 783101
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLSouvenir;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 783102
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 783103
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 783104
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 783105
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 783106
    if-eqz v2, :cond_0

    .line 783107
    const-string p0, "container_post"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783108
    invoke-static {v1, v2, p1, p2}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 783109
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 783110
    if-eqz v2, :cond_1

    .line 783111
    const-string p0, "formatting_string"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783112
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 783113
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 783114
    if-eqz v2, :cond_2

    .line 783115
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783116
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 783117
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 783118
    if-eqz v2, :cond_3

    .line 783119
    const-string p0, "media_elements"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783120
    invoke-static {v1, v2, p1, p2}, LX/4T3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 783121
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 783122
    if-eqz v2, :cond_4

    .line 783123
    const-string p0, "souvenir_cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783124
    invoke-static {v1, v2, p1, p2}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 783125
    :cond_4
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 783126
    if-eqz v2, :cond_5

    .line 783127
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783128
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 783129
    :cond_5
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 783130
    if-eqz v2, :cond_6

    .line 783131
    const-string p0, "titleForSummary"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783132
    invoke-static {v1, v2, p1, p2}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 783133
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 783134
    if-eqz v2, :cond_7

    .line 783135
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 783136
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 783137
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 783138
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 783100
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSouvenir;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLSouvenir$Serializer;->a(Lcom/facebook/graphql/model/GraphQLSouvenir;LX/0nX;LX/0my;)V

    return-void
.end method
