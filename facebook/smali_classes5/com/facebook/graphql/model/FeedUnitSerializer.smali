.class public Lcom/facebook/graphql/model/FeedUnitSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 742952
    const-class v0, Lcom/facebook/graphql/model/FeedUnit;

    new-instance v1, Lcom/facebook/graphql/model/FeedUnitSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/FeedUnitSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 742953
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 742954
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/FeedUnit;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 742955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-interface {p0}, LX/0jT;->f()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 742956
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 742957
    const-class v1, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit;

    .line 742958
    :goto_1
    move-object v0, v1

    .line 742959
    invoke-static {v0}, LX/2Ah;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 742960
    invoke-virtual {v0, p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 742961
    return-void

    .line 742962
    :sswitch_0
    const-string v2, "Story"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "StorySet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "CelebrationsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "HoldoutAdFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "SurveyFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "ResearchPollFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "SocialWifiFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "InstagramPhotosFromFriendsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "GreetingCardPromotionFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "FriendsLocationsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "QuickPromotionFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "QuickPromotionNativeTemplateFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "GoodwillAnniversaryCampaignFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "PlaceReviewFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "PageStoriesYouMissedFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "PagesYouMayAdvertiseFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "PagesYouMayLikeFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "GroupsYouShouldJoinFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "GroupsYouShouldCreateFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "SaleGroupsNearYouFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "GroupTopStoriesFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "SavedCollectionFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v2, "VideoChainingFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v2, "ArticleChainingFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v2, "EventCollectionFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v2, "UnseenStoriesFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v2, "StoryGallerySurveyFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v2, "MobilePageAdminPanelFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v2, "PaginatedGroupsYouShouldJoinFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v2, "CustomizedStory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v2, "CreativePagesYouMayLikeFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v2, "PYMLWithLargeImageFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v2, "ClientBumpingPlaceHolderFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v2, "PeopleYouMayInviteFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v2, "PaginatedPeopleYouMayKnowFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v2, "GoodwillThrowbackPromotionFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v2, "GoodwillThrowbackFriendversaryStory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v2, "NoContentFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v2, "NoContentGoodFriendsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v2, "NuxGoodFriendsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v2, "FindPagesFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v2, "FindFriendsFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v2, "PeopleYouShouldFollowFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v2, "CommerceSaleStoriesFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v2, "TopicCustomizationStory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string v2, "ProductsDealsForYouFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string v2, "MessengerActiveNowFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string v2, "MessengerGenericFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string v2, "AYMTPageSlideshowFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string v2, "PaginatedGroupsPeopleYouMayInviteFeedUnit"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x31

    goto/16 :goto_0

    .line 742963
    :pswitch_0
    const-class v1, Lcom/facebook/graphql/model/GraphQLStory;

    goto/16 :goto_1

    .line 742964
    :pswitch_1
    const-class v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    goto/16 :goto_1

    .line 742965
    :pswitch_2
    const-class v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    goto/16 :goto_1

    .line 742966
    :pswitch_3
    const-class v1, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;

    goto/16 :goto_1

    .line 742967
    :pswitch_4
    const-class v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    goto/16 :goto_1

    .line 742968
    :pswitch_5
    const-class v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    goto/16 :goto_1

    .line 742969
    :pswitch_6
    const-class v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    goto/16 :goto_1

    .line 742970
    :pswitch_7
    const-class v1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    goto/16 :goto_1

    .line 742971
    :pswitch_8
    const-class v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    goto/16 :goto_1

    .line 742972
    :pswitch_9
    const-class v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    goto/16 :goto_1

    .line 742973
    :pswitch_a
    const-class v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    goto/16 :goto_1

    .line 742974
    :pswitch_b
    const-class v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    goto/16 :goto_1

    .line 742975
    :pswitch_c
    const-class v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    goto/16 :goto_1

    .line 742976
    :pswitch_d
    const-class v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    goto/16 :goto_1

    .line 742977
    :pswitch_e
    const-class v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    goto/16 :goto_1

    .line 742978
    :pswitch_f
    const-class v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    goto/16 :goto_1

    .line 742979
    :pswitch_10
    const-class v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    goto/16 :goto_1

    .line 742980
    :pswitch_11
    const-class v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    goto/16 :goto_1

    .line 742981
    :pswitch_12
    const-class v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    goto/16 :goto_1

    .line 742982
    :pswitch_13
    const-class v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    goto/16 :goto_1

    .line 742983
    :pswitch_14
    const-class v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    goto/16 :goto_1

    .line 742984
    :pswitch_15
    const-class v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    goto/16 :goto_1

    .line 742985
    :pswitch_16
    const-class v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    goto/16 :goto_1

    .line 742986
    :pswitch_17
    const-class v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    goto/16 :goto_1

    .line 742987
    :pswitch_18
    const-class v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    goto/16 :goto_1

    .line 742988
    :pswitch_19
    const-class v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;

    goto/16 :goto_1

    .line 742989
    :pswitch_1a
    const-class v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    goto/16 :goto_1

    .line 742990
    :pswitch_1b
    const-class v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    goto/16 :goto_1

    .line 742991
    :pswitch_1c
    const-class v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    goto/16 :goto_1

    .line 742992
    :pswitch_1d
    const-class v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    goto/16 :goto_1

    .line 742993
    :pswitch_1e
    const-class v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    goto/16 :goto_1

    .line 742994
    :pswitch_1f
    const-class v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    goto/16 :goto_1

    .line 742995
    :pswitch_20
    const-class v1, Lcom/facebook/graphql/model/GraphQLClientBumpingPlaceHolderFeedUnit;

    goto/16 :goto_1

    .line 742996
    :pswitch_21
    const-class v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    goto/16 :goto_1

    .line 742997
    :pswitch_22
    const-class v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    goto/16 :goto_1

    .line 742998
    :pswitch_23
    const-class v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    goto/16 :goto_1

    .line 742999
    :pswitch_24
    const-class v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    goto/16 :goto_1

    .line 743000
    :pswitch_25
    const-class v1, Lcom/facebook/graphql/model/GraphQLNoContentFeedUnit;

    goto/16 :goto_1

    .line 743001
    :pswitch_26
    const-class v1, Lcom/facebook/graphql/model/GraphQLNoContentGoodFriendsFeedUnit;

    goto/16 :goto_1

    .line 743002
    :pswitch_27
    const-class v1, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;

    goto/16 :goto_1

    .line 743003
    :pswitch_28
    const-class v1, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    goto/16 :goto_1

    .line 743004
    :pswitch_29
    const-class v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    goto/16 :goto_1

    .line 743005
    :pswitch_2a
    const-class v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    goto/16 :goto_1

    .line 743006
    :pswitch_2b
    const-class v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    goto/16 :goto_1

    .line 743007
    :pswitch_2c
    const-class v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    goto/16 :goto_1

    .line 743008
    :pswitch_2d
    const-class v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    goto/16 :goto_1

    .line 743009
    :pswitch_2e
    const-class v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    goto/16 :goto_1

    .line 743010
    :pswitch_2f
    const-class v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    goto/16 :goto_1

    .line 743011
    :pswitch_30
    const-class v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x72442839 -> :sswitch_15
        -0x70a4a2e1 -> :sswitch_f
        -0x681541e0 -> :sswitch_21
        -0x5fe74222 -> :sswitch_29
        -0x5bdbec3a -> :sswitch_2f
        -0x5b14d858 -> :sswitch_1b
        -0x5af615b9 -> :sswitch_23
        -0x56fb7e27 -> :sswitch_17
        -0x54662c46 -> :sswitch_25
        -0x53a428b7 -> :sswitch_27
        -0x533f585d -> :sswitch_10
        -0x51fc7735 -> :sswitch_9
        -0x4ed3e2e6 -> :sswitch_18
        -0x4c06fe62 -> :sswitch_13
        -0x496e994a -> :sswitch_2a
        -0x46f2ee24 -> :sswitch_4
        -0x4688f7fe -> :sswitch_26
        -0x46799faf -> :sswitch_2c
        -0x46672d33 -> :sswitch_28
        -0x44774584 -> :sswitch_5
        -0x4064a59f -> :sswitch_19
        -0x383fb37f -> :sswitch_d
        -0x342a59b5 -> :sswitch_2
        -0x3369315c -> :sswitch_6
        -0x30c82027 -> :sswitch_1a
        -0x2dc9932c -> :sswitch_1d
        -0x2ca60061 -> :sswitch_2e
        -0x275591d0 -> :sswitch_1f
        -0x21900448 -> :sswitch_e
        -0x1d51a5c6 -> :sswitch_7
        -0x1b7352de -> :sswitch_30
        -0xe59b30c -> :sswitch_3
        0x4717afd -> :sswitch_1c
        0x4c808d5 -> :sswitch_0
        0x542975e -> :sswitch_16
        0x10f699c1 -> :sswitch_31
        0x1409faf4 -> :sswitch_1e
        0x14583a39 -> :sswitch_24
        0x1757d8b4 -> :sswitch_20
        0x180c5609 -> :sswitch_b
        0x193455fc -> :sswitch_8
        0x42554c1c -> :sswitch_12
        0x4af7005f -> :sswitch_14
        0x5a045f73 -> :sswitch_2b
        0x61d8ffb8 -> :sswitch_a
        0x655ab173 -> :sswitch_c
        0x65f211ca -> :sswitch_11
        0x6a3d0f4d -> :sswitch_1
        0x78e4008f -> :sswitch_22
        0x7af1f23b -> :sswitch_2d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 743012
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/FeedUnitSerializer;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0nX;LX/0my;)V

    return-void
.end method
