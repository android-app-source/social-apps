.class public final Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillCampaign$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillCampaign$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageOverlay;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755580
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 755581
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 755582
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755583
    return-void
.end method

.method public constructor <init>(LX/4WW;)V
    .locals 1

    .prologue
    .line 755584
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 755585
    iget-object v0, p1, LX/4WW;->b:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755586
    iget-object v0, p1, LX/4WW;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->g:Ljava/lang/String;

    .line 755587
    iget-object v0, p1, LX/4WW;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755588
    iget-object v0, p1, LX/4WW;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755589
    iget-object v0, p1, LX/4WW;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755590
    iget-object v0, p1, LX/4WW;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    .line 755591
    iget-object v0, p1, LX/4WW;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755592
    iget-object v0, p1, LX/4WW;->i:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755593
    iget-object v0, p1, LX/4WW;->j:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755594
    iget-object v0, p1, LX/4WW;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755595
    iget-object v0, p1, LX/4WW;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755596
    iget-object v0, p1, LX/4WW;->m:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    .line 755597
    iget-object v0, p1, LX/4WW;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r:Ljava/lang/String;

    .line 755598
    iget-object v0, p1, LX/4WW;->o:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    .line 755599
    iget-object v0, p1, LX/4WW;->p:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    .line 755600
    iget-object v0, p1, LX/4WW;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755601
    iget-object v0, p1, LX/4WW;->r:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755602
    iget-object v0, p1, LX/4WW;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755603
    iget-object v0, p1, LX/4WW;->t:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    .line 755604
    iget-object v0, p1, LX/4WW;->u:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755605
    iget-object v0, p1, LX/4WW;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755606
    iget-object v0, p1, LX/4WW;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755607
    iget-object v0, p1, LX/4WW;->x:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755608
    iget-object v0, p1, LX/4WW;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755609
    iget-object v0, p1, LX/4WW;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755610
    iget-object v0, p1, LX/4WW;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D:Ljava/lang/String;

    .line 755611
    iget-object v0, p1, LX/4WW;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755612
    iget-object v0, p1, LX/4WW;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755613
    iget-object v0, p1, LX/4WW;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755614
    iget-object v0, p1, LX/4WW;->E:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755615
    iget-object v0, p1, LX/4WW;->F:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 755616
    return-void
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755617
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->g:Ljava/lang/String;

    .line 755619
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->g:Ljava/lang/String;

    return-object v0
.end method

.method private D()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755620
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755621
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    .line 755622
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755623
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755624
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755625
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755626
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755627
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755628
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755629
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755630
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755631
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755635
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755636
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755637
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755632
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755633
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755634
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D:Ljava/lang/String;

    .line 755658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D:Ljava/lang/String;

    return-object v0
.end method

.method private N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755644
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755645
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755646
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755638
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755639
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755640
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 34

    .prologue
    .line 755375
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755376
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 755377
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 755378
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 755379
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 755380
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 755381
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 755382
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 755383
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 755384
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 755385
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 755386
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 755387
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 755388
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 755389
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 755390
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v16

    .line 755391
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v17

    .line 755392
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 755393
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 755394
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v20

    .line 755395
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 755396
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 755397
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 755398
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 755399
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 755400
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 755401
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->M()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 755402
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 755403
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 755404
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 755405
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 755406
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 755407
    const/16 v33, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 755408
    const/16 v33, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 755409
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 755410
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 755411
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 755412
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 755413
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 755414
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 755415
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 755416
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 755417
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 755418
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 755419
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 755420
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 755421
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 755422
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755423
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755424
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755425
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755426
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755427
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755428
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755429
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755430
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755431
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755432
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755433
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755434
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755435
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755436
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755437
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755438
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 755439
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755440
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 755441
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 755442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 755443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 755444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 755446
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755447
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755448
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 755449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 755451
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755452
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755453
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 755454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755455
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 755456
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755457
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755458
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 755459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 755461
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755462
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755463
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 755464
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755465
    if-eqz v2, :cond_4

    .line 755466
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755467
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k:Ljava/util/List;

    move-object v1, v0

    .line 755468
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 755469
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755470
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 755471
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755472
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755473
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 755474
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755475
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 755476
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755477
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755478
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 755479
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755480
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 755481
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755482
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755483
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 755484
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755485
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 755486
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755487
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755488
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 755489
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755490
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 755491
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755492
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755493
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 755494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 755495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 755496
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755497
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    .line 755498
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 755499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755500
    if-eqz v2, :cond_b

    .line 755501
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755502
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    move-object v1, v0

    .line 755503
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 755504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755505
    if-eqz v2, :cond_c

    .line 755506
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755507
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    move-object v1, v0

    .line 755508
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 755509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 755511
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755512
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755513
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 755514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 755516
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755517
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755518
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 755519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 755521
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755522
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755523
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 755524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 755525
    if-eqz v2, :cond_10

    .line 755526
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755527
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    move-object v1, v0

    .line 755528
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 755529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 755531
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755532
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755533
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 755534
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755535
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 755536
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755537
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755538
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 755539
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755540
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->K()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 755541
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755542
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755543
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 755544
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 755545
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 755546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755547
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755548
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 755549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 755551
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755552
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755553
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 755554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 755556
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755557
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755558
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 755559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 755561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755562
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755563
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 755564
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755565
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 755566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755567
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755568
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 755569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 755571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755572
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755573
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 755574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 755576
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 755577
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->H:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 755578
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 755579
    if-nez v1, :cond_1b

    :goto_0
    return-object p0

    :cond_1b
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755319
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 755320
    const v0, -0x23504ba1

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755321
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 755322
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 755323
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 755324
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 755325
    const/4 v0, 0x0

    .line 755326
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 755329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 755338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    .line 755341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r:Ljava/lang/String;

    .line 755344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageOverlay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    .line 755347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755348
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755349
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    .line 755350
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 755356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 755357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    .line 755359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755360
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755361
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 755362
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755363
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755365
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755369
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755370
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755371
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 755372
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 755373
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 755374
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
