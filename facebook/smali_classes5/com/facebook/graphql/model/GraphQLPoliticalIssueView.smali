.class public final Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPoliticalIssueView$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPoliticalIssueView$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775955
    const-class v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775954
    const-class v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 775952
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775953
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775949
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775950
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->h:Ljava/lang/String;

    .line 775951
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 775937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 775939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 775940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 775941
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 775942
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 775943
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 775944
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 775945
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 775946
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 775947
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775948
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 775959
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775960
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;

    .line 775961
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775962
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775963
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775935
    const v0, -0x7230133f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->e:Ljava/lang/String;

    .line 775934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->f:Ljava/lang/String;

    .line 775931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method
