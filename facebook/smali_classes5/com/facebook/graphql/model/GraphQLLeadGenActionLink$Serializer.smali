.class public final Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 762724
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 762725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 762723
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 762534
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 762535
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x17

    const/16 v5, 0x15

    const/4 v4, 0x0

    .line 762536
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 762537
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762538
    if-eqz v2, :cond_0

    .line 762539
    const-string v3, "ad_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762540
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762541
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762542
    if-eqz v2, :cond_1

    .line 762543
    const-string v3, "agree_to_privacy_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762544
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762545
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 762546
    if-eqz v2, :cond_2

    .line 762547
    const-string v3, "android_minimal_screen_form_height"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762548
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 762549
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 762550
    if-eqz v2, :cond_3

    .line 762551
    const-string v3, "android_small_screen_phone_threshold"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762552
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 762553
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762554
    if-eqz v2, :cond_4

    .line 762555
    const-string v3, "disclaimer_accept_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762556
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762557
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762558
    if-eqz v2, :cond_5

    .line 762559
    const-string v3, "disclaimer_continue_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762560
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762561
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762562
    if-eqz v2, :cond_6

    .line 762563
    const-string v3, "error_codes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762564
    invoke-static {v1, v2, p1, p2}, LX/4Ou;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 762565
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762566
    if-eqz v2, :cond_7

    .line 762567
    const-string v3, "error_message_brief"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762568
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762569
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762570
    if-eqz v2, :cond_8

    .line 762571
    const-string v3, "error_message_detail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762572
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762573
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762574
    if-eqz v2, :cond_9

    .line 762575
    const-string v3, "fb_data_policy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762576
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762577
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762578
    if-eqz v2, :cond_a

    .line 762579
    const-string v3, "fb_data_policy_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762580
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762581
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762582
    if-eqz v2, :cond_b

    .line 762583
    const-string v3, "follow_up_action_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762584
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762585
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762586
    if-eqz v2, :cond_c

    .line 762587
    const-string v3, "follow_up_action_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762588
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762589
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762590
    if-eqz v2, :cond_d

    .line 762591
    const-string v3, "landing_page_cta"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762592
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762593
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762594
    if-eqz v2, :cond_e

    .line 762595
    const-string v3, "landing_page_redirect_instruction"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762596
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762597
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762598
    if-eqz v2, :cond_f

    .line 762599
    const-string v3, "lead_gen_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762600
    invoke-static {v1, v2, p1, p2}, LX/4Os;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 762601
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762602
    if-eqz v2, :cond_10

    .line 762603
    const-string v3, "lead_gen_data_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762604
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762605
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762606
    if-eqz v2, :cond_11

    .line 762607
    const-string v3, "lead_gen_deep_link_user_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762608
    invoke-static {v1, v2, p1}, LX/4Ot;->a(LX/15i;ILX/0nX;)V

    .line 762609
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762610
    if-eqz v2, :cond_12

    .line 762611
    const-string v3, "lead_gen_user_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762612
    invoke-static {v1, v2, p1}, LX/4P2;->a(LX/15i;ILX/0nX;)V

    .line 762613
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762614
    if-eqz v2, :cond_13

    .line 762615
    const-string v3, "link_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762616
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762617
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762618
    if-eqz v2, :cond_14

    .line 762619
    const-string v3, "link_display"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762620
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762621
    :cond_14
    invoke-virtual {v1, v0, v5, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 762622
    if-eqz v2, :cond_15

    .line 762623
    const-string v2, "link_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762624
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-virtual {v1, v0, v5, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762625
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762626
    if-eqz v2, :cond_16

    .line 762627
    const-string v3, "link_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762628
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762629
    :cond_16
    invoke-virtual {v1, v0, p0, v4}, LX/15i;->a(IIS)S

    move-result v2

    .line 762630
    if-eqz v2, :cond_17

    .line 762631
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762632
    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v0, p0, v2}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762633
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762634
    if-eqz v2, :cond_18

    .line 762635
    const-string v3, "link_video_endscreen_icon"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762636
    invoke-static {v1, v2, p1}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 762637
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762638
    if-eqz v2, :cond_19

    .line 762639
    const-string v3, "nux_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762640
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762641
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762642
    if-eqz v2, :cond_1a

    .line 762643
    const-string v3, "nux_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762644
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762645
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 762646
    if-eqz v2, :cond_1b

    .line 762647
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762648
    invoke-static {v1, v2, p1, p2}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 762649
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762650
    if-eqz v2, :cond_1c

    .line 762651
    const-string v3, "primary_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762652
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762653
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762654
    if-eqz v2, :cond_1d

    .line 762655
    const-string v3, "privacy_checkbox_error"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762656
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762657
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762658
    if-eqz v2, :cond_1e

    .line 762659
    const-string v3, "privacy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762660
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762661
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762662
    if-eqz v2, :cond_1f

    .line 762663
    const-string v3, "progress_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762664
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762665
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762666
    if-eqz v2, :cond_20

    .line 762667
    const-string v3, "secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762668
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762669
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762670
    if-eqz v2, :cond_21

    .line 762671
    const-string v3, "select_text_hint"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762672
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762673
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762674
    if-eqz v2, :cond_22

    .line 762675
    const-string v3, "send_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762676
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762677
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762678
    if-eqz v2, :cond_23

    .line 762679
    const-string v3, "sent_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762680
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762681
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762682
    if-eqz v2, :cond_24

    .line 762683
    const-string v3, "share_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762684
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762685
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762686
    if-eqz v2, :cond_25

    .line 762687
    const-string v3, "short_secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762688
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762689
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 762690
    if-eqz v2, :cond_26

    .line 762691
    const-string v3, "skip_experiments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762692
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 762693
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762694
    if-eqz v2, :cond_27

    .line 762695
    const-string v3, "split_flow_landing_page_hint_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762696
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762697
    :cond_27
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762698
    if-eqz v2, :cond_28

    .line 762699
    const-string v3, "split_flow_landing_page_hint_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762700
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762701
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762702
    if-eqz v2, :cond_29

    .line 762703
    const-string v3, "submit_card_instruction_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762704
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762705
    :cond_29
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762706
    if-eqz v2, :cond_2a

    .line 762707
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762708
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762709
    :cond_2a
    const/16 v2, 0x2b

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762710
    if-eqz v2, :cond_2b

    .line 762711
    const-string v3, "unsubscribe_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762712
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762713
    :cond_2b
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762714
    if-eqz v2, :cond_2c

    .line 762715
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762716
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762717
    :cond_2c
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 762718
    if-eqz v2, :cond_2d

    .line 762719
    const-string v3, "country_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 762720
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 762721
    :cond_2d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 762722
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 762533
    check-cast p1, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLLeadGenActionLink$Serializer;->a(Lcom/facebook/graphql/model/GraphQLLeadGenActionLink;LX/0nX;LX/0my;)V

    return-void
.end method
