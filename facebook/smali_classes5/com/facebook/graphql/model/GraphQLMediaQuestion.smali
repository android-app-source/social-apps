.class public final Lcom/facebook/graphql/model/GraphQLMediaQuestion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaQuestion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaQuestion$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765586
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaQuestion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 765592
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaQuestion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765590
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 765591
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765587
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765588
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->e:Ljava/lang/String;

    .line 765589
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    .line 765543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 765583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->g:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->g:Ljava/util/List;

    .line 765585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765580
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765581
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->h:Ljava/lang/String;

    .line 765582
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765593
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 765594
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->i:Ljava/lang/String;

    .line 765595
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 765577
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 765578
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 765579
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->j:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 765562
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765563
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 765564
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->k()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 765565
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->l()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 765566
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 765567
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 765568
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 765569
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 765570
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 765571
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 765572
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 765573
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 765574
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 765575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765576
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 765549
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765550
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->k()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 765551
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->k()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    .line 765552
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->k()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 765553
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMediaQuestion;

    .line 765554
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->f:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    .line 765555
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 765556
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 765557
    if-eqz v2, :cond_1

    .line 765558
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;

    .line 765559
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->g:Ljava/util/List;

    move-object v1, v0

    .line 765560
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 765561
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 765548
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 765545
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 765546
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMediaQuestion;->j:Z

    .line 765547
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 765544
    const v0, 0xe3f1bca

    return v0
.end method
