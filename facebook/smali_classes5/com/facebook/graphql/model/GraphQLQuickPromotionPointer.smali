.class public final Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777782
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777730
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 777780
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 777781
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->e:Ljava/lang/String;

    .line 777779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 777771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->g:Ljava/util/List;

    .line 777773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 777768
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->h:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777769
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->h:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->h:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 777770
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->h:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777765
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777766
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->i:Ljava/lang/String;

    .line 777767
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 777762
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777763
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 777764
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 777746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777747
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 777748
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 777749
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->l()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 777750
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 777751
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 777752
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 777753
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 777754
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 777755
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->m()Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    if-ne v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 777756
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 777757
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->o()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 777758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 777760
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->m()Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v0

    goto :goto_0

    .line 777761
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->o()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 777733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777734
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 777735
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777736
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 777737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;

    .line 777738
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777739
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 777740
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 777741
    if-eqz v2, :cond_1

    .line 777742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;

    .line 777743
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->g:Ljava/util/List;

    move-object v1, v0

    .line 777744
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777745
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionPointer;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 777731
    const v0, 0x25742c07

    return v0
.end method
