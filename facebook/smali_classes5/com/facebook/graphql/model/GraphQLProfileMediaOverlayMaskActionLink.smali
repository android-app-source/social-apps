.class public final Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777038
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 777037
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 777035
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 777036
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777032
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777033
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 777034
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 777029
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 777030
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 777031
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->f:J

    return-wide v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777011
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777012
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777013
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 777028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 777025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->j:Ljava/lang/String;

    .line 777041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777020
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777021
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 777022
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777017
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777018
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l:Ljava/lang/String;

    .line 777019
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 777014
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777015
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m:Ljava/lang/String;

    .line 777016
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 777008
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->n:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 777009
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->n:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->n:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 777010
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->n:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 776985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 776986
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 776987
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 776988
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 776989
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 776990
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 776991
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 776992
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 776993
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 776994
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 776995
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 776996
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 776997
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 776998
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 776999
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 777000
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 777001
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 777002
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 777003
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 777004
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->r()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 777005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 777006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 777007
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->r()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 776957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 776958
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 776959
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 776960
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 776961
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    .line 776962
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 776963
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 776964
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 776965
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 776966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    .line 776967
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 776968
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 776969
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 776970
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->l()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 776971
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    .line 776972
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->h:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 776973
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 776974
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 776975
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 776976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    .line 776977
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 776978
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 776979
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 776980
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 776981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;

    .line 776982
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 776983
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 776984
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 776954
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 776955
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMaskActionLink;->f:J

    .line 776956
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 776953
    const v0, 0x352bf2b1

    return v0
.end method
