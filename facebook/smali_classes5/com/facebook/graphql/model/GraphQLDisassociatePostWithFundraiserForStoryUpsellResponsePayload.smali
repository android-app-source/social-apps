.class public final Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 747387
    const-class v0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 747388
    const-class v0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 747389
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 747390
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->e:Ljava/lang/String;

    .line 747393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 747394
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 747395
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 747396
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 747397
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 747398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 747399
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 747400
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 747401
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 747402
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 747403
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 747404
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 747405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 747406
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 747407
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 747408
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 747409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;

    .line 747410
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDisassociatePostWithFundraiserForStoryUpsellResponsePayload;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 747411
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 747412
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 747413
    const v0, 0x46974d53

    return v0
.end method
