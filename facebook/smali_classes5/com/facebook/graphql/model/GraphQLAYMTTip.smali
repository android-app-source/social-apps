.class public final Lcom/facebook/graphql/model/GraphQLAYMTTip;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTTip$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTTip$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743315
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTTip$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 743316
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTTip$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 743317
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 743318
    return-void
.end method

.method private r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743319
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 743320
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 743321
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k:Z

    return v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743322
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743323
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m:Ljava/lang/String;

    .line 743324
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n:Ljava/lang/String;

    .line 743327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743328
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743329
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 743330
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743331
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743332
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q:Ljava/lang/String;

    .line 743333
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743334
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743335
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->r:Ljava/lang/String;

    .line 743336
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->r:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 743271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 743273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 743274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 743275
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 743276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 743277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 743278
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 743279
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->t()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 743280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 743281
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->u()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 743282
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->v()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 743283
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->w()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 743284
    const/16 v12, 0xe

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 743285
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 743286
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 743287
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 743288
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 743289
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 743290
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 743291
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->r()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 743292
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 743293
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 743294
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 743295
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 743296
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 743297
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 743298
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 743299
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743300
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 743301
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 743302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 743303
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 743304
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 743305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 743306
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    .line 743307
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTTip;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 743308
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->u()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 743309
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->u()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 743310
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->u()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 743311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    .line 743312
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    .line 743313
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 743314
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 743267
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 743268
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k:Z

    .line 743269
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 743266
    const v0, 0xab8e43c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->e:Ljava/lang/String;

    .line 743265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->f:Ljava/lang/String;

    .line 743262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743257
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743258
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->g:Ljava/lang/String;

    .line 743259
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743255
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->h:Ljava/lang/String;

    .line 743256
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 743253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743248
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743249
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j:Ljava/lang/String;

    .line 743250
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 743245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 743247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 743242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 743243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o:Ljava/lang/String;

    .line 743244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o:Ljava/lang/String;

    return-object v0
.end method
