.class public final Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 743896
    const-class v0, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 743897
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 743898
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 743899
    const/16 v0, 0x194

    .line 743900
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 743901
    new-instance v3, LX/186;

    const/16 v2, 0x80

    invoke-direct {v3, v2}, LX/186;-><init>(I)V

    .line 743902
    const/4 v4, 0x0

    .line 743903
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_7

    .line 743904
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 743905
    :goto_0
    move v2, v4

    .line 743906
    if-eqz v1, :cond_0

    .line 743907
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/186;->c(I)V

    .line 743908
    invoke-virtual {v3, v5, v0, v5}, LX/186;->a(ISI)V

    .line 743909
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 743910
    invoke-virtual {v3}, LX/186;->d()I

    move-result v2

    .line 743911
    :cond_0
    invoke-virtual {v3, v2}, LX/186;->d(I)V

    .line 743912
    invoke-static {v3}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v2

    move-object v1, v2

    .line 743913
    move-object v2, v1

    .line 743914
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLAdditionalSuggestedPostAdItemsConnection;-><init>()V

    .line 743915
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 743916
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 743917
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 743918
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 743919
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 743920
    :cond_1
    return-object v1

    .line 743921
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 743922
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_6

    .line 743923
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 743924
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 743925
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_3

    if-eqz v7, :cond_3

    .line 743926
    const-string p0, "edges"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 743927
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 743928
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, p0, :cond_4

    .line 743929
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, p0, :cond_4

    .line 743930
    invoke-static {p1, v3}, LX/4Kj;->b(LX/15w;LX/186;)I

    move-result v7

    .line 743931
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 743932
    :cond_4
    invoke-static {v6, v3}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 743933
    goto :goto_1

    .line 743934
    :cond_5
    const-string p0, "page_info"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 743935
    invoke-static {p1, v3}, LX/264;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 743936
    :cond_6
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 743937
    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 743938
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, LX/186;->b(II)V

    .line 743939
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_7
    move v2, v4

    move v6, v4

    goto :goto_1
.end method
