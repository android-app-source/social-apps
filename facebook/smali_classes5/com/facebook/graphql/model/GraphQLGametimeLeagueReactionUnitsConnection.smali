.class public final Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754657
    const-class v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 754605
    const-class v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 754655
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 754656
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754652
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754653
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->e:Ljava/util/List;

    .line 754654
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754649
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754650
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 754651
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754646
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754647
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->g:Ljava/lang/String;

    .line 754648
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 754643
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 754644
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 754645
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->h:I

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 754640
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 754641
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754642
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 754627
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754628
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 754629
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 754630
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 754631
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 754632
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 754633
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 754634
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 754635
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 754636
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 754637
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 754638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754639
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 754609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 754610
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 754611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 754612
    if-eqz v1, :cond_3

    .line 754613
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;

    .line 754614
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->e:Ljava/util/List;

    move-object v1, v0

    .line 754615
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754616
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 754617
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 754618
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;

    .line 754619
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 754620
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 754621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 754623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;

    .line 754624
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 754625
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 754626
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 754606
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 754607
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGametimeLeagueReactionUnitsConnection;->h:I

    .line 754608
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 754604
    const v0, 0x4d67f176    # 2.4321008E8f

    return v0
.end method
