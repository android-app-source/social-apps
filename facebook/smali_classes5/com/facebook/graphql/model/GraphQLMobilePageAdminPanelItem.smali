.class public final Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            ">;"
        }
    .end annotation
.end field

.field public l:I

.field public m:D

.field public n:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766841
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 766842
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 766843
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 766844
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y:LX/0x2;

    .line 766845
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->g:Ljava/lang/String;

    .line 766848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p:Ljava/lang/String;

    .line 766851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p:Ljava/lang/String;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 766854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 766832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y:LX/0x2;

    if-nez v0, :cond_0

    .line 766833
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y:LX/0x2;

    .line 766834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 766855
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766856
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 766857
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 766858
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->A()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 766859
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 766860
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 766861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 766862
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 766863
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 766864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 766865
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->B()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 766866
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->c()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 766867
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 766868
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/util/List;)I

    move-result v14

    .line 766869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 766870
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 766871
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 766872
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y()LX/0Px;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v18

    .line 766873
    const/16 v19, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 766874
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 766875
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 766876
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 766877
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 766878
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 766879
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 766880
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 766881
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 766882
    const/16 v3, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 766883
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 766884
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 766885
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 766886
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 766887
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 766888
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 766889
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 766890
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 766891
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 766892
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 766893
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 766894
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766895
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 766896
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 766897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 766898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 766899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 766900
    if-eqz v1, :cond_a

    .line 766901
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766902
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s:Ljava/util/List;

    move-object v1, v0

    .line 766903
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 766904
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 766905
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 766906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766907
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 766908
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 766909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 766911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766912
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766913
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 766914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766915
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 766916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766917
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766918
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 766919
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 766920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 766921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766922
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 766923
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 766924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 766925
    if-eqz v2, :cond_4

    .line 766926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766927
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x:Ljava/util/List;

    move-object v1, v0

    .line 766928
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 766929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 766931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766932
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766933
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 766934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 766935
    if-eqz v2, :cond_6

    .line 766936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766937
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k:Ljava/util/List;

    move-object v1, v0

    .line 766938
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 766939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 766940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 766941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766942
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n:Lcom/facebook/graphql/model/GraphQLPage;

    .line 766943
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 766944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 766946
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 766947
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766948
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 766949
    if-nez v1, :cond_9

    :goto_1
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_1

    :cond_a
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766950
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 766951
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 766952
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 766953
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 766954
    const/4 v0, 0x0

    .line 766955
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 766956
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 766957
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l:I

    .line 766958
    const/16 v0, 0x8

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m:D

    .line 766959
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r:I

    .line 766960
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q:Ljava/lang/String;

    .line 766963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 766964
    const v0, -0x5ad5ee87

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766835
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766836
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 766837
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->h:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766838
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766839
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766840
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766811
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766812
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766813
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k:Ljava/util/List;

    .line 766795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766796
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766797
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766798
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l:I

    return v0
.end method

.method public final p()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766799
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766800
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766801
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m:D

    return-wide v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766802
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766803
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n:Lcom/facebook/graphql/model/GraphQLPage;

    .line 766804
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766805
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766806
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 766807
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 766808
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 766809
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 766810
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r:I

    return v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766814
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766815
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s:Ljava/util/List;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s:Ljava/util/List;

    .line 766816
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766817
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766818
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t:Ljava/util/List;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t:Ljava/util/List;

    .line 766819
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766820
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766821
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u:Ljava/lang/String;

    .line 766822
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766823
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766824
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v:Ljava/lang/String;

    .line 766825
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 766826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 766828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    return-object v0
.end method

.method public final y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 766830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x:Ljava/util/List;

    .line 766831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
