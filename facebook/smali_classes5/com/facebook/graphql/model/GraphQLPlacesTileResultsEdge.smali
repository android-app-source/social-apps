.class public final Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:D

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775523
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 775522
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 775520
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 775521
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775500
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775501
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775502
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 775509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 775511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 775512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 775513
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 775514
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 775515
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 775516
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->k()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 775517
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 775518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775519
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 775524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 775525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 775527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 775528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 775529
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775530
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 775531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 775532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 775533
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 775534
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 775535
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 775536
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775537
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 775538
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 775539
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 775540
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 775541
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775506
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->e:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775507
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->e:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 775508
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->e:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 775503
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 775504
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->g:D

    .line 775505
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 775499
    const v0, -0x31a9b2c7

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775496
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 775497
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 775498
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->f:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    return-object v0
.end method

.method public final k()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 775493
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 775494
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 775495
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->g:D

    return-wide v0
.end method
