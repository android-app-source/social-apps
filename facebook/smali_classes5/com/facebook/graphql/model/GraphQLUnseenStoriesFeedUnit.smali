.class public final Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 789055
    const-class v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 789056
    const-class v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 789057
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 789058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4064a59f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 789059
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->p:LX/0x2;

    .line 789060
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789061
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789062
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->i:Ljava/lang/String;

    .line 789063
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789064
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789065
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k:Ljava/lang/String;

    .line 789066
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789067
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789068
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789069
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789070
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789071
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789072
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789073
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789074
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->o:Ljava/lang/String;

    .line 789075
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 789076
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789077
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789078
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g:Ljava/lang/String;

    .line 789079
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 789080
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 789081
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 789082
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 789083
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 789052
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->p:LX/0x2;

    if-nez v0, :cond_0

    .line 789053
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->p:LX/0x2;

    .line 789054
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->p:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 789084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 789085
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 788987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788988
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 788989
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 788990
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 788991
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 788992
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 788993
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 788994
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 788995
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 788996
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 788997
    const/16 v2, 0xb

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 788998
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 788999
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 789000
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 789001
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 789002
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 789003
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 789004
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 789005
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 789006
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 789007
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 789008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 789009
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 789010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 789011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 789012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 789013
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 789014
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;

    .line 789015
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLActor;

    .line 789016
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 789017
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789018
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 789019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;

    .line 789020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789021
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 789022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789023
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 789024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;

    .line 789025
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 789026
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 789027
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789028
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 789029
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->h:J

    .line 789030
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 789031
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 789032
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->h:J

    .line 789033
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789034
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->n:Ljava/lang/String;

    .line 789037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 789038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 789039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 789040
    :goto_0
    return-object v0

    .line 789041
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 789042
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 789043
    const v0, -0x4064a59f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789044
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789045
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->f:Ljava/lang/String;

    .line 789046
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 789047
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 789048
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLActor;

    .line 789049
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 789050
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 789051
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
