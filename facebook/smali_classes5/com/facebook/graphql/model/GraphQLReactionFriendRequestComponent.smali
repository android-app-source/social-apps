.class public final Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778168
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 778167
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 778165
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 778166
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->e:Ljava/lang/String;

    .line 778164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 778159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 778161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->g:Ljava/lang/String;

    .line 778158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 778132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 778133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 778134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 778144
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778145
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 778146
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 778147
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->l()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 778148
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 778149
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 778150
    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v3, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 778151
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 778152
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 778153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778154
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 778155
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->j()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 778136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 778137
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->l()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778138
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->l()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 778139
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->l()Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 778140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;

    .line 778141
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactionFriendRequestComponent;->h:Lcom/facebook/graphql/model/GraphQLReactionFriendingPossibility;

    .line 778142
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 778143
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 778135
    const v0, -0x30d9d46b

    return v0
.end method
