.class public final Lcom/facebook/graphql/model/GraphQLProductImage$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProductImage;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 776500
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductImage;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLProductImage$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLProductImage$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 776501
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 776502
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLProductImage;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 776503
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 776504
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/4Rh;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 776505
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 776506
    check-cast p1, Lcom/facebook/graphql/model/GraphQLProductImage;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/model/GraphQLProductImage$Serializer;->a(Lcom/facebook/graphql/model/GraphQLProductImage;LX/0nX;LX/0my;)V

    return-void
.end method
