.class public final Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782288
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 782287
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782285
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 782286
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 782270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 782272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 782273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 782274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 782275
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 782276
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 782277
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 782278
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 782279
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 782280
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 782281
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 782282
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->o()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 782283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782284
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 782267
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 782268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 782269
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782264
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782265
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->e:Ljava/lang/String;

    .line 782266
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 782289
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 782290
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->h:I

    .line 782291
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->i:Z

    .line 782292
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->k:I

    .line 782293
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 782263
    const v0, 0x4a5e2c50    # 3640084.0f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->f:Ljava/lang/String;

    .line 782247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->g:Ljava/lang/String;

    .line 782262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782257
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782258
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782259
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->h:I

    return v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782254
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782255
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782256
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->i:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 782251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 782252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j:Ljava/lang/String;

    .line 782253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 782248
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 782249
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 782250
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->k:I

    return v0
.end method
