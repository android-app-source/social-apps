.class public final Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEntityCardContextItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748197
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 748196
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 748194
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 748195
    return-void
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748191
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748192
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 748193
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->i:Ljava/lang/String;

    .line 748190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->i:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j:Ljava/lang/String;

    .line 748158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 748198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 748200
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->n()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 748201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 748202
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 748203
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 748204
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 748205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 748206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 748207
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 748208
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 748209
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 748210
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 748211
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 748212
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 748213
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 748214
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 748215
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 748216
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 748217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748218
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 748219
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 748162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->n()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 748164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->n()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 748165
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->n()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 748166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748167
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 748168
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 748169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 748170
    if-eqz v2, :cond_1

    .line 748171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748172
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->g:Ljava/util/List;

    move-object v1, v0

    .line 748173
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 748174
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748175
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 748176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748177
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748178
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 748179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 748181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 748182
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748183
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 748184
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->e:Ljava/lang/String;

    .line 748161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 748155
    const v0, -0x68d02651

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 748152
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748153
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->g:Ljava/util/List;

    .line 748154
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 748149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->h:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->h:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->h:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 748151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->h:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 748144
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m:Ljava/lang/String;

    .line 748145
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m:Ljava/lang/String;

    return-object v0
.end method
