.class public final Lcom/facebook/graphql/executor/GraphQLQueryExecutor$2;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0

    .prologue
    .line 741728
    iput-object p1, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$2;->a:LX/0tX;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 741729
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$2;->a:LX/0tX;

    iget-object v0, v0, LX/0tX;->m:LX/0Sj;

    const-string v1, "GraphQLQueryExecutor"

    const-string v2, "waitForQueueDrain"

    invoke-interface {v0, v1, v2}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v1

    .line 741730
    if-eqz v1, :cond_0

    .line 741731
    invoke-interface {v1}, LX/0cV;->a()V

    .line 741732
    :cond_0
    :try_start_0
    sget-object v0, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741733
    if-eqz v1, :cond_1

    .line 741734
    invoke-interface {v1}, LX/0cV;->b()V

    .line 741735
    :cond_1
    return-void

    .line 741736
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 741737
    invoke-interface {v1}, LX/0cV;->b()V

    :cond_2
    throw v0
.end method
