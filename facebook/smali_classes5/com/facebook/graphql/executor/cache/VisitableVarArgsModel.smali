.class public Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements LX/0jT;


# instance fields
.field private final a:LX/15i;

.field public final b:LX/0w5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/16a;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0w5;LX/15i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;",
            "LX/15i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 742400
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0w5;

    const/4 v2, 0x0

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15i;

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;-><init>(LX/0w5;LX/16a;LX/15i;)V

    .line 742401
    return-void
.end method

.method public constructor <init>(LX/0w5;LX/16a;LX/15i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;",
            "LX/16a;",
            "LX/15i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 742395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742396
    iput-object p1, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->b:LX/0w5;

    .line 742397
    iput-object p2, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->c:LX/16a;

    .line 742398
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15i;

    iput-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    .line 742399
    return-void
.end method

.method public constructor <init>(LX/16a;LX/15i;)V
    .locals 3

    .prologue
    .line 742393
    const/4 v2, 0x0

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16a;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15i;

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;-><init>(LX/0w5;LX/16a;LX/15i;)V

    .line 742394
    return-void
.end method

.method public static a(LX/186;LX/16a;LX/0Px;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">(",
            "LX/186;",
            "LX/16a;",
            "LX/0Px",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 742388
    if-nez p1, :cond_0

    invoke-virtual {p0, p2, v2}, LX/186;->a(Ljava/util/List;Z)I

    move-result v0

    .line 742389
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 742390
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 742391
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    return v0

    .line 742392
    :cond_0
    invoke-virtual {p0, p2, p1, v2}, LX/186;->a(Ljava/util/List;LX/16a;Z)I

    move-result v0

    goto :goto_0
.end method

.method private g()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 742374
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    if-nez v0, :cond_0

    .line 742375
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 742376
    :goto_0
    return-object v0

    .line 742377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 742378
    if-nez v0, :cond_1

    .line 742379
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 742380
    goto :goto_0

    .line 742381
    :cond_1
    iget-object v1, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->c:LX/16a;

    if-eqz v1, :cond_2

    .line 742382
    iget-object v1, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    iget-object v2, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->c:LX/16a;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->b(IILX/16a;)Ljava/util/Iterator;

    move-result-object v0

    .line 742383
    :goto_1
    if-nez v0, :cond_3

    .line 742384
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 742385
    goto :goto_0

    .line 742386
    :cond_2
    iget-object v1, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->b:LX/0w5;

    iget-object v2, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    invoke-virtual {v1, v2, v0, v3}, LX/0w5;->a(LX/15i;II)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_1

    .line 742387
    :cond_3
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 742373
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->c:LX/16a;

    invoke-direct {p0}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->g()LX/0Px;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a(LX/186;LX/16a;LX/0Px;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 742353
    invoke-direct {p0}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->g()LX/0Px;

    move-result-object v5

    .line 742354
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 742355
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v0

    move v2, v0

    :goto_0
    if-ge v4, v7, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 742356
    instance-of v1, v0, LX/0jT;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 742357
    check-cast v1, LX/0jT;

    invoke-interface {p1, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v1

    .line 742358
    if-nez v1, :cond_0

    move v2, v3

    .line 742359
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 742360
    :cond_0
    if-eq v1, v0, :cond_1

    move v2, v3

    :cond_1
    move-object v0, v1

    .line 742361
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 742362
    :cond_2
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 742363
    :cond_3
    if-eqz v2, :cond_4

    .line 742364
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->b:LX/0w5;

    iget-object v1, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->c:LX/16a;

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    const/4 v10, 0x0

    .line 742365
    new-instance v8, LX/186;

    const/16 v9, 0x80

    invoke-direct {v8, v9}, LX/186;-><init>(I)V

    .line 742366
    invoke-static {v8, v1, v2}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a(LX/186;LX/16a;LX/0Px;)I

    move-result v9

    .line 742367
    invoke-virtual {v8, v9}, LX/186;->d(I)V

    .line 742368
    invoke-virtual {v8}, LX/186;->e()[B

    move-result-object v9

    .line 742369
    new-instance v8, LX/15i;

    invoke-static {v9}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    const/4 v12, 0x1

    move-object v11, v10

    move-object v13, v10

    invoke-direct/range {v8 .. v13}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 742370
    const-string v9, "VisitableVarArgsModel.forRows"

    invoke-virtual {v8, v9}, LX/15i;->a(Ljava/lang/String;)V

    .line 742371
    new-instance v9, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;

    invoke-direct {v9, v0, v1, v8}, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;-><init>(LX/0w5;LX/16a;LX/15i;)V

    move-object p0, v9

    .line 742372
    :cond_4
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 742346
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 742352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 742351
    const/4 v0, 0x0

    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 742350
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 742349
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/VisitableVarArgsModel;->a:LX/15i;

    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 742348
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 742347
    const/4 v0, 0x0

    return v0
.end method
