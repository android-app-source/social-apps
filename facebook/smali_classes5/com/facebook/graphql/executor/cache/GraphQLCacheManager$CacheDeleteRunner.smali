.class public final Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1mR;


# direct methods
.method public constructor <init>(LX/1mR;)V
    .locals 0

    .prologue
    .line 742201
    iput-object p1, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 742202
    iget-object v0, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    iget-object v1, v0, LX/1mR;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 742203
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    iget-object v2, v2, LX/1mR;->d:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 742204
    iget-object v2, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    iget-object v2, v2, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 742205
    iget-object v3, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    const/4 v4, 0x0

    .line 742206
    iput-object v4, v3, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 742207
    iget-object v3, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    iget-object v3, v3, LX/1mR;->d:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 742208
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742209
    iget-object v1, p0, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;->a:LX/1mR;

    .line 742210
    iget-object v3, v1, LX/1mR;->a:LX/0si;

    invoke-interface {v3, v0}, LX/0si;->a(Ljava/util/Set;)V

    .line 742211
    const v0, -0x6bfc3a28

    invoke-static {v2, v5, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 742212
    return-void

    .line 742213
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
