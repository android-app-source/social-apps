.class public final Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0zO;

.field public final synthetic b:LX/4Ve;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/1Mz;


# direct methods
.method public constructor <init>(LX/1Mz;LX/0zO;LX/4Ve;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 742659
    iput-object p1, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->d:LX/1Mz;

    iput-object p2, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->a:LX/0zO;

    iput-object p3, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->b:LX/4Ve;

    iput-object p4, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 742660
    iget-object v0, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->a:LX/0zO;

    .line 742661
    iget v1, v0, LX/0zO;->v:I

    move v1, v1

    .line 742662
    iget-object v0, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->d:LX/1Mz;

    iget-object v0, v0, LX/1Mz;->b:LX/0tc;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0tc;->a(Z)LX/1NB;

    move-result-object v2

    .line 742663
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->b:LX/4Ve;

    .line 742664
    iget-object v3, v0, LX/4Ve;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v0, v3

    .line 742665
    invoke-virtual {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 742666
    instance-of v4, v0, LX/0jT;

    if-eqz v4, :cond_0

    .line 742667
    iget-object v4, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->d:LX/1Mz;

    iget-object v4, v4, LX/1Mz;->g:LX/0ti;

    check-cast v0, LX/0jT;

    invoke-virtual {v4, v0}, LX/0ti;->a(LX/0jT;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 742668
    :catch_0
    move-exception v0

    .line 742669
    :try_start_1
    iget-object v3, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v3, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742670
    invoke-virtual {v2, v1}, LX/1NB;->a(I)V

    .line 742671
    :goto_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->d:LX/1Mz;

    iget-object v0, v0, LX/1Mz;->a:LX/0t5;

    iget-object v2, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->b:LX/4Ve;

    .line 742672
    iget-object v3, v2, LX/4Ve;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v2, v3

    .line 742673
    invoke-virtual {v2}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0t5;->a(ILjava/util/Set;)V

    .line 742674
    return-void

    .line 742675
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->b:LX/4Ve;

    .line 742676
    iget-object v3, v0, LX/4Ve;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v0, v3

    .line 742677
    invoke-virtual {v2, v0}, LX/1NB;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 742678
    iget-object v3, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v4, LX/4Ve;

    iget-object v5, p0, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;->b:LX/4Ve;

    .line 742679
    iget-object v6, v5, LX/4Ve;->b:Lcom/facebook/graphql/executor/live/ResponseMetadata;

    move-object v5, v6

    .line 742680
    invoke-direct {v4, v0, v5}, LX/4Ve;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/graphql/executor/live/ResponseMetadata;)V

    const v0, 0x7cfd92a5

    invoke-static {v3, v4, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 742681
    invoke-virtual {v2, v1}, LX/1NB;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2, v1}, LX/1NB;->a(I)V

    throw v0
.end method
