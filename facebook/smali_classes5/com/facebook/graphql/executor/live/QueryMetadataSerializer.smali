.class public Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/executor/live/QueryMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 742717
    const-class v0, Lcom/facebook/graphql/executor/live/QueryMetadata;

    new-instance v1, Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 742718
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 742719
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/executor/live/QueryMetadata;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 742711
    if-nez p0, :cond_0

    .line 742712
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 742713
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 742714
    invoke-static {p0, p1, p2}, Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;->b(Lcom/facebook/graphql/executor/live/QueryMetadata;LX/0nX;LX/0my;)V

    .line 742715
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 742716
    return-void
.end method

.method private static b(Lcom/facebook/graphql/executor/live/QueryMetadata;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 742707
    const-string v0, "config_id"

    iget-object v1, p0, Lcom/facebook/graphql/executor/live/QueryMetadata;->configId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 742708
    const-string v0, "long_lived"

    iget-boolean v1, p0, Lcom/facebook/graphql/executor/live/QueryMetadata;->longLived:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 742709
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 742710
    check-cast p1, Lcom/facebook/graphql/executor/live/QueryMetadata;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;->a(Lcom/facebook/graphql/executor/live/QueryMetadata;LX/0nX;LX/0my;)V

    return-void
.end method
