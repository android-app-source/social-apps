.class public Lcom/facebook/graphql/executor/live/QueryMetadata;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final configId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "config_id"
    .end annotation
.end field

.field public final longLived:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "long_lived"
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 742702
    const-class v0, Lcom/facebook/graphql/executor/live/QueryMetadataSerializer;

    return-object v0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 742703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742704
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/executor/live/QueryMetadata;->configId:Ljava/lang/String;

    .line 742705
    iput-boolean p2, p0, Lcom/facebook/graphql/executor/live/QueryMetadata;->longLived:Z

    .line 742706
    return-void
.end method
