.class public final Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic b:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

.field public final synthetic c:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;)V
    .locals 0

    .prologue
    .line 741738
    iput-object p1, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->c:LX/0tX;

    iput-object p2, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p3, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->b:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 741739
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->c:LX/0tX;

    iget-object v0, v0, LX/0tX;->h:LX/0SI;

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v0, v1}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 741740
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->b:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    invoke-virtual {v0}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741741
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->c:LX/0tX;

    iget-object v0, v0, LX/0tX;->h:LX/0SI;

    invoke-interface {v0}, LX/0SI;->f()V

    .line 741742
    return-void

    .line 741743
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;->c:LX/0tX;

    iget-object v1, v1, LX/0tX;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    throw v0
.end method
