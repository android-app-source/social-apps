.class public Lcom/facebook/graphql/executor/GraphQLBatchRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tc;

.field private final d:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final e:LX/11E;

.field private final f:LX/11H;

.field private final g:LX/0sf;

.field public final h:LX/0v6;

.field public i:LX/3U2;

.field public final j:LX/03V;

.field private final k:LX/0t2;

.field private final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0tA;

.field private final n:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final o:LX/1kv;

.field private final p:I

.field public q:I

.field public r:I

.field public s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0zO;",
            "LX/3U1;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/11X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 585264
    const-class v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    sput-object v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/locks/ReadWriteLock;LX/0v6;LX/0Ot;LX/0tc;LX/11E;LX/11H;LX/0sf;LX/03V;LX/0t2;Ljava/util/Set;LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kv;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/locks/ReadWriteLock;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0v6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/locks/ReadWriteLock;",
            "LX/0v6;",
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;",
            "LX/0tc;",
            "LX/11E;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0sf;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0t2;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "LX/0tA;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1kv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585005
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    .line 585006
    new-instance v0, LX/3Ty;

    invoke-direct {v0, p0}, LX/3Ty;-><init>(Lcom/facebook/graphql/executor/GraphQLBatchRunner;)V

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->t:LX/11X;

    .line 585007
    iput-object p3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->b:LX/0Ot;

    .line 585008
    iput-object p4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->c:LX/0tc;

    .line 585009
    iput-object p1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 585010
    iput-object p2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585011
    iput-object p5, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->e:LX/11E;

    .line 585012
    iput-object p6, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->f:LX/11H;

    .line 585013
    iput-object p7, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->g:LX/0sf;

    .line 585014
    iput-object p8, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->j:LX/03V;

    .line 585015
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->t:LX/11X;

    .line 585016
    iput-object v1, v0, LX/0v6;->e:LX/11X;

    .line 585017
    iput-object p9, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->k:LX/0t2;

    .line 585018
    iput-object p10, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->l:Ljava/util/Set;

    .line 585019
    iput-object p11, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->m:LX/0tA;

    .line 585020
    iget v0, p2, LX/0v6;->n:I

    move v0, v0

    .line 585021
    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    .line 585022
    iput-object p12, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 585023
    iput-object p13, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->o:LX/1kv;

    .line 585024
    return-void
.end method

.method private a()I
    .locals 8

    .prologue
    const/16 v7, 0x17

    const v6, 0x310027    # 4.499994E-39f

    .line 585254
    :try_start_0
    invoke-static {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->b(Lcom/facebook/graphql/executor/GraphQLBatchRunner;)I

    move-result v0

    .line 585255
    const-string v1, "num_cache_hits"

    invoke-static {p0, v1, v0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a(Lcom/facebook/graphql/executor/GraphQLBatchRunner;Ljava/lang/String;I)V

    .line 585256
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const-string v4, "local_db_read"

    const-string v5, "success"

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585257
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v1, v6, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    :goto_0
    return v0

    .line 585258
    :catch_0
    move-exception v0

    .line 585259
    :try_start_1
    const-string v1, "local_db_read"

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310027    # 4.499994E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v1, v2, v3, v4}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 585260
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->j:LX/03V;

    const-string v2, "GraphQLBatchRunner.satisfyFromCache"

    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585261
    iget-object v4, v3, LX/0v6;->c:Ljava/lang/String;

    move-object v3, v4

    .line 585262
    const/16 v4, 0x7d0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585263
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v6, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v1, v6, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0
.end method

.method private a(LX/2lk;)V
    .locals 8

    .prologue
    const/16 v7, 0x1e

    const v6, 0x310027    # 4.499994E-39f

    .line 585226
    const-string v0, "GraphQLBatchRunner.updateDBForBatch"

    const v1, -0x8b0a431

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 585227
    const/4 v2, 0x0

    .line 585228
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->d()V

    .line 585229
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 585230
    invoke-interface {p1}, LX/2lk;->b()V

    .line 585231
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const-string v4, "has_consistent_fields"

    invoke-interface {v0, v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 585232
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const/16 v4, 0x13

    invoke-interface {v0, v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 585233
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v0, v0, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585234
    iget-object v1, v0, LX/0zO;->a:LX/0zS;

    move-object v1, v1

    .line 585235
    iget-boolean v1, v1, LX/0zS;->h:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v1, v0}, LX/0v6;->d(LX/0zO;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 585236
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3U1;

    iget-object v1, v1, LX/3U1;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585237
    add-int/lit8 v3, v2, 0x1

    .line 585238
    :try_start_1
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3U1;

    iget-object v2, v2, LX/3U1;->a:LX/1kt;

    invoke-interface {v2, v1}, LX/1kt;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v2, v3

    .line 585239
    goto :goto_1

    :cond_1
    move v0, v2

    move v2, v0

    .line 585240
    goto :goto_0

    .line 585241
    :cond_2
    :try_start_2
    const-string v0, "local_db_write"

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310027    # 4.499994E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v0, v1, v3, v4}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 585242
    const-string v0, "db_write_count"

    invoke-static {p0, v0, v2}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a(Lcom/facebook/graphql/executor/GraphQLBatchRunner;Ljava/lang/String;I)V

    .line 585243
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v6, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 585244
    const v0, 0x416b1ab7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 585245
    return-void

    .line 585246
    :catch_0
    move-exception v0

    .line 585247
    :goto_2
    :try_start_3
    const-string v1, "local_db_write"

    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x310027    # 4.499994E-39f

    iget v5, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v1, v3, v4, v5}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 585248
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 585249
    :catchall_0
    move-exception v0

    :goto_3
    const-string v1, "db_write_count"

    invoke-static {p0, v1, v2}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a(Lcom/facebook/graphql/executor/GraphQLBatchRunner;Ljava/lang/String;I)V

    .line 585250
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v1, v6, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 585251
    const v1, 0x65697960

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 585252
    :catchall_1
    move-exception v0

    move v2, v3

    goto :goto_3

    .line 585253
    :catch_1
    move-exception v0

    move v2, v3

    goto :goto_2
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLBatchRunner;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const v3, 0x310027    # 4.499994E-39f

    .line 585223
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585224
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 585225
    :cond_0
    return-void
.end method

.method private static b(Lcom/facebook/graphql/executor/GraphQLBatchRunner;)I
    .locals 15

    .prologue
    const/4 v1, 0x0

    .line 585122
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    sget-object v2, LX/1NE;->DB_CACHE:LX/1NE;

    invoke-virtual {v0, v2}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    .line 585123
    :goto_0
    if-eqz v0, :cond_0

    .line 585124
    invoke-virtual {v0}, LX/1NB;->f()V

    .line 585125
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    sget-object v2, LX/1NE;->DB_CACHE:LX/1NE;

    invoke-virtual {v0, v2}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    goto :goto_0

    .line 585126
    :cond_0
    const/4 v3, 0x0

    .line 585127
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585128
    iget-object v2, v0, LX/0v6;->b:Ljava/util/List;

    move-object v0, v2

    .line 585129
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 585130
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 585131
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585132
    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 585133
    iget-object v2, v0, LX/0zO;->a:LX/0zS;

    move-object v2, v2

    .line 585134
    iget-boolean v2, v2, LX/0zS;->g:Z

    if-eqz v2, :cond_3

    .line 585135
    const/4 v14, 0x0

    const/4 v10, 0x0

    .line 585136
    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v2

    if-nez v2, :cond_f

    .line 585137
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 585138
    :cond_2
    :goto_2
    move-object v2, v10

    .line 585139
    const/4 v9, 0x0

    .line 585140
    if-nez v2, :cond_15

    move-object v7, v9

    .line 585141
    :goto_3
    move-object v2, v7

    .line 585142
    :goto_4
    if-eqz v2, :cond_4

    .line 585143
    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v2, v3

    .line 585144
    goto :goto_4

    .line 585145
    :cond_4
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585146
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v8

    .line 585147
    invoke-virtual {v8, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 585148
    new-instance v9, Ljava/util/Stack;

    invoke-direct {v9}, Ljava/util/Stack;-><init>()V

    .line 585149
    invoke-virtual {v9, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 585150
    :cond_5
    invoke-virtual {v9}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    .line 585151
    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0zO;

    .line 585152
    iget-object v10, v2, LX/0v6;->o:LX/0vV;

    invoke-virtual {v10, v7}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    .line 585153
    if-eqz v7, :cond_5

    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 585154
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0zO;

    .line 585155
    invoke-virtual {v8, v7}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 585156
    invoke-virtual {v8, v7}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 585157
    invoke-virtual {v9, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 585158
    :cond_7
    move-object v0, v8

    .line 585159
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585160
    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 585161
    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 585162
    :cond_8
    move-object v2, v4

    .line 585163
    sget-object v3, LX/1NE;->DB_CACHE:LX/1NE;

    .line 585164
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585165
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 585166
    iget-object v5, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3U1;

    iput-object v3, v0, LX/3U1;->b:LX/1NE;

    goto :goto_7

    .line 585167
    :cond_a
    move-object v2, v2

    .line 585168
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v3, v3, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_b

    const/4 v0, 0x1

    :goto_8
    const-string v3, "All requests should have an associated result list. (may be null for a cache miss)"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 585169
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 585170
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 585171
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 585172
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v2}, LX/0v6;->e()LX/11X;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0zO;

    invoke-interface {v6, v1, v2}, LX/11X;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/0zO;)V

    .line 585173
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0zO;

    .line 585174
    iget-object v6, v2, LX/0v6;->d:Ljava/util/Set;

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 585175
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 585176
    goto :goto_a

    :cond_b
    move v0, v1

    .line 585177
    goto :goto_8

    :cond_c
    move v1, v3

    :goto_b
    move v2, v1

    .line 585178
    goto :goto_9

    .line 585179
    :cond_d
    return v2

    :cond_e
    move v1, v2

    goto :goto_b

    .line 585180
    :cond_f
    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v2

    invoke-virtual {v2}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v2

    .line 585181
    if-eqz v2, :cond_10

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_11

    .line 585182
    :cond_10
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    goto/16 :goto_2

    .line 585183
    :cond_11
    new-instance v11, LX/0w7;

    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v7

    invoke-virtual {v7}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v7

    invoke-direct {v11, v7}, LX/0w7;-><init>(Ljava/util/Map;)V

    .line 585184
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move-object v9, v10

    :goto_c
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 585185
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 585186
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4a1;

    .line 585187
    iget-object v8, v2, LX/4a1;->a:LX/0zO;

    instance-of v8, v8, LX/0zO;

    if-eqz v8, :cond_2

    .line 585188
    iget-object v8, v2, LX/4a1;->a:LX/0zO;

    check-cast v8, LX/0zO;

    .line 585189
    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 585190
    if-eqz v8, :cond_2

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_2

    .line 585191
    invoke-interface {v8, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v8}, Lcom/facebook/graphql/executor/GraphQLResult;->a()Ljava/util/Map;

    move-result-object v8

    .line 585192
    iget-object v13, v2, LX/4a1;->b:Ljava/lang/String;

    invoke-interface {v8, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 585193
    if-eqz v8, :cond_2

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_2

    .line 585194
    sget-object v13, LX/4Ul;->a:[I

    iget-object v2, v2, LX/4a1;->c:LX/4Zz;

    invoke-virtual {v2}, LX/4Zz;->ordinal()I

    move-result v2

    aget v2, v13, v2

    packed-switch v2, :pswitch_data_0

    move-object v2, v9

    :goto_d
    move-object v9, v2

    .line 585195
    goto :goto_c

    .line 585196
    :pswitch_0
    invoke-interface {v8, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v11, v7, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    goto :goto_c

    .line 585197
    :pswitch_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v11, v7, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    goto :goto_c

    .line 585198
    :pswitch_2
    invoke-virtual {v11, v7, v8}, LX/0w7;->a(Ljava/lang/String;Ljava/util/List;)LX/0w7;

    goto :goto_c

    .line 585199
    :pswitch_3
    invoke-static {v7, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_d

    .line 585200
    :cond_12
    if-eqz v9, :cond_13

    iget-object v2, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v2, :cond_14

    .line 585201
    :cond_13
    iget-object v2, v0, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 585202
    iget-object v7, v0, LX/0zO;->n:LX/0w5;

    move-object v7, v7

    .line 585203
    invoke-static {v2, v7}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v2

    invoke-virtual {v0}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v7

    .line 585204
    iput-object v7, v2, LX/0zO;->i:Ljava/lang/String;

    .line 585205
    move-object v2, v2

    .line 585206
    iget-object v7, v0, LX/0zO;->b:LX/0zT;

    invoke-virtual {v2, v7}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    goto/16 :goto_2

    .line 585207
    :cond_14
    iget-object v2, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 585208
    iget-object v2, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 585209
    new-instance v12, LX/0w7;

    invoke-virtual {v11}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v7

    invoke-direct {v12, v7}, LX/0w7;-><init>(Ljava/util/Map;)V

    .line 585210
    iget-object v7, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v12, v7, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 585211
    iget-object v2, v0, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 585212
    iget-object v7, v0, LX/0zO;->n:LX/0w5;

    move-object v7, v7

    .line 585213
    invoke-static {v2, v7}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v12}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v2

    iget-object v7, v0, LX/0zO;->b:LX/0zT;

    invoke-virtual {v2, v7}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 585214
    :cond_15
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 585215
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0zO;

    .line 585216
    iget-object v8, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3U1;

    .line 585217
    if-eqz v8, :cond_16

    iget-object v7, v8, LX/3U1;->a:LX/1kt;

    invoke-interface {v7}, LX/1kt;->b()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v7

    .line 585218
    :goto_10
    if-nez v7, :cond_17

    move-object v7, v9

    .line 585219
    goto/16 :goto_3

    .line 585220
    :cond_16
    iget-object v8, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0si;

    invoke-interface {v8, v7}, LX/0sj;->b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v7

    goto :goto_10

    .line 585221
    :cond_17
    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_18
    move-object v7, v10

    .line 585222
    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 585079
    const-string v0, "GraphQLBatchRunner.tryFetch"

    const v2, 0x4c70130f    # 6.2934076E7f

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 585080
    :try_start_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 585081
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v0, v0, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585082
    new-instance v5, LX/3U1;

    invoke-direct {v5}, LX/3U1;-><init>()V

    .line 585083
    iget-object v2, v0, LX/0zO;->g:LX/1kt;

    .line 585084
    if-eqz v2, :cond_1

    :goto_1
    iput-object v2, v5, LX/3U1;->a:LX/1kt;

    .line 585085
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585086
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->k:LX/0t2;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 585087
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    if-eqz v1, :cond_0

    .line 585088
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v1}, LX/1NB;->e()V

    .line 585089
    :cond_0
    const v1, -0x1ecadeb8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 585090
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->g:LX/0sf;

    invoke-virtual {v2, v0}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v2

    goto :goto_1

    .line 585091
    :cond_2
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->c:LX/0tc;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0tc;->a(ZLjava/util/Collection;)LX/3U2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    .line 585092
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a()I

    move-result v0

    .line 585093
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v2, v2, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_4

    .line 585094
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v0}, LX/0v6;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585095
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    if-eqz v0, :cond_3

    .line 585096
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->e()V

    .line 585097
    :cond_3
    const v0, 0x39048adc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 585098
    :goto_2
    return-void

    .line 585099
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->e()V

    .line 585100
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->c:LX/0tc;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/0tc;->a(ZLjava/util/Collection;)LX/3U2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    .line 585101
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    sget-object v2, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {v0, v2}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 585102
    :goto_3
    if-eqz v1, :cond_5

    .line 585103
    invoke-virtual {v1}, LX/1NB;->f()V

    .line 585104
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    sget-object v2, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {v1, v2}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v1

    .line 585105
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 585106
    :cond_5
    const-string v1, "network_blocker_count"

    invoke-static {p0, v1, v0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a(Lcom/facebook/graphql/executor/GraphQLBatchRunner;Ljava/lang/String;I)V

    .line 585107
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->f()V

    .line 585108
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v0}, LX/0v6;->g()V

    .line 585109
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v0, v0, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 585110
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v2, v0}, LX/0v6;->d(LX/0zO;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 585111
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3U1;

    .line 585112
    sget-object v2, LX/1NE;->NETWORK:LX/1NE;

    iput-object v2, v0, LX/3U1;->b:LX/1NE;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 585113
    :cond_7
    :try_start_3
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->g()LX/2lk;

    move-result-object v0

    .line 585114
    invoke-direct {p0, v0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a(LX/2lk;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 585115
    :goto_5
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    if-eqz v0, :cond_8

    .line 585116
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->e()V

    .line 585117
    :cond_8
    const v0, 0x5f610125

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    .line 585118
    :catch_0
    move-exception v0

    .line 585119
    :try_start_4
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->j:LX/03V;

    const-string v2, "GraphQLBatchRunner.updateDB"

    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585120
    iget-object v4, v3, LX/0v6;->c:Ljava/lang/String;

    move-object v3, v4

    .line 585121
    const/16 v4, 0x7d0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5
.end method

.method private f()V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const v5, 0x310027    # 4.499994E-39f

    .line 585071
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->f:LX/11H;

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->e:LX/11E;

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585072
    const-string v0, "network"

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v0, v1, v2, v3}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585073
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v5, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 585074
    return-void

    .line 585075
    :catch_0
    move-exception v0

    .line 585076
    :try_start_1
    const-string v1, "network"

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310027    # 4.499994E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v1, v2, v3, v4}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 585077
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585078
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v1, v5, v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0
.end method

.method private g()LX/2lk;
    .locals 5

    .prologue
    .line 585050
    const-string v0, "GraphQLBatchRunner.updateDBForBatch"

    const v1, 0x50b4e7d5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 585051
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->o:LX/1kv;

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, LX/1kv;->a(LX/0v6;Ljava/util/Map;)LX/2lk;

    move-result-object v1

    .line 585052
    invoke-interface {v1}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 585053
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    iget-object v0, v0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/1NC;

    invoke-interface {v1}, LX/2lk;->d()LX/3Bq;

    move-result-object v2

    iput-object v2, v0, LX/1NC;->a:LX/3Bq;

    .line 585054
    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v0

    .line 585055
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->m:LX/0tA;

    invoke-interface {v1}, LX/2lk;->d()LX/3Bq;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0tA;->b(Ljava/lang/String;LX/3Bq;)V

    .line 585056
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const/16 v4, 0x97

    invoke-interface {v0, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 585057
    :try_start_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->c()V

    .line 585058
    invoke-interface {v1}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 585059
    invoke-interface {v1}, LX/2lk;->d()LX/3Bq;

    move-result-object v2

    .line 585060
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jV;

    .line 585061
    invoke-interface {v0, v2}, LX/0jV;->a(LX/3Bq;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 585062
    :catch_0
    move-exception v0

    .line 585063
    :try_start_2
    sget-object v1, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->a:Ljava/lang/Class;

    const-string v2, "Error updating memory cache"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 585064
    const-string v1, "local_memory_write"

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310027    # 4.499994E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v1, v2, v3, v4}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 585065
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 585066
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const/16 v4, 0x11

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 585067
    :catchall_1
    move-exception v0

    const v1, -0x4b948fa4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 585068
    :cond_1
    :try_start_4
    const-string v0, "local_memory_write"

    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310027    # 4.499994E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static {v0, v2, v3, v4}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 585069
    :try_start_5
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const/16 v4, 0x11

    invoke-interface {v0, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 585070
    const v0, 0x4a735e4f    # 3987347.8f

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1
.end method

.method private h()V
    .locals 5

    .prologue
    const v4, 0x310027    # 4.499994E-39f

    .line 585046
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585047
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const-string v2, "results_returned"

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->q:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 585048
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const-string v2, "errors_returned"

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->r:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 585049
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x2

    const v8, 0x310027    # 4.499994E-39f

    .line 585025
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 585026
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unable to acquire run lock, query runner is shut down"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0v6;->a(Ljava/lang/Throwable;)I

    .line 585027
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 585028
    :goto_0
    return-void

    .line 585029
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    const/16 v3, 0xf

    invoke-interface {v0, v8, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 585030
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585031
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 585032
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h()V

    .line 585033
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v8, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 585034
    :catch_0
    move-exception v2

    .line 585035
    :try_start_1
    iget v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->r:I

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v1, v2}, LX/0v6;->a(Ljava/lang/Throwable;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->r:I

    .line 585036
    const-string v0, "GraphQLBatchRunner.run"

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585037
    iget-object v3, v1, LX/0v6;->c:Ljava/lang/String;

    move-object v1, v3

    .line 585038
    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->j:LX/03V;

    iget-object v4, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310027    # 4.499994E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-static/range {v0 .. v6}, LX/1l8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 585039
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 585040
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h()V

    .line 585041
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v0, v8, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 585042
    :catchall_0
    move-exception v0

    :goto_1
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 585043
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h()V

    .line 585044
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->p:I

    invoke-interface {v2, v8, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0

    .line 585045
    :catchall_1
    move-exception v0

    move v1, v7

    goto :goto_1
.end method
