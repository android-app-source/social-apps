.class public final enum Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventActionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum INVITE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum SHARE_SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "DEFAULT_STYLE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "SHARE_SEND"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE_SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "INVITE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->INVITE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const-string v1, "SHARE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725851
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE_SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->INVITE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725831
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 1

    .prologue
    .line 725832
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 725833
    :goto_0
    return-object v0

    .line 725834
    :cond_1
    const-string v0, "DEFAULT_STYLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725835
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0

    .line 725836
    :cond_2
    const-string v0, "SHARE_SEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725837
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE_SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0

    .line 725838
    :cond_3
    const-string v0, "INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725839
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->INVITE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0

    .line 725840
    :cond_4
    const-string v0, "SEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725841
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0

    .line 725842
    :cond_5
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0

    .line 725844
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 1

    .prologue
    .line 725829
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 1

    .prologue
    .line 725830
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method
