.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum ACTIVE_NOW:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum ALL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum ALL_REMAINING_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum BIRTHDAYS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum BLENDED_HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum BMR:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum BYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum CAMERA_ROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum COMBINED_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum CONVERSATION_REQUESTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum CONVERSATION_STARTERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum CYMK:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum EXTERNAL_URL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum FEATURED_STICKER_PACKS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum GAMES:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum GIFS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum INVITE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MESSAGE_REQUEST_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MESSAGE_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MESSENGER_ADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MESSENGER_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MONTAGE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MULTIACCOUNT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum MY_FB_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum PHOTO_REMINDERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum PYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum RECENT_SMS_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum ROOM_SUGGESTIONS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum RTC_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum SUBSCRIPTION_CONTENTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum SUBSCRIPTION_NUX:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "ACTIVE_NOW"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ACTIVE_NOW:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "ALL_REMAINING_THREADS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_REMAINING_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "ANNOUNCEMENT"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729128
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "BMR"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BMR:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "BYMM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "CAMERA_ROLL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CAMERA_ROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "CONVERSATION_REQUESTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_REQUESTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "CONVERSATION_STARTERS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_STARTERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "CYMK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CYMK:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "EXTERNAL_URL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->EXTERNAL_URL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "FEATURED_STICKER_PACKS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->FEATURED_STICKER_PACKS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "GIFS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GIFS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MESSAGE_REQUEST_THREADS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_REQUEST_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MESSAGE_THREADS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MONTAGE_COMPOSER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MONTAGE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MOST_RECENT_THREADS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "INVITE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->INVITE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "PHOTO_REMINDERS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PHOTO_REMINDERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "PYMM"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "RTC_RECOMMENDATION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RTC_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "SUBSCRIPTION_NUX"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_NUX:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729146
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "VIDEOS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729147
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MULTIACCOUNT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MULTIACCOUNT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729148
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "RECENT_SMS_THREADS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RECENT_SMS_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729149
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "BLENDED_HSCROLL"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BLENDED_HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729150
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "ALL_CONTACTS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729151
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "GAMES"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GAMES:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729152
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "ROOM_SUGGESTIONS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ROOM_SUGGESTIONS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MESSENGER_ADS"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_ADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "SUBSCRIPTION_CONTENTS"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_CONTENTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "DIRECT_M"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "BIRTHDAYS_DIRECT_M"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BIRTHDAYS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MESSENGER_EVENTS_DIRECT_M"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "MY_FB_EVENTS_DIRECT_M"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MY_FB_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    const-string v1, "COMBINED_DIRECT_M"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->COMBINED_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729160
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ACTIVE_NOW:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_REMAINING_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BMR:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CAMERA_ROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_REQUESTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_STARTERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CYMK:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->EXTERNAL_URL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->FEATURED_STICKER_PACKS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GIFS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_REQUEST_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MONTAGE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->INVITE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PHOTO_REMINDERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RTC_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_NUX:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MULTIACCOUNT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RECENT_SMS_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BLENDED_HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GAMES:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ROOM_SUGGESTIONS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_ADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_CONTENTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BIRTHDAYS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MY_FB_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->COMBINED_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;
    .locals 2

    .prologue
    .line 729026
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    .line 729027
    :goto_0
    return-object v0

    .line 729028
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 729029
    packed-switch v0, :pswitch_data_0

    .line 729030
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729031
    :pswitch_1
    const-string v0, "CAMERA_ROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729032
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CAMERA_ROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729033
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729034
    :pswitch_2
    const-string v0, "ALL_REMAINING_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_REMAINING_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729036
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729037
    :pswitch_3
    const-string v0, "BLENDED_HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729038
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BLENDED_HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729039
    :cond_4
    const-string v0, "CONVERSATION_REQUESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729040
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_REQUESTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729041
    :cond_5
    const-string v0, "CONVERSATION_STARTERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729042
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CONVERSATION_STARTERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729043
    :cond_6
    const-string v0, "EXTERNAL_URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729044
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->EXTERNAL_URL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729045
    :cond_7
    const-string v0, "MULTIACCOUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 729046
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MULTIACCOUNT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729047
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto :goto_0

    .line 729048
    :pswitch_4
    const-string v0, "MESSENGER_ADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 729049
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_ADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729050
    :cond_9
    const-string v0, "PYMM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 729051
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729052
    :cond_a
    const-string v0, "COMBINED_DIRECT_M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 729053
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->COMBINED_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729054
    :cond_b
    const-string v0, "BIRTHDAYS_DIRECT_M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 729055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BIRTHDAYS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729056
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729057
    :pswitch_5
    const-string v0, "FEATURED_STICKER_PACKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 729058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->FEATURED_STICKER_PACKS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729059
    :cond_d
    const-string v0, "MESSAGE_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 729060
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729061
    :cond_e
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 729062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729063
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729064
    :pswitch_6
    const-string v0, "INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 729065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->INVITE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729066
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729067
    :pswitch_7
    const-string v0, "MONTAGE_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 729068
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MONTAGE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729069
    :cond_11
    const-string v0, "SUBSCRIPTION_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 729070
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_NUX:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729071
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729072
    :pswitch_8
    const-string v0, "PHOTO_REMINDERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 729073
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->PHOTO_REMINDERS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729074
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729075
    :pswitch_9
    const-string v0, "MOST_RECENT_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 729076
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MOST_RECENT_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729077
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729078
    :pswitch_a
    const-string v0, "ROOM_SUGGESTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 729079
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ROOM_SUGGESTIONS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729080
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729081
    :pswitch_b
    const-string v0, "MESSAGE_REQUEST_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 729082
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_REQUEST_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729083
    :cond_16
    const-string v0, "RECENT_SMS_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 729084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RECENT_SMS_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729085
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729086
    :pswitch_c
    const-string v0, "BMR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 729087
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BMR:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729088
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729089
    :pswitch_d
    const-string v0, "ACTIVE_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 729090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ACTIVE_NOW:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729091
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729092
    :pswitch_e
    const-string v0, "SUBSCRIPTION_CONTENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 729093
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->SUBSCRIPTION_CONTENTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729094
    :cond_1a
    const-string v0, "MY_FB_EVENTS_DIRECT_M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 729095
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MY_FB_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729096
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729097
    :pswitch_f
    const-string v0, "RTC_RECOMMENDATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 729098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->RTC_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729099
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729100
    :pswitch_10
    const-string v0, "GIFS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 729101
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GIFS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729102
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729103
    :pswitch_11
    const-string v0, "ANNOUNCEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 729104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729105
    :cond_1e
    const-string v0, "BYMM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 729106
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->BYMM:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729107
    :cond_1f
    const-string v0, "GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 729108
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->GAMES:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729109
    :cond_20
    const-string v0, "MESSENGER_EVENTS_DIRECT_M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 729110
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSENGER_EVENTS_DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729111
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729112
    :pswitch_12
    const-string v0, "ALL_CONTACTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 729113
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ALL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729114
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729115
    :pswitch_13
    const-string v0, "CYMK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 729116
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->CYMK:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729117
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729118
    :pswitch_14
    const-string v0, "DIRECT_M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 729119
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->DIRECT_M:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    .line 729120
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_14
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;
    .locals 1

    .prologue
    .line 729122
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;
    .locals 1

    .prologue
    .line 729121
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    return-object v0
.end method
