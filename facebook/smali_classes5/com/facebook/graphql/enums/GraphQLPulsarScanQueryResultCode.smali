.class public final enum Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum BACKGROUND_LOCATION_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum BLE_PT_DISABLED_QE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum BUCKET_SUPPRESSED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum HOLDOUT_GROUP:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum HOTSPOT_DETECTED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum NULL_PAGE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum OUTSIDE_THRESHOLD:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum STATUS_DEACTIVATED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS_ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS_BACKGROUND_FORCE_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS_BACKGROUND_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum SUCCESS_BOUNDRY:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum USER_DISABLED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum USER_FAILS_PULSAR_GK:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

.field public static final enum USER_SNOOZED_OR_HIDDEN:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738685
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738686
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738687
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS_BACKGROUND"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738688
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS_ADMIN_FORCE_ON"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738689
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS_BACKGROUND_FORCE_ON"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738690
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS_BACKGROUND_FORCE_OFF"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738691
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "USER_DISABLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_DISABLED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738692
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "HOTSPOT_DETECTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOTSPOT_DETECTED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738693
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "HOLDOUT_GROUP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOLDOUT_GROUP:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738694
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "ADMIN_FORCE_ON"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738695
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "BLE_PT_DISABLED_QE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BLE_PT_DISABLED_QE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738696
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "USER_FAILS_PULSAR_GK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_FAILS_PULSAR_GK:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738697
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "BACKGROUND_LOCATION_OFF"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BACKGROUND_LOCATION_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738698
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "NULL_PAGE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->NULL_PAGE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738699
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "SUCCESS_BOUNDRY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BOUNDRY:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738700
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "STATUS_DEACTIVATED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->STATUS_DEACTIVATED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738701
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "OUTSIDE_THRESHOLD"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->OUTSIDE_THRESHOLD:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738702
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "USER_SNOOZED_OR_HIDDEN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_SNOOZED_OR_HIDDEN:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738703
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    const-string v1, "BUCKET_SUPPRESSED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BUCKET_SUPPRESSED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738704
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_DISABLED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOTSPOT_DETECTED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOLDOUT_GROUP:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BLE_PT_DISABLED_QE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_FAILS_PULSAR_GK:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BACKGROUND_LOCATION_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->NULL_PAGE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BOUNDRY:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->STATUS_DEACTIVATED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->OUTSIDE_THRESHOLD:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_SNOOZED_OR_HIDDEN:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BUCKET_SUPPRESSED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738705
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
    .locals 1

    .prologue
    .line 738706
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    .line 738707
    :goto_0
    return-object v0

    .line 738708
    :cond_1
    const-string v0, "SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738710
    :cond_2
    const-string v0, "BACKGROUND_LOCATION_OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738711
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BACKGROUND_LOCATION_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738712
    :cond_3
    const-string v0, "SUCCESS_BACKGROUND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738713
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738714
    :cond_4
    const-string v0, "SUCCESS_ADMIN_FORCE_ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738715
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738716
    :cond_5
    const-string v0, "SUCCESS_BACKGROUND_FORCE_ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738717
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738718
    :cond_6
    const-string v0, "SUCCESS_BACKGROUND_FORCE_OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BACKGROUND_FORCE_OFF:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738720
    :cond_7
    const-string v0, "SUCCESS_BOUNDRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->SUCCESS_BOUNDRY:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738722
    :cond_8
    const-string v0, "USER_FAILS_PULSAR_GK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_FAILS_PULSAR_GK:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738724
    :cond_9
    const-string v0, "NULL_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738725
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->NULL_PAGE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738726
    :cond_a
    const-string v0, "STATUS_DEACTIVATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738727
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->STATUS_DEACTIVATED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738728
    :cond_b
    const-string v0, "OUTSIDE_THRESHOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 738729
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->OUTSIDE_THRESHOLD:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto :goto_0

    .line 738730
    :cond_c
    const-string v0, "ADMIN_FORCE_ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 738731
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->ADMIN_FORCE_ON:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738732
    :cond_d
    const-string v0, "USER_DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 738733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_DISABLED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738734
    :cond_e
    const-string v0, "HOLDOUT_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 738735
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOLDOUT_GROUP:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738736
    :cond_f
    const-string v0, "HOTSPOT_DETECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 738737
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->HOTSPOT_DETECTED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738738
    :cond_10
    const-string v0, "BLE_PT_DISABLED_QE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 738739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BLE_PT_DISABLED_QE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738740
    :cond_11
    const-string v0, "USER_SNOOZED_OR_HIDDEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 738741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->USER_SNOOZED_OR_HIDDEN:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738742
    :cond_12
    const-string v0, "BUCKET_SUPPRESSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 738743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->BUCKET_SUPPRESSED:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0

    .line 738744
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
    .locals 1

    .prologue
    .line 738745
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;
    .locals 1

    .prologue
    .line 738746
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    return-object v0
.end method
