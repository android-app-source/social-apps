.class public final enum Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

.field public static final enum IN_APP_URL:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

.field public static final enum JSON_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737829
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 737830
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    const-string v1, "JSON_CHECKOUT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->JSON_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 737831
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    const-string v1, "IN_APP_URL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->IN_APP_URL:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 737832
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->JSON_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->IN_APP_URL:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737833
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
    .locals 1

    .prologue
    .line 737834
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 737835
    :goto_0
    return-object v0

    .line 737836
    :cond_1
    const-string v0, "JSON_CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737837
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->JSON_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    goto :goto_0

    .line 737838
    :cond_2
    const-string v0, "IN_APP_URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737839
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->IN_APP_URL:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    goto :goto_0

    .line 737840
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
    .locals 1

    .prologue
    .line 737841
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
    .locals 1

    .prologue
    .line 737842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    return-object v0
.end method
