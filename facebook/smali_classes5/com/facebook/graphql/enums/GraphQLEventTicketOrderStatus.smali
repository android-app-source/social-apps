.class public final enum Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum CHARGE_SUCCESSFUL:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum FAILED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum PURCHASED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum REFUNDED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum RESERVED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum TIMED_OUT:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "RESERVED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->RESERVED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "PURCHASED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->PURCHASED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "REFUNDED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->REFUNDED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->FAILED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "TIMED_OUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->TIMED_OUT:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const-string v1, "CHARGE_SUCCESSFUL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->CHARGE_SUCCESSFUL:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726026
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->RESERVED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->PURCHASED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->REFUNDED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->FAILED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->TIMED_OUT:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->CHARGE_SUCCESSFUL:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726027
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
    .locals 1

    .prologue
    .line 726028
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 726029
    :goto_0
    return-object v0

    .line 726030
    :cond_1
    const-string v0, "RESERVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726031
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->RESERVED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726032
    :cond_2
    const-string v0, "CHARGE_SUCCESSFUL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726033
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->CHARGE_SUCCESSFUL:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726034
    :cond_3
    const-string v0, "PURCHASED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->PURCHASED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726036
    :cond_4
    const-string v0, "FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726037
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->FAILED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726038
    :cond_5
    const-string v0, "REFUNDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726039
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->REFUNDED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726040
    :cond_6
    const-string v0, "TIMED_OUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726041
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->TIMED_OUT:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0

    .line 726042
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
    .locals 1

    .prologue
    .line 726043
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
    .locals 1

    .prologue
    .line 726044
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    return-object v0
.end method
