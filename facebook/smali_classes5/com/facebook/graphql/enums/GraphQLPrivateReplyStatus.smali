.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum CANNOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum EXPIRED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum MESSAGING_DISABLED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum NOT_USER_CREATED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum RECIPIENT_FILTER:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum REPLYABLE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738415
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "ALREADY_REPLIED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "CANNOT_MESSAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->CANNOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "NOT_USER_CREATED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->NOT_USER_CREATED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "RECIPIENT_FILTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->RECIPIENT_FILTER:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "REPLYABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->REPLYABLE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const-string v1, "MESSAGING_DISABLED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->MESSAGING_DISABLED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738423
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->CANNOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->NOT_USER_CREATED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->RECIPIENT_FILTER:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->REPLYABLE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->MESSAGING_DISABLED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738443
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
    .locals 1

    .prologue
    .line 738426
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 738427
    :goto_0
    return-object v0

    .line 738428
    :cond_1
    const-string v0, "REPLYABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738429
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->REPLYABLE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738430
    :cond_2
    const-string v0, "ALREADY_REPLIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738431
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738432
    :cond_3
    const-string v0, "RECIPIENT_FILTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->RECIPIENT_FILTER:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738434
    :cond_4
    const-string v0, "EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738436
    :cond_5
    const-string v0, "NOT_USER_CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->NOT_USER_CREATED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738438
    :cond_6
    const-string v0, "CANNOT_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->CANNOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738440
    :cond_7
    const-string v0, "MESSAGING_DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738441
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->MESSAGING_DISABLED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0

    .line 738442
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
    .locals 1

    .prologue
    .line 738425
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
    .locals 1

    .prologue
    .line 738424
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    return-object v0
.end method
