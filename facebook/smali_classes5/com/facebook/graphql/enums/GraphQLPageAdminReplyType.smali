.class public final enum Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

.field public static final enum ACTIVITY_REPLY:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

.field public static final enum COMMERCE_LINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

.field public static final enum COMMERCE_UNLINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 730077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    .line 730078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    const-string v1, "ACTIVITY_REPLY"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->ACTIVITY_REPLY:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    .line 730079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    const-string v1, "COMMERCE_LINK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_LINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    .line 730080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    const-string v1, "COMMERCE_UNLINK"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_UNLINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    .line 730081
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->ACTIVITY_REPLY:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_LINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_UNLINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;
    .locals 1

    .prologue
    .line 730067
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    .line 730068
    :goto_0
    return-object v0

    .line 730069
    :cond_1
    const-string v0, "ACTIVITY_REPLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730070
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->ACTIVITY_REPLY:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    goto :goto_0

    .line 730071
    :cond_2
    const-string v0, "COMMERCE_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730072
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_LINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    goto :goto_0

    .line 730073
    :cond_3
    const-string v0, "COMMERCE_UNLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 730074
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->COMMERCE_UNLINK:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    goto :goto_0

    .line 730075
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;
    .locals 1

    .prologue
    .line 730065
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;
    .locals 1

    .prologue
    .line 730066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    return-object v0
.end method
