.class public final enum Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum ENT_CONTENT:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum ENT_PAGE_RATING:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum ENT_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum POP_PER_APP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum PRIVACY_FIELD:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum PROFILE_FIELD_POP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum PROFILE_SECTION:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum TIMELINE_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725302
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725303
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "PRIVACY_FIELD"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PRIVACY_FIELD:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725304
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "PROFILE_FIELD_POP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_FIELD_POP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725305
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "PROFILE_SECTION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_SECTION:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725306
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "ENT_CONTENT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_CONTENT:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725307
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "TIMELINE_STORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->TIMELINE_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725308
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "ENT_STORY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725309
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "ENT_PAGE_RATING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_PAGE_RATING:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725310
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    const-string v1, "POP_PER_APP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->POP_PER_APP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725311
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PRIVACY_FIELD:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_FIELD_POP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_SECTION:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_CONTENT:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->TIMELINE_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_PAGE_RATING:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->POP_PER_APP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725333
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;
    .locals 1

    .prologue
    .line 725314
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 725315
    :goto_0
    return-object v0

    .line 725316
    :cond_1
    const-string v0, "PRIVACY_FIELD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725317
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PRIVACY_FIELD:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725318
    :cond_2
    const-string v0, "PROFILE_FIELD_POP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_FIELD_POP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725320
    :cond_3
    const-string v0, "PROFILE_SECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725321
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->PROFILE_SECTION:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725322
    :cond_4
    const-string v0, "ENT_CONTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725323
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_CONTENT:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725324
    :cond_5
    const-string v0, "TIMELINE_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->TIMELINE_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725326
    :cond_6
    const-string v0, "ENT_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_STORY:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725328
    :cond_7
    const-string v0, "ENT_PAGE_RATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725329
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_PAGE_RATING:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725330
    :cond_8
    const-string v0, "POP_PER_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725331
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->POP_PER_APP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0

    .line 725332
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;
    .locals 1

    .prologue
    .line 725313
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;
    .locals 1

    .prologue
    .line 725312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    return-object v0
.end method
