.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum ERROR:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum PAYMENT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum STATUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "CONFIRMATION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ERROR:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "PAYMENT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const-string v1, "STATUS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->STATUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737621
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ERROR:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->STATUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737614
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
    .locals 1

    .prologue
    .line 737599
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 737600
    :goto_0
    return-object v0

    .line 737601
    :cond_1
    const-string v0, "CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737602
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0

    .line 737603
    :cond_2
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737604
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ERROR:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0

    .line 737605
    :cond_3
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0

    .line 737607
    :cond_4
    const-string v0, "PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737608
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0

    .line 737609
    :cond_5
    const-string v0, "STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737610
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->STATUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0

    .line 737611
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
    .locals 1

    .prologue
    .line 737613
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
    .locals 1

    .prologue
    .line 737612
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    return-object v0
.end method
