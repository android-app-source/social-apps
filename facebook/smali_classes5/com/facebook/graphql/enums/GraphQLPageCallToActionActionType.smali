.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum FIRST_PARTY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum PHONE_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

.field public static final enum WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 730082
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730083
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730084
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "PHONE_CALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->PHONE_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730085
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "WEBSITE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730086
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "LEAD_GEN"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730087
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "EMAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730088
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    const-string v1, "FIRST_PARTY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->FIRST_PARTY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730089
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->PHONE_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->FIRST_PARTY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730090
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;
    .locals 1

    .prologue
    .line 730091
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    .line 730092
    :goto_0
    return-object v0

    .line 730093
    :cond_1
    const-string v0, "MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730095
    :cond_2
    const-string v0, "PHONE_CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->PHONE_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730097
    :cond_3
    const-string v0, "WEBSITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 730098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730099
    :cond_4
    const-string v0, "LEAD_GEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 730100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730101
    :cond_5
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 730102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730103
    :cond_6
    const-string v0, "FIRST_PARTY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 730104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->FIRST_PARTY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0

    .line 730105
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;
    .locals 1

    .prologue
    .line 730106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;
    .locals 1

    .prologue
    .line 730107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    return-object v0
.end method
