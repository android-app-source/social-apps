.class public final enum Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

.field public static final enum NON_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

.field public static final enum SEAL:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 741137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 741138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    const-string v1, "NON_LIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NON_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 741139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    const-string v1, "SEAL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->SEAL:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 741140
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NON_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->SEAL:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
    .locals 1

    .prologue
    .line 741143
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 741144
    :goto_0
    return-object v0

    .line 741145
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741146
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    goto :goto_0

    .line 741147
    :cond_2
    const-string v0, "NON_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741148
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NON_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    goto :goto_0

    .line 741149
    :cond_3
    const-string v0, "SEAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741150
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->SEAL:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    goto :goto_0

    .line 741151
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
    .locals 1

    .prologue
    .line 741142
    const-class v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
    .locals 1

    .prologue
    .line 741141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    return-object v0
.end method
