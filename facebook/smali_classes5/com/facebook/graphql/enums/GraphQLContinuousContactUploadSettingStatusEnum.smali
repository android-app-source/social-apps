.class public final enum Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

.field public static final enum DEFAULT:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

.field public static final enum OFF:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

.field public static final enum ON:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724764
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 724765
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 724766
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    const-string v1, "ON"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->ON:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 724767
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->OFF:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 724768
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->ON:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->OFF:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724752
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
    .locals 1

    .prologue
    .line 724755
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 724756
    :goto_0
    return-object v0

    .line 724757
    :cond_1
    const-string v0, "DEFAULT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724758
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    goto :goto_0

    .line 724759
    :cond_2
    const-string v0, "ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724760
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->ON:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    goto :goto_0

    .line 724761
    :cond_3
    const-string v0, "OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724762
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->OFF:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    goto :goto_0

    .line 724763
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
    .locals 1

    .prologue
    .line 724754
    const-class v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
    .locals 1

    .prologue
    .line 724753
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    return-object v0
.end method
