.class public final enum Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum ATTACH_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum CANCEL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum MARK_AS_PAID:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum MARK_AS_SHIPPED:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum REQUEST_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

.field public static final enum VIEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737789
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737790
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "MARK_AS_PAID"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_PAID:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737791
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->CANCEL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737792
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "MARK_AS_SHIPPED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_SHIPPED:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737793
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "REQUEST_NEW_RECEIPT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->REQUEST_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737794
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "ATTACH_NEW_RECEIPT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->ATTACH_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737795
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const-string v1, "VIEW_RECEIPT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->VIEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737796
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_PAID:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->CANCEL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_SHIPPED:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->REQUEST_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->ATTACH_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->VIEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737797
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
    .locals 1

    .prologue
    .line 737774
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 737775
    :goto_0
    return-object v0

    .line 737776
    :cond_1
    const-string v0, "MARK_AS_PAID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737777
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_PAID:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737778
    :cond_2
    const-string v0, "CANCEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->CANCEL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737780
    :cond_3
    const-string v0, "MARK_AS_SHIPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737781
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->MARK_AS_SHIPPED:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737782
    :cond_4
    const-string v0, "REQUEST_NEW_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737783
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->REQUEST_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737784
    :cond_5
    const-string v0, "ATTACH_NEW_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737785
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->ATTACH_NEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737786
    :cond_6
    const-string v0, "VIEW_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737787
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->VIEW_RECEIPT:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0

    .line 737788
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
    .locals 1

    .prologue
    .line 737773
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
    .locals 1

    .prologue
    .line 737772
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    return-object v0
.end method
