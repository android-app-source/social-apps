.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum GAMES:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum MESSENGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum NEARBY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum PAGE_ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum SCHOOL:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum TOP_PAGE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum WORK:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum WORKPLACE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum WORKPLACE_1_1:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

.field public static final enum WORKPLACE_MANAGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "FAMILY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "TOP_PAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->TOP_PAGE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "PAGE_ADMIN"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->PAGE_ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "LIFE_EVENT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "WORK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORK:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "SCHOOL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->SCHOOL:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "MESSENGER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "FRIEND_LIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "GAMES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "EVENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "CLOSE_FRIENDS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->NEARBY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "CURRENT_CITY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "WORKPLACE_1_1"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_1_1:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "WORKPLACE_MANAGER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_MANAGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    const-string v1, "WORKPLACE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726993
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->TOP_PAGE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->PAGE_ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORK:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->SCHOOL:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->NEARBY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_1_1:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_MANAGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
    .locals 1

    .prologue
    .line 726996
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    .line 726997
    :goto_0
    return-object v0

    .line 726998
    :cond_1
    const-string v0, "FAMILY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727000
    :cond_2
    const-string v0, "LIFE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727002
    :cond_3
    const-string v0, "TOP_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->TOP_PAGE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727004
    :cond_4
    const-string v0, "WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727005
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORK:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727006
    :cond_5
    const-string v0, "SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727007
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->SCHOOL:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727008
    :cond_6
    const-string v0, "MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727010
    :cond_7
    const-string v0, "PAGE_ADMIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->PAGE_ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727012
    :cond_8
    const-string v0, "FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727014
    :cond_9
    const-string v0, "GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727016
    :cond_a
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727018
    :cond_b
    const-string v0, "CLOSE_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727019
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto :goto_0

    .line 727020
    :cond_c
    const-string v0, "NEARBY_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727021
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->NEARBY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0

    .line 727022
    :cond_d
    const-string v0, "CURRENT_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0

    .line 727024
    :cond_e
    const-string v0, "WORKPLACE_1_1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_1_1:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0

    .line 727026
    :cond_f
    const-string v0, "WORKPLACE_MANAGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727027
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE_MANAGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0

    .line 727028
    :cond_10
    const-string v0, "WORKPLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727029
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->WORKPLACE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0

    .line 727030
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
    .locals 1

    .prologue
    .line 726995
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;
    .locals 1

    .prologue
    .line 726994
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    return-object v0
.end method
