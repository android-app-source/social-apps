.class public final enum Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field public static final enum COMPACT:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field public static final enum FULL:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 740115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 740116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->FULL:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 740117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    const-string v1, "COMPACT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->COMPACT:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 740118
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->FULL:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->COMPACT:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 1

    .prologue
    .line 740120
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 740121
    :goto_0
    return-object v0

    .line 740122
    :cond_1
    const-string v0, "FULL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740123
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->FULL:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    goto :goto_0

    .line 740124
    :cond_2
    const-string v0, "COMPACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->COMPACT:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    goto :goto_0

    .line 740126
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 1

    .prologue
    .line 740127
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 1

    .prologue
    .line 740128
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-object v0
.end method
