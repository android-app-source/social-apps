.class public final enum Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum AIRPORT_SAVED_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum COLLECTION_CURATION_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DAILY_DIALOGUE_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DESKTOP_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DESKTOP_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DESKTOP_SAVED_FOLLOWUP:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum DESKTOP_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum EGO_REPORT_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum EGO_REPORT_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum EGO_REPORT_NUX_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum EGO_REPORT_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum ENT_PAGE_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum INLINE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MBASIC_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MESSENGER_DOT_COM_SAVE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_NETEGO_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_NETEGO_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_PAGE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_PAGE_TOAST_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum MOBILE_STORY_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum NETEGO_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum NETEGO_COLLECTION_RATING_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum NETEGO_TOP100_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum NETEGO_TOP100_STATUSBOX:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum NEWSFEED_QUESTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum OG_COLLECTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum PAPER_SAVED_TAB:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum REMINDER_BOX_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum REMINDER_BOX_SEE_MORE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum REMINDER_BOX_TITLE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_COLLECTION_HEADER_DROPDOWN:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_DASHBOARD_IN_APP_BROWSER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_DASHBOARD_ME_TAB_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_EMAIL_REMINDER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_HEAVY_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_LIGHT_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_ME_TAB_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_NEAR_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_NEWSFEED_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_OFF_FB_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_PLUGIN_OVERLAY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_PRODUCTS_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_TOAST_RECENT_ITEM_COUNT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SAVED_VIDEOS_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SEARCH_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SOCIAL_CONTEXT_FACEPILE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum SOCIAL_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum STORY_ATTACHMENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum VIDEO_DOWNLOAD_FEED_ROW:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum VIDEO_DOWNLOAD_FINISHED_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum VIDEO_DOWNLOAD_QUOTA_EXCEEDED:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public static final enum WELCOME_PAGE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724150
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724151
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "OG_COLLECTION_LINK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->OG_COLLECTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724152
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "EGO_REPORT_LINK"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "EGO_REPORT_PLUS_BUTTON"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "EGO_REPORT_HEADER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "NETEGO_COLLECTION_HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "NETEGO_COLLECTION_RATING_HEADER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_RATING_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SOCIAL_CONTEXT_FACEPILE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_CONTEXT_FACEPILE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SOCIAL_PLUS_BUTTON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "NEWSFEED_QUESTION_LINK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NEWSFEED_QUESTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "EGO_REPORT_NUX_LINK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_NUX_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "NETEGO_TOP100_HEADER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "NETEGO_TOP100_STATUSBOX"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_STATUSBOX:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "WELCOME_PAGE_LINK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->WELCOME_PAGE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_BOOKMARK"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "ENT_PAGE_ACTION_BAR"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->ENT_PAGE_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_COLLECTION_HEADER"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "COLLECTION_CURATION_FOOTER"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->COLLECTION_CURATION_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "STORY_ATTACHMENT_FOOTER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->STORY_ATTACHMENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724169
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724170
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DESKTOP_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724171
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_STORY_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_STORY_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_NETEGO_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_PAGE_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_NETEGO_SEE_ALL_LINK"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724175
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_PAGE_TOAST_LINK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_TOAST_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724176
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DESKTOP_SAVED_BOOKMARK"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724177
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_SAVED_BOOKMARK"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724178
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "REMINDER_BOX_TITLE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_TITLE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724179
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "REMINDER_BOX_SEE_ALL_LINK"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724180
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "REMINDER_BOX_SEE_MORE_LINK"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_MORE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724181
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SEARCH_SHORTCUT"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SEARCH_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724182
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "PAPER_SAVED_TAB"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->PAPER_SAVED_TAB:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724183
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DESKTOP_SAVED_FOLLOWUP"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_FOLLOWUP:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724184
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_DUMMY_STORY_SEE_ALL_LINK"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724185
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724186
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_EMAIL_REMINDER"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_EMAIL_REMINDER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724187
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MBASIC_SAVED_BOOKMARK"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MBASIC_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724188
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724189
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724190
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_NEAR_PLACE_NOTIF"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEAR_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724191
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_HEAVY_REMINDER_NOTIF"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_HEAVY_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724192
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_COLLECTION_HEADER_DROPDOWN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER_DROPDOWN:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724193
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_DASHBOARD_IN_APP_BROWSER"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724194
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_DASHBOARD_ME_TAB_SEE_MORE"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_ME_TAB_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724195
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_ME_TAB_SECTION_HEADER"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_ME_TAB_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724196
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_PRODUCTS_NOTIF"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PRODUCTS_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724197
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_PLUGIN_OVERLAY"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PLUGIN_OVERLAY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724198
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_NEWSFEED_ENTRY_POINT"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEWSFEED_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724199
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "AIRPORT_SAVED_PLACE_NOTIF"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->AIRPORT_SAVED_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724200
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_OFF_FB_NOTIF"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_OFF_FB_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724201
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_TOAST_RECENT_ITEM_COUNT"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_TOAST_RECENT_ITEM_COUNT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724202
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "VIDEO_DOWNLOAD_FINISHED_NOTIF"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FINISHED_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "VIDEO_DOWNLOAD_QUOTA_EXCEEDED"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_QUOTA_EXCEEDED:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724204
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DAILY_DIALOGUE_PINNED_UNIT"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DAILY_DIALOGUE_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724205
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_LIGHT_REMINDER_NOTIF"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_LIGHT_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724206
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "DESKTOP_DUMMY_STORY_SEE_ALL_LINK"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724207
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724208
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "MESSENGER_DOT_COM_SAVE_CONFIRMATION"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MESSENGER_DOT_COM_SAVE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "VIDEO_DOWNLOAD_FEED_ROW"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FEED_ROW:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724211
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "INLINE_DOWNLOAD"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->INLINE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724212
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const-string v1, "SAVED_VIDEOS_REMINDER_NOTIF"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724213
    const/16 v0, 0x3f

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->OG_COLLECTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_RATING_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_CONTEXT_FACEPILE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NEWSFEED_QUESTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_NUX_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_STATUSBOX:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->WELCOME_PAGE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->ENT_PAGE_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->COLLECTION_CURATION_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->STORY_ATTACHMENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_STORY_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_TOAST_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_TITLE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_MORE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SEARCH_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->PAPER_SAVED_TAB:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_FOLLOWUP:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_EMAIL_REMINDER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MBASIC_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEAR_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_HEAVY_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER_DROPDOWN:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_ME_TAB_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_ME_TAB_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PRODUCTS_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PLUGIN_OVERLAY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEWSFEED_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->AIRPORT_SAVED_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_OFF_FB_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_TOAST_RECENT_ITEM_COUNT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FINISHED_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_QUOTA_EXCEEDED:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DAILY_DIALOGUE_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_LIGHT_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MESSENGER_DOT_COM_SAVE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FEED_ROW:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->INLINE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;
    .locals 2

    .prologue
    .line 724215
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 724216
    :goto_0
    return-object v0

    .line 724217
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 724218
    packed-switch v0, :pswitch_data_0

    .line 724219
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724220
    :pswitch_1
    const-string v0, "REMINDER_BOX_SEE_ALL_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724221
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724222
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724223
    :pswitch_2
    const-string v0, "REMINDER_BOX_SEE_MORE_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724224
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_SEE_MORE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724225
    :cond_3
    const-string v0, "MOBILE_DUMMY_STORY_SEE_ALL_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724227
    :cond_4
    const-string v0, "SAVED_PRODUCTS_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PRODUCTS_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724229
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724230
    :pswitch_3
    const-string v0, "MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724232
    :cond_6
    const-string v0, "MESSENGER_DOT_COM_SAVE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MESSENGER_DOT_COM_SAVE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724234
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724235
    :pswitch_4
    const-string v0, "SAVED_NEAR_PLACE_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEAR_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_0

    .line 724237
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724238
    :pswitch_5
    const-string v0, "EGO_REPORT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724240
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724241
    :pswitch_6
    const-string v0, "SOCIAL_CONTEXT_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_CONTEXT_FACEPILE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724243
    :cond_a
    const-string v0, "SAVED_COLLECTION_HEADER_DROPDOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724244
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER_DROPDOWN:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724245
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724246
    :pswitch_7
    const-string v0, "ENT_PAGE_ACTION_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 724247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->ENT_PAGE_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724248
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724249
    :pswitch_8
    const-string v0, "SAVED_HEAVY_REMINDER_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 724250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_HEAVY_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724251
    :cond_d
    const-string v0, "SAVED_LIGHT_REMINDER_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 724252
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_LIGHT_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724253
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724254
    :pswitch_9
    const-string v0, "SAVED_VIDEOS_REMINDER_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 724255
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_REMINDER_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724256
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724257
    :pswitch_a
    const-string v0, "EGO_REPORT_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 724258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724259
    :cond_10
    const-string v0, "SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 724260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER_MESSENGER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724261
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724262
    :pswitch_b
    const-string v0, "DESKTOP_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 724263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724264
    :cond_12
    const-string v0, "DESKTOP_SAVED_FOLLOWUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 724265
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_FOLLOWUP:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724266
    :cond_13
    const-string v0, "DAILY_DIALOGUE_PINNED_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 724267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DAILY_DIALOGUE_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724268
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724269
    :pswitch_c
    const-string v0, "COLLECTION_CURATION_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 724270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->COLLECTION_CURATION_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724271
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724272
    :pswitch_d
    const-string v0, "EGO_REPORT_PLUS_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 724273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724274
    :cond_16
    const-string v0, "EGO_REPORT_NUX_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 724275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->EGO_REPORT_NUX_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724276
    :cond_17
    const-string v0, "NETEGO_TOP100_STATUSBOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 724277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_STATUSBOX:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724278
    :cond_18
    const-string v0, "SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 724279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_VIDEOS_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724280
    :cond_19
    const-string v0, "SAVED_DASHBOARD_ME_TAB_SEE_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 724281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_ME_TAB_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724282
    :cond_1a
    const-string v0, "VIDEO_DOWNLOAD_FINISHED_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 724283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FINISHED_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724284
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724285
    :pswitch_e
    const-string v0, "SEARCH_SHORTCUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 724286
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SEARCH_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724287
    :cond_1c
    const-string v0, "SAVED_PLUGIN_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 724288
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_PLUGIN_OVERLAY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724289
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724290
    :pswitch_f
    const-string v0, "DESKTOP_SAVED_BOOKMARK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 724291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724292
    :cond_1e
    const-string v0, "VIDEO_DOWNLOAD_QUOTA_EXCEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 724293
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_QUOTA_EXCEEDED:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724294
    :cond_1f
    const-string v0, "VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 724295
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724296
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724297
    :pswitch_10
    const-string v0, "NETEGO_TOP100_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 724298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_TOP100_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724299
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724300
    :pswitch_11
    const-string v0, "MOBILE_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 724301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724302
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724303
    :pswitch_12
    const-string v0, "NETEGO_COLLECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 724304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724305
    :cond_23
    const-string v0, "AIRPORT_SAVED_PLACE_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 724306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->AIRPORT_SAVED_PLACE_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724307
    :cond_24
    const-string v0, "INLINE_DOWNLOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 724308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->INLINE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724309
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724310
    :pswitch_13
    const-string v0, "SAVED_EMAIL_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 724311
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_EMAIL_REMINDER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724312
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724313
    :pswitch_14
    const-string v0, "OG_COLLECTION_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 724314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->OG_COLLECTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724315
    :cond_27
    const-string v0, "SAVED_BOOKMARK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 724316
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724317
    :cond_28
    const-string v0, "VIDEO_DOWNLOAD_FEED_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 724318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_FEED_ROW:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724319
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724320
    :pswitch_15
    const-string v0, "SOCIAL_PLUS_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 724321
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SOCIAL_PLUS_BUTTON:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724322
    :cond_2a
    const-string v0, "MOBILE_PAGE_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 724323
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724324
    :cond_2b
    const-string v0, "MOBILE_SAVED_BOOKMARK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 724325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724326
    :cond_2c
    const-string v0, "MBASIC_SAVED_BOOKMARK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 724327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MBASIC_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724328
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724329
    :pswitch_16
    const-string v0, "SAVED_COLLECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 724330
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_COLLECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724331
    :cond_2e
    const-string v0, "STORY_ATTACHMENT_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 724332
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->STORY_ATTACHMENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724333
    :cond_2f
    const-string v0, "MOBILE_STORY_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 724334
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_STORY_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724335
    :cond_30
    const-string v0, "MOBILE_PAGE_TOAST_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 724336
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_PAGE_TOAST_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724337
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724338
    :pswitch_17
    const-string v0, "NEWSFEED_QUESTION_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 724339
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NEWSFEED_QUESTION_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724340
    :cond_32
    const-string v0, "MOBILE_NETEGO_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 724341
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724342
    :cond_33
    const-string v0, "SAVED_NEWSFEED_ENTRY_POINT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 724343
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_NEWSFEED_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724344
    :cond_34
    const-string v0, "DESKTOP_DUMMY_STORY_SEE_ALL_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 724345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_DUMMY_STORY_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724346
    :cond_35
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724347
    :pswitch_18
    const-string v0, "DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 724348
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->DESKTOP_PRODUCT_DETAILS_SAVE_BUTTON_FLYOUT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724349
    :cond_36
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724350
    :pswitch_19
    const-string v0, "NETEGO_COLLECTION_RATING_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 724351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->NETEGO_COLLECTION_RATING_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724352
    :cond_37
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724353
    :pswitch_1a
    const-string v0, "MOBILE_NETEGO_SEE_ALL_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 724354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724355
    :cond_38
    const-string v0, "SAVED_ME_TAB_SECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 724356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_ME_TAB_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724357
    :cond_39
    const-string v0, "SAVED_TOAST_RECENT_ITEM_COUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 724358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_TOAST_RECENT_ITEM_COUNT:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724359
    :cond_3a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724360
    :pswitch_1b
    const-string v0, "WELCOME_PAGE_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 724361
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->WELCOME_PAGE_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724362
    :cond_3b
    const-string v0, "PAPER_SAVED_TAB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 724363
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->PAPER_SAVED_TAB:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724364
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724365
    :pswitch_1c
    const-string v0, "REMINDER_BOX_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 724366
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->REMINDER_BOX_TITLE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724367
    :cond_3d
    const-string v0, "SAVED_DASHBOARD_IN_APP_BROWSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 724368
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_DASHBOARD_IN_APP_BROWSER:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724369
    :cond_3e
    const-string v0, "SAVED_OFF_FB_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 724370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->SAVED_OFF_FB_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    .line 724371
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_1c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;
    .locals 1

    .prologue
    .line 724372
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;
    .locals 1

    .prologue
    .line 724373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    return-object v0
.end method
