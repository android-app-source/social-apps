.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

.field public static final enum OFFSITE_AD:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728780
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    .line 728781
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    const-string v1, "OFFSITE_AD"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->OFFSITE_AD:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    .line 728782
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->OFFSITE_AD:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728783
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;
    .locals 1

    .prologue
    .line 728784
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    .line 728785
    :goto_0
    return-object v0

    .line 728786
    :cond_1
    const-string v0, "OFFSITE_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728787
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->OFFSITE_AD:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    goto :goto_0

    .line 728788
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;
    .locals 1

    .prologue
    .line 728789
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;
    .locals 1

    .prologue
    .line 728790
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerAdPropertyType;

    return-object v0
.end method
