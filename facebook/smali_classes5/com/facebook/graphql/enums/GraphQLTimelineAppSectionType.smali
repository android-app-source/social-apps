.class public final enum Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum APP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum AT_WORK:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum FITNESS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum GAMES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum REVIEWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum SAVED_FOR_LATER:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum SPORTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740633
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740634
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "APP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->APP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740635
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "ABOUT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740636
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740637
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "PHOTOS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740638
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "LIKES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740639
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "MAP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740640
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "SUBSCRIPTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740641
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "SUBSCRIBERS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740642
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "PAGE_LIKES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740643
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "NOTES"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740644
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "EVENTS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740645
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "GROUPS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740646
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "BOOKS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740647
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "MUSIC"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740648
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "TV_SHOWS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740649
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "MOVIES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740650
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "FITNESS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740651
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "CONTACT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740652
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "SPORTS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740653
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "GAMES"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740654
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "SAVED_FOR_LATER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SAVED_FOR_LATER:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740655
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "REVIEWS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740656
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "AT_WORK"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->AT_WORK:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740657
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const-string v1, "VIDEOS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740658
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->APP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SAVED_FOR_LATER:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->AT_WORK:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740632
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .locals 1

    .prologue
    .line 740659
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 740660
    :goto_0
    return-object v0

    .line 740661
    :cond_1
    const-string v0, "APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->APP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740663
    :cond_2
    const-string v0, "ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740664
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740665
    :cond_3
    const-string v0, "FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740666
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740667
    :cond_4
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740668
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740669
    :cond_5
    const-string v0, "LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740670
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740671
    :cond_6
    const-string v0, "MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740672
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740673
    :cond_7
    const-string v0, "SUBSCRIPTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740675
    :cond_8
    const-string v0, "SUBSCRIBERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740676
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740677
    :cond_9
    const-string v0, "PAGE_LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740678
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740679
    :cond_a
    const-string v0, "NOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 740680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740681
    :cond_b
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 740682
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0

    .line 740683
    :cond_c
    const-string v0, "BOOKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 740684
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740685
    :cond_d
    const-string v0, "MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 740686
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740687
    :cond_e
    const-string v0, "TV_SHOWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 740688
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740689
    :cond_f
    const-string v0, "MOVIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 740690
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740691
    :cond_10
    const-string v0, "FITNESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 740692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740693
    :cond_11
    const-string v0, "CONTACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 740694
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740695
    :cond_12
    const-string v0, "GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 740696
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740697
    :cond_13
    const-string v0, "SPORTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 740698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740699
    :cond_14
    const-string v0, "GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 740700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740701
    :cond_15
    const-string v0, "SAVED_FOR_LATER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 740702
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SAVED_FOR_LATER:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740703
    :cond_16
    const-string v0, "REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 740704
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740705
    :cond_17
    const-string v0, "AT_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 740706
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->AT_WORK:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740707
    :cond_18
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 740708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0

    .line 740709
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .locals 1

    .prologue
    .line 740631
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .locals 1

    .prologue
    .line 740630
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    return-object v0
.end method
