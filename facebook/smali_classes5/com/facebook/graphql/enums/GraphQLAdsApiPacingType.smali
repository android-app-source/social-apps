.class public final enum Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum DAY_PARTING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum DISABLED:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum PROBABILISTIC_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723265
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723266
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723267
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DISABLED:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723268
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "DAY_PARTING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DAY_PARTING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723269
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "NO_PACING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723270
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const-string v1, "PROBABILISTIC_PACING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->PROBABILISTIC_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723271
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DISABLED:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DAY_PARTING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->PROBABILISTIC_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .locals 1

    .prologue
    .line 723273
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 723274
    :goto_0
    return-object v0

    .line 723275
    :cond_1
    const-string v0, "STANDARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723276
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0

    .line 723277
    :cond_2
    const-string v0, "DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DISABLED:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0

    .line 723279
    :cond_3
    const-string v0, "DAY_PARTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->DAY_PARTING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0

    .line 723281
    :cond_4
    const-string v0, "NO_PACING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0

    .line 723283
    :cond_5
    const-string v0, "PROBABILISTIC_PACING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->PROBABILISTIC_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0

    .line 723285
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .locals 1

    .prologue
    .line 723286
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .locals 1

    .prologue
    .line 723287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    return-object v0
.end method
