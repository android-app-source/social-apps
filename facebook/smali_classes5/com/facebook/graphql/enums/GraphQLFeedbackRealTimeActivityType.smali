.class public final enum Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

.field public static final enum COMMENT:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 726545
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 726546
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 726547
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 726548
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 726549
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726550
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;
    .locals 1

    .prologue
    .line 726551
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 726552
    :goto_0
    return-object v0

    .line 726553
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    goto :goto_0

    .line 726555
    :cond_2
    const-string v0, "COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726556
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    goto :goto_0

    .line 726557
    :cond_3
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726558
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    goto :goto_0

    .line 726559
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;
    .locals 1

    .prologue
    .line 726560
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;
    .locals 1

    .prologue
    .line 726561
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    return-object v0
.end method
