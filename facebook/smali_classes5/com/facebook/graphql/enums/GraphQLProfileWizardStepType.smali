.class public final enum Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738625
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738627
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "CORE_PROFILE_FIELD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738628
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "COVER_PHOTO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738629
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "PROFILE_PICTURE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738630
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "INTRO_CARD_BIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738631
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    const-string v1, "INTRO_CARD_FEATURED_PHOTOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738632
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738633
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;
    .locals 1

    .prologue
    .line 738634
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 738635
    :goto_0
    return-object v0

    .line 738636
    :cond_1
    const-string v0, "COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738638
    :cond_2
    const-string v0, "CORE_PROFILE_FIELD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738639
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738640
    :cond_3
    const-string v0, "COVER_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738641
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738642
    :cond_4
    const-string v0, "PROFILE_PICTURE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738643
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738644
    :cond_5
    const-string v0, "INTRO_CARD_BIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738646
    :cond_6
    const-string v0, "INTRO_CARD_FEATURED_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0

    .line 738648
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;
    .locals 1

    .prologue
    .line 738649
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;
    .locals 1

    .prologue
    .line 738650
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    return-object v0
.end method
