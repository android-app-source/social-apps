.class public final enum Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

.field public static final enum ALL_CAPS:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

.field public static final enum ALL_LOWER_CASE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 724069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 724070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    const-string v1, "ALL_CAPS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_CAPS:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 724071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    const-string v1, "ALL_LOWER_CASE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_LOWER_CASE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 724072
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_CAPS:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_LOWER_CASE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724056
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;
    .locals 1

    .prologue
    .line 724059
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 724060
    :goto_0
    return-object v0

    .line 724061
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    goto :goto_0

    .line 724063
    :cond_2
    const-string v0, "ALL_CAPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724064
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_CAPS:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    goto :goto_0

    .line 724065
    :cond_3
    const-string v0, "ALL_LOWER_CASE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_LOWER_CASE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    goto :goto_0

    .line 724067
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;
    .locals 1

    .prologue
    .line 724058
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;
    .locals 1

    .prologue
    .line 724057
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    return-object v0
.end method
