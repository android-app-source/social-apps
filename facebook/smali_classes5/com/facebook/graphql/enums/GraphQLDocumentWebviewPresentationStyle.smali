.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum FACEBOOK:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum HTML_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum SOCIAL_EMBED:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum TRACKER:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum TWEET:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public static final enum YOUTUBE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725247
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725248
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "SOCIAL_EMBED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->SOCIAL_EMBED:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725249
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "FULL_SCREEN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725250
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "HTML_INTERACTIVE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->HTML_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725251
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "AD"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725252
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "TRACKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TRACKER:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725253
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "TWEET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TWEET:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725254
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "INSTAGRAM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725255
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "YOUTUBE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->YOUTUBE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725256
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "VINE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725257
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const-string v1, "FACEBOOK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725258
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->SOCIAL_EMBED:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->HTML_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TRACKER:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TWEET:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->YOUTUBE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 1

    .prologue
    .line 725260
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 725261
    :goto_0
    return-object v0

    .line 725262
    :cond_1
    const-string v0, "HTML_INTERACTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->HTML_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725264
    :cond_2
    const-string v0, "SOCIAL_EMBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725265
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->SOCIAL_EMBED:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725266
    :cond_3
    const-string v0, "FULL_SCREEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725268
    :cond_4
    const-string v0, "AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725270
    :cond_5
    const-string v0, "TRACKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TRACKER:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725272
    :cond_6
    const-string v0, "TWEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TWEET:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725274
    :cond_7
    const-string v0, "INSTAGRAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725276
    :cond_8
    const-string v0, "YOUTUBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->YOUTUBE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725278
    :cond_9
    const-string v0, "VINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 725279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725280
    :cond_a
    const-string v0, "FACEBOOK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 725281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0

    .line 725282
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 1

    .prologue
    .line 725283
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 1

    .prologue
    .line 725284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    return-object v0
.end method
