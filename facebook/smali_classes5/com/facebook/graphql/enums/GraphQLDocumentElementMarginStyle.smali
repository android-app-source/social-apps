.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public static final enum AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public static final enum COLUMN_WIDTH:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public static final enum FULL_BLEED:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 725005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 725006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    const-string v1, "FULL_BLEED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->FULL_BLEED:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 725007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    const-string v1, "COLUMN_WIDTH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->COLUMN_WIDTH:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 725008
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->FULL_BLEED:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->COLUMN_WIDTH:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725009
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 1

    .prologue
    .line 725010
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 725011
    :goto_0
    return-object v0

    .line 725012
    :cond_1
    const-string v0, "AUTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    goto :goto_0

    .line 725014
    :cond_2
    const-string v0, "FULL_BLEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->FULL_BLEED:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    goto :goto_0

    .line 725016
    :cond_3
    const-string v0, "COLUMN_WIDTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->COLUMN_WIDTH:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    goto :goto_0

    .line 725018
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 1

    .prologue
    .line 725019
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 1

    .prologue
    .line 725020
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    return-object v0
.end method
