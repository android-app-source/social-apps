.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

.field public static final enum LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

.field public static final enum POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 723913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    const-string v1, "POST_ENGAGEMENT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 723914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    const-string v1, "LINK_CLICKS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 723915
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723911
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .locals 1

    .prologue
    .line 723902
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 723903
    :goto_0
    return-object v0

    .line 723904
    :cond_1
    const-string v0, "POST_ENGAGEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723905
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    goto :goto_0

    .line 723906
    :cond_2
    const-string v0, "LINK_CLICKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723907
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    goto :goto_0

    .line 723908
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .locals 1

    .prologue
    .line 723910
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .locals 1

    .prologue
    .line 723909
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    return-object v0
.end method
