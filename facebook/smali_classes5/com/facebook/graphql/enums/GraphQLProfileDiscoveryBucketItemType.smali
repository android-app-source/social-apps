.class public final enum Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum CURATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum CURATION_SEE_LESS_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum CURATION_SEE_MORE_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum CURATION_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum END:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum PERSON:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum PIVOT:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738467
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738468
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "PERSON"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PERSON:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738469
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "CURATION_QUESTION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738470
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "CURATION_SEE_MORE_TAGGING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_MORE_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738471
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "CURATION_SEE_LESS_TAGGING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_LESS_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738472
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "CURATION_TAGGING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738473
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "END"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->END:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738474
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "INTRO_CARD_BIO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738475
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "FEATURED_PHOTOS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    const-string v1, "PIVOT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738477
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PERSON:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_MORE_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_LESS_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->END:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738480
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;
    .locals 1

    .prologue
    .line 738481
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    .line 738482
    :goto_0
    return-object v0

    .line 738483
    :cond_1
    const-string v0, "PERSON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738484
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PERSON:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738485
    :cond_2
    const-string v0, "CURATION_QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738487
    :cond_3
    const-string v0, "CURATION_SEE_MORE_TAGGING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_MORE_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738489
    :cond_4
    const-string v0, "CURATION_SEE_LESS_TAGGING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738490
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_SEE_LESS_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738491
    :cond_5
    const-string v0, "CURATION_TAGGING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->CURATION_TAGGING:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738493
    :cond_6
    const-string v0, "END"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->END:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738495
    :cond_7
    const-string v0, "INTRO_CARD_BIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738497
    :cond_8
    const-string v0, "FEATURED_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738499
    :cond_9
    const-string v0, "PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0

    .line 738501
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;
    .locals 1

    .prologue
    .line 738479
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;
    .locals 1

    .prologue
    .line 738478
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    return-object v0
.end method
