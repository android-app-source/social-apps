.class public final enum Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field public static final enum FOOD_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field public static final enum PHOTOS_OF_NEXT_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field public static final enum POPULAR_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 738154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const-string v1, "POPULAR_PHOTOS_OF_PLACE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->POPULAR_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 738155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const-string v1, "FOOD_PHOTOS_OF_PLACE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->FOOD_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 738156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const-string v1, "PHOTOS_OF_NEXT_PLACE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->PHOTOS_OF_NEXT_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 738157
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->POPULAR_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->FOOD_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->PHOTOS_OF_NEXT_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738158
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .locals 1

    .prologue
    .line 738159
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 738160
    :goto_0
    return-object v0

    .line 738161
    :cond_1
    const-string v0, "POPULAR_PHOTOS_OF_PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738162
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->POPULAR_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    goto :goto_0

    .line 738163
    :cond_2
    const-string v0, "FOOD_PHOTOS_OF_PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738164
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->FOOD_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    goto :goto_0

    .line 738165
    :cond_3
    const-string v0, "PHOTOS_OF_NEXT_PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738166
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->PHOTOS_OF_NEXT_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    goto :goto_0

    .line 738167
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .locals 1

    .prologue
    .line 738168
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .locals 1

    .prologue
    .line 738169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    return-object v0
.end method
