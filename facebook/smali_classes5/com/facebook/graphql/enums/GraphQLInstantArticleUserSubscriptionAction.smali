.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

.field public static final enum ACCEPTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

.field public static final enum REJECTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

.field public static final enum VIEWED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727577
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    .line 727578
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    const-string v1, "VIEWED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->VIEWED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    .line 727579
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    .line 727580
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->REJECTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    .line 727581
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->VIEWED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->REJECTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727584
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;
    .locals 1

    .prologue
    .line 727585
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    .line 727586
    :goto_0
    return-object v0

    .line 727587
    :cond_1
    const-string v0, "VIEWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727588
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->VIEWED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    goto :goto_0

    .line 727589
    :cond_2
    const-string v0, "ACCEPTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727590
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    goto :goto_0

    .line 727591
    :cond_3
    const-string v0, "REJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->REJECTED:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    goto :goto_0

    .line 727593
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;
    .locals 1

    .prologue
    .line 727583
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;
    .locals 1

    .prologue
    .line 727582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    return-object v0
.end method
