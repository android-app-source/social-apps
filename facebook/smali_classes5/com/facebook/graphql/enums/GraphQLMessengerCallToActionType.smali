.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum ACCOUNT_LINK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum FACEBOOK_REPORT_A_PROBLEM:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum MANAGE_MESSAGES:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum OPEN_CANCEL_RIDE_MUTATION:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum OPEN_NATIVE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum OPEN_URL:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum PAYMENT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum POSTBACK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum SUBSCRIPTION_PRESELECT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "OPEN_NATIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_NATIVE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "OPEN_URL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "OPEN_CANCEL_RIDE_MUTATION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_CANCEL_RIDE_MUTATION:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "POSTBACK"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->POSTBACK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "ACCOUNT_LINK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->ACCOUNT_LINK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "MANAGE_MESSAGES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->MANAGE_MESSAGES:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "SHARE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728853
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "PAYMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728854
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "FACEBOOK_REPORT_A_PROBLEM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->FACEBOOK_REPORT_A_PROBLEM:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728855
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    const-string v1, "SUBSCRIPTION_PRESELECT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SUBSCRIPTION_PRESELECT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728856
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_NATIVE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_CANCEL_RIDE_MUTATION:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->POSTBACK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->ACCOUNT_LINK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->MANAGE_MESSAGES:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->FACEBOOK_REPORT_A_PROBLEM:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SUBSCRIPTION_PRESELECT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728844
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;
    .locals 1

    .prologue
    .line 728821
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    .line 728822
    :goto_0
    return-object v0

    .line 728823
    :cond_1
    const-string v0, "OPEN_NATIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728824
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_NATIVE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728825
    :cond_2
    const-string v0, "OPEN_URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728826
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728827
    :cond_3
    const-string v0, "OPEN_CANCEL_RIDE_MUTATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728828
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->OPEN_CANCEL_RIDE_MUTATION:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728829
    :cond_4
    const-string v0, "POSTBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728830
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->POSTBACK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728831
    :cond_5
    const-string v0, "ACCOUNT_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728832
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->ACCOUNT_LINK:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728833
    :cond_6
    const-string v0, "MANAGE_MESSAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728834
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->MANAGE_MESSAGES:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728835
    :cond_7
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728836
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728837
    :cond_8
    const-string v0, "PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 728838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728839
    :cond_9
    const-string v0, "FACEBOOK_REPORT_A_PROBLEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 728840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->FACEBOOK_REPORT_A_PROBLEM:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728841
    :cond_a
    const-string v0, "SUBSCRIPTION_PRESELECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 728842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->SUBSCRIPTION_PRESELECT:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0

    .line 728843
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;
    .locals 1

    .prologue
    .line 728819
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;
    .locals 1

    .prologue
    .line 728820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    return-object v0
.end method
