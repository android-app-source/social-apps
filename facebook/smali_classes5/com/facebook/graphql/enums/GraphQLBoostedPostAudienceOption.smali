.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum CREATE_NEW:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum CUSTOM_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum DEPRECATED_12:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum DISTRICT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum EVENT_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum IG_PROMOTED_POST_AUTO:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum LOCAL:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum LOOKALIKE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "GROUPER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "NCPP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "CUSTOM_AUDIENCE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CUSTOM_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "LOOKALIKE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOOKALIKE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "DEPRECATED_5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "FANS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "LOCAL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOCAL:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "IG_PROMOTED_POST_AUTO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->IG_PROMOTED_POST_AUTO:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "SAVED_AUDIENCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "EVENT_ENGAGEMENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->EVENT_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "DISTRICT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DISTRICT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "DEPRECATED_12"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DEPRECATED_12:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const-string v1, "CREATE_NEW"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CREATE_NEW:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 724035
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CUSTOM_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOOKALIKE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOCAL:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->IG_PROMOTED_POST_AUTO:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->EVENT_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DISTRICT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DEPRECATED_12:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CREATE_NEW:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723993
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .locals 1

    .prologue
    .line 723994
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 723995
    :goto_0
    return-object v0

    .line 723996
    :cond_1
    const-string v0, "GROUPER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723997
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 723998
    :cond_2
    const-string v0, "NCPP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724000
    :cond_3
    const-string v0, "CUSTOM_AUDIENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CUSTOM_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724002
    :cond_4
    const-string v0, "LOOKALIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOOKALIKE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724004
    :cond_5
    const-string v0, "FANS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724005
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724006
    :cond_6
    const-string v0, "LOCAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724007
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOCAL:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724008
    :cond_7
    const-string v0, "IG_PROMOTED_POST_AUTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->IG_PROMOTED_POST_AUTO:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724010
    :cond_8
    const-string v0, "SAVED_AUDIENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724012
    :cond_9
    const-string v0, "EVENT_ENGAGEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->EVENT_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724014
    :cond_a
    const-string v0, "DISTRICT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->DISTRICT:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724016
    :cond_b
    const-string v0, "CREATE_NEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 724017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->CREATE_NEW:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    .line 724018
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .locals 1

    .prologue
    .line 724019
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .locals 1

    .prologue
    .line 724020
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    return-object v0
.end method
