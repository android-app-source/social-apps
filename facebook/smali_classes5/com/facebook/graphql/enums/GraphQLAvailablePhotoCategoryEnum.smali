.class public final enum Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum FOOD:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum INDOOR:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum PEOPLE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum PLACE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723510
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723511
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->ALL:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723512
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "FOOD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->FOOD:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723513
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PLACE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723514
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "PEOPLE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723515
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const-string v1, "INDOOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->INDOOR:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723516
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->ALL:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->FOOD:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PLACE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->INDOOR:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;
    .locals 1

    .prologue
    .line 723520
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    .line 723521
    :goto_0
    return-object v0

    .line 723522
    :cond_1
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723523
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->ALL:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0

    .line 723524
    :cond_2
    const-string v0, "FOOD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->FOOD:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0

    .line 723526
    :cond_3
    const-string v0, "PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PLACE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0

    .line 723528
    :cond_4
    const-string v0, "PEOPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0

    .line 723530
    :cond_5
    const-string v0, "INDOOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->INDOOR:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0

    .line 723532
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;
    .locals 1

    .prologue
    .line 723518
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;
    .locals 1

    .prologue
    .line 723517
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    return-object v0
.end method
