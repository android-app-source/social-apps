.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

.field public static final enum CHECKOUT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

.field public static final enum IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727753
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    .line 727754
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    const-string v1, "CHECKOUT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    .line 727755
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    const-string v1, "SAVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    .line 727756
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    .line 727757
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727758
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;
    .locals 1

    .prologue
    .line 727759
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    .line 727760
    :goto_0
    return-object v0

    .line 727761
    :cond_1
    const-string v0, "CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727762
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    goto :goto_0

    .line 727763
    :cond_2
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727764
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    goto :goto_0

    .line 727765
    :cond_3
    const-string v0, "IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    goto :goto_0

    .line 727767
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;
    .locals 1

    .prologue
    .line 727768
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;
    .locals 1

    .prologue
    .line 727769
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    return-object v0
.end method
