.class public final enum Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum AUTO_EXPAND:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum COMMENT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum EVENT_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum FEED_STORY_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum FOLLOW:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum GROUP_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum GROUP_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum GROUP_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum HOVER:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum OUTBOUND_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum PHOTO_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum PIVOT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum SALE_POST_CREATION:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum SEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum TIMELINE_PROFILE_PICTURE_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public static final enum VIDEO_PLAY:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726610
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726611
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726612
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "PAGE_LIKE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726613
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726614
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "OUTBOUND_CLICK"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->OUTBOUND_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "SEND_REQUEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "SHARE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "VIDEO_PLAY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->VIDEO_PLAY:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "PIVOT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "PHOTO_CLICK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PHOTO_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "HOVER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->HOVER:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726621
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "AUTO_EXPAND"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->AUTO_EXPAND:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726622
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "EVENT_JOIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726623
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "GROUP_LIKE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726624
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "GROUP_JOIN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726625
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "GROUP_VIEW_PERMALINK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "SAVE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726627
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "TIMELINE_PROFILE_PICTURE_CLICK"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->TIMELINE_PROFILE_PICTURE_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726628
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "FEED_STORY_PERMALINK"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FEED_STORY_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726629
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "EVENT_VIEW_PERMALINK"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726630
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "FOLLOW"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726631
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    const-string v1, "SALE_POST_CREATION"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SALE_POST_CREATION:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726632
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->OUTBOUND_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->VIDEO_PLAY:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PHOTO_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->HOVER:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->AUTO_EXPAND:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->TIMELINE_PROFILE_PICTURE_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FEED_STORY_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SALE_POST_CREATION:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726609
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;
    .locals 1

    .prologue
    .line 726562
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 726563
    :goto_0
    return-object v0

    .line 726564
    :cond_1
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726566
    :cond_2
    const-string v0, "PAGE_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726568
    :cond_3
    const-string v0, "COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726570
    :cond_4
    const-string v0, "OUTBOUND_CLICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726571
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->OUTBOUND_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726572
    :cond_5
    const-string v0, "SEND_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726573
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726574
    :cond_6
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726575
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726576
    :cond_7
    const-string v0, "VIDEO_PLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->VIDEO_PLAY:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726578
    :cond_8
    const-string v0, "PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 726579
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PIVOT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726580
    :cond_9
    const-string v0, "PHOTO_CLICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726581
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PHOTO_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726582
    :cond_a
    const-string v0, "HOVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 726583
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->HOVER:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726584
    :cond_b
    const-string v0, "AUTO_EXPAND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 726585
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->AUTO_EXPAND:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto :goto_0

    .line 726586
    :cond_c
    const-string v0, "EVENT_JOIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726588
    :cond_d
    const-string v0, "EVENT_VIEW_PERMALINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 726589
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726590
    :cond_e
    const-string v0, "GROUP_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 726591
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726592
    :cond_f
    const-string v0, "GROUP_JOIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 726593
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_JOIN:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726594
    :cond_10
    const-string v0, "GROUP_VIEW_PERMALINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 726595
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->GROUP_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726596
    :cond_11
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 726597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726598
    :cond_12
    const-string v0, "TIMELINE_PROFILE_PICTURE_CLICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 726599
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->TIMELINE_PROFILE_PICTURE_CLICK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726600
    :cond_13
    const-string v0, "FEED_STORY_PERMALINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 726601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FEED_STORY_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726602
    :cond_14
    const-string v0, "FOLLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 726603
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726604
    :cond_15
    const-string v0, "SALE_POST_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 726605
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SALE_POST_CREATION:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0

    .line 726606
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;
    .locals 1

    .prologue
    .line 726608
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;
    .locals 1

    .prologue
    .line 726607
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    return-object v0
.end method
