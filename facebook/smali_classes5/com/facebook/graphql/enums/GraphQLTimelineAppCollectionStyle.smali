.class public final enum Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum FEED:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum REVIEW_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740595
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740596
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740597
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "REVIEW_LIST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->REVIEW_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740598
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "CONTACT_LIST"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740599
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "GRID"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740600
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "PHOTOS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740601
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "MAP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740602
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "ABOUT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740603
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "NOTES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740604
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    const-string v1, "FEED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->FEED:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740605
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->REVIEW_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->FEED:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740606
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 740607
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 740608
    :goto_0
    return-object v0

    .line 740609
    :cond_1
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740610
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740611
    :cond_2
    const-string v0, "REVIEW_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740612
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->REVIEW_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740613
    :cond_3
    const-string v0, "CONTACT_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740614
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740615
    :cond_4
    const-string v0, "GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740616
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740617
    :cond_5
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740618
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740619
    :cond_6
    const-string v0, "MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740620
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740621
    :cond_7
    const-string v0, "ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740622
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740623
    :cond_8
    const-string v0, "NOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740624
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740625
    :cond_9
    const-string v0, "FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740626
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->FEED:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0

    .line 740627
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 740628
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 740629
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-object v0
.end method
