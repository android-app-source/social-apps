.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum ADMIN_MENU_TEST_LINK:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum CITY_HUB_CATEGORY_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum CITY_HUB_LOCAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum CITY_HUB_PYML_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum CITY_HUB_SOCIAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum COVER_PHOTO_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum CTA_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum DYNAMIC_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum FEED_STORY_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum FEED_STORY_SEARCH:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGES_ACTIONS_UNIT_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGES_ACTION_BAR_CHANNEL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGES_EDIT_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGES_SERVICES_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGE_PLUGIN_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PAGE_PRESENCE_LHS_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum PROSERVICES_SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum UNOWNED_PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 730159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "COVER_PHOTO_SURFACE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->COVER_PHOTO_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "HOVER_CARD_SURFACE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGE_PLUGIN_SURFACE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PLUGIN_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "ADMIN_MENU_TEST_LINK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->ADMIN_MENU_TEST_LINK:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGE_PRESENCE_LHS_CARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PRESENCE_LHS_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "SEARCH_ENTITY_CARD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PROSERVICES_SEARCH_ENTITY_CARD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PROSERVICES_SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730169
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "FEED_STORY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730170
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "FEED_STORY_SEARCH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_SEARCH:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730171
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGES_ACTIONS_UNIT_SURFACE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTIONS_UNIT_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGES_SERVICES_SURFACE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_SERVICES_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "CTA_HOVER_CARD_SURFACE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CTA_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "DYNAMIC_HOVER_CARD_SURFACE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->DYNAMIC_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730175
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGES_EDIT_PAGE_SURFACE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_EDIT_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730176
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "CITY_HUB_SOCIAL_MODULE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_SOCIAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730177
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "CITY_HUB_LOCAL_MODULE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_LOCAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730178
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "CITY_HUB_CATEGORY_MODULE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_CATEGORY_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730179
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "CITY_HUB_PYML_MODULE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_PYML_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730180
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "UNOWNED_PAGE_COVER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNOWNED_PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730181
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "PAGES_ACTION_BAR_CHANNEL"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTION_BAR_CHANNEL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730182
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "DEPRECATED_23"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730183
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    const-string v1, "FEED_STORY_ATTACHMENT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730184
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->COVER_PHOTO_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PLUGIN_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->ADMIN_MENU_TEST_LINK:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PRESENCE_LHS_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PROSERVICES_SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_SEARCH:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTIONS_UNIT_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_SERVICES_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CTA_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->DYNAMIC_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_EDIT_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_SOCIAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_LOCAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_CATEGORY_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_PYML_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNOWNED_PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTION_BAR_CHANNEL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;
    .locals 1

    .prologue
    .line 730110
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 730111
    :goto_0
    return-object v0

    .line 730112
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730113
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730114
    :cond_2
    const-string v0, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730115
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730116
    :cond_3
    const-string v0, "COVER_PHOTO_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 730117
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->COVER_PHOTO_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730118
    :cond_4
    const-string v0, "HOVER_CARD_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 730119
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730120
    :cond_5
    const-string v0, "PAGE_PLUGIN_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 730121
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PLUGIN_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730122
    :cond_6
    const-string v0, "ADMIN_MENU_TEST_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 730123
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->ADMIN_MENU_TEST_LINK:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730124
    :cond_7
    const-string v0, "PAGE_PRESENCE_LHS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 730125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGE_PRESENCE_LHS_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730126
    :cond_8
    const-string v0, "SEARCH_ENTITY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 730127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730128
    :cond_9
    const-string v0, "PROSERVICES_SEARCH_ENTITY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 730129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PROSERVICES_SEARCH_ENTITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730130
    :cond_a
    const-string v0, "FEED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 730131
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730132
    :cond_b
    const-string v0, "FEED_STORY_SEARCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 730133
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_SEARCH:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto :goto_0

    .line 730134
    :cond_c
    const-string v0, "FEED_STORY_ATTACHMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 730135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->FEED_STORY_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730136
    :cond_d
    const-string v0, "PAGES_ACTIONS_UNIT_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 730137
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTIONS_UNIT_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730138
    :cond_e
    const-string v0, "PAGES_ACTION_BAR_CHANNEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 730139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_ACTION_BAR_CHANNEL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730140
    :cond_f
    const-string v0, "PAGES_SERVICES_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 730141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_SERVICES_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730142
    :cond_10
    const-string v0, "CTA_HOVER_CARD_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 730143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CTA_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730144
    :cond_11
    const-string v0, "DYNAMIC_HOVER_CARD_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 730145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->DYNAMIC_HOVER_CARD_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730146
    :cond_12
    const-string v0, "PAGES_EDIT_PAGE_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 730147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_EDIT_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730148
    :cond_13
    const-string v0, "CITY_HUB_SOCIAL_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 730149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_SOCIAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730150
    :cond_14
    const-string v0, "CITY_HUB_LOCAL_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 730151
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_LOCAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730152
    :cond_15
    const-string v0, "CITY_HUB_CATEGORY_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 730153
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_CATEGORY_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730154
    :cond_16
    const-string v0, "CITY_HUB_PYML_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 730155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_PYML_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730156
    :cond_17
    const-string v0, "UNOWNED_PAGE_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 730157
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNOWNED_PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0

    .line 730158
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;
    .locals 1

    .prologue
    .line 730109
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;
    .locals 1

    .prologue
    .line 730108
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    return-object v0
.end method
