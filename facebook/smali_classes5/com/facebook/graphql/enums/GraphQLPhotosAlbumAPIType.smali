.class public final enum Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum FRIENDS_WALLS:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public static final enum WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "COVER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738121
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "WALL"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738123
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "FRIENDS_WALLS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->FRIENDS_WALLS:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "NORMAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "APP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "SHARED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const-string v1, "OTHER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738128
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->FRIENDS_WALLS:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 1

    .prologue
    .line 738130
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 738131
    :goto_0
    return-object v0

    .line 738132
    :cond_1
    const-string v0, "PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738133
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738134
    :cond_2
    const-string v0, "COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738136
    :cond_3
    const-string v0, "MOBILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738137
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738138
    :cond_4
    const-string v0, "WALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738140
    :cond_5
    const-string v0, "FRIENDS_WALLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->FRIENDS_WALLS:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738142
    :cond_6
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738144
    :cond_7
    const-string v0, "APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738146
    :cond_8
    const-string v0, "SHARED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738148
    :cond_9
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0

    .line 738150
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 1

    .prologue
    .line 738151
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 1

    .prologue
    .line 738152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method
