.class public final enum Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

.field public static final enum DESCRUCTIVE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

.field public static final enum PRIMARY:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 737799
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 737800
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    const-string v1, "DESCRUCTIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->DESCRUCTIVE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 737801
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->PRIMARY:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 737802
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->DESCRUCTIVE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->PRIMARY:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
    .locals 1

    .prologue
    .line 737804
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 737805
    :goto_0
    return-object v0

    .line 737806
    :cond_1
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    goto :goto_0

    .line 737808
    :cond_2
    const-string v0, "DESCRUCTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737809
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->DESCRUCTIVE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    goto :goto_0

    .line 737810
    :cond_3
    const-string v0, "PRIMARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737811
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->PRIMARY:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    goto :goto_0

    .line 737812
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
    .locals 1

    .prologue
    .line 737813
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
    .locals 1

    .prologue
    .line 737814
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    return-object v0
.end method
