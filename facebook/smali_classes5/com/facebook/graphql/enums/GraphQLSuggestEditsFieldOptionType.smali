.class public final enum Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum LOCATED_INSIDE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740428
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740429
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "HAS_VALUE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740430
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "DOESNT_HAVE_VALUE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740431
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "ADD_VALUE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740432
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "LOCATED_INSIDE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->LOCATED_INSIDE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740433
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "ALWAYS_OPEN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740434
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const-string v1, "PERMANENTLY_CLOSED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740435
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->LOCATED_INSIDE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;
    .locals 1

    .prologue
    .line 740437
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 740438
    :goto_0
    return-object v0

    .line 740439
    :cond_1
    const-string v0, "HAS_VALUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740441
    :cond_2
    const-string v0, "DOESNT_HAVE_VALUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740443
    :cond_3
    const-string v0, "ADD_VALUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740445
    :cond_4
    const-string v0, "LOCATED_INSIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->LOCATED_INSIDE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740447
    :cond_5
    const-string v0, "ALWAYS_OPEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740448
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740449
    :cond_6
    const-string v0, "PERMANENTLY_CLOSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0

    .line 740451
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;
    .locals 1

    .prologue
    .line 740452
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;
    .locals 1

    .prologue
    .line 740453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    return-object v0
.end method
