.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public static final enum INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public static final enum MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public static final enum SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const-string v1, "INLINE_SELECT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const-string v1, "MESSENGER_CHECKBOX"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728004
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 1

    .prologue
    .line 728006
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 728007
    :goto_0
    return-object v0

    .line 728008
    :cond_1
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    goto :goto_0

    .line 728010
    :cond_2
    const-string v0, "INLINE_SELECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    goto :goto_0

    .line 728012
    :cond_3
    const-string v0, "SELECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    goto :goto_0

    .line 728014
    :cond_4
    const-string v0, "MESSENGER_CHECKBOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    goto :goto_0

    .line 728016
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 1

    .prologue
    .line 728017
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 1

    .prologue
    .line 728005
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-object v0
.end method
