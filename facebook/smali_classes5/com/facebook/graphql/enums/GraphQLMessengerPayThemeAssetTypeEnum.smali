.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum CHAT_BUBBLE_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum FULLSCREEN_BOTTOM:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum FULLSCREEN_CARD_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum FULLSCREEN_TOP:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum REQUEST_PAY_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum SEND_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum THUMBNAIL:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729283
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729284
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "CHAT_BUBBLE_VIEW"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->CHAT_BUBBLE_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729285
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "REQUEST_PAY_VIEW"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->REQUEST_PAY_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729286
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "SEND_VIEW"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->SEND_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729287
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "FULLSCREEN_CARD_VIEW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_CARD_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729288
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "FULLSCREEN_TOP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_TOP:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729289
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "FULLSCREEN_BOTTOM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_BOTTOM:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729290
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    const-string v1, "THUMBNAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->THUMBNAIL:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729291
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->CHAT_BUBBLE_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->REQUEST_PAY_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->SEND_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_CARD_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_TOP:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_BOTTOM:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->THUMBNAIL:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
    .locals 1

    .prologue
    .line 729293
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    .line 729294
    :goto_0
    return-object v0

    .line 729295
    :cond_1
    const-string v0, "THUMBNAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->THUMBNAIL:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729297
    :cond_2
    const-string v0, "CHAT_BUBBLE_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->CHAT_BUBBLE_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729299
    :cond_3
    const-string v0, "REQUEST_PAY_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->REQUEST_PAY_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729301
    :cond_4
    const-string v0, "SEND_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->SEND_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729303
    :cond_5
    const-string v0, "FULLSCREEN_CARD_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_CARD_VIEW:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729305
    :cond_6
    const-string v0, "FULLSCREEN_TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_TOP:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729307
    :cond_7
    const-string v0, "FULLSCREEN_BOTTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 729308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->FULLSCREEN_BOTTOM:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0

    .line 729309
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
    .locals 1

    .prologue
    .line 729310
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;
    .locals 1

    .prologue
    .line 729311
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerPayThemeAssetTypeEnum;

    return-object v0
.end method
