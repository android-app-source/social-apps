.class public final enum Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public static final enum COMPOSER:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public static final enum FRAMES:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public static final enum LIVE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public static final enum PROFILE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738679
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738680
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738681
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738682
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const-string v1, "FRAMES"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->FRAMES:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738683
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->LIVE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738684
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->FRAMES:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->LIVE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738665
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;
    .locals 1

    .prologue
    .line 738668
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    .line 738669
    :goto_0
    return-object v0

    .line 738670
    :cond_1
    const-string v0, "COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738671
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    goto :goto_0

    .line 738672
    :cond_2
    const-string v0, "PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738673
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    goto :goto_0

    .line 738674
    :cond_3
    const-string v0, "FRAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738675
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->FRAMES:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    goto :goto_0

    .line 738676
    :cond_4
    const-string v0, "LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738677
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->LIVE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    goto :goto_0

    .line 738678
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;
    .locals 1

    .prologue
    .line 738667
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;
    .locals 1

    .prologue
    .line 738666
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    return-object v0
.end method
