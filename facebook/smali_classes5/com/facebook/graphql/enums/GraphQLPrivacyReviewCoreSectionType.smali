.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

.field public static final enum SECTION_DATA_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

.field public static final enum SECTION_EXPLANATION_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

.field public static final enum SECTION_HEADER_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 738385
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    const-string v1, "SECTION_HEADER_ITEM_TYPE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_HEADER_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 738386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    const-string v1, "SECTION_DATA_ITEM_TYPE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_DATA_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 738387
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    const-string v1, "SECTION_EXPLANATION_ITEM_TYPE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_EXPLANATION_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 738388
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_HEADER_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_DATA_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_EXPLANATION_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
    .locals 1

    .prologue
    .line 738390
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 738391
    :goto_0
    return-object v0

    .line 738392
    :cond_1
    const-string v0, "SECTION_HEADER_ITEM_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_HEADER_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    goto :goto_0

    .line 738394
    :cond_2
    const-string v0, "SECTION_DATA_ITEM_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_DATA_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    goto :goto_0

    .line 738396
    :cond_3
    const-string v0, "SECTION_EXPLANATION_ITEM_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738397
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->SECTION_EXPLANATION_ITEM_TYPE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    goto :goto_0

    .line 738398
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
    .locals 1

    .prologue
    .line 738399
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
    .locals 1

    .prologue
    .line 738400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    return-object v0
.end method
