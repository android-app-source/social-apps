.class public final enum Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum DOCUMENT_MARGIN:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum MEDIUM:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740514
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740515
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740516
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740517
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740518
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740519
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "EXTRA_LARGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740520
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "DOCUMENT_MARGIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->DOCUMENT_MARGIN:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740521
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    const-string v1, "NONE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->NONE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740522
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->DOCUMENT_MARGIN:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->NONE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740525
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;
    .locals 1

    .prologue
    .line 740526
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 740527
    :goto_0
    return-object v0

    .line 740528
    :cond_1
    const-string v0, "EXTRA_SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740530
    :cond_2
    const-string v0, "SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->SMALL:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740532
    :cond_3
    const-string v0, "MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740533
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740534
    :cond_4
    const-string v0, "LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740535
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740536
    :cond_5
    const-string v0, "EXTRA_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740538
    :cond_6
    const-string v0, "DOCUMENT_MARGIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->DOCUMENT_MARGIN:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740540
    :cond_7
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->NONE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0

    .line 740542
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;
    .locals 1

    .prologue
    .line 740524
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;
    .locals 1

    .prologue
    .line 740523
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    return-object v0
.end method
