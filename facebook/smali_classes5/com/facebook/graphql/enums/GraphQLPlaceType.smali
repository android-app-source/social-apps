.class public final enum Lcom/facebook/graphql/enums/GraphQLPlaceType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPlaceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum COUNTRY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum STATE_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738283
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738284
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738285
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->CITY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738286
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "STATE_PROVINCE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->STATE_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738287
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "COUNTRY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738288
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738289
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "RESIDENCE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738290
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const-string v1, "TEXT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->TEXT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738291
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->CITY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->STATE_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;->TEXT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 1

    .prologue
    .line 738266
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 738267
    :goto_0
    return-object v0

    .line 738268
    :cond_1
    const-string v0, "PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738270
    :cond_2
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->CITY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738272
    :cond_3
    const-string v0, "STATE_PROVINCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->STATE_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738274
    :cond_4
    const-string v0, "COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738276
    :cond_5
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738278
    :cond_6
    const-string v0, "RESIDENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738280
    :cond_7
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->TEXT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0

    .line 738282
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 1

    .prologue
    .line 738265
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 1

    .prologue
    .line 738264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method
