.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public static final enum LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public static final enum LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public static final enum NO_LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725241
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 725242
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    const-string v1, "NO_LOOPING"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->NO_LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 725243
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    const-string v1, "LOOPING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 725244
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    const-string v1, "LOOPING_WITH_CROSS_FADE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 725245
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->NO_LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1

    .prologue
    .line 725232
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 725233
    :goto_0
    return-object v0

    .line 725234
    :cond_1
    const-string v0, "NO_LOOPING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->NO_LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    goto :goto_0

    .line 725236
    :cond_2
    const-string v0, "LOOPING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    goto :goto_0

    .line 725238
    :cond_3
    const-string v0, "LOOPING_WITH_CROSS_FADE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    goto :goto_0

    .line 725240
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1

    .prologue
    .line 725230
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1

    .prologue
    .line 725231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    return-object v0
.end method
