.class public final enum Lcom/facebook/graphql/enums/GraphQLDisplayStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDisplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

.field public static final enum BLOCK:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

.field public static final enum INLINE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    .line 724948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    const-string v1, "INLINE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    .line 724949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    const-string v1, "BLOCK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->BLOCK:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    .line 724950
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->BLOCK:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724951
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDisplayStyle;
    .locals 1

    .prologue
    .line 724940
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    .line 724941
    :goto_0
    return-object v0

    .line 724942
    :cond_1
    const-string v0, "INLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724943
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    goto :goto_0

    .line 724944
    :cond_2
    const-string v0, "BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724945
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->BLOCK:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    goto :goto_0

    .line 724946
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDisplayStyle;
    .locals 1

    .prologue
    .line 724939
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDisplayStyle;
    .locals 1

    .prologue
    .line 724938
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    return-object v0
.end method
