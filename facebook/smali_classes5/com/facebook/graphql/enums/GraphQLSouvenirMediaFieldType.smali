.class public final enum Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public static final enum BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public static final enum BURST_VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 740095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const-string v1, "BURST"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    const-string v1, "BURST_VIDEO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST_VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740100
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST_VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .locals 1

    .prologue
    .line 740103
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    .line 740104
    :goto_0
    return-object v0

    .line 740105
    :cond_1
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740106
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    goto :goto_0

    .line 740107
    :cond_2
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740108
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    goto :goto_0

    .line 740109
    :cond_3
    const-string v0, "BURST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740110
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    goto :goto_0

    .line 740111
    :cond_4
    const-string v0, "BURST_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740112
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST_VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    goto :goto_0

    .line 740113
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .locals 1

    .prologue
    .line 740102
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;
    .locals 1

    .prologue
    .line 740101
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    return-object v0
.end method
