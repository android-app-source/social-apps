.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

.field public static final enum HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

.field public static final enum HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

.field public static final enum ROW:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 727561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    const-string v1, "ROW"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->ROW:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 727562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    const-string v1, "HONEYBEE_COMPRESSED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 727563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    const-string v1, "HONEYBEE_FULL_WIDTH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 727564
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->ROW:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727565
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .locals 1

    .prologue
    .line 727566
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 727567
    :goto_0
    return-object v0

    .line 727568
    :cond_1
    const-string v0, "ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->ROW:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    goto :goto_0

    .line 727570
    :cond_2
    const-string v0, "HONEYBEE_COMPRESSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727571
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    goto :goto_0

    .line 727572
    :cond_3
    const-string v0, "HONEYBEE_FULL_WIDTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727573
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    goto :goto_0

    .line 727574
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .locals 1

    .prologue
    .line 727575
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .locals 1

    .prologue
    .line 727576
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    return-object v0
.end method
