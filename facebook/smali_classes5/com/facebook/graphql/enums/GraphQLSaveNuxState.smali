.class public final enum Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSaveNuxState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

.field public static final enum ELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

.field public static final enum FORCE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

.field public static final enum INELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739813
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 739814
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    const-string v1, "INELIGIBLE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->INELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 739815
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    const-string v1, "ELIGIBLE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->ELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 739816
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    const-string v1, "FORCE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->FORCE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 739817
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->INELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->ELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->FORCE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739829
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
    .locals 1

    .prologue
    .line 739820
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 739821
    :goto_0
    return-object v0

    .line 739822
    :cond_1
    const-string v0, "INELIGIBLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739823
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->INELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    goto :goto_0

    .line 739824
    :cond_2
    const-string v0, "ELIGIBLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739825
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->ELIGIBLE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    goto :goto_0

    .line 739826
    :cond_3
    const-string v0, "FORCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739827
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->FORCE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    goto :goto_0

    .line 739828
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
    .locals 1

    .prologue
    .line 739819
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
    .locals 1

    .prologue
    .line 739818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    return-object v0
.end method
