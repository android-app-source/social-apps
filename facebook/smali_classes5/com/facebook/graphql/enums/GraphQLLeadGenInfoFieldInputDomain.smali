.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field public static final enum ZIPCODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "ZIPCODE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "COUNTRY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "CITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "STATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    const-string v1, "PROVINCE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727980
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727971
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;
    .locals 1

    .prologue
    .line 727981
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 727982
    :goto_0
    return-object v0

    .line 727983
    :cond_1
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727984
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727985
    :cond_2
    const-string v0, "PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727986
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727987
    :cond_3
    const-string v0, "ZIPCODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727988
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727989
    :cond_4
    const-string v0, "COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727991
    :cond_5
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727992
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727993
    :cond_6
    const-string v0, "STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727994
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727995
    :cond_7
    const-string v0, "PROVINCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727996
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0

    .line 727997
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;
    .locals 1

    .prologue
    .line 727969
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;
    .locals 1

    .prologue
    .line 727970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    return-object v0
.end method
