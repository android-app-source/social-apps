.class public final enum Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

.field public static final enum CREDIT_CARD:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

.field public static final enum PAYPAL:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    .line 729382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    const-string v1, "CREDIT_CARD"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->CREDIT_CARD:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    .line 729383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    const-string v1, "PAYPAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->PAYPAL:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    .line 729384
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->CREDIT_CARD:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->PAYPAL:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;
    .locals 1

    .prologue
    .line 729371
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    .line 729372
    :goto_0
    return-object v0

    .line 729373
    :cond_1
    const-string v0, "CREDIT_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729374
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->CREDIT_CARD:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    goto :goto_0

    .line 729375
    :cond_2
    const-string v0, "PAYPAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729376
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->PAYPAL:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    goto :goto_0

    .line 729377
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;
    .locals 1

    .prologue
    .line 729379
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;
    .locals 1

    .prologue
    .line 729378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    return-object v0
.end method
