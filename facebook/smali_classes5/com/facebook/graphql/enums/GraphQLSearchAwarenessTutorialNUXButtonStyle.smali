.class public final enum Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

.field public static final enum DEFAULT:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

.field public static final enum HIDDEN_ON_FIRST_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

.field public static final enum NEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 739999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 740000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    const-string v1, "NEXT_BUTTON"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->NEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 740001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    const-string v1, "HIDDEN_ON_FIRST_IMPRESSION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->HIDDEN_ON_FIRST_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 740002
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->NEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->HIDDEN_ON_FIRST_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739997
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
    .locals 1

    .prologue
    .line 739988
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 739989
    :goto_0
    return-object v0

    .line 739990
    :cond_1
    const-string v0, "DEFAULT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739991
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    goto :goto_0

    .line 739992
    :cond_2
    const-string v0, "NEXT_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739993
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->NEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    goto :goto_0

    .line 739994
    :cond_3
    const-string v0, "HIDDEN_ON_FIRST_IMPRESSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->HIDDEN_ON_FIRST_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    goto :goto_0

    .line 739996
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
    .locals 1

    .prologue
    .line 739986
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
    .locals 1

    .prologue
    .line 739987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    return-object v0
.end method
