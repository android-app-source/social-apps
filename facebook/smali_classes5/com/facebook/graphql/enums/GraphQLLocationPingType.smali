.class public final enum Lcom/facebook/graphql/enums/GraphQLLocationPingType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLocationPingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationPingType;

.field public static final enum DURATION:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

.field public static final enum FOREVER:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728249
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    .line 728250
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    const-string v1, "DURATION"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->DURATION:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    .line 728251
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    const-string v1, "FOREVER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->FOREVER:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    .line 728252
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->DURATION:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->FOREVER:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728239
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationPingType;
    .locals 1

    .prologue
    .line 728240
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    .line 728241
    :goto_0
    return-object v0

    .line 728242
    :cond_1
    const-string v0, "DURATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->DURATION:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    goto :goto_0

    .line 728244
    :cond_2
    const-string v0, "FOREVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->FOREVER:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    goto :goto_0

    .line 728246
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationPingType;
    .locals 1

    .prologue
    .line 728247
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLocationPingType;
    .locals 1

    .prologue
    .line 728248
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    return-object v0
.end method
