.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH_AND_DAY:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH_AND_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH_DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH_DAY_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum MONTH_DAY_YEAR_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

.field public static final enum YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH_AND_DAY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_DAY:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH_AND_YEAR"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH_DAY_YEAR"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "YEAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH_DAY_YEAR_TIME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 725000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "DAY_TIME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 725001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 725002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    const-string v1, "MONTH_DAY_TIME"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 725003
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_DAY:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724992
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;
    .locals 1

    .prologue
    .line 724969
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    .line 724970
    :goto_0
    return-object v0

    .line 724971
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724972
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724973
    :cond_2
    const-string v0, "MONTH_AND_DAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724974
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_DAY:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724975
    :cond_3
    const-string v0, "MONTH_AND_YEAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724976
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_AND_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724977
    :cond_4
    const-string v0, "MONTH_DAY_YEAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724978
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724979
    :cond_5
    const-string v0, "YEAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724980
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->YEAR:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724981
    :cond_6
    const-string v0, "MONTH_DAY_YEAR_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724982
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_YEAR_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724983
    :cond_7
    const-string v0, "DAY_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724984
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724985
    :cond_8
    const-string v0, "MONTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724986
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724987
    :cond_9
    const-string v0, "MONTH_DAY_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724988
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->MONTH_DAY_TIME:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0

    .line 724989
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;
    .locals 1

    .prologue
    .line 724991
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;
    .locals 1

    .prologue
    .line 724990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentDateStyle;

    return-object v0
.end method
