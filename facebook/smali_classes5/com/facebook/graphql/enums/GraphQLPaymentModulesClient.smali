.class public final enum Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

.field public static final enum MOCK:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

.field public static final enum PAGES_COMMERCE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737825
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 737826
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    const-string v1, "MOCK"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->MOCK:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 737827
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    const-string v1, "PAGES_COMMERCE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->PAGES_COMMERCE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 737828
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->MOCK:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->PAGES_COMMERCE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737824
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;
    .locals 1

    .prologue
    .line 737815
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 737816
    :goto_0
    return-object v0

    .line 737817
    :cond_1
    const-string v0, "PAGES_COMMERCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->PAGES_COMMERCE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    goto :goto_0

    .line 737819
    :cond_2
    const-string v0, "MOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->MOCK:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    goto :goto_0

    .line 737821
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;
    .locals 1

    .prologue
    .line 737823
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;
    .locals 1

    .prologue
    .line 737822
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    return-object v0
.end method
