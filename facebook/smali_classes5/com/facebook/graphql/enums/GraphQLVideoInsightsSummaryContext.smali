.class public final enum Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum BROADCAST_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum MINUTES_VIEWED:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum PEAK_LIVE_VIEWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum PEAK_VIEW_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum REVENUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum TOTAL_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum TOTAL_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum VIDEO_POSTS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

.field public static final enum VIDEO_VIEWS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 741194
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741195
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "VIDEO_POSTS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_POSTS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741196
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "VIDEO_VIEWS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_VIEWS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741197
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "MINUTES_VIEWED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->MINUTES_VIEWED:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741198
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "TOTAL_ENGAGEMENT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741199
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "TOTAL_FOLLOWERS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741200
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "PEAK_LIVE_VIEWERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_LIVE_VIEWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741201
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "BROADCAST_TIME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->BROADCAST_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741202
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "PEAK_VIEW_TIME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_VIEW_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    const-string v1, "REVENUE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->REVENUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741204
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_POSTS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_VIEWS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->MINUTES_VIEWED:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_LIVE_VIEWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->BROADCAST_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_VIEW_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->REVENUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741170
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;
    .locals 1

    .prologue
    .line 741173
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    .line 741174
    :goto_0
    return-object v0

    .line 741175
    :cond_1
    const-string v0, "VIDEO_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_POSTS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741177
    :cond_2
    const-string v0, "VIDEO_VIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741178
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_VIEWS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741179
    :cond_3
    const-string v0, "MINUTES_VIEWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741180
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->MINUTES_VIEWED:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741181
    :cond_4
    const-string v0, "TOTAL_ENGAGEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 741182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741183
    :cond_5
    const-string v0, "TOTAL_FOLLOWERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 741184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741185
    :cond_6
    const-string v0, "PEAK_LIVE_VIEWERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_LIVE_VIEWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741187
    :cond_7
    const-string v0, "BROADCAST_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 741188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->BROADCAST_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741189
    :cond_8
    const-string v0, "PEAK_VIEW_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_VIEW_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741191
    :cond_9
    const-string v0, "REVENUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 741192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->REVENUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0

    .line 741193
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;
    .locals 1

    .prologue
    .line 741172
    const-class v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;
    .locals 1

    .prologue
    .line 741171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    return-object v0
.end method
