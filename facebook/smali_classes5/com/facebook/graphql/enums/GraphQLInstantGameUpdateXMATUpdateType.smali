.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

.field public static final enum GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    .line 727616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    .line 727617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    const-string v1, "GAME_SCORE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    .line 727618
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727614
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;
    .locals 1

    .prologue
    .line 727607
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    .line 727608
    :goto_0
    return-object v0

    .line 727609
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727610
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    goto :goto_0

    .line 727611
    :cond_2
    const-string v0, "GAME_SCORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727612
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    goto :goto_0

    .line 727613
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;
    .locals 1

    .prologue
    .line 727605
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;
    .locals 1

    .prologue
    .line 727606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    return-object v0
.end method
