.class public final enum Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

.field public static final enum SEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

.field public static final enum UNSEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729576
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 729577
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 729578
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    const-string v1, "SEEN_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->SEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 729579
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    const-string v1, "UNSEEN_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 729580
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->SEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729581
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;
    .locals 1

    .prologue
    .line 729582
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 729583
    :goto_0
    return-object v0

    .line 729584
    :cond_1
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729585
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    goto :goto_0

    .line 729586
    :cond_2
    const-string v0, "SEEN_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->SEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    goto :goto_0

    .line 729588
    :cond_3
    const-string v0, "UNSEEN_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729589
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSEEN_ONLY:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    goto :goto_0

    .line 729590
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;
    .locals 1

    .prologue
    .line 729591
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;
    .locals 1

    .prologue
    .line 729592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    return-object v0
.end method
