.class public final enum Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_NUX:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_PUSH_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_PUSH_FAIL_CARD_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum R_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737946
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_VERIFICATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_VERIFICATION_PROCESSING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_MANUAL_REVIEW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_SENDER_RISK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_RECIPIENT_RISK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_DECLINED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_EXPIRED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_SAME_CARD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_CUSTOMER_SERVICE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_CHARGEBACK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_CANCELED_SYSTEM_FAIL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_COMPLETED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_NUX"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_NUX:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_PROCESSING"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_PUSH_FAIL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    const-string v1, "R_PENDING_PUSH_FAIL_CARD_EXPIRED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL_CARD_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737965
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_NUX:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL_CARD_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737945
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .locals 1

    .prologue
    .line 737904
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    .line 737905
    :goto_0
    return-object v0

    .line 737906
    :cond_1
    const-string v0, "R_PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737907
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737908
    :cond_2
    const-string v0, "R_PENDING_VERIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737909
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737910
    :cond_3
    const-string v0, "R_PENDING_VERIFICATION_PROCESSING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737911
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737912
    :cond_4
    const-string v0, "R_PENDING_MANUAL_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737913
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737914
    :cond_5
    const-string v0, "R_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737915
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737916
    :cond_6
    const-string v0, "R_CANCELED_SENDER_RISK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737917
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737918
    :cond_7
    const-string v0, "R_CANCELED_RECIPIENT_RISK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737919
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737920
    :cond_8
    const-string v0, "R_CANCELED_DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737921
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737922
    :cond_9
    const-string v0, "R_CANCELED_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 737923
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737924
    :cond_a
    const-string v0, "R_CANCELED_SAME_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 737925
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737926
    :cond_b
    const-string v0, "R_CANCELED_CUSTOMER_SERVICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 737927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto :goto_0

    .line 737928
    :cond_c
    const-string v0, "R_CANCELED_CHARGEBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 737929
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737930
    :cond_d
    const-string v0, "R_CANCELED_SYSTEM_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 737931
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737932
    :cond_e
    const-string v0, "R_COMPLETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 737933
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737934
    :cond_f
    const-string v0, "R_PENDING_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 737935
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_NUX:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737936
    :cond_10
    const-string v0, "R_PENDING_PROCESSING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 737937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737938
    :cond_11
    const-string v0, "R_PENDING_PUSH_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 737939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737940
    :cond_12
    const-string v0, "R_PENDING_PUSH_FAIL_CARD_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 737941
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->R_PENDING_PUSH_FAIL_CARD_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0

    .line 737942
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .locals 1

    .prologue
    .line 737944
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;
    .locals 1

    .prologue
    .line 737943
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    return-object v0
.end method
