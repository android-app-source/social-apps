.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum FOR_SALE_RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum MEMBER_BIO_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum OLDER_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum PINNED_POST_HEADER:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727271
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727272
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "NOTIFICATIONS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727273
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "RECENT_ACTIVITY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727274
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "OLDER_POSTS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->OLDER_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727275
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "FOR_SALE_RANKED_POSTS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->FOR_SALE_RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727276
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "DEPRECATED_5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727277
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "RANKED_POSTS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727278
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "PINNED_POST_HEADER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->PINNED_POST_HEADER:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727279
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const-string v1, "MEMBER_BIO_POSTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->MEMBER_BIO_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727280
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->OLDER_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->FOR_SALE_RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->DEPRECATED_5:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->PINNED_POST_HEADER:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->MEMBER_BIO_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727281
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;
    .locals 1

    .prologue
    .line 727282
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 727283
    :goto_0
    return-object v0

    .line 727284
    :cond_1
    const-string v0, "NOTIFICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727286
    :cond_2
    const-string v0, "RECENT_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727288
    :cond_3
    const-string v0, "OLDER_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->OLDER_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727290
    :cond_4
    const-string v0, "FOR_SALE_RANKED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->FOR_SALE_RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727292
    :cond_5
    const-string v0, "RANKED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727293
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->RANKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727294
    :cond_6
    const-string v0, "PINNED_POST_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727295
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->PINNED_POST_HEADER:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727296
    :cond_7
    const-string v0, "MEMBER_BIO_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727297
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->MEMBER_BIO_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0

    .line 727298
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;
    .locals 1

    .prologue
    .line 727299
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;
    .locals 1

    .prologue
    .line 727300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    return-object v0
.end method
