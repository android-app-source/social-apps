.class public final enum Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ADDING_PAYMENT_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum CANCEL_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_CONFIRM_CANCELLATION_SHARED_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_CONFIRM_CANCELLATION_TIME_LIMIT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_DRIVERS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_INVALID_CONTACT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_INVALID_PAYMENT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_INVALID_PICKUP_DROPOFF:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_NON_EXISTENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_NOT_AUTHORIZED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_NO_SERVICE_IN_AREA:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_PAYMENT_OUTSTANDING_BALANCE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_RIDE_TYPE_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_SAME_PICKUP_DROPOFF_LOCATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_TOO_MANY_CANCELLATIONS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_DESTINATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_DESTINATION_NOT_EDITABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_DESTINATION_NOT_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_DESTINATION_NOT_VALID:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_PAYMENT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_USER_IN_DRIVER_MODE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_USER_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum INTERNAL_SERVER_ERROR:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum PRESET_PRICE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum SURGE_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739731
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739732
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739733
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "SURGE_ACCEPTANCE_FLOW_NEEDED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SURGE_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739734
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_ACTIVE_RIDE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739735
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_NOT_AUTHORIZED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NOT_AUTHORIZED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739736
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "INTERNAL_SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->INTERNAL_SERVER_ERROR:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739737
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_NON_EXISTENT_REQUEST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NON_EXISTENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739738
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739739
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_INVALID_CONTACT_INFORMATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_CONTACT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739740
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_INVALID_PAYMENT_INFORMATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PAYMENT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739741
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_TOO_MANY_CANCELLATIONS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_TOO_MANY_CANCELLATIONS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739742
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_DRIVERS_UNAVAILABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_DRIVERS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739743
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739744
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739745
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_RIDE_TYPE_NOT_ALLOWED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_RIDE_TYPE_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739746
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_PAYMENT_OUTSTANDING_BALANCE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_PAYMENT_OUTSTANDING_BALANCE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739747
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_INVALID_PICKUP_DROPOFF"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PICKUP_DROPOFF:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739748
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "CANCEL_ACCEPTANCE_FLOW_NEEDED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->CANCEL_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739749
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_PAYMENT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PAYMENT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739750
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ADDING_PAYMENT_NEEDED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ADDING_PAYMENT_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739751
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_USER_NOT_ALLOWED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739752
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_USER_IN_DRIVER_MODE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_IN_DRIVER_MODE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739753
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_NO_SERVICE_IN_AREA"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NO_SERVICE_IN_AREA:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739754
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739755
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_SAME_PICKUP_DROPOFF_LOCATION"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_SAME_PICKUP_DROPOFF_LOCATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739756
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739757
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_CONFIRM_CANCELLATION_TIME_LIMIT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_TIME_LIMIT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739758
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_CONFIRM_CANCELLATION_SHARED_RIDE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_SHARED_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739759
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_DESTINATION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739760
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739761
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_DESTINATION_NOT_OWNER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739762
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_DESTINATION_NOT_EDITABLE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_EDITABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739763
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_UPDATE_DESTINATION_NOT_VALID"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_VALID:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739764
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739765
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    const-string v1, "PRESET_PRICE_FLOW_NEEDED"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->PRESET_PRICE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739766
    const/16 v0, 0x23

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SURGE_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NOT_AUTHORIZED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->INTERNAL_SERVER_ERROR:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NON_EXISTENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_CONTACT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PAYMENT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_TOO_MANY_CANCELLATIONS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_DRIVERS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_RIDE_TYPE_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_PAYMENT_OUTSTANDING_BALANCE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PICKUP_DROPOFF:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->CANCEL_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PAYMENT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ADDING_PAYMENT_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_IN_DRIVER_MODE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NO_SERVICE_IN_AREA:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_SAME_PICKUP_DROPOFF_LOCATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_TIME_LIMIT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_SHARED_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_EDITABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_VALID:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->PRESET_PRICE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739730
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;
    .locals 2

    .prologue
    .line 739634
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    .line 739635
    :goto_0
    return-object v0

    .line 739636
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 739637
    packed-switch v0, :pswitch_data_0

    .line 739638
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739639
    :pswitch_1
    const-string v0, "ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_NON_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739641
    :cond_2
    const-string v0, "ERROR_PAYMENT_OUTSTANDING_BALANCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739642
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_PAYMENT_OUTSTANDING_BALANCE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739643
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739644
    :pswitch_2
    const-string v0, "ERROR_UPDATE_DESTINATION_NOT_VALID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_VALID:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739646
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739647
    :pswitch_3
    const-string v0, "PRESET_PRICE_FLOW_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739648
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->PRESET_PRICE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739649
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739650
    :pswitch_4
    const-string v0, "ERROR_UPDATE_PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739651
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PAYMENT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739652
    :cond_6
    const-string v0, "ERROR_UPDATE_DESTINATION_NOT_EDITABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_EDITABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739654
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto :goto_0

    .line 739655
    :pswitch_5
    const-string v0, "ERROR_CONFIRM_CANCELLATION_SHARED_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739656
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_SHARED_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739657
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739658
    :pswitch_6
    const-string v0, "SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 739659
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739660
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739661
    :pswitch_7
    const-string v0, "ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 739662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739663
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739664
    :pswitch_8
    const-string v0, "SURGE_ACCEPTANCE_FLOW_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 739665
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SURGE_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739666
    :cond_b
    const-string v0, "ERROR_NON_EXISTENT_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739667
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NON_EXISTENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739668
    :cond_c
    const-string v0, "ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739669
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_EMAIL_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739670
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739671
    :pswitch_9
    const-string v0, "INTERNAL_SERVER_ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 739672
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->INTERNAL_SERVER_ERROR:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739673
    :cond_e
    const-string v0, "ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 739674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_VERIFY_ACCOUNT_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739675
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739676
    :pswitch_a
    const-string v0, "ERROR_TOO_MANY_CANCELLATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 739677
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_TOO_MANY_CANCELLATIONS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739678
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739679
    :pswitch_b
    const-string v0, "ERROR_UPDATE_DESTINATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 739680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739681
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739682
    :pswitch_c
    const-string v0, "ERROR_ACTIVE_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 739683
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_ACTIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739684
    :cond_12
    const-string v0, "ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 739685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PHONE_NUMBER_IN_PROVIDER_APP_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739686
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739687
    :pswitch_d
    const-string v0, "ADDING_PAYMENT_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 739688
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ADDING_PAYMENT_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739689
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739690
    :pswitch_e
    const-string v0, "ERROR_NOT_AUTHORIZED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 739691
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NOT_AUTHORIZED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739692
    :cond_15
    const-string v0, "ERROR_UPDATE_DESTINATION_NOT_OWNER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 739693
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_DESTINATION_NOT_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739694
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739695
    :pswitch_f
    const-string v0, "ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 739696
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CANCEL_RIDE_USER_NOT_AN_OWNER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739697
    :cond_17
    const-string v0, "ERROR_CONFIRM_CANCELLATION_TIME_LIMIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 739698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_CONFIRM_CANCELLATION_TIME_LIMIT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739699
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739700
    :pswitch_10
    const-string v0, "ERROR_USER_NOT_ALLOWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 739701
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739702
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739703
    :pswitch_11
    const-string v0, "ERROR_INVALID_CONTACT_INFORMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 739704
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_CONTACT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739705
    :cond_1a
    const-string v0, "ERROR_INVALID_PAYMENT_INFORMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 739706
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PAYMENT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739707
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739708
    :pswitch_12
    const-string v0, "ERROR_DRIVERS_UNAVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 739709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_DRIVERS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739710
    :cond_1c
    const-string v0, "ERROR_USER_IN_DRIVER_MODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 739711
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_USER_IN_DRIVER_MODE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739712
    :cond_1d
    const-string v0, "ERROR_SAME_PICKUP_DROPOFF_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 739713
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_SAME_PICKUP_DROPOFF_LOCATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739714
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739715
    :pswitch_13
    const-string v0, "ERROR_INVALID_PICKUP_DROPOFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 739716
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PICKUP_DROPOFF:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739717
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739718
    :pswitch_14
    const-string v0, "ERROR_RIDE_TYPE_NOT_ALLOWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 739719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_RIDE_TYPE_NOT_ALLOWED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739720
    :cond_20
    const-string v0, "CANCEL_ACCEPTANCE_FLOW_NEEDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 739721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->CANCEL_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739722
    :cond_21
    const-string v0, "ERROR_NO_SERVICE_IN_AREA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 739723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_NO_SERVICE_IN_AREA:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739724
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739725
    :pswitch_15
    const-string v0, "ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 739726
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UNEXPECTED_FAILURE_FROM_RIDE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    .line 739727
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;
    .locals 1

    .prologue
    .line 739729
    const-class v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;
    .locals 1

    .prologue
    .line 739728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    return-object v0
.end method
