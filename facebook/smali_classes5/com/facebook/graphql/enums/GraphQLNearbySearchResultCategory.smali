.class public final enum Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum PLACES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum QUERIES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum REGIONS_ONLY:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum TOPICS:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum TOPICS_AND_REGION:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729491
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729492
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "PLACES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->PLACES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729493
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "TOPICS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729494
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "TOPICS_AND_REGION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS_AND_REGION:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729495
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "QUERIES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->QUERIES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729496
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    const-string v1, "REGIONS_ONLY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->REGIONS_ONLY:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729497
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->PLACES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS_AND_REGION:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->QUERIES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->REGIONS_ONLY:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;
    .locals 1

    .prologue
    .line 729499
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    .line 729500
    :goto_0
    return-object v0

    .line 729501
    :cond_1
    const-string v0, "PLACES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->PLACES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0

    .line 729503
    :cond_2
    const-string v0, "TOPICS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0

    .line 729505
    :cond_3
    const-string v0, "TOPICS_AND_REGION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->TOPICS_AND_REGION:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0

    .line 729507
    :cond_4
    const-string v0, "REGIONS_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->REGIONS_ONLY:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0

    .line 729509
    :cond_5
    const-string v0, "QUERIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->QUERIES:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0

    .line 729511
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;
    .locals 1

    .prologue
    .line 729512
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;
    .locals 1

    .prologue
    .line 729513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    return-object v0
.end method
