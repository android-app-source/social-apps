.class public final enum Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

.field public static final enum EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

.field public static final enum HSCROLL:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

.field public static final enum LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

.field public static final enum UNGROUPED_LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738502
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738503
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738504
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const-string v1, "HSCROLL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738505
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738506
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    const-string v1, "UNGROUPED_LIST"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNGROUPED_LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738507
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNGROUPED_LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738508
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
    .locals 1

    .prologue
    .line 738509
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    .line 738510
    :goto_0
    return-object v0

    .line 738511
    :cond_1
    const-string v0, "EXPANDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    goto :goto_0

    .line 738513
    :cond_2
    const-string v0, "HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    goto :goto_0

    .line 738515
    :cond_3
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    goto :goto_0

    .line 738517
    :cond_4
    const-string v0, "UNGROUPED_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNGROUPED_LIST:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    goto :goto_0

    .line 738519
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
    .locals 1

    .prologue
    .line 738520
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;
    .locals 1

    .prologue
    .line 738521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfileDiscoverySectionViewStyleEnum;

    return-object v0
.end method
