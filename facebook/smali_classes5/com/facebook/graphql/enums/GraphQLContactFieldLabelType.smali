.class public final enum Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum HOME:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum HOME_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum MOBILE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum WORK:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public static final enum WORK_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724697
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724698
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724699
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724700
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724701
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "HOME_FAX"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724702
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "WORK_FAX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724703
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const-string v1, "OTHER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->OTHER:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724704
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->OTHER:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724705
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;
    .locals 1

    .prologue
    .line 724682
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 724683
    :goto_0
    return-object v0

    .line 724684
    :cond_1
    const-string v0, "MOBILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724686
    :cond_2
    const-string v0, "HOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724687
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724688
    :cond_3
    const-string v0, "WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724690
    :cond_4
    const-string v0, "HOME_FAX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724691
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->HOME_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724692
    :cond_5
    const-string v0, "WORK_FAX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724693
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->WORK_FAX:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724694
    :cond_6
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724695
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->OTHER:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0

    .line 724696
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;
    .locals 1

    .prologue
    .line 724681
    const-class v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;
    .locals 1

    .prologue
    .line 724680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    return-object v0
.end method
