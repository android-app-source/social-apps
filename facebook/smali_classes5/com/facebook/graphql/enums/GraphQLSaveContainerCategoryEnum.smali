.class public final enum Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

.field public static final enum POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

.field public static final enum QUICK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739796
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 739797
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 739798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 739799
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    const-string v1, "QUICK_PROMOTION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->QUICK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 739800
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->QUICK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739801
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;
    .locals 1

    .prologue
    .line 739802
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    .line 739803
    :goto_0
    return-object v0

    .line 739804
    :cond_1
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739805
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->POST:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    goto :goto_0

    .line 739806
    :cond_2
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    goto :goto_0

    .line 739808
    :cond_3
    const-string v0, "QUICK_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739809
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->QUICK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    goto :goto_0

    .line 739810
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;
    .locals 1

    .prologue
    .line 739811
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;
    .locals 1

    .prologue
    .line 739812
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    return-object v0
.end method
