.class public final enum Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

.field public static final enum CELSIUS:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

.field public static final enum FAHRENHEIT:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    .line 741362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    const-string v1, "CELSIUS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->CELSIUS:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    .line 741363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    const-string v1, "FAHRENHEIT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->FAHRENHEIT:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    .line 741364
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->CELSIUS:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->FAHRENHEIT:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741360
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;
    .locals 1

    .prologue
    .line 741351
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    .line 741352
    :goto_0
    return-object v0

    .line 741353
    :cond_1
    const-string v0, "FAHRENHEIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->FAHRENHEIT:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    goto :goto_0

    .line 741355
    :cond_2
    const-string v0, "CELSIUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->CELSIUS:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    goto :goto_0

    .line 741357
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;
    .locals 1

    .prologue
    .line 741359
    const-class v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;
    .locals 1

    .prologue
    .line 741358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    return-object v0
.end method
