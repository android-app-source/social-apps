.class public final enum Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum MUTUAL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum PHOTO_UPLOADS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum RECENT_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum REFRESHER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "RECENT_FRIENDS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->RECENT_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "MUTUAL_FRIENDS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MUTUAL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "FOLLOWERS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "COMPOSER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "PHOTO_UPLOADS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->PHOTO_UPLOADS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 740999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "REFRESHER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->REFRESHER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 741000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    const-string v1, "DISCOVERY_BUCKET"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 741001
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->RECENT_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MUTUAL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->PHOTO_UPLOADS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->REFRESHER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741025
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
    .locals 1

    .prologue
    .line 741004
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 741005
    :goto_0
    return-object v0

    .line 741006
    :cond_1
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741007
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741008
    :cond_2
    const-string v0, "RECENT_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->RECENT_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741010
    :cond_3
    const-string v0, "MUTUAL_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MUTUAL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741012
    :cond_4
    const-string v0, "FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 741013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741014
    :cond_5
    const-string v0, "FOLLOWERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 741015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741016
    :cond_6
    const-string v0, "COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741018
    :cond_7
    const-string v0, "PHOTO_UPLOADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 741019
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->PHOTO_UPLOADS:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741020
    :cond_8
    const-string v0, "REFRESHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741021
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->REFRESHER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741022
    :cond_9
    const-string v0, "DISCOVERY_BUCKET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 741023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0

    .line 741024
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
    .locals 1

    .prologue
    .line 741003
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;
    .locals 1

    .prologue
    .line 741002
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    return-object v0
.end method
