.class public final enum Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum S_SENT:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737966
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737967
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_PENDING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737968
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_PENDING_VERIFICATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737969
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_PENDING_VERIFICATION_PROCESSING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_PENDING_MANUAL_REVIEW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_SENDER_RISK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_RECIPIENT_RISK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_DECLINED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_EXPIRED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_SAME_CARD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_CUSTOMER_SERVICE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_CHARGEBACK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_CANCELED_SYSTEM_FAIL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_COMPLETED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    const-string v1, "S_SENT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_SENT:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737982
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_SENT:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737983
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .locals 1

    .prologue
    .line 737984
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    .line 737985
    :goto_0
    return-object v0

    .line 737986
    :cond_1
    const-string v0, "S_PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737988
    :cond_2
    const-string v0, "S_PENDING_VERIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737989
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737990
    :cond_3
    const-string v0, "S_PENDING_VERIFICATION_PROCESSING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737991
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_VERIFICATION_PROCESSING:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737992
    :cond_4
    const-string v0, "S_PENDING_MANUAL_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737993
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_PENDING_MANUAL_REVIEW:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737994
    :cond_5
    const-string v0, "S_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737996
    :cond_6
    const-string v0, "S_CANCELED_SENDER_RISK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737997
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SENDER_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 737998
    :cond_7
    const-string v0, "S_CANCELED_RECIPIENT_RISK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_RECIPIENT_RISK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 738000
    :cond_8
    const-string v0, "S_CANCELED_DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 738002
    :cond_9
    const-string v0, "S_CANCELED_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 738004
    :cond_a
    const-string v0, "S_CANCELED_SAME_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738005
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SAME_CARD:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 738006
    :cond_b
    const-string v0, "S_CANCELED_CUSTOMER_SERVICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 738007
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CUSTOMER_SERVICE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto :goto_0

    .line 738008
    :cond_c
    const-string v0, "S_CANCELED_CHARGEBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 738009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_CHARGEBACK:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto/16 :goto_0

    .line 738010
    :cond_d
    const-string v0, "S_CANCELED_SYSTEM_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 738011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_CANCELED_SYSTEM_FAIL:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto/16 :goto_0

    .line 738012
    :cond_e
    const-string v0, "S_COMPLETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 738013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto/16 :goto_0

    .line 738014
    :cond_f
    const-string v0, "S_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 738015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->S_SENT:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto/16 :goto_0

    .line 738016
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .locals 1

    .prologue
    .line 738017
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;
    .locals 1

    .prologue
    .line 738018
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    return-object v0
.end method
