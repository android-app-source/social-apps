.class public final enum Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum OPEN_COMPOSER:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum SHARE_LOCATION:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

.field public static final enum WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "OPEN_COMPOSER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->OPEN_COMPOSER:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724722
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "SHARE_LOCATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SHARE_LOCATION:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724723
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "SEND_MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724724
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NONE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724725
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "NO_WAVE_SENT_OR_RECEIVED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724726
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "WAVE_SENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724727
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "WAVE_RECEIVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724728
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const-string v1, "WAVE_SENT_AND_RECEIVED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724729
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->OPEN_COMPOSER:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SHARE_LOCATION:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NONE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724730
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
    .locals 1

    .prologue
    .line 724731
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 724732
    :goto_0
    return-object v0

    .line 724733
    :cond_1
    const-string v0, "OPEN_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724734
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->OPEN_COMPOSER:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724735
    :cond_2
    const-string v0, "SHARE_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SHARE_LOCATION:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724737
    :cond_3
    const-string v0, "SEND_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724738
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724739
    :cond_4
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724740
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NONE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724741
    :cond_5
    const-string v0, "NO_WAVE_SENT_OR_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724742
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724743
    :cond_6
    const-string v0, "WAVE_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724744
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724745
    :cond_7
    const-string v0, "WAVE_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724746
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724747
    :cond_8
    const-string v0, "WAVE_SENT_AND_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724748
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0

    .line 724749
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
    .locals 1

    .prologue
    .line 724750
    const-class v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
    .locals 1

    .prologue
    .line 724751
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    return-object v0
.end method
