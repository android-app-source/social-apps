.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum LISTEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public static final enum WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 730186
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730187
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730188
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "BOOK_NOW"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730189
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "MAKE_RESERVATION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730190
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "CALL_NOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730191
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "CHARITY_DONATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730192
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "CONTACT_US"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730193
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "DONATE_NOW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730194
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "MESSAGE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730195
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "OPEN_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730196
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "PLAY_NOW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730197
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "SHOP_NOW"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730198
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "SIGN_UP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730199
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "WATCH_NOW"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730200
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "GET_OFFER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730201
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "GET_OFFER_VIEW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730202
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "REQUEST_QUOTE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "BOOK_APPOINTMENT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730204
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "LISTEN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LISTEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730205
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "EMAIL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730206
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "LEARN_MORE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730207
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "REQUEST_APPOINTMENT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730208
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "READ_ARTICLES"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "VIDEO_CALL"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "ORDER_NOW"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730211
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "GET_DIRECTIONS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730212
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const-string v1, "BUY_TICKETS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730213
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LISTEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 1

    .prologue
    .line 730215
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 730216
    :goto_0
    return-object v0

    .line 730217
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730219
    :cond_2
    const-string v0, "BOOK_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730221
    :cond_3
    const-string v0, "MAKE_RESERVATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 730222
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730223
    :cond_4
    const-string v0, "CALL_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 730224
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730225
    :cond_5
    const-string v0, "CHARITY_DONATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 730226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730227
    :cond_6
    const-string v0, "CONTACT_US"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 730228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730229
    :cond_7
    const-string v0, "DONATE_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 730230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730231
    :cond_8
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 730232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730233
    :cond_9
    const-string v0, "OPEN_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 730234
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730235
    :cond_a
    const-string v0, "PLAY_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 730236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730237
    :cond_b
    const-string v0, "SHOP_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 730238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto :goto_0

    .line 730239
    :cond_c
    const-string v0, "SIGN_UP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 730240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730241
    :cond_d
    const-string v0, "WATCH_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 730242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730243
    :cond_e
    const-string v0, "GET_OFFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 730244
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730245
    :cond_f
    const-string v0, "GET_OFFER_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 730246
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730247
    :cond_10
    const-string v0, "REQUEST_QUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 730248
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730249
    :cond_11
    const-string v0, "BOOK_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 730250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730251
    :cond_12
    const-string v0, "LISTEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 730252
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LISTEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730253
    :cond_13
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 730254
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730255
    :cond_14
    const-string v0, "LEARN_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 730256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730257
    :cond_15
    const-string v0, "REQUEST_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 730258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730259
    :cond_16
    const-string v0, "READ_ARTICLES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 730260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730261
    :cond_17
    const-string v0, "VIDEO_CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 730262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730263
    :cond_18
    const-string v0, "ORDER_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 730264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730265
    :cond_19
    const-string v0, "GET_DIRECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 730266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730267
    :cond_1a
    const-string v0, "BUY_TICKETS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 730268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0

    .line 730269
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 1

    .prologue
    .line 730270
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 1

    .prologue
    .line 730271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method
