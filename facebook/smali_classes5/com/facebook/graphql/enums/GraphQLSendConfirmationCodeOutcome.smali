.class public final enum Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum INVALID_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum PHONE_CALLING_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum PHONE_CANNOT_REACH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum PHONE_NUMBER_ALREADY_VERIFIED:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum PHONE_USED_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum SUCCESS:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum UNKNOWN_ERROR:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNKNOWN_ERROR:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "PHONE_NUMBER_ALREADY_VERIFIED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_NUMBER_ALREADY_VERIFIED:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "INVALID_PHONE_NUMBER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->INVALID_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "PHONE_USED_TOO_MUCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_USED_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "PHONE_CALLING_TOO_MUCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CALLING_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    const-string v1, "PHONE_CANNOT_REACH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CANNOT_REACH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740045
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNKNOWN_ERROR:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_NUMBER_ALREADY_VERIFIED:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->INVALID_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_USED_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CALLING_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CANNOT_REACH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740046
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;
    .locals 1

    .prologue
    .line 740047
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    .line 740048
    :goto_0
    return-object v0

    .line 740049
    :cond_1
    const-string v0, "SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740050
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740051
    :cond_2
    const-string v0, "UNKNOWN_ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740052
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNKNOWN_ERROR:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740053
    :cond_3
    const-string v0, "PHONE_NUMBER_ALREADY_VERIFIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740054
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_NUMBER_ALREADY_VERIFIED:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740055
    :cond_4
    const-string v0, "INVALID_PHONE_NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740056
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->INVALID_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740057
    :cond_5
    const-string v0, "PHONE_USED_TOO_MUCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_USED_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740059
    :cond_6
    const-string v0, "PHONE_CALLING_TOO_MUCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740060
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CALLING_TOO_MUCH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740061
    :cond_7
    const-string v0, "PHONE_CANNOT_REACH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_CANNOT_REACH:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0

    .line 740063
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;
    .locals 1

    .prologue
    .line 740064
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;
    .locals 1

    .prologue
    .line 740065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    return-object v0
.end method
