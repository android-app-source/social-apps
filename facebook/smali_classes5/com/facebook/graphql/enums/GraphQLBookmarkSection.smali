.class public final enum Lcom/facebook/graphql/enums/GraphQLBookmarkSection;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBookmarkSection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum APP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum BUSINESS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum BUSINESS_PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum COMBINED_APP_AND_TOOLS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum COMPANY_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum DEVELOPER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum DEVELOPER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum EVENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum FACEBOOK_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum FOLDER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum FUNDRAISER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum GAMETIME_LIVE_EVENTS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum GAME_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum INTEREST_AND_CURATED_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum INTEREST_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum LIST_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum NEWS_FEED_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum NON_COMPANY_GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum NON_COMPANY_GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PAYMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PAYMENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PINNABLE_PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum PLATFORM_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum RED_ENVELOPE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum SUBSCRIBED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum TOPIC_FEEDS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum USER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum USER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_CROSS_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_GROUPS_TEAM:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

.field public static final enum WORK_PINNED_GROUPS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723607
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723608
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "FACEBOOK_APP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FACEBOOK_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723609
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "APP_TOOL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723610
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "APP"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723611
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "BUSINESS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723612
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "BUSINESS_PAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS_PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723613
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PLATFORM_APP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PLATFORM_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723614
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "FRIEND_LIST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "GROUP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "FUNDRAISER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "FUNDRAISER_TOOL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "GROUP_TOOL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "INTEREST_LIST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "INTEREST_AND_CURATED_LIST"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_AND_CURATED_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723621
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "LIST_TOOL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->LIST_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723622
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PAGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723623
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PAGE_TOOL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723624
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PINNABLE_PAGE_TOOL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PINNABLE_PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723625
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "USER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "USER_TOOL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723627
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "DEVELOPER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723628
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "DEVELOPER_TOOL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723629
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "GAME_TOOL"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAME_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723630
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "COMPANY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723631
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "COMPANY_TOOL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723632
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "NON_COMPANY_GROUP"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723633
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "NON_COMPANY_GROUP_TOOL"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723634
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "EVENT"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723635
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "EVENT_TOOL"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723636
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "SUBSCRIBED_SEARCHES"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->SUBSCRIBED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723637
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "FOLDER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FOLDER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723638
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "COMBINED_APP_AND_TOOLS"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMBINED_APP_AND_TOOLS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723639
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "GAMETIME_LIVE_EVENTS"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAMETIME_LIVE_EVENTS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723640
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_TEAM"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_TEAM:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723641
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_FEEDBACK"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723642
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_ANNOUNCEMENT"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723643
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_SOCIAL"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723644
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_CROSS_COMPANY"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_CROSS_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723645
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_GROUPS_MULTI_COMPANY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723646
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "NEWS_FEED"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723647
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "NEWS_FEED_TOOL"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723648
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "TOPIC_FEEDS"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->TOPIC_FEEDS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723649
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PAYMENT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723650
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "PAYMENT_TOOL"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723651
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "RED_ENVELOPE"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->RED_ENVELOPE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723652
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723653
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    const-string v1, "WORK_PINNED_GROUPS"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_PINNED_GROUPS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723654
    const/16 v0, 0x2f

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FACEBOOK_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS_PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PLATFORM_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_AND_CURATED_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->LIST_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PINNABLE_PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAME_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->SUBSCRIBED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FOLDER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMBINED_APP_AND_TOOLS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAMETIME_LIVE_EVENTS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_TEAM:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_CROSS_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->TOPIC_FEEDS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->RED_ENVELOPE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_PINNED_GROUPS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBookmarkSection;
    .locals 2

    .prologue
    .line 723656
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    .line 723657
    :goto_0
    return-object v0

    .line 723658
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 723659
    packed-switch v0, :pswitch_data_0

    .line 723660
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723661
    :pswitch_1
    const-string v0, "WORK_GROUPS_FEEDBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723663
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723664
    :pswitch_2
    const-string v0, "FACEBOOK_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723665
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FACEBOOK_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723666
    :cond_3
    const-string v0, "INTEREST_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723667
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723668
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723669
    :pswitch_3
    const-string v0, "COMPANY_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723670
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723671
    :cond_5
    const-string v0, "EVENT_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723672
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723673
    :cond_6
    const-string v0, "PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723675
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723676
    :pswitch_4
    const-string v0, "GAME_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723677
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAME_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto :goto_0

    .line 723678
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723679
    :pswitch_5
    const-string v0, "GROUP_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 723680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723681
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723682
    :pswitch_6
    const-string v0, "DEVELOPER_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 723683
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723684
    :cond_a
    const-string v0, "COMBINED_APP_AND_TOOLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 723685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMBINED_APP_AND_TOOLS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723686
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723687
    :pswitch_7
    const-string v0, "USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 723688
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723689
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723690
    :pswitch_8
    const-string v0, "GAMETIME_LIVE_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 723691
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GAMETIME_LIVE_EVENTS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723692
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723693
    :pswitch_9
    const-string v0, "LIST_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 723694
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->LIST_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723695
    :cond_e
    const-string v0, "FUNDRAISER_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 723696
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723697
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723698
    :pswitch_a
    const-string v0, "BUSINESS_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 723699
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS_PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723700
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723701
    :pswitch_b
    const-string v0, "PLATFORM_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 723702
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PLATFORM_APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723703
    :cond_11
    const-string v0, "TOPIC_FEEDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 723704
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->TOPIC_FEEDS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723705
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723706
    :pswitch_c
    const-string v0, "PAGE_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 723707
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723708
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723709
    :pswitch_d
    const-string v0, "INTEREST_AND_CURATED_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 723710
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->INTEREST_AND_CURATED_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723711
    :cond_14
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 723712
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723713
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723714
    :pswitch_e
    const-string v0, "NON_COMPANY_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 723715
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723716
    :cond_16
    const-string v0, "PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 723717
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAGE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723718
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723719
    :pswitch_f
    const-string v0, "NEWS_FEED_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 723720
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723721
    :cond_18
    const-string v0, "PAYMENT_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 723722
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PAYMENT_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723723
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723724
    :pswitch_10
    const-string v0, "COMPANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 723725
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723726
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723727
    :pswitch_11
    const-string v0, "USER_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 723728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->USER_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723729
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723730
    :pswitch_12
    const-string v0, "SUBSCRIBED_SEARCHES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 723731
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->SUBSCRIBED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723732
    :cond_1c
    const-string v0, "NEWS_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 723733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723734
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723735
    :pswitch_13
    const-string v0, "APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 723736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723737
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723738
    :pswitch_14
    const-string v0, "PINNABLE_PAGE_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 723739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->PINNABLE_PAGE_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723740
    :cond_1f
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 723741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->EVENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723742
    :cond_20
    const-string v0, "WORK_PINNED_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 723743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_PINNED_GROUPS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723744
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723745
    :pswitch_15
    const-string v0, "BUSINESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 723746
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->BUSINESS:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723747
    :cond_22
    const-string v0, "WORK_GROUPS_MULTI_COMPANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 723748
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723749
    :cond_23
    const-string v0, "WORK_GROUPS_CROSS_COMPANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 723750
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_CROSS_COMPANY:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723751
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723752
    :pswitch_16
    const-string v0, "NON_COMPANY_GROUP_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 723753
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->NON_COMPANY_GROUP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723754
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723755
    :pswitch_17
    const-string v0, "RED_ENVELOPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 723756
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->RED_ENVELOPE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723757
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723758
    :pswitch_18
    const-string v0, "FOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 723759
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FOLDER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723760
    :cond_27
    const-string v0, "WORK_GROUPS_TEAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 723761
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_TEAM:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723762
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723763
    :pswitch_19
    const-string v0, "DEVELOPER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 723764
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->DEVELOPER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723765
    :cond_29
    const-string v0, "WORK_GROUPS_ANNOUNCEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 723766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723767
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723768
    :pswitch_1a
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 723769
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->GROUP:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723770
    :cond_2b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723771
    :pswitch_1b
    const-string v0, "APP_TOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 723772
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->APP_TOOL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723773
    :cond_2c
    const-string v0, "FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 723774
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723775
    :cond_2d
    const-string v0, "WORK_GROUPS_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 723776
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->WORK_GROUPS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723777
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723778
    :pswitch_1c
    const-string v0, "FUNDRAISER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 723779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    .line 723780
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBookmarkSection;
    .locals 1

    .prologue
    .line 723781
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBookmarkSection;
    .locals 1

    .prologue
    .line 723782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    return-object v0
.end method
