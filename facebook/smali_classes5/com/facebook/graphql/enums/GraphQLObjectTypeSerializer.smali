.class public Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLObjectType;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 729618
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;

    invoke-direct {v1}, Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 729619
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 729629
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0nX;)V
    .locals 3

    .prologue
    .line 729621
    const/4 v0, 0x1

    .line 729622
    if-eqz v0, :cond_0

    .line 729623
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 729624
    :cond_0
    iget-object v1, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 729625
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 729626
    :cond_1
    if-eqz v0, :cond_2

    .line 729627
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 729628
    :cond_2
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 729620
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {p1, p2}, Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0nX;)V

    return-void
.end method
