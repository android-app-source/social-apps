.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum ADJUSTED_FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum BUTTON_FILLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum BUTTON_OUTLINE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum CAPTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum DARK_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum EXPANDABLE_FULLSCREEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum LANDSCAPE_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum LIGHT_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum NON_ADJUSTED_FIT_TO_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum PHOTO_GRAY_OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum ROTATION_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum TEXT_NO_CUSTOM_MEASURE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum TILT_TO_PAN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum VIDEO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum VIDEO_PLAY_PAUSE_DISABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

.field public static final enum VIDEO_UNMUTED_FORCED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727784
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727785
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "TILT_TO_PAN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TILT_TO_PAN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727786
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "BUTTON_OUTLINE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_OUTLINE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727787
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "BUTTON_FILLED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_FILLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727788
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "EXPANDABLE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727789
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "EXPANDABLE_FULLSCREEN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE_FULLSCREEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727790
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "OVERLAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727791
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "CAPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CAPTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727792
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "FIT_TO_HEIGHT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727793
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "DARK_GRADIENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->DARK_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727794
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "LIGHT_GRADIENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LIGHT_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727795
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "SHOW_INTERACTION_HINT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727796
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "NON_ADJUSTED_FIT_TO_WIDTH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->NON_ADJUSTED_FIT_TO_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727797
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "PHOTO_GRAY_OVERLAY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->PHOTO_GRAY_OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "TEXT_NO_CUSTOM_MEASURE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TEXT_NO_CUSTOM_MEASURE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727799
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "ADJUSTED_FIT_TO_HEIGHT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ADJUSTED_FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727800
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "LANDSCAPE_ENABLED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LANDSCAPE_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727801
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "ROTATION_ENABLED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ROTATION_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727802
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "VIDEO_PLAY_PAUSE_DISABLED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_PLAY_PAUSE_DISABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727803
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "VIDEO_UNMUTED_FORCED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_UNMUTED_FORCED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "VIDEO_MUTED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "SCRUBBABLE_GIF"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727806
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "CENTER_ALIGNED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727807
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    const-string v1, "BUTTON_COMPACT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727808
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TILT_TO_PAN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_OUTLINE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_FILLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE_FULLSCREEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CAPTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->DARK_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LIGHT_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->NON_ADJUSTED_FIT_TO_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->PHOTO_GRAY_OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TEXT_NO_CUSTOM_MEASURE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ADJUSTED_FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LANDSCAPE_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ROTATION_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_PLAY_PAUSE_DISABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_UNMUTED_FORCED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727809
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;
    .locals 1

    .prologue
    .line 727810
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    .line 727811
    :goto_0
    return-object v0

    .line 727812
    :cond_1
    const-string v0, "TILT_TO_PAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TILT_TO_PAN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727814
    :cond_2
    const-string v0, "BUTTON_OUTLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_OUTLINE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727816
    :cond_3
    const-string v0, "BUTTON_FILLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727817
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_FILLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727818
    :cond_4
    const-string v0, "EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727819
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727820
    :cond_5
    const-string v0, "EXPANDABLE_FULLSCREEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727821
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE_FULLSCREEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727822
    :cond_6
    const-string v0, "OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727823
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727824
    :cond_7
    const-string v0, "CAPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727825
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CAPTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727826
    :cond_8
    const-string v0, "FIT_TO_HEIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727827
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727828
    :cond_9
    const-string v0, "DARK_GRADIENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727829
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->DARK_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727830
    :cond_a
    const-string v0, "LIGHT_GRADIENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727831
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LIGHT_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727832
    :cond_b
    const-string v0, "SHOW_INTERACTION_HINT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727833
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto :goto_0

    .line 727834
    :cond_c
    const-string v0, "NON_ADJUSTED_FIT_TO_WIDTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727835
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->NON_ADJUSTED_FIT_TO_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727836
    :cond_d
    const-string v0, "PHOTO_GRAY_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727837
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->PHOTO_GRAY_OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727838
    :cond_e
    const-string v0, "TEXT_NO_CUSTOM_MEASURE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727839
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TEXT_NO_CUSTOM_MEASURE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727840
    :cond_f
    const-string v0, "ADJUSTED_FIT_TO_HEIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727841
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ADJUSTED_FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727842
    :cond_10
    const-string v0, "LANDSCAPE_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LANDSCAPE_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727844
    :cond_11
    const-string v0, "VIDEO_PLAY_PAUSE_DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727845
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_PLAY_PAUSE_DISABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727846
    :cond_12
    const-string v0, "VIDEO_UNMUTED_FORCED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 727847
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_UNMUTED_FORCED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727848
    :cond_13
    const-string v0, "VIDEO_MUTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 727849
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727850
    :cond_14
    const-string v0, "ROTATION_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 727851
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ROTATION_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727852
    :cond_15
    const-string v0, "SCRUBBABLE_GIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 727853
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727854
    :cond_16
    const-string v0, "CENTER_ALIGNED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 727855
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727856
    :cond_17
    const-string v0, "BUTTON_COMPACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 727857
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0

    .line 727858
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;
    .locals 1

    .prologue
    .line 727859
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;
    .locals 1

    .prologue
    .line 727860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    return-object v0
.end method
