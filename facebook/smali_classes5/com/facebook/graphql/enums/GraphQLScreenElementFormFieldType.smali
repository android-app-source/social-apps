.class public final enum Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum CHECKBOX:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DATE_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DATE_TIME:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DATE_TIME_RANGE_MULTI_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DATE_TIME_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DEPRECATED_19:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DEPRECATED_22:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum DEPRECATED_8:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum MARKET_OPTIN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum MESSENGER_PLATFORM_OPT_IN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum PAYMENT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum PRODUCT_ITEM_LIST:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum QUANTITY_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SELECTION_INT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SELECTION_PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SELECTION_PRODUCT_WITH_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SELECTION_STRING:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum SHOPPING_CART:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum TIME_SLOT_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739844
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "CHECKBOX"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "CONTACT_INFO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DATE_PICKER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DATE_TIME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DATE_TIME_SELECTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DATE_TIME_RANGE_MULTI_SELECTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_RANGE_MULTI_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DEPRECATED_8"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_8:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739853
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SELECTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739854
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SELECTION_INT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_INT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739855
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SELECTION_PRODUCT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739856
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SELECTION_STRING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_STRING:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739857
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SHOPPING_CART"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SHOPPING_CART:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739858
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "TEXT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TEXT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739859
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "TIME_SLOT_PICKER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TIME_SLOT_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739860
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "PAYMENT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739861
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "UNIVERSAL_CHECKOUT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739862
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "PRODUCT_ITEM_LIST"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PRODUCT_ITEM_LIST:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739863
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DEPRECATED_19"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_19:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739864
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "SELECTION_PRODUCT_WITH_SELECTOR"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT_WITH_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739865
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "MARKET_OPTIN"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MARKET_OPTIN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739866
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "DEPRECATED_22"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_22:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739867
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "MESSENGER_PLATFORM_OPT_IN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MESSENGER_PLATFORM_OPT_IN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const-string v1, "QUANTITY_SELECTOR"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->QUANTITY_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739869
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_RANGE_MULTI_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_8:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_INT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_STRING:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SHOPPING_CART:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TEXT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TIME_SLOT_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PRODUCT_ITEM_LIST:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_19:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT_WITH_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MARKET_OPTIN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DEPRECATED_22:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MESSENGER_PLATFORM_OPT_IN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->QUANTITY_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739872
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 1

    .prologue
    .line 739873
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 739874
    :goto_0
    return-object v0

    .line 739875
    :cond_1
    const-string v0, "ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739876
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739877
    :cond_2
    const-string v0, "CHECKBOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739878
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739879
    :cond_3
    const-string v0, "CONTACT_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739881
    :cond_4
    const-string v0, "DATE_PICKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739882
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739883
    :cond_5
    const-string v0, "DATE_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739884
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739885
    :cond_6
    const-string v0, "DATE_TIME_SELECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739886
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739887
    :cond_7
    const-string v0, "DATE_TIME_RANGE_MULTI_SELECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739888
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->DATE_TIME_RANGE_MULTI_SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739889
    :cond_8
    const-string v0, "MARKET_OPTIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 739890
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MARKET_OPTIN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739891
    :cond_9
    const-string v0, "MESSENGER_PLATFORM_OPT_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 739892
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->MESSENGER_PLATFORM_OPT_IN:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739893
    :cond_a
    const-string v0, "PRODUCT_ITEM_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 739894
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PRODUCT_ITEM_LIST:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739895
    :cond_b
    const-string v0, "QUANTITY_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739896
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->QUANTITY_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto :goto_0

    .line 739897
    :cond_c
    const-string v0, "SELECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739898
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739899
    :cond_d
    const-string v0, "SELECTION_INT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 739900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_INT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739901
    :cond_e
    const-string v0, "SELECTION_PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 739902
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739903
    :cond_f
    const-string v0, "SELECTION_PRODUCT_WITH_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 739904
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_PRODUCT_WITH_SELECTOR:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739905
    :cond_10
    const-string v0, "SELECTION_STRING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 739906
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_STRING:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739907
    :cond_11
    const-string v0, "SHOPPING_CART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 739908
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SHOPPING_CART:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739909
    :cond_12
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 739910
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TEXT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739911
    :cond_13
    const-string v0, "TIME_SLOT_PICKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 739912
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TIME_SLOT_PICKER:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739913
    :cond_14
    const-string v0, "PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 739914
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->PAYMENT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739915
    :cond_15
    const-string v0, "UNIVERSAL_CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 739916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0

    .line 739917
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 1

    .prologue
    .line 739871
    const-class v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 1

    .prologue
    .line 739870
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method
