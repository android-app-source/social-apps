.class public final enum Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 730039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "MENTION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "CHECKIN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "REVIEW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "ALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const-string v1, "ENDORSEMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730046
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730047
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;
    .locals 1

    .prologue
    .line 730048
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    .line 730049
    :goto_0
    return-object v0

    .line 730050
    :cond_1
    const-string v0, "MENTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730051
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730052
    :cond_2
    const-string v0, "CHECKIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730053
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730054
    :cond_3
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 730055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730056
    :cond_4
    const-string v0, "REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 730057
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730058
    :cond_5
    const-string v0, "ENDORSEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 730059
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730060
    :cond_6
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 730061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0

    .line 730062
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;
    .locals 1

    .prologue
    .line 730063
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;
    .locals 1

    .prologue
    .line 730064
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    return-object v0
.end method
