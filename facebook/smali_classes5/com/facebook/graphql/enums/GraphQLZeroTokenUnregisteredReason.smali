.class public final enum Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_IN_DIALTONE_COUNTRY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_IN_REGION:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_IN_REGION_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_IN_REGION_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_IN_REGION_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NOT_ON_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum NO_CAMPAIGN:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum ON_ROAMING_SIM:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum ON_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum UNSUPPORTED_BROWSER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

.field public static final enum UNSUPPORTED_CATEGORY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 741393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "UNSUPPORTED_CARRIER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_ON_CARRIER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_ON_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "UNSUPPORTED_CATEGORY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CATEGORY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "UNAVAILABLE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741398
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NO_CAMPAIGN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NO_CAMPAIGN:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741399
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_IN_REGION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741400
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_IN_REGION_UNSUPPORTED_CARRIER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741401
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_IN_REGION_WIFI"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741402
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_IN_REGION_SUPPORTED_CARRIER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741403
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "NOT_IN_DIALTONE_COUNTRY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_DIALTONE_COUNTRY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741404
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "ON_WIFI"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741405
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "UNSUPPORTED_BROWSER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_BROWSER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741406
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    const-string v1, "ON_ROAMING_SIM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_ROAMING_SIM:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741407
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_ON_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CATEGORY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NO_CAMPAIGN:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_DIALTONE_COUNTRY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_BROWSER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_ROAMING_SIM:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;
    .locals 1

    .prologue
    .line 741409
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    .line 741410
    :goto_0
    return-object v0

    .line 741411
    :cond_1
    const-string v0, "UNSUPPORTED_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741412
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741413
    :cond_2
    const-string v0, "NOT_ON_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741414
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_ON_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741415
    :cond_3
    const-string v0, "UNSUPPORTED_CATEGORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_CATEGORY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741417
    :cond_4
    const-string v0, "UNAVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 741418
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741419
    :cond_5
    const-string v0, "NO_CAMPAIGN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 741420
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NO_CAMPAIGN:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741421
    :cond_6
    const-string v0, "NOT_IN_REGION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741423
    :cond_7
    const-string v0, "NOT_IN_REGION_UNSUPPORTED_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 741424
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741425
    :cond_8
    const-string v0, "NOT_IN_REGION_WIFI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741426
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741427
    :cond_9
    const-string v0, "NOT_IN_REGION_SUPPORTED_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 741428
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_REGION_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741429
    :cond_a
    const-string v0, "NOT_IN_DIALTONE_COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 741430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->NOT_IN_DIALTONE_COUNTRY:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741431
    :cond_b
    const-string v0, "ON_WIFI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 741432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_WIFI:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto :goto_0

    .line 741433
    :cond_c
    const-string v0, "UNSUPPORTED_BROWSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 741434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSUPPORTED_BROWSER:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto/16 :goto_0

    .line 741435
    :cond_d
    const-string v0, "ON_ROAMING_SIM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 741436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->ON_ROAMING_SIM:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto/16 :goto_0

    .line 741437
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;
    .locals 1

    .prologue
    .line 741438
    const-class v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;
    .locals 1

    .prologue
    .line 741439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    return-object v0
.end method
