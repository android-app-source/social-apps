.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum DETAILED_TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723844
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "LOCATIONS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "GENDERS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "INTERESTS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "AGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    const-string v1, "DETAILED_TARGETING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->DETAILED_TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723850
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->DETAILED_TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723828
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;
    .locals 1

    .prologue
    .line 723831
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    .line 723832
    :goto_0
    return-object v0

    .line 723833
    :cond_1
    const-string v0, "AGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723834
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0

    .line 723835
    :cond_2
    const-string v0, "DETAILED_TARGETING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723836
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->DETAILED_TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0

    .line 723837
    :cond_3
    const-string v0, "GENDERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0

    .line 723839
    :cond_4
    const-string v0, "INTERESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0

    .line 723841
    :cond_5
    const-string v0, "LOCATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0

    .line 723843
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;
    .locals 1

    .prologue
    .line 723830
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;
    .locals 1

    .prologue
    .line 723829
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    return-object v0
.end method
