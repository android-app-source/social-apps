.class public final enum Lcom/facebook/graphql/enums/GraphQLPromptFeedType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPromptFeedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

.field public static final enum GOOD_FRIENDS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

.field public static final enum NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738651
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    .line 738652
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    const-string v1, "NEWS_FEED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    .line 738653
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    const-string v1, "GOOD_FRIENDS_FEED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->GOOD_FRIENDS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    .line 738654
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->GOOD_FRIENDS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptFeedType;
    .locals 1

    .prologue
    .line 738656
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    .line 738657
    :goto_0
    return-object v0

    .line 738658
    :cond_1
    const-string v0, "NEWS_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738659
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    goto :goto_0

    .line 738660
    :cond_2
    const-string v0, "GOOD_FRIENDS_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738661
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->GOOD_FRIENDS_FEED:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    goto :goto_0

    .line 738662
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptFeedType;
    .locals 1

    .prologue
    .line 738663
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPromptFeedType;
    .locals 1

    .prologue
    .line 738664
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    return-object v0
.end method
