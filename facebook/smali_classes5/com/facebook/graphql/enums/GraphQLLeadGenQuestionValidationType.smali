.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

.field public static final enum DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

.field public static final enum NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728108
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 728109
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    const-string v1, "NUMBER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 728110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 728111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 728112
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;
    .locals 1

    .prologue
    .line 728115
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    .line 728116
    :goto_0
    return-object v0

    .line 728117
    :cond_1
    const-string v0, "NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728118
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    goto :goto_0

    .line 728119
    :cond_2
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728120
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    goto :goto_0

    .line 728121
    :cond_3
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728122
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    goto :goto_0

    .line 728123
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;
    .locals 1

    .prologue
    .line 728114
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;
    .locals 1

    .prologue
    .line 728113
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    return-object v0
.end method
