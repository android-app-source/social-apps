.class public final enum Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

.field public static final enum UNVIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

.field public static final enum VIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739830
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 739831
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    const-string v1, "UNVIEWED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNVIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 739832
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    const-string v1, "VIEWED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->VIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 739833
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNVIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->VIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .locals 1

    .prologue
    .line 739835
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    .line 739836
    :goto_0
    return-object v0

    .line 739837
    :cond_1
    const-string v0, "UNVIEWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNVIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    goto :goto_0

    .line 739839
    :cond_2
    const-string v0, "VIEWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->VIEWED:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    goto :goto_0

    .line 739841
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .locals 1

    .prologue
    .line 739842
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;
    .locals 1

    .prologue
    .line 739843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    return-object v0
.end method
