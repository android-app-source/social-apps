.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

.field public static final enum LIVE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

.field public static final enum SEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

.field public static final enum UNSEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739537
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 739538
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 739539
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    const-string v1, "SEEN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->SEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 739540
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    const-string v1, "UNSEEN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 739541
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->SEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739542
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
    .locals 1

    .prologue
    .line 739543
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 739544
    :goto_0
    return-object v0

    .line 739545
    :cond_1
    const-string v0, "LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    goto :goto_0

    .line 739547
    :cond_2
    const-string v0, "SEEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->SEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    goto :goto_0

    .line 739549
    :cond_3
    const-string v0, "UNSEEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    goto :goto_0

    .line 739551
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
    .locals 1

    .prologue
    .line 739552
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
    .locals 1

    .prologue
    .line 739553
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    return-object v0
.end method
