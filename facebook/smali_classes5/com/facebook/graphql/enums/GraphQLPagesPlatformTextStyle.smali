.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum ALIGN_CENTER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum BLACK:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum BLUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum BOLD:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum GRAY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum GREEN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum ITALIC:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum RED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum UNDERLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

.field public static final enum WHITE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737713
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737714
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "DEFAULT_STYLE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737715
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "ALIGN_CENTER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ALIGN_CENTER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "BOLD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "ITALIC"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "STRIKETHROUGH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737719
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "SUBSCRIPT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "SUPERSCRIPT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "UNDERLINE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737722
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "BLUE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737723
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "GRAY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GRAY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737724
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "GREEN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GREEN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737725
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "RED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->RED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737726
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "BLACK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLACK:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737727
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const-string v1, "WHITE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->WHITE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737728
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ALIGN_CENTER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GRAY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GREEN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->RED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLACK:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->WHITE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737679
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;
    .locals 1

    .prologue
    .line 737680
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 737681
    :goto_0
    return-object v0

    .line 737682
    :cond_1
    const-string v0, "DEFAULT_STYLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737683
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737684
    :cond_2
    const-string v0, "ALIGN_CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ALIGN_CENTER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737686
    :cond_3
    const-string v0, "BOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737687
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737688
    :cond_4
    const-string v0, "ITALIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737690
    :cond_5
    const-string v0, "STRIKETHROUGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737691
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737692
    :cond_6
    const-string v0, "SUBSCRIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737693
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737694
    :cond_7
    const-string v0, "SUPERSCRIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737695
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737696
    :cond_8
    const-string v0, "UNDERLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737697
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737698
    :cond_9
    const-string v0, "BLUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 737699
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737700
    :cond_a
    const-string v0, "GRAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 737701
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GRAY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737702
    :cond_b
    const-string v0, "GREEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 737703
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->GREEN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto :goto_0

    .line 737704
    :cond_c
    const-string v0, "RED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 737705
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->RED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto/16 :goto_0

    .line 737706
    :cond_d
    const-string v0, "BLACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 737707
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->BLACK:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto/16 :goto_0

    .line 737708
    :cond_e
    const-string v0, "WHITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 737709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->WHITE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto/16 :goto_0

    .line 737710
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;
    .locals 1

    .prologue
    .line 737711
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;
    .locals 1

    .prologue
    .line 737712
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    return-object v0
.end method
