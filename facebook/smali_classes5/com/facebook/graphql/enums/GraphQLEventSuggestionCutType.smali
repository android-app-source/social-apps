.class public final enum Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum CALENDAR_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum EVENT_RELATED:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum GROUP_YOU_ARE_IN:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum NON_FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum PAGE_YOU_LIKE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum POPULAR_AMONG_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum POPULAR_NEARBY:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->ALL:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "EVENT_RELATED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->EVENT_RELATED:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "PAGE_YOU_LIKE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->PAGE_YOU_LIKE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "GROUP_YOU_ARE_IN"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->GROUP_YOU_ARE_IN:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "POPULAR_AMONG_FRIENDS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_AMONG_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "POPULAR_NEARBY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_NEARBY:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "FRIENDS_EVENTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "NON_FRIENDS_EVENTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->NON_FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const-string v1, "CALENDAR_EVENTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->CALENDAR_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725980
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->ALL:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->EVENT_RELATED:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->PAGE_YOU_LIKE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->GROUP_YOU_ARE_IN:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_AMONG_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_NEARBY:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->NON_FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->CALENDAR_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725981
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
    .locals 1

    .prologue
    .line 725982
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 725983
    :goto_0
    return-object v0

    .line 725984
    :cond_1
    const-string v0, "POPULAR_AMONG_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725985
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_AMONG_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725986
    :cond_2
    const-string v0, "POPULAR_NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->POPULAR_NEARBY:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725988
    :cond_3
    const-string v0, "EVENT_RELATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725989
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->EVENT_RELATED:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725990
    :cond_4
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725991
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->ALL:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725992
    :cond_5
    const-string v0, "PAGE_YOU_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725993
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->PAGE_YOU_LIKE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725994
    :cond_6
    const-string v0, "GROUP_YOU_ARE_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->GROUP_YOU_ARE_IN:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725996
    :cond_7
    const-string v0, "FRIENDS_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725997
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 725998
    :cond_8
    const-string v0, "NON_FRIENDS_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->NON_FRIENDS_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 726000
    :cond_9
    const-string v0, "CALENDAR_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->CALENDAR_EVENTS:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0

    .line 726002
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
    .locals 1

    .prologue
    .line 726003
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
    .locals 1

    .prologue
    .line 726004
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    return-object v0
.end method
