.class public final enum Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

.field public static final enum CONFIDENCE_HIGH:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

.field public static final enum CONFIDENCE_LOW:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

.field public static final enum CONFIDENCE_MEDIUM:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738245
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    .line 738246
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    const-string v1, "CONFIDENCE_LOW"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_LOW:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    .line 738247
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    const-string v1, "CONFIDENCE_MEDIUM"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_MEDIUM:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    .line 738248
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    const-string v1, "CONFIDENCE_HIGH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_HIGH:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    .line 738249
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_LOW:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_MEDIUM:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_HIGH:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;
    .locals 1

    .prologue
    .line 738235
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    .line 738236
    :goto_0
    return-object v0

    .line 738237
    :cond_1
    const-string v0, "CONFIDENCE_LOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_LOW:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    goto :goto_0

    .line 738239
    :cond_2
    const-string v0, "CONFIDENCE_MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_MEDIUM:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    goto :goto_0

    .line 738241
    :cond_3
    const-string v0, "CONFIDENCE_HIGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->CONFIDENCE_HIGH:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    goto :goto_0

    .line 738243
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;
    .locals 1

    .prologue
    .line 738234
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;
    .locals 1

    .prologue
    .line 738233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;

    return-object v0
.end method
