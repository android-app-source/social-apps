.class public final enum Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMailboxFolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum DISABLED:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum HIDDEN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum INBOX:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum LEGACY:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum MONTAGE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum SPAM:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728610
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728611
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "INBOX"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->INBOX:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728612
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "SPAM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->SPAM:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728613
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->OTHER:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728614
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->PENDING:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "MONTAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->MONTAGE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "HIDDEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->HIDDEN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "LEGACY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->LEGACY:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const-string v1, "DISABLED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->DISABLED:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728620
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->INBOX:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->SPAM:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->OTHER:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->PENDING:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->MONTAGE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->HIDDEN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->LEGACY:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->DISABLED:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728621
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .locals 1

    .prologue
    .line 728622
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 728623
    :goto_0
    return-object v0

    .line 728624
    :cond_1
    const-string v0, "INBOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728625
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->INBOX:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728626
    :cond_2
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728627
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->OTHER:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728628
    :cond_3
    const-string v0, "SPAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728629
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->SPAM:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728630
    :cond_4
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728631
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->PENDING:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728632
    :cond_5
    const-string v0, "MONTAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728633
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->MONTAGE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728634
    :cond_6
    const-string v0, "HIDDEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728635
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->HIDDEN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728636
    :cond_7
    const-string v0, "LEGACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->LEGACY:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728638
    :cond_8
    const-string v0, "DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 728639
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->DISABLED:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728640
    :cond_9
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 728641
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0

    .line 728642
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .locals 1

    .prologue
    .line 728643
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .locals 1

    .prologue
    .line 728644
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    return-object v0
.end method
