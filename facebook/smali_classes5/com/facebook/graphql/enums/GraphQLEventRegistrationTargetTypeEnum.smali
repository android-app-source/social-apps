.class public final enum Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

.field public static final enum PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

.field public static final enum PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725939
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 725940
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    const-string v1, "PER_ORDER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 725941
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    const-string v1, "PER_TICKET"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 725942
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725943
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;
    .locals 1

    .prologue
    .line 725944
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 725945
    :goto_0
    return-object v0

    .line 725946
    :cond_1
    const-string v0, "PER_ORDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    goto :goto_0

    .line 725948
    :cond_2
    const-string v0, "PER_TICKET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    goto :goto_0

    .line 725950
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;
    .locals 1

    .prologue
    .line 725951
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;
    .locals 1

    .prologue
    .line 725952
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    return-object v0
.end method
