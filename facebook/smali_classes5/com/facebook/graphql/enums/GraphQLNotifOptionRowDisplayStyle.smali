.class public final enum Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum ACTION_SHEET_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum BASIC_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum BLUE_CIRCLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum CHEVRON_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum H_SCROLL_PROFILE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum PLAIN_CHECK:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum POPUP_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum PROFILE_IMAGE_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum PROFILE_IMAGE_WITH_CHECK_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum SELECTOR_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum SLIDE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum SQUARE_RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum SWIPE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

.field public static final enum WASH_TEXTS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "BASIC_MENU"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BASIC_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "SWIPE_MENU_OPTION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SWIPE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729559
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "POPUP_MENU_OPTION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->POPUP_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "SLIDE_MENU_OPTION"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SLIDE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "CHEVRON_MENU_OPTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->CHEVRON_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "PROFILE_IMAGE_OPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "ACTION_SHEET_OPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->ACTION_SHEET_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729564
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "PROFILE_IMAGE_WITH_CHECK_OPTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_WITH_CHECK_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729565
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "H_SCROLL_PROFILE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->H_SCROLL_PROFILE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729566
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "SELECTOR_MENU_OPTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SELECTOR_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729567
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "RADIO_BUTTON"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729568
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "SQUARE_RADIO_BUTTON"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SQUARE_RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729569
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "PLAIN_CHECK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PLAIN_CHECK:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729570
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "BLUE_CIRCLE_BUTTON"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BLUE_CIRCLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729571
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "TOGGLE_ON"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729572
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "TOGGLE_OFF"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729573
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "WASH_TEXTS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->WASH_TEXTS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729574
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    const-string v1, "TEXT_WITH_BUTTON"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729575
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BASIC_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SWIPE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->POPUP_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SLIDE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->CHEVRON_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->ACTION_SHEET_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_WITH_CHECK_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->H_SCROLL_PROFILE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SELECTOR_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SQUARE_RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PLAIN_CHECK:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BLUE_CIRCLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->WASH_TEXTS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729516
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;
    .locals 1

    .prologue
    .line 729517
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    .line 729518
    :goto_0
    return-object v0

    .line 729519
    :cond_1
    const-string v0, "BASIC_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729520
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BASIC_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729521
    :cond_2
    const-string v0, "SWIPE_MENU_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SWIPE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729523
    :cond_3
    const-string v0, "POPUP_MENU_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->POPUP_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729525
    :cond_4
    const-string v0, "SLIDE_MENU_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729526
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SLIDE_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729527
    :cond_5
    const-string v0, "CHEVRON_MENU_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729528
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->CHEVRON_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729529
    :cond_6
    const-string v0, "PROFILE_IMAGE_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729530
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729531
    :cond_7
    const-string v0, "ACTION_SHEET_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 729532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->ACTION_SHEET_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729533
    :cond_8
    const-string v0, "PROFILE_IMAGE_WITH_CHECK_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 729534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_WITH_CHECK_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729535
    :cond_9
    const-string v0, "H_SCROLL_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 729536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->H_SCROLL_PROFILE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729537
    :cond_a
    const-string v0, "SELECTOR_MENU_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 729538
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SELECTOR_MENU_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729539
    :cond_b
    const-string v0, "RADIO_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 729540
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto :goto_0

    .line 729541
    :cond_c
    const-string v0, "SQUARE_RADIO_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 729542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->SQUARE_RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729543
    :cond_d
    const-string v0, "PLAIN_CHECK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 729544
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PLAIN_CHECK:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729545
    :cond_e
    const-string v0, "BLUE_CIRCLE_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 729546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BLUE_CIRCLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729547
    :cond_f
    const-string v0, "TOGGLE_ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 729548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729549
    :cond_10
    const-string v0, "TOGGLE_OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 729550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729551
    :cond_11
    const-string v0, "WASH_TEXTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 729552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->WASH_TEXTS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729553
    :cond_12
    const-string v0, "TEXT_WITH_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 729554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0

    .line 729555
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;
    .locals 1

    .prologue
    .line 729515
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;
    .locals 1

    .prologue
    .line 729514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    return-object v0
.end method
