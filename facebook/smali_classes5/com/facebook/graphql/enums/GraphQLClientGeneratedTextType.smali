.class public final enum Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

.field public static final enum TIME:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    .line 724140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    const-string v1, "TIME"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->TIME:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    .line 724141
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->TIME:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;
    .locals 1

    .prologue
    .line 724143
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    .line 724144
    :goto_0
    return-object v0

    .line 724145
    :cond_1
    const-string v0, "TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724146
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->TIME:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    goto :goto_0

    .line 724147
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;
    .locals 1

    .prologue
    .line 724148
    const-class v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;
    .locals 1

    .prologue
    .line 724149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    return-object v0
.end method
