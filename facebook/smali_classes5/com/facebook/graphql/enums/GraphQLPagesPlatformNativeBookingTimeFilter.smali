.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

.field public static final enum FUTURE_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

.field public static final enum PAST_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737522
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 737523
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 737524
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    const-string v1, "FUTURE_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->FUTURE_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 737525
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    const-string v1, "PAST_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->PAST_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 737526
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->FUTURE_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->PAST_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737510
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;
    .locals 1

    .prologue
    .line 737513
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 737514
    :goto_0
    return-object v0

    .line 737515
    :cond_1
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    goto :goto_0

    .line 737517
    :cond_2
    const-string v0, "FUTURE_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->FUTURE_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    goto :goto_0

    .line 737519
    :cond_3
    const-string v0, "PAST_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737520
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->PAST_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    goto :goto_0

    .line 737521
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;
    .locals 1

    .prologue
    .line 737512
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;
    .locals 1

    .prologue
    .line 737511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    return-object v0
.end method
