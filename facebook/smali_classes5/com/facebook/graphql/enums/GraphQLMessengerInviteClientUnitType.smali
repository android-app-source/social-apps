.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

.field public static final enum HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

.field public static final enum VSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729185
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    .line 729186
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    const-string v1, "HSCROLL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    .line 729187
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    const-string v1, "VSCROLL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->VSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    .line 729188
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->VSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
    .locals 1

    .prologue
    .line 729178
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    .line 729179
    :goto_0
    return-object v0

    .line 729180
    :cond_1
    const-string v0, "HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->HSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    goto :goto_0

    .line 729182
    :cond_2
    const-string v0, "VSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729183
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->VSCROLL:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    goto :goto_0

    .line 729184
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
    .locals 1

    .prologue
    .line 729176
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;
    .locals 1

    .prologue
    .line 729175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    return-object v0
.end method
