.class public final enum Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

.field public static final enum MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

.field public static final enum POST_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

.field public static final enum PRE_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727873
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 727874
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const-string v1, "PRE_ROLL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->PRE_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 727875
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const-string v1, "MID_ROLL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 727876
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const-string v1, "POST_ROLL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->POST_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 727877
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->PRE_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->POST_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727872
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .locals 1

    .prologue
    .line 727863
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 727864
    :goto_0
    return-object v0

    .line 727865
    :cond_1
    const-string v0, "PRE_ROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->PRE_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    goto :goto_0

    .line 727867
    :cond_2
    const-string v0, "MID_ROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    goto :goto_0

    .line 727869
    :cond_3
    const-string v0, "POST_ROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727870
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->POST_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    goto :goto_0

    .line 727871
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .locals 1

    .prologue
    .line 727862
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .locals 1

    .prologue
    .line 727861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    return-object v0
.end method
