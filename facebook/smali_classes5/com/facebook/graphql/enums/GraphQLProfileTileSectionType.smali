.class public final enum Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum ABOUT:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum MUTUALITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum PYMK:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738612
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738613
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "ABOUT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738614
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "CONTACT_INFO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738615
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738616
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "FOLLOWERS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "MUTUALITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->MUTUALITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PENDING:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "PHOTOS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "PYMK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PYMK:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738621
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "DEPRECATED_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738622
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "RECENT_ACTIVITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738623
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    const-string v1, "VIDEOS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738624
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->MUTUALITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PENDING:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PYMK:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .locals 1

    .prologue
    .line 738589
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    .line 738590
    :goto_0
    return-object v0

    .line 738591
    :cond_1
    const-string v0, "ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738593
    :cond_2
    const-string v0, "CONTACT_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738594
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738595
    :cond_3
    const-string v0, "FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738596
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738597
    :cond_4
    const-string v0, "FOLLOWERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738598
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738599
    :cond_5
    const-string v0, "MUTUALITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738600
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->MUTUALITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738601
    :cond_6
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738602
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PENDING:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738603
    :cond_7
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738604
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738605
    :cond_8
    const-string v0, "PYMK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PYMK:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738607
    :cond_9
    const-string v0, "RECENT_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738608
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->RECENT_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738609
    :cond_a
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738610
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0

    .line 738611
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .locals 1

    .prologue
    .line 738587
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;
    .locals 1

    .prologue
    .line 738586
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    return-object v0
.end method
