.class public final enum Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum ACCEPTED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum DELAYED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum DELIVERED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum ETA:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "ETA"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ETA:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "IN_TRANSIT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "OUT_FOR_DELIVERY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "DELIVERED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELIVERED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    const-string v1, "DELAYED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELAYED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740074
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ETA:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELIVERED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELAYED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740075
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;
    .locals 1

    .prologue
    .line 740076
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 740077
    :goto_0
    return-object v0

    .line 740078
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740079
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740080
    :cond_2
    const-string v0, "ETA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ETA:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740082
    :cond_3
    const-string v0, "ACCEPTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740083
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->ACCEPTED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740084
    :cond_4
    const-string v0, "IN_TRANSIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740085
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740086
    :cond_5
    const-string v0, "OUT_FOR_DELIVERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740087
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740088
    :cond_6
    const-string v0, "DELIVERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740089
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELIVERED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740090
    :cond_7
    const-string v0, "DELAYED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740091
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->DELAYED:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0

    .line 740092
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;
    .locals 1

    .prologue
    .line 740093
    const-class v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;
    .locals 1

    .prologue
    .line 740094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    return-object v0
.end method
