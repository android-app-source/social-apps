.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum ADDRESS_1:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum ADDRESS_2:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum COUNTRY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum FULL_ADDRESS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum STATE_OR_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

.field public static final enum ZIPCODE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737622
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737623
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->CITY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737624
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "FULL_ADDRESS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->FULL_ADDRESS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737625
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "ADDRESS_1"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_1:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "ADDRESS_2"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_2:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737627
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "STATE_OR_PROVINCE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->STATE_OR_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737628
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "ZIPCODE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737629
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const-string v1, "COUNTRY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737630
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->CITY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->FULL_ADDRESS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_1:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_2:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->STATE_OR_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737631
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 1

    .prologue
    .line 737632
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 737633
    :goto_0
    return-object v0

    .line 737634
    :cond_1
    const-string v0, "ADDRESS_1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737635
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_1:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737636
    :cond_2
    const-string v0, "ADDRESS_2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ADDRESS_2:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737638
    :cond_3
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737639
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->CITY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737640
    :cond_4
    const-string v0, "STATE_OR_PROVINCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737641
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->STATE_OR_PROVINCE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737642
    :cond_5
    const-string v0, "ZIPCODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737643
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737644
    :cond_6
    const-string v0, "COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737646
    :cond_7
    const-string v0, "FULL_ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->FULL_ADDRESS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0

    .line 737648
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 1

    .prologue
    .line 737649
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 1

    .prologue
    .line 737650
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method
