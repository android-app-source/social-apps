.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

.field public static final enum MAP:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

.field public static final enum MAP_PIN:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

.field public static final enum REGULAR:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729235
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 729236
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->REGULAR:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 729237
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 729238
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    const-string v1, "MAP_PIN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP_PIN:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 729239
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->REGULAR:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP_PIN:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
    .locals 1

    .prologue
    .line 729225
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 729226
    :goto_0
    return-object v0

    .line 729227
    :cond_1
    const-string v0, "REGULAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->REGULAR:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    goto :goto_0

    .line 729229
    :cond_2
    const-string v0, "MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    goto :goto_0

    .line 729231
    :cond_3
    const-string v0, "MAP_PIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->MAP_PIN:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    goto :goto_0

    .line 729233
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
    .locals 1

    .prologue
    .line 729224
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
    .locals 1

    .prologue
    .line 729223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    return-object v0
.end method
