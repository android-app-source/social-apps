.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum CHECK_BOX_TEXT_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum CUSTOMIZED_DISCLAIMER:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum DISCLAIMER_PAGE_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum PRIVACY_LINK_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum PRIVACY_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum SHORT_SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728018
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "SHORT_SECURE_SHARING_TEXT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SHORT_SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "SECURE_SHARING_TEXT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "PRIVACY_WITH_PHONE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "CHECK_BOX_TEXT_WITH_PHONE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CHECK_BOX_TEXT_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "PRIVACY_LINK_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_LINK_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "DISCLAIMER_PAGE_TITLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->DISCLAIMER_PAGE_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const-string v1, "CUSTOMIZED_DISCLAIMER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CUSTOMIZED_DISCLAIMER:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728026
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SHORT_SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CHECK_BOX_TEXT_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_LINK_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->DISCLAIMER_PAGE_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CUSTOMIZED_DISCLAIMER:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728027
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
    .locals 1

    .prologue
    .line 728028
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 728029
    :goto_0
    return-object v0

    .line 728030
    :cond_1
    const-string v0, "SHORT_SECURE_SHARING_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728031
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SHORT_SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728032
    :cond_2
    const-string v0, "SECURE_SHARING_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728033
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728034
    :cond_3
    const-string v0, "PRIVACY_WITH_PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728036
    :cond_4
    const-string v0, "CHECK_BOX_TEXT_WITH_PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728037
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CHECK_BOX_TEXT_WITH_PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728038
    :cond_5
    const-string v0, "PRIVACY_LINK_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728039
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_LINK_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728040
    :cond_6
    const-string v0, "DISCLAIMER_PAGE_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728041
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->DISCLAIMER_PAGE_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728042
    :cond_7
    const-string v0, "CUSTOMIZED_DISCLAIMER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728043
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->CUSTOMIZED_DISCLAIMER:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0

    .line 728044
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
    .locals 1

    .prologue
    .line 728045
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
    .locals 1

    .prologue
    .line 728046
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    return-object v0
.end method
