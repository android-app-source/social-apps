.class public final enum Lcom/facebook/graphql/enums/GraphQLRideStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLRideStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum REQUESTED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum RIDE_IN_PROGRESS:Lcom/facebook/graphql/enums/GraphQLRideStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739767
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739768
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "REQUESTED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739769
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "DRIVER_ON_THE_WAY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739770
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "DRIVER_ARRIVING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739771
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "DRIVER_CANCELED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739772
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "RIDER_CANCELED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739773
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "RIDE_IN_PROGRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_IN_PROGRESS:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739774
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    const-string v1, "RIDE_COMPLETE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRideStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739775
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLRideStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_IN_PROGRESS:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRideStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739776
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideStatus;
    .locals 1

    .prologue
    .line 739777
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    .line 739778
    :goto_0
    return-object v0

    .line 739779
    :cond_1
    const-string v0, "REQUESTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739781
    :cond_2
    const-string v0, "DRIVER_ON_THE_WAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739783
    :cond_3
    const-string v0, "DRIVER_ARRIVING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739784
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739785
    :cond_4
    const-string v0, "DRIVER_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739786
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739787
    :cond_5
    const-string v0, "RIDER_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739788
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739789
    :cond_6
    const-string v0, "RIDE_IN_PROGRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739790
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_IN_PROGRESS:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739791
    :cond_7
    const-string v0, "RIDE_COMPLETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739792
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0

    .line 739793
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideStatus;
    .locals 1

    .prologue
    .line 739794
    const-class v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLRideStatus;
    .locals 1

    .prologue
    .line 739795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRideStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLRideStatus;

    return-object v0
.end method
