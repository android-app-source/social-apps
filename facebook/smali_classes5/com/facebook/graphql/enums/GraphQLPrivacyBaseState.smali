.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public static final enum EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public static final enum FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public static final enum FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public static final enum SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 578099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const-string v1, "EVERYONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578102
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const-string v1, "FRIENDS_OF_FRIENDS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578103
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const-string v1, "SELF"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578104
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 578107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;
    .locals 1

    .prologue
    .line 578108
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 578109
    :goto_0
    return-object v0

    .line 578110
    :cond_1
    const-string v0, "EVERYONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578111
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    goto :goto_0

    .line 578112
    :cond_2
    const-string v0, "FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 578113
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    goto :goto_0

    .line 578114
    :cond_3
    const-string v0, "FRIENDS_OF_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 578115
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    goto :goto_0

    .line 578116
    :cond_4
    const-string v0, "SELF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 578117
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    goto :goto_0

    .line 578118
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;
    .locals 1

    .prologue
    .line 578106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;
    .locals 1

    .prologue
    .line 578105
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    return-object v0
.end method
