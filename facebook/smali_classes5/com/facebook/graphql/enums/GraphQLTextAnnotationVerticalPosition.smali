.class public final enum Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum BOTTOM:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum CENTER:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum TOP:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740491
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740492
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->TOP:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740493
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->CENTER:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740494
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740495
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740496
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    const-string v1, "ABOVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740497
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->TOP:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->CENTER:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;
    .locals 1

    .prologue
    .line 740499
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    .line 740500
    :goto_0
    return-object v0

    .line 740501
    :cond_1
    const-string v0, "TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->TOP:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0

    .line 740503
    :cond_2
    const-string v0, "CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->CENTER:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0

    .line 740505
    :cond_3
    const-string v0, "BOTTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0

    .line 740507
    :cond_4
    const-string v0, "BELOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0

    .line 740509
    :cond_5
    const-string v0, "ABOVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0

    .line 740511
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;
    .locals 1

    .prologue
    .line 740512
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;
    .locals 1

    .prologue
    .line 740513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    return-object v0
.end method
