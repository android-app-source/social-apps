.class public final enum Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum ARTS_CULTURE_HUMANITIES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum EDUCATION:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum ENVIRONMENT_AND_ANIMALS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum HEALTH:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum HUMAN_SERVICES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum INTERNATIONAL_FOREIGN_AFFAIRS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum MUTUAL_MEMBERSHIP_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum PUBLIC_SOCIETAL_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum RELIGION_RELATED:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "ARTS_CULTURE_HUMANITIES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ARTS_CULTURE_HUMANITIES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724075
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "EDUCATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "ENVIRONMENT_AND_ANIMALS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ENVIRONMENT_AND_ANIMALS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "HEALTH"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HEALTH:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "HUMAN_SERVICES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HUMAN_SERVICES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "INTERNATIONAL_FOREIGN_AFFAIRS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->INTERNATIONAL_FOREIGN_AFFAIRS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "PUBLIC_SOCIETAL_BENEFIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->PUBLIC_SOCIETAL_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724081
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "RELIGION_RELATED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->RELIGION_RELATED:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724082
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    const-string v1, "MUTUAL_MEMBERSHIP_BENEFIT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->MUTUAL_MEMBERSHIP_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724083
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ARTS_CULTURE_HUMANITIES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ENVIRONMENT_AND_ANIMALS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HEALTH:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HUMAN_SERVICES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->INTERNATIONAL_FOREIGN_AFFAIRS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->PUBLIC_SOCIETAL_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->RELIGION_RELATED:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->MUTUAL_MEMBERSHIP_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724084
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
    .locals 1

    .prologue
    .line 724085
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    .line 724086
    :goto_0
    return-object v0

    .line 724087
    :cond_1
    const-string v0, "ARTS_CULTURE_HUMANITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724088
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ARTS_CULTURE_HUMANITIES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724089
    :cond_2
    const-string v0, "EDUCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724091
    :cond_3
    const-string v0, "ENVIRONMENT_AND_ANIMALS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724092
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->ENVIRONMENT_AND_ANIMALS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724093
    :cond_4
    const-string v0, "HEALTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HEALTH:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724095
    :cond_5
    const-string v0, "HUMAN_SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->HUMAN_SERVICES:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724097
    :cond_6
    const-string v0, "INTERNATIONAL_FOREIGN_AFFAIRS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->INTERNATIONAL_FOREIGN_AFFAIRS:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724099
    :cond_7
    const-string v0, "PUBLIC_SOCIETAL_BENEFIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->PUBLIC_SOCIETAL_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724101
    :cond_8
    const-string v0, "RELIGION_RELATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->RELIGION_RELATED:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724103
    :cond_9
    const-string v0, "MUTUAL_MEMBERSHIP_BENEFIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->MUTUAL_MEMBERSHIP_BENEFIT:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0

    .line 724105
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
    .locals 1

    .prologue
    .line 724106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;
    .locals 1

    .prologue
    .line 724107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    return-object v0
.end method
