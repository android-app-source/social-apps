.class public final enum Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum ADS_INTEGRITY_USS_AD_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum AUDIENCE_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum BALLOT_CANDIDATE_OFFICES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum BALLOT_OFFICE_CANDIDATES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum CARRIER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum CM_MIGRATION_ISSUES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum DATA_FETCHER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum DEVELOPER_EVENT_FIVE_STAR_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum DEVELOPER_OPTIONS_LIST_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum DEVELOPER_STAR_RATING_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum DEVELOPER_THANK_YOU_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum FEED_STORY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum FEED_TOPIC_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum FRIENDS_MATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum FRIEND_AUDIT_SURVEY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum FRIEND_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum GOODWILL_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum GROUP_CREATE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum ISP_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum LANGUAGE_DIALECTS_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum LANGUAGE_DIALECTS_SELECTION_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum MOBILE_FEED_AD_STORY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum NETWORK_PROVIDER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum PAGE_AND_PROFILE_CONNECTEDNESS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum PAYMENT_SETTINGS_HELP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum PHONE_AND_EMAIL:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum PHONE_CLAIMING:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum QP_PREVIEW:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum REMOTE_RESEARCH:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum SPOKEN_LANGUAGES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum SPOKEN_LANGUAGE_DIALECTS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum STORY_REF:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum THROWBACK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum UNIVERSAL_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum UNIVERSAL_FEEDBACK_TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740183
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740184
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "AUDIENCE_PREFERENCES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->AUDIENCE_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740185
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "CM_MIGRATION_ISSUES"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CM_MIGRATION_ISSUES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740186
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "DATA_FETCHER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DATA_FETCHER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740187
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "FEED_STORY_PHOTO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_STORY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740188
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "FEED_TOPIC_TYPEAHEAD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_TOPIC_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740189
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "FRIEND_AUDIT_SURVEY_MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_AUDIT_SURVEY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740190
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "FRIEND_TYPEAHEAD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740191
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "FRIENDS_MATRIX"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIENDS_MATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740192
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "GOODWILL_PINNED_UNIT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GOODWILL_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740193
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "GROUP_CREATE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GROUP_CREATE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740194
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "MOBILE_FEED_AD_STORY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->MOBILE_FEED_AD_STORY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740195
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "NETWORK_PROVIDER_DETECTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NETWORK_PROVIDER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740196
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "NOTIFICATION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740197
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "PAGE_AND_PROFILE_CONNECTEDNESS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAGE_AND_PROFILE_CONNECTEDNESS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740198
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "PAYMENT_SETTINGS_HELP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAYMENT_SETTINGS_HELP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740199
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "PHONE_AND_EMAIL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_AND_EMAIL:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740200
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "PHONE_CLAIMING"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_CLAIMING:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740201
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "QP_PREVIEW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->QP_PREVIEW:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740202
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "REMOTE_RESEARCH"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->REMOTE_RESEARCH:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "SPOKEN_LANGUAGES"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740204
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "STORY_REF"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->STORY_REF:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740205
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "THROWBACK_PROMOTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->THROWBACK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740206
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "UNIVERSAL_FEEDBACK"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740207
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "UNIVERSAL_FEEDBACK_TEXT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK_TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740208
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "DEVELOPER_EVENT_FIVE_STAR_SURVEY"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_EVENT_FIVE_STAR_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "CARRIER_DETECTION"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CARRIER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "ADS_INTEGRITY_USS_AD_SURVEY"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ADS_INTEGRITY_USS_AD_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740211
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "ISP_DETECTION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ISP_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740212
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "DEVELOPER_STAR_RATING_SURVEY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_STAR_RATING_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740213
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "DEVELOPER_OPTIONS_LIST_SURVEY"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_OPTIONS_LIST_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740214
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "DEVELOPER_THANK_YOU_SURVEY"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_THANK_YOU_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740215
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "SPOKEN_LANGUAGE_DIALECTS"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGE_DIALECTS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740216
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "BALLOT_OFFICE_CANDIDATES"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_OFFICE_CANDIDATES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740217
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "BALLOT_CANDIDATE_OFFICES"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_CANDIDATE_OFFICES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740218
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "LANGUAGE_DIALECTS_LIKERT"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740219
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    const-string v1, "LANGUAGE_DIALECTS_SELECTION_LIKERT"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_SELECTION_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740220
    const/16 v0, 0x25

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->AUDIENCE_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CM_MIGRATION_ISSUES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DATA_FETCHER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_STORY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_TOPIC_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_AUDIT_SURVEY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIENDS_MATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GOODWILL_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GROUP_CREATE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->MOBILE_FEED_AD_STORY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NETWORK_PROVIDER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAGE_AND_PROFILE_CONNECTEDNESS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAYMENT_SETTINGS_HELP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_AND_EMAIL:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_CLAIMING:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->QP_PREVIEW:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->REMOTE_RESEARCH:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->STORY_REF:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->THROWBACK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK_TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_EVENT_FIVE_STAR_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CARRIER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ADS_INTEGRITY_USS_AD_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ISP_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_STAR_RATING_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_OPTIONS_LIST_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_THANK_YOU_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGE_DIALECTS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_OFFICE_CANDIDATES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_CANDIDATE_OFFICES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_SELECTION_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;
    .locals 2

    .prologue
    .line 740222
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    .line 740223
    :goto_0
    return-object v0

    .line 740224
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 740225
    packed-switch v0, :pswitch_data_0

    .line 740226
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740227
    :pswitch_1
    const-string v0, "AUDIENCE_PREFERENCES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->AUDIENCE_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740229
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740230
    :pswitch_2
    const-string v0, "CM_MIGRATION_ISSUES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CM_MIGRATION_ISSUES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740232
    :cond_3
    const-string v0, "ADS_INTEGRITY_USS_AD_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ADS_INTEGRITY_USS_AD_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740234
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740235
    :pswitch_3
    const-string v0, "QP_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->QP_PREVIEW:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740237
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740238
    :pswitch_4
    const-string v0, "DEVELOPER_THANK_YOU_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_THANK_YOU_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740240
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740241
    :pswitch_5
    const-string v0, "CARRIER_DETECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->CARRIER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740243
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto :goto_0

    .line 740244
    :pswitch_6
    const-string v0, "FEED_STORY_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_STORY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740246
    :cond_8
    const-string v0, "GOODWILL_PINNED_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GOODWILL_PINNED_UNIT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740248
    :cond_9
    const-string v0, "DEVELOPER_STAR_RATING_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740249
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_STAR_RATING_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740250
    :cond_a
    const-string v0, "BALLOT_OFFICE_CANDIDATES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 740251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_OFFICE_CANDIDATES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740252
    :cond_b
    const-string v0, "BALLOT_CANDIDATE_OFFICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 740253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->BALLOT_CANDIDATE_OFFICES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740254
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740255
    :pswitch_7
    const-string v0, "ISP_DETECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 740256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->ISP_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740257
    :cond_d
    const-string v0, "MOBILE_FEED_AD_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 740258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->MOBILE_FEED_AD_STORY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740259
    :cond_e
    const-string v0, "DEVELOPER_OPTIONS_LIST_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 740260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_OPTIONS_LIST_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740261
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740262
    :pswitch_8
    const-string v0, "DEVELOPER_EVENT_FIVE_STAR_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 740263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DEVELOPER_EVENT_FIVE_STAR_SURVEY:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740264
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740265
    :pswitch_9
    const-string v0, "NOTIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 740266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740267
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740268
    :pswitch_a
    const-string v0, "GROUP_CREATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 740269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->GROUP_CREATE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740270
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740271
    :pswitch_b
    const-string v0, "SPOKEN_LANGUAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 740272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGES:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740273
    :cond_13
    const-string v0, "LANGUAGE_DIALECTS_LIKERT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 740274
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740275
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740276
    :pswitch_c
    const-string v0, "FRIEND_TYPEAHEAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 740277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740278
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740279
    :pswitch_d
    const-string v0, "PHONE_AND_EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 740280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_AND_EMAIL:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740281
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740282
    :pswitch_e
    const-string v0, "PAYMENT_SETTINGS_HELP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 740283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAYMENT_SETTINGS_HELP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740284
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740285
    :pswitch_f
    const-string v0, "FEED_TOPIC_TYPEAHEAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 740286
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FEED_TOPIC_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740287
    :cond_18
    const-string v0, "STORY_REF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 740288
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->STORY_REF:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740289
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740290
    :pswitch_10
    const-string v0, "PHONE_CLAIMING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 740291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PHONE_CLAIMING:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740292
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740293
    :pswitch_11
    const-string v0, "UNIVERSAL_FEEDBACK_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 740294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK_TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740295
    :cond_1b
    const-string v0, "SPOKEN_LANGUAGE_DIALECTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 740296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->SPOKEN_LANGUAGE_DIALECTS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740297
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740298
    :pswitch_12
    const-string v0, "REMOTE_RESEARCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 740299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->REMOTE_RESEARCH:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740300
    :cond_1d
    const-string v0, "THROWBACK_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 740301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->THROWBACK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740302
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740303
    :pswitch_13
    const-string v0, "NETWORK_PROVIDER_DETECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 740304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NETWORK_PROVIDER_DETECTION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740305
    :cond_1f
    const-string v0, "LANGUAGE_DIALECTS_SELECTION_LIKERT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 740306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->LANGUAGE_DIALECTS_SELECTION_LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740307
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740308
    :pswitch_14
    const-string v0, "PAGE_AND_PROFILE_CONNECTEDNESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 740309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->PAGE_AND_PROFILE_CONNECTEDNESS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740310
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740311
    :pswitch_15
    const-string v0, "UNIVERSAL_FEEDBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 740312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNIVERSAL_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740313
    :cond_22
    const-string v0, "FRIENDS_MATRIX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 740314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIENDS_MATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740315
    :cond_23
    const-string v0, "FRIEND_AUDIT_SURVEY_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 740316
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->FRIEND_AUDIT_SURVEY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740317
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740318
    :pswitch_16
    const-string v0, "DATA_FETCHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 740319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->DATA_FETCHER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    .line 740320
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;
    .locals 1

    .prologue
    .line 740321
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;
    .locals 1

    .prologue
    .line 740322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    return-object v0
.end method
