.class public Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTypeDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 738381
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    new-instance v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTypeDeserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTypeDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 738382
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 738383
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 738380
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    return-object v0
.end method
