.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

.field public static final enum SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

.field public static final enum SEE_MORE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

.field public static final enum SEE_MORE_TO_SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729008
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    const-string v1, "SEE_ALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729009
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    const-string v1, "SEE_MORE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729010
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    const-string v1, "SEE_MORE_TO_SEE_ALL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE_TO_SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729011
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE_TO_SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729012
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;
    .locals 1

    .prologue
    .line 729013
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    .line 729014
    :goto_0
    return-object v0

    .line 729015
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729016
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    goto :goto_0

    .line 729017
    :cond_2
    const-string v0, "SEE_ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729018
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    goto :goto_0

    .line 729019
    :cond_3
    const-string v0, "SEE_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729020
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    goto :goto_0

    .line 729021
    :cond_4
    const-string v0, "SEE_MORE_TO_SEE_ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729022
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->SEE_MORE_TO_SEE_ALL:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    goto :goto_0

    .line 729023
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;
    .locals 1

    .prologue
    .line 729024
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;
    .locals 1

    .prologue
    .line 729025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    return-object v0
.end method
