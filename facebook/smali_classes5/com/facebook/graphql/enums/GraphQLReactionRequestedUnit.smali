.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum COMMERCE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum COMMERCE_EMPTY_SHOP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum CREATE_EVENT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum CRITIC_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum CTA_SETUP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_109:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_124:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_62:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_63:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_64:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_65:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_67:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_68:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum DEPRECATED_86:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum FUNDRAISERS_NATIVE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GAMETIME_TEAM_PAGE_RECENT_MATCHES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GAME_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GAME_CHALLENGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GAME_CTA_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GET_QUOTE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum GROUPS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum INVITE_FRIENDS_TO_LIKE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum LOCAL_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum LOCAL_PLACE_SURVEY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum MORE_GAMES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_ADDITIONAL_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_BUSINESS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_CAREER_AND_PERSONAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_EDIT_GENERAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_EMAIL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_INSTAGRAM_USERNAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_INTERESTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_LOCATION_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_LONG_DESC:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_MAP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_MAP_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_META_INFO_SECTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_MILESTONES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_MISSION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_OPERATION_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_PAGE_ID:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_PARKING_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_PHONECALL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_POLITICS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_PRIVACY_POLICY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ABOUT_WEBSITE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ADMIN_SUMMARY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_AFTER_PARTY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_BROADCASTER_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CLAIM_UNOWNED_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_COMMUNITY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_COMPOSER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CONSTITUENCY_ARTICLES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CREATE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CREATION_TIP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_CURRENTLY_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_FAN_CLUB_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_FRIENDS_CITY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_CONTEXT_ROWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_HIGHLIGHTS_SEARCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_INFO_3D_TOUCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_JOBS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_JOBS_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_JOB_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_LIKE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_MODAL_PAGE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_NOTES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_OFFERS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_OLD_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PHOTOS_HSCROLLABLE_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PHOTOS_SOCIAL_CONTEXT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PLACES_SET_SEARCH_PIVOTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_MOBILE_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_MOBILE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_MOBILE_POST_1:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_POSTS_MOBILE_POST_2:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PROFILE_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PAGE_SERVICES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PERMALINK_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PHOTOS_AND_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PHOTOS_AT_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PHOTOS_BY_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PHOTOS_OF_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PINNED_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum POSTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum POSTS_TO_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PRIVATE_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum PUBLIC_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum REQUEST_APPOINTMENT_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum SERVICES_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum SHOP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum UNKNOWN_UNIT_TYPE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum UNOWNED_PAGE_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public static final enum WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "CONTEXT_ROWS_PLACEHOLDER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "RELATED_PAGES_PLACEHOLDER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "SERVICES_NUX"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SERVICES_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_4"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_SERVICES_CARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_SERVICES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "SHOP_NUX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SHOP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PHOTOS_BY_PAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_BY_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "LOCAL_CONTENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PHOTOS_OF_PAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_OF_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739046
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PHOTOS_AT_PAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AT_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_INFO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_INFO_3D_TOUCH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO_3D_TOUCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739049
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "COMMERCE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739050
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "COMMERCE_EMPTY_SHOP"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE_EMPTY_SHOP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739051
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PUBLIC_GROUPS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PUBLIC_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739052
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PRIVATE_GROUPS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PRIVATE_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "VIDEOS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "CHILD_LOCATIONS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PINNED_POST"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PINNED_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739056
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "UPCOMING_EVENTS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "FUNDRAISERS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "FUNDRAISERS_NATIVE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS_NATIVE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "POSTS_TO_PAGE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS_TO_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739060
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "REVIEWS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739061
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "REVIEW_COMPOSER"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739062
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "WELCOME_HOME"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GAME_CTA_ACTION_BAR"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CTA_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739064
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "UNOWNED_PAGE_ATTRIBUTION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNOWNED_PAGE_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739065
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GAME_CHALLENGE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CHALLENGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GAME_ABOUT"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "MORE_GAMES"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->MORE_GAMES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "CRITIC_REVIEWS"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CRITIC_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "OFFERS"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "INVITE_FRIENDS_TO_LIKE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->INVITE_FRIENDS_TO_LIKE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "UNKNOWN_UNIT_TYPE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNKNOWN_UNIT_TYPE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PHOTOS_AND_ALBUMS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AND_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_BACKGROUND"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_CONTACT_INFO"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739075
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_EMAIL_INFO"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EMAIL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_INSTAGRAM_USERNAME"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INSTAGRAM_USERNAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_LONG_DESC"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LONG_DESC:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_MAP"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_MAP_INFO"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_OPERATION_HOURS"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPERATION_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739081
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_PARKING_INFO"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PARKING_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739082
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_PHONECALL"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PHONECALL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739083
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_WEBSITE_INFO"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_WEBSITE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739084
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_ABOUT"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739085
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_DESCRIPTION"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739086
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_OLD_INFO_CARD_PLACEHOLDER"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OLD_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739087
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_APPOINTMENT_STATUS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739088
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_LIKE_INVITE_CARD"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LIKE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739089
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_JOBS_CARD"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739090
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_JOBS_UNIT"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739091
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_OFFERS_CARD"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OFFERS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739092
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PHOTOS_SOCIAL_CONTEXT"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_SOCIAL_CONTEXT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739093
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PHOTOS_HSCROLLABLE_ALBUMS"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_HSCROLLABLE_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739094
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ADMIN_SUMMARY"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_SUMMARY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ADMIN_TIPS"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_COMPOSER_CARD"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMPOSER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GAMETIME_TEAM_PAGE_RECENT_MATCHES"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAMETIME_TEAM_PAGE_RECENT_MATCHES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_62"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_62:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_63"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_63:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_64"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_64:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_65"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_65:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739102
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739103
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_67"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_67:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739104
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_68"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_68:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739105
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739106
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739107
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739108
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739109
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_AFTER_PARTY_CARD"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_AFTER_PARTY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CONSTITUENCY_ARTICLES_CARD"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONSTITUENCY_ARTICLES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PERMALINK_POST"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PERMALINK_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739112
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PROFILE_OVERLAYS"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PROFILE_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "POSTS"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "LIVE_VIDEOS"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_JOB_CREATION"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_DIVIDER"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_FRIENDS_CITY_ACTIVITY"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_CITY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_JOB_UPSELL"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_META_INFO_SECTIONS"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_META_INFO_SECTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739121
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_NOTES_CARD"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_NOTES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_86"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_86:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739123
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_METABOX"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_CARD"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_PAYMENT_OPTIONS"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_LOCATION_INFO"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LOCATION_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_POLITICS_INFO"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_POLITICS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739128
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CATEGORY_BASED_RECOMMENDATIONS"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_ADMIN_TIPS"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_OPEN_HOURS"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_BUSINESS_INFO"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BUSINESS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_ADDITIONAL_CONTACT_INFO"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_ADDITIONAL_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PLACES_SET_SEARCH_PIVOTS"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PLACES_SET_SEARCH_PIVOTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_PRIVACY_POLICY"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PRIVACY_POLICY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_MILESTONES"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MILESTONES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GET_QUOTE_NUX"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GET_QUOTE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "REQUEST_APPOINTMENT_NUX"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REQUEST_APPOINTMENT_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "CTA_SETUP_NUX"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CTA_SETUP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_COMMUNITY_CARD"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMMUNITY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "GROUPS_CREATION"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GROUPS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CREATE_INLINE_UPSELL"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "UPCOMING_EVENTS_CARD"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_CAREER_AND_PERSONAL_INFO"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CAREER_AND_PERSONAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_109"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_109:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739146
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PYML_CITY_RECOMMENDATIONS"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739147
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_LOCATION_CARD"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739148
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CREATION_TIP_CARD"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATION_TIP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739149
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_INTERESTS"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INTERESTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739150
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_MISSION"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MISSION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739151
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CLAIM_UNOWNED_UPSELL"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CLAIM_UNOWNED_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739152
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_CURRENTLY_LIVE_VIDEO"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CURRENTLY_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "CREATE_EVENT_CARD"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CREATE_EVENT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_EDIT_GENERAL_INFO"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EDIT_GENERAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_BROADCASTER_ENTRY_POINT"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_BROADCASTER_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "LOCAL_PLACE_SURVEY"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_PLACE_SURVEY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_MODAL_PAGE_INVITE_CARD"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_MODAL_PAGE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_CONTEXT_ROWS"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_CONTEXT_ROWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_HIGHLIGHTS_SEARCH"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_SEARCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "DEPRECATED_124"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_124:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_FAN_CLUB_CARD"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FAN_CLUB_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_MOBILE_FOOTER"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_MOBILE_HEADER"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_MOBILE_POST_1"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_1:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_POSTS_MOBILE_POST_2"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_2:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_ABOUT_PAGE_ID"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAGE_ID:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const-string v1, "PAGE_PHOTOS"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739168
    const/16 v0, 0x84

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SERVICES_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_SERVICES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SHOP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_BY_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_OF_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AT_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO_3D_TOUCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE_EMPTY_SHOP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PUBLIC_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PRIVATE_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PINNED_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS_NATIVE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS_TO_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CTA_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNOWNED_PAGE_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CHALLENGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->MORE_GAMES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CRITIC_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->INVITE_FRIENDS_TO_LIKE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNKNOWN_UNIT_TYPE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AND_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EMAIL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INSTAGRAM_USERNAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LONG_DESC:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPERATION_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PARKING_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PHONECALL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_WEBSITE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OLD_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LIKE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OFFERS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_SOCIAL_CONTEXT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_HSCROLLABLE_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_SUMMARY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMPOSER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAMETIME_TEAM_PAGE_RECENT_MATCHES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_62:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_63:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_64:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_65:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_67:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_68:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_AFTER_PARTY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONSTITUENCY_ARTICLES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PERMALINK_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PROFILE_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_CITY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_META_INFO_SECTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_NOTES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_86:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LOCATION_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_POLITICS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BUSINESS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_ADDITIONAL_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PLACES_SET_SEARCH_PIVOTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PRIVACY_POLICY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MILESTONES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GET_QUOTE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REQUEST_APPOINTMENT_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CTA_SETUP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMMUNITY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GROUPS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CAREER_AND_PERSONAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_109:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATION_TIP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INTERESTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MISSION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CLAIM_UNOWNED_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CURRENTLY_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CREATE_EVENT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EDIT_GENERAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_BROADCASTER_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_PLACE_SURVEY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_MODAL_PAGE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_CONTEXT_ROWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_SEARCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->DEPRECATED_124:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FAN_CLUB_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_1:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_2:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAGE_ID:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
    .locals 2

    .prologue
    .line 739170
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 739171
    :goto_0
    return-object v0

    .line 739172
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 739173
    packed-switch v0, :pswitch_data_0

    .line 739174
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739175
    :pswitch_1
    const-string v0, "PAGE_CATEGORY_BASED_RECOMMENDATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739177
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739178
    :pswitch_2
    const-string v0, "UNKNOWN_UNIT_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNKNOWN_UNIT_TYPE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739180
    :cond_3
    const-string v0, "PAGE_ABOUT_LONG_DESC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LONG_DESC:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739182
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739183
    :pswitch_3
    const-string v0, "SHOP_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SHOP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739185
    :cond_5
    const-string v0, "PAGE_ABOUT_ADDITIONAL_CONTACT_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_ADDITIONAL_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739187
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739188
    :pswitch_4
    const-string v0, "PAGE_ABOUT_CAREER_AND_PERSONAL_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739189
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CAREER_AND_PERSONAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739190
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto :goto_0

    .line 739191
    :pswitch_5
    const-string v0, "LOCAL_CONTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739193
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739194
    :pswitch_6
    const-string v0, "COMMERCE_EMPTY_SHOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 739195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE_EMPTY_SHOP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739196
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739197
    :pswitch_7
    const-string v0, "SERVICES_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 739198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->SERVICES_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739199
    :cond_a
    const-string v0, "PINNED_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 739200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PINNED_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739201
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739202
    :pswitch_8
    const-string v0, "GROUPS_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739203
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GROUPS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739204
    :cond_c
    const-string v0, "GAME_CTA_ACTION_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739205
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CTA_ACTION_BAR:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739206
    :cond_d
    const-string v0, "PAGE_ABOUT_INSTAGRAM_USERNAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 739207
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INSTAGRAM_USERNAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739208
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739209
    :pswitch_9
    const-string v0, "CONTEXT_ROWS_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 739210
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739211
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739212
    :pswitch_a
    const-string v0, "PAGE_JOBS_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 739213
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739214
    :cond_10
    const-string v0, "PERMALINK_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 739215
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PERMALINK_POST:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739216
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739217
    :pswitch_b
    const-string v0, "PAGE_ABOUT_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 739218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739219
    :cond_12
    const-string v0, "PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 739220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_PICTURE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739221
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739222
    :pswitch_c
    const-string v0, "REVIEW_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 739223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739224
    :cond_14
    const-string v0, "PAGE_HIGHLIGHTS_METABOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 739225
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739226
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739227
    :pswitch_d
    const-string v0, "CREATE_EVENT_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 739228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CREATE_EVENT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739229
    :cond_16
    const-string v0, "PAGE_POSTS_DIVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 739230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739231
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739232
    :pswitch_e
    const-string v0, "REQUEST_APPOINTMENT_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 739233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REQUEST_APPOINTMENT_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739234
    :cond_18
    const-string v0, "PAGE_HIGHLIGHTS_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 739235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739236
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739237
    :pswitch_f
    const-string v0, "PAGE_JOB_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 739238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739239
    :cond_1a
    const-string v0, "PAGE_JOB_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 739240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739241
    :cond_1b
    const-string v0, "PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 739242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_POSTS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739243
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739244
    :pswitch_10
    const-string v0, "PAGE_ABOUT_MISSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 739245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MISSION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739246
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739247
    :pswitch_11
    const-string v0, "PAGE_PHOTOS_SOCIAL_CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 739248
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_SOCIAL_CONTEXT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739249
    :cond_1e
    const-string v0, "PAGE_POSTS_MOBILE_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 739250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739251
    :cond_1f
    const-string v0, "PAGE_POSTS_MOBILE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 739252
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739253
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739254
    :pswitch_12
    const-string v0, "PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 739255
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_OF_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739256
    :cond_21
    const-string v0, "PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 739257
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_BY_FRIENDS_AT_PLACE_PAGE_SURFACE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739258
    :cond_22
    const-string v0, "PAGE_POSTS_MOBILE_POST_1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 739259
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_1:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739260
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739261
    :pswitch_13
    const-string v0, "PAGE_ABOUT_PHONECALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 739262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PHONECALL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739263
    :cond_24
    const-string v0, "PAGE_BROADCASTER_ENTRY_POINT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 739264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_BROADCASTER_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739265
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739266
    :pswitch_14
    const-string v0, "RELATED_PAGES_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 739267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739268
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739269
    :pswitch_15
    const-string v0, "PAGE_INFO_3D_TOUCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 739270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO_3D_TOUCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739271
    :cond_27
    const-string v0, "PAGE_JOBS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 739272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_JOBS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739273
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739274
    :pswitch_16
    const-string v0, "PAGE_NOTES_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 739275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_NOTES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739276
    :cond_29
    const-string v0, "PAGE_POSTS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 739277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739278
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739279
    :pswitch_17
    const-string v0, "PAGE_OLD_INFO_CARD_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 739280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OLD_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739281
    :cond_2b
    const-string v0, "PAGE_OFFERS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 739282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_OFFERS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739283
    :cond_2c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739284
    :pswitch_18
    const-string v0, "PAGE_CLAIM_UNOWNED_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 739285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CLAIM_UNOWNED_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739286
    :cond_2d
    const-string v0, "PAGE_HIGHLIGHTS_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 739287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739288
    :cond_2e
    const-string v0, "PAGE_CREATE_INLINE_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 739289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739290
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739291
    :pswitch_19
    const-string v0, "PAGE_SERVICES_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 739292
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_SERVICES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739293
    :cond_30
    const-string v0, "FUNDRAISERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 739294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739295
    :cond_31
    const-string v0, "CRITIC_REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 739296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CRITIC_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739297
    :cond_32
    const-string v0, "PAGE_ABOUT_PAGE_ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 739298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAGE_ID:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739299
    :cond_33
    const-string v0, "PAGE_FAN_CLUB_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 739300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FAN_CLUB_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739301
    :cond_34
    const-string v0, "PAGE_HIGHLIGHTS_SEARCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 739302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_SEARCH:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739303
    :cond_35
    const-string v0, "PAGE_LOCATION_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 739304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739305
    :cond_36
    const-string v0, "PAGE_COMPOSER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 739306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMPOSER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739307
    :cond_37
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739308
    :pswitch_1a
    const-string v0, "CHILD_LOCATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 739309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739310
    :cond_38
    const-string v0, "UNOWNED_PAGE_ATTRIBUTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 739311
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNOWNED_PAGE_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739312
    :cond_39
    const-string v0, "PAGE_COMMUNITY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 739313
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_COMMUNITY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739314
    :cond_3a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739315
    :pswitch_1b
    const-string v0, "PAGE_ABOUT_BACKGROUND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 739316
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739317
    :cond_3b
    const-string v0, "PAGE_LIKE_INVITE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 739318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_LIKE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739319
    :cond_3c
    const-string v0, "PAGE_AFTER_PARTY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 739320
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_AFTER_PARTY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739321
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739322
    :pswitch_1c
    const-string v0, "POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 739323
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739324
    :cond_3e
    const-string v0, "OFFERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 739325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739326
    :cond_3f
    const-string v0, "PAGE_CREATION_TIP_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 739327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CREATION_TIP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739328
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739329
    :pswitch_1d
    const-string v0, "LIVE_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 739330
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739331
    :cond_41
    const-string v0, "MORE_GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 739332
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->MORE_GAMES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739333
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739334
    :pswitch_1e
    const-string v0, "UPCOMING_EVENTS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 739335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739336
    :cond_43
    const-string v0, "LOCAL_PLACE_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 739337
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->LOCAL_PLACE_SURVEY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739338
    :cond_44
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739339
    :pswitch_1f
    const-string v0, "COMMERCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 739340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->COMMERCE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739341
    :cond_45
    const-string v0, "REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 739342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739343
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739344
    :pswitch_20
    const-string v0, "PAGE_MODAL_PAGE_INVITE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 739345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_MODAL_PAGE_INVITE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739346
    :cond_47
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739347
    :pswitch_21
    const-string v0, "PAGE_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 739348
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739349
    :cond_48
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739350
    :pswitch_22
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 739351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739352
    :cond_49
    const-string v0, "PAGE_ADMIN_SUMMARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 739353
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_SUMMARY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739354
    :cond_4a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739355
    :pswitch_23
    const-string v0, "PAGE_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 739356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739357
    :cond_4b
    const-string v0, "PUBLIC_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 739358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PUBLIC_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739359
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739360
    :pswitch_24
    const-string v0, "PRIVATE_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 739361
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PRIVATE_GROUPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739362
    :cond_4d
    const-string v0, "PAGE_CONSTITUENCY_ARTICLES_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 739363
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONSTITUENCY_ARTICLES_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739364
    :cond_4e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739365
    :pswitch_25
    const-string v0, "PAGE_ADMIN_TIPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 739366
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739367
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739368
    :pswitch_26
    const-string v0, "PHOTOS_AND_ALBUMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 739369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AND_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739370
    :cond_50
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739371
    :pswitch_27
    const-string v0, "PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 739372
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_PROMOTIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739373
    :cond_51
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739374
    :pswitch_28
    const-string v0, "GAME_CHALLENGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 739375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_CHALLENGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739376
    :cond_52
    const-string v0, "PAGE_ABOUT_PRIVACY_POLICY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 739377
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PRIVACY_POLICY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739378
    :cond_53
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 739379
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739380
    :cond_54
    const-string v0, "PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 739381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ADMIN_OVERVIEW_MARKETWATCH_CARD:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739382
    :cond_55
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739383
    :pswitch_29
    const-string v0, "UPCOMING_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 739384
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739385
    :cond_56
    const-string v0, "PAGE_ABOUT_INTERESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 739386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_INTERESTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739387
    :cond_57
    const-string v0, "PAGE_FRIENDS_CITY_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 739388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_FRIENDS_CITY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739389
    :cond_58
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739390
    :pswitch_2a
    const-string v0, "PAGE_ABOUT_MILESTONES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 739391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MILESTONES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739392
    :cond_59
    const-string v0, "PAGE_ABOUT_OPEN_HOURS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 739393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739394
    :cond_5a
    const-string v0, "PAGE_PROFILE_OVERLAYS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 739395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PROFILE_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739396
    :cond_5b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739397
    :pswitch_2b
    const-string v0, "FUNDRAISERS_NATIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 739398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->FUNDRAISERS_NATIVE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739399
    :cond_5c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739400
    :pswitch_2c
    const-string v0, "PAGE_ABOUT_MAP_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 739401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_MAP_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739402
    :cond_5d
    const-string v0, "PAGE_APPOINTMENT_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 739403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739404
    :cond_5e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739405
    :pswitch_2d
    const-string v0, "GAMETIME_TEAM_PAGE_RECENT_MATCHES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 739406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAMETIME_TEAM_PAGE_RECENT_MATCHES:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739407
    :cond_5f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739408
    :pswitch_2e
    const-string v0, "PAGE_ABOUT_EMAIL_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 739409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EMAIL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739410
    :cond_60
    const-string v0, "PAGE_POSTS_MOBILE_POST_2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 739411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_POSTS_MOBILE_POST_2:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739412
    :cond_61
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739413
    :pswitch_2f
    const-string v0, "PAGE_ABOUT_OPERATION_HOURS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 739414
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_OPERATION_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739415
    :cond_62
    const-string v0, "PAGE_ABOUT_PAYMENT_OPTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 739416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739417
    :cond_63
    const-string v0, "PAGE_HIGHLIGHTS_ADMIN_TIPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 739418
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_ADMIN_TIPS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739419
    :cond_64
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739420
    :pswitch_30
    const-string v0, "CTA_SETUP_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 739421
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->CTA_SETUP_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739422
    :cond_65
    const-string v0, "POSTS_TO_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 739423
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->POSTS_TO_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739424
    :cond_66
    const-string v0, "PAGE_ABOUT_CONTACT_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 739425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739426
    :cond_67
    const-string v0, "PAGE_ABOUT_PARKING_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 739427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_PARKING_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739428
    :cond_68
    const-string v0, "PAGE_ABOUT_WEBSITE_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 739429
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_WEBSITE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739430
    :cond_69
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739431
    :pswitch_31
    const-string v0, "PAGE_ABOUT_BUSINESS_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 739432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_BUSINESS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739433
    :cond_6a
    const-string v0, "PAGE_ABOUT_LOCATION_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 739434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_LOCATION_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739435
    :cond_6b
    const-string v0, "PAGE_ABOUT_POLITICS_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 739436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_POLITICS_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739437
    :cond_6c
    const-string v0, "PAGE_HIGHLIGHTS_CONTEXT_ROWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 739438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_HIGHLIGHTS_CONTEXT_ROWS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739439
    :cond_6d
    const-string v0, "PHOTOS_OF_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 739440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_OF_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739441
    :cond_6e
    const-string v0, "PHOTOS_AT_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 739442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_AT_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739443
    :cond_6f
    const-string v0, "PHOTOS_BY_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 739444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PHOTOS_BY_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739445
    :cond_70
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739446
    :pswitch_32
    const-string v0, "INVITE_FRIENDS_TO_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 739447
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->INVITE_FRIENDS_TO_LIKE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739448
    :cond_71
    const-string v0, "PAGE_ABOUT_META_INFO_SECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 739449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_META_INFO_SECTIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739450
    :cond_72
    const-string v0, "PAGE_CURRENTLY_LIVE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 739451
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_CURRENTLY_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739452
    :cond_73
    const-string v0, "PAGE_PLACES_SET_SEARCH_PIVOTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 739453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PLACES_SET_SEARCH_PIVOTS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739454
    :cond_74
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739455
    :pswitch_33
    const-string v0, "PAGE_PHOTOS_HSCROLLABLE_ALBUMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 739456
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PHOTOS_HSCROLLABLE_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739457
    :cond_75
    const-string v0, "PAGE_PYML_CITY_RECOMMENDATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 739458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739459
    :cond_76
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739460
    :pswitch_34
    const-string v0, "GET_QUOTE_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 739461
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GET_QUOTE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739462
    :cond_77
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739463
    :pswitch_35
    const-string v0, "GAME_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 739464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->GAME_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739465
    :cond_78
    const-string v0, "PAGE_ABOUT_EDIT_GENERAL_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 739466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->PAGE_ABOUT_EDIT_GENERAL_INFO:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739467
    :cond_79
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739468
    :pswitch_36
    const-string v0, "WELCOME_HOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 739469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    .line 739470
    :cond_7a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
    .locals 1

    .prologue
    .line 739471
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
    .locals 1

    .prologue
    .line 739472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    return-object v0
.end method
