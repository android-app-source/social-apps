.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum ON_UPDATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum PAGINATION_NEXT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum PAGINATION_PREVIOUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "ON_TAP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "PAGINATION_NEXT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_NEXT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737559
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "PAGINATION_PREVIOUS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_PREVIOUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "ON_UPDATE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_UPDATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "ON_LOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "ON_ITEM_SELECT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const-string v1, "ON_ITEM_REMOVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737564
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_NEXT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_PREVIOUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_UPDATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737567
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;
    .locals 1

    .prologue
    .line 737568
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    .line 737569
    :goto_0
    return-object v0

    .line 737570
    :cond_1
    const-string v0, "ON_TAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737571
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737572
    :cond_2
    const-string v0, "ON_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737573
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_UPDATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737574
    :cond_3
    const-string v0, "PAGINATION_NEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737575
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_NEXT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737576
    :cond_4
    const-string v0, "PAGINATION_PREVIOUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->PAGINATION_PREVIOUS:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737578
    :cond_5
    const-string v0, "ON_LOADING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737579
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737580
    :cond_6
    const-string v0, "ON_ITEM_SELECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737581
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737582
    :cond_7
    const-string v0, "ON_ITEM_REMOVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737583
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0

    .line 737584
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;
    .locals 1

    .prologue
    .line 737566
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;
    .locals 1

    .prologue
    .line 737565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    return-object v0
.end method
