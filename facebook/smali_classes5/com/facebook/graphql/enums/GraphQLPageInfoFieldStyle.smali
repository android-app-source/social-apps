.class public final enum Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

.field public static final enum LINE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

.field public static final enum PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 736956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    .line 736957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    const-string v1, "LINE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->LINE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    .line 736958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    const-string v1, "PARAGRAPH"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    .line 736959
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->LINE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 736955
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;
    .locals 1

    .prologue
    .line 736948
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    .line 736949
    :goto_0
    return-object v0

    .line 736950
    :cond_1
    const-string v0, "LINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 736951
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->LINE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    goto :goto_0

    .line 736952
    :cond_2
    const-string v0, "PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 736953
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    goto :goto_0

    .line 736954
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;
    .locals 1

    .prologue
    .line 736946
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;
    .locals 1

    .prologue
    .line 736947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    return-object v0
.end method
