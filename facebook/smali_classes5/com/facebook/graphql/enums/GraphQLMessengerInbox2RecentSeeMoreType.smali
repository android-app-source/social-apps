.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

.field public static final enum BOTTOM_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

.field public static final enum FLOATING_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    .line 728973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    const-string v1, "BOTTOM_PILL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->BOTTOM_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    .line 728974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    const-string v1, "FLOATING_PILL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->FLOATING_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    .line 728975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    .line 728976
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->BOTTOM_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->FLOATING_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728977
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;
    .locals 1

    .prologue
    .line 728978
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    .line 728979
    :goto_0
    return-object v0

    .line 728980
    :cond_1
    const-string v0, "BOTTOM_PILL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->BOTTOM_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    goto :goto_0

    .line 728982
    :cond_2
    const-string v0, "FLOATING_PILL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728983
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->FLOATING_PILL:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    goto :goto_0

    .line 728984
    :cond_3
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728985
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->NONE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    goto :goto_0

    .line 728986
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;
    .locals 1

    .prologue
    .line 728987
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;
    .locals 1

    .prologue
    .line 728988
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    return-object v0
.end method
