.class public final enum Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum ACTIONS_ON_PAGE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_DELETE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_MARK_SPAM:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_NEW_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_SPONSOR_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum MESSAGES_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_ACTIVITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_MESSAGING_REACTION:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_PAGES_PLATFORM_CONVERSIONS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_PEOPLE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_POSTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_SAVES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum OVERVIEW_VISITS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum PAGE_VIEWS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum REACH:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737226
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737227
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "ACTIONS_ON_PAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->ACTIONS_ON_PAGE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737228
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "PAGE_VIEWS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_VIEWS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737229
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "PAGE_LIKES"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737230
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "REACH"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->REACH:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737231
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "POST_ENGAGEMENTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737232
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "VIDEOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737233
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_BLOCKS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737234
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_DELETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_DELETE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737235
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_MARK_SPAM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_MARK_SPAM:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737236
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_VISITS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VISITS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737237
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_VIDEOS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737238
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_POSTS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POSTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737239
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_POST_ENGAGEMENTS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737240
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_PEOPLE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PEOPLE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737241
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_MESSAGING_REACTION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_MESSAGING_REACTION:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737242
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_LIKES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737243
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_FOLLOWERS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737244
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_PAGES_PLATFORM_CONVERSIONS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PAGES_PLATFORM_CONVERSIONS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737245
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_ACTIVITY_CARD"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_ACTIVITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737246
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_THREADS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737247
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_SPONSOR_BLOCKS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_SPONSOR_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737248
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "MESSAGES_NEW_THREADS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_NEW_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737249
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    const-string v1, "OVERVIEW_SAVES"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_SAVES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737250
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->ACTIONS_ON_PAGE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_VIEWS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->REACH:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_DELETE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_MARK_SPAM:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VISITS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POSTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PEOPLE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_MESSAGING_REACTION:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PAGES_PLATFORM_CONVERSIONS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_ACTIVITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_SPONSOR_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_NEW_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_SAVES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737251
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
    .locals 1

    .prologue
    .line 737177
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    .line 737178
    :goto_0
    return-object v0

    .line 737179
    :cond_1
    const-string v0, "ACTIONS_ON_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737180
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->ACTIONS_ON_PAGE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737181
    :cond_2
    const-string v0, "PAGE_VIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_VIEWS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737183
    :cond_3
    const-string v0, "PAGE_LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->PAGE_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737185
    :cond_4
    const-string v0, "REACH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->REACH:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737187
    :cond_5
    const-string v0, "POST_ENGAGEMENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737189
    :cond_6
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737191
    :cond_7
    const-string v0, "MESSAGES_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737193
    :cond_8
    const-string v0, "MESSAGES_DELETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737194
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_DELETE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737195
    :cond_9
    const-string v0, "MESSAGES_MARK_SPAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 737196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_MARK_SPAM:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737197
    :cond_a
    const-string v0, "OVERVIEW_VISITS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 737198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VISITS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737199
    :cond_b
    const-string v0, "OVERVIEW_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 737200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto :goto_0

    .line 737201
    :cond_c
    const-string v0, "OVERVIEW_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 737202
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POSTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737203
    :cond_d
    const-string v0, "OVERVIEW_POST_ENGAGEMENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 737204
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_POST_ENGAGEMENTS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737205
    :cond_e
    const-string v0, "OVERVIEW_PEOPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 737206
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PEOPLE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737207
    :cond_f
    const-string v0, "OVERVIEW_MESSAGING_REACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 737208
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_MESSAGING_REACTION:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737209
    :cond_10
    const-string v0, "OVERVIEW_LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 737210
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_LIKES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737211
    :cond_11
    const-string v0, "OVERVIEW_FOLLOWERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 737212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737213
    :cond_12
    const-string v0, "OVERVIEW_PAGES_PLATFORM_CONVERSIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 737214
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_PAGES_PLATFORM_CONVERSIONS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737215
    :cond_13
    const-string v0, "OVERVIEW_ACTIVITY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 737216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_ACTIVITY_CARD:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737217
    :cond_14
    const-string v0, "OVERVIEW_SAVES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 737218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->OVERVIEW_SAVES:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737219
    :cond_15
    const-string v0, "MESSAGES_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 737220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737221
    :cond_16
    const-string v0, "MESSAGES_SPONSOR_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 737222
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_SPONSOR_BLOCKS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737223
    :cond_17
    const-string v0, "MESSAGES_NEW_THREADS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 737224
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->MESSAGES_NEW_THREADS:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0

    .line 737225
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
    .locals 1

    .prologue
    .line 737176
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
    .locals 1

    .prologue
    .line 737175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    return-object v0
.end method
