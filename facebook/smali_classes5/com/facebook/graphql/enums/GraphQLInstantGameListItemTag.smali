.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

.field public static final enum NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727594
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    .line 727595
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    .line 727596
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;
    .locals 1

    .prologue
    .line 727598
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    .line 727599
    :goto_0
    return-object v0

    .line 727600
    :cond_1
    const-string v0, "NEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    goto :goto_0

    .line 727602
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;
    .locals 1

    .prologue
    .line 727603
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;
    .locals 1

    .prologue
    .line 727604
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    return-object v0
.end method
