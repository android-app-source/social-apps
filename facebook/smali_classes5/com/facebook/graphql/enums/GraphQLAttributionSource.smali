.class public final enum Lcom/facebook/graphql/enums/GraphQLAttributionSource;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAttributionSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum AMEX:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum FREEBASE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum HARVARD:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum INGRAM:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum RANDOM_HOUSE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

.field public static final enum WIKIVOYAGE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723447
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723448
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "WIKIPEDIA"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723449
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "WIKIVOYAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIVOYAGE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723450
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "FREEBASE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->FREEBASE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723451
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "AMEX"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->AMEX:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723452
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "HARVARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->HARVARD:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723453
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "INGRAM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->INGRAM:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723454
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const-string v1, "RANDOM_HOUSE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->RANDOM_HOUSE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723455
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIVOYAGE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->FREEBASE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->AMEX:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->HARVARD:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->INGRAM:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->RANDOM_HOUSE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .locals 1

    .prologue
    .line 723457
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 723458
    :goto_0
    return-object v0

    .line 723459
    :cond_1
    const-string v0, "WIKIPEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723461
    :cond_2
    const-string v0, "WIKIVOYAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIVOYAGE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723463
    :cond_3
    const-string v0, "FREEBASE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->FREEBASE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723465
    :cond_4
    const-string v0, "AMEX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->AMEX:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723467
    :cond_5
    const-string v0, "HARVARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723468
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->HARVARD:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723469
    :cond_6
    const-string v0, "INGRAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723470
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->INGRAM:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723471
    :cond_7
    const-string v0, "RANDOM_HOUSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->RANDOM_HOUSE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0

    .line 723473
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .locals 1

    .prologue
    .line 723474
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .locals 1

    .prologue
    .line 723475
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    return-object v0
.end method
