.class public final enum Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum COMBINED:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum FRIENDS_VISITING_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum NEARBY:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum NEARBY_WHILE_TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum PULSE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726732
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726733
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "COMBINED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->COMBINED:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726734
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "NEARBY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726735
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "TRAVELING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726736
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "NEARBY_WHILE_TRAVELING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY_WHILE_TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726737
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "FRIENDS_VISITING_HOMETOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->FRIENDS_VISITING_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726738
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const-string v1, "PULSE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726739
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->COMBINED:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY_WHILE_TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->FRIENDS_VISITING_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726731
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;
    .locals 1

    .prologue
    .line 726714
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 726715
    :goto_0
    return-object v0

    .line 726716
    :cond_1
    const-string v0, "COMBINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726717
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->COMBINED:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726718
    :cond_2
    const-string v0, "NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726720
    :cond_3
    const-string v0, "TRAVELING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726722
    :cond_4
    const-string v0, "NEARBY_WHILE_TRAVELING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->NEARBY_WHILE_TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726724
    :cond_5
    const-string v0, "FRIENDS_VISITING_HOMETOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726725
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->FRIENDS_VISITING_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726726
    :cond_6
    const-string v0, "PULSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726727
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0

    .line 726728
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;
    .locals 1

    .prologue
    .line 726730
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;
    .locals 1

    .prologue
    .line 726729
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    return-object v0
.end method
