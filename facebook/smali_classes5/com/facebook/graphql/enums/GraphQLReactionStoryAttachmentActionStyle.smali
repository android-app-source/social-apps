.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum ADMIN_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum LAUNCH_PLACETIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum LIKED_PAGE_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum LIKE_PAGE_IN_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum LIKE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum PAGE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum PAGE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum RESPOND_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum VIEW_IN_APP_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

.field public static final enum WRITE_ON_TIMELINE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739473
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739474
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "SEND_MESSAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739475
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "OPEN_URL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "VIEW_PROFILE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739477
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "WRITE_ON_TIMELINE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->WRITE_ON_TIMELINE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739478
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "VIEW_IN_APP_STORE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_IN_APP_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739479
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "LIKE_STORY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739480
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "RESPOND_TO_EVENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->RESPOND_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739481
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "LAUNCH_PLACETIP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LAUNCH_PLACETIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739482
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "PAGE_OPEN_STORE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739483
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "PAGE_OPEN_COLLECTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739484
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "LIKE_PAGE_IN_ATTACHMENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_PAGE_IN_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739485
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "ADMIN_PAGE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->ADMIN_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739486
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    const-string v1, "LIKED_PAGE_OPTIONS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKED_PAGE_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739487
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->WRITE_ON_TIMELINE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_IN_APP_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->RESPOND_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LAUNCH_PLACETIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_PAGE_IN_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->ADMIN_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKED_PAGE_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739488
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;
    .locals 1

    .prologue
    .line 739489
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 739490
    :goto_0
    return-object v0

    .line 739491
    :cond_1
    const-string v0, "SEND_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739493
    :cond_2
    const-string v0, "OPEN_URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739495
    :cond_3
    const-string v0, "VIEW_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739497
    :cond_4
    const-string v0, "WRITE_ON_TIMELINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->WRITE_ON_TIMELINE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739499
    :cond_5
    const-string v0, "VIEW_IN_APP_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->VIEW_IN_APP_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739501
    :cond_6
    const-string v0, "LIKE_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739503
    :cond_7
    const-string v0, "RESPOND_TO_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->RESPOND_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739505
    :cond_8
    const-string v0, "LAUNCH_PLACETIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 739506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LAUNCH_PLACETIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739507
    :cond_9
    const-string v0, "PAGE_OPEN_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 739508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739509
    :cond_a
    const-string v0, "PAGE_OPEN_COLLECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 739510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->PAGE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739511
    :cond_b
    const-string v0, "LIKE_PAGE_IN_ATTACHMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_PAGE_IN_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto :goto_0

    .line 739513
    :cond_c
    const-string v0, "ADMIN_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->ADMIN_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto/16 :goto_0

    .line 739515
    :cond_d
    const-string v0, "LIKED_PAGE_OPTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 739516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKED_PAGE_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto/16 :goto_0

    .line 739517
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;
    .locals 1

    .prologue
    .line 739518
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;
    .locals 1

    .prologue
    .line 739519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    return-object v0
.end method
