.class public final enum Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum ADDRESS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum DATE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum INFO_REQUEST:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum INFO_REQUEST_CANCEL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum LIST_OF_STRINGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum PAGE_TAGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum PHONE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum STRING:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum TEXT_LINK:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum TEXT_LISTS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final enum UPSELL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738554
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738555
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->DATE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->EMAIL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "INFO_REQUEST"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738559
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "INFO_REQUEST_CANCEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST_CANCEL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "LIST_OF_STRINGS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->LIST_OF_STRINGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "PAGE_TAGS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PAGE_TAGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "PARAGRAPH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "PHONE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PHONE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738564
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "STRING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->STRING:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738565
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "TEXT_LINK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LINK:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738566
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "TEXT_LISTS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LISTS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738567
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    const-string v1, "UPSELL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738568
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->DATE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->EMAIL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST_CANCEL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->LIST_OF_STRINGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PAGE_TAGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PHONE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->STRING:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LINK:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LISTS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738553
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;
    .locals 1

    .prologue
    .line 738522
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 738523
    :goto_0
    return-object v0

    .line 738524
    :cond_1
    const-string v0, "ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738526
    :cond_2
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->DATE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738528
    :cond_3
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->EMAIL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738530
    :cond_4
    const-string v0, "INFO_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738532
    :cond_5
    const-string v0, "INFO_REQUEST_CANCEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738533
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->INFO_REQUEST_CANCEL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738534
    :cond_6
    const-string v0, "LIST_OF_STRINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738535
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->LIST_OF_STRINGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738536
    :cond_7
    const-string v0, "PAGE_TAGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PAGE_TAGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738538
    :cond_8
    const-string v0, "PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738540
    :cond_9
    const-string v0, "PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PHONE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738542
    :cond_a
    const-string v0, "STRING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738543
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->STRING:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738544
    :cond_b
    const-string v0, "TEXT_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 738545
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LINK:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto :goto_0

    .line 738546
    :cond_c
    const-string v0, "TEXT_LISTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 738547
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LISTS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto/16 :goto_0

    .line 738548
    :cond_d
    const-string v0, "UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 738549
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto/16 :goto_0

    .line 738550
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;
    .locals 1

    .prologue
    .line 738552
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;
    .locals 1

    .prologue
    .line 738551
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    return-object v0
.end method
