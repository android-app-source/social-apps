.class public final enum Lcom/facebook/graphql/enums/GraphQLScreenElementType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLScreenElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum ADDRESS_LABEL:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum ALERT_BAR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum DATE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum EMBED:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum HEADING:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum IMAGE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum SEPARATOR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum TEXT_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739920
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "CONFIRMATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739921
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "CONTACT_INFO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739922
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "CONTAINER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "COVER_PHOTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "DATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->DATE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739925
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "EMBED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->EMBED:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739926
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739927
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "FORM_FIELD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739928
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "HEADING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->HEADING:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "IMAGE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "NAVIGABLE_ITEM"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "PARAGRAPH"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "PRODUCT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "SEPARATOR"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->SEPARATOR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "TEXT_ITEM"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->TEXT_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "UNIVERSAL_CHECKOUT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "PAGE_COVER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "ADDRESS_LABEL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS_LABEL:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const-string v1, "ALERT_BAR"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ALERT_BAR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739939
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->DATE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->EMBED:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->HEADING:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->SEPARATOR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->TEXT_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS_LABEL:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ALERT_BAR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739940
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .locals 1

    .prologue
    .line 739941
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 739942
    :goto_0
    return-object v0

    .line 739943
    :cond_1
    const-string v0, "ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739945
    :cond_2
    const-string v0, "ADDRESS_LABEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739946
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ADDRESS_LABEL:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739947
    :cond_3
    const-string v0, "ALERT_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739948
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ALERT_BAR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739949
    :cond_4
    const-string v0, "CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739950
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739951
    :cond_5
    const-string v0, "CONTACT_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 739952
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739953
    :cond_6
    const-string v0, "CONTAINER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 739954
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739955
    :cond_7
    const-string v0, "COVER_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739957
    :cond_8
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 739958
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->DATE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739959
    :cond_9
    const-string v0, "EMBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 739960
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->EMBED:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739961
    :cond_a
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 739962
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739963
    :cond_b
    const-string v0, "FORM_FIELD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739964
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto :goto_0

    .line 739965
    :cond_c
    const-string v0, "HEADING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739966
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->HEADING:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739967
    :cond_d
    const-string v0, "IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 739968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739969
    :cond_e
    const-string v0, "NAVIGABLE_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 739970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739971
    :cond_f
    const-string v0, "PAGE_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 739972
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PAGE_COVER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739973
    :cond_10
    const-string v0, "PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 739974
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739975
    :cond_11
    const-string v0, "PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 739976
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739977
    :cond_12
    const-string v0, "SEPARATOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 739978
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->SEPARATOR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739979
    :cond_13
    const-string v0, "TEXT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 739980
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->TEXT_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739981
    :cond_14
    const-string v0, "UNIVERSAL_CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 739982
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNIVERSAL_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0

    .line 739983
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .locals 1

    .prologue
    .line 739984
    const-class v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .locals 1

    .prologue
    .line 739985
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    return-object v0
.end method
