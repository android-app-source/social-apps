.class public final enum Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum CROWDSOURCING:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum CROWDSOURCING_MULTI_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum CROWDSOURCING_OSM_STREET_NAME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum CROWDSOURCING_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum GRAPH_EDITOR_INFO_CARD:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum PRICE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

.field public static final enum WAIT_TIME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738224
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738225
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "CROWDSOURCING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738226
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "PRICE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->PRICE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738227
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "WAIT_TIME"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->WAIT_TIME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738228
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "CROWDSOURCING_MULTI_VALUE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_MULTI_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738229
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "CROWDSOURCING_OSM_STREET_NAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_OSM_STREET_NAME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738230
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "CROWDSOURCING_SUGGESTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738231
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    const-string v1, "GRAPH_EDITOR_INFO_CARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->GRAPH_EDITOR_INFO_CARD:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738232
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->PRICE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->WAIT_TIME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_MULTI_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_OSM_STREET_NAME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->GRAPH_EDITOR_INFO_CARD:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;
    .locals 1

    .prologue
    .line 738205
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    .line 738206
    :goto_0
    return-object v0

    .line 738207
    :cond_1
    const-string v0, "CROWDSOURCING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738208
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738209
    :cond_2
    const-string v0, "PRICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738210
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->PRICE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738211
    :cond_3
    const-string v0, "WAIT_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->WAIT_TIME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738213
    :cond_4
    const-string v0, "CROWDSOURCING_MULTI_VALUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738214
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_MULTI_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738215
    :cond_5
    const-string v0, "CROWDSOURCING_OSM_STREET_NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_OSM_STREET_NAME:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738217
    :cond_6
    const-string v0, "CROWDSOURCING_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->CROWDSOURCING_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738219
    :cond_7
    const-string v0, "GRAPH_EDITOR_INFO_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->GRAPH_EDITOR_INFO_CARD:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0

    .line 738221
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;
    .locals 1

    .prologue
    .line 738222
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;
    .locals 1

    .prologue
    .line 738223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    return-object v0
.end method
