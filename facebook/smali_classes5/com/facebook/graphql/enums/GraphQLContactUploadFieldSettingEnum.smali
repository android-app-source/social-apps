.class public final enum Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

.field public static final enum EXTENDED:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

.field public static final enum MINIMAL:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    .line 724717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    const-string v1, "MINIMAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->MINIMAL:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    .line 724718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    const-string v1, "EXTENDED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->EXTENDED:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    .line 724719
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->MINIMAL:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->EXTENDED:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724706
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;
    .locals 1

    .prologue
    .line 724707
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    .line 724708
    :goto_0
    return-object v0

    .line 724709
    :cond_1
    const-string v0, "MINIMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724710
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->MINIMAL:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    goto :goto_0

    .line 724711
    :cond_2
    const-string v0, "EXTENDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724712
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->EXTENDED:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    goto :goto_0

    .line 724713
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;
    .locals 1

    .prologue
    .line 724714
    const-class v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;
    .locals 1

    .prologue
    .line 724715
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    return-object v0
.end method
