.class public final enum Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum BID_GENERATION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum CARD_RANKING:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum IMPRESSION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum VIEW:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

.field public static final enum XOUT:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728271
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728272
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "BID_GENERATION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->BID_GENERATION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728273
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "IMPRESSION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->IMPRESSION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728274
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "VIEW"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->VIEW:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728275
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "CLICK"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728276
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "XOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->XOUT:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728277
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    const-string v1, "CARD_RANKING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CARD_RANKING:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728278
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->BID_GENERATION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->IMPRESSION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->VIEW:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->XOUT:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CARD_RANKING:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728270
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;
    .locals 1

    .prologue
    .line 728255
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    .line 728256
    :goto_0
    return-object v0

    .line 728257
    :cond_1
    const-string v0, "BID_GENERATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->BID_GENERATION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728259
    :cond_2
    const-string v0, "IMPRESSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->IMPRESSION:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728261
    :cond_3
    const-string v0, "VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->VIEW:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728263
    :cond_4
    const-string v0, "CLICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728265
    :cond_5
    const-string v0, "XOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->XOUT:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728267
    :cond_6
    const-string v0, "CARD_RANKING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CARD_RANKING:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0

    .line 728269
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;
    .locals 1

    .prologue
    .line 728254
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;
    .locals 1

    .prologue
    .line 728253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    return-object v0
.end method
