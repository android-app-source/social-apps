.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum DEPRECATED_ALL_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum DEPRECATED_ALL_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727213
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727214
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727215
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "ON"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727216
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "DEPRECATED_ALL_POSTS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727217
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "DEPRECATED_ALL_ACTIVITY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727218
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string v1, "HIGHLIGHTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727219
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 1

    .prologue
    .line 727198
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 727199
    :goto_0
    return-object v0

    .line 727200
    :cond_1
    const-string v0, "OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727201
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 727202
    :cond_2
    const-string v0, "ON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727203
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 727204
    :cond_3
    const-string v0, "DEPRECATED_ALL_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727205
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 727206
    :cond_4
    const-string v0, "DEPRECATED_ALL_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727207
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->DEPRECATED_ALL_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 727208
    :cond_5
    const-string v0, "HIGHLIGHTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727209
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 727210
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 1

    .prologue
    .line 727211
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 1

    .prologue
    .line 727212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method
